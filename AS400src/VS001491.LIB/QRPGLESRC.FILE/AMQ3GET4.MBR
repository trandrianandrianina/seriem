000000	     H                                                                          
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      * Program name: AMQ3GET4                                       *          
000000	      *                                                              *          
000000	      * Description: Sample RPG program that gets messages from      *          
000000	      *              a message queue (example using MQGET)           *          
000000	      *                                                              *          
000000	      * <N_OCO_COPYRIGHT>                                            *          
000000	      * Licensed Materials - Property of IBM                         *          
000000	      *                                                              *          
000000	      * 63H9336                                                      *          
000000	      * (c) Copyright IBM Corp. 1994, 2005 All Rights Reserved.      *          
000000	      *                                                              *          
000000	      * US Government Users Restricted Rights - Use, duplication or  *          
000000	      * disclosure restricted by GSA ADP Schedule Contract with      *          
000000	      * IBM Corp.                                                    *          
000000	      * <NOC_COPYRIGHT>                                              *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      * Function:                                                    *          
000000	      *                                                              *          
000000	      *                                                              *          
000000	      *   AMQ3GET4 is a sample RPG program to get messages from a    *          
000000	      *   message queue, and is an example of MQGET                  *          
000000	      *                                                              *          
000000	      *      -- sample gets messages from the queue named in         *          
000000	      *         the parameter                                        *          
000000	      *                                                              *          
000000	      *      -- displays the contents of the message queue,          *          
000000	      *         assuming each message data to represent a line       *          
000000	      *         of text to be written                                *          
000000	      *                                                              *          
000000	      *         messages are removed from the queue                  *          
000000	      *                                                              *          
000000	      *      -- writes a message for each MQI reason other than      *          
000000	      *         RCNONE; stops if there is a MQI completion code      *          
000000	      *         of CCFAIL                                            *          
000000	      *                                                              *          
000000	      *    Program logic:                                            *          
000000	      *         MQCONN to the target queue manager                   *          
000000	      *         MQOPEN queue for INPUT                               *          
000000	      *         while no MQI failures,                               *          
000000	      *         .  MQGET next message, remove from queue             *          
000000	      *         .  display the result                                *          
000000	      *         .  (no message available is failure, and ends loop)  *          
000000	      *         MQCLOSE the subject queue                            *          
000000	      *         MQDISC from the queue manager                        *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *                                                              *          
000000	      *                                                              *          
000000	      *   Exceptions signaled:  none                                 *          
000000	      *   Exceptions monitored: none                                 *          
000000	      *                                                              *          
000000	      *   AMQ3GET4 has 2 parameters - (1) name of the target queue   *          
000000	      *                               (2) name of the queue manager  *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	     FQSYSPRT   O    F  132        PRINTER                                      
000000	      *                                                                         
000000	      ** call parameters                                                        
000000	      *  Connection handle                                                      
000000	     D HCONN           S             10I 0                                      
000000	      * Options                                                                 
000000	     D OPTS            S             10I 0                                      
000000	      * Object handle                                                           
000000	     D HOBJ            S             10I 0                                      
000000	      * Completion codes                                                        
000000	     D OCODE           S             10I 0                                      
000000	     D CCODE           S             10I 0                                      
000000	      * Reason                                                                  
000000	     D REASON          S             10I 0                                      
000000	      * Buffer length                                                           
000000	     D BUFLEN          S             10I 0                                      
000000	      * Message length                                                          
000000	     D MSGLEN          S             10I 0                                      
000000	      * Buffer                                                                  
000000	     D BUFFER          S             60                                         
000000	      * Buffer pointer                                                          
000000	     D BUFPTR          S               *   INZ (%ADDR(BUFFER))                  
000000	      *                                                                         
000000	      **  Declare MQI structures needed                                         
000000	      * MQI named constants                                                     
000000	     D/COPY CMQG                                                                
000000	      * Object Descriptor                                                       
000000	     D MQOD            DS                                                       
000000	     D/COPY CMQODG                                                              
000000	      * Message Descriptor                                                      
000000	     D MQMD            DS                                                       
000000	     D/COPY CMQMDG                                                              
000000	      * Get message options                                                     
000000	     D MQGMO           DS                                                       
000000	     I/COPY CMQGMOG                                                             
000000	      ** note, sample uses defaults where it can                                
000000	      *                                                                         
000000	      * program parameters are: 48-character target queue name                  
000000	      *                         48-character queue manager name                 
000000	     C     *ENTRY        PLIST                                                  
000000	     C                   PARM                    QNAME            48            
000000	     C                   PARM                    QMNAME           48            
000000	      ** indicate that sample program has started                               
000000	     C                   MOVEL     'start'       STATUS            5            
000000	     C                   EXCEPT    SAMPLE                                       
000000	      *                                                                         
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *   Create object descriptor for source queue                  *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	     C                   MOVEL     QNAME         ODON             48            
000000	      *                                                                         
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *   Connect to the queue manager                               *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	     C                   CALLP     MQCONN(QMNAME : HCONN : OCODE : REASON)      
000000	      *                                                                         
000000	      * report reason and stop if it failed                                     
000000	     C     OCODE         IFEQ      CCFAIL                                       
000000	      **  "MQCONN ended with reason code ...."                                  
000000	     C                   MOVEL     'MQCONN'      CNAME             6            
000000	     C                   MOVE      REASON        RCODE            10            
000000	     C                   EXCEPT    MQCALL                                       
000000	      **  "unable to connect to MQM"                                            
000000	     C                   MOVE      OCODE         CCODE                          
000000	     C                   EXCEPT    MESSC                                        
000000	     C                   SETON                                        LR        
000000	     C                   RETURN                                                 
000000	     C                   ENDIF                                                  
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *   Open the named message queue for input (and fail if MQM    *          
000000	      *   is quiescing); exclusive or shared use of the queue is     *          
000000	      *   controlled by the queue definition here                    *          
000000	      *                                                              *          
000000	      *   Resulting queue handle is HOBJ                             *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	      * options are input-as-queue-def and fail-if-quiescing                    
000000	     C     OOINPQ        ADD       OOFIQ         OPTS                           
000000	      * call ...                                                                
000000	     C                   CALLP     MQOPEN(HCONN : MQOD : OPTS : HOBJ :          
000000	     C                              OCODE : REASON)                             
000000	      *                                                                         
000000	      * report reason, if any; stop if failed                                   
000000	     C     REASON        IFNE      RCNONE                                       
000000	      **  "MQOPEN ended with reason code ...."                                  
000000	     C                   MOVEL     'MQOPEN'      CNAME             6            
000000	     C                   MOVE      REASON        RCODE            10            
000000	     C                   EXCEPT    MQCALL                                       
000000	     C                   ENDIF                                                  
000000	      *                                                                         
000000	     C     OCODE         IFEQ      CCFAIL                                       
000000	      **  "unable to open queue for input"                                      
000000	     C                   EXCEPT    MESS                                         
000000	     C                   ENDIF                                                  
000000	      *                                                                         
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *   Get messages from message queue                            *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	      **  initial loop condition based on result of MQOPEN                      
000000	     C                   MOVE      OCODE         CCODE                          
000000	      *   buffer length available ...                                           
000000	     C                   Z-ADD     60            BUFLEN                         
000000	      **  start of loop to read all the messages . . .                          
000000	     C     CCODE         DOWNE     CCFAIL                                       
000000	      * option is to wait up to 15 seconds for next message                     
000000	     C                   Z-ADD     GMWT          GMOPT                          
000000	      *   wait                                                                  
000000	     C                   ADD       GMCONV        GMOPT                          
000000	     C                   Z-ADD     15000         GMWI                           
000000	      *   up to 15sec                                                           
000000	      **  MsgId and CorrelId are selectors that must be cleared                 
000000	      **  to get messages in sequence, and they are set each MQGET              
000000	     C                   MOVEL     MINONE        MDMID                          
000000	     C                   MOVEL     CINONE        MDCID                          
000000	      **  clear buffer because MQGET only fills to length of message            
000000	     C                   MOVEL     *BLANKS       BUFFER                         
000000	      *  call ...                                                               
000000	     C                   CALLP     MQGET(HCONN : HOBJ : MQMD : MQGMO :          
000000	     C                              BUFLEN : BUFPTR : MSGLEN : CCODE :          
000000	     C                              REASON)                                     
000000	      *                                                                         
000000	      ** report reason, if any                                                  
000000	     C                   SELECT                                                 
000000	     C     REASON        WHENEQ    RCNONE                                       
000000	      ** special text for  "no more messages"                                   
000000	     C     REASON        WHENEQ    RC2033                                       
000000	     C                   EXCEPT    NOMESS                                       
000000	      **  otherwise say "MQGET ended with reason code ...."                     
000000	     C                   OTHER                                                  
000000	     C                   MOVEL     'MQGET '      CNAME                          
000000	     C                   MOVE      REASON        RCODE                          
000000	     C                   EXCEPT    MQCALL                                       
000000	     C                   ENDSL                                                  
000000	      *                                                                         
000000	      ** . . .display each message received                                     
000000	     C     CCODE         IFNE      CCFAIL                                       
000000	     C                   EXCEPT    TEXT                                         
000000	     C                   ENDIF                                                  
000000	      *                                                                         
000000	      **  end DO-while loop; MQI failure causes loop to end                     
000000	     C                   ENDDO                                                  
000000	      *                                                                         
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *   Close the source queue (if it was opened)                  *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	      * test if queue had been opened                                           
000000	     C     OCODE         IFNE      CCFAIL                                       
000000	      * ... close queue (with no options) if it is open                         
000000	     C                   Z-ADD     CONONE        OPTS                           
000000	     C                   CALLP     MQCLOSE(HCONN : HOBJ : OPTS :                
000000	     C                              CCODE : REASON)                             
000000	      *                                                                         
000000	      * report reason, if any, resulting from this call                         
000000	     C     REASON        IFNE      RCNONE                                       
000000	      **  "MQCLOS ended with reason code ...."                                  
000000	     C                   MOVEL     'MQCLOS'      CNAME                          
000000	     C                   MOVE      REASON        RCODE                          
000000	     C                   EXCEPT    MQCALL                                       
000000	     C                   ENDIF                                                  
000000	      *                                                                         
000000	      * end if (OCODE  not CCFAIL)                                              
000000	     C                   ENDIF                                                  
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      *   Disconnect from the queue manager                          *          
000000	      *                                                              *          
000000	      ****************************************************************          
000000	     C                   CALLP     MQDISC(HCONN : OCODE : REASON)               
000000	      *                                                                         
000000	      * report reason and stop if it failed                                     
000000	     C     OCODE         IFEQ      CCFAIL                                       
000000	      **  "MQDISC ended with reason code ...."                                  
000000	     C                   MOVEL     'MQDISC'      CNAME             6            
000000	     C                   MOVE      REASON        RCODE            10            
000000	     C                   EXCEPT    MQCALL                                       
000000	     C                   ENDIF                                                  
000000	      *                                                                         
000000	      ** "Sample AMQ3GET4 end"                                                  
000000	     C                   MOVEL     'end  '       STATUS            5            
000000	     C                   EXCEPT    SAMPLE                                       
000000	     C                   SETON                                        LR        
000000	      ****************************************************************          
000000	     OQSYSPRT   E            SAMPLE         1                                   
000000	     O                                           20 'Sample AMQ3GET4 '          
000000	     O                       STATUS              25                             
000000	     O          E            TEXT           1                                   
000000	     O                                           10 'message <'                 
000000	     O                       BUFFER                                             
000000	     O                                              '>'                         
000000	     O          E            MQCALL         1                                   
000000	     O                       CNAME               10                             
000000	     O                                              ' ended with reason code'   
000000	     O                       RCODE               45                             
000000	     O          E            MESSC          1                                   
000000	     O                                           25 'Unable to connect to'      
000000	     O                                              ' queue manager'            
000000	     O          E            MESS           1                                   
000000	     O                                           25 'Unable to open queue'      
000000	     O                                              ' for input'                
000000	     O          E            NOMESS         1                                   
000000	     O                                           25 'no more messages'          
000000	      ****************************************************************          
000000	      *                                                              *          
000000	      * END OF AMQ3GET4                                              *          
000000	      *                                                              *          
000000	      ****************************************************************          
