//=================================================================================================
//==>                                                                       23/12/2009 - 23/07/2010
//==> Permet de lancer des requ�tes sur un I5
//==> A faire:
//=================================================================================================
package as400;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class RequeteSQLDB2
{
    private Connection system=null;
	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

    /**
     * V�rification des drivers
     * @return
     * @throws SQLException 
     */
    private void verifDriver() throws SQLException
	{
    	DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
    }

    /**
     * Connection au serveur
     * @param serveur
     * @param user
     * @param password
     * @return
     */
    public boolean connexion(String serveur, String user, String password)
	{
        try
        {
        	verifDriver();
    	    system = DriverManager.getConnection("jdbc:as400://" + serveur, user, password);
        }
        catch (SQLException exc)
        {
            msgErreur += "Echec de la connection: " + exc.getMessage();
            return false;
        }
        return true;
    }

    /**
     * Connection au serveur
     * @param serveur
     * @return
     */
    public boolean connexion(String serveur)
	{
    	try
        {
        	verifDriver();
    	    system = DriverManager.getConnection("jdbc:as400://" + serveur);
        }
        catch (SQLException exc)
        {
            msgErreur += "Echec de la connection: " + exc.getMessage();
            return false;
        }
        return true;
    }

    /**
     * Lancement d'une requ�te Select - Pb avec le select non clos�
     * @param requete
     * @return
     */
    public ResultSet requeteSelect(String requete)
    {
    	PreparedStatement select=null;
    	try
    	{
    		// Pr�pare le statement
    		select = system.prepareStatement(requete);
    		//return select.executeQuery();
    		ResultSet rs = select.executeQuery();
    		//select.close();
        	return rs;
    	}
    	catch (SQLException exc)
    	{
    		if (select != null) try {select.close();} catch (SQLException e) {}
    		msgErreur += "Echec de la requ�te: " + exc.getMessage();
    		return null;
    	}
    }

    /**
     * Retourne le nombre d'enregistrement suite � un Select
     * @param requete
     * @return
     */
    public int nbrEnrgSelect(String requete)
    {
    	int count=0;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
    		select = system.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			while (rs.next())
    				count++;
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "Echec de la requ�te: " + exc.getMessage();
    		return -1;
    	}
    	
    	return count;
    }
    
    /**
     * Retourne le premier enregistrement de la colonne voulu suite � un Select
     * @param requete
     * @return
     * @throws SQLException 
     */
    public String firstEnrgSelect(String requete, String labelCol)
    {
    	String valeur=null;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
    		select = system.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			rs.next();
    			valeur = rs.getString(labelCol);
    			rs.close();
    		}
			select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "Echec de la requ�te: " + exc.getMessage();
    		return null;
    	}
    	
    	return valeur;
    }
    
    /**
     * Retourne le premier enregistrement de la colonne voulu suite � un Select (col doit �tre sup�rieur � 0)
     * @param requete
     * @return
     * @throws SQLException 
     */
    public String firstEnrgSelect(String requete, int col)
    {
    	String valeur=null;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
    		select = system.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			rs.next();
    			valeur = rs.getString(col);
    			rs.close();
    		}
			select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "Echec de la requ�te: " + exc.getMessage();
    		return null;
    	}
    	
    	return valeur;
    }

    /**
     * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist
     * @param requete
     * @return
     */
    public ArrayList<String> traitementSelect(String requete)
    {
    	StringBuffer chaine = new StringBuffer(0);
    	ArrayList<String> resultat = new ArrayList<String>();
    	int i=0;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
//System.out.println(requete);    			
    		select = system.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
//System.out.println(rs.getMetaData().getColumnCount());    			
    			while (rs.next())
        		{
        			chaine.setLength(0);
        			// Si plusieurs colonnes
        			if (rs.getMetaData().getColumnCount() > 1)
        			{
        				for (i=1; i <= rs.getMetaData().getColumnCount()-1; i++)
        					chaine.append(rs.getString(i)).append("|");
            			chaine.append(rs.getString(i));
            			resultat.add(chaine.toString());
        			}
        			else // Une seule colonne
       					resultat.add(rs.getString(1));
        		}
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "Echec de la requ�te: " + exc.getMessage();
    		System.out.println(msgErreur);
    		return null;
    	}
//System.out.println("--> " + resultat.size());
    	return resultat;
    }

    /**
     * Lancement d'une requ�te insert, delete, update
     * @param requete
     * @return
     */
    public int requete(String requete)
    {
    	Statement stmt=null;
    	try
    	{
    		// Prepare le statement
    		stmt = system.createStatement();
            int resultat = stmt.executeUpdate(requete);
            stmt.close();
            return resultat;
    	}
    	catch (SQLException exc)
    	{
    		if (stmt != null) try{stmt.close();}catch (SQLException e){}
    		msgErreur += "Echec de la requ�te: " + exc.getMessage();
    		return -1;
    	}
    }
    
    /**
     * D�connexion du serveur
     */
    public void deconnexion()
    {
    	try
    	{
    		system.close();
    	}
    	catch (SQLException exc)
    	{
    		msgErreur += "Echec de la d�connection: " + exc.getMessage();
    	}
    }
    
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
