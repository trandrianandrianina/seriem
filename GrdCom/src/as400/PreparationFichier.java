//=================================================================================================
//==>                                                                       20/09/2010 - 27/10/2015
//==>  Ordre CL pour pr�parer le PINJBDV sur l'AS/400
//==> A faire:
//=================================================================================================
package as400;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.LocalDataArea;

public class PreparationFichier
{
	// Variables
	private AS400 system=null;
	private CommandCall commandeCL=null;
	
	/**
	 * Constructeur
	 * @param as
	 * @param user
	 * @param mdp
	 */
	public PreparationFichier(String as, String user, String mdp)
	{
		system = new AS400(as, user, mdp);
		commandeCL = new CommandCall(system);
	}
	
	
	/**
	 * Lance un ordre sur l'AS/400 
	 * @param chaine
	 * @return
	 */
	private String commande(String chaine)
	{
		if (system == null) return null;
		if ((chaine == null) || (chaine.trim().equals(""))) return null;
		
		try
		{
			boolean ret = commandeCL.run(chaine);
			AS400Message[] msg = commandeCL.getMessageList();
			if (msg.length > 0)
				return msg[0].getID();
			return "CPF0000"; 
		}
		catch (Exception e)	{}
		return "CPF0000";
	}
	
	/**
	 * Cr�ation du fichier PINJBDV
	 * @param bib
	 * @return
	 */
	public boolean prepareFichier(String bib, char lettre)
	{
		if ((bib == null) || (bib.trim().equals(""))) return false;
		if (lettre == ' ') lettre = 'W';
		
		// Ecriture dans la *LDA
		String lda=null;
		LocalDataArea area = new LocalDataArea(system);
		try
		{
			lda = area.read();
			lda = lda.substring(0, 1024) + lettre;
			area.write(lda);
		}
		catch (Exception e) {}

		// Cr�ation bibiloth�que de travail
		commande("CRTLIB LIB("+bib+")");
		commande("CHGCURLIB CURLIB("+bib+")");
		
		nettoyerFichiers("PINJBDV", bib, lettre);
		nettoyerFichiers("PINJBDVDSE", bib, lettre);
		nettoyerFichiers("PINJBDVDSL", bib, lettre);
		nettoyerFichiers("PINJBDVDSX", bib, lettre);

		return true;
	}

	/**
	 * Nettoie et cr�er les fichiers d'injection
	 * @return
	 */
	private boolean nettoyerFichiers(String pinjbdv, String bib, char lettre)
	{
		// Nettoyage ou cr�ation du fichier
		String ret = commande("CLRPFM FILE("+bib +"/"+pinjbdv+")");
//System.out.println("-clr-> " + ret);		
		if (!ret.equals("CPF3101"))
			ret = commande("CRTPF FILE("+bib +"/"+pinjbdv+") SRCFILE("+lettre+"GVMAS/QDDSFCH) SRCMBR(*FILE) OPTION(*NOSRC *NOLIST) SIZE(*NOMAX) WAITFILE(*CLS) WAITRCD(*NOMAX)");
		return true;
	}
	
	/**
	 * Lancement du programme d'injection
	 * @param bib
	 * @return
	 */
	public boolean injecteFichier(String bib, char lettre)
	{
		if ((bib == null) || (bib.trim().equals(""))) return false;
		if (lettre == ' ') lettre = 'W';
		
		// On fait le CPY
		String ret = commande("CPYF FROMFILE("+bib+"/PINJBDVDSE) TOFILE("+bib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
//System.out.println("--> " + ret);		
		if (ret.equals("CPC2955") || ret.equals("CPF4011"))
		{
			ret = commande("CPYF FROMFILE("+bib+"/PINJBDVDSL) TOFILE("+bib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
//System.out.println("--> " + ret);
			if (ret.equals("CPC2955") || ret.equals("CPF4011"))
			{
				ret = commande("CPYF FROMFILE("+bib+"/PINJBDVDSX) TOFILE("+bib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
//System.out.println("--> " + ret);

				// Lancement du programme d'injection
				ret = commande("ADDLIBLE LIB(M_GPL)");
				ret = commande("ADDLIBLE LIB("+lettre+"EXMAS)");
				ret = commande("ADDLIBLE LIB("+lettre+"EXPAS)");
				ret = commande("ADDLIBLE LIB("+lettre+"GVMAS)");
				ret = commande("ADDLIBLE LIB("+lettre+"GVMX)");
				ret = commande("ADDLIBLE LIB("+lettre+"S001522)");
System.out.println("--> ADDLIBLE "+lettre+"S001522 :" + ret);				
				//ret = commande("CALL PGM(SGVMIVCQ) ");
				ret = commande("CALL PGM(SGVMIVCL) PARM('0')"); // <- Ca plante chez nous (msgo CPF4103) ca cache un truc !! (10/01/2013)
				// Attention peut planter sur le SNDRCVFM (*REQUESTER) car le fichier PSEMSEC n'est pas l� o� il faut... Et M_GPL doit �tre en ligne (sinon �a marche moins bien)
System.out.println("--> Injection :" + ret);				
				return true;
			}
		}
		return false;
	}

	/**
	 * D�connecte la session
	 */
	public void deconnecte()
	{
		if (system != null)
			system.disconnectAllServices();
	}
}
