
// http://www.jdom.org/dist/binary/

package test;

import java.io.*;
import java.util.List;
import java.util.Iterator;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class TestXmlJDom
{
   static Document document;
   static Element racine;

   public static void main(String[] args)
   {
      //On cr�e une instance de SAXBuilder
      SAXBuilder sxb = new SAXBuilder();
      try
      {
         //On cr�e un nouveau document JDOM avec en argument le fichier XML
         //Le parsing est termin� ;)
         document = sxb.build(new File("ORDERS_P_99866031000066_2109576.xml"));
      }
      catch(Exception e){}

      //On initialise un nouvel �l�ment racine avec l'�l�ment racine du document.
      racine = document.getRootElement();
      Namespace espaceNom = Namespace.getNamespace("", "http://www.psol.com/FT/2002/ORDERS");
      racine.removeNamespaceDeclaration(espaceNom);
	  System.out.println("--> " + racine.getName() + " " + racine.getNamespace());
	  
	  List cmd = document.getRootElement().getChildren("Commande");
	  System.out.println("-*-> " + cmd.size());
      //M�thode d�finie dans la partie 3.2. de cet article
      afficheALL();
      
      System.out.println("--> Termin�");
   }
   
   //Ajouter cette m�thodes � la classe JDOM2
   static void afficheALL()
   {
	   List listeEnfants = racine.getChildren();
	   System.out.println("--> " + listeEnfants.size() );    	  

      //On cr�e un Iterator sur notre liste
      Iterator i = listeEnfants.iterator();
      while(i.hasNext())
      {
         //On recr�e l'Element courant � chaque tour de boucle afin de
         //pouvoir utiliser les m�thodes propres aux Element comme :
         //selectionner un noeud fils, modifier du texte, etc...
         Element courant = (Element)i.next();
         //On affiche le nom de l'element courant
         System.out.println(courant.getName() + " " + courant.getChildren().size());
      }
   }
}


