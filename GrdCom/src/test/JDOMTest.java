package test;

import java.util.*;
import java.io.*;
import java.io.*;
import org.jdom.*;
import org.jdom.output.*; 
 
 
public class JDOMTest {
     //simple class de test
    //later cette classe aura une m�thode qui appel lobjet bean de la page jsp et extrait le fichier
	//XML! check conception later, better way
	
	private Collection objectToBeExtracted;
	///////les element statiques pour d�finir les noms des balises...
       //Nous allons commencer notre arborescence en cr�ant la racine XML
	   //qui sera ici "personnes".
	   static Element racine = new Element("personnes");
 
	   //On cr�e un nouveau Document JDOM bas� sur la racine que l'on vient de cr�er
	   static org.jdom.Document document = new Document(racine);
 
	   public static void main(String[] args)
	   {
		   new JDOMTest();
	   }
	   
	public	void testFunction()
	{
		  //On cr�e un nouvelle Element etudiant et on l'ajoute 
	      //en temps qu'Element de racine
	      Element etudiant = new Element("etudiant");
	      racine.addContent(etudiant);
 
	      //On cr�e un nouvelle Attribut classe et on l'ajoute � etudiant
	     //gr�ce � la m�thode setAttribute
	      Attribute classe = new Attribute("classe","P2");
	      etudiant.setAttribute(classe);
 
	      //On cr�e un nouvelle Element nom, on lui assigne du text 
	      //et on l'ajoute en temps qu'Element de etudiant
	      Element nom = new Element("nom");
	      nom.setText("CynO");
	      etudiant.addContent(nom);
 
	      //Les deux appels qui suivent seront d�finis dans la partie 2.3.
	      //affiche();
	      enregistre("Exercice 1.xml");	
		;
	}
	//les deux m�thodes affiche et enregistre
	////////////////////////////////////////////////////////////////////////////////////////////
	
//	Ajouter ces deux m�thodes � notre class JDOM
//once test Ok, cette partie � am�liorer le maximum
//selon les besoins du stockage ..
	static void affiche()
	{ 
	   try
	   {
	      //On utilise ici un affichage classic avec getPrettyFormat()
	      XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
	      sortie.output(document, System.out);
	   }
	   catch (java.io.IOException e){}
	}
 
	static void enregistre(String fichier)
	{ 
	   try
	   {
	      //On utilise ici un affichage classic avec getPrettyFormat()
	      XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
	      //Remarquez qu'il suffit simplement de cr�er une instance de FileOutputStream
	      //avec en argument le nom du fichier pour effectuer la s�rialisation.
	      sortie.output(document, new FileOutputStream(fichier));
	   }
	   catch (java.io.IOException e){}
	} 
	////////////////////////////////////////////////////////////////////////////////////////////
 
}
