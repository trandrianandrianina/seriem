//=================================================================================================
//==>                                                                       17/09/2010 - 20/09/2010
//==>  Description des préférences de l'utilisateur
//==> A faire:
//=================================================================================================
package programme;


public class Parametres
{
	// Variables
	private String profil=null;
	private String mdp=null;
	private String AS400=null;
	private String dossierSource=null;
	private String curLib=null;
	private char lettre='W';
	
	/**
	 * @return the profil
	 */
	public String getProfil()
	{
		return profil;
	}
	/**
	 * @param profil the profil to set
	 */
	public void setProfil(String profil)
	{
		this.profil = profil;
	}
	/**
	 * @return the mdp
	 */
	public String getMdp()
	{
		return mdp;
	}
	/**
	 * @param mdp the mdp to set
	 */
	public void setMdp(String mdp)
	{
		this.mdp = mdp;
	}
	/**
	 * @return the aS400
	 */
	public String getAS400()
	{
		return AS400;
	}
	/**
	 * @param aS400 the aS400 to set
	 */
	public void setAS400(String aS400)
	{
		AS400 = aS400;
	}
	/**
	 * @return the dossierSource
	 */
	public String getDossierSource()
	{
		return dossierSource;
	}
	/**
	 * @param dossierSource the dossierSource to set
	 */
	public void setDossierSource(String dossierSource)
	{
		this.dossierSource = dossierSource;
	}
	/**
	 * @return the curLib
	 */
	public String getCurLib()
	{
		return curLib;
	}
	/**
	 * @param curLib the curLib to set
	 */
	public void setCurLib(String curLib)
	{
		this.curLib = curLib;
	}
	/**
	 * @return the lettre
	 */
	public char getLettre()
	{
		return lettre;
	}
	/**
	 * @param lettre the lettre to set
	 */
	public void setLettre(char lettre)
	{
		this.lettre = lettre;
	}
	
}
