//=================================================================================================
//==>                                                                       17/09/2010 - 17/09/2010
//==>  Permet de g�rer (enregistrement, lecture) des param�tres de l'interface
//==> A faire:
//=================================================================================================
package programme;

import java.io.File;

import outilsXml.XMLTools;

public class ParametresManager
{
	// Constantes
	private static final String dossierParametre="injectionxml";
	private static final String fichierParametre="parametres.xml";

	// Variables
	private Parametres parametres=null; 

	/**
	 * Retourne les param�tres de l'interface 
	 * @return the prefsUser
	 */
	public Parametres getParametres()
	{
		if (parametres == null)
			if (!chargeParametres()) parametres = new Parametres();
		return parametres;
	}

	/**
	 * Sauvegarde les param�tres sur disque
	 */
	public void sauveParametres()
	{
		if (parametres == null) return;
		// V�rification de l'existence du fichier param�tre 
		File parafic = new File(System.getProperty("user.home") + File.separatorChar+ dossierParametre + File.separatorChar + fichierParametre);
		if (!parafic.getParentFile().exists()) parafic.getParentFile().mkdirs();
		
		// Sauvegarde du fichier sur disque
		try
		{
			XMLTools.encodeToFile(parametres, parafic.getAbsolutePath());
		}
		catch (Exception e) {}
	}
	
	/**
	 * Charge les param�tres � partir d'un fichier dans le home de l'utilisateur
	 */
	private boolean chargeParametres()
	{
		// V�rification de l'existence du fichier param�tre 
		File parafic = new File(System.getProperty("user.home") + File.separatorChar+ dossierParametre + File.separatorChar + fichierParametre);
		if (!parafic.exists()) return false;
		
		try
		{
			parametres = (Parametres) XMLTools.decodeFromFile(parafic.getAbsolutePath());
		}
		catch (Exception e)
		{
			 return false;
		}
		return true;
	}

}
