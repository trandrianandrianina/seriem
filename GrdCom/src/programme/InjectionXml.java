//=================================================================================================
//==>                                                                       17/09/2010 - 14/03/2012
//==>  Interface pour injecter les fichiers XML dans le PINJBDV
//==> A faire:
//=================================================================================================
// --> Correction mineure le 14/03/2012 : le bouton inject� se d�grise lors d'un changement de dossier via le bouton Parcourir

package programme;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import analyse.AnalyseFichierFT;
import as400.PreparationFichier;
import as400.RequeteSQLDB2;


/**
 * @author St�phane V�n�ri
 */
public class InjectionXml extends JFrame
{
	// Constantes
	private static final long serialVersionUID = 1L;
	
	// Variables
	private RequeteSQLDB2 db2 = new RequeteSQLDB2();
	private Parametres parametres=null;
	private ParametresManager parametresManager=new ParametresManager();
	private PreparationFichier prep=null;
	private InjectionXml moa=this;
	private Console console =null;

	

	/**
	 * Constructeur
	 */
	public InjectionXml()
	{
		// Utilisation du Look & Feel syst�me
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e) {}

		// Initialisations
		initComponents();
		initInterface();
		
		// Cr�ation de la console et redirection des messages de la console
		console = new Console(moa);
		PrintStream out = new PrintStream( new TextAreaOutputStream( console.getArea() ) );
		System.setOut( out );
		System.setErr( out );
		
		okButton.setEnabled(true);
	}

	
	//-->
	//--> M�thodes priv�es
	//-->

	/**
	 * Initialisation au lancement de l'application 
	 */
	private void initInterface()
	{
		lectureParametres();
		rafraichirTable();
	}

	/**
	 * 
	 */
	private void lanceTraitement()
	{
		Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

			new Thread(new Runnable() {
			public void run()
			{
				try
				{
					traitement();
					Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					console.setVisible(true);
				}
			}
		}).start();
		
	}
	
	/**
	 * Traitement des fichiers s�lectionn�s
	 */
	private void traitement()
	{
		ArrayList<String> listeFichier = getFichiersSelectionnes();
		if (listeFichier == null) return;
		String fichiererreur=null;
		String erreur=null;

		// Pr�pare les fichiers sur l'AS/400
		preparationFichier();

		// Connexion � la base de l'AS/400
		boolean ret = db2.connexion(parametres.getAS400(), parametres.getProfil(), parametres.getMdp());
		if (!ret)
		{
			System.out.println("--> " + db2.getMsgErreur());
			console.setVisible(true);
		}
		if (db2 == null) return;
		
		// Analyse des fichiers XML et maj du pinjbdv
		pbar_Traitement.setStringPainted(true);
		pbar_Traitement.setMaximum(listeFichier.size()+1);
		pbar_Traitement.setValue(0);
		pbar_Traitement.setString("Traitement des fichiers xml");
		for (int i=0; i<listeFichier.size(); i++)
		{
//System.out.println("--> " + listeFichier.get(i));

			// Analyse les fichiers XML 
			AnalyseFichierFT ft = new AnalyseFichierFT(listeFichier.get(i));
			ft.traitement(db2, parametres.getLettre(), parametres.getCurLib());
			//ft.afficheListe();
			erreur = ft.miseafichier(db2, parametres.getLettre(), parametres.getCurLib()); 
			if (erreur != null)
			{
				fichiererreur = listeFichier.get(i);
				break;
			}
			pbar_Traitement.setValue(i+1);
		}

		pbar_Traitement.setString("Injection des donn�es dans la base");
		if (erreur == null)
		{
			ret = injectionFichier();
			if (ret)
			{
				// On renomme les fichiers trait�s
				for (int i=0; i<listeFichier.size(); i++)
				{
					File ficori = new File(listeFichier.get(i));
					File ficdes = new File(listeFichier.get(i).replaceFirst(".xml", ".bak"));
					ficori.renameTo(ficdes);
				}
				pbar_Traitement.setValue(pbar_Traitement.getValue()+1);
				Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
				pbar_Traitement.setString("Injection termin�e.");
				JOptionPane.showMessageDialog(this, "Les donn�es ont bien �t� inject�es dans la base.", " Injection PINJBDV", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else
		{
			Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
			console.setVisible(true);
			System.out.println("--> Fichier concern� : " + fichiererreur);
			System.out.println("--> Raison : \n " + db2.getMsgErreur());
			System.out.println("--> Requ�te : \n " + erreur);
			JOptionPane.showMessageDialog(this, " Traitement arr�t�.\nProbl�me lors du remplissage du PINJBDV\navec les donn�es du fichier:\n"+fichiererreur, " Erreur", JOptionPane.ERROR_MESSAGE);
		}
		//JOptionPane.showMessageDialog(this, "Yop", " Debug", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Remplit une table avec les donn�es
	 */
	private void rafraichirTable()
	{
		String[] liste = listageDossier(parametres.getDossierSource());
		chargementTable(liste);
	}
	
	/**
	 * Retourne la liste des fichiers s�lectionn�s pour le traitement
	 * @return
	 */
	private ArrayList<String> getFichiersSelectionnes()
	{
		ArrayList<String> listeFichiers = new ArrayList<String>();

		// Parcourt de la table pour ne prendre que les lignes s�lectionn�es
		for (int i=0; i<t_Fichiers.getModel().getRowCount(); i++)
			if ((Boolean)t_Fichiers.getValueAt(i, 0) == Boolean.TRUE)
				listeFichiers.add(parametres.getDossierSource() + File.separatorChar + t_Fichiers.getValueAt(i, 1));
		
		return listeFichiers;
	}
	
	/**
	 * Charge la table avec la liste des fichiers xml trouv�s
	 * @param liste
	 */
	private void chargementTable(String[] liste)
	{
		if (liste == null) return;
		
		// Cr�ation de la liste des lignes � s�lectionner
		Object[][] data = new Object[liste.length][2];
		for (int i=0; i<liste.length; i++)
		{
			data[i][0] = Boolean.TRUE;
			data[i][1] = liste[i];
		}
		
		// Chargement de la table des fichiers
		t_Fichiers.setModel(new DefaultTableModel(
				data,
				new String[] {
					"S\u00e9lection", "Fichiers"
				}
			) {
				private static final long serialVersionUID = 1L;
				Class[] columnTypes = new Class[] {
					Boolean.class, String.class
				};
				boolean[] columnEditable = new boolean[] {
					true, false
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				public boolean isCellEditable(int rowIndex, int columnIndex) {
					return columnEditable[columnIndex];
				}
			});
			{
				TableColumnModel cm = t_Fichiers.getColumnModel();
				cm.getColumn(0).setMaxWidth(80);
			}
			
		// On d�grise le bouton d'injection
		if (liste.length > 0) okButton.setEnabled(true);
	}
	
	/**
	 * Ouvre la boite de dialogue pour s�lectionner un dossier
	 */
	private void selectionDossier()
	{
		// On ouvre directement la boite de dialogue pour s�l�ctionner le dossier source 
		File dossier = getSelectionDossier(".");
		if (dossier != null)
		{
			parametres.setDossierSource(dossier.getAbsolutePath());
			l_DossierSource.setText(dossier.getAbsolutePath());
		}
	}
	
	/**
	 * Chargement des param�tres 
	 */
	private void lectureParametres()
	{
		parametres = parametresManager.getParametres();
		
		// Si on n'a pas trouv� le fichier param�tre
		if (parametres.getDossierSource() == null)
		{
			selectionDossier();
		}
		else // Sinon fichier param�tres trouv�
		{
			if (parametres.getAS400() != null) tf_AS400.setText(parametres.getAS400());
			if (parametres.getProfil() != null) tf_User.setText(parametres.getProfil());
			if (parametres.getMdp() != null) pf_Mdp.setText(parametres.getMdp());
			if (parametres.getDossierSource() != null) l_DossierSource.setText(parametres.getDossierSource());
			if (parametres.getCurLib() != null) tf_CurLib.setText(parametres.getCurLib());
			tf_Lettre.setText(""+parametres.getLettre());
		}
	}
	
	/**
	 * V�rification & enregistrement des param�tres sur le disque
	 */
	private boolean enregistrementParametres()
	{
		// Contr�le des zones saisies
		if (!tf_AS400.getText().trim().equals("")) parametres.setAS400(tf_AS400.getText().trim());
		else return false;
		if (!tf_User.getText().trim().equals("")) parametres.setProfil(tf_User.getText().trim());
		else return false;
		char[] mdp = pf_Mdp.getPassword(); 
		if ((mdp != null) && (mdp.length > 0)) parametres.setMdp(new String(mdp));
		else return false;
		if (!tf_CurLib.getText().trim().equals("")) parametres.setCurLib(tf_CurLib.getText().trim());
		else return false;
		if (!tf_Lettre.getText().trim().equals("")) parametres.setLettre(tf_Lettre.getText().trim().charAt(0));
		else return false;
		
		// Enregistrement dans le fichier
		parametresManager.sauveParametres();
		
		return true;
	}

	/**
	 * Ouvre une boite de dialogue afin de s�lectionner un dossier  
	 */
	private File getSelectionDossier(String origine)
	{
		if (origine == null) origine = ".";
		JFileChooser choix = new JFileChooser(origine);
		choix.setApproveButtonText("S�lectionner");
		choix.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (choix.showOpenDialog(this) == JFileChooser.CANCEL_OPTION) return null;
		return choix.getSelectedFile();
	}
	
	/**
	 * Listage des fichiers xml
	 */
	private String[] listageDossier(String dossier)
	{
		File fdossier = new File(dossier);
		if (!fdossier.exists()) return null;
		
		return fdossier.list(new FilenameFilter() {
			public boolean accept(File arg0, String arg1)
			{
				return arg1.toLowerCase().startsWith("orders_") && arg1.toLowerCase().endsWith(".xml");
			}
			});
	}
	
	/**
	 * Preparation du fichier sur l'AS/400
	 * @return
	 */
	private boolean preparationFichier()
	{
		prep = new PreparationFichier(parametres.getAS400(), parametres.getProfil(), parametres.getMdp());
		boolean ret = prep.prepareFichier(parametres.getCurLib(), parametres.getLettre());

		return ret;
	}

	/**
	 * Injection du fichier sur l'AS/400
	 * @return
	 */
	private boolean injectionFichier()
	{
		if (prep == null) return false;
		boolean ret = prep.injecteFichier(parametres.getCurLib(), parametres.getLettre());
		
		return ret;
	}

	//-->
	//--> Gestion des actions
	//-->
	
	private void bt_ParcourirActionPerformed(ActionEvent e)
	{
		selectionDossier();
		rafraichirTable();
	}

	private void cancelButtonActionPerformed(ActionEvent e)
	{
		if (prep != null) prep.deconnecte();
		System.exit(0);
	}

	private void okButtonActionPerformed(ActionEvent e) {
		if (!enregistrementParametres()) return;
		
		okButton.setEnabled(false);
		lanceTraitement();
	}
 

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		panel2 = new JPanel();
		scrollPane1 = new JScrollPane();
		t_Fichiers = new JTable();
		panel4 = new JPanel();
		panel1 = new JPanel();
		l_DossierSource = new JLabel();
		bt_Parcourir = new JButton();
		panel3 = new JPanel();
		l_AS400 = new JLabel();
		tf_AS400 = new JTextField();
		l_User = new JLabel();
		tf_User = new JTextField();
		l_CurLib = new JLabel();
		tf_CurLib = new JTextField();
		l_Mdp = new JLabel();
		pf_Mdp = new JPasswordField();
		l_Lettre = new JLabel();
		tf_Lettre = new JTextField();
		panel5 = new JPanel();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		pbar_Traitement = new JProgressBar();

		//======== this ========
		setTitle("Injection fichier XML dans le PINJBDV (2.3)");
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setName("dialogPane");
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setName("contentPanel");
				contentPanel.setLayout(new BorderLayout());

				//======== panel2 ========
				{
					panel2.setBorder(new TitledBorder("Liste des fichiers \u00e0 injecter"));
					panel2.setName("panel2");
					panel2.setLayout(new BorderLayout());

					//======== scrollPane1 ========
					{
						scrollPane1.setName("scrollPane1");

						//---- t_Fichiers ----
						t_Fichiers.setModel(new DefaultTableModel(
							new Object[][] {
								{null, null},
								{false, null},
							},
							new String[] {
								"S\u00e9lection", "Nom des fichiers"
							}
						) {
							Class<?>[] columnTypes = new Class<?>[] {
								Boolean.class, String.class
							};
							boolean[] columnEditable = new boolean[] {
								true, false
							};
							@Override
							public Class<?> getColumnClass(int columnIndex) {
								return columnTypes[columnIndex];
							}
							@Override
							public boolean isCellEditable(int rowIndex, int columnIndex) {
								return columnEditable[columnIndex];
							}
						});
						{
							TableColumnModel cm = t_Fichiers.getColumnModel();
							cm.getColumn(0).setMaxWidth(80);
						}
						t_Fichiers.setName("t_Fichiers");
						scrollPane1.setViewportView(t_Fichiers);
					}
					panel2.add(scrollPane1, BorderLayout.CENTER);
				}
				contentPanel.add(panel2, BorderLayout.CENTER);
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);

		//======== panel4 ========
		{
			panel4.setName("panel4");
			panel4.setLayout(new BorderLayout());

			//======== panel1 ========
			{
				panel1.setBorder(new TitledBorder("Dossier des fichiers \u00e0 injecter"));
				panel1.setMinimumSize(new Dimension(455, 75));
				panel1.setPreferredSize(new Dimension(473, 75));
				panel1.setMaximumSize(new Dimension(455, 75));
				panel1.setName("panel1");
				panel1.setLayout(new TableLayout(new double[][] {
					{TableLayout.FILL, TableLayout.PREFERRED},
					{TableLayout.PREFERRED}}));
				((TableLayout)panel1.getLayout()).setHGap(5);
				((TableLayout)panel1.getLayout()).setVGap(5);

				//---- l_DossierSource ----
				l_DossierSource.setBorder(new BevelBorder(BevelBorder.LOWERED));
				l_DossierSource.setText(" ");
				l_DossierSource.setName("l_DossierSource");
				panel1.add(l_DossierSource, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

				//---- bt_Parcourir ----
				bt_Parcourir.setText("Parcourir");
				bt_Parcourir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				bt_Parcourir.setToolTipText("Permet de s\u00e9lectionner  le dossier qui contient les fichiers xml");
				bt_Parcourir.setName("bt_Parcourir");
				bt_Parcourir.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						bt_ParcourirActionPerformed(e);
					}
				});
				panel1.add(bt_Parcourir, new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
			}
			panel4.add(panel1, BorderLayout.SOUTH);

			//======== panel3 ========
			{
				panel3.setBorder(new TitledBorder("Identifiants"));
				panel3.setName("panel3");
				panel3.setLayout(new TableLayout(new double[][] {
					{110, 151, 86, 88, TableLayout.FILL},
					{TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}}));
				((TableLayout)panel3.getLayout()).setHGap(5);
				((TableLayout)panel3.getLayout()).setVGap(5);

				//---- l_AS400 ----
				l_AS400.setText("Adresse serveur");
				l_AS400.setName("l_AS400");
				panel3.add(l_AS400, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));

				//---- tf_AS400 ----
				tf_AS400.setToolTipText("Adresse IP de l'AS/400");
				tf_AS400.setName("tf_AS400");
				panel3.add(tf_AS400, new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

				//---- l_User ----
				l_User.setText("Utilisateur");
				l_User.setName("l_User");
				panel3.add(l_User, new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));

				//---- tf_User ----
				tf_User.setToolTipText("Utilisateur AS/400");
				tf_User.setName("tf_User");
				panel3.add(tf_User, new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

				//---- l_CurLib ----
				l_CurLib.setText("Biblioth\u00e8que");
				l_CurLib.setName("l_CurLib");
				panel3.add(l_CurLib, new TableLayoutConstraints(2, 1, 2, 1, TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));

				//---- tf_CurLib ----
				tf_CurLib.setToolTipText("Nom de la bibiloth\u00e8que de travail (ex: FMPRO)");
				tf_CurLib.setName("tf_CurLib");
				panel3.add(tf_CurLib, new TableLayoutConstraints(3, 1, 3, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

				//---- l_Mdp ----
				l_Mdp.setText("Mot de passe");
				l_Mdp.setName("l_Mdp");
				panel3.add(l_Mdp, new TableLayoutConstraints(0, 2, 0, 2, TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));

				//---- pf_Mdp ----
				pf_Mdp.setToolTipText("Mot de passe AS/400");
				pf_Mdp.setName("pf_Mdp");
				panel3.add(pf_Mdp, new TableLayoutConstraints(1, 2, 1, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

				//---- l_Lettre ----
				l_Lettre.setText("Lettre");
				l_Lettre.setName("l_Lettre");
				panel3.add(l_Lettre, new TableLayoutConstraints(2, 2, 2, 2, TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));

				//---- tf_Lettre ----
				tf_Lettre.setToolTipText("Lettre de l'environnment de S\u00e9rie M (W la plupart du temps)");
				tf_Lettre.setText("W");
				tf_Lettre.setName("tf_Lettre");
				panel3.add(tf_Lettre, new TableLayoutConstraints(3, 2, 3, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
			}
			panel4.add(panel3, BorderLayout.NORTH);
		}
		contentPane.add(panel4, BorderLayout.NORTH);

		//======== panel5 ========
		{
			panel5.setName("panel5");
			panel5.setLayout(new BorderLayout());

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setName("buttonBar");
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 0, 85, 80};
				((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 1.0, 0.0, 0.0};

				//---- okButton ----
				okButton.setText("Injecter");
				okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				okButton.setEnabled(false);
				okButton.setToolTipText("Lance le traitement  sur les fichiers xml s\u00e9lectionn\u00e9s dans la liste");
				okButton.setName("okButton");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						okButtonActionPerformed(e);
					}
				});
				buttonBar.add(okButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Fermer");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.setName("cancelButton");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						cancelButtonActionPerformed(e);
					}
				});
				buttonBar.add(cancelButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			panel5.add(buttonBar, BorderLayout.SOUTH);

			//---- pbar_Traitement ----
			pbar_Traitement.setName("pbar_Traitement");
			panel5.add(pbar_Traitement, BorderLayout.NORTH);
		}
		contentPane.add(panel5, BorderLayout.SOUTH);

		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JPanel panel2;
	private JScrollPane scrollPane1;
	private JTable t_Fichiers;
	private JPanel panel4;
	private JPanel panel1;
	private JLabel l_DossierSource;
	private JButton bt_Parcourir;
	private JPanel panel3;
	private JLabel l_AS400;
	private JTextField tf_AS400;
	private JLabel l_User;
	private JTextField tf_User;
	private JLabel l_CurLib;
	private JTextField tf_CurLib;
	private JLabel l_Mdp;
	private JPasswordField pf_Mdp;
	private JLabel l_Lettre;
	private JTextField tf_Lettre;
	private JPanel panel5;
	private JPanel buttonBar;
	private JButton okButton;
	private JButton cancelButton;
	private JProgressBar pbar_Traitement;
	// JFormDesigner - End of variables declaration  //GEN-END:variables

	//-->
	//--> Programme de lancement
	//-->
	
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable(){
			public void run()
			{
				new InjectionXml();
		    }
        });
	}

}
