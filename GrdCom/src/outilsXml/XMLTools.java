//=================================================================================================
//==>                                                                       01/04/2010 - 03/06/2010
//==> Encodage/décodage des objets en XML
//==> A faire:
//=================================================================================================
package outilsXml;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class XMLTools
{

	/**
	 * Sérialisation d'un objet dans un fichier
	 * @param object objet a serialiser
	 * @param filename chemin du fichier
	 */
	public static void encodeToFile(Object object, String fileName) throws FileNotFoundException, IOException
	{
		// Ouverture de l'encodeur vers le fichier
		XMLEncoder encoder = new XMLEncoder(new FileOutputStream(fileName));
		try
		{
			// Sérialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		}
		finally
		{
			// Fermeture de l'encodeur
			encoder.close();
		}
	}
	
	/**
	 * Désérialisation d'un objet dans un fichier
	 * @param filename chemin du fichier
	 */
	public static Object decodeFromFile(String fileName) throws FileNotFoundException, IOException
	{
	    Object object = null;
	    
	    // Ouverture de décodeur
	    XMLDecoder decoder = new XMLDecoder(new FileInputStream(fileName));
	    try
	    {
	        // Désérialisation de l'objet
	        object = decoder.readObject();
	    }
	    finally
	    {
	        // Fermeture du decodeur
	        decoder.close();
	    }
	    return object;
	}

	/**
	 * Sérialisation d'un objet dans un StringBuffer
	 * @param object objet a sérialiser
	 */
	public static StringBuffer encodeToStringBuffer(Object object) throws FileNotFoundException, IOException
	{
		return new StringBuffer(encodeToString(object));
		/* Alternative qui fonctionne mais soucis si les codepages sont différents AS400->Linux pas exemple
		final StringBuffer sb = new StringBuffer();
		
		// Ouverture de l'encodeur vers le fichier
		XMLEncoder encoder = new XMLEncoder(new OutputStream() {
			public void write(int b) throws IOException
			{
				sb.append((char)b);
			}
		});
		try
		{
			// Sérialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		}
		finally
		{
			// Fermeture de l'encodeur
			encoder.close();
		}
		return sb;
		*/
	}
	
	/**
	 * Désérialisation d'un objet dans un StringBuffer
	 * @param sb StringBuffer
	 */
	public static Object decodeFromStringBuffer(final StringBuffer sb) throws FileNotFoundException, IOException
	{
		return decodeFromString(sb.toString());
	    /* Alternative qui fonctionne mais soucis si les codepages sont différents AS400->Linux pas exemple
	    Object object = null;
	 
	    // ouverture de decodeur
	    XMLDecoder decoder = new XMLDecoder(new InputStream() {
	    	private int position = 0;
			public int read() throws IOException
			{
				if (position < sb.length())
					return sb.charAt(position++);
				else
					return -1;
			}
		});
	    try
	    {
	        // deserialisation de l'objet
	        object = decoder.readObject();
	    }
	    finally
	    {
	        // fermeture du decodeur
	        decoder.close();
	    }
	    return object;
	    */
	}

	/**
	 * Sérialisation d'un objet dans un String
	 * @param object objet a sérialiser
	 */
	public static String encodeToString(Object object) throws FileNotFoundException, IOException
	{
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(baos);

		// Ouverture de l'encodeur vers le flux
		try
		{
			// Sérialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		}
		finally
		{
			// Fermeture de l'encodeur
			encoder.close();
		}
		return baos.toString("UTF-8");
	}

	/**
	 * Désérialisation d'un objet dans un String
	 * @param s String
	 */
	public static Object decodeFromString(final String s) throws FileNotFoundException, IOException
	{
	    Object object = null;
	    
	    // ouverture de decodeur
	    XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(s.getBytes("UTF-8")));
	    try
	    {
	        // désérialisation de l'objet
	        object = decoder.readObject();
	    }
	    finally
	    {
	        // fermeture du decodeur
	        decoder.close();
	    }
	    return object;
	}

}
