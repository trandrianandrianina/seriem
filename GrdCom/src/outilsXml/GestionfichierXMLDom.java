package outilsXml;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class GestionfichierXMLDom
{
	/**
	 * Lecture du fichier XML et on retourne le document lu
	 * @param nomfichier
	 * @return
	 */
	public Document getRacine(File nomfichier)
	{
		if (nomfichier == null) return null;
			
		//On cr�e une instance de SAXBuilder
		SAXBuilder sxb = new SAXBuilder();
		try
		{
			//On cr�e un nouveau document JDOM avec en argument le fichier XML
			//Le parsing est termin� ;)
			return sxb.build(nomfichier);
		}
		catch(Exception e){}
		
		return null;
	}

	/**
	 * Lecture du fichier XML et on retourne le document lu
	 * @param nomfichier
	 * @return
	 */
	public Document getRacine(String nomfichier)
	{
		if (nomfichier == null) return null;
		return getRacine(new File(nomfichier));
	}
	
	/**
	 * Retourne l'�l�ment pour un tag en particulier (parcourt simple � un niveau)
	 * @param tag
	 * @return
	 */
	public Element getTag(Element parent, String tag)
	{
		if ((parent == null) || (tag == null)) return null;
		tag = tag.trim();

		// On se positionne dans la racine du doc
		List listeEnfants = parent.getChildren();

		//On cr�e un Iterator sur notre liste
		Iterator i = listeEnfants.iterator();
		while(i.hasNext())
		{
			//On recr�e l'Element courant � chaque tour de boucle
			Element courant = (Element)i.next();
			if (courant.getName().equals(tag))
				return courant;
		}
		
		return null;
	}

	/**
	 * Recherche r�cursive d'un tag dans l'arboresence 
	 * @param parent
	 * @param tag
	 * @return
	 */
	public static void getTagR(Element parent, String tag, ArrayList listeElement)
	{
		if (!listeElement.isEmpty()) return;
		
		if ((parent == null) || (tag == null)) return;
		tag = tag.trim();
		
		//On cr�� un Iterator sur notre liste
		Iterator i = parent.getChildren().iterator();
		while(i.hasNext()) 
		{
			//On recr�e l'�l�ment courant � chaque tour de boucle
			Element courant = (Element)i.next();
//System.out.println("--> " + courant.getName() + "|" + tag);			
			if (courant.getName().equals(tag))
			{
				listeElement.add(courant);
				return;
			}
			else
				getTagR(courant, tag, listeElement);
		}
	}

	/**
	 * Recherche r�cursive d'un tag dans l'arboresence 
	 * @param parent
	 * @param tag
	 * @return
	 */
	public static void getTagRAll(Element parent, String tag, ArrayList listeElement)
	{
		if ((parent == null) || (tag == null)) return;
		tag = tag.trim();
		
		//On cr�� un Iterator sur notre liste
		Iterator i = parent.getChildren().iterator();
		while(i.hasNext()) 
		{
			//On recr�e l'�l�ment courant � chaque tour de boucle
			Element courant = (Element)i.next();
//System.out.println("--> " + courant.getName() + "|" + tag);			
			if (courant.getName().equals(tag))
				listeElement.add(courant);
			else
				getTagRAll(courant, tag, listeElement);
		}
	}

	/**
	 * Retourne les donn�es pour un �l�ment
	 * @param elt
	 * @return
	 */
	public List getDonnee(Element elt)
	{
		if (elt == null) return null;
		
		return elt.getChildren();
	}
}
