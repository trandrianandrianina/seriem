//=================================================================================================
//==>                                                                       17/09/2010 - 04/04/2013
//==>  ATTENTION il faut penser aussi � modifier le RPGLE SGVMIVSP dans WS001522
//==> 				pour l'ETB et le num�ro client
//==>			c'est la classe PreparationFichier qui s'occupe de lancer le prg RPG
//=================================================================================================


package analyse;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.jdom.Document;
import org.jdom.Element;

import outilsXml.GestionfichierXMLDom;
import as400.RequeteSQLDB2;

public class AnalyseFichierFT
{
	// Constantes
	private static final int NOMAX=9999;
	private static final String ETB="CCF";
	
	// Variables
	private Document doc=null;
	private GestionfichierXMLDom gf=null;
	private HashMap<String, String> dataEntete=new HashMap<String, String>();
	private ArrayList<HashMap<String, String>> dataLigne=new ArrayList<HashMap<String, String>>();
	private boolean commandeDLR=false;
	
	/**
	 * Constructeur
	 * @param nomfichier
	 */
	public AnalyseFichierFT(String nomfichier)
	{
		this(new File(nomfichier));
	}

	/**
	 * Constructeur
	 * @param nomfichier
	 */
	public AnalyseFichierFT(File nomfichier)
	{
		if (nomfichier == null) return;
		gf = new GestionfichierXMLDom();
		doc = gf.getRacine(nomfichier.getAbsoluteFile());
	}

	/**
	 * Traitement
	 * @return
	 */
	public boolean traitement(RequeteSQLDB2 db2, char lettre, String curlib)
	{
		if (doc == null) return false;
		
		// On r�cup�re les donn�es dans le fichier XML
		commandeDLR = isCommandeDLR();
		traitementEntete();
		traitementLignes(db2, lettre, curlib);
		
		// On compare les lignes afin de regrouper les lignes qui ont les m�me adresses
		HashMap<String, String> regroupeLignes = new HashMap<String, String>();
		int cptgroupe=0;
		for (int i=0; i< dataLigne.size(); i++)
		{
			String nomgroupe = dataLigne.get(i).get("BVRBV"); // + dataLigne.get(i).get("BENOML") + dataLigne.get(i).get("BERUEL") + dataLigne.get(i).get("BELOCL")+ dataLigne.get(i).get("BEVILL") + dataLigne.get(i).get("BECDPL") + dataLigne.get(i).get("BEPAYL");
//System.out.println("--> " + nomgroupe);			
			String groupe = regroupeLignes.get(nomgroupe);
			if (groupe == null)
			{
//System.out.println("	--> avantages : " + dataLigne.get(i).get("BVRBV"));				
				groupe = "" + cptgroupe++;
				regroupeLignes.put(nomgroupe, groupe);
			}
			dataLigne.get(i).put("groupe", groupe);
		}

		
		return true;
	}
	
	/**
	 * Si c'est pas une commande DLR alors c'est une commande EDEAL
	 */
	private boolean isCommandeDLR()
	{
		ArrayList listeElement=new ArrayList();
		GestionfichierXMLDom.getTagR(doc.getRootElement(), "CommandeEdeal", listeElement);
		
		return listeElement.size() == 0; 
	}
	
	/**
	 * Traitement Entete
	 * @return
	 */
	private boolean traitementEntete()
	{
		ArrayList listeElement=new ArrayList();
		Element element=null;
		
		// Recherche des zones
		// - BXZP1
		dataEntete.put("BXZP1", recupTag("NumeroCommande", doc.getRootElement(), listeElement, NOMAX));
		//dataEntete.put("BERCC", dataEntete.get("BXZP1"));
		
		// - BEDCO
		String date = recupTagDate("DateCommande", doc.getRootElement(), listeElement);
		dataEntete.put("BEDCO", date);
		if (date.trim().length() > 0)
			dataEntete.put("BEDAT", date.substring(1));
		else
			dataEntete.put("BEDAT", date);

		// - BEDEV
		dataEntete.put("BEDEV", recupTag("Devise", doc.getRootElement(), listeElement, NOMAX));
		
		// - BEMEX
		dataEntete.put("BEMEX", recupTag("CodeTransport", doc.getRootElement(), listeElement, NOMAX));

		// - BXZP1 & BXZP2
		listeElement.clear();
		element = recupElement("Acheteur", doc.getRootElement(), listeElement);
		if (element != null)
		{
			dataEntete.put("siret", recupTag("Siret", element, listeElement, NOMAX));
			dataEntete.put("BXZP2", recupTag("NomAcheteur", element, listeElement, NOMAX));
			dataEntete.put("BXZP3", recupTag("Telephone", element, listeElement, NOMAX));
		}
		else
		{
			dataEntete.put("siret", "");
			dataEntete.put("BXZP2", "");
			dataEntete.put("BXZP3", "");
		}
		
		// - BXZP4
		dataEntete.put("BXZP4", recupTag("NumeroContrat", doc.getRootElement(), listeElement, NOMAX));
		
		return true;
	}

	/**
	 * Traitement des Lignes
	 * @return
	 */
	private boolean traitementLignes(RequeteSQLDB2 db2, char lettre, String curlib)
	{
		ArrayList<Element> listelignes=new ArrayList<Element>();
		
		// Recherche des lignes
		GestionfichierXMLDom.getTagRAll(doc.getRootElement(), "Lignes", listelignes);
//System.out.println("--> " + listelignes.size());
		for (int i=0; i<listelignes.size(); i++)
			traitementLigne(listelignes.get(i), db2, lettre, curlib);
		
		return true;
	}
	
	/**
	 * Traitement d'une ligne
	 * @param ligne
	 * @return
	 */
	private boolean traitementLigne(Element ligne, RequeteSQLDB2 db2, char lettre, String curlib)
	{
		if (ligne == null) return false;

		String chaine=null;
		HashMap<String, String> dLigne=new HashMap<String, String>();
		dataLigne.add(dLigne);
		
		ArrayList<Element> listeElement=new ArrayList<Element>();
		Element element=null, element1=null;

		// - BENOML & BEPAYL, ... adresse de livraison
		listeElement.clear();
		element = recupElement("AdresseLivraison", ligne, listeElement);
		if (element != null)
		{
			listeElement.clear();
			GestionfichierXMLDom.getTagRAll(element, "Nom", listeElement);
			for (int i=0; i<listeElement.size(); i++)
			{
				element1 = (Element)listeElement.get(i);
				switch(i)
				{
					case 0 : 	if (element1 != null) 
								{
									if (element1.getText().length() > 30)
										dLigne.put("BENOML", element1.getText().substring(0, 30));
									else
										dLigne.put("BENOML", element1.getText());
								}
								else dLigne.put("BENOML", "");
					
					case 1 : 	if (element1 != null)
								{
									if (element1.getText().length() > 30)
										dLigne.put("BECPLL", element1.getText().substring(0, 30));
									else
										dLigne.put("BECPLL", element1.getText());
								}
								else dLigne.put("BECPLL", "");
					
					case 2 : 	if (element1 != null)
								{
									if (element1.getText().length() > 30)
										dLigne.put("BELOCL", element1.getText().substring(0, 30));
									else
										dLigne.put("BELOCL", element1.getText());
								}
								else dLigne.put("BELOCL", "");
				}
					
			}
			dLigne.put("BERUEL", recupTag("Rue", ligne, listeElement, 30));
			dLigne.put("BEVILL", recupTag("Ville", ligne, listeElement, 24));
			dLigne.put("BECDPL", recupTag("CodePostal", ligne, listeElement, NOMAX));
			dLigne.put("BEPAYL", recupTag("CodePays", ligne, listeElement, 30));
		}

		// - BVRBV
		//dLigne.put("BVRBV", recupTag("CodeAdresseLivraison", ligne, listeElement, NOMAX));
		dLigne.put("BVRBV", recupTag("ReferenceCommandeClient", ligne, listeElement, NOMAX).substring(2));

		// - BLCPL / BLNLI 
		StringBuffer num = new StringBuffer(recupTag("NumeroPosteCommande", ligne, listeElement, NOMAX));
		for (int l=num.length(); l < 4; l++)
			num.insert(0, '0');
		dLigne.put("BLCPL", num.toString());
		dLigne.put("BLNLI", num.toString());

		// - BLART
		if (commandeDLR) // Commande DLR
			dLigne.put("BLART", getCodeArticleGencode(db2, lettre, curlib, recupTag("ReferenceArticleAcheteur", ligne, listeElement, NOMAX), ETB));
		else // commande EDEAL
			dLigne.put("BLART", getCodeArticleLib4(db2, lettre, curlib, recupTag("ReferenceArticleVendeur", ligne, listeElement, NOMAX), ETB));
		
		// - A1UNV pour le BLQTE (non utilis� directement)
		dLigne.put("A1UNV", getA1UNV(db2, lettre, curlib, dLigne.get("BLART"), ETB));
		
		// - BLQTE
		dLigne.put("BLQTE", recupTag("Quantite", ligne, listeElement, NOMAX));

		// - BLLDP
		chaine = recupTagDate("DateLivraison", ligne, listeElement).trim();
		if (chaine.length() > 0)
			dLigne.put("BLLDLP", "-" + chaine);  // N�gatif pour injection DLS (voir Marc)
		else
			dLigne.put("BLLDLP", "");

		// - BLPRX
		dLigne.put("BLPRX", recupTag("PrixUnitaireNet", ligne, listeElement, NOMAX));
		
		if (commandeDLR)
		{
			// - BLZP1 / BLZP2 / BLZP3
			dLigne.put("BLZP1", "");
			dLigne.put("BLZP2", "");
			dLigne.put("BLZP3", "");
			String instruction = recupTag("InstructionLivraison", ligne, listeElement, NOMAX);
			if (instruction.length() > 15)
			{
				dLigne.put("BLZP1", instruction.substring(0, 15));
				if (instruction.length() > 30)
				{
					dLigne.put("BLZP2", instruction.substring(15, 30));
					if (instruction.length() > 45)
						dLigne.put("BLZP3", instruction.substring(30, 45));
					else
						dLigne.put("BLZP3", instruction.substring(30));
				}
				else
					dLigne.put("BLZP2", instruction.substring(15));
			}
			else
				dLigne.put("BLZP1", instruction);

			// - BXZP5
			dLigne.put("BXZP5", recupTag("ReferenceCodeChantier", ligne, listeElement, NOMAX));

			// - BLLIB3
			dLigne.put("BLLIB3", recupTag("LibelleArticleVendeur", ligne, listeElement, 30));

			// BLLIB4 - ReferenceCommandeClient ou Besoin ou Conditions particuli�res 
			dLigne.put("BLLIB4", recupTag("ReferenceCommandeClient", ligne, listeElement, NOMAX));
			
			// BXZP16
			dLigne.put("BXZP16", recupTag("CodeAdresseLivraison", ligne, listeElement, NOMAX));
		}
		
		return true;
	}
	
	public void afficheListe()
	{
		// Entete
//System.out.println("Entete --");
		for (Entry<String, String> currentEntry : dataEntete.entrySet())
			System.out.println(currentEntry.getKey() + " " + currentEntry.getValue());
		// Lignes
//System.out.println("Ligne -- " + dataLigne.size());
		for (int i=0; i< dataLigne.size(); i++)
		{
//System.out.println(" -- " + i);
			for (Entry<String, String> currentEntry : dataLigne.get(i).entrySet())
				System.out.println(currentEntry.getKey() + " " + currentEntry.getValue());
		}

	}
	
	/**
	 * Ins�re les enregistrements dans les PINJBDVXX
	 * @param db2
	 * @param lettre
	 * @param curlib
	 * @return
	 */
	public String miseafichier(RequeteSQLDB2 db2, char lettre, String curlib)
	{
		if (db2 == null) return "Pas de connexion db2";
		String requete=null;
		int r=0;
		
		HashMap<String, String> regroupeLignes = new HashMap<String, String>();
		HashMap<String, String>data=null;
		
		// On recherche le code client
		//String[] codeclient = getCodeClient(db2, lettre, curlib, dataEntete.get("Siret"), "GRD", data.get("BECDPL"));
//System.out.println("-codeclient-> " + codeclient + " " + dataEntete.get("Siret"));		
		//if (codeclient == null) return "Code client introuvable, Siret:" + dataEntete.get("Siret");
//System.out.println("-codeclient-> " + codeclient[0] + " " + codeclient[1]);

		// On traite chaque ligne  
		for (int i=0; i< dataLigne.size(); i++)
		{
			data = dataLigne.get(i);

			// Insertion des Lignes
			requete = "INSERT INTO "+curlib+".PINJBDVDSL (BLETB, BLMAG, BLRBV, BLERL, BLCPL, BLART, BLQTE, BLLDLP, BLPRX, BLDAT, BLLIB3, BLLIB4, BLZP1, BLZP2, BLZP3, BLNLI ) VALUES ('"+ETB+"', '  ', '"+data.get("BVRBV")+"', 'L'," +
			  "'"+getStringTraitee(data.get("BLCPL"))+"', " +
              "'"+getStringTraitee(data.get("BLART"))+"', " +
              ""+getQTE(data.get("BLQTE"), data.get("A1UNV"))+", " +
              "'"+getStringTraitee(data.get("BLLDLP"))+"', " +
              ""+getPRX(data.get("BLPRX"), data.get("A1UNV"))+", " +
  			     dataEntete.get("BEDAT")+", " +
              "'"+getStringTraitee(data.get("BLLIB3"))+"', " +
              "'"+getStringTraitee(data.get("BLLIB4"))+"', " +
              "'"+getStringTraitee(data.get("BLZP1"))+"', " +
              "'"+getStringTraitee(data.get("BLZP2"))+"', " +
              "'"+getStringTraitee(data.get("BLZP3"))+"', " +
                 data.get("BLNLI") +
              " )";
//System.out.println("--> L : " + r +  " " + requete);
			r = db2.requete(requete);
			if (r < 0)
			{
				//System.out.println("--> L : " + r +  " " + requete);
				return requete;
			}
				
			// On traite les lignes ayant la m�me adresse de livraison (dc m�me ent�te)
			String groupe = data.get("groupe");
			if (regroupeLignes.get(groupe) == null)
			regroupeLignes.put(groupe, "1");
			else
			{
				//System.out.println("--> " + groupe);
				continue;
			}
			
			// Insertion des ent�tes
			requete = "INSERT INTO "+curlib+".PINJBDVDSE (BEETB, BEMAG, BERBV, BEERL, BENCLP, BENCLS, BEDCO, BEDAT, BEDEV, BEMEX, BENOML, BECPLL, BERUEL, BELOCL, BECDPL, BEVILL, BEPAYL, BEOPT, BEVDE) VALUES ('"+ETB+"', '  ', '"+data.get("BVRBV")+"', 'E'," +
			  "411291, " +
			  "000, " +
			  "'"+getStringTraitee(dataEntete.get("BEDCO"))+"', " +
					  dataEntete.get("BEDAT")+", " +
              "'"+getStringTraitee(dataEntete.get("BEDEV"))+"', " +
              "'"+getStringTraitee(dataEntete.get("BEMEX"))+"', " +
              "'"+getStringTraitee(data.get("BENOML"))+"', " +
              "'"+getStringTraitee(data.get("BECPLL"))+"', " +
              "'"+getStringTraitee(data.get("BERUEL"))+"', " +
              "'"+getStringTraitee(data.get("BELOCL"))+"', " +
              "'"+getStringTraitee(data.get("BECDPL"))+"', " +
              "'"+getStringTraitee(data.get("BEVILL"))+"', " +
              "'"+getStringTraitee(data.get("BEPAYL"))+"', " +
              "'HOM', " +
              "'VTI'" + // Anciennement 'VF '
              		" )";
//System.out.println("--> E : " + r +  " " + requete);
			r = db2.requete(requete);
			if (r < 0)
			{
				//System.out.println("--> E : " + r +  " " + requete);
				return requete;
			}
			
			// Insertion des Extensions
			requete = "INSERT INTO "+curlib+".PINJBDVDSX (BXETB, BXMAGE, BXRBV, BXERL, BXZP1, BXZP2, BXZP3, BXZP4, BXZP5, BXDAT, BXZP16 ) VALUES ('"+ETB+"', '  ', '"+data.get("BVRBV")+"', 'X'," +
			"'"+getStringTraitee(dataEntete.get("BXZP1"))+"', " +
			"'"+getStringTraitee(dataEntete.get("BXZP2"))+"', " +
			"'"+getStringTraitee(dataEntete.get("BXZP3"))+"', " + 
			"'"+getStringTraitee(dataEntete.get("BXZP4"))+"', " + 
			"'"+getStringTraitee(data.get("BXZP5"))+"', " + 
			dataEntete.get("BEDAT")+ ", " +
			"'" + data.get("BXZP16") + "'" + 
			")";
//System.out.println("--> X : " + r +  " " + requete);
			r = db2.requete(requete);
			if (r < 0)
			{
				//System.out.println("--> X : " + r +  " " + requete);
				return requete;
			}

		}

//System.out.println("--> Fin mise � jour");
		return null;
	}
	
	/**
	 * Recherche le code article � partir du gencode
	 * @param db2
	 * @param lettre
	 * @param curlib
	 * @param ean
	 * @param etb
	 * @return
	 */
	private String getCodeArticleGencode(RequeteSQLDB2 db2, char lettre, String curlib, String ean, String etb)
	{
		if (ean == null) return null;
		ean = ean.trim();
		if (ean.length() > 13)
			ean = ean.substring(1);
		
		String code=null;
		String requete = "Select GCCOD from " + curlib + ".PGVMGCDM where GCETB = '"+etb+"' and GCGCD = " + ean;
		code = db2.firstEnrgSelect(requete, "GCCOD");
//System.out.println("--> requete " + requete);		
//System.out.println("--> Code article via Gencode " + code);		
		if (code == null) return ean;
		
		return code;
	}

	/**
	 * Recherche le code article � partir du code
	 * @param db2
	 * @param lettre
	 * @param curlib
	 * @param ean
	 * @param etb
	 * @return
	 */
	private String getCodeArticleLib4(RequeteSQLDB2 db2, char lettre, String curlib, String lib, String etb)
	{
		if (lib == null) return null;
		lib = lib.trim();
		
		String code=null;
		String requete = "Select A1ART from " + curlib + ".PGVMEAAM where A1ETB = '"+etb+"' and A1LB3 = '" + lib+"'";
		code = db2.firstEnrgSelect(requete, "A1ART");
//System.out.println("--> requete " + requete);		
//System.out.println("--> Code article via Lib4 " + code);
		if (code == null) return lib;
		
		return code;
	}

	/**
	 * Recherche le A1UNV � partir du code article
	 * @param db2
	 * @param lettre
	 * @param curlib
	 * @param art
	 * @param etb
	 * @return
	 */
	private String getA1UNV(RequeteSQLDB2 db2, char lettre, String curlib, String art, String etb)
	{
		if (art == null) return null;
		art = art.trim();
		
		String a1unv=null;
		String requete = "Select A1UNV from " + curlib + ".PGVMARTM where A1ETB = '"+etb+"' and A1ART = '" + art+"'";
		a1unv = db2.firstEnrgSelect(requete, "A1UNV");
//System.out.println("--> requete " + requete);		
//System.out.println("--> Code article via Lib4 " + code);
		if (a1unv == null) return "";
		
		return a1unv;
	}

	/**
	 * REcherche le code client � partir du num�ro siret
	 * @param db2
	 * @param lettre
	 * @param curlib
	 * @param siret
	 * @param etb
	 * @return
	 */
	private String[] getCodeClient(RequeteSQLDB2 db2, char lettre, String curlib, String siret, String etb, String codepostal)
	{
		if (siret == null) return null;
		String requete = "Select CLCLI, CLLIV from " + curlib + ".PGVMCLIM where CLETB = '"+etb+"' and CLSRN='" + siret.substring(0, 9) + "' and CLSRT='" +siret.substring(9)+ "' and CLCDP1 = " + codepostal;
		ArrayList<String> result= db2.traitementSelect(requete);
		if (result.size() < 1) return null;
//System.out.println("> " + result.get(0));		
		return result.get(0).split("\\|");
	}

	private boolean verif_version()
	{
		return true;
	}
	
	/**
	 * Convertie une date AAAA-MM-JJ en 1AAMMJJ
	 * @param date
	 * @return
	 */
	private String convertDate(String date)
	{
		if (date == null) return "";
		String[] liste = date.split("\\-");

		if (liste.length == 3)
			return "1"+liste[0].substring(2)+liste[1]+liste[2];
		else
			return "";
	}
	
	/**
	 * Remplace le carat�re ' par un blanc
	 * @param chaine
	 * @return
	 */
	private String getStringTraitee(String chaine)
	{
		if (chaine == null) return null;
		
		return chaine.replaceAll("\'", " ");
		
	}
	
	private String getQTE(String chaine, String a1unv)
	{
		if (chaine.equals("")) return "0";
		
		if (isUEtoile(a1unv))
			return "" + Double.parseDouble(chaine) / 100;
		else
			return chaine;
	}
	
	private String getPRX(String chaine, String a1unv)
	{
		if (chaine.equals("")) return "0";
		
		if (isUEtoile(a1unv))
			return "" + Double.parseDouble(chaine) * 100;
		else
			return chaine;
	}

	/**
	 * Teste que le 2� caract�re du A1UNV qui doit �tre �gal � *
	 * @param a1unv
	 * @return
	 */
	private boolean isUEtoile(String a1unv)
	{
		if( (a1unv == null) || (a1unv.length() != 2)) return false;
		
		return (a1unv.charAt(1) == '*');
		
	}
	
	/**
	 * R�cup�ration de l'�l�ment
	 * @param nomTag
	 * @param parent
	 * @param listeElement
	 * @return 
	 */
	private Element recupElement(String nomTag, Element parent, ArrayList<Element> listeElement)
	{
		GestionfichierXMLDom.getTagR(parent, nomTag, listeElement);
		if (listeElement.size() > 0)
			return (Element)listeElement.get(0);
		else
			return null;
	}

	/**
	 * R�cup�ration de la valeur d'un Tag
	 * @param nomTag
	 * @param parent
	 * @param listeElement
	 * @param longueur
	 * @return 
	 */
	private String recupTag(String nomTag, Element parent, ArrayList<Element> listeElement, int longueur)
	{
		listeElement.clear();
		Element element = recupElement(nomTag, parent, listeElement);
		if (element != null)
			if (element.getText().length() > longueur)
				return element.getText().substring(0, longueur);
			else
				return element.getText();
		else
			return "";
	}

	/**
	 * R�cup�ration de la valeur d'un Tag de type Date
	 * @param nomTag
	 * @param parent
	 * @param listeElement
	 */
	private String recupTagDate(String nomTag, Element parent, ArrayList<Element> listeElement)
	{
		listeElement.clear();
		GestionfichierXMLDom.getTagR(parent, nomTag, listeElement);
		Element element = recupElement(nomTag, parent, listeElement);
		if (element != null)
			return convertDate(element.getText());
		else
			return "";
	}

}
