/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.client;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libswing.composant.dialogueattente.SwingWorkerAttente;
import ri.seriem.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Mod�le de l'�cran du programme pour tester les webservices.
 */
public class ModeleTesteur extends AbstractModelePanel {
  // Variables
  private String url = null;
  private EnumMethodeHttp methodeRequeteHttp = EnumMethodeHttp.POST;
  private String body = null;
  private String reponse = null;
  private boolean modeDeveloppeur = false;
  
  /**
   * Constructeur
   */
  public ModeleTesteur() {
    super();
  }
  
  // -- M�thodes publiques
  
  /**
   * Initialisation.
   */
  @Override
  public void initialiserDonnees() {
  }
  
  /**
   * Chargement des donn�es de base.
   */
  @Override
  public void chargerDonnees() {
    // Permet d'initialiser les valeurs uniquement en mode d�veloppeur
    if (modeDeveloppeur) {
      url = "http://172.31.1.249:8030/WebShop/ws/article/listeprixht";
      methodeRequeteHttp = EnumMethodeHttp.POST;
      body = "{\"codeEtablissement\":\"001\",\"numeroClient\":102015, \"suffixeClient\":0, \r\n"
          + "\"listeCodeArticle\":[\"LEG081960\", null, \"OMP11000013300\", \"\", \"NEX10269981\", \"NEX10269983\"]}";
    }
  }
  
  /**
   * Efface les variables
   */
  public void effacerVariable() {
    url = null;
    body = null;
    reponse = null;
    rafraichir();
  }
  
  /**
   * Lance la requ�te Http.
   */
  public void lancerRequeteHttp(Object pFenetre) {
    RequeteHttp requeteHttp = new RequeteHttp(url, methodeRequeteHttp, body);
    SwingWorkerAttente.executer(pFenetre, requeteHttp, "Appel du Webservice en cours ...");
    reponse = requeteHttp.getReponse();
    rafraichir();
  }
  
  /**
   * Modifier l'Url � lancer.
   */
  public void modifierUrl(String pUrl) {
    pUrl = Constantes.normerTexte(pUrl);
    // Contr�le que l'url a bien chang�
    if (Constantes.equals(pUrl, url)) {
      return;
    }
    url = pUrl;
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la m�thode de la requ�te Http.
   */
  public void modifierMethodeRequeteHttp(EnumMethodeHttp pMethodeHttp) {
    // Contr�le que la m�thode a bien chang�
    if (Constantes.equals(pMethodeHttp, methodeRequeteHttp)) {
      return;
    }
    methodeRequeteHttp = pMethodeHttp;
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le body.
   */
  public void modifierBody(String pBody) {
    pBody = Constantes.normerTexte(pBody);
    // Contr�le que le body a bien chang�
    if (Constantes.equals(pBody, body)) {
      return;
    }
    body = pBody;
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Retourne si la r�ponse est au format Html.
   */
  public boolean isReponseHtml() {
    return RequeteHttp.isReponseHtml(reponse);
  }
  
  // -- M�thodes priv�es
  
  // -- Accesseurs
  
  /**
   * Retourne l'url de la requ�te Http.
   */
  public String getUrl() {
    return url;
  }
  
  /**
   * Retourne la m�thode de la requ�te Http.
   */
  public EnumMethodeHttp getMethodeRequeteHttp() {
    return methodeRequeteHttp;
  }
  
  /**
   * Retourne le body pour la requ�te (param�tre des m�thodes POST).
   */
  public String getBody() {
    return body;
  }
  
  /**
   * Retourne la r�ponse de l'url lanc�e.
   */
  public String getReponse() {
    return reponse;
  }
  
}
