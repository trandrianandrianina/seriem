/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libswing.moteur.mvc.InterfaceSoumission;

/**
 * Cette classe regroupe les m�thodes permettant de lancer des requ�tes Http.
 */
public class RequeteHttp implements InterfaceSoumission {
  // Variables
  private String url = null;
  private EnumMethodeHttp methodeRequeteHttp = null;
  private String body = null;
  private String reponse = null;
  private Client client = ClientBuilder.newClient();
  private Gson gson = new GsonBuilder().setPrettyPrinting().create();
  private JsonParser jsonParser = new JsonParser();
  
  /**
   * Constructeur.
   */
  public RequeteHttp(String pUrl, EnumMethodeHttp pMethodeRequeteHttp, String pBody) {
    url = pUrl;
    methodeRequeteHttp = pMethodeRequeteHttp;
    body = pBody;
  }
  
  // -- M�thodes publiques
  
  @Override
  public void executer() {
    if (methodeRequeteHttp == null) {
      throw new MessageErreurException("Vous devez s�lectionner une m�thode Get ou Post avant de lancer la requ�te.");
    }
    
    switch (methodeRequeteHttp) {
      case GET:
        lancerRequeteGet();
        break;
      case POST:
        lancerRequetePost();
        break;
    }
  }
  
  /**
   * Retourne si la r�ponse est au format Html.
   */
  public static boolean isReponseHtml(String pReponse) {
    if (Constantes.normerTexte(pReponse).isEmpty()) {
      return false;
    }
    if (pReponse.toLowerCase().contains("<html>") && pReponse.toLowerCase().contains("</html>")) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si la r�ponse est au format Json.
   */
  public static boolean isReponseJson(String pReponse) {
    if (Constantes.normerTexte(pReponse).isEmpty()) {
      return false;
    }
    if (pReponse.toLowerCase().contains("{") && pReponse.toLowerCase().contains("}")) {
      return true;
    }
    return false;
  }
  
  // -- M�thodes priv�es
  
  /**
   * Lance une requ�te Http m�thode Get.
   */
  private void lancerRequeteGet() {
    try {
      // Contr�le de l'Url
      controlerUrl();
      
      // Lancement de la requ�te Get
      WebTarget webTarget = client.target(url);
      Invocation.Builder invocationBuilder = webTarget.request();
      Response response = invocationBuilder.get();
      
      // R�cup�ration de la r�ponse
      reponse = response.readEntity(String.class);
      formaterReponse();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors du traitement de la requ�te Get.");
    }
  }
  
  /**
   * Lance une requ�te Http m�thode Post.
   */
  private void lancerRequetePost() {
    try {
      // Contr�le de l'Url
      controlerUrl();
      
      // Lancement de la requ�te Post
      WebTarget webTarget = client.target(url);
      Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
      Response response = invocationBuilder.post(Entity.json(body));
      
      // R�cup�ration de la r�ponse
      reponse = response.readEntity(String.class);
      formaterReponse();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors du traitement de la requ�te Post.");
    }
  }
  
  /**
   * Contr�le l'Url.
   */
  private void controlerUrl() {
    if (url == null || !url.trim().toLowerCase().startsWith("http")) {
      throw new MessageErreurException("L'url n'est pas valide");
    }
  }
  
  /**
   * Formate la reponse en fonction de son type.
   */
  private void formaterReponse() {
    // R�ponse au format Html
    if (isReponseHtml(reponse)) {
      return;
    }
    
    // Reponse au format Json
    if (isReponseJson(reponse)) {
      JsonElement jsonElement = jsonParser.parse(reponse);
      reponse = gson.toJson(jsonElement);
    }
  }
  
  // -- Accesseurs
  
  public String getReponse() {
    return reponse;
  }
}
