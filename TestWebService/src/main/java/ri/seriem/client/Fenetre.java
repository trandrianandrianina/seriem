/*
 * Created by JFormDesigner on Thu Nov 18 15:53:01 CET 2021
 */

package ri.seriem.client;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import ri.seriem.libcommun.outils.Constantes;

/**
 * Classe d�crivant la fen�tre principale du logicel.
 */
public class Fenetre extends JFrame {
  
  /**
   * Constructeur.
   */
  public Fenetre() {
    // Mise en place du Look&Feel
    Constantes.forcerLookAndFeelNimbus();
    
    // Affichage de la fen�tre pricipale
    initComponents();
    setSize(1280, 700);
    setLocationByPlatform(true);
    setVisible(true);
    
    // Affichage des panels
    ModeleTesteur modele = new ModeleTesteur();
    VueTesteur vue = new VueTesteur(modele, this);
    add(vue, BorderLayout.CENTER);
    
    vue.afficher();
  }
  
  /**
   * Initialisation des composants servant � construire la fen�tre.
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setTitle("Logiciel pour tester les Webservices Restful");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    pack();
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
