/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.client;

/**
 * Lanceur du logiciel de test des Webservices.
 */
public class Testeur {
  
  public static void main(String[] args) {
    new Fenetre();
  }
  
}
