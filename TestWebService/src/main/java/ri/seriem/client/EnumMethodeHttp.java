/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.client;

import ri.seriem.libcommun.outils.MessageErreurException;

/**
 * Liste des m�thodes http possibles.
 */
public enum EnumMethodeHttp {
  GET(0, "M�thode Get"),
  POST(1, "M�thode Post");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumMethodeHttp(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le num�ro sous lequel la valeur est persist�e en base de donn�es.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libell� associ� au num�ro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le num�ro associ� dans une cha�ne de caract�re.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet �num par son num�ro.
   */
  static public EnumMethodeHttp valueOfByNumero(Integer pNumero) {
    for (EnumMethodeHttp value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("La m�thode de la requ�te http est invalide : " + pNumero);
  }
  
}
