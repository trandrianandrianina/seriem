/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.client;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libswing.composant.dialoguestandard.dialogueerreur.DialogueErreur;
import ri.seriem.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.seriem.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.seriem.libswing.composant.primitif.bouton.EnumBouton;
import ri.seriem.libswing.composant.primitif.bouton.SNBouton;
import ri.seriem.libswing.composant.primitif.label.SNLabelChamp;
import ri.seriem.libswing.composant.primitif.label.SNLabelTitre;
import ri.seriem.libswing.composant.primitif.panel.SNPanel;
import ri.seriem.libswing.composant.primitif.panel.SNPanelContenu;
import ri.seriem.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.seriem.libswing.composant.primitif.saisie.SNBlocTexte;
import ri.seriem.libswing.composant.primitif.saisie.SNTexte;
import ri.seriem.libswing.moteur.SNCharteGraphique;
import ri.seriem.libswing.moteur.graphique.SNBandeauTitre;
import ri.seriem.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Ecran de lancement d'une requ�te Http afin de tester des Webservices.
 */
public class VueTesteur extends AbstractVuePanel<ModeleTesteur> {
  // Constantes
  private static final String BOUTON_LANCER = "Lancer";
  private static final String BOUTON_EFFACER = "Effacer les valeurs";
  
  // Variables
  private JFrame fenetre = null;
  
  /**
   * Constructeur.
   */
  public VueTesteur(ModeleTesteur pModele, JFrame pFenetre) {
    super(pModele);
    fenetre = pFenetre;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    this.setBackground(SNCharteGraphique.COULEUR_FOND);
    this.setOpaque(true);
    
    // Configurer la barre de titre
    pnlBpresentation.setCapitaliserPremiereLettre(false);
    pnlBpresentation.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_EXPLOITATION);
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(BOUTON_EFFACER, 'e', true);
    snBarreBouton.ajouterBouton(BOUTON_LANCER, 'l', false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonBonCour(pSNBouton);
      }
    });
  }
  
  /**
   * Ferme l'application.
   */
  public void fermerApplication() {
    if (fenetre == null) {
      return;
    }
    fenetre.dispose();
  }
  
  /**
   * Rafraichir �cran.
   */
  @Override
  public void rafraichir() {
    rafraichirUrl();
    rafraichirMethodeHttp();
    rafraichirBody();
    rafraichirReponse();
    rafraichirBoutonEffacer();
    rafraichirBoutonLancer();
  }
  
  // -- M�thodes priv�es
  
  /**
   * Rafraichir l'Url.
   */
  private void rafraichirUrl() {
    String url = getModele().getUrl();
    if (url == null) {
      tfUrlALancer.setText("");
    }
    else {
      tfUrlALancer.setText(Constantes.normerTexte(url));
    }
  }
  
  /**
   * Rafraichir la m�thode Http.
   */
  private void rafraichirMethodeHttp() {
    EnumMethodeHttp methodeHttp = getModele().getMethodeRequeteHttp();
    if (methodeHttp == null) {
      rbMethodeGet.setSelected(false);
      rbMethodePost.setSelected(false);
    }
    else {
      switch (methodeHttp) {
        case GET:
          rbMethodeGet.setSelected(true);
          break;
        case POST:
          rbMethodePost.setSelected(true);
          break;
      }
    }
  }
  
  /**
   * Rafraichir le body.
   */
  private void rafraichirBody() {
    String body = getModele().getBody();
    if (body == null) {
      tfBody.setText("");
    }
    else {
      tfBody.setText(Constantes.normerTexte(body));
    }
    // Le composant n'est visible que pour la m�thode POST
    if (Constantes.equals(getModele().getMethodeRequeteHttp(), EnumMethodeHttp.POST)) {
      lbBody.setVisible(true);
      scpBody.setVisible(true);
    }
    else {
      lbBody.setVisible(false);
      scpBody.setVisible(false);
    }
  }
  
  /**
   * Rafraichir la r�ponse.
   */
  private void rafraichirReponse() {
    String reponse = getModele().getReponse();
    epReponse.setEditable(true);
    if (reponse == null) {
      epReponse.setContentType("text/plain");
      epReponse.setText("");
    }
    else {
      // D�finition du type de la chaine � afficher
      if (getModele().isReponseHtml()) {
        epReponse.setContentType("text/html");
      }
      else {
        epReponse.setContentType("text/plain");
      }
      epReponse.setText(Constantes.normerTexte(reponse));
    }
    epReponse.setCaretPosition(0);
    epReponse.setEditable(false);
  }
  
  /**
   * Rafraichir le bouton Effacer.
   */
  private void rafraichirBoutonEffacer() {
    boolean actif = true;
    snBarreBouton.activerBouton(BOUTON_EFFACER, actif);
  }
  
  /**
   * Rafraichir le bouton Lancer.
   */
  private void rafraichirBoutonLancer() {
    boolean actif = true;
    // D�sactiver le bouton si aucune url n'est renseign�e ou si la m�thode de la requ�te n'a pas �t� d�fini
    if (Constantes.normerTexte(tfUrlALancer.getText()).isEmpty() || getModele().getMethodeRequeteHttp() == null) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_LANCER, actif);
  }
  
  /**
   * Gestion des actions sur les boutons.
   */
  private void btTraiterClicBoutonBonCour(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_EFFACER)) {
        getModele().effacerVariable();
      }
      else if (pSNBouton.isBouton(BOUTON_LANCER)) {
        getModele().lancerRequeteHttp(fenetre);
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        fermerApplication();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // -- M�thodes �v�nementielles
  
  /**
   * Traitement lors de la perte de focus sur le champ Url.
   */
  private void tfUrlALancerFocusLost(FocusEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierUrl(tfUrlALancer.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Traitement si la m�thode Get est activ�.
   */
  private void rbMethodeGetItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierMethodeRequeteHttp(EnumMethodeHttp.GET);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Traitement si la m�thode Post est activ�.
   */
  private void rbMethodePostItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierMethodeRequeteHttp(EnumMethodeHttp.POST);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Traitement lors de la perte de focus sur le champ Body.
   */
  private void tfBodyFocusLost(FocusEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierBody(tfBody.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Construit le panel avec ses composants.
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBpresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlRequete = new SNPanel();
    lbUrlALancer = new SNLabelChamp();
    tfUrlALancer = new SNTexte();
    rbMethodeGet = new SNRadioButton();
    rbMethodePost = new SNRadioButton();
    lbBody = new SNLabelChamp();
    scpBody = new JScrollPane();
    tfBody = new SNBlocTexte();
    lblTitreReponse = new SNLabelTitre();
    scpReponse = new JScrollPane();
    epReponse = new JEditorPane();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- pnlBpresentation ----
    pnlBpresentation.setText("Test des WebServices");
    pnlBpresentation.setName("pnlBpresentation");
    add(pnlBpresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
      
      // ======== pnlRequete ========
      {
        pnlRequete.setOpaque(false);
        pnlRequete.setBorder(new TitledBorder(""));
        pnlRequete.setName("pnlRequete");
        pnlRequete.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlRequete.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlRequete.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlRequete.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlRequete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ---- lbUrlALancer ----
        lbUrlALancer.setText("URL \u00e0 lancer");
        lbUrlALancer.setName("lbUrlALancer");
        pnlRequete.add(lbUrlALancer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfUrlALancer ----
        tfUrlALancer.setName("tfUrlALancer");
        tfUrlALancer.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfUrlALancerFocusLost(e);
          }
        });
        pnlRequete.add(tfUrlALancer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbMethodeGet ----
        rbMethodeGet.setText("Get");
        rbMethodeGet.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbMethodeGet.setName("rbMethodeGet");
        rbMethodeGet.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbMethodeGetItemStateChanged(e);
          }
        });
        pnlRequete.add(rbMethodeGet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbMethodePost ----
        rbMethodePost.setText("Post");
        rbMethodePost.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbMethodePost.setName("rbMethodePost");
        rbMethodePost.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbMethodePostItemStateChanged(e);
          }
        });
        pnlRequete.add(rbMethodePost, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbBody ----
        lbBody.setText("Body au format JSON");
        lbBody.setName("lbBody");
        pnlRequete.add(lbBody, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== scpBody ========
        {
          scpBody.setPreferredSize(new Dimension(100, 100));
          scpBody.setMinimumSize(new Dimension(100, 100));
          scpBody.setName("scpBody");
          
          // ---- tfBody ----
          tfBody.setMinimumSize(new Dimension(100, 100));
          tfBody.setPreferredSize(new Dimension(100, 2000));
          tfBody.setName("tfBody");
          tfBody.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfBodyFocusLost(e);
            }
          });
          scpBody.setViewportView(tfBody);
        }
        pnlRequete.add(scpBody, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlRequete,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lblTitreReponse ----
      lblTitreReponse.setText("R\u00e9ponse du Webservice");
      lblTitreReponse.setName("lblTitreReponse");
      pnlContenu.add(lblTitreReponse,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpReponse ========
      {
        scpReponse.setPreferredSize(new Dimension(800, 500));
        scpReponse.setMinimumSize(new Dimension(800, 200));
        scpReponse.setName("scpReponse");
        
        // ---- epReponse ----
        epReponse.setPreferredSize(new Dimension(850, 1000));
        epReponse.setMinimumSize(new Dimension(850, 1000));
        epReponse.setFont(epReponse.getFont().deriveFont(epReponse.getFont().getSize() + 2f));
        epReponse.setName("epReponse");
        scpReponse.setViewportView(epReponse);
      }
      pnlContenu.add(scpReponse,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlContenu.add(snBarreBouton,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbMethodeGet);
    buttonGroup1.add(rbMethodePost);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre pnlBpresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlRequete;
  private SNLabelChamp lbUrlALancer;
  private SNTexte tfUrlALancer;
  private SNRadioButton rbMethodeGet;
  private SNRadioButton rbMethodePost;
  private SNLabelChamp lbBody;
  private JScrollPane scpBody;
  private SNBlocTexte tfBody;
  private SNLabelTitre lblTitreReponse;
  private JScrollPane scpReponse;
  private JEditorPane epReponse;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
