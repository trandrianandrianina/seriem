/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.outil.documentfilter;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Filtre permettant de saisir en majuscule, changer une saisie clavier et de limiter le nombre de caract�res saisis.
 */
public class SaisieDefinition extends DocumentFilter {
  // Constantes
  // Chiffre, +, -, virgule et point
  public static final int NOMBRE_DECIMAL = 0;
  // Que des chiffres
  public static final int NOMBRE_ENTIER = 1;
  // Chiffres et s�parateur (pour num�ros de tiers)
  public static final int NOMBRE_ENTIER_AVEC_SEPARATEUR = 2;
  
  private static final char[] CARACTERE_DECIMAL = { '+', '-', ',', '.' };
  private static final char[] CARACTERE_SEPARATEUR = { '/', '-' };
  
  // Variables
  private boolean isUpperCase = false;
  // Par d�faut valeur max d'un entier (ou presque)
  private int longueur = 0x0FFFFFFF;
  // Liste des caract�res autoris�s
  private char[] listeCaracteresAutorises = null;
  // Liste des caract�res non autoris�s
  private char[] listeCaracteresNonAutorises = null;
  // Hostfield num�rique ou alpha (par d�faut doit �tre alpha car c'est le cas le plus courant)
  // Modifi� le 22/09/2017, la valeur pr�c�dente �tait false
  private boolean isAlpha = true;
  private int typeNumerique = NOMBRE_DECIMAL;
  // Autorise ou pas la tabulation automatique lorsque la longueur max de la zone est atteinte
  // Attention si on autorise la tabulation, il a un probl�me avec le requestfocus si la tabulation est d�clench�e
  // Suite � la SNC-8996 et � une discussion interne, on tente de reproduire le comportement windows et non celui du 5250
  private boolean tabulationAutomatique = false;
  private StringBuilder sb = new StringBuilder();
  private static Robot tab = null;
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur) {
    setNbrMaxCar(alongueur);
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(boolean auppercase) {
    setUpperCase(auppercase);
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur, boolean auppercase) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
  }
  
  /**
   * Constructeur.
   * @param listeA des caract�res autoris�s.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
  }
  
  /**
   * Constructeur.
   * @param listeA des caract�res autoris�s.
   * @param listeNA des caract�res non autoris�s.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, char[] listeNA) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
    listeCaracteresNonAutorises = listeNA;
  }
  
  /**
   * Constructeur.
   * @param listeA des caract�res autoris�s.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, boolean isAlphaNum) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
    isAlpha = isAlphaNum;
  }
  
  /**
   * Constructeur.
   * @param listeA des caract�res autoris�s.
   * @param listeNA des caract�res non autoris�s.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, char[] listeNA, boolean isAlphaNum) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
    listeCaracteresNonAutorises = listeNA;
    isAlpha = isAlphaNum;
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, boolean isAlphaNum, boolean tabulationAuto) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    isAlpha = isAlphaNum;
    tabulationAutomatique = tabulationAuto;
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur, int pTypeNumerique) {
    setNbrMaxCar(alongueur);
    isAlpha = false;
    typeNumerique = pTypeNumerique;
  }
  
  /**
   * Initialise le nombre de caract�res max.
   */
  private void setNbrMaxCar(int alongueur) {
    longueur = alongueur;
  }
  
  /**
   * Initialise si la saisie n'accepte que des majuscules.
   */
  private void setUpperCase(boolean auppercase) {
    isUpperCase = auppercase;
  }
  
  /**
   * V�rifie que le caract�re saisie soit autoris�.
   */
  private boolean isAutorized(String texte) {
    boolean trouve = true;
    char[] lettres = texte.toCharArray();
    
    // Test des caract�res autoris�s
    if (listeCaracteresAutorises != null) {
      for (char lettre : lettres) {
        trouve = false;
        for (int i = listeCaracteresAutorises.length; --i >= 0;) {
          if (lettre == listeCaracteresAutorises[i]) {
            trouve = true;
            break;
          }
        }
        if (!trouve) {
          break;
        }
      }
    }
    else {
      trouve = true;
    }
    
    // Test des caract�res non autoris�s
    if ((listeCaracteresNonAutorises != null) && (trouve)) {
      for (char lettre : lettres) {
        for (int i = listeCaracteresNonAutorises.length; --i >= 0;) {
          if (lettre == listeCaracteresNonAutorises[i]) {
            trouve = false;
            break;
          }
        }
        if (!trouve) {
          break;
        }
      }
    }
    
    return trouve;
  }
  
  /**
   * Teste le contenu d'une zone num�rique.
   */
  private boolean valideNumerique(StringBuilder pChaine) {
    switch (typeNumerique) {
      case NOMBRE_DECIMAL:
        for (char caractereChaine : pChaine.toString().toCharArray()) {
          boolean trouve = false;
          if (!Character.isDigit(caractereChaine)) {
            for (char caractereAutorise : CARACTERE_DECIMAL) {
              if (caractereChaine == caractereAutorise) {
                trouve = true;
                break;
              }
            }
            return trouve;
          }
        }
        break;
      case NOMBRE_ENTIER:
        for (char caractereChaine : pChaine.toString().toCharArray()) {
          if (!Character.isDigit(caractereChaine)) {
            return false;
          }
        }
        break;
      case NOMBRE_ENTIER_AVEC_SEPARATEUR:
        for (char caractereChaine : pChaine.toString().toCharArray()) {
          boolean trouve = false;
          if (!Character.isDigit(caractereChaine)) {
            for (char caractereAutorise : CARACTERE_SEPARATEUR) {
              if (caractereChaine == caractereAutorise) {
                trouve = true;
                break;
              }
            }
            return trouve;
          }
        }
        break;
    }
    return true;
  }
  
  /**
   * En insertion.
   */
  public void insertString(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
    // V�rification du caract�re
    if (!isAutorized(text)) {
      return;
    }
    if (!isAlpha) {
      // On affine le test avec les zones num�riques
      sb.setLength(0);
      sb.append(fb.getDocument().getText(0, fb.getDocument().getLength())).insert(offset, text);
      if (!valideNumerique(sb)) {
        return;
      }
    }
    
    // Insertion
    if ((fb.getDocument().getLength() + text.length() - length) <= longueur) {
      super.insertString(fb, offset, isUpperCase ? getUpperCase(text) : text, attrs);
    }
    else {
      Toolkit.getDefaultToolkit().beep();
      // super.insertString(fb, offset, string.toUpperCase(), attr);
    }
  }
  
  /**
   * En remplacement.
   */
  @Override
  public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
    // V�rification du caract�re
    if (!isAutorized(text)) {
      return;
    }
    // Si le texte est plus long que le champ alors on le tronque
    if (text.length() > longueur) {
      text = text.substring(0, longueur);
    }
    if (!isAlpha) {
      // On affine le test avec les zones num�riques
      sb.setLength(0);
      sb.append(fb.getDocument().getText(0, fb.getDocument().getLength())).replace(offset, offset + length, text);
      if (!valideNumerique(sb)) {
        return;
      }
    }
    
    // Remplacement
    if ((fb.getDocument().getLength() - length) < longueur) {
      // traitement du point rempla�� par la virgule en format num�rique
      if (!isAlpha && text.equals(".")) {
        super.replace(fb, offset, length, ",", attrs);
      }
      // Sinon mise en majuscule
      else {
        super.replace(fb, offset, length, isUpperCase ? getUpperCase(text) : text, attrs);
      }
      
      if (tabulationAutomatique && fb.getDocument().getLength() == longueur) {
        tabulation();
      }
    }
    else {
      Toolkit.getDefaultToolkit().beep();
      // super.replace(fb, offset, length, text.toUpperCase(), attrs);
    }
  }
  
  // -- M�thodes priv�es
  
  /**
   * Traitement pour la transformation d'un chaine en majuscule.
   */
  private String getUpperCase(String chaine) {
    // Traitement sp�cial sur le � qui est transform� en M lors du passage en Majuscule
    int pos = chaine.indexOf('�');
    if (pos == -1) {
      return chaine.toUpperCase();
    }
    else {
      return chaine.replaceAll("�", "�").toUpperCase().replaceAll("�", "�");
    }
  }
  
  /**
   * Simulation de la Tabulation.
   */
  private static void tabulation() {
    try {
      // La tabulation est effectu�e
      if (tab == null) {
        tab = new Robot();
      }
      // La touche ALT-GR est forc�e relev� afin d'�viter de d�clencher le raccourci de Windows ALT-GR + TAB
      // Exemple avec le sympbole @ qui est fait avec ALT-GR + 0 et qui d�clenche un raccourci clavier � cause de ALT-GR + TAB car
      // l'utilisateur n'a pas eu le temps de relacher la toucje ALT-GR. attention sur cetaines JVM (Java 8 � priori) une exception est
      // d�clench� car la JVM est bugg�e d'o� le conseil de mettre � jour la JVM sur le poste de travail
      try {
        tab.keyRelease(KeyEvent.VK_ALT_GRAPH);
      }
      catch (Exception ekey) {
        // Pas de trace afin d'�viter le fichier de log inutilement
      }
      // Attention au Shift sinon on effectue une tabulation arri�re (avec les portables)
      tab.keyPress(KeyEvent.VK_TAB);
      tab.keyRelease(KeyEvent.VK_TAB);
    }
    catch (AWTException e) {
      Toolkit.getDefaultToolkit().beep();
    }
  }
  
  public int getTypeNumerique() {
    return typeNumerique;
  }
  
  public void setTypeNumerique(int typeNumerique) {
    this.typeNumerique = typeNumerique;
  }
  
  public boolean isAlpha() {
    return isAlpha;
  }
  
  public void setAlpha(boolean isAlpha) {
    this.isAlpha = isAlpha;
  }
}
