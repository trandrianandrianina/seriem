/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.outil.bean;

/**
 * Nom des propri�t�s standards rencontr�s sur les composants graphiques JFormDesigner.
 *
 * Cette Enum permet de centraliser et figer le nom des propri�t�s disponibles. Ce nom doit correspondre � la casse pr�t au nom visible
 * dans l'�diteur JFormDesigner. En utilisant cet Enum, on �vite des erreurs de saisie sur le nom des propri�t�s.
 *
 * Si la propri�t� que vous souhaitez modifier n'est pas dans cette liste, il faut l'ajouter.
 */
public enum EnumProprieteBean {
  ACTION("action"),
  ACTION_COMMAND("actionCommand"),
  ALIGNMENT_X("alignmentX"),
  ALIGNMENT_Y("alignmentY"),
  AUTOSCROLLS("autoscrolls"),
  BACKGROUND("background"),
  BORDER("border"),
  BORDER_PAINTED("borderPainted"),
  BOUTON_PRECONFIGURE("boutonPreconfigure"),
  COMPONENT_ORIENTATION("componentOrientation"),
  COMPONENT_POPUP_MENU("componentPopupMenu"),
  CONTENT_AREA_FILLED("contentAreaFilled"),
  CURSOR("cursor"),
  DEFAULT_CAPABLE("defaultCapable"),
  DISABLED_ICON("disabledIcon"),
  DISABLED_SELECTED_ICON("disabledSelectedIcon"),
  DISPLAY_MNEMONIC_INDEX("displayedMnemonicIndex"),
  DOUBLE_BUFFERED("doubleBuffered"),
  ENABLED("enabled"),
  FOCUSABLE("focusable"),
  FOCUS_CYCLE_ROOT("focusCycleRoot"),
  FOCUS_PAINTED("focusPainted"),
  FOCUS_TRAVERSAL_POLICY_PROVIDER("focusTraversalPolicyProvider"),
  FONT("font"),
  FOREGROUND("foreground"),
  HIDE_ACTION_TEXT("hideActionText"),
  HORIZONTAL_ALIGNMENT("horizontalAlignment"),
  HORIZONTAL_TEXT_POSITION("horizontalTextPosition"),
  ICON("icon"),
  ICON_TEXT_GAP("iconTextGap"),
  INHERITS_POUPU_MENU("inheritsPopupMenu"),
  LIBELLE("libelle"),
  MARGIN("margin"),
  MAXIMUM_SIZE("maximumSize"),
  MINIMUM_SIZE("minimumSize"),
  MNEMONIC("mnemonic"),
  MULTI_CLICK_THRESHHOLD("multiClickThreshhold"),
  NAME("name"),
  NEXT_FOCUSABLE_COMPONENT("nextFocusableComponent"),
  OPAQUE("opaque"),
  PREFERRED_SIZE("preferredSize"),
  PRESSED_ICON("pressedIcon"),
  REQUEST_FOCUS_ENABLED("requestFocusEnabled"),
  ROLLOVER_ENABLED("rolloverEnabled"),
  ROLLOVER_ICON("rolloverIcon"),
  ROLLOVER_SELECTED_ICON("rolloverSelectedIcon"),
  SELECTED("selected"),
  SELECTED_ICON("selectedIcon"),
  TEXT("text"),
  TOOLTIP_TEXT("toolTipText"),
  VERIFY_INPUT_WHEN_FOCUS_TAREGT("verifyInputWhenFocusTarget"),
  VERTICAL_ALIGNMENT("verticalAlignment"),
  VERTICAL_TEXT_POSITION("verticalTextPosition"),
  VISIBLE("visible");
  
  private final String nom;
  
  /**
   * Constructeur.
   */
  EnumProprieteBean(String pNom) {
    nom = pNom;
  }
  
  /**
   * Nom de la propri�t�.
   * Ce nom doit correspondre � la casse pr�t au nom visible dans l'�diteur JFormDesigner.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Retourner le nom de la propri�t�.
   */
  @Override
  public String toString() {
    return String.valueOf(nom);
  }
}
