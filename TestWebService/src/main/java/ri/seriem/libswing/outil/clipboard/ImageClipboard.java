/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.outil.clipboard;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Permet d'envoyer une image dans le presse-pied.
 * Cette classe est le pendant de StrignSelection pour une image.
 */
public class ImageClipboard implements Transferable {
  private Image image;
  
  /**
   * Constructeur.
   */
  public ImageClipboard(Image pImage) {
    image = pImage;
  }
  
  /**
   * Retourner les types de donn�es support�s.
   */
  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return new DataFlavor[] { DataFlavor.imageFlavor };
  }
  
  /**
   * Indiquer si le type de donn�es fournis en parma�tre est support� par cette classe.
   * Seules les images sont support�es.
   */
  @Override
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    return DataFlavor.imageFlavor.equals(flavor);
  }
  
  /**
   * Retourner l'objet � transf�rer (l'image).
   */
  @Override
  public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
    if (!DataFlavor.imageFlavor.equals(flavor)) {
      throw new UnsupportedFlavorException(flavor);
    }
    
    return image;
  }
}
