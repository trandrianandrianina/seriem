/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.outil.clipboard;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import ri.seriem.libcommun.outils.Trace;

/**
 * Gestion du clipboard.
 *
 * Cette classe centralise les m�thodes permettant de copier quelquechose dans le presse-papier Windows et de le r�cup�rer. Deux types
 * d'objets sont g�r�s : les cha�nes de caract�res et les images.
 */
public class Clipboard {
  /**
   * Envoyer un texte dans le presse-papier.
   * @param pTexte Le texte � copier dans le presse-papier.
   */
  public static void envoyerTexte(String pTexte) {
    // V�rifier les param�tres
    if (pTexte == null) {
      return;
    }
    
    // Cr�er l'objet qui est envoy� au clipboard
    StringSelection transferable = new StringSelection(pTexte);
    
    // Envoyer l'objet dans le clipboard
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(transferable, null);
  }
  
  /**
   * R�cup�rer un texte � partir du presse-papier.
   * @return Texte r�cup�r� ou null si rien.
   */
  public static String recupererTexte() {
    // R�cup�rer le contenu du clipboard
    Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    if (transferable == null) {
      return null;
    }
    
    // Tester s'il contient du texte
    boolean contientTexte = transferable.isDataFlavorSupported(DataFlavor.stringFlavor);
    if (!contientTexte) {
      return null;
    }
    
    // R�cup�rer le texte
    try {
      return (String) transferable.getTransferData(DataFlavor.stringFlavor);
    }
    catch (UnsupportedFlavorException e) {
      Trace.erreur(e, "Erreur lors de la r�cup�ration d'une cha�ne de caract�res � partir du clipboard");
    }
    catch (IOException e) {
      Trace.erreur(e, "Erreur lors de la r�cup�ration d'une cha�ne de caract�res � partir du clipboard");
    }
    
    return null;
  }
  
  /**
   * Envoyer une image dans le presse papier.
   * @param pImage L'image � copier dans le presse-papier.
   */
  public static void envoyerImage(Image pImage) {
    // V�rifier les param�tres
    if (pImage == null) {
      return;
    }
    
    // Cr�er l'objet qui est envoy� au clipboard
    ImageClipboard transferable = new ImageClipboard(pImage);
    
    // Envoyer l'objet dans le clipboard
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(transferable, null);
  }
  
  /**
   * R�cup�rer une image � partir du presse-papier.
   * @return Image r�cup�r�e ou null si rien.
   */
  public static Image recupererImage() {
    // R�cup�rer le contenu du clipboard
    Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    if (transferable == null) {
      return null;
    }
    
    // Tester s'il contient du texte
    boolean contientImage = transferable.isDataFlavorSupported(DataFlavor.imageFlavor);
    if (!contientImage) {
      return null;
    }
    
    // R�cup�rer l'image
    try {
      return (Image) transferable.getTransferData(DataFlavor.imageFlavor);
    }
    catch (UnsupportedFlavorException e) {
      Trace.erreur(e, "Erreur lors de la r�cup�ration d'une image � partir du clipboard");
    }
    catch (IOException e) {
      Trace.erreur(e, "Erreur lors de la r�cup�ration d'une image � partir du clipboard");
    }
    return null;
  }
}
