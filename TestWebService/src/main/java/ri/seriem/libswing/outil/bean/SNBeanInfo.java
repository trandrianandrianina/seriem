/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.outil.bean;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import java.util.HashMap;
import java.util.Map;

import ri.seriem.libcommun.outils.MessageErreurException;

/**
 * Classe pour faciliter la manipulation des PropertyDescriptor.
 *
 * Toutes les classes SNBeanInfo de nos composants graphiques doivent h�riter de cette classe. Cette classe apporte des facilit�s pour
 * d�velopper les BeanInfo :
 * - elle fournit des m�thodes pour modifier facilement les propri�t�s des Bean (setCategorie, setCategorieAvecNomComposant,
 * setLectureSeule, setVisible, ...).
 * - ces m�thodes attendent un EnumProprieteBean pour conna�tre la propri�t� � modifier, ce qui �vite des erreurs sur les noms.
 * - la m�thode bloquerTout() permet de mettre ne lecture seule toutes les propri�t�s.
 * - la gestion des PropertyDescriptor est effectu�e automatiquement par cette classe (pas besoin de les g�rer).
 * - la m�thode getListePropertyDescriptor() retourne ce qui doit �tre retourn�e par getPropertyDescriptors().
 */
public class SNBeanInfo extends SimpleBeanInfo {
  private static final String ATTRIBUT_CATEGORIE = "category";
  private static final String ATTRIBUT_LECTURE_SEULE = "readOnly";
  
  private Map<EnumProprieteBean, PropertyDescriptor> mapPropertyDescriptor = new HashMap<EnumProprieteBean, PropertyDescriptor>();
  private Class beanClass = null;
  
  /**
   * Retourner la description de la propri�t� (PropertyDescriptor) pour la prori�t� dont le nom est pass� en param�tre.
   * Lors du premier appel, le PropertyDescriptor est cr�� est stock� dans une Map. Lors des appels suivants, c'est le PropertyDescriptor
   * cr�� initialement qui est retourn�.
   */
  private PropertyDescriptor getPropertyDescriptor(EnumProprieteBean pEnumProprieteBean) {
    // Tester les param�tres
    if (beanClass == null) {
      throw new MessageErreurException("La classe du composant graphique n'est pas renseign�e, appeler setBeanClass() pour le faire.");
    }
    if (pEnumProprieteBean == null) {
      throw new MessageErreurException("Le nom de la propri�t� du composant graphique est invalide.");
    }
    
    // Rechercher la propri�t� dans la liste
    PropertyDescriptor propertyDescriptor = mapPropertyDescriptor.get(pEnumProprieteBean);
    
    // Tester si on n'a pas trouv� la propri�t� dans la map
    if (propertyDescriptor == null) {
      // Cr�er la propri�t�
      try {
        propertyDescriptor = new PropertyDescriptor(pEnumProprieteBean.getNom(), beanClass);
      }
      catch (IntrospectionException e) {
        throw new MessageErreurException(e, "Erreur lors de la r�cup�ration de la propri�t� '" + pEnumProprieteBean.getNom()
            + "' du composant graphique " + beanClass.getSimpleName());
      }
      
      // Ajouter la propri�t� � la map
      mapPropertyDescriptor.put(pEnumProprieteBean, propertyDescriptor);
    }
    
    // Retourner la propri�t�
    return propertyDescriptor;
  }
  
  /**
   * Retourner la liste des descriptions de propri�t�s que la m�thode getPropertyDescriptors() doit renvoyer.
   * Cette m�thode doit �tre appel�e par les m�thodes getPropertyDescriptors() des classes enfants.
   */
  @Override
  public PropertyDescriptor[] getPropertyDescriptors() {
    PropertyDescriptor[] liste = new PropertyDescriptor[mapPropertyDescriptor.size()];
    int index = 0;
    for (Map.Entry<EnumProprieteBean, PropertyDescriptor> mapEntry : mapPropertyDescriptor.entrySet()) {
      liste[index++] = mapEntry.getValue();
    }
    return liste;
  }
  
  /**
   * Retourner le BeanInfo de la classe parente s'il y a un.
   */
  @Override
  public BeanInfo[] getAdditionalBeanInfo() {
    try {
      return new BeanInfo[] { Introspector.getBeanInfo(getBeanClass().getSuperclass()) };
    }
    catch (IntrospectionException e) {
      return null;
    }
  }
  
  /**
   * Mettre en lecture seule toutes les propri�tes.
   * Cette m�thode parcourt toutes les propri�t�s disponibles dans EnumProprieteBean et recherche si elles existante dans le JavaBean
   * � modifier. Si la propri�t� est trouv�e, elle est mise en lecture seule. L'id�e est de tout bloquer par d�faut et d'autoriser
   * explictement, composant par composant, les propri�t�s qui sont modifiables.
   */
  public void bloquerTout() {
    for (EnumProprieteBean enumProprieteBean : EnumProprieteBean.values()) {
      // V�rifier que le nom de la propri�t� est viable
      if (enumProprieteBean != null) {
        try {
          // R�cup�rer la propri�t� du JavaBean
          PropertyDescriptor propertyDescriptor = getPropertyDescriptor(enumProprieteBean);
          if (propertyDescriptor != null) {
            // Mettre la propri�t� en lecture seule si on l'a trouv�
            propertyDescriptor.setValue(ATTRIBUT_LECTURE_SEULE, Boolean.TRUE);
          }
        }
        catch (MessageErreurException e) {
          // Ne pas faire d'erreur
        }
      }
    }
  }
  
  /**
   * Affecter une cat�gorie � la propri�t� du composant graphique.
   * La propri�t� est d�sign�e par un EnumProprieteBean.
   */
  public void setCategorie(EnumProprieteBean pEnumBeanProperty, String pCategorie) {
    // Tester les param�tres
    if (beanClass == null) {
      throw new MessageErreurException("La classe du composant graphique n'est pas renseign�e, appeler setBeanClass() pour le faire.");
    }
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propri�t� du composant graphique est invalide.");
    }
    if (pCategorie == null || pCategorie.isEmpty()) {
      throw new MessageErreurException("La cat�gorie est invalide pour le composant graphique : " + beanClass.getSimpleName());
    }
    
    // R�cup�rer la propri�t�
    PropertyDescriptor propertyDescriptor = getPropertyDescriptor(pEnumBeanProperty);
    
    // Renseigner l'attribut lecture seule
    if (propertyDescriptor != null) {
      propertyDescriptor.setValue(ATTRIBUT_CATEGORIE, pCategorie);
    }
  }
  
  /**
   * Affecter une cat�gorie � la propri�t� du composant graphique.
   * La propri�t� est d�sign�e par un EnumProprieteBean.
   * Le nom de la cat�gorie est le nom de la classe du composant graphique. Avec cela par exemple, les propri�t�s de SNBouton son rang�es
   * dans la cat�gorie "SNBouton".
   */
  public void setCategorieAvecNomComposant(EnumProprieteBean pEnumBeanProperty) {
    // Tester les param�tres
    if (beanClass == null) {
      throw new MessageErreurException("La classe du composant graphique n'est pas renseign�e, appeler setBeanClass() pour le faire.");
    }
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propri�t� du composant graphique est invalide.");
    }
    
    // Modifier la cat�gorie de la propri�t�
    setCategorie(pEnumBeanProperty, beanClass.getSimpleName());
  }
  
  /**
   * Mettre une propri�t� en lecture seule.
   * La propri�t� est d�sign�e par un EnumProprieteBean.
   */
  public void setLectureSeule(EnumProprieteBean pEnumBeanProperty, boolean pLectureSeule) {
    // Tester les param�tres
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propri�t� du composant graphique est invalide.");
    }
    
    // R�cup�rer la propri�t�
    PropertyDescriptor propertyDescriptor = getPropertyDescriptor(pEnumBeanProperty);
    
    // Renseigner l'attribut lecture seule
    if (propertyDescriptor != null) {
      propertyDescriptor.setValue(ATTRIBUT_LECTURE_SEULE, pLectureSeule);
    }
  }
  
  /**
   * Cacher une propri�t� du composant graphique.
   * La propri�t� est d�sign�e par un EnumProprieteBean.
   */
  public void setVisible(EnumProprieteBean pEnumBeanProperty, boolean pVisible) {
    // Tester les param�tres
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propri�t� du composant graphique est invalide.");
    }
    
    // R�cup�rer la propri�t�
    PropertyDescriptor propertyDescriptor = getPropertyDescriptor(pEnumBeanProperty);
    
    // Renseigner l'attribut lecture seule
    if (propertyDescriptor != null) {
      propertyDescriptor.setHidden(!pVisible);
    }
  }
  
  /**
   * Obtenir la classe du composant graphique "Bean" dont cette classe est le BeanInfo.
   */
  public Class getBeanClass() {
    return beanClass;
  }
  
  /**
   * Renseigner la classe du composant graphique "Bean" dont cette classe est le BeanInfo.
   */
  public void setBeanClass(Class pBeanClass) {
    beanClass = pBeanClass;
  }
}
