/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc;

/**
 * Interface standard d'une vue �cran.
 *
 * Une vue �cran se charge d'afficher les donn�es du mod�le (c'est le r�les "Vue" du pattern MVC). Elle de modifie pas directement
 * sur les donn�es du mod�le car elle utilise les m�thodes du mod�le impl�mentant les diff�rentes actions possibles sur les donn�es.
 *
 * Lors de l'affichage initial d'un �cran, les m�thodes sont appel�es dans l'ordre suivant :
 * 1- modele.initialiserDonnees()
 * 2- vue.initialiserComposants()
 * 3- vue.rafraichir()
 * 4- modele.chargerDonnees()
 * 5- vue.rafraichir()
 *
 * Des impl�mentations de cette interface sont disponibles : AbstractVueSession, AbstractVueDialogue.
 */
public interface InterfaceVue<M extends InterfaceModele> {
  /**
   * Initialiser les composants graphiques de l'�cran.
   *
   * Les donn�es du mod�le ne sont pas encore charg�es � ce stade. L'objectif est d'afficher la bo�te de dialogue tr�s rapidement afin que
   * l'utilisateur ait une r�ponse visuelle imm�diate apr�s son action.
   *
   * En pratique, cette m�thode appelle la m�thode initCompoments de JFormDesigner. On peut ajouter manuellement des composants ou adapter
   * les propri�t�s des composants existants. C'est notamment l� qu'on enregistre les raccourcis claviers.
   */
  public void initialiserComposants();
  
  /**
   * Rafraichir les donn�es de l'�cran.
   *
   * Cette m�thode est appel�e � chaque fois que les donn�es du mod�le �voluent. L'objectif de cette m�thode est de mettre � jour les
   * donn�es visibles � l'�cran en fonctions des donn�es du mod�le. Les composants graphiques peuvent devenir actifs ou inactifs,
   * �ditables ou non �ditables, visibles ou non visibles.
   *
   * La m�thode rafra�chir() est appel�e deux fois, une premi�re fois apr�s initialisation des donn�es du mod�le (initialiserDonnees) et
   * initialisation des composants graphiques (initialiserComposants), une seconde dois apr�s le chargement des donn�es du mod�le
   * (chargerDonnees).
   */
  public void rafraichir();
  
  /**
   * Afficher la vue.
   */
  public void afficher();
  
  /**
   * Cacher la vue.
   */
  public void cacher();
  
  /**
   * Notifier la vue que les donn�es du mod�le ont chang�es.
   */
  public void notifier();
  
  /**
   * Mod�le associ� � la vue.
   */
  public M getModele();
}
