/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc.panel;

import java.awt.Cursor;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libcommun.outils.Trace;
import ri.seriem.libswing.composant.dialoguestandard.dialogueerreur.DialogueErreur;
import ri.seriem.libswing.moteur.mvc.ChargementDonnees;
import ri.seriem.libswing.moteur.mvc.InterfaceModele;
import ri.seriem.libswing.moteur.mvc.InterfaceVue;

/**
 * Vue associ�e � un composant JPanel.
 *
 * Une vue contient toujours un lien vers le mod�le associ�. C'est ce qui permet de lire les donn�es du mod�le pour rafra�chir l'affichage
 * et qui permet d'appeler les m�thodes �v�nementiels du mod�les lors d'une action graphique de l'utilisateur.
 *
 * La vue peut �tre utilis�e seule, �tre enfant d'un vue parente ou au contraire contenir plusieurs vues enfants. Il est �galement
 * possible que la vue soit � la fois enfant d'une vue parente et contienne plusieurs vues enfants. Dans tous les cas, les m�thodes
 * initialiserComposants() et rafraichir() sont � surcharger normalement.
 *
 * Pour une vue parent contenant une vue liste et une liste d�tail :
 * - Utiliser un CardLayout dans un JPanel de l'�cran.
 * - Ajouter les vues au CardLayout dans la m�thode initialiserComposants() :
 * Exemple : pnlContenu.add(getAbstractVueListe(), EnumModeListeDetail.LISTE.getCode());
 * - G�rer le permutation des vues dans la m�thode Rafraichir() :
 * Exemple : cardLayout.show(pnlContenu, EnumModeListeDetail.LISTE.getCode());
 */
public abstract class AbstractVuePanel<M extends InterfaceModele> extends JPanel implements InterfaceVue<M>, Observer {
  private final M modele;
  private Map<Object, InterfaceVue<?>> listeVueEnfant = new HashMap<Object, InterfaceVue<?>>();
  private InterfaceVue<?> vueEnfantActive = null;
  private boolean protectionReentrance = false;
  private boolean composantsInitialises = false;
  private boolean evenementsActifs = false;
  
  /**
   * Constructeur.
   */
  public AbstractVuePanel(M pModele) {
    super();
    
    // V�rifier que le mod�le n'est pas nul
    modele = pModele;
    if (pModele == null) {
      throw new MessageErreurException("Impossible de cr�er la vue car le mod�le est invalide.");
    }
    
    // Etre notifier lorsque le mod�le est modifi� (pour faire le rafraichissement)
    modele.abonnerVue(this);
  }
  
  // M�thodes de l'interface InterfaceVue
  
  @Override
  public abstract void initialiserComposants();
  
  @Override
  public abstract void rafraichir();
  
  @Override
  public final void afficher() {
    try {
      // Activer la protection contre la r�entrance
      protectionReentrance = true;
      
      // Afficher le curseur d'attente
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      
      // Indiquer au mod�le que les donn�es ne sont pas charg�es.
      modele.setDonneesChargees(false);
      
      // Initialiser les donn�es du mod�le
      // L'id�e est de nettoyer rapidement les donn�es avant le premier rafra�chissement et avant le chargement des donn�es lourdes.
      Trace.debug(modele.getClass(), "initialiserDonnees");
      modele.initialiserDonnees();
      
      // Initialiser les donn�es du mod�le enfant souhait� si celui-ci a chang�
      InterfaceVue<?> vueEnfantSouhaitee = getVueEnfantSouhaitee();
      if (vueEnfantSouhaitee != null && vueEnfantSouhaitee != vueEnfantActive) {
        Trace.debug(vueEnfantSouhaitee.getModele().getClass(), "initialiserDonnees");
        vueEnfantSouhaitee.getModele().initialiserDonnees();
      }
      
      // D�sactiver la gestion des �v�nements (avant initialiserComposants et rafraichir).
      evenementsActifs = false;
      
      // Initialiser les composants graphiques si cela n'a pas �t� effectu�
      if (!composantsInitialises) {
        Trace.debug(getClass(), "initialiserComposants");
        initialiserComposants();
        composantsInitialises = true;
        
        // Nommer la fen�tre du nom de sa classe Vue (pour QFTest)
        // Cette �tape doit �tre effectu�e apr�s initialiserComposants() car JFormDesigner donne le nom "this" au composant.
        setName(getClass().getSimpleName());
      }
      
      // Rafra�chir la vue (les donn�es sont justes initialis�es � ce stade).
      Trace.debug(getClass(), "rafraichir");
      rafraichir();
      
      // R�activer la gestion des �v�nements
      evenementsActifs = true;
      
      // Rafra�chir la vue enfant souhait�e (les composants graphiques sont cr��s et les donn�es sont rafra�chies � vide).
      // Le chargement des donn�es de la vue enfant n'a pas encore �t� fait � ce stade. L'objectif est d'afficher les composants
      // rapidement m�me s'ils sont vides. Les donn�es de la vue enfant seront charg�es plus tard, apr�s la chargement des donn�es de
      // la vue parent, lors du deuxi�me rafra�ssissement de la vue parent.
      if (vueEnfantSouhaitee != null) {
        vueEnfantSouhaitee.notifier();
      }
      
      // D�sactiver la protection contre la r�entrance avant le chargement des donn�es
      protectionReentrance = false;
      
      // Lancer le chargement des donn�es de la vue (dans une thread � part)
      ChargementDonnees chargementDonnees = new ChargementDonnees(modele);
      chargementDonnees.execute();
      
      // Afficher le panel
      setVisible(true);
    }
    catch (Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
      evenementsActifs = true;
      protectionReentrance = false;
    }
  }
  
  @Override
  public final void cacher() {
    setVisible(false);
  }
  
  @Override
  public final void notifier() {
    try {
      // Activer la protection contre la r�entrance
      if (protectionReentrance) {
        throw new MessageErreurException(
            "La m�thode " + getClass().getSimpleName() + ".rafraichir() a �t� appel� en cours de rafra�chissement de l'�cran. "
                + "Cela a provoqu� une r�eentrance. Prot�ger les m�thodes �v�nementielles avec isEvenementsActifs() pour �viter cela.");
      }
      protectionReentrance = true;
      
      // D�sactiver la gestion des �v�nements (avant initialiserComposants et rafraichir).
      evenementsActifs = false;
      
      // Initialiser les composants graphiques si cela n'a pas �t� effectu�
      if (!composantsInitialises) {
        Trace.debug(getClass(), "initialiserComposants");
        initialiserComposants();
        composantsInitialises = true;
        
        // Nommer la fen�tre du nom de sa classe Vue (pour QFTest)
        // Cette �tape doit �tre effectu�e apr�s initialiserComposants() car JFormDesigner donne le nom "this" au composant.
        setName(getClass().getSimpleName());
      }
      
      // Rafra�chir la vue
      Trace.debug(getClass(), "rafraichir");
      rafraichir();
      
      // R�activer la gestion des �v�nements
      evenementsActifs = true;
      
      // Rafra�chir la vue enfant
      InterfaceVue<?> vueEnfantSouhaitee = getVueEnfantSouhaitee();
      if (vueEnfantSouhaitee != null) {
        if (vueEnfantSouhaitee != vueEnfantActive) {
          // Si la vue enfant a chang�e, on appelle la m�thode afficher() qui charge les donn�es en asynchrone et d�clenche leur
          // rafra�chissement lorsque le chargement est termin�
          vueEnfantSouhaitee.afficher();
        }
        else {
          // Si la vue enfant n'a pas chang�e, un simplement rafra�chissement suffit.
          vueEnfantSouhaitee.notifier();
        }
      }
      
      // M�moriser la nouvelle vue active
      vueEnfantActive = vueEnfantSouhaitee;
      
      // Afficher le curseur par d�faut
      setCursor(Cursor.getDefaultCursor());
      
      // D�sactiver la protection contre la r�entrance
      protectionReentrance = false;
    }
    catch (Exception exception) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(exception);
      evenementsActifs = true;
      protectionReentrance = false;
    }
  }
  
  /**
   * Ajouter une vue enfant correspondant � la cl� fournie.
   * Lors de cette association des vues, le mod�le de la vue parent est renseign� dans le mod�le de la vue enfant.
   */
  public final void ajouterVueEnfant(Object pCle, InterfaceVue<?> pVueEnfant) {
    listeVueEnfant.put(pCle, pVueEnfant);
    if (pVueEnfant != null && pVueEnfant.getModele() != null) {
      pVueEnfant.getModele().setInterfaceModeleParent(getModele());
    }
  }
  
  // M�thodes de l'interface Observer
  
  /**
   * Mettre � jour les donn�es de la vue.
   * Cette m�thode est appel�e lorsque les donn�es du mod�le �volue et qu'il notifie ces observateurs.
   */
  @Override
  public final void update(Observable o, Object arg) {
    notifier();
  }
  
  // M�thodes priv�es
  
  /**
   * Fournir la vue enfant qui doit �tre affich�e.
   * Ce n'est peut-�tre pas la m�me que celle qui est r�ellement affich�.
   */
  private InterfaceVue<?> getVueEnfantSouhaitee() {
    if (modele.getCleVueEnfantActive() == null) {
      return null;
    }
    
    return listeVueEnfant.get(modele.getCleVueEnfantActive());
  }
  
  // Accesseurs
  
  /**
   * Mod�le associ� � la vue.
   */
  @Override
  public final M getModele() {
    return modele;
  }
  
  /**
   * Indiquer si les �v�nements doivent �tre trait�s.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }
  
  /**
   * V�rifier si les donn�es du mod�le sont charg�es.
   *
   * Lors de l'affichage initial, la valeur est mise � false avant initialiserDonnees() et mise � true apr�s chargerDonnees().
   * Lors des chargements ult�rieurs de donn�es, la valeur est mise � false avant l'appel � chargerDonnees() et remise � true lorsque
   * chargerDonnees() est termin�.
   */
  public final boolean isDonneesChargees() {
    return modele.isDonneesChargees();
  }
}
