/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc;

import javax.swing.SwingWorker;

import ri.seriem.libcommun.outils.Trace;
import ri.seriem.libswing.composant.dialoguestandard.dialogueerreur.DialogueErreur;

/**
 * Charge les donn�es en dehors de la thread graphique.
 * Les erreurs qui surviennent pendant le chargement des donn�es sont affich�es dans une bo�te de dialogue (dans la thread graphique).
 */
public class ChargementDonnees extends SwingWorker<Exception, Integer> {
  private int tempsChargement = 0;
  private final InterfaceModele modele;
  
  public ChargementDonnees(InterfaceModele pModele) {
    modele = pModele;
  }
  
  @Override
  protected Exception doInBackground() throws Exception {
    // Charger les donn�es
    try {
      Trace.debug(modele.getClass(), "chargerDonnees");
      modele.chargerDonnees();
      
      // Ajouter une attente artificielle � des fins de mise au point (attends TEMPS_CHARGEMENT en millisecondes)
      if (Trace.isModeDebug() && tempsChargement > 0 && tempsChargement < 60000) {
        try {
          Thread.sleep(tempsChargement);
        }
        catch (InterruptedException e) {
          // RAS
        }
      }
      
      return null;
    }
    catch (Exception e) {
      return e;
    }
  }
  
  @Override
  public void done() {
    try {
      // R�cup�ration du r�sultat du chargement des donn�es
      Exception e = get();
      
      // Indiquer au mod�le que les donn�es sont charg�es.
      modele.setDonneesChargees(true);
      
      // Afficher le message d'erreur s'il y en a un
      if (e != null) {
        DialogueErreur.afficher(e);
      }
      
      // Rafraichir l'�cran avec les donn�es
      modele.rafraichir();
    }
    catch (Exception e) {
      // Il y a eu un probl�me pendant le rafra�chissement des donn�es
      DialogueErreur.afficher(e);
    }
  }
}
