/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc.panel;

import java.util.Observable;
import java.util.Observer;

import ri.seriem.libswing.moteur.mvc.InterfaceModele;

/**
 * Mod�le associ� � une vue JPanel.
 *
 * Le mod�le peut �tre utilis� seul, associ� � un mod�le parent ou au contraire en tant que mod�le parent. Il est �galement possible que
 * le mod�le soit � la fois parent et soit affili� � un autre mod�le parent.
 *
 * Le mod�le peut �tre utilis� seul, auquel cas son utilisation est simple. Il suffit de :
 * - Surcharger la m�thode initialiserDonnees() si besoin.
 * - Surcharger la m�thode chargerDonnees().
 *
 * Le mod�le est associ� � un mod�le parent si sa vue correspondante est contenue dans une autre vue. C'est par exemple le cas des mod�les
 * "Liste" et "D�tail" dans un �cran liste/d�tail. C'est aussi le cas des mod�les "Onglet". La particularit� est que ce mod�le va d�l�guer
 * son rafra�chiseement au mod�le parent. Pour utiliser ce mod�le :
 * - D�finir une m�thode getModeleParent() qui retourne abstractModeleParent correctement typ� (cast n�cessaire).
 * - Surcharger la m�thode initialiserDonnees() si besoin.
 * - Surcharger la m�thode chargerDonnees().
 *
 * A l'inverse, le mod�le peut agir en tant que mod�le parent de plusieurs mod�les enfants. Pour utiliser ce mod�le :
 * - Surcharger la m�thode initialiserDonnees() si besoin.
 * - Surcharger la m�thode chargerDonnees().
 * - Utiliser les m�thodes getMode() et setMode() pour g�rer l'affichage des vues enfants dans un �cran de type liste/d�tail.
 * - Utiliser les m�thodes setTitre() et getTitre() pour g�rer l'affichage du titre.
 */
public abstract class AbstractModelePanel extends Observable implements InterfaceModele {
  // Constantes
  public static final String VUE_LISTE = "Liste";
  public static final String VUE_DETAIL = "D�tail";
  
  // Variables
  private InterfaceModele interfaceModeleParent = null;
  private Object cleVueEnfantActive = null;
  private String titreEcran = "";
  private boolean donneesChargees = false;
  
  /**
   * Constructeur.
   */
  public AbstractModelePanel() {
  }
  
  // -- M�thodes de InterfaceModele
  
  @Override
  abstract public void initialiserDonnees();
  
  @Override
  abstract public void chargerDonnees();
  
  @Override
  public final void rafraichir() {
    if (interfaceModeleParent != null) {
      // Le mod�le a un parent, on demande le rafra�chissement du parent
      interfaceModeleParent.rafraichir();
    }
    else {
      // Pas de parent, on demande le rafra�chisement de la vue associ�e au mod�le
      setChanged();
      notifyObservers();
    }
  }
  
  @Override
  public final void abonnerVue(Observer pObserver) {
    addObserver(pObserver);
  }
  
  @Override
  public final InterfaceModele getInterfaceModeleParent() {
    return interfaceModeleParent;
  }
  
  @Override
  public final void setInterfaceModeleParent(InterfaceModele pInterfaceModeleParent) {
    interfaceModeleParent = pInterfaceModeleParent;
  }
  
  @Override
  public final Object getCleVueEnfantActive() {
    return cleVueEnfantActive;
  }
  
  @Override
  public final void setCleVueEnfantActive(Object pCleVueEnfantActive) {
    cleVueEnfantActive = pCleVueEnfantActive;
  }
  
  @Override
  public final boolean isDonneesChargees() {
    return donneesChargees;
  }
  
  @Override
  public final void setDonneesChargees(boolean pDonneesChargees) {
    donneesChargees = pDonneesChargees;
  }
  
  @Override
  public final boolean isModeSortie() {
    return false;
  }
  
  @Override
  public final void setModeSortie(int modeSortie) {
  }
  
  @Override
  public final String getTitreEcran() {
    // Retourner le titre du parent si le mod�le � un parent.
    if (interfaceModeleParent != null) {
      return interfaceModeleParent.getTitreEcran();
    }
    else {
      return titreEcran;
    }
  }
  
  @Override
  public final void setTitreEcran(String pTitreEcran) {
    // Modifier le titre du parent si le mod�le � un parent.
    if (interfaceModeleParent != null) {
      interfaceModeleParent.setTitreEcran(pTitreEcran);
    }
    else {
      titreEcran = pTitreEcran;
    }
  }
  
}
