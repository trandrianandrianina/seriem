/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc;

/**
 * Interface standard d'un traitement qui sera trait� en t�che de fond.
 * 
 * Une classe SwingWorkerAttente s'occupe l'affichage d'une boite boite de dialogue d'attente et en parall�le ex�cute le traitement
 * d�finit
 * dans la m�thode.
 */
public interface InterfaceSoumission {
  /**
   * Demande l'ex�cution en t�che de fond.
   * 
   * Cette m�thode est appel�e par la classe SwingWorkerAttente.
   */
  public void executer();
}
