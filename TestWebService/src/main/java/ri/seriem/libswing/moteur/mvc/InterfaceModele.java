/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc;

import java.util.Observer;

/**
 * Interface d'un mod�le �cran.
 *
 * Un mod�le �cran centralise les donn�es destin�es � �tre affich�es par une vue ainsi que la logique m�tier lorsque des actions sont
 * effectu�es (� ce titre, il condense les r�les "Mod�le" et "Contr�leur" du pattern MVC). Il n'a pas � intervenir directement sur la vue.
 * Le mod�le met � jour ses donn�es, notifie la vue (via la m�thode rafraichir) et celle-ci se met � jour.
 *
 * Lors de l'affichage initial d'un �cran, les m�thodes sont appel�es dans l'ordre suivant :
 * 1- modele.initialiserDonnees()
 * 2- vue.initialiserComposants()
 * 3- vue.rafraichir()
 * 4- modele.chargerDonnees()
 * 5- vue.rafraichir()
 *
 * Plusieurs abstractions de cette interface sont disponibles :
 * - AbstractModeleDialogue : � utiliser pour les JDialog.
 * - AbstractModelPanel : � utiliser pour les JPanel.
 */
public interface InterfaceModele {
  /**
   * Initialiser les donn�es avant la phase de chargement des donn�es.
   *
   * Cela permet de nettoyer rapidement les donn�es pour le rafra�chissement de la vue effectu� avant le chargement des donn�es.
   * Cette op�ration doit �tre rapide pour ne pas bloquer l'interface graphique. Les op�rations longues doivent �tre effectu�es dans la
   * m�thode chargerDonnees().
   */
  public void initialiserDonnees();
  
  /**
   * Charger les donn�es du mod�le.
   *
   * Cette m�thode est appel�e lorsqu'une vue est affich�e pour la premi�re fois et lorsque l'affichage permute et r�affiche une vue
   * pr�c�demment cach�e. Elle doit donc pouvoir �tre appel�e plusieurs fois de suite sans erreurs. Elle est appel�e entre le premier
   * rafra�chissement de la vue (sans donn�es) et le second rafra�chissement (avec donn�es).
   * Son r�le est de charger les donn�es qui sont n�cessaires � la vue mais qui sont longues � charger.
   *
   * Cette m�thode est appel�e en dehors de la thread graphique pour ne pas geler l'interface graphique. Il est interdit d'effectuer
   * des op�rations graphiques dans cette m�thode sous peine de cr�er des plantages al�atoire dans le logiciel (en particulier,
   * d'appeler la m�thode rafra�chir). Les exceptions g�n�r�es dans cette m�thode sont r�cup�r�es pour �tre affich�e dans une bo�te de
   * dialogue � partir de la thread graphique.
   */
  public void chargerDonnees();
  
  /**
   * Rafra�chir les vues associ�es au mod�le.
   *
   * Cette m�thode est � appeler � chaque fois que le mod�le souhaite d�clencher un rafra�chissement des donn�es. Elle d�clenche un
   * rafra�chissement de toutes les vues qui se sont abonn�es via la m�thode abonnverVue().
   *
   * Il est indispensable que cette m�thode soit appel�e � partir de la thread graphique.
   *
   * Si le mod�le r�f�rence un mod�le parent, la demande est syst�matiquement transmise au mod�le parent. Le mod�le parent rafra�chit
   * sa propre vue, ce qui provoque �galement le rafra�chissement de la vue enfant active.
   */
  public void rafraichir();
  
  /**
   * Abonner une vue aux notifications de rafra�chissement du mod�le.
   *
   * La vue ajout�e via cette m�thode sera rafra�chie � chaque appel de la m�thode rafraichir().
   */
  public void abonnerVue(Observer pObserver);
  
  /**
   * Interface du mod�le parent (la valeur null est retourn�e s'il n'y a pas de mod�le parent).
   *
   * Le mod�le stocke et retourne l'interface du mod�le parent pour se permettre d'avoir tout type de mod�le en tant que parent :
   * AbstractModelePanel, AbstractModeleDialogue, ...
   */
  public InterfaceModele getInterfaceModeleParent();
  
  /**
   * Modifier le mod�le parent (null pour enlever le mod�le parent).
   */
  public void setInterfaceModeleParent(InterfaceModele pInterfaceModeleParent);
  
  /**
   * Cl� de la vue enfant active.
   */
  public Object getCleVueEnfantActive();
  
  /**
   * Modifier la cl� de la vue enfant active.
   *
   * La cl� peut �tre un objet quelconque. Il est pratique d'utiliser une cha�ne de caract�re pour les vue liste/d�tail et d'utiliser
   * un Integer pour les vues onglets.
   */
  public void setCleVueEnfantActive(Object pCleVueEnfantActive);
  
  /**
   * V�rifier si les donn�es du mod�le ont �t� charg�es.
   *
   * Lors de l'affichage initial, la valeur est mise � false avant initialiserDonnees() et mise � true apr�s chargerDonnees().
   * Lors des chargements ult�rieurs de donn�es, la valeur est mise � false avant l'appel � chargerDonnees() et remise � true lorsque
   * chargerDonnees() est termin�.
   */
  public boolean isDonneesChargees();
  
  /**
   * Indiquer si les donn�es du mod�le ont �t� charg�es.
   */
  public void setDonneesChargees(boolean pDonneesChargees);
  
  /**
   * V�rifier si le mod�le a demand� la sortie de la vue.
   * Pour une vue de type bo�te de dialogue, cela signifie que la boite de dialogue va se fermer.
   */
  public boolean isModeSortie();
  
  /**
   * Modifier le mode de sortie.
   */
  public void setModeSortie(int modeSortie);
  
  /**
   * Titre de l'�cran.
   *
   * Si ce mod�le � un parent, c'est le titre du parent qui est retourn�. Ainsi, dans une hi�rarchie de mod�le, c'est toujours le titre
   * du mod�le en haut de la hi�rarchie qui est retourn�.
   */
  public String getTitreEcran();
  
  /**
   * Modifier le titre de l'�cran.
   *
   * Si ce mod�le � un parent, c'est le titre du parent qui est modifi�. Ainsi, dans une hi�rarchie de mod�le, c'est toujours le titre
   * du mod�le en haut de la hi�rarchie qui est modifi�.
   */
  public void setTitreEcran(String pTitreEcran);
}
