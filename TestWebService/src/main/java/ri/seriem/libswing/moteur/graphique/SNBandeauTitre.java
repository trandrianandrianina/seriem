/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.graphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.image.OutilImage;
import ri.seriem.libswing.composant.primitif.panel.SNPanelDegrade;
import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Bandeau de titre situ� en haut de tous les �crans du logiciel.
 *
 * Le bandeau comporte un titre � gauche et le logo de l'�tablissement � droite. Le bandeau est associ� � un �tablissement via les
 * m�thodes setIdEtablissement ou setCodeEtablissement().
 *
 * Il pr�sente un fond d�grad� de la couleur fonc�e � gauche vers la couleur claire � droite. Seule la couleur fonc�e est param�trable via
 * la m�thode setCouleurFoncee(). La couleur claire est d�termin�e automatiquement � partir de la couleur fonc�e. De m�me, la couleur du
 * titre (blanc ou noir) d�pend uniquement de la luminosit� de la couleur fonc�e du bandeau. Si la couleur fonc�e est sombre, le titre
 * est blanc. Si la couleur fonc�e est claire, le titre est noir.
 *
 */
public class SNBandeauTitre extends SNPanelDegrade {
  // Variables
  private String titre = null;
  private String sousTitre = null;
  private boolean capitaliserPremiereLettre = true;
  private ImageIcon imageIcon = null;
  
  /**
   * Constructeur.
   */
  public SNBandeauTitre() {
    super();
    initComponents();
    setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
  }
  
  /**
   * Rafraichir l'affichage du composant graphique.
   */
  private void rafraichir() {
    rafraichirTitre();
    rafraichirSousTitre();
    rafraichirLogo();
  }
  
  /**
   * Rafraichir le titre.
   */
  private void rafraichirTitre() {
    // Modifier la couleur du titre en fonction de la couleur effective du bandeau
    if (OutilImage.isCouleurSombre(getCouleurFoncee())) {
      lbTitre.setForeground(Color.WHITE);
    }
    else {
      lbTitre.setForeground(Color.BLACK);
    }
    
    // Mettre la premi�re lettre du titre en majuscules si c'est demand�
    String titreCorrige;
    if (titre != null && capitaliserPremiereLettre) {
      titreCorrige = Constantes.capitaliserPremiereLettre(titre);
    }
    else {
      titreCorrige = titre;
    }
    
    // Rafra�chir le titre
    if (titreCorrige != null) {
      lbTitre.setText(titreCorrige);
    }
    else {
      lbTitre.setText("");
    }
  }
  
  /**
   * Rafraichir le titre.
   */
  private void rafraichirSousTitre() {
    // Modifier la couleur du titre en fonction de la couleur effective du bandeau
    if (OutilImage.isCouleurSombre(getCouleurFoncee())) {
      lbSousTitre.setForeground(Color.WHITE);
    }
    else {
      lbSousTitre.setForeground(Color.BLACK);
    }
    
    // Modifier le sous-titre s'il vient d'un programme RPG
    String sousTitreCorrige = sousTitre;
    
    // Rafra�chir le sous-titre
    if (sousTitreCorrige != null) {
      lbSousTitre.setText(sousTitreCorrige);
    }
    else {
      lbSousTitre.setText("");
    }
  }
  
  /**
   * Rafraichir le logo.
   */
  private void rafraichirLogo() {
    // Rafra�chir le logo
    lbLogo.setIcon(imageIcon);
  }
  
  /**
   * Changer la couleur fonc�e du d�grad�.
   */
  @Override
  public void setCouleurFoncee(Color pCouleurFoncee) {
    super.setCouleurFoncee(pCouleurFoncee);
    rafraichir();
  }
  
  /**
   * Titre du bandeau.
   * @return Titre.
   */
  public String getText() {
    return titre;
  }
  
  /**
   * Modifier le titre du bandeau.
   * @param pTitre Nouveau titre.
   */
  public void setText(String pTitre) {
    // Tester si la valeur a chang�
    if (Constantes.equals(titre, pTitre)) {
      return;
    }
    
    // Modifier le titre
    titre = pTitre;
    rafraichir();
  }
  
  /**
   * Sous-titre du bandeau.
   */
  public String getSousTitre() {
    return sousTitre;
  }
  
  /**
   * Modifier le sous-titre du bandeau.
   * @param pSousTitre Nouveau sous-titre.
   */
  public void setSousTitre(String pSousTitre) {
    // Tester si la valeur a chang�
    if (Constantes.equals(sousTitre, pSousTitre)) {
      return;
    }
    
    // Modifier le titre
    sousTitre = pSousTitre;
    rafraichir();
  }
  
  /**
   * Modifier le sous-titre du bandeau ainsi que son infobulle.
   * @param pSousTitre Nouveau sous-titre.
   */
  public void setSousTitre(String pSousTitre, String pInfoBulleSousTitre) {
    setSousTitre(pSousTitre);
    if (pInfoBulleSousTitre != null && !pInfoBulleSousTitre.isEmpty()) {
      pnlTitre.setToolTipText(pInfoBulleSousTitre);
    }
    else {
      pnlTitre.setToolTipText(null);
    }
  }
  
  /**
   * V�rifier si le titre est modifi� de fa�on � avoir uniquement sa premi�re lettre en majuscule.
   * @return true=titre modifi�, false=titre non modifi�.
   */
  public boolean isCapitaliserPremiereLettre() {
    return capitaliserPremiereLettre;
  }
  
  /**
   * Indiquer si le titre doit �tre modifi� de fa�on � avoir uniquement sa premi�re lettre en majuscule.
   *
   * Souvent, ce param�tre est mis � true pour les titres issus des �crans RPG car ceux-ci sont fournis enti�rement en majuscules.
   * A l'inverse pour les �crans Java, ce param�tre est souveznt mis � false car le titre est correctement format� et peut contenir
   * des identficiants d'objets m�tiers. Il n'est pas judicieux de modifier le titre dans ce cas.
   *
   * @param pCapitaliserPremiereLettre true=modifier le titre, false=ne pas modifier le titre.
   */
  public void setCapitaliserPremiereLettre(boolean pCapitaliserPremiereLettre) {
    // Tester si la valeur a chang�
    if (capitaliserPremiereLettre == pCapitaliserPremiereLettre) {
      return;
    }
    
    // Modifier la valeur
    capitaliserPremiereLettre = pCapitaliserPremiereLettre;
    rafraichir();
  }
  
  public void setDateCreation(String pDateCreation) {
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlTitre = new JPanel();
    lbTitre = new JLabel();
    lbSousTitre = new JLabel();
    lbLogo = new JLabel();
    
    // ======== this ========
    setPreferredSize(new Dimension(950, 55));
    setMinimumSize(new Dimension(0, 55));
    setMaximumSize(new Dimension(2147483647, 55));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 55, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
    
    // ======== pnlTitre ========
    {
      pnlTitre.setBorder(new EmptyBorder(5, 15, 5, 5));
      pnlTitre.setOpaque(false);
      pnlTitre.setName("pnlTitre");
      pnlTitre.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlTitre.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlTitre.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlTitre.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
      ((GridBagLayout) pnlTitre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ---- lbTitre ----
      lbTitre.setText("Titre");
      lbTitre.setFont(new Font(Font.SANS_SERIF, lbTitre.getFont().getStyle() | Font.BOLD, 20));
      lbTitre.setForeground(Color.white);
      lbTitre.setName("lbTitre");
      pnlTitre.add(lbTitre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbSousTitre ----
      lbSousTitre.setFont(new Font("sansserif", Font.PLAIN, 11));
      lbSousTitre.setForeground(Color.white);
      lbSousTitre.setText("Sous-titre");
      lbSousTitre.setName("lbSousTitre");
      pnlTitre.add(lbSousTitre,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlTitre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 5), 0, 0));
    
    // ---- lbLogo ----
    lbLogo.setName("lbLogo");
    add(lbLogo,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlTitre;
  private JLabel lbTitre;
  private JLabel lbSousTitre;
  private JLabel lbLogo;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
