/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.moteur.mvc.dialogue;

import java.util.Observable;
import java.util.Observer;

import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libswing.moteur.mvc.InterfaceModele;

/**
 * Mod�le pour une bo�te de dialogue.
 */
public abstract class AbstractModeleDialogue extends Observable implements InterfaceModele {
  // Constantes
  private static final int NE_PAS_SORTIR = 0;
  private static final int SORTIE_AVEC_VALIDATION = 1;
  private static final int SORTIE_AVEC_ANNULATION = 2;
  private static final int SORTIE_AVEC_SUPPRESSION = 3;
  
  // Variables
  private Object cleVueEnfantActive = null;
  private String titreEcran = "";
  private int modeSortie = NE_PAS_SORTIR;
  private boolean donneesChargees = false;
  
  /**
   * Constructeur.
   */
  public AbstractModeleDialogue() {
  }
  
  // -- M�thodes de InterfaceModele
  
  @Override
  abstract public void initialiserDonnees();
  
  @Override
  abstract public void chargerDonnees();
  
  @Override
  public final void rafraichir() {
    setChanged();
    notifyObservers();
  }
  
  @Override
  public final void abonnerVue(Observer pObserver) {
    addObserver(pObserver);
  }
  
  @Override
  public final InterfaceModele getInterfaceModeleParent() {
    return null;
  }
  
  @Override
  public final void setInterfaceModeleParent(InterfaceModele pInterfaceModeleParent) {
    throw new MessageErreurException("Le mod�le d'une bo�te de dialogue ne peut pas avoir de mod�le parent");
  }
  
  @Override
  public final Object getCleVueEnfantActive() {
    return cleVueEnfantActive;
  }
  
  @Override
  public final void setCleVueEnfantActive(Object pCleVueEnfantActive) {
    cleVueEnfantActive = pCleVueEnfantActive;
  }
  
  @Override
  public final boolean isDonneesChargees() {
    return donneesChargees;
  }
  
  @Override
  public final void setDonneesChargees(boolean donneesChargees) {
    this.donneesChargees = donneesChargees;
  }
  
  @Override
  public final boolean isModeSortie() {
    return modeSortie != NE_PAS_SORTIR;
  }
  
  @Override
  public final void setModeSortie(int pModeSortie) {
    modeSortie = pModeSortie;
  }
  
  @Override
  public final String getTitreEcran() {
    return titreEcran;
  }
  
  @Override
  public final void setTitreEcran(String pTitreEcran) {
    titreEcran = pTitreEcran;
  }
  
  // -- M�thodes publiques
  
  /**
   * Quitter la boite de dialogue en validant la saisie. Les donn�es modifi�es sont conserv�es.
   * Cette m�thode fixe le mode de sortie avec validation et demande un rafra�chissement pour d�clencher la fermeture de la vue.
   * Si la m�thode est surcharg�ee, il est recommand� d'appel� cette m�thode parente � la fin.
   */
  public void quitterAvecValidation() {
    modeSortie = SORTIE_AVEC_VALIDATION;
    rafraichir();
  }
  
  /**
   * Quitter la boite de dialogue an annulant la saisie. Les donn�es modifi�es ne sont pas conserv�es.
   * Cette m�thode fixe le mode de sortie avec annulation et demande un rafra�chissement pour d�clencher la fermeture de la vue.
   * Si la m�thode est surcharg�ee, il est recommand� d'appel� cette m�thode parente � la fin.
   */
  public void quitterAvecAnnulation() {
    modeSortie = SORTIE_AVEC_ANNULATION;
    rafraichir();
  }
  
  /**
   * Quitter la boite de dialogue an annulant la saisie. Les donn�es modifi�es seront supprim�es de base de donn�es.
   * Cette m�thode fixe le mode de sortie avec suppression et demande un rafra�chissement pour d�clencher la fermeture de la vue.
   * Si la m�thode est surcharg�ee, il est recommand� d'appel� cette m�thode parente � la fin.
   */
  public void quitterAvecSuppression() {
    modeSortie = SORTIE_AVEC_SUPPRESSION;
    rafraichir();
  }
  
  // -- Accesseurs
  
  /**
   * Tester si l'utilisateur est sortie de la bo�te de dialogue en validant sa saisie.
   */
  public final boolean isSortieAvecValidation() {
    return modeSortie == SORTIE_AVEC_VALIDATION;
  }
  
  /**
   * Tester si l'utilisateur est sortie de la bo�te de dialogue en annulant sa saisie.
   */
  public final boolean isSortieAvecAnnulation() {
    return modeSortie == SORTIE_AVEC_ANNULATION;
  }
  
  /**
   * Tester si l'utilisateur est sortie de la bo�te de dialogue en supprimant l'objet de sa saisie.
   */
  public final boolean isSortieAvecSuppression() {
    return modeSortie == SORTIE_AVEC_SUPPRESSION;
  }
  
}
