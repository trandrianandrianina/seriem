/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.dialogueattente;

import ri.seriem.libcommun.outils.Trace;
import ri.seriem.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Mod�le de la bo�te de dialogue "Message d'information".
 */
public class ModeleDialogueAttente extends AbstractModeleDialogue {
  private final String MESSAGE_PAR_DEFAUT = "Veuillez patienter ...";
  
  // Variables
  private String message = "";
  
  /**
   * Constructeur.
   */
  public ModeleDialogueAttente() {
    super();
  }
  
  // M�thodes standards du mod�le
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
  }
  
  /**
   * Renseigner le message utilisateur.
   * Un message par d�faut est affich� si le message re�u n'est pas valide (null ou vide).
   */
  public void modifierMessage(String pMessage) {
    // Mettre un message par d�faut en cas d'absence de messsage
    if (pMessage != null && !pMessage.trim().isEmpty()) {
      message = pMessage;
    }
    else {
      message = MESSAGE_PAR_DEFAUT;
    }
    
    // Tracer le message en enlevant tous les retours � la ligne �ventuels
    Trace.info(message.replaceAll("(\\r|\\n)", " "));
    rafraichir();
  }
  
  // -- Accesseurs
  
  /**
   * Message d'information
   */
  public String getMessage() {
    return message;
  }
}
