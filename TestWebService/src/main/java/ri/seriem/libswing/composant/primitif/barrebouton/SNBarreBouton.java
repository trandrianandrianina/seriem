/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.barrebouton;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libcommun.outils.Trace;
import ri.seriem.libswing.composant.dialoguestandard.dialogueerreur.DialogueErreur;
import ri.seriem.libswing.composant.primitif.bouton.EnumBouton;
import ri.seriem.libswing.composant.primitif.bouton.EnumCategorieBouton;
import ri.seriem.libswing.composant.primitif.bouton.SNBouton;
import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Barre de boutons situ�es dans les �crans et bo�tes de dialogue.
 *
 * Ce composant doit �tre utilis� pour tout affichage de boutons dans le logiciel. Il g�re l'affichage des boutons des cat�gories
 * standards (orange), validation (vert), annulation (rouge) et des boutons de type image.
 *
 * En mode standard (non condens�), les boutons standards sont justifi�s � gauche tandis que les boutons de validation et d'annulation
 * sont justifi�s � droite. En mode condens�, tous les boutons sont justifi�s � droite, qu'ils soient des cat�gories standard, validation
 * ou annulation. C'est par exemple utilis� dans les barres de boutons pour les encarts de recherche.
 *
 * La barre de boutons peut g�rer l'affichage d'un menu contextuel. Pour cela, il suffit de lier la barre de boutons avec le menu
 * contextuel de la fen�tre via la m�thode setMenuContextuel() puis d'ajouter les boutons devant �tre pr�sents dans le menu contextuel
 * via la m�thode ajouterBoutonContextuel(). Le bouton sera ajout� � la barre et dans le menu contextuel. Son libell�, son mn�monique et
 * sa visibilit� seront g�r�s de concert dans la barre de boutons et le menu contextuel. Sous JFormDesigner, le menu contextuel est un
 * composant JPopupMenu ajout� dans la zone libre autour de la fen�tre principale. Il ne doit pas contenir d'entr�es puisque celles-ci
 * sont g�r�es via la barre de boutons. Pour afficher le menu contextuel sur un composant, il faut renseigner la propri�t�
 * "componentPopupMenu" du composant avec le menu contextuel.
 *
 * Sa hauteur est de 65 pixels, ce qui correspond � la hauteur standard de deux composants (30 pixels) et d'une marge standard (5 pixels).
 *
 * Utilisation :
 * - Ajouter un composant SNBarreBouton � un �cran.
 * - Utiliser la m�thode setModeCondense(true) pour activer le mode condens�.
 * - Utiliser la m�thode setModeNavigation(true) pour activer le mode navigation.
 * - Utiliser snBarreBouton.ajouterBouton(EnumBouton.QUITTER) pour ajouter un bouton pr�-configur� � un �cran.
 * - Utiliser snBarreBouton.ajouterBouton(BOUTON_AFFICHERDETAIL, 'c') pour ajouter un bouton personnalis� � un �cran.
 * - Utiliser la m�thode ajouterBoutonListener() pour �tre notifi� lorsqu'un bouton est cliqu�.
 */
public class SNBarreBouton extends JPanel {
  static private final int HAUTEUR = 50;
  static private final int MARGE_INTERNE = 5;
  static private final int MARGE_EXTERNE = 10;
  
  private List<SNBouton> listeBoutonStandard = new ArrayList<SNBouton>();
  private List<SNBouton> listeBoutonValidation = new ArrayList<SNBouton>();
  private List<SNBouton> listeBoutonAnnulation = new ArrayList<SNBouton>();
  private List<SNBouton> listeBoutonImage = new ArrayList<SNBouton>();
  private JPanel pnlGauche = null;
  private JViewport vpGauche = null;
  private JPanel pnlDroite = null;
  private JPanel pnlNavigation = null;
  private SNBouton btnNavigationPrecedent = null;
  private JLabel lbCompteur = null;
  private SNBouton btnNavigationSuivant = null;
  private SNBouton snPageSuivante = null;
  private SNBoutonListener boutonListener = null;
  private boolean modeCondense = false;
  private boolean modeNavigation = false;
  private int compteurMax = 0;
  private int compteurCourant = 0;
  private JPopupMenu popupMenu = null;
  
  /**
   * Constructeur par d�faut.
   */
  public SNBarreBouton() {
    this(false);
  }
  
  /**
   * Constructeur param�tr�.
   * Le panneau principal est utilis� comme premier panneau des �crans et bo�tes de dialogue. Il contient l'ensemble des composants
   * � l'exception de la barre de boutons (SNBarreBouton). En tant que composant parent, il est opaque et colorie la zone avec la couleur
   * de fond standard. Il comporte �galement une marge pour s�parer les composants des bords de l'�cran ou de labo�te de dialogue.
   */
  public SNBarreBouton(boolean pModeCondense) {
    modeCondense = pModeCondense;
    
    // Nommer le composant (pour QFTest)
    setName(getClass().getSimpleName());
    
    // D�finir la taille minimum de la barre de boutons
    setMinimumSize(new Dimension(0, HAUTEUR + MARGE_INTERNE + MARGE_EXTERNE));
    setPreferredSize(new Dimension(0, HAUTEUR + MARGE_INTERNE + MARGE_EXTERNE));
    
    // D�finir le layout principal
    setLayout(new GridBagLayout());
    
    // Apparence du composant dans le designer
    if (Beans.isDesignTime()) {
      setOpaque(true);
      setBackground(SNCharteGraphique.COULEUR_BOUTON_ORANGE);
      add(new JLabel(getClass().getSimpleName()), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BASELINE, new Insets(0, 0 - 0, 0, 0), 0, 0));
      return;
    }
    
    // Rendre le composant transparent
    setOpaque(false);
    
    // Afficher un bord en mode design ou en mode debug
    if (Beans.isDesignTime() || Trace.isModeDebug()) {
      setBorder(BorderFactory.createLineBorder(SNCharteGraphique.COULEUR_DEBUG));
    }
    
    // Configurer le panel sup�rieur, pr�sent uniquement pour remplir l'espace, les boutons devant toujours �tre en bas
    JPanel pnlHaut = new JPanel();
    pnlHaut.setName("pnlHaut");
    pnlHaut.setOpaque(false);
    add(pnlHaut,
        new GridBagConstraints(0, 0, 4, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // Configurer le panel de gauche
    FlowLayout layoutGauche = new FlowLayout();
    layoutGauche.setAlignment(FlowLayout.LEFT);
    layoutGauche.setHgap(MARGE_INTERNE);
    layoutGauche.setVgap(0);
    pnlGauche = new JPanel(layoutGauche);
    pnlGauche.setName("pnlGauche");
    pnlGauche.setOpaque(false);
    pnlGauche.setMinimumSize(new Dimension(0, HAUTEUR));
    pnlGauche.setPreferredSize(new Dimension(0, HAUTEUR));
    
    // Configurer le viewport de gauche
    vpGauche = new JViewport();
    vpGauche.setView(pnlGauche);
    vpGauche.setMinimumSize(new Dimension(0, HAUTEUR));
    vpGauche.setPreferredSize(new Dimension(0, HAUTEUR));
    vpGauche.setOpaque(false);
    add(vpGauche, new GridBagConstraints(2, 1, 1, 1, 1, 0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
        new Insets(MARGE_INTERNE, MARGE_EXTERNE - MARGE_INTERNE, MARGE_EXTERNE, 0), 0, 0));
    
    // Configurer le pannel de droite
    FlowLayout layoutDroite = new FlowLayout();
    layoutDroite.setAlignment(FlowLayout.RIGHT);
    layoutDroite.setHgap(MARGE_INTERNE);
    layoutDroite.setVgap(0);
    pnlDroite = new JPanel(layoutDroite);
    pnlDroite.setName("pnlDroite");
    pnlDroite.setOpaque(false);
    add(pnlDroite, new GridBagConstraints(4, 1, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
        new Insets(MARGE_INTERNE, 0, MARGE_EXTERNE, MARGE_EXTERNE - MARGE_INTERNE), 0, 0));
    
    // G�rer l'affichage du viewport dans le panel de gauche
    rafraichirPageGauche();
    
    // Etre notifier lorsque le viewport est redimensionn�
    vpGauche.addComponentListener(new ComponentListener() {
      @Override
      public void componentResized(ComponentEvent e) {
        rafraichirPageGauche();
      }
      
      @Override
      public void componentMoved(ComponentEvent e) {
      }
      
      @Override
      public void componentShown(ComponentEvent e) {
      }
      
      @Override
      public void componentHidden(ComponentEvent e) {
      }
    });
    
    // Appeler la m�thode valider() lors d'un apppui sur la touche "Entr�e"
    InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    ActionMap actionMap = getActionMap();
    inputMap.put(SNCharteGraphique.TOUCHE_ENTREE, "Enter");
    actionMap.put("Enter", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        valider();
      }
    });
  }
  
  // -- M�thodes priv�es
  
  /**
   * Traiter un appui sur la touche "Entr�e".
   *
   * Si le composant avec le focus a un InputVerifier, on l'appelle pour v�rifier que la saisie en cours est valide. Si la saisie n'est
   * pas valide, l'action sur la touche entr�e ne produit rien. Si la saisie est valide, le focus sur le composant en cours est enlev�
   * pour que les listeners correspondants soient appel�s.
   */
  private void valider() {
    try {
      // Ne rien faire s'il n'y a pas de listener
      if (boutonListener == null) {
        return;
      }
      
      // Rechercher le premier bouton de la cat�gorie "Validation" qui est actif
      SNBouton snBoutonValide = null;
      for (SNBouton snBouton : listeBoutonValidation) {
        if (snBouton.isActif()) {
          snBoutonValide = snBouton;
          break;
        }
      }
      
      // Sortir si aucun bouton "Validation" est actif
      if (snBoutonValide == null) {
        return;
      }
      
      // Si le composant avec le focus a un InputVerifier, on l'appelle pour v�rifier que la saisie en cours est valide.
      Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
      if (component != null && component instanceof JComponent) {
        JComponent jcomponent = (JComponent) component;
        InputVerifier inputVerifier = jcomponent.getInputVerifier();
        if (inputVerifier != null) {
          if (!inputVerifier.shouldYieldFocus(jcomponent)) {
            return;
          }
        }
      }
      
      // Enlever le focus du composant en cours pour que les listeners correspondants soient appel�s
      KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
      
      // La m�thode quitterAvecValidation est appel�e plus tard pour laisser � la thread graphique le temps de traiter la perte de focus.
      final SNBouton snBoutonValide2 = snBoutonValide;
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          try {
            boutonListener.traiterClicBouton(snBoutonValide2);
          }
          catch (Exception exception) {
            DialogueErreur.afficher(exception);
          }
        }
      });
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Rafra�chir les entr�es du menu contextuel.
   */
  private void rafraichirMenuContextuel() {
    // Tester si un menu contextuel est d�fini
    if (popupMenu == null) {
      return;
    }
    
    // Supprimer tous les points de menu du menu contextuel
    popupMenu.removeAll();
    
    // Ajouter les boutons pr�-configur�s s'ils sont contextuels et actifs
    for (SNBouton snBouton : listeBoutonValidation) {
      // Tester si le bouton est contextuel et actif
      if (snBouton.isContextuel() && snBouton.isEnabled()) {
        // La police des menus pour ce type de bouton est mise en gras
        String texte = snBouton.getText();
        texte = texte.replace("<html>", "<html><b>").replace("</html>", "</b></html>");
        // Cr�ation du point de menu
        JMenuItem menuItem = new JMenuItem(texte);
        // Le nom du menu est identique au bouton afin de faire le lien pour les actions
        menuItem.setName(snBouton.getName());
        menuItem.setMnemonic(snBouton.getMnemonic());
        menuItem.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            miActionPerformed(e);
          }
        });
        popupMenu.add(menuItem);
      }
    }
    
    // Ajouter les boutons standards s'ils sont contextuels et actifs
    for (SNBouton snBouton : listeBoutonStandard) {
      // Tester si le bouton est contextuel et actif
      if (snBouton.isContextuel() && snBouton.isEnabled()) {
        JMenuItem menuItem = new JMenuItem(snBouton.getText());
        // Le nom du menu est identique au bouton afin de faire le lien pour les actions
        menuItem.setName(snBouton.getName());
        menuItem.setMnemonic(snBouton.getMnemonic());
        menuItem.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            miActionPerformed(e);
          }
        });
        popupMenu.add(menuItem);
      }
    }
  }
  
  /**
   * Rafra�chir l'ensemble des boutons de la barre.
   */
  private void rafraichirBouton() {
    if (pnlGauche == null || pnlDroite == null) {
      return;
    }
    
    // Effacer les panels
    pnlGauche.removeAll();
    pnlDroite.removeAll();
    
    // Ajouter les boutons standards
    for (SNBouton snBouton : listeBoutonStandard) {
      if (modeCondense) {
        pnlDroite.add(snBouton);
      }
      else {
        pnlGauche.add(snBouton);
      }
    }
    
    // Ajouter les boutons de la cat�gorie "Validation"
    for (SNBouton snBouton : listeBoutonValidation) {
      pnlDroite.add(snBouton);
    }
    
    // Ajouter les boutons de la cat�gorie "Annulation" en dernier pour qu'ils soient toujours le plus � droite
    for (SNBouton snBouton : listeBoutonAnnulation) {
      pnlDroite.add(snBouton);
    }
    
    // Rafraichir le Viewport de gauche
    rafraichirPageGauche();
    
    // Rafraichir le menu contextuel
    rafraichirMenuContextuel();
    
    // Forcer la rafra�chissement du composant
    // Utile si la barre de boutons est modifi�es dynamiquement dans la m�thode rafraichir() d'une vue.
    revalidate();
    repaint();
  }
  
  /**
   * Calculer la largeur du panel de gauche en fonction des boutons visibles.
   */
  private int calculerLargeurPanelGauche() {
    if (modeCondense) {
      return 0;
    }
    int largeur = MARGE_INTERNE;
    for (SNBouton snBouton : listeBoutonStandard) {
      if (snBouton.isVisible()) {
        largeur += snBouton.getPreferredSize().getWidth() + MARGE_INTERNE;
      }
    }
    return largeur;
  }
  
  /**
   * Calculer la longueur du d�placement lorsqu'on clique sur les boutons pr�c�dent ou suivant.
   * L'objectif est d'avancer d'un nombre entier de boutons afin que le bouton tronqu� en fin de ligne soit affich� en entier
   * dans la page suivante.
   */
  private int calculerDeplacement() {
    int nombreBouton = vpGauche.getWidth() / (SNBouton.TAILLE.width + MARGE_INTERNE);
    return nombreBouton * (SNBouton.TAILLE.width + MARGE_INTERNE);
  }
  
  /**
   * Recalculer le positionnement du panel de gauche dans le viewport.
   */
  private void rafraichirPageGauche() {
    rafraichirPageGauche(vpGauche.getViewPosition());
  }
  
  /**
   * Calculer le positionnement du panel de gauche dans le viewport.
   * En fonction de leur taille respective et de la positon, les boutons "Page pr�c�dente" et "Page suivante" sont affich�s ou non.
   */
  private void rafraichirPageGauche(Point pPoint) {
    if (vpGauche == null || pnlGauche == null) {
      return;
    }
    
    // Red�finir la dimension du panel gauche en focntion de son contenu.
    // Etape importante car ses dimensions ne sont pas dict�es par le parent (ViewPort).
    Dimension dimensionPanelGauche = new Dimension(calculerLargeurPanelGauche(), pnlGauche.getHeight());
    pnlGauche.setMinimumSize(dimensionPanelGauche);
    pnlGauche.setPreferredSize(dimensionPanelGauche);
    pnlGauche.setSize(dimensionPanelGauche);
    
    // Masquer les boutons pages pr�c�dentes et suivantes avant de v�rifier les dimensions
    // Le fait de masquer les boutons peut lib�rer de la place pour pourvoir afficher tous les boutons
    if (snPageSuivante != null) {
      snPageSuivante.setVisible(false);
    }
    
    // Se positionner � z�ro si on est trop loin vers la droite
    if (pPoint.x <= 0) {
      pPoint.x = 0;
    }
    // Se positionner � z�ro si la barre de boutons � afficher est plus petite que l'espace disponible.
    else if (pnlGauche.getWidth() < vpGauche.getWidth()) {
      pPoint.x = 0;
    }
    // Se positionner de fa�on � ce que l'espace disponible soit rempli de boutons.
    else if (pPoint.x > pnlGauche.getWidth() - vpGauche.getWidth()) {
      pPoint.x = pnlGauche.getWidth() - vpGauche.getWidth();
    }
    
    // D�caler la barre de boutons dans le viewport
    vpGauche.setViewPosition(pPoint);
    
    // Afficher les boutons si on est pas � fond � gauche et � fond � droite
    if (pPoint.x > 0 || pPoint.x < pnlGauche.getWidth() - vpGauche.getWidth()) {
      // Cr�er le bouton suivant la premi�re fois
      if (snPageSuivante == null) {
        snPageSuivante = new SNBouton(EnumCategorieBouton.IMAGE, "bouton_suivant", KeyEvent.VK_LESS, false);
        add(snPageSuivante, new GridBagConstraints(3, 1, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.NONE,
            new Insets(MARGE_EXTERNE, MARGE_INTERNE, MARGE_EXTERNE, MARGE_EXTERNE), 0, 0));
        snPageSuivante.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            afficherPageSuivante();
          }
        });
      }
      
      // Afficher les boutons
      snPageSuivante.setVisible(true);
    }
  }
  
  /**
   * Afficher la page suivante de la barre de boutons.
   */
  private void afficherPageSuivante() {
    Point point = vpGauche.getViewPosition();
    if (point.x >= pnlGauche.getWidth() - vpGauche.getWidth()) {
      point.x = 0;
    }
    else {
      point.x += calculerDeplacement();
    }
    rafraichirPageGauche(point);
  }
  
  /**
   * Rafra�chir les donn�es de navigation.
   */
  private void rafraichirNavigation() {
    // Rafra�chir le texte
    if (compteurCourant <= 0 || compteurMax <= 0) {
      lbCompteur.setText("");
    }
    else {
      lbCompteur.setText("" + compteurCourant + " / " + compteurMax);
    }
    
    // Activer ou d�sactiver le bouton pr�c�dent
    if (compteurCourant > 1) {
      btnNavigationPrecedent.setEnabled(true);
    }
    else {
      btnNavigationPrecedent.setEnabled(false);
    }
    
    // Activer ou d�sactiver le bouton suivant
    if (compteurCourant < compteurMax) {
      btnNavigationSuivant.setEnabled(true);
    }
    else {
      btnNavigationSuivant.setEnabled(false);
    }
  }
  
  /**
   * Traiter les clics sur les boutons situ�s dans la barre de boutons.
   */
  private void btActionPerformed(ActionEvent e) {
    try {
      // Tester s'il y a un listener
      if (boutonListener == null) {
        throw new MessageErreurException("Aucune op�ration n'a �t� associ�e aux boutons de cette barre de boutons.");
      }
      
      // R�cup�rer le bouton � l'origine de l'�v�nement
      SNBouton bouton = (SNBouton) e.getSource();
      boutonListener.traiterClicBouton(bouton);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter les clics sur les entr�es du menu contextuel.
   */
  private void miActionPerformed(ActionEvent e) {
    try {
      // Tester s'il y a un listener
      if (boutonListener == null) {
        throw new MessageErreurException("Aucune op�ration n'a �t� associ�e aux boutons de cette barre de boutons.");
      }
      
      // Rechercher le bouton correspondant au menu contextuel via les libell�s
      JMenuItem menuItem = (JMenuItem) e.getSource();
      for (SNBouton snBouton : listeBoutonValidation) {
        if (snBouton.isContextuel() && Constantes.equals(snBouton.getName(), menuItem.getName())) {
          boutonListener.traiterClicBouton(snBouton);
          return;
        }
      }
      for (SNBouton snBouton : listeBoutonStandard) {
        if (snBouton.isContextuel() && Constantes.equals(snBouton.getName(), menuItem.getName())) {
          boutonListener.traiterClicBouton(snBouton);
          return;
        }
      }
      
      // Afficher un message d'erreur si le menu contextuel ne correspond � aucun bouton
      throw new MessageErreurException("Aucun bouton n'est associ� au menu contextuel : " + menuItem.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // -- M�thodes publiques
  
  /**
   * Ajouter un listener lorsque l'un des boutons de la barre est cliqu�.
   */
  public void ajouterBoutonListener(SNBoutonListener pBoutonListener) {
    boutonListener = pBoutonListener;
  }
  
  /**
   * Ajouter un bouton � la barre de boutons.
   * Le param�tre visibilit� permet d'indiquer si le bouton doit �tre visible ou non lors de l'affichage initial de la barre d'outils.
   */
  public void ajouterBouton(SNBouton pSNBouton, boolean pVisible) {
    if (pSNBouton == null) {
      throw new MessageErreurException("Le bouton � ajouter � la barre de boutons est invalide.");
    }
    SNBouton snBoutonExistant = getBouton(pSNBouton.getName());
    if (snBoutonExistant != null) {
      throw new MessageErreurException("Impossible d'ajouter deux boutons avec le m�me nom : " + pSNBouton.getName());
    }
    
    // Ajouter le bouton � la liste
    switch (pSNBouton.getCategorie()) {
      case STANDARD:
        listeBoutonStandard.add(pSNBouton);
        pSNBouton.activer(pVisible, false);
        break;
      
      case VALIDATION:
        listeBoutonValidation.add(pSNBouton);
        pSNBouton.activer(pVisible, listeBoutonValidation.size() > 1);
        
        // Modifier les boutons validation existant
        if (listeBoutonValidation.size() > 1) {
          for (SNBouton snBouton : listeBoutonValidation) {
            snBouton.activer(snBouton.isActif(), true);
          }
        }
        break;
      
      case ANNULATION:
        listeBoutonAnnulation.add(pSNBouton);
        pSNBouton.activer(pVisible, false);
        break;
      
      case IMAGE:
        listeBoutonImage.add(pSNBouton);
        pSNBouton.activer(pVisible, false);
        break;
      
      default:
        throw new MessageErreurException("La cat�gorie de boutons est inconnue.");
    }
    
    // Ajouter un listener au bouton
    pSNBouton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btActionPerformed(e);
      }
    });
    
    // Rafra�chir la barre de boutons
    rafraichirBouton();
  }
  
  /**
   * Associer la barre de boutons � un menu contextuel.
   */
  public void setMenuContextuel(JPopupMenu pPopupMenu) {
    // Sortir si la valeur n'a pas chang�e
    if (Constantes.equals(popupMenu, pPopupMenu)) {
      return;
    }
    
    // M�moriser le nouveau menu contextuel
    popupMenu = pPopupMenu;
    
    // Rafra�chir les entr�es du menu contextuel
    if (popupMenu != null) {
      rafraichirMenuContextuel();
    }
  }
  
  /**
   * Ajouter un bouton contextuel � la barre de boutons.
   * Un bouton contextuel est comme un bouton stantard mais il sera �galement affich� dans le menu contextuel s'il est actif.
   */
  public void ajouterBoutonContextuel(String pLibelle, Character pMnemonique, boolean pVisible) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libell� du bouton est invalide.");
    }
    if (pMnemonique == null) {
      throw new MessageErreurException("Le bouton \"" + pLibelle + "\" ne comporte pas de mn�monique.");
    }
    ajouterBouton(new SNBouton(pLibelle, pMnemonique, true), pVisible);
  }
  
  /**
   * Ajouter un bouton contextuel � la barre de boutons.
   * Un bouton contextuel est comme un bouton pr�-configur� mais il sera �galement affich� dans le menu contextuel s'il est actif.
   */
  public void ajouterBoutonContextuel(EnumBouton pTypeBouton, boolean pVisible) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    ajouterBouton(new SNBouton(pTypeBouton, true), pVisible);
  }
  
  /**
   * Ajouter un bouton standard � la barre de boutons.
   * Un bouton standard est de couleur orange. On doit d�finir un libell� et un mn�monique. Le param�tre visibilit� permet d'indiquer
   * si le bouton doit �tre visible ou non lors de l'affichage initial de la barre d'outils.
   */
  public void ajouterBouton(String pLibelle, Character pMnemonique, boolean pVisible) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libell� du bouton est invalide.");
    }
    if (pMnemonique == null) {
      throw new MessageErreurException("Le bouton \"" + pLibelle + "\" ne comporte pas de mn�monique.");
    }
    ajouterBouton(new SNBouton(pLibelle, pMnemonique, false), pVisible);
  }
  
  /**
   * Ajouter un bouton pr�-configur� � la barre de boutons.
   * Le libell�, la couleur et le mn�monique dont d�finis de fa�on standard. Le param�tre visibilit� permet d'indiquer si le bouton
   * doit �tre visible ou non lors de l'affichage initial de la barre d'outils.
   */
  public void ajouterBouton(EnumBouton pTypeBouton, boolean pVisible) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    ajouterBouton(new SNBouton(pTypeBouton), pVisible);
  }
  
  /**
   * Supprimer un bouton de la barre de boutons.
   */
  public void supprimerBouton(SNBouton pSNBouton) {
    // V�rifier les param�tres
    if (pSNBouton == null) {
      throw new MessageErreurException("Le bouton � supprimer de la barre de boutons est invalide.");
    }
    
    // Supprimer le bouton de la liste correspondante
    switch (pSNBouton.getCategorie()) {
      case STANDARD:
        listeBoutonStandard.remove(pSNBouton);
        break;
      
      case VALIDATION:
        listeBoutonValidation.remove(pSNBouton);
        break;
      
      case ANNULATION:
        listeBoutonAnnulation.remove(pSNBouton);
        break;
      
      case IMAGE:
        listeBoutonImage.remove(pSNBouton);
        break;
      
      default:
        throw new MessageErreurException("La cat�gorie de boutons est inconnue.");
    }
    
    // Rafra�chir la barre de boutons
    rafraichirBouton();
  }
  
  /**
   * Supprimer tous les boutons.
   */
  public void supprimerTout() {
    listeBoutonStandard.clear();
    listeBoutonValidation.clear();
    listeBoutonAnnulation.clear();
    rafraichirBouton();
  }
  
  /**
   * R�cup�rer un bouton par son nom.
   */
  public SNBouton getBouton(String pNom) {
    for (SNBouton snBouton : listeBoutonValidation) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    for (SNBouton snBouton : listeBoutonAnnulation) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    for (SNBouton snBouton : listeBoutonStandard) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    for (SNBouton snBouton : listeBoutonImage) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    return null;
  }
  
  /**
   * R�cup�rer un bouton par son type.
   */
  public SNBouton getBouton(EnumBouton pTypeBouton) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    return getBouton(pTypeBouton.getTexte());
  }
  
  /**
   * Tester si un bouton est actif ou non.
   */
  public boolean isBoutonActif(String pNom) {
    SNBouton snBouton = getBouton(pNom);
    if (snBouton == null) {
      throw new MessageErreurException("Aucune bouton ne porte le nom suivant : " + pNom);
    }
    return snBouton.isActif();
  }
  
  /**
   * Tester si un bouton pr�-configur� est actif ou non.
   */
  public boolean isBoutonActif(EnumBouton pTypeBouton) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    return isBoutonActif(pTypeBouton.getTexte());
  }
  
  /**
   * Activer un bouton.
   * Les boutons des cat�gories standards et annulation sont masqu�s. Les boutons de la cat�gorie image sont gris�s.
   * Les boutons de la cat�gorie validation sont gris�s s'il n'y a qu'un seul bouton de cette cat�gorie et sont masqu�s s'il y a plusieurs
   * boutons de cette cat�gorie.
   */
  public void activerBouton(String pNom, boolean pVisible) {
    SNBouton snBouton = getBouton(pNom);
    if (snBouton == null) {
      throw new MessageErreurException("Aucun bouton ne porte le nom suivant : " + pNom);
    }
    
    snBouton.activer(pVisible, listeBoutonValidation.size() > 1);
    rafraichirPageGauche();
    rafraichirMenuContextuel();
  }
  
  /**
   * Afficher ou cacher un bouton pr�-configur�.
   * Les boutons des cat�gories standards et annulation sont masqu�s. Les boutons des cat�gories validation et image sont juste gris�s.
   */
  public void activerBouton(EnumBouton pTypeBouton, boolean pVisible) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    activerBouton(pTypeBouton.getTexte(), pVisible);
  }
  
  /**
   * Mode condens�.
   */
  public boolean isModeCondense() {
    return modeCondense;
  }
  
  /**
   * Mode navigation.
   */
  public boolean isModeNavigation() {
    return modeNavigation;
  }
  
  /**
   * Activer ou d�sactiver le mode navigation.
   * Le mode navigation fait appara�tre des boutons permettant de parcourir le r�sultat d'une recherche. Les �l�ments suivants
   * appara�ssent � gauche de la berre d'outils : un bouton pr�c�dent pour afficher le r�sultat pr�c�dent, un compteur pour conna�tre
   * l'index courant et le nombre de r�sultats de la recherche, un bouton suivant pour afficher le r�sultat suivant.
   */
  public void setModeNavigation(boolean pModeNavigation) {
    if (modeNavigation == pModeNavigation) {
      return;
    }
    modeNavigation = pModeNavigation;
    
    // Cr�er les composants la premi�re fois
    if (pnlNavigation == null) {
      // Initialiser le panel compteur
      // On souhaite qu'il ait la m�me dimension qu'un bouton.
      pnlNavigation = new JPanel(new GridBagLayout());
      pnlNavigation.setMinimumSize(SNBouton.TAILLE);
      pnlNavigation.setPreferredSize(SNBouton.TAILLE);
      pnlNavigation.setOpaque(false);
      add(pnlNavigation, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.NONE,
          new Insets(MARGE_EXTERNE, MARGE_EXTERNE, MARGE_EXTERNE, 0), 0, 0));
      
      // Ajouter le bouton navigation pr�c�dente
      btnNavigationPrecedent = new SNBouton(EnumBouton.NAVIGATION_PRECEDENT);
      ajouterBouton(btnNavigationPrecedent, true);
      pnlNavigation.add(btnNavigationPrecedent,
          new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      
      // Ajouter le libell� avec le compteur
      lbCompteur = new JLabel("");
      lbCompteur.setFont(SNCharteGraphique.POLICE_TITRE);
      // lbCompteur.setMaximumSize(new Dimension(100, HAUTEUR));
      pnlNavigation.add(lbCompteur,
          new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      
      // Initialiser le bouton suivant
      btnNavigationSuivant = new SNBouton(EnumBouton.NAVIGATION_SUIVANT);
      ajouterBouton(btnNavigationSuivant, true);
      pnlNavigation.add(btnNavigationSuivant,
          new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }
    
    pnlNavigation.setVisible(modeNavigation);
  }
  
  /**
   * Compteur maximum de la navigation.
   */
  public int getCompteurMax() {
    return compteurMax;
  }
  
  /**
   * Modifier le compteur maximum de la navigation
   */
  public void setCompteurMax(int pCompteurMax) {
    if (compteurMax == pCompteurMax) {
      return;
    }
    compteurMax = pCompteurMax;
    rafraichirNavigation();
  }
  
  /**
   * Compteur courant de la navigation.
   */
  public int getCompteurCourant() {
    return compteurCourant;
  }
  
  /**
   * Modifier le compteur courant de la navigation
   */
  public void setCompteurCourant(int pCompteurCourant) {
    if (compteurCourant == pCompteurCourant) {
      return;
    }
    compteurCourant = pCompteurCourant;
    rafraichirNavigation();
  }
}
