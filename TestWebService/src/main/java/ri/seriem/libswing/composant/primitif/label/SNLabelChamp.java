/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.label;

import java.awt.Dimension;

import javax.swing.SwingConstants;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Libell� de champ avec la pr�sentation standard.
 * 
 * Ce composant remplace JLabel et d�rive de SNLabel.
 * Il est � utiliser pour tous les libell�s de champs. Il utilise la police standard align�e vers la droite.
 * Si le message est important il sera affich� en rouge sinon en noir.
 * 
 * D'autres composants libell�s sont disponibles :
 * - SNLabelMessage : pour les messages � mettre en �vidence en fonction de leur importance.
 * - SNLabelTitre : pour les titres de sections ou de tableaux.
 * - SNLabelUnite : pour les unit�s associ�es � un champ.
 * 
 * Caract�ristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 * - Align� � droite.
 */
public class SNLabelChamp extends SNLabel {
  private final Dimension TAILLE = new Dimension(150, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  
  /**
   * Constructeur.
   */
  public SNLabelChamp() {
    super();
    
    setOpaque(false);
    setMinimumSize(TAILLE);
    setPreferredSize(TAILLE);
    setMaximumSize(TAILLE);
    setFont(SNCharteGraphique.POLICE_STANDARD);
    setHorizontalAlignment(SwingConstants.TRAILING);
  }
}
