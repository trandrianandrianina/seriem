/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.label;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;

import ri.seriem.libcommun.commun.message.EnumImportanceMessage;
import ri.seriem.libcommun.commun.message.Message;
import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Cette classe abstraite remplace les JLabel.
 * 
 * Elle ne peut pas �tre utilis�e directement. Il faut utiliser ses composants enfants :
 * - SNLabelChamp : pour nommer les champs
 * - SNLabelUnite : pour les labels cadr�s � gauche apparaissant � la droite d'un champ
 * - SNLabelTitre : pour les titres de sections ou de tableaux.
 * - SNLabelMessage : pour les messages � mettre en �vidence en fonction de leur importance.
 * 
 * Caract�ristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 */
abstract class SNLabel extends JLabel {
  private final Dimension TAILLE = new Dimension(150, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  private EnumImportanceMessage importanceMessage = EnumImportanceMessage.NORMAL;
  private String texte;
  
  /**
   * Constructeur.
   */
  protected SNLabel() {
    super();
    
    setOpaque(false);
    setMinimumSize(TAILLE);
    setPreferredSize(TAILLE);
    setMaximumSize(TAILLE);
  }
  
  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichir() {
    // Rafra�chir le texte
    if (texte != null) {
      super.setText(texte);
    }
    else {
      super.setText("");
    }
    
    // Chanegr la police en focntino de l'importance
    if (importanceMessage != null) {
      // Les labels de titres sont toujours en gras
      if (this instanceof SNLabelTitre) {
        switch (importanceMessage) {
          case HAUT:
            setFont(SNCharteGraphique.POLICE_TITRE);
            setForeground(Color.red);
            break;
          
          default:
            setFont(SNCharteGraphique.POLICE_TITRE);
            setForeground(Color.black);
            break;
        }
      }
      else {
        switch (importanceMessage) {
          case NORMAL:
            setFont(SNCharteGraphique.POLICE_STANDARD);
            setForeground(Color.black);
            break;
          
          case MOYEN:
            setFont(SNCharteGraphique.POLICE_TITRE);
            setForeground(Color.black);
            break;
          
          case HAUT:
            setFont(SNCharteGraphique.POLICE_TITRE);
            setForeground(Color.red);
            break;
          
          default:
            setFont(SNCharteGraphique.POLICE_STANDARD);
            setForeground(Color.black);
            break;
        }
      }
    }
    // L'importance est nulle alors, on utilise les valeurs par d�faut du composant
    else {
      setFont(SNCharteGraphique.POLICE_STANDARD);
      setForeground(Color.black);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Message affich�.
   */
  public Message getMessage() {
    return Message.getMessage(texte, importanceMessage);
  }
  
  /**
   * Modifier le message � afficher.
   */
  public void setMessage(Message message) {
    if (message != null) {
      texte = message.getTexte();
      importanceMessage = message.getImportanceMessage();
    }
    else {
      texte = "";
      importanceMessage = EnumImportanceMessage.NORMAL;
    }
    rafraichir();
  }
  
  /**
   * Modifier le texte du message � afficher.
   * A utiliser conjointement avec setImportanceMessage() pour d�finir l'importance du message. La m�thode setMessage() permet de faire
   * les deux � la fois.
   */
  @Override
  public void setText(String pTexte) {
    texte = pTexte;
    rafraichir();
  }
  
  /**
   * Importance du message.
   */
  public EnumImportanceMessage getImportanceMessage() {
    return importanceMessage;
  }
  
  /**
   * Modifier l'importance du message.
   * A utiliser conjointement avec setText() pour d�finir le texte du message. La m�thode setMessage() permet de faire les deux � la fois.
   */
  public void setImportanceMessage(EnumImportanceMessage pImportanceMessage) {
    if (pImportanceMessage != null) {
      importanceMessage = pImportanceMessage;
    }
    else {
      importanceMessage = EnumImportanceMessage.NORMAL;
    }
    rafraichir();
  }
}
