/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.radiobouton;

import java.awt.Dimension;

import javax.swing.JRadioButton;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Bouton radio avec la pr�sentation standard.
 *
 * Ce composant remplace JRadioButton. Il est � utiliser pour tous les boutons radios des �crans.
 * 
 * Caract�ristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 */
public class SNRadioButton extends JRadioButton {
  private final Dimension TAILLE_MINIMUM = new Dimension(150, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  
  /**
   * Constructeur.
   */
  public SNRadioButton() {
    super();
    setOpaque(false);
    setMinimumSize(TAILLE_MINIMUM);
    setPreferredSize(TAILLE_MINIMUM);
    setFont(SNCharteGraphique.POLICE_STANDARD);
  }
}
