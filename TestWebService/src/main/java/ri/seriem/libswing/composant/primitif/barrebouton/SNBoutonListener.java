/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.barrebouton;

import java.util.EventListener;

import ri.seriem.libswing.composant.primitif.bouton.SNBouton;

/**
 * Interface pour recevoir les �v�nements li�s aux boutons.
 */
public interface SNBoutonListener extends EventListener {
  /**
   * Appel� lorsqu'un bouton est cliqu�.
   */
  public void traiterClicBouton(SNBouton pSNBouton);
}
