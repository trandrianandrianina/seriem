/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.bouton;

import java.awt.Color;
import java.awt.Font;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Cat�gorie de bouton (ne pas ajouter de nouvelles valeurs dans cette �num�ration sans en discuter avec l'�quipe).
 * 
 * La cat�gorie de boutons permet de regrouper les boutons en fonction de leur r�le et de d�finir leurs aspect. Le r�le des c�t�gories est
 * d�crit ci-dessous.
 * 
 * Description des cat�gories :
 * - Standard : Ce sont les boutons ordinaires. La majorit� des boutons doit �tre de cette cat�gorie. Leur courleur est orange.
 * - Validation : La cat�gorie validation est r�serv�e pour toutes les op�rations permettant de confirmer une op�ration, valider une
 * modification ou lancer une �dition. Leur couleur est verte.
 * - Annulation : La cat�gorie annulation est r�serv�e aux op�rations permettant d'annuler une modification en cours ou tout simplement
 * de quitter un �cran sans faire de modification (fermer la session ou retourner au r�sultat de la recherche). Leur couleur est rouge.
 * - Image : Ce sont des boutons constitu�s d'une image.
 */
public enum EnumCategorieBouton {
  STANDARD("Standard", SNCharteGraphique.COULEUR_BOUTON_ORANGE, SNCharteGraphique.POLICE_BOUTON),
  VALIDATION("Validation", SNCharteGraphique.COULEUR_BOUTON_VERT, SNCharteGraphique.POLICE_BOUTON),
  ANNULATION("Annulation", SNCharteGraphique.COULEUR_BOUTON_ROUGE, SNCharteGraphique.POLICE_BOUTON),
  IMAGE("Image", SNCharteGraphique.COULEUR_BOUTON_ORANGE, SNCharteGraphique.POLICE_BOUTON);
  
  private final String libelle;
  private final Color couleurFond;
  private final Font police;
  
  /**
   * Constructeur.
   */
  private EnumCategorieBouton(String pLibelle, Color pCouleurFond, Font pPolice) {
    libelle = pLibelle;
    couleurFond = pCouleurFond;
    police = pPolice;
  }
  
  /**
   * Retourner le code associ� dans une cha�ne de caract�re.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Libell�.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Couleur de fond des boutons associ�s � cette cat�gorie.
   */
  public Color getCouleurFond() {
    return couleurFond;
  }
  
  /**
   * Police des boutons associ�s � cette cat�gorie.
   */
  public Font getPolice() {
    return police;
  }
}
