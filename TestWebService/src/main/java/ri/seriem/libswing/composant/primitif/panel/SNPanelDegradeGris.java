/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.panel;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Panneau pr�sentant un fond avec un d�grad� de couleurs gris.
 * Ce panneau est utilis� comme trame de fond pour tous les �crans RPG v2 (c'est le gris qui entoure la partie jaune de l'�cran).
 * Ce coutour gris autour des �crans n'est plus souhait� et nous l'enlevons progressivement.
 */
public class SNPanelDegradeGris extends SNPanelDegrade {
  /**
   * Constructeur.
   */
  public SNPanelDegradeGris() {
    super(DIRECTION_VERTICAL, SNCharteGraphique.COULEUR_FOND_DEGRADE_GRIS_FONCE);
  }
}
