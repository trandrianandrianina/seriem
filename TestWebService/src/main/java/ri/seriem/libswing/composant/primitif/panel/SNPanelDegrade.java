/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.panel;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;

import javax.swing.JPanel;

import ri.seriem.libcommun.outils.image.OutilImage;
import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Panneau pr�sentant un fond avec un d�grad� de couleurs.
 *
 * Le d�grad� va d'une couleur dite fonc�e � une couleur dite claire. La oculeur claire est calcul�e � partir de la couleur fonc�e fournie
 * au composant. La direction du d�grad� peut �tre horizontale ou verticale :
 * - Si la direction est horizontale, le d�grad� va de la couleur fonc�e � gauche vers la couleur claire � droite.
 * - Si la direction est verticale, le d�grad� va de la couleur fonc�e en bas � la couleur claire en haut.
 *
 * Composants alternatifs :
 * - SNPanelDegradeGris : Bien que SNPanelDegrade a des valeurs par d�faut grises (en cas de param�tres invalides), il ne faut pas compter
 * dessus. Pour �tre certains d'avoir un d�grad� gris, utiliser SNPanelDegradeGris.
 */
public class SNPanelDegrade extends JPanel {
  // Constantes
  public static final int DIRECTION_HORIZONTAL = 0;
  public static final int DIRECTION_VERTICAL = 1;
  private static final float POURCENTAGE_ECLAIRCISSEMENT = 0.90f;
  
  // Variables
  private int direction = DIRECTION_HORIZONTAL;
  private Color couleurFoncee = SNCharteGraphique.COULEUR_FOND_DEGRADE_GRIS_FONCE;
  
  /**
   * Constructeur par d�faut.
   * La direction et les couleurs par d�faut sont alors utilis�es (horizontal et gris).
   */
  public SNPanelDegrade() {
    super();
  }
  
  /**
   * Constructeur avec pr�cision de la direction et des couleurs claires et fonc�s du d�grad�.
   */
  public SNPanelDegrade(int pDirection, Color pCouleurFoncee) {
    super();
    setDirection(pDirection);
    setCouleurFoncee(pCouleurFoncee);
  }
  
  /**
   * Dessiner le composant.
   */
  @Override
  protected void paintComponent(Graphics graphics) {
    super.paintComponent(graphics);
    
    // R�cup�rer les bords du panneau
    Rectangle bounds = getBounds();
    
    // Calculer la couleur claire
    Color couleurClaire = OutilImage.eclaircirCouleur(couleurFoncee, POURCENTAGE_ECLAIRCISSEMENT);
    
    // Construire le gradient de couleur
    Paint gradientPaint = null;
    if (direction == DIRECTION_HORIZONTAL) {
      gradientPaint = new GradientPaint(bounds.x, 0, couleurFoncee, bounds.x + bounds.width, 0, couleurClaire);
    }
    else {
      gradientPaint = new GradientPaint(0, bounds.y + bounds.height, couleurFoncee, 0, bounds.y, couleurClaire);
    }
    
    // Colorier le panneau en d�rgad�
    Graphics2D graphics2D = (Graphics2D) graphics;
    graphics2D.setPaint(gradientPaint);
    graphics2D.fillRect(0, 0, bounds.width, bounds.height);
  }
  
  /**
   * Couleur fonc�e du d�grad�.
   * La couleur claire est d�termin�e � partir de la couleur fonc�e.
   */
  public Color getCouleurFoncee() {
    return couleurFoncee;
  }
  
  /**
   * Changer la couleur fonc�e du d�grad�.
   * On met gris fonc� par d�faut si le param�tre est null.
   * @param pCouleurFoncee Couleur fonc�e du d�grad�.
   */
  public void setCouleurFoncee(Color pCouleurFoncee) {
    if (pCouleurFoncee != null) {
      couleurFoncee = pCouleurFoncee;
    }
    else {
      couleurFoncee = SNCharteGraphique.COULEUR_FOND_DEGRADE_GRIS_FONCE;
    }
    
    repaint();
  }
  
  /**
   * Direction du d�grad� (horizontal ou vertical).
   */
  public int getDirection() {
    return direction;
  }
  
  /**
   * Changer la direction du d�grad� (horizontal ou vertical).
   * Le sens horizontal est pris par d�faut si le param�tre est invalide.
   */
  public void setDirection(int pDirection) {
    if (pDirection == DIRECTION_VERTICAL) {
      direction = DIRECTION_VERTICAL;
    }
    else {
      direction = DIRECTION_HORIZONTAL;
    }
    
    repaint();
  }
}
