/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.bouton;

import java.awt.event.KeyEvent;

/**
 * Bouton pr�-configur� (ne pas ajouter de nouvelles valeurs dans cette �num�ration sans en discuter avec l'�quipe).
 *
 * Cette �num�ration contient les boutons standardis�s. Chaque bouton a un libell� et une fonction bien pr�cise. Afin d'avoir un logiciel
 * homog�ne, il est fortement recommand� d'utiliser ces boutons lorsque la fonction propos� par le bouton correspond au besoin de
 * l'�cran.
 *
 * Le premier param�tre est la cat�gorie de boutons (validation, annulation, standard ou image). Le second param�tre est le libell� du
 * bouton pour les boutons textuels ou le nom de l'image du bouton pour les boutons images. Le trois�me param�tre est le mn�monique
 * associ� au bouton, soit sous forme de caract�re (char) soit sous forme de code unicode (int).
 *
 * Boutons de validation :
 * - VALIDER : valider une modification ou une action en cours.
 * - EDITER : comme "VALIDER" mais suivi de la g�n�ration d'une �dition.
 * - CONSULTER : afficher l'�l�ment s�lectionn� (�cran liste/d�tail).
 * - CONTINUER : passer � la page suivante dans un assistant.
 * - CONNECTER : se connecter au logiciel.
 * - RECHERCHER : lancer une recherche.
 *
 * Boutons d'annulation :
 * - ANNULER : annuler une modification ou une action en cours.
 * - FERMER : fermer un �cran sur lequel aucune saisie ou action n'a �t� effectu� (rien � valider ou annuler).
 * - RETOURNER_RECHERCHE : comme "FERMER" mais avec retour au r�sultat de la recherche (�cran liste/d�tail).
 * - QUITTER : comme "FERMER" mais cela ferme la session en cours (l'onglet de la barre de session dispara�t).
 *
 * Boutons standard :
 * - INITIALISER RECHERCHE : effacer les crit�res de recherche et la liste de r�sultats.
 *
 * Boutons image :
 * - NAVIGATION_PRECEDENT : se d�placer sur le r�sultat pr�c�dent d'une recherche.
 * - NAVIGATION_SUIVANT : se d�placer sur le r�sultat suivant d'une recherche.
 */
public enum EnumBouton {
  VALIDER(EnumCategorieBouton.VALIDATION, "Valider", KeyEvent.VK_ENTER),
  EDITER(EnumCategorieBouton.VALIDATION, "Editer", KeyEvent.VK_ENTER),
  CONSULTER(EnumCategorieBouton.VALIDATION, "Consulter", KeyEvent.VK_ENTER),
  CONTINUER(EnumCategorieBouton.VALIDATION, "Continuer", KeyEvent.VK_ENTER),
  CONNECTER(EnumCategorieBouton.VALIDATION, "Connecter", KeyEvent.VK_ENTER),
  LIGNES(EnumCategorieBouton.VALIDATION, "Aller lignes", KeyEvent.VK_ENTER),
  PIED(EnumCategorieBouton.VALIDATION, "Aller pied", KeyEvent.VK_ENTER),
  RECHERCHER(EnumCategorieBouton.VALIDATION, "Rechercher", 'R'),
  ANNULER(EnumCategorieBouton.ANNULATION, "Annuler", KeyEvent.VK_BACK_SPACE),
  FERMER(EnumCategorieBouton.ANNULATION, "Fermer", KeyEvent.VK_BACK_SPACE),
  RETOURNER_RECHERCHE(EnumCategorieBouton.ANNULATION, "Retourner recherche", KeyEvent.VK_BACK_SPACE),
  ALLER_ECRAN_PRECEDENT(EnumCategorieBouton.ANNULATION, "Retourner �cran pr�c�dent", KeyEvent.VK_BACK_SPACE),
  RETOURNER_ENTETE(EnumCategorieBouton.ANNULATION, "Retourner ent�te", KeyEvent.VK_BACK_SPACE),
  RETOURNER_LIGNES(EnumCategorieBouton.ANNULATION, "Retourner lignes", KeyEvent.VK_BACK_SPACE),
  QUITTER(EnumCategorieBouton.ANNULATION, "Quitter", 'Q'),
  INITIALISER_RECHERCHE(EnumCategorieBouton.STANDARD, "Initialiser recherche", 'I'),
  NAVIGATION_PRECEDENT(EnumCategorieBouton.IMAGE, "fleche_gauche", KeyEvent.VK_LEFT),
  NAVIGATION_SUIVANT(EnumCategorieBouton.IMAGE, "fleche_droite", KeyEvent.VK_RIGHT);
  
  private final EnumCategorieBouton categorie;
  private final String texte;
  private final Character mnemonique;
  
  /**
   * Constructeur avec un char comme mn�monique.
   */
  EnumBouton(EnumCategorieBouton pCategorie, String pTexte, Character pMnemonique) {
    categorie = pCategorie;
    texte = pTexte;
    mnemonique = pMnemonique;
  }
  
  /**
   * Constructeur avec un code unicode comme mn�monique.
   */
  EnumBouton(EnumCategorieBouton pCategorie, String pTexte, int pMnemonique) {
    categorie = pCategorie;
    texte = pTexte;
    mnemonique = (char) pMnemonique;
  }
  
  /**
   * Cat�gorie du bouton : standard, validation, annulation, image.
   */
  public EnumCategorieBouton getCategorie() {
    return categorie;
  }
  
  /**
   * Texte associ� au bouton.
   * Pour les boutons contenant du texte, c'est le texte du libell�. Pour les boutons de la cat�gorie image, c'est le nom de l'image.
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * Mn�monique associ� au bouton.
   */
  public Character getMnemonique() {
    return mnemonique;
  }
  
  /**
   * Retourner le texte du bouton.
   */
  @Override
  public String toString() {
    return String.valueOf(texte);
  }
}
