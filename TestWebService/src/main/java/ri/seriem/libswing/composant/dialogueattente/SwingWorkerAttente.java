/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.dialogueattente;

import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.SwingWorker;

import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libswing.composant.dialoguestandard.dialogueerreur.DialogueErreur;
import ri.seriem.libswing.moteur.mvc.InterfaceSoumission;

/**
 * Cette classe permet d'afficher une boite de dialogue pendant un traitement long, une fois le traitement termin� la boite de dialogue se
 * ferme automatiquement.
 * On peut personnaliser le message � afficher pendant le traitement.
 */
public class SwingWorkerAttente extends SwingWorker<Void, Void> {
  // Variables
  private ModeleDialogueAttente modeleAttente = null;
  private DialogueAttente vueAttente = null;
  private InterfaceSoumission objetMetier = null;
  
  /**
   * Constructeur.
   */
  private SwingWorkerAttente(Window pParent, InterfaceSoumission pObjet) {
    objetMetier = pObjet;
    initialiserComposants();
  }
  
  /**
   * Initialise les composants n�cessaires.
   */
  private void initialiserComposants() {
    modeleAttente = new ModeleDialogueAttente();
    vueAttente = new DialogueAttente(modeleAttente);
    
    // D�tection de l'arr�t du traitement
    addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("state")) {
          if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
            vueAttente.cacher();
          }
        }
      }
    });
  }
  
  /**
   * Traitement � effetcuer en t�che de fond.
   */
  @Override
  protected Void doInBackground() throws Exception {
    try {
      // Nommer la thread pour la rep�rer dans les traces
      Thread.currentThread().setName("SwingWorkerAttente.doInBackground");
      if (objetMetier != null) {
        objetMetier.executer();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
    return null;
  }
  
  /**
   * Affiche la boite de dialogue et d�marre le traitement en tache de fond.
   */
  public static void executer(Window pParent, InterfaceSoumission pObjet, String pMessage) {
    SwingWorkerAttente attente = new SwingWorkerAttente(pParent, pObjet);
    
    // Ex�cute doit �tre plac�e avant l'affichage car la boite de dialogue est modale
    attente.execute();
    // On personnalise le message et on affiche
    attente.getModeleAttente().modifierMessage(pMessage);
    attente.getVueAttente().afficher();
  }
  
  /**
   * Affiche la boite de dialogue et d�marre le traitement en tache de fond.
   */
  public static void executer(Object pParent, InterfaceSoumission pObjet, String pMessage) {
    if (pParent instanceof Window) {
      executer((Window) pParent, pObjet, pMessage);
    }
    else {
      throw new MessageErreurException("L'objet pParent n'est pas de type Window.");
    }
  }
  
  // -- Accesseurs
  
  public ModeleDialogueAttente getModeleAttente() {
    return modeleAttente;
  }
  
  public DialogueAttente getVueAttente() {
    return vueAttente;
  }
}
