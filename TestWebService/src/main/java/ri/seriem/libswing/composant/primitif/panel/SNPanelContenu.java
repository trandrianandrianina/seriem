/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.panel;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Panneau de contenu d'un �cran ou d'une bo�te de dialogue.
 *
 * Le panneau contenu est utilis� comme second panneau des �crans et bo�tes de dialogue.(apr�s SNPanelFond). Il contient l'ensemble des
 * composants de l'�cran � l'exception du bandeau sup�rieur et de la barre de boutons (SNBarreBouton). Il comporte une marge pour s�parer
 * les composants des bords de l'�cran.
 *
 * Caract�ristiques graphiques :
 * - Transparent.
 * - Pas de cadre et pas de titre.
 * - Marge externe sur le pourtour.
 */
public class SNPanelContenu extends JPanel {
  
  /**
   * Constructeur standard.
   */
  public SNPanelContenu() {
    super();
    setOpaque(false);
    setBorder(new EmptyBorder(SNCharteGraphique.MARGE_EXTERNE, SNCharteGraphique.MARGE_EXTERNE, SNCharteGraphique.MARGE_EXTERNE,
        SNCharteGraphique.MARGE_EXTERNE));
    
  }
  
}
