/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.saisie;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Highlighter;

import ri.seriem.libswing.moteur.SNCharteGraphique;
import ri.seriem.libswing.outil.clipboard.Clipboard;
import ri.seriem.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Saisie ou consultation d'un bloc de texte alphanum�rique (caract�res autoris�es : aplhapnum�riques).
 *
 * Ce composant remplace JTextArea. Il est � utiliser pour tous les saisies de bloc de textes aplhanum�riques. Il est possible de
 * contrainte le nombre de caract�res saisissables via la m�thode setLongueur().
 *
 * Ce composant est �quip� d'un menu contextuel qui apparait avec un clic droit de la souris et permet de copier le texte qu'il contient
 * dans le presse papier.
 *
 * Il existe aussi :
 * - SNTexte pour les textes.
 * - SNNombreEntier pour les nombres entiers.
 * - SNNombreDecimal pour les nombres d�cimaux.
 * - SNIdentifiant pour les identifiants.
 *
 * Caract�ristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 * - Alignement � gauche.
 */
public class SNBlocTexte extends JTextArea {
  private final Dimension TAILLE_MINIMUM = new Dimension(100, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  private final JPopupMenu menuContextuel = new JPopupMenu("Copie");
  private JMenuItem copie = new JMenuItem("Copier");
  private int longueur = 0;
  
  public SNBlocTexte() {
    super();
    setOpaque(false);
    setMinimumSize(TAILLE_MINIMUM);
    setPreferredSize(TAILLE_MINIMUM);
    setFont(SNCharteGraphique.POLICE_STANDARD);
    
    // Menu contextuel de copie
    copie.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // Si une partie du texte du composant est surlign�e (s�lectionn�e) c'est elle que l'on copie
        Highlighter surlignement = getHighlighter();
        if (surlignement.getHighlights() != null && surlignement.getHighlights().length > 0) {
          String texteSelectionne =
              getText().substring(surlignement.getHighlights()[0].getStartOffset(), surlignement.getHighlights()[0].getEndOffset());
          Clipboard.envoyerTexte(texteSelectionne);
        }
        // Sinon on copie tout le texte
        else {
          Clipboard.envoyerTexte(getText());
        }
      }
    });
    menuContextuel.add(copie);
    setComponentPopupMenu(menuContextuel);
    
  }
  
  @Override
  public void setEnabled(boolean pEnabled) {
    setEditable(pEnabled);
    setFocusable(pEnabled);
    if (!pEnabled) {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
    }
    else {
      setBackground(Color.WHITE);
    }
  }
  
  /**
   * Nombre de caract�res autoris�s pour la saisie.
   * @return Nombre de caract�res.
   */
  public int getLongueur() {
    return longueur;
  }
  
  /**
   * Modifier le nombre de caract�res autoris�s pour la saisie.
   * @param pLongueur Nombre de caract�res (ne peut �tre inf�rieur � 1).
   */
  public void setLongueur(int pLongueur) {
    setLongueur(pLongueur, true);
  }
  
  /**
   * Modifier le nombre de caract�res autoris�s pour la saisie.
   * @param pLongueur Nombre de caract�res (ne peut �tre inf�rieur � 1).
   * @param pAlphanumerique true si alphanum�rique, false si num�rique uniquement.
   */
  public void setLongueur(int pLongueur, boolean pAlphanumerique) {
    // Contr�ler le param�tre
    if (pLongueur < 1) {
      pLongueur = 1;
    }
    
    // Formater la saisie
    SaisieDefinition saisieDefinition = new SaisieDefinition(pLongueur, false, pAlphanumerique, false);
    ((AbstractDocument) this.getDocument()).setDocumentFilter(saisieDefinition);
  }
  
}
