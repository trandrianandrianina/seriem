/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.dialogueattente;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.seriem.libswing.moteur.SNCharteGraphique;
import ri.seriem.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la bo�te de dialogue "Message d'information".
 * Elle permet d'afficher des messages simples � l'utilisateur. Celui-ci peut uniquement cliquer sur un bouton "Fermer"
 * pour fermer
 * la bo�te de dialogue.
 */
public class DialogueAttente extends AbstractVueDialogue<ModeleDialogueAttente> {
  /**
   * Contructeur.
   */
  public DialogueAttente(ModeleDialogueAttente pModele) {
    super(pModele);
  }
  
  /**
   * Afficher la bo�te de dialogue avec un message.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficher(String pMessage) {
    ModeleDialogueAttente modele = new ModeleDialogueAttente();
    modele.modifierMessage(pMessage);
    DialogueAttente vue = new DialogueAttente(modele);
    vue.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    vue.afficher();
  }
  
  /**
   * Initialiser les composants graphiques de la bo�te de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }
  
  /**
   * Rafraichissement des donn�es � l'�cran.
   */
  @Override
  public void rafraichir() {
    // Lire le message du mod�le
    String message = getModele().getMessage();
    if (message == null) {
      message = "";
    }
    
    // Formater le message utilisateur pour l'afficher en HTML.
    message = message.replaceAll("<", "&lt;");
    message = message.replaceAll(">", "&gt;");
    message = message.replaceAll("\\n", "<br>");
    message = "<html><div style='text-align: center;'>" + message + "</div></html>";
    lbMessageUtilisateur.setText(message);
  }
  
  // M�thodes �v�nementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    lbMessageUtilisateur = new JLabel();
    pnlControle = new JPanel();
    
    // ======== this ========
    setTitle("Message d'attente");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(null);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(238, 238, 210));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(null);
      
      // ---- lbMessageUtilisateur ----
      lbMessageUtilisateur.setFont(lbMessageUtilisateur.getFont().deriveFont(lbMessageUtilisateur.getFont().getStyle() | Font.BOLD,
          lbMessageUtilisateur.getFont().getSize() + 3f));
      lbMessageUtilisateur.setBackground(new Color(238, 238, 210));
      lbMessageUtilisateur.setBorder(new EmptyBorder(10, 10, 10, 10));
      lbMessageUtilisateur.setAutoscrolls(false);
      lbMessageUtilisateur.setHorizontalAlignment(SwingConstants.CENTER);
      lbMessageUtilisateur.setHorizontalTextPosition(SwingConstants.CENTER);
      lbMessageUtilisateur.setName("lbMessageUtilisateur");
      pnlContenu.add(lbMessageUtilisateur);
      lbMessageUtilisateur.setBounds(10, 28, 590, 145);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < pnlContenu.getComponentCount(); i++) {
          Rectangle bounds = pnlContenu.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = pnlContenu.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        pnlContenu.setMinimumSize(preferredSize);
        pnlContenu.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(pnlContenu);
    pnlContenu.setBounds(0, 0, 610, 205);
    
    // ======== pnlControle ========
    {
      pnlControle.setPreferredSize(new Dimension(296, 65));
      pnlControle.setBackground(new Color(238, 238, 210));
      pnlControle.setName("pnlControle");
      pnlControle.setLayout(null);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < pnlControle.getComponentCount(); i++) {
          Rectangle bounds = pnlControle.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = pnlControle.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        pnlControle.setMinimumSize(preferredSize);
        pnlControle.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(pnlControle);
    pnlControle.setBounds(0, 205, 610, 60);
    
    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < contentPane.getComponentCount(); i++) {
        Rectangle bounds = contentPane.getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = contentPane.getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      contentPane.setMinimumSize(preferredSize);
      contentPane.setPreferredSize(preferredSize);
    }
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private JLabel lbMessageUtilisateur;
  private JPanel pnlControle;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
