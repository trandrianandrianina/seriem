/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.bouton;

import java.awt.Dimension;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libcommun.outils.Trace;
import ri.seriem.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Bouton.
 *
 * Un bouton est d�finit par une cat�gorie, un texte et un mn�monique. La cat�gorie de boutons d�finit l'aspect g�n�ral du bouton
 * (validation, annulation, standard ou image). Le texte est le libell� du bouton pour les boutons textuels ou le nom de l'image
 * pour les boutons images. Le mn�monique est la touche de raccourcie associ�e au bouton, soit sous forme de caract�re (char),
 * soit sous forme de code unicode (int).
 *
 * Les boutons images n'ont pas de bord et ont un fond transparent. Seul l'image est visible sur l'�cran. Afin de fournir un ressenti �
 * l'utilisateur, il est n�cessaire de fournir deux images suppl�mentaires : une lorsque la souris passe au dessus du bouton et une
 * autre lorsque l'utilisateur clique sur le bouton. Cette classe recherche donc trois images (remplacer le mot "texte" par la valeur
 * fournie lors de la cr�ation du bouton) :
 * - /images/texte.png : l'image standard.
 * - /images/texte_rollover.png : l'image lorsque la souris est au dessus du bouton.
 * - /images/texte_pressed.png : l'image lorsque le bouton est enfonc�.
 */
public class SNBouton extends JButton {
  static public final Dimension TAILLE = new Dimension(SNCharteGraphique.LARGEUR_STANDARD_BOUTON, 50);
  
  private EnumBouton boutonPreconfigure = null;
  private EnumCategorieBouton categorie = null;
  private String libelle = null;
  private boolean contextuel = false;
  private String infobulle = null;
  private Integer index = null;
  private SNBarreBouton snBarreBouton = null;
  
  /**
   * Constructeur par d�faut.
   */
  public SNBouton() {
    super();
    rafraichir();
  }
  
  /**
   * Constructeur le plus complet d'un bouton.
   */
  public SNBouton(EnumCategorieBouton pCategorie, String pLibelle, Character pMnemonique, boolean pContextuel) {
    super();
    
    // Tester les param�tres obligatoires
    if (pCategorie == null) {
      throw new MessageErreurException("La cat�gorie du bouton est invalide.");
    }
    if (pLibelle == null) {
      throw new MessageErreurException("Le libell� du bouton est invalide.");
    }
    
    // Nommer le bouton (pour QFTest), on utilise le libell� non transform�
    setName(pLibelle);
    
    // Configurer le bouton
    categorie = pCategorie;
    libelle = pLibelle;
    setMnemonicSansRafraichir(pMnemonique);
    contextuel = pContextuel;
    rafraichir();
  }
  
  /**
   * Constructeur d'un bouton qui accepte un code unicode comme mn�monique.
   */
  public SNBouton(EnumCategorieBouton pCategorie, String pTexte, int pMnemonique, boolean pContextuel) {
    this(pCategorie, pTexte, Character.valueOf((char) pMnemonique), pContextuel);
  }
  
  /**
   * Constructeur pour un bouton standard.
   * Un bouton standard a une couleur de fond orange. Il est forc�ment associ� � un mn�monique fournit en param�tre.
   */
  public SNBouton(String pLibelle, Character pMnemonique, boolean pContextuel) {
    this(EnumCategorieBouton.STANDARD, pLibelle, pMnemonique, pContextuel);
  }
  
  /**
   * Constructeur pour un bouton pr�-configur�.
   * Les boutons pr�-configr�s sont les boutons de type validation ou annulation. Ils ont des libell�s standardis�s.
   */
  public SNBouton(EnumBouton pBoutonPreconfigure, boolean pContextuel) {
    setBoutonPreconfigure(pBoutonPreconfigure, pContextuel);
  }
  
  /**
   * Constructeur pour un bouton pr�-configur�.
   * Les boutons pr�-configr�s sont les boutons de type validation ou annulation. Ils ont des libell�s standardis�s.
   */
  public SNBouton(EnumBouton pBoutonPreconfigure) {
    setBoutonPreconfigure(pBoutonPreconfigure, false);
  }
  
  /**
   * Rafra�chir le bouton.
   */
  private void rafraichir() {
    // Afficher le curseur "main" lorsque la souris passe au dessus du bouton
    setCursor(SNCharteGraphique.CURSEUR_MAIN);
    
    // D�finir la couleur de fond
    if (categorie != null) {
      setBackground(categorie.getCouleurFond());
    }
    else {
      setBackground(SNCharteGraphique.COULEUR_BOUTON_ORANGE);
    }
    
    // D�finir la police
    if (categorie != null) {
      setFont(categorie.getPolice());
    }
    else {
      setFont(SNCharteGraphique.POLICE_BOUTON);
    }
    
    // D�finir l'infobulle qui rapelle le mn�monique
    if (getMnemonic() != 0) {
      setToolTipText(genererInfobulle(getMnemonic()));
    }
    else {
      setToolTipText(null);
    }
    
    // Traiter les boutons image et texte � part
    if (categorie != null && categorie == EnumCategorieBouton.IMAGE) {
      // D�terminer les noms des fichiers images
      String fichierStandard = "/images/" + libelle + ".png";
      String fichierRollover = "/images/" + libelle + "_rollover.png";
      String fichierPressed = "/images/" + libelle + "_pressed.png";
      
      try {
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(fichierStandard));
        setIcon(imageIcon);
        
        // D�finir la taille bu bouton en focntion de la taille de l'image
        setMinimumSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        
        // Rendre le bouton transparent
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);
        
        // Charger l'image du rollover
        try {
          setRolloverIcon(new ImageIcon(getClass().getResource(fichierRollover)));
        }
        catch (Exception ex) {
          Trace.alerte("Erreur lors du chargement de l'image du bouton : " + fichierRollover);
        }
        
        // Charger l'image du bouton press�
        try {
          setPressedIcon(new ImageIcon(getClass().getResource(fichierPressed)));
        }
        catch (Exception ex) {
          Trace.alerte("Erreur lors du chargement de l'image du bouton : " + fichierPressed);
        }
      }
      catch (Exception ex) {
        Trace.alerte("Erreur lors du chargement de l'image du bouton : " + fichierStandard);
        // Le nom de l'image sert d'alternative si on ne trouve pas l'image
        setText(libelle);
      }
    }
    else {
      // Initialiser le libell� initial
      String libelleHTML = libelle;
      
      // D�finir le libell� affich� dans le bouton en fonction du mn�monique
      if (getMnemonic() != 0) {
        libelleHTML = soulignerMnemonique(libelleHTML, getMnemonic());
        libelleHTML = libelleHTML.replace("\n", "<br>");
        libelleHTML = "<html><center>" + libelleHTML + "</center></html>";
      }
      else {
        libelleHTML = "<html><center>" + libelleHTML + "</center></html>";
      }
      setText(libelleHTML);
      
      // D�finir la taille bu bouton, fixe pour un bouton textuel
      setMinimumSize(TAILLE);
      setPreferredSize(TAILLE);
      setMaximumSize(TAILLE);
    }
  }
  
  /**
   * Souligner la lettre correspondant au mn�monique dans le texte.
   * Le mn�monique est d'abord recherch� dans la premi�re de chaque mot puis il est recherch� sur l'int�gralit� du texte.
   */
  private String soulignerMnemonique(String pTexte, int pMnemonique) {
    // Ne rien faire si le texte ou le mn�monique fourni est null
    if (pTexte == null || pMnemonique == 0) {
      return pTexte;
    }
    
    // Rechercher le mn�monique � la premi�re lettre de chaque mot
    int indexMnemonique = -1;
    for (int i = 0; i < pTexte.length(); i++) {
      if (Character.toUpperCase(pTexte.charAt(i)) == Character.toUpperCase(pMnemonique)) {
        if (i == 0 || pTexte.charAt(i - 1) == ' ' || pTexte.charAt(i - 1) == '\'') {
          indexMnemonique = i;
          break;
        }
      }
    }
    
    // Rechercher le mn�monique n'importe o� dans le texte
    if (indexMnemonique == -1) {
      indexMnemonique = pTexte.toUpperCase().indexOf(Character.toUpperCase(pMnemonique));
    }
    
    // Entourer la lettre trouv�e par <u> et </u>
    String texte = pTexte;
    if (indexMnemonique >= 0 && indexMnemonique <= pTexte.length()) {
      texte = pTexte.substring(0, indexMnemonique) + "<u>" + pTexte.charAt(indexMnemonique) + "</u>"
          + pTexte.substring(indexMnemonique + 1, pTexte.length());
    }
    
    return texte;
  }
  
  /**
   * G�n�rer le texte d'infobulle qui rappelle le raccourci utilisable pour d�clencher le bouton.
   */
  private String genererInfobulle(int pMnemonique) {
    if (pMnemonique == 0) {
      return "";
    }
    switch (pMnemonique) {
      case KeyEvent.VK_ENTER:
        return "Alt + Entr�e";
      case KeyEvent.VK_BACK_SPACE:
        return "Alt + Retour arri�re";
      case KeyEvent.VK_SPACE:
        return "Alt + Espace";
      case KeyEvent.VK_LEFT:
        return "Alt + Fl�che gauche";
      case KeyEvent.VK_RIGHT:
        return "Alt + Fl�che doite";
      case KeyEvent.VK_UP:
        return "Alt + Fl�che sup�rieure";
      case KeyEvent.VK_DOWN:
        return "Alt + Fl�che inf�rieure";
      case KeyEvent.VK_INSERT:
        return "Alt + Insert";
      case KeyEvent.VK_DELETE:
        return "Alt + Suppr";
      case KeyEvent.VK_PAGE_UP:
        return "Alt + Page sup�rieure";
      case KeyEvent.VK_PAGE_DOWN:
        return "Alt + Page inf�rieure";
      case KeyEvent.VK_HOME:
        return "Alt + D�but";
      case KeyEvent.VK_END:
        return "Alt + Fin";
      default:
        // Le cast en char est n�cessaire pour avoir la lettre et pas le code de la lettre
        return "Alt + " + (char) Character.toUpperCase(pMnemonique);
    }
  }
  
  /**
   * Tester si le bouton est actif ou non.
   */
  public boolean isActif() {
    return isVisible() && isEnabled();
  }
  
  /**
   * Activer un bouton.
   * Les boutons des cat�gories standards et annulation sont masqu�s. Par d�faut, les boutons des cat�gories validation et image sont
   * gris�s sauf si l'indicateur pour forcer le masquage est � true.
   */
  public void activer(boolean pVisible, boolean forcerMasquage) {
    // Mettre le bouton visible et d�gris� s'il faut l'activer (on fait les deux pour que cela marche ind�pendemment de l'�tat de d�part)
    if (pVisible) {
      setVisible(true);
      setEnabled(true);
    }
    // Griser les boutons validation et images sauf si le masquage est forc�
    else if (!forcerMasquage && (categorie == EnumCategorieBouton.VALIDATION || categorie == EnumCategorieBouton.IMAGE)) {
      setVisible(true);
      setEnabled(false);
    }
    // Masquer les autres boutons
    else {
      setVisible(false);
      setEnabled(false);
    }
  }
  
  /**
   * R�cup�rer la description du bouton pr�configur� si ce bouton est un bouton pr�configur�.
   * Cela retourne une valeur null si ce n'est pas un bouton pr�configur�.
   */
  public EnumBouton getBoutonPreconfigure() {
    return boutonPreconfigure;
  }
  
  /**
   * Transformer ce bouton en bouton pr�configur�.
   */
  public void setBoutonPreconfigure(EnumBouton pBoutonPreconfigure) {
    setBoutonPreconfigure(pBoutonPreconfigure, false);
  }
  
  /**
   * Transformer ce bouton en bouton pr�configur�.
   */
  public void setBoutonPreconfigure(EnumBouton pBoutonPreconfigure, boolean pContextuel) {
    boutonPreconfigure = pBoutonPreconfigure;
    contextuel = pContextuel;
    
    // Mettre � jour les propri�t�s avec celles du bouton pr�configur�
    if (boutonPreconfigure != null) {
      // Mettre � jour la cat�gorie avec celle du bouton pr�configur�
      categorie = boutonPreconfigure.getCategorie();
      
      // Mettre � jour le libell� avec celui du bouton pr�configur�
      libelle = boutonPreconfigure.getTexte();
      
      // Mettre � jour le mn�monique avec celui du bouton pr�configur�
      setMnemonicSansRafraichir(boutonPreconfigure.getMnemonique());
      
      // Nommer le bouton (pour QFTest), on utilise le libell� non transform�
      setName(libelle);
    }
    else {
      // Mettre la cat�gorie standard si le bouton n'est plus un bouton pr�configur�
      categorie = EnumCategorieBouton.STANDARD;
    }
    
    // Rafra�chir le bouton
    rafraichir();
  }
  
  /**
   * Cat�gorie du bouton : standard, validation ou annulation.
   * Si le bouton est pr�configur�, la cat�gorie de la pr�configuration � la priorit� sur celle configur�e sur ce bouton.
   * Si aucune cat�gorie n'est d�finie, la cat�gorie de boutons STANDARD est retourn�e par d�faut.
   */
  public EnumCategorieBouton getCategorie() {
    return categorie;
  }
  
  /**
   * Texte brut pour cr�er le bouton, non transform�.
   * Si le bouton est pr�configur�, le libell� de la pr�configuration � la priorit� sur celui configur� sur ce bouton.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le texte du bouton.
   */
  public void setLibelle(String pLibelle) {
    // Ne rien modifier s'il s'agit d'un bouton pr�configur�
    if (boutonPreconfigure != null) {
      throw new MessageErreurException("Il n'est pas possible de modifier le libell� d'un bouton pr�configur�.");
    }
    
    // Stocker le texte non transform�
    libelle = pLibelle;
    
    // Nommer le bouton (pour QFTest), on utilise le libell� non transform�
    setName(pLibelle);
    
    // Rafra�chir le bouton
    rafraichir();
  }
  
  /**
   * Modifier le raccourci clavier associ� au bouton.
   */
  private void setMnemonicSansRafraichir(int pMnemonic) {
    // Appeler la m�thode parente pour modifier le mn�monique du bouton
    super.setMnemonic(pMnemonic);
  }
  
  /**
   * Modifier le raccourci clavier associ� au bouton.
   * Les mn�moniques doivent �tre en majuscules pour fonctionner. On utilise l'attribut standard des composants Java pour m�moriser le
   * mn�monique.
   */
  @Override
  public void setMnemonic(int pMnemonic) {
    // Ne rien modifier s'il s'agit d'un bouton pr�configur�
    if (boutonPreconfigure != null) {
      throw new MessageErreurException("Il n'est pas possible de modifier le mn�monique d'un bouton pr�configur�.");
    }
    
    // Appeler la m�thode parente pour modifier le mn�monique du bouton
    super.setMnemonic(pMnemonic);
    
    // Rafra�chir le bouton
    rafraichir();
  }
  
  /**
   * Indique si l'op�ration permise par ce bouton est �galement accessible via un menu contextuel.
   */
  public boolean isContextuel() {
    return contextuel;
  }
  
  /**
   * Tester si le bouton a le libell� fourni en param�tre.
   */
  public boolean isBouton(String pLibelle) {
    return getName().equals(pLibelle);
  }
  
  /**
   * Tester si le bouton est le bouton pr�-configur� fourni en param�tre.
   */
  public boolean isBouton(EnumBouton pEnumBouton) {
    if (boutonPreconfigure == null) {
      return false;
    }
    return boutonPreconfigure.equals(pEnumBouton);
  }
  
  /**
   * Index associ� au bouton (null si aucun index).
   */
  public Integer getIndex() {
    return index;
  }
  
  /**
   * Modifier l'index associ� au bouton.
   * Dans certains cas, il est utile d'associer un index au bouton afin de faciliter son identification ult�rieurement. C'est
   * notamment le cas pour les boutons o� le libell� est construit dynamiquement.
   */
  public void setIndex(Integer pIndex) {
    index = pIndex;
  }
  
  /**
   * R�cup�rer la barre de boutons dans laquelle est situ� le bouton.
   * @return null si la barre de boutons n'est dans aucune barre.
   */
  public SNBarreBouton getBarreBouton() {
    return snBarreBouton;
  }
  
  /**
   * Ajouter le bouton � une barre de boutons.
   */
  public void setBarreBouton(SNBarreBouton pSNBarreBouton) {
    // Tester si la valeur a chang�
    if (Constantes.equals(snBarreBouton, pSNBarreBouton)) {
      return;
    }
    
    // M�moriser l'ancienne barre de boutons
    SNBarreBouton snBarreBoutonPrecedente = snBarreBouton;
    
    // Renseigner la nouvelle valeur
    snBarreBouton = pSNBarreBouton;
    
    // S'enlever de l'ancienne barre de boutons.
    // ATTENTION : A faire apr�s avoir renseigner la nouvelle barre de boutons dans ce bouton pour �viter une boucle infinie.
    // En effet, la m�thode ici pr�sente va �tre rappel�e par la barre de boutons pour supprimer le lien dans les deux sens dans le cas o�
    // le bouton est enlev� � partir de la barre de boutons.
    if (snBarreBoutonPrecedente != null) {
      snBarreBoutonPrecedente.supprimerBouton(this);
    }
    
    // Ajouter le bouton � la nouvelle barre de boutons
    if (snBarreBouton != null) {
      snBarreBouton.ajouterBouton(this, true);
    }
  }
}
