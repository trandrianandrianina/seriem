/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.bouton;

import java.beans.BeanDescriptor;
import java.beans.PropertyDescriptor;

import ri.seriem.libswing.outil.bean.EnumProprieteBean;
import ri.seriem.libswing.outil.bean.SNBeanInfo;

/**
 * BeanInfo pour SNBouton.
 * G�re le composant graphique sous JFormDesigner.
 */
public class SNBoutonBeanInfo extends SNBeanInfo {
  
  {
    // Renseigner la classe JavaBean associ� � ce BeanInfo
    setBeanClass(SNBouton.class);
  }
  
  @Override
  public BeanDescriptor getBeanDescriptor() {
    // Indiquer si le composant est un container ou non (pour ne pas avoir � choisir lors de l'import sous JFormDesigner)
    BeanDescriptor beanDescriptor = new BeanDescriptor(getBeanClass());
    beanDescriptor.setValue("isContainer", Boolean.FALSE);
    return beanDescriptor;
  }
  
  @Override
  public PropertyDescriptor[] getPropertyDescriptors() {
    // Mettre en lecture seule toutes les propri�tes
    bloquerTout();
    
    // Activer les propri�t�s utiles de SNBouton
    setLectureSeule(EnumProprieteBean.BOUTON_PRECONFIGURE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.BOUTON_PRECONFIGURE);
    
    setLectureSeule(EnumProprieteBean.LIBELLE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.LIBELLE);
    
    setLectureSeule(EnumProprieteBean.MNEMONIC, false);
    setCategorieAvecNomComposant(EnumProprieteBean.MNEMONIC);
    
    // Propri�t� � cacher car valeur par d�faut non ma�trisable "Text"
    setVisible(EnumProprieteBean.TEXT, false);
    
    // Retourner la liste des propri�t�s configur�es
    return super.getPropertyDescriptors();
  }
}
