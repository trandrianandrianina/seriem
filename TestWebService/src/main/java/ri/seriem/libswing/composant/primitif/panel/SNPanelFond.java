/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.panel;

import java.awt.BorderLayout;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Panneau de fond standard des �crans et bo�tes de dialogue.
 *
 * C'est le premier panneau qui est pos� sur un �cran ou une bo�te de dialogue. Il a pour r�le de colorier le fond dans la couleur
 * standard de S�rie N. Il n'a aucun autre effet graphique (bord, marge, ...). Il est donc opaque tandis que les composants qui y sont
 * ins�r�s sont transparents afin de conserver la couleur de fond impos�e par ce composant.
 *
 * Il utilise un BorderLayout. Typiquement, la zone centrale contient un SNPanelContenu, la zone sup�rieure contient le bandeau de titre
 * et la zone inf�rieure contient la barre de bouton.
 *
 * Il est judicieux de nommer ce composant pnlFond pour le r�p�rer rapidement.
 *
 * Caract�ristiques graphiques :
 * - Fond opaque.
 * - Couleur de fond standard (jaune p�le).
 * - Pas de cadre et pas de titre.
 * - Pas de marge.
 * - BorderLayout.
 */
public class SNPanelFond extends SNPanel {
  /**
   * Constructeur standard.
   */
  public SNPanelFond() {
    super();
    setOpaque(true);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setLayout(new BorderLayout());
  }
}
