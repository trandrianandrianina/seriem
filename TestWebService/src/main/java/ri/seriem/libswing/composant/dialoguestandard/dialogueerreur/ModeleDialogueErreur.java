/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.dialoguestandard.dialogueerreur;

import ri.seriem.libcommun.outils.MessageErreurException;
import ri.seriem.libcommun.outils.Trace;
import ri.seriem.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;
import ri.seriem.libswing.outil.clipboard.Clipboard;

/**
 * Mod�le de la bo�te de dialogue d'affichage des messages d'erreur.
 */
public class ModeleDialogueErreur extends AbstractModeleDialogue {
  private Throwable throwable = null;
  private String message = null;
  private String messageDetaille = null;
  private boolean afficherDetail = false;
  
  /**
   * Constructeur.
   */
  public ModeleDialogueErreur() {
    super();
  }
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
  }
  
  /**
   * Modifier l'exception � afficher.
   *
   * Si une exception g�n�rique est pass�e en param�tre, c'est le message par d�faut qui est affich� ("Erreur technique").
   *
   * Si une exception MessageUtilisateurException est pass�e en param�tre, le message � afficher sera extrait de cette exception.
   * Il peut arriver que plusieurs MessageUtilisateurException soient empil�s, chacun avec un message diff�rent. Le premier message sera
   * le plus vague tandis que le dernier sera le plus pr�cis. ans ce cas, tous les messages sont affich�s dans la bo�te de dialogue,
   * s�par�s par un retour un ligne.
   *
   * @param pThrowable Exception contenant le message � afficher dans la bo�te de dialogue.
   */
  public void modifierException(Throwable pThrowable) {
    // V�rifier si la valeur a chang�
    if (pThrowable == throwable) {
      return;
    }
    
    // Mettre � jour la valeur
    throwable = pThrowable;
    
    // Construire le message utilisateur en recherchant les MessageUtilisateurException dans la pile d'exceptions
    message = "";
    for (Throwable cause = throwable; cause != null; cause = cause.getCause()) {
      if (cause instanceof MessageErreurException) {
        if (message.trim().isEmpty()) {
          message = cause.getLocalizedMessage();
        }
        else {
          message += "\n" + cause.getLocalizedMessage();
        }
      }
    }
    
    // Mettre un message par d�faut en cas d'absence de messsage
    if (message.trim().isEmpty()) {
      message = MessageErreurException.MESSAGE_ERREUR_TECHNIQUE;
    }
    
    // Tracer le message
    if (throwable != null) {
      Trace.erreur(throwable, message.replaceAll("(\\r|\\n)", " "));
    }
    else {
      Trace.info(message.replaceAll("(\\r|\\n)", " "));
    }
    
    // Construire le message d�taill� avec la stacktrace
    // S�curis� pour afficher la bo�te de dialogue y compris en cas d'erreur lors du traitement de l'exception
    messageDetaille = null;
    try {
      StringBuilder messageBuilder = new StringBuilder();
      
      // Afficher un r�sum� des messages en d�but de message
      for (Throwable cause = throwable; cause != null; cause = cause.getCause()) {
        messageBuilder.append(cause.getMessage() + "\n");
      }
      if (messageBuilder.length() > 0) {
        messageBuilder.append("\n");
      }
      
      // Afficher les messages empil�s dans les exceptions
      for (Throwable cause = throwable; cause != null; cause = cause.getCause()) {
        messageBuilder.append(cause.getClass().getSimpleName() + " : " + cause.getMessage() + "\n");
        for (StackTraceElement element : cause.getStackTrace()) {
          messageBuilder.append(element.toString());
          messageBuilder.append("\n");
        }
        messageBuilder.append("\n");
      }
      
      // Mettre � jour la valeur
      messageDetaille = messageBuilder.toString();
    }
    catch (Exception e) {
      // G�n�rer uniquement une trace car nous sommes dans la bo�te de dialogue d'affichage des erreurs !
      Trace.erreur(e, "Erreur dans la bo�te de dialogue d'affichage des messages d'erreur.");
    }
    
    // Rafra�chir la bo�te de dialogue
    rafraichir();
  }
  
  /**
   * Activer ou d�sactiver l'affichage du message d�taill�.
   *
   * @param pAfficherDetail false=afficher uniquement le message d'erreur, true = afficher le d�tail de l'exception.
   */
  public void modifierAfficherDetail(boolean pAfficherDetail) {
    afficherDetail = pAfficherDetail;
    rafraichir();
  }
  
  /**
   * Copier le message d�taill� dans le clipboard.
   */
  public void copierMessageDetaille() {
    if (messageDetaille != null && !messageDetaille.isEmpty()) {
      Clipboard.envoyerTexte(messageDetaille);
    }
    else if (message != null && !message.isEmpty()) {
      Clipboard.envoyerTexte(message);
    }
    else {
      Clipboard.envoyerTexte(MessageErreurException.MESSAGE_ERREUR_TECHNIQUE);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Exception associ�e au message.
   */
  public Throwable getThrowable() {
    return throwable;
  }
  
  /**
   * Message associ� � l'exception.
   */
  public String getMessage() {
    return message;
  }
  
  /**
   * Message d�taill� associ� � l'exception.
   * Ce message contient la stacktrace.
   */
  public String getMessageDetaille() {
    return messageDetaille;
  }
  
  /**
   * Indique s'il faut afficher le message d�taill�.
   */
  public boolean isAfficherDetail() {
    return afficherDetail;
  }
  
}
