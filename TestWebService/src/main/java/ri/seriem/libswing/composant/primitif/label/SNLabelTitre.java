/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.label;

import java.awt.Dimension;

import javax.swing.SwingConstants;

import ri.seriem.libswing.moteur.SNCharteGraphique;

/**
 * Libell� de titre.
 * 
 * Ce composant remplace JLabel et d�rive de SNLabel.
 * Il est � utiliser pour tous les libell�s contenant un titre de section ou un titre de tableau.
 * Il utilise la police d�di�e au titre, align�e vers la gauche. Le titre est �galement align� vers le bas pour accentuer son lien avec
 * le composant dont il est le titre, situ� en dessous, et augmenter la marge avec le composant situ� dessus.
 * Si le message est important il sera affich� en rouge sinon en noir.
 * 
 * D'autres composants libell�s sont disponibles :
 * - SNLabelChamp : pour les libell�s de champs standards.
 * - SNLabelMessage : pour les messages � mettre en �vidence en fonction de leur importance.
 * - SNLabelUnite : pour les unit�s associ�es � un champ.
 * 
 * Caract�ristiques graphiques :
 * - Fond transparent.
 * - Police pour les titres.
 * - Align� � droite.
 * - Align� vers le bas.
 */
public class SNLabelTitre extends SNLabel {
  private final Dimension TAILLE = new Dimension(150, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  
  /**
   * Constructeur.
   */
  public SNLabelTitre() {
    super();
    
    setFont(SNCharteGraphique.POLICE_TITRE);
    setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_EDITABLE);
    setOpaque(false);
    setMinimumSize(TAILLE);
    setPreferredSize(TAILLE);
    setMaximumSize(TAILLE);
    setHorizontalAlignment(SwingConstants.LEFT);
    setVerticalAlignment(SwingConstants.BOTTOM);
  }
}
