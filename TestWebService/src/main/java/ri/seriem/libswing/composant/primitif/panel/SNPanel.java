/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libswing.composant.primitif.panel;

import javax.swing.JPanel;

/**
 * Panneau avec la pr�sentation standard (sans cadre et sans titre).
 *
 * Le panneau standard permet de regrouper les composants. Il doit �tre utilis� � la place de JPanel lorsqu'il faut regrouper des
 * composants sans artefacts graphiques. Le fond est transparent pour laisse appara�tre la couleur de fond de la fen�tre.
 *
 * Caract�ristiques graphiques :
 * - Fond transparent.
 * - Pas de cadre et pas de titre.
 */
public class SNPanel extends JPanel {
  /**
   * Constructeur standard.
   */
  public SNPanel() {
    super();
    setOpaque(false);
  }
}
