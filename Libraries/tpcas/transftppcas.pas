//------------------------------------------------------------------------------
//--> Outil pour transf�rer des objets SAVF du PC vers l'AS/400
//--> Param�tres:
//-->     Nom du fichier SAVF (avec l'extension .savf)     [Facultatif]
//-->     Chemin source sur le PC                          [Facultatif]
//-->     Nom de la biblioth�que dest sur l'AS/400         [Facultatif]
//-->     Adresse IP de l'AS/400                           [Facultatif]
//-->     Titre que l'on souhaite donner � l'application   [Facultatif]
//-->     Ligne crypt�                                     [Facultatif]
//------------------------------------------------------------------------------
unit transftppcas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Psock, NMFtp, ComCtrls, FileCtrl, ZipMstr19;

type
  TF_Transfert = class(TForm)
    NMFTP_Transfert: TNMFTP;
    OD_Chercher: TOpenDialog;
    GB_Connection: TGroupBox;
    L_Ip: TLabel;
    L_Port: TLabel;
    E_Ip: TEdit;
    E_Port: TEdit;
    GB_Proxy: TGroupBox;
    CB_Proxy: TCheckBox;
    L_Proxy: TLabel;
    L_PortProxy: TLabel;
    E_Proxy: TEdit;
    E_PortProxy: TEdit;
    L_Login: TLabel;
    L_Pwd: TLabel;
    E_Login: TEdit;
    E_Pwd: TEdit;
    BT_Connection: TButton;
    GB_Transfert: TGroupBox;
    E_FichierPC: TEdit;
    L_FichierPC: TLabel;
    L_Biblio: TLabel;
    BT_Transfert: TButton;
    BT_Chercher: TButton;
    GB_Status: TGroupBox;
    L_Status: TLabel;
    BT_Fermer: TButton;
    PB_Transfert: TProgressBar;
    CB_Namefmt: TCheckBox;
    ZM_Module: TZipMaster19;
    procedure BT_connectionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BT_transfertClick(Sender: TObject);
    procedure NMFTP_TransfertSuccess(Trans_Type: TCmdType);
    procedure NMFTP_TransfertConnectionFailed(Sender: TObject);
    procedure NMFTP_TransfertConnect(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CB_ProxyClick(Sender: TObject);
    procedure BT_FermerClick(Sender: TObject);
    procedure BT_ChercherClick(Sender: TObject);
    procedure NMFTP_TransfertDisconnect(Sender: TObject);
    procedure NMFTP_TransfertFailure(var Handled: Boolean; Trans_Type: TCmdType);
    procedure NMFTP_TransfertError(Sender: TComponent; Errno: Word; Errmsg: String);
    procedure NMFTP_TransfertStatus(Sender: TComponent; Status: String);
    procedure NMFTP_TransfertAuthenticationFailed(var Handled: Boolean);
    procedure NMFTP_TransfertAuthenticationNeeded(var Handled: Boolean);
    procedure E_PwdKeyPress(Sender: TObject; var Key: Char);
    procedure NMFTP_TransfertPacketSent(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //procedure ZM_ModuleProgress(Sender: TObject; ProgrType: ProgressType;
    //  Filename: String; FileSize: Integer);
    function Dezippage(nomfichier : string) : boolean;
    function SuppressionDossier : boolean;
    function MonCrypt(chaineori : String; mode : char) : String;
    procedure TransfertSavf;
    function Deconcat(archive:string; repdest:string) : integer;
    procedure ZM_ModuleProgress(Sender: TObject;
      details: TZMProgressDetails);
  private
    { D�clarations priv�es}
    NomFichier : string;
    DossierSrc : string;
    Bibliotheque : string;
    Extension : string;
    encours : integer;
    Infos : String;
    VersionSaj : String;
  public
    { D�clarations publiques}
  end;

var
  F_Transfert: TF_Transfert;
  erreur, danslaplace : boolean;
  erreurmsg : string;
  alphabet : array[0..37] of Char=(' ','F','1','C','W','D','E','R','2','G',';','H','0','I','3','M','A','B','Y','N','4','O','P','J','Q','5','6','S','8','L','T','U','7','V','9','X','K','Z');
  cle : array[0..12] of Char=('9','5','1','4','3','5','6','7','2','3','9','0','6');

implementation

{$R *.DFM}


function TF_Transfert.MonCrypt(chaineori : String; mode : char) : string;
var
    intPos, intCur, indice : integer;
    chaine : String;
begin
    intCur:=1;
    while intCur < length(chaineori) do begin
        if intPos >= 13 then intpos := 0;
        if mode = 'C' then begin
            indice := 0;
            while chaineori[intCur] <> alphabet[indice] do
                Inc(indice);
            indice := indice + StrToInt(cle[intPos]);
            if indice > 37 then
                indice := indice - 37;
            chaine := chaine + alphabet[indice]; end
        else
            if mode = 'D' then begin
                indice := 0;
                while chaineori[intCur] <> alphabet[indice] do
                    Inc(indice);
                indice := indice - StrToInt(cle[intPos]);
                if indice <= 0 then
                    indice := 37 + indice;
                chaine := chaine + alphabet[indice];
            end;
        Inc(intPos);
        Inc(intCur);
    end;
    MonCrypt := chaine;
end;

procedure TF_Transfert.TransfertSavf;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
begin
    // Fichiers binaires
    encours := 1;
    retour := FindFirst('c:\tempsavf\*.savf', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            NMFTP_Transfert.Upload('c:\tempsavf\' + listef.name, '');
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);
    encours := 0;

    // Fichiers textes
    if not CB_Namefmt.Checked then begin
        NMFTP_Transfert.Mode(MODE_ASCII);
        NMFTP_Transfert.DoCommand('site namefmt 0');
        NMFTP_Transfert.Upload('c:\tempsavf\lib.txt', 'libtxt');
        NMFTP_Transfert.DoCommand('site namefmt 1');
        NMFTP_Transfert.Mode(MODE_IMAGE); end
    else begin // Cas pour Hexadome
        NMFTP_Transfert.Mode(MODE_ASCII);
        NMFTP_Transfert.Upload('c:\tempsavf\lib.txt', 'libtxt.file/libtxt.mbr');
        NMFTP_Transfert.Mode(MODE_IMAGE);
    end;
end;

function TF_Transfert.SuppressionDossier : boolean;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
begin
    retour := FindFirst('c:\tempsavf\*.*', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            // On s'assure que les fichiers ne soient pas en lecture seule
            attrs := FileGetAttr('c:\tempsavf\'+listef.name);
            if attrs and faReadOnly <> 0 then
                FileSetAttr('c:\tempsavf\'+listef.name, attrs - faReadOnly);
            // Puis on les d�truit
            if (DeleteFile('c:\tempsavf\'+listef.name) = False) then begin
                MessageDlg('Impossible de supprimer '+listef.name, mtError, [mbOk], 0);
                ret := False;
            end;
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);
    ChDir('c:\');
    ret := RemoveDir('c:\tempsavf');

    SuppressionDossier := ret;
end;

function TF_Transfert.Dezippage(nomfichier : string) : boolean;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
    chaine : string;
    F : TextFile;
begin
    // Cr�ation du r�pertoire temporaire
    if not DirectoryExists('C:\tempsavf') then
        if not CreateDir('C:\tempsavf') then begin
            raise Exception.Create('Impossible de cr�er c:\tempsavf');
            Dezippage := false;
        end;

    // D�concat�nation
    PB_Transfert.Position := 0;
    L_Status.Caption := 'Extraction en cours ...';
    L_Status.refresh;
    nbrfichier := Deconcat(nomfichier, 'C:\tempsavf');
    L_Status.Caption := 'Extraction termin�e';

    // V�rification de la version du SAJ
    if not FileExists('C:\tempsavf\version.txt') then begin
        ShowMessage('Fichier version non trouv� dans le fichier SAJ.');
        DeleteFile(nomfichier);
        Dezippage := False;
        Exit; end
    else begin
        VersionSaj := MonCrypt(Infos, 'D');
        VersionSaj := Copy(VersionSAj, 3+Pos('EXP', VersionSaj), 5);
        //ShowMessage(VersionSaj);
        AssignFile(F, 'C:\tempsavf\version.txt');
        Reset(F);
        Readln(F, chaine);
        CloseFile(F);
        if StrToInt(Trim(chaine)) <= StrToInt(VersionSaj) then begin
            ShowMessage('La version du fichier SAJ est plus ancienne que la version install�e au si�ge.');
            DeleteFile(nomfichier);
            Dezippage := False;
            Exit;
        end;
    end;

    // D�compression
    L_Status.Caption := 'D�compression en cours ...';
    retour := FindFirst('c:\tempsavf\*.zip', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            PB_Transfert.Position := 0;
            ZM_Module.ZipFileName:= 'C:\tempsavf\'+listef.name;
            //L_Status.Caption := 'En cours: ' + listef.name;
            ZM_Module.FSpecArgs.Add('*.*');
            ZM_Module.ExtrBaseDir := 'C:\tempsavf';
            ZM_Module.ExtrOptions := ZM_Module.ExtrOptions+[ExtrOverwrite];
            ZM_Module.Extract;
            DeleteFile('c:\tempsavf\'+listef.name);
            Dec(nbrfichier);
            L_Status.Caption := 'Fichier(s) restant(s): ' + IntToStr(nbrfichier) + ' (' + listef.name+' pass�)';
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);
    L_Status.Caption := 'D�compression termin�e';

    Dezippage := True;
end;

procedure TF_Transfert.BT_connectionClick(Sender: TObject);
begin
    if BT_connection.Caption = '&Connexion' then begin
        L_Status.caption := 'Connexion en cours ...';
        BT_Fermer.Enabled := False;
        if CB_Proxy.Checked = True  then begin
            NMFTP_Transfert.Proxy := E_proxy.text;
            NMFTP_Transfert.ProxyPort := StrToInt(E_portproxy.text);
        end;
        NMFTP_Transfert.Host := E_ip.text;
        NMFTP_Transfert.Port := StrToInt(E_port.text);
        NMFTP_Transfert.TimeOut :=  20000;
        NMFTP_Transfert.UserID := E_login.text;
        NMFTP_Transfert.Password := E_pwd.text;
        NMFTP_Transfert.Connect;
        GB_Transfert.Visible := True;
        BT_Connection.Caption := '&D�connexion';
        BT_Fermer.Caption := 'D�connecter et &Fermer'; end
    else begin
        NMFTP_Transfert.Disconnect;
        E_Pwd.Text := '';
        BT_connection.Caption := '&Connexion';
        BT_Fermer.Caption := '&Fermer';
        L_Status.caption := 'D�connexion r�ussie';
        GB_Transfert.Visible := False;
    end;
end;

procedure TF_Transfert.FormCreate(Sender: TObject);
begin
    CB_Proxy.Checked := False;
    GB_Proxy.Visible := False;
    GB_Transfert.Visible := False;
    erreurmsg := '';
    danslaplace := False;

    case ParamCount of
        0 : begin
                NomFichier := '';
                DossierSrc := '';
                Bibliotheque := 'seriemftp';
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert PC -> AS/400';
                Infos := '';
            end;
        1 : begin

                NomFichier := Trim(ParamStr(1));
                DossierSrc := '';
                Bibliotheque := 'seriemftp';
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert PC -> AS/400';
                Infos := '';
            end;
        2 : begin
                NomFichier := Trim(ParamStr(1));
                DossierSrc := Trim(ParamStr(2));
                Bibliotheque := 'seriemftp';
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert PC -> AS/400';
                Infos := '';
            end;
        3 : begin
                NomFichier := Trim(ParamStr(1));
                DossierSrc := Trim(ParamStr(2));
                Bibliotheque := Trim(ParamStr(3));
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert PC -> AS/400';
                Infos := '';
            end;
        4 : begin
                NomFichier := Trim(ParamStr(1));
                DossierSrc := Trim(ParamStr(2));
                Bibliotheque := Trim(ParamStr(3));
                E_ip.text := Trim(ParamStr(4));
                F_Transfert.Caption := 'Transfert PC -> AS/400';
                Infos := '';
            end;
        5 : begin
                NomFichier := Trim(ParamStr(1));
                DossierSrc := Trim(ParamStr(2));
                Bibliotheque := Trim(ParamStr(3));
                E_ip.text := Trim(ParamStr(4));
                F_Transfert.Caption := Trim(Paramstr(5));
                Infos := '';
            end;
        6 : begin
                NomFichier := Trim(ParamStr(1));
                DossierSrc := Trim(ParamStr(2));
                Bibliotheque := Trim(ParamStr(3));
                E_ip.text := Trim(ParamStr(4));
                F_Transfert.Caption := Trim(Paramstr(5));
                Infos := ParamStr(6);
            end;
    end;
    if LowerCase(NomFichier) = '*.zip' then begin
        OD_Chercher.FilterIndex := 2;
        NomFichier := '';
    end;

    if (DossierSrc <> '') and (NomFichier <> '') then begin
        E_FichierPC.Text := Trim(DossierSrc) + '\' + Trim(NomFichier);
        OD_Chercher.InitialDir := Trim(DossierSrc); end
    else begin
        E_FichierPC.Text := '';
        OD_Chercher.InitialDir := Trim(DossierSrc);
    end;

    L_Biblio.Caption := 'vers la biblioth�que: ' + UpperCase(Bibliotheque);

end;

procedure TF_Transfert.BT_transfertClick(Sender: TObject);
var
    position : integer;
begin
    // On pr�vient que le transfert � �t� lanc�
    BT_Fermer.Enabled := False;
    BT_Connection.Enabled := False;
    PB_Transfert.Position := 0;

    //On v�rifie que le fichier existe bien sur le PC
    L_Status.Caption := 'V�rification en cours ...';
    if not FileExists(E_FichierPC.Text) then begin
        L_Status.Caption := 'Fichier non trouv� sur le PC';
        BT_Fermer.Enabled := True;
        BT_Connection.Enabled := True;
        Exit;
    end;

    // On v�rifie le type de fichier � transf�rer
    position := LastDelimiter('.', E_FichierPC.Text);
    extension := Copy(E_FichierPC.Text, position+1, 3);

    if LowerCase(extension) = 'saj' then
        if Dezippage(E_FichierPC.Text) = false then begin
            L_Status.Caption := 'Probl�me lors de la d�compression';
            BT_Fermer.Enabled := True;
            Exit;
        end;

    // On se met dans la bibiloth�que SERIEMFTP
    if danslaplace = False then
        if not CB_Namefmt.Checked then
            NMFTP_Transfert.ChangeDir(Bibliotheque)
        else // Cas pour Hexadome
            NMFTP_Transfert.ChangeDir('/QSYS.LIB/' + Bibliotheque + '.LIB');


    // On initialise le mode pour l'AS/400
    if erreur = False then begin
        L_Status.Caption := 'Transfert en cours ...';
        danslaplace := True;
        if not CB_Namefmt.Checked then begin
            erreurmsg := 'Echec lors de SITE NAMEFMT 1';
            NMFTP_Transfert.DoCommand('site namefmt 1');
        end;
        if LowerCase(extension) = 'saj' then begin
            TransfertSavf;
            SuppressionDossier; end
        else
            NMFTP_Transfert.Upload(E_FichierPC.Text, '');
    end
    else begin
        BT_Fermer.Enabled := True;
        BT_Connection.Enabled := True;
    end;
end;

procedure TF_Transfert.NMFTP_TransfertSuccess(Trans_Type: TCmdType);
begin
    case Trans_Type of
        cmdUpload: begin
            if (LowerCase(extension) = 'saj') and (encours = 1) then begin
                L_Status.caption := 'Transfert en cours ...';
                Exit;
            end;
            L_Status.caption := 'Transfert ex�cut� avec succ�s';
            E_FichierPC.Text := '';
            BT_Fermer.Enabled := True;
            BT_Connection.Enabled := True;
            BT_Fermer.SetFocus;
        end;
    end;
end;

procedure TF_Transfert.NMFTP_TransfertConnectionFailed(Sender: TObject);
begin
    L_Status.caption := 'Echec � la connexion';
end;

procedure TF_Transfert.NMFTP_TransfertConnect(Sender: TObject);
begin
    BT_Fermer.Enabled := True;
    L_Status.caption := 'Connexion r�ussie';
    NMFTP_Transfert.Mode(MODE_IMAGE);
end;


procedure TF_Transfert.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    NMFTP_Transfert.Disconnect;
end;

procedure TF_Transfert.CB_ProxyClick(Sender: TObject);
begin
    if CB_Proxy.Checked = True then
        GB_Proxy.Visible := True
    else
        GB_Proxy.Visible := False;
end;

procedure TF_Transfert.BT_FermerClick(Sender: TObject);
begin
    NMFTP_Transfert.Disconnect;
    Close;
end;

procedure TF_Transfert.BT_ChercherClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le fichier';
    OD_Chercher.Execute;
    E_FichierPC.Text := OD_Chercher.FileName;
end;

procedure TF_Transfert.NMFTP_TransfertDisconnect(Sender: TObject);
begin
    L_Status.Caption := 'D�connect�';
    GB_Transfert.Visible := False;
    BT_Fermer.Enabled := True;
    BT_Connection.Enabled := True;
    BT_connection.Caption := '&Connexion';
end;

procedure TF_Transfert.NMFTP_TransfertFailure(var Handled: Boolean;
  Trans_Type: TCmdType);
begin
    Handled := True;
    erreur := True;
    Case Trans_Type of
        cmdChangeDir: begin
                          L_Status.Caption := 'Cr�ation de ' + UpperCase(Bibliotheque);
                          if not CB_Namefmt.Checked then begin
                              NMFTP_Transfert.MakeDirectory(Bibliotheque);
                              NMFTP_Transfert.ChangeDir(Bibliotheque); end
                          else begin // Cas pour Hexadome
                              NMFTP_Transfert.MakeDirectory('/QSYS.LIB/' + Bibliotheque + '.LIB');
                              NMFTP_Transfert.ChangeDir('/QSYS.LIB/' + Bibliotheque + '.LIB');
                          end;
                          erreur := False;
                      end;
        cmdMakeDir: L_Status.Caption := 'Echec de MakeDir';
        cmdUpload: begin
                       L_Status.Caption := 'Echec du transfert';
                       BT_Fermer.Enabled := True;
                       BT_Connection.Enabled := True;
                   end;
        cmdDoCommand: L_Status.Caption := erreurmsg;
    else
        erreur := False;
    end;

end;

procedure TF_Transfert.NMFTP_TransfertError(Sender: TComponent; Errno: Word;
  Errmsg: String);
begin
    L_Status.Caption := 'Erreur n�' + IntToStr(Errno);
    BT_Fermer.Enabled := True;
    BT_Connection.Enabled := True;
end;

procedure TF_Transfert.NMFTP_TransfertStatus(Sender: TComponent; Status: String);
begin
     L_Status.Caption := Status + '('+ IntToStr(NMFTP_Transfert.ReplyNumber) +')';
     BT_Fermer.Enabled := True;
     BT_Connection.Enabled := True;
end;

procedure TF_Transfert.NMFTP_TransfertAuthenticationFailed(var Handled: Boolean);
begin
     L_Status.Caption := 'Erreur lors de l''identification';
end;

procedure TF_Transfert.NMFTP_TransfertAuthenticationNeeded(var Handled: Boolean);
begin
     L_Status.Caption := 'Erreur lors de l''identification';
end;


procedure TF_Transfert.E_PwdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then BT_connectionClick(BT_Connection);
end;

procedure TF_Transfert.NMFTP_TransfertPacketSent(Sender: TObject);
begin
    PB_Transfert.Max := NMFTP_Transfert.BytesTotal;
    PB_Transfert.Position := Round(NMFTP_Transfert.BytesSent);
end;

procedure TF_Transfert.FormShow(Sender: TObject);
begin
    if E_Ip.Text <> '127.0.0.1' then
        E_Pwd.SetFocus;
end;

//------------------------------------------------------------------------------
//--> Gestion des barres de progression
//------------------------------------------------------------------------------
(*
procedure TF_Transfert.ZM_ModuleProgress(Sender: TObject;
  ProgrType: ProgressType; Filename: String; FileSize: Integer);
begin
    case ProgrType of
        TotalFiles2Process: begin
                                PB_Transfert.Max := FileSize;
                                PB_Transfert.Step := 1;
                            end;
        NewFile:            begin
                                PB_Transfert.stepIt;
                            end;
    end;
end;
     *)

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Proc�dure de "d�concat�nation"
//           archive : chemin+nom du fichier � d�compresser
//           repdest : chemin du repertoire ou seront cr��s les fichiers
//                     de l'archive (sans le "\" final)
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function TF_Transfert.Deconcat(archive:string; repdest:string) : integer;
var
ms,msWrite : TMemoryStream;
buf : array of Byte;
iFileLength,i,j,k, PosMarkDeb, PosMarkFin, PosMarkSuiv : Integer;
marqueur, nomFic: String;
test : Boolean;
nbrfic : integer;
begin
     PB_Transfert.Max := 10;
     PB_Transfert.Step := 1;
     nbrfic := 0;
     marqueur := '???!!!#/\';
     ms := TMemoryStream.Create;
 try
     ms.LoadFromfile(archive);
     buf := @ms.memory^;


    //gestion du r�pertoire de destination
    if not DirectoryExists(repdest) then
           if not CreateDir(repdest) then
              raise Exception.Create('Impossible de cr�er '+ repdest);
    ifileLength := ms.size;
    k := 0;
    i := 0;
    while i< ifileLength do begin
        test := false;
        if (buf[i] = 63)and (buf[i+1] = 63) and (buf[i+2] = 63) and (buf[i+3] = 33) and (buf[i+4] = 33)and (buf[i+5] = 33)and (buf[i+6] = 35)and (buf[i+7] = 47)and (buf[i+8] = 92) then
           test := true;
        if (test) then
           inc(k);

        if (k=1)and(test) then
           PosMarkDeb := i;
        if (k=2)and(test) then
           PosMarkFin := i;
        if (k=3)and(test) then
           PosMarkSuiv := i;

        if (k=3) and (test) Then
        begin
             //recup le nom du fichier
             for j := PosMarkDeb + 9 to PosMarkFin - 1  do
                 nomfic := nomfic + Chr(Buf[j]);

             //Ecriture des donn�es
             msWrite := TMemoryStream.Create;
             For j := PosMarkFin+9 to PosMarkSuiv-1 do
                  msWrite.Write(buf[j],1);
             msWrite.SaveToFile(repdest+'\'+nomfic);
             nbrfic := nbrfic + 1;
             if PB_Transfert.position >= 10 then
                 PB_Transfert.position := 0;
             PB_Transfert.stepIt;
             msWrite.Free;

             k := 0;
             nomfic := '';
             dec(i);
        end;

        Inc(i);
    end;
    //recup le nom du fichier
    for j := PosMarkDeb + 9 to PosMarkFin - 1  do
        nomfic := nomfic + Chr(Buf[j]);

    //Ecriture des donn�es
    msWrite := TMemoryStream.Create;
    try
       For j := PosMarkFin+9 to iFileLength-1 do
           msWrite.Write(buf[j],1);
       msWrite.SaveToFile(repdest+'\'+nomfic);
       nbrfic := nbrfic + 1;
       if PB_Transfert.position >= 10 then
           PB_Transfert.position := 0;
       PB_Transfert.stepIt

    finally
       msWrite.Free;
    end;
 finally
    buf := nil;
    ms.free;
 end;
 Deconcat := nbrfic;
end;

//------------------------------------------------------------------------------
//--> Gestion des barres de progression
//------------------------------------------------------------------------------
procedure TF_Transfert.ZM_ModuleProgress(Sender: TObject;
  details: TZMProgressDetails);
begin
(*
        case ProgrType of
        TotalFiles2Process: begin
                                PB_Transfert.Max := FileSize;
                                PB_Transfert.Step := 1;
                            end;
        NewFile:            begin
                                PB_Transfert.stepIt;
                            end;
    end;
    *)
end;

end.
