Uses Windows;

function OPENCOM(OpenString:PChar):Integer; stdcall; external 'RSCOM.DLL';
procedure TIMEOUTS (TOut: Integer); stdcall; external 'RSCOM.DLL';
procedure BUFFERSIZE (Size: Integer); stdcall; external 'RSCOM.DLL';
procedure CLOSECOM(); stdcall; external 'RSCOM.DLL';
procedure SENDBYTE (Dat: Integer); stdcall; external 'RSCOM.DLL';
function READBYTE(): Integer; stdcall; external 'RSCOM.DLL';
procedure SENDSTRING (Buffer: PChar); stdcall; external 'RSCOM.DLL';
function READSTRING (): PChar; stdcall; external 'RSCOM.DLL';
procedure CLEARBUFFER (); stdcall; external 'RSCOM.DLL';
function INBUFFER (): DWORD; stdcall; external 'RSCOM.DLL';
function OUTBUFFER (): DWORD; stdcall; external 'RSCOM.DLL';
procedure DTR(d:WORD); stdcall; external 'RSCOM.DLL';
procedure RTS(d:WORD); stdcall; external 'RSCOM.DLL';
procedure TXD(d:WORD); stdcall; external 'RSCOM.DLL';
function CTS:Integer; stdcall; external 'RSCOM.DLL';
function DSR:Integer; stdcall; external 'RSCOM.DLL';
function RI:Integer; stdcall; external 'RSCOM.DLL';
function DCD:Integer; stdcall; external 'RSCOM.DLL';
function INPUTS():Integer; stdcall; external 'RSCOM.DLL';
procedure TIMEINIT(); stdcall; external 'RSCOM.DLL';
function TIMEREAD(): Real; stdcall; external 'RSCOM.DLL';
procedure DELAY(DelayTime: Real); stdcall; external 'RSCOM.DLL';
procedure REALTIME(); stdcall; external 'RSCOM.DLL';
procedure NORMALTIME(); stdcall; external 'RSCOM.DLL';



procedure TForm1.Button1Click(Sender: TObject);
begin 
OpenCom('COM1: baud=9600 parity=N data=8 stop=0'); //Ouvre et param�tre la connexion sur COM1
Timer1.Enabled:=True; //Active le timer 
end; 


