unit Unit1;

//Programm� par Yoann
//http://delphipage.free.fr/
//DLL disponible � l'adresse suivante : http://www.b-kainka.de/

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Timer1: TTimer;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { D�clarations priv�es }
    SortieClgn:Integer;
  public
    { D�clarations publiques }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

//Importe les proc�dures et les fonctions de la DLL que l'on a besoin
function OPENCOM(OpenString:PChar):Integer; stdcall; external 'RSCOM.DLL';
procedure CLOSECOM(); stdcall; external 'RSCOM.DLL';
procedure TXD(d:WORD); stdcall; external 'RSCOM.DLL';

procedure TForm1.FormCreate(Sender: TObject);
begin
SortieClgn:=0; //Initialise la variable "SortieClgn"
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
OpenCom('COM1: baud=9600 parity=N data=8 stop=0'); //Ouvre et param�tre la connexion sur COM1
{On peut �galement faire ceci : "OpenCom('COM1:9600,N,8,0');" mais la notation recommand�e par l'auteur (Burkhard Kainka) de la DLL pour toutes les versions de Windows est la premi�re}
Timer1.Enabled:=True; //Active le timer
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Timer1.Enabled:=False; //D�sactive le timer
Label1.Visible:=False; //Enl�ve le label
TXD(0); //Envoi la valeur "0" vers la sortie pour que la LED soit allum�e
CloseCOM; //Lib�re le port
end;

procedure TForm1.Timer1Timer(Sender: TObject);
Var i:integer;
begin
//Alterne les "0" est les "1"
if SortieClgn=0 then
SortieClgn:=1
else
SortieClgn:=0;
TXD(SortieClgn);
Label1.Visible:=True; //Affiche le label
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if Timer1.Enabled=True then //Si le Timer n'est pas arr�ter alors on se d�connecte avant de quitter le programme
Button2Click(Sender)
end;

end.
