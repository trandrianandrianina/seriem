object Form1: TForm1
  Left = 261
  Top = 205
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Utiliser le port s�rie avec Delphi'
  ClientHeight = 88
  ClientWidth = 277
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 104
    Top = 16
    Width = 70
    Height = 13
    Caption = 'Horloge cr�e...'
    Visible = False
  end
  object Button1: TButton
    Left = 8
    Top = 48
    Width = 113
    Height = 25
    Caption = 'Connexion'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 144
    Top = 48
    Width = 113
    Height = 25
    Caption = 'D�connexion'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 200
    OnTimer = Timer1Timer
  end
end
