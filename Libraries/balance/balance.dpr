program balance;
{$APPTYPE GUI}
uses
  Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs, Clipbrd, TypInfo;

Function GfApiMainSessionConnect(SessionId: LongInt; AppName: String; CapBits: LongInt; ConMode: LongInt; WinHand: LongInt; Anchor: LongInt; var ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiMainSessionDisconnect(ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiScriptGlobalSet(ApiHand: LongInt; VarName: String; VarValue: String; VarTyp: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';

//Importe les proc�dures et les fonctions de la DLL que l'on a besoin
function OPENCOM(OpenString:PChar):Integer; stdcall; external 'RSCOM.DLL';
procedure CLOSECOM; stdcall; external 'RSCOM.DLL';
function READBYTE(): Integer; stdcall; external 'RSCOM.DLL';
procedure DTR(d:WORD); stdcall; external 'RSCOM.DLL';
procedure RTS(d:WORD); stdcall; external 'RSCOM.DLL';
procedure TXD(d:WORD); stdcall; external 'RSCOM.DLL';
procedure TIMEINIT(); stdcall; external 'RSCOM.DLL';

{$R *.RES}

const
    GF_OK : longint = 0;    // No error / OK
    GF_SCRIPT_VAR_STRING : longint = 0;
    GF_SCRIPT_VAR_NUMERIC : longint = 1;
    GF_EOF : longint = -1;  // End of file
    GF_FAIL : longint = 1;  // Failure
    GF_FALSE : longint = 0; // False
    GF_TRUE : longint = 1; // True

    GF_CONNECT_LOCKED : longint = 1; //One application to session.
    GF_CONNECT_API : longint = 2;    //Connect to API only. No session given
    GF_CONNECT_DLL : longint = 3;    //Connection is with DLL. Need not be done
    GF_CONENCT_NEW : longint = 4;    //Start a blind session when it doesn't exist

    MAXITER = 1000;
    SOH = $01;
    STX = $02;
    CR = $0D;
    LF = $0A;

var
  octet, octetf, octet1, octet2 : byte;
  trouve : boolean;
  bufc : array[1..11] of char;
  i, cpt: Integer;
  chaine : ansistring;
  port, vitesse, parite, bitdata, bitstop, unite : string;

  api_hand : longint;
  ois_error : longint;
  nomvar : array[0..14] of char;
  buffer : array[0..255] of char;
  handlexplore : hwnd;
  sessionid : char;

begin
  // Lecture des param�tres entrants
  port := 'COM1';
  vitesse := '1200';
  parite := 'Y';
  bitdata := '7';
  bitstop := '1';
  unite := '0';
  if ParamCount = 7 then begin
      sessionid := char(ParamStr(1)[1]);
      port := ParamStr(2);
      vitesse := ParamStr(3);
      parite := ParamStr(4);
      bitdata := ParamStr(5);
      bitstop := ParamStr(6);
      unite := ParamStr(7); end
  else
      Application.Terminate;

  // Connexion � la session JWalk en cours
  handlexplore := FindWindow(nil, 'balance');
  api_hand := 0;
  ois_error := GfApiMainSessionConnect(ord(sessionid), 'balance', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
  if ois_error <> GF_OK then begin
      Application.Terminate;
  end;

  // Init port
  TIMEINIT;
  DTR(1);
  RTS(1);
  TXD(1);

  //Ouvre et param�tre la connexion sur COM
  chaine := Trim(port) +': baud='+Trim(vitesse)+' parity='+Trim(parite)+' data='+Trim(bitdata)+' stop='+Trim(bitstop);
//  ShowMessage(chaine);
  i := OpenCom(PChar(chaine));
//  ShowMessage(inttostr(i));
  chaine := '';
  trouve := False;
  repeat
      octet := READBYTE();
  until (octet = SOH);
  repeat
      octet := READBYTE();
  until (octet = STX);

  if octet = STX then begin
      if not trouve then begin
        repeat
            repeat
                octet1 := READBYTE();
            until (octet1 <> 255);
            repeat
                octet2 := READBYTE();
            until (octet2 <> 255);
            // Recherche du Poids Net
            if (octet1 = $30) and (octet2 = $33) then
                trouve := True
            else begin
                repeat
                    octet := READBYTE();
                until (octet = STX);
            end;
        until trouve;

        // Poids Net
        for i := 1 to 10 do begin
            repeat
                octet := READBYTE();
            until (octet <> 255);
            bufc[i] := char(octet);
        end;
    end;

    // fin
    repeat
        octet := READBYTE();
        if octet <> LF then octetf := octet;
    until (octetf = CR) and (octet = LF);
  end;
  CloseCom;

  // Si besoin on enleve l'unit�
  if unite = '0' then
      for i := 8 to 10 do
          bufc[i] := ' ';

  chaine := string(bufc);
//  StringReplace(chaine, '.', ',', [rfReplaceAll]);

  // Mise en place des donn�es dans le presse papier
  Clipboard.SetTextBuf(Pchar(chaine));

  StrPCopy(nomvar, 'poids');
  StrPCopy(buffer, chaine);
  ois_error := GfApiScriptGlobalSet(api_hand, nomvar, buffer, GF_SCRIPT_VAR_STRING);
  if ois_error <> GF_OK then begin
      Showmessage('Erreur lors de l''init de la variable JWALK');
  end;

  // D�connection de la session JWalk en cours
  ois_error := GfApiMainSessionDisconnect(api_hand);
end.
