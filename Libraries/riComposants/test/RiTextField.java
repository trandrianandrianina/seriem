package test;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;

import base.Constantes;
import base.SaisieDefinition;



/**
 * Cette classe h�rite d'un Jtextfield et permet de g�rer son �v�nementiel au sein m�me du composant
 * @author ritoudb
 *
 */
public class RiTextField extends JTextField 
{
	private static final long serialVersionUID = 2840216410707841899L;
	private boolean isModeLabel = false;
	private String monLabel = null;
	
	/**
	 * constructeur permettant de g�rer l'�v�nementiel du JTextfield
	 */
	public RiTextField()
	{
		super();
		this.addFocusListener(new FocusListener() 
		{
            public void focusGained(FocusEvent ev1) {
            	passerModeSaisie();
            }
 
           public void focusLost(FocusEvent ev2) {
        	    passerModeLabel();
           }
        });
	}
	
	/**
	 * permet d'initialiser le label descriptif du JTextfield
	 * @param label
	 */
	public void initModeLabel(String label)
	{
		monLabel = label;
		setToolTipText(monLabel);
		//System.out.println("-[RiTextField]-passerModeLabel-> INIT label:|" + monLabel + "|");
	}
	
	/**
	 * afficher le label du JTextfield
	 */
	public void passerModeLabel()
	{
		if(monLabel!=null && getText().trim().equals(""))
		{
			//System.out.println("-[RiTextField]-passerModeLabel-> AP label:|" + monLabel + "|");
			//System.out.println("-[RiTextField]-passerModeLabel-> AV text:|" + getText().trim() + "|");
			setForeground(Constantes.CL_TEXT_SORTIE);
			this.setText(monLabel);
			isModeLabel = true;
			//System.out.println("-[RiTextField]-passerModeLabel-> AP text:|" + getText().trim() + "|");
			//System.out.println("-[RiTextField]-passerModeLabel-> AP label:|" + monLabel + "|");
		}
		//else System.out.println("-[RiTextField]-passerModeLabel-> RATE text:|" + getText().trim() + "|");
	}
	
	/**
	 * passer en mode saisie : supprimer le label et changer la couleur
	 */
	public void passerModeSaisie()
	{
		if(isModeLabel)
		{
			//System.out.println("-[RiTextField]-passerModeLabel-> AV SAIS text:|" + getText().trim() + "|");
			setText("");
			//System.out.println("-[RiTextField]-passerModeLabel-> AP SAIS text:|" + getText().trim() + "|");
			setForeground(Constantes.CL_BT_CLAIR);
		}
		else if(isEditable())
			selectAll();
			
		isModeLabel = false;	
	}
	
	/**
	 * nettoie la saisie avant getData() afin de faire disparaitre le label si pr�sent
	 * @return String
	 */
	public String nettoyerLaSaisie()
	{
//System.out.println("-[RiTextField]-nettoyerLaSaisie-> Je nettoie");
		if(isModeLabel)
			 return "";
		else
			return getText(); 
	}
	
	/**
	 * Fixe le nombre maximal de caract�res que l'on peut saisir dans un RITextField
	 */
	public void setLongueurSaisie(int maxLength)
	{
		AbstractDocument doc = (AbstractDocument) getDocument();
		doc.setDocumentFilter(new SaisieDefinition(maxLength));
	}
	
	public boolean isEnModeLabel()
	{
		return isModeLabel;
	}
	
	public String getLabel()
	{
		return monLabel;
	}
	
}


