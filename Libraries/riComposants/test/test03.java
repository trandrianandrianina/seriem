/*
 * Created by JFormDesigner on Wed Mar 03 13:57:41 CET 2010
 */

package test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * @author St�phane V�n�ri
 */
public class test03 extends JFrame
{
	private static final long serialVersionUID = 1L;
	//private ResourceBundle oldbundle=null;
	private boolean loadFileI18n=true, executeI18n=false;
	private String[] TYPEARG_DataBrut={" ", "@LARG1@", "@LARG2@", "@LARG3@", "@LARG4@", "@LARG5@"};
	private String[] TYPEARG_Value={"0", "1", "2", "3", "4", "5"};

	public test03()
	{
		initComponents();
		//riComboBox1.setDataBrut(TYPEARG_DataBrut, TYPEARG_Value);
		//riComboBox1.setData();
		riComboBox1.setModel(new DefaultComboBoxModel(TYPEARG_DataBrut));
		//for(int i=0; i<riComboBox1.getItemCount(); i++)
		//	System.out.println(riComboBox1.getItemAt(i));
		riComboBox1.setSelectedIndex(1);
		setVisible(true);
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		riComboBox1 = new JComboBox();

		//======== this ========
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		//---- riComboBox1 ----
		riComboBox1.setName("riComboBox1");
		contentPane.add(riComboBox1);
		riComboBox1.setBounds(135, 65, 160, riComboBox1.getPreferredSize().height);

		{ // compute preferred size
			Dimension preferredSize = new Dimension();
			for(int i = 0; i < contentPane.getComponentCount(); i++) {
				Rectangle bounds = contentPane.getComponent(i).getBounds();
				preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
				preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
			}
			Insets insets = contentPane.getInsets();
			preferredSize.width += insets.right;
			preferredSize.height += insets.bottom;
			contentPane.setMinimumSize(preferredSize);
			contentPane.setPreferredSize(preferredSize);
		}
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JComboBox riComboBox1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}