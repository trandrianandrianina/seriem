package test;

import java.awt.Color;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;


/**
 * @version 1.0 11/09/98
 */
public class MultiLineHeaderRenderer extends JList implements TableCellRenderer
{
	public MultiLineHeaderRenderer()
	{

System.out.println("-opaque-> " + UIManager.getLookAndFeel().getDescription());		
System.out.println("-opaque-> " + UIManager.getColor("List.focusCellHighlightBorder"));		
		//setOpaque(true);

		setForeground(UIManager.getColor("TableHeader.foreground"));
System.out.println("-fore-> " + UIManager.getColor("TableHeader.foreground"));

		//setBackground(get)
		//setBackground(UIManager.getColor("MenuBar[Enabled].backgroundPainter"));
		//d.put("TableHeader:\"TableHeader.renderer\"[Disabled].backgroundPainter", new LazyPainter("com.sun.java.swing.plaf.nimbus.TableHeaderTableHeaderRendererPainter", TableHeaderTableHeaderRendererPainter.BACKGROUND_DISABLED, new Insets(5, 5, 5, 5), new Dimension(22, 20), false, AbstractRegionPainter.PaintContext.CacheMode.NINE_SQUARE_SCALE, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY));
System.out.println("-back-> " + UIManager.getColor("MenuBar[Enabled].backgroundPainter"));	

		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		ListCellRenderer renderer = getCellRenderer();
		//((JLabel)renderer).setHorizontalAlignment(JLabel.CENTER);
		setCellRenderer(renderer);
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	{
		//setFont(table.getFont());
		String str = (value == null) ? "" : value.toString();
		BufferedReader br = new BufferedReader(new StringReader(str));
		String line;
		Vector<String> v = new Vector<String>();
		try {
			while ((line = br.readLine()) != null) {
				v.addElement(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		setListData(v);
		return this;
	}
}