package test;

import javax.swing.JPasswordField;
import javax.swing.text.Document;

public class RiPasswordField extends JPasswordField
{
	// Constante
	private static final long serialVersionUID = -1159570518053561751L;

	public RiPasswordField()
	{
	}

	public RiPasswordField(String text)
	{
		super(text);
	}

	public RiPasswordField(int columns)
	{
		super(columns);
	}

	public RiPasswordField(String text, int columns)
	{
		super(text, columns);
	}

	public RiPasswordField(Document doc, String txt, int columns)
	{
		super(doc, txt, columns);
	}

	public String getText()
	{
		return new String(getPassword());
	}
}
