/*
 * Created by JFormDesigner on Wed Apr 14 21:59:56 CEST 2010
 */

package test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * @author St�phane V�n�ri
 */
public class test06 extends JFrame
{
	private static final long serialVersionUID = 1L;

	public test06()
	{
		initComponents();
		label1.setLocation(50,50);
		label1.setSize(label1.getPreferredSize());
		label2.setLocation(50,50);
		label2.setSize(label2.getPreferredSize());
		setComponentZOrder(label2, 0);
		setComponentZOrder(label1, 1);
		pack();
	}
	
	public static void main(String args[])
	{
		new test06().setVisible(true);
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		label2 = new JLabel();
		label1 = new JLabel();

		//======== this ========
		setLocationByPlatform(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		{ // compute preferred size
			Dimension preferredSize = new Dimension();
			for(int i = 0; i < contentPane.getComponentCount(); i++) {
				Rectangle bounds = contentPane.getComponent(i).getBounds();
				preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
				preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
			}
			Insets insets = contentPane.getInsets();
			preferredSize.width += insets.right;
			preferredSize.height += insets.bottom;
			contentPane.setMinimumSize(preferredSize);
			contentPane.setPreferredSize(preferredSize);
		}
		pack();
		setLocationRelativeTo(getOwner());

		//---- label2 ----
		label2.setText("dessus");
		label2.setName("label2");

		//---- label1 ----
		label1.setText("Dessous");
		label1.setName("label1");
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JLabel label2;
	private JLabel label1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
