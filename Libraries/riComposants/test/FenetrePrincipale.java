/*
 * Created by JFormDesigner on Mon Dec 21 12:39:31 CET 2009
 */

package test;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JSpinner;
import gfx.graphe.RiGraphe;

import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 * @author David Biason
 * 
 */
public class FenetrePrincipale extends JFrame 
{
	private static final long serialVersionUID = 1L;
	//private boolean loadFileI18n=true, executeI18n=false;
	RiGraphe rg=null, rg2=null;

	public FenetrePrincipale()
	{
		initComponents();
		
		/* ***************************************** Int�gration du graphe ***********************************************/
		
		// Cr�er un type de flux de donn�es
		String titre = "March� du logiciel de Compta/Gestion";
		boolean legend=true;
		Object[][] donnee = new Object[3][2];
		donnee[0][0]="Serie M";
		donnee[0][1]=new Double(700);
		donnee[1][0]="SAP";
		donnee[1][1]=new Double(300);
		donnee[2][0]="Merde";
		donnee[2][1]=new Double(10);
		
		Object[][] donnee2 = new Object[8][2];
		donnee2[0][0]="Robert";
		donnee2[0][1]=new Double(100);
		donnee2[1][0]="Mary";
		donnee2[1][1]=new Double(50);
		donnee2[2][0]="John";
		donnee2[2][1]=new Double(120);
		donnee2[3][0]="Ellen";
		donnee2[3][1]=new Double(180);
		donnee2[4][0]="Jack";
		donnee2[4][1]=new Double(20);
		donnee2[5][0]="David";
		donnee2[5][1]=new Double(220);
		donnee2[6][0]="Mark";
		donnee2[6][1]=new Double(130);
		donnee2[7][0]="Andy";
		donnee2[7][1]=new Double(10);

		Object[][] donnee3 = new Object[8][2];
		donnee3[0][0]="Robert";
		donnee3[0][1]=new Double(100);
		donnee3[1][0]="Mary";
		donnee3[1][1]=new Double(50);
		donnee3[2][0]="John";
		donnee3[2][1]=new Double(120);
		donnee3[3][0]="Ellen";
		donnee3[3][1]=new Double(180);
		donnee3[4][0]="Jack";
		donnee3[4][1]=new Double(20);
		donnee3[5][0]="David";
		donnee3[5][1]=new Double(220);
		donnee3[6][0]="Mark";
		donnee3[6][1]=new Double(130);
		donnee3[7][0]="Andy";
		donnee3[7][1]=new Double(10);

		rg = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
		rg.setDonnee(donnee, null, false);
		rg.getGraphe(titre, legend);
		label1.setIcon(rg.getPicture(500, 350));

		rg2 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
		rg2.setDonnee(donnee2, "Serie 1", false);
		//rg2.setDonnee(donnee3, "Serie 2", false);
		//rg2.getGrapheBar3D("Bonne Humeur", "Employ�s", "Degr�", RiGraphe.VERTICAL, legend);
		rg2.getGraphe("Bonne Humeur", legend);
		label2.setIcon (rg2.getPicture(500, 350));

		setVisible(true);
	}
	
	public static void main(String[] args) 
	{
		FenetrePrincipale fen = new FenetrePrincipale();
	}

	private void button1ActionPerformed(ActionEvent e) {
		rg.sendToClipBoard(label1.getWidth(), label1.getHeight());
	}

	private void button2ActionPerformed(ActionEvent e) {
		rg.saveGraphe(null, label1.getWidth(), label1.getHeight());
	}
	
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		label1 = new JLabel();
		label2 = new JLabel();
		button1 = new JButton();
		button2 = new JButton();
		spinner1 = new JSpinner();

		//======== this ========
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		//---- label1 ----
		label1.setText("text");
		label1.setName("label1");
		contentPane.add(label1);
		label1.setBounds(10, 15, 500, 350);

		//---- label2 ----
		label2.setText("text");
		label2.setName("label2");
		contentPane.add(label2);
		label2.setBounds(515, 15, 500, 350);

		//---- button1 ----
		button1.setText("text");
		button1.setName("button1");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button1ActionPerformed(e);
			}
		});
		contentPane.add(button1);
		button1.setBounds(new Rectangle(new Point(160, 405), button1.getPreferredSize()));

		//---- button2 ----
		button2.setText("text");
		button2.setName("button2");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button2ActionPerformed(e);
			}
		});
		contentPane.add(button2);
		button2.setBounds(new Rectangle(new Point(230, 400), button2.getPreferredSize()));

		//---- spinner1 ----
		spinner1.setName("spinner1");
		contentPane.add(spinner1);
		spinner1.setBounds(new Rectangle(new Point(525, 415), spinner1.getPreferredSize()));

		contentPane.setPreferredSize(new Dimension(1035, 480));
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JLabel label1;
	private JLabel label2;
	private JButton button1;
	private JButton button2;
	private JSpinner spinner1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
