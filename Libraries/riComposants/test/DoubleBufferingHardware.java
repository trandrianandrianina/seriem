package test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class DoubleBufferingHardware extends JFrame{
	   // boucle d'affichage
	   RenderingThread renderingThread = new RenderingThread(); 
	   // variable permettant d'utiliser la m�moire VRAM
	   BufferStrategy strategy; 
	   // buffer m�moire o� les images et les textes sont appliqu�s
	   Graphics buffer; 
	   int x = 0; // coordonn�e x de l'affichage du texte

	   public DoubleBufferingHardware(){
	      setSize( 400, 400 );
	      setVisible( true );
	   
	      // inhibe la m�thode courante d'affichage du composant 
	      setIgnoreRepaint( true );  
	   
	      // on cr�� 2 buffers dans la VRAM donc c'est du double-buffering
	      createBufferStrategy( 2 ); 
	   
	      // r�cup�re les buffers graphiques dans la m�moire VRAM
	      strategy = getBufferStrategy(); 
	      buffer = strategy.getDrawGraphics();
	      renderingThread.start();  
	   }

	   public void graphicalRender(){
	      buffer.setColor( Color.black ); 
	      buffer.fillRect( 0, 0, 400, 400 ); 
	      buffer.setColor( Color.white ); 
	      buffer.drawString( "affichage d'une ligne de texte", x, 200 );
	      x++; 
	      if ( x > 400 ) x = 0; 
	      //on envoie toutes les donn�es du buffer m�moire vers le buffer d'affichage
	      strategy.show(); 
	   }

	   class RenderingThread extends Thread{
	     /**
	      *  Ce thread appelle le rafraichissement de notre fen�tre 
	      *  toutes les 10 milli-secondes
	      */
	      public void run(){
	         while( true ){
	            try {
	                // on appelle notre propre m�thode de rafraichissement
	               graphicalRender();
	               sleep( 10 );
	            } catch ( Exception e ) {} 
	         }
	      }
	   }   
	   public static void main(String[] args){new DoubleBufferingHardware();}
	}
