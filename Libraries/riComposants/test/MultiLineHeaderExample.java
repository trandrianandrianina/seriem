package test;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 * @version 1.0 11/09/98
 */
public class MultiLineHeaderExample extends JFrame
{
	MultiLineHeaderExample() {
		super( "Multi-Line Header Example" );

		DefaultTableModel dm = new DefaultTableModel();
		dm.setDataVector(new Object[][]{{"a","b","c"}, {"A","B","C"}}, new Object[]{"1st\nalpha","2nd\nbeta","3rd\ngamma"});

		JTable table = new JTable( dm );
		MultiLineHeaderRenderer renderer = new MultiLineHeaderRenderer();

		int ligne=0;
		Enumeration enum1 = table.getColumnModel().getColumns();
		while (enum1.hasMoreElements())
		{
			System.out.println("\t--> + " + ligne++);    	
			((TableColumn)enum1.nextElement()).setHeaderRenderer(renderer);
		}   
		JScrollPane scroll = new JScrollPane( table );
		getContentPane().add( scroll );
		setSize( 400, 110 );
		setVisible(true);
	}

	public static void main(String[] args)
	{
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}	
		MultiLineHeaderExample frame = new MultiLineHeaderExample();
		frame.addWindowListener( new WindowAdapter() {
			public void windowClosing( WindowEvent e ) {
				System.exit(0);
			}
		});
	}
}