package test;

import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class CouchesAvecZOrder extends javax.swing.JFrame {
    public CouchesAvecZOrder() {
        initComponents();
    }
 
    private void placeLesCouches(){
        JPanel zpanel;
        JLabel dessous;
        JLabel dessus;
 
        zpanel = new JPanel();
 
        // Le r�glage du layout est particulier avec le ZOrder.
        // Pour simplifier on le supprime.
        zpanel.setLayout(null);
 
        zpanel.setPreferredSize(new java.awt.Dimension(400, 200));
        dessus = new JLabel("au dessus !");
 
        // Comme il n'y a plus de layout on doit g�rer les tailles.
        dessus.setSize(dessus.getPreferredSize());
 
        // Inutile, pour jouer avec l'opacit� de fa�on � mieux faire
        // apparaitre ce qui est dessus/dessous
        //dessus.setOpaque(true);
 
        zpanel.setComponentZOrder(dessus, 0);
        dessous = new JLabel("en dessous ! (et en partie masqu�)");
        dessous.setSize(dessous.getPreferredSize());
        zpanel.setComponentZOrder(dessous, 1);
        add(zpanel);
        pack();
    }
 
  	@SuppressWarnings("unchecked")
  	private void initComponents() {
    	setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
 
    	pack();
  	}                        
 
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                CouchesAvecZOrder couches;
 
                couches = new CouchesAvecZOrder();
                couches.placeLesCouches();
                couches.setVisible(true);
            }
        });
    }
}