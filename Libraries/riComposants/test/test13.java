/*
 * Created by JFormDesigner on Tue Mar 03 17:50:28 CET 2015
 */

package test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import gfx.RiPanelSliding;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * @author Emmanuel MARCQ
 */
public class test13 extends JFrame {
	RiPanelSliding fond = new RiPanelSliding();
	public static void main(String args[])
	{
		new test13();
	}
	
	public test13() {
		initComponents();
		
		contentPanel.add(fond, BorderLayout.CENTER);
		
		fond.setLayout(new CardLayout());
		fond.add(panel1);
		fond.add(panel2);
		setSize(800, 600);
		setVisible(true);
	}

	private void bt_suivActionPerformed(ActionEvent e) {
		fond.nextSlidPanel(20, panel2, true);
		fond.refresh();
	}

	private void bt_predActionPerformed(ActionEvent e) {
		fond.nextSlidPanel(20, panel1, false);
		fond.refresh();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		buttonBar = new JPanel();
		bt_pred = new JButton();
		bt_suiv = new JButton();
		panel1 = new JPanel();
		label1 = new JLabel();
		panel2 = new JPanel();
		label2 = new JLabel();

		//======== this ========
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setName("dialogPane");
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setName("contentPanel");
				contentPanel.setLayout(new BorderLayout());
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setName("buttonBar");
				buttonBar.setLayout(new FlowLayout());

				//---- bt_pred ----
				bt_pred.setText("Pred");
				bt_pred.setName("bt_pred");
				bt_pred.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bt_predActionPerformed(e);
					}
				});
				buttonBar.add(bt_pred);

				//---- bt_suiv ----
				bt_suiv.setText("Suiv");
				bt_suiv.setName("bt_suiv");
				bt_suiv.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bt_suivActionPerformed(e);
					}
				});
				buttonBar.add(bt_suiv);
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);

		//======== panel1 ========
		{
			panel1.setBackground(Color.blue);
			panel1.setName("panel1");
			panel1.setLayout(null);

			//---- label1 ----
			label1.setText("PANEL 1");
			label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
			label1.setName("label1");
			panel1.add(label1);
			label1.setBounds(115, 105, 295, label1.getPreferredSize().height);

			{ // compute preferred size
				Dimension preferredSize = new Dimension();
				for(int i = 0; i < panel1.getComponentCount(); i++) {
					Rectangle bounds = panel1.getComponent(i).getBounds();
					preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
					preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
				}
				Insets insets = panel1.getInsets();
				preferredSize.width += insets.right;
				preferredSize.height += insets.bottom;
				panel1.setMinimumSize(preferredSize);
				panel1.setPreferredSize(preferredSize);
			}
		}

		//======== panel2 ========
		{
			panel2.setBackground(new Color(204, 0, 0));
			panel2.setName("panel2");
			panel2.setLayout(null);

			//---- label2 ----
			label2.setText("PANEL 2");
			label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 6f));
			label2.setName("label2");
			panel2.add(label2);
			label2.setBounds(new Rectangle(new Point(95, 75), label2.getPreferredSize()));

			{ // compute preferred size
				Dimension preferredSize = new Dimension();
				for(int i = 0; i < panel2.getComponentCount(); i++) {
					Rectangle bounds = panel2.getComponent(i).getBounds();
					preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
					preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
				}
				Insets insets = panel2.getInsets();
				preferredSize.width += insets.right;
				preferredSize.height += insets.bottom;
				panel2.setMinimumSize(preferredSize);
				panel2.setPreferredSize(preferredSize);
			}
		}

		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JPanel buttonBar;
	private JButton bt_pred;
	private JButton bt_suiv;
	private JPanel panel1;
	private JLabel label1;
	private JPanel panel2;
	private JLabel label2;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
