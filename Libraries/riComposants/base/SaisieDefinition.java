//=================================================================================================
//==>                                                                       08/10/2008 - 08/01/20154
//==> Filtre permettant de saisir en majuscule, changer une saisie clavier et de limiter le nombre de caract�res saisis
//==> A faire: Rien... elle est parfaite !!
//=================================================================================================

package base;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class SaisieDefinition extends DocumentFilter //implements KeyListener
{
	private boolean isUpperCase=false;	
	private int longueur=0x0FFFFFFF;					// Par d�faut valeur max d'un entier (ou presque)
	private char[] listeCaracteresAutorises=null;		// Liste des caract�res autoris�s
	private char[] listeCaracteresNonAutorises=null;	// Liste des caract�res non autoris�s
	private boolean isAlpha = false;					// hostfield num�rique ou alpha
	private StringBuilder sb = new StringBuilder();
	private static Robot tab = null;

	/**
	 * Constructeur
	 * @param alongueur
	 */
	public SaisieDefinition(int alongueur)
	{
		setNbrMaxCar(alongueur);
	}

	/**
	 * Constructeur
	 * @param uppercase
	 */
	public SaisieDefinition(boolean auppercase)
	{
		setUpperCase(auppercase);
	}

	/**
	 * Constructeur
	 * @param alongueur
	 * @param auppercase
	 */
	public SaisieDefinition(int alongueur, boolean auppercase)
	{
		setNbrMaxCar(alongueur);
		setUpperCase(auppercase);
	}

	/**
	 * Constructeur
	 * @param alongueur
	 * @param auppercase
	 * @param listeA des caract�res autoris�s
	 */
	public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA)
	{
		setNbrMaxCar(alongueur);
		setUpperCase(auppercase);
		listeCaracteresAutorises = listeA;
	}
	
	/**
	 * Constructeur
	 * @param alongueur
	 * @param auppercase
	 * @param listeA des caract�res autoris�s
	 * @param listeNA des caract�res non autoris�s
	 */
	public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, char[] listeNA)
	{
		setNbrMaxCar(alongueur);
		setUpperCase(auppercase);
		listeCaracteresAutorises = listeA;
		listeCaracteresNonAutorises = listeNA;
	}

	/**
	 * Constructeur
	 * @param alongueur
	 * @param auppercase
	 * @param listeA des caract�res autoris�s
	 * @param isAlphaNum
	 */
	public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, boolean isAlphaNum)
	{
		setNbrMaxCar(alongueur);
		setUpperCase(auppercase);
		listeCaracteresAutorises = listeA;
		isAlpha = isAlphaNum;
	}

	/**
	 * Constructeur
	 * @param alongueur
	 * @param auppercase
	 * @param listeA des caract�res autoris�s
	 * @param listeNA des caract�res non autoris�s
	 * @param isAlphaNum
	 */
	public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, char[] listeNA, boolean isAlphaNum)
	{
		setNbrMaxCar(alongueur);
		setUpperCase(auppercase);
		listeCaracteresAutorises = listeA;
		listeCaracteresNonAutorises = listeNA;
		isAlpha = isAlphaNum;
	}

	/**
	 * Constructeur
	 * @param alongueur
	 * @param auppercase
	 *
	public SaisieDefinition(int alongueur, boolean auppercase, char[] liste, boolean isAlphaNum, JComponent composant)
	{
		setNbrMaxCar(alongueur);
		setUpperCase(auppercase);
		listeCaracteres = liste;
		isAlpha = isAlphaNum;
		setComposant(composant);
	}*/

	/**
	 * Initialise le nombre de carct�res max
	 * @param alongueur
	 */
	private void setNbrMaxCar(int alongueur)
	{
		longueur = alongueur;
	}

	/**
	 * Initialise si la saisie n'accepte que des majuscules
	 * @param auppercase
	 */
	private void setUpperCase(boolean auppercase)
	{
		isUpperCase = auppercase;
	}

	/**
	 * V�rifie que le caract�re saisie soit autoris�
	 * @param lettre
	 * @return
	 *
	private boolean isAutorized(char lettre)
	{
		boolean trouve=false;
		
		// Test des caract�res autoris�s
		if (listeCaracteresAutorises != null)
		{
			for (int i=listeCaracteresAutorises.length; --i>=0;)
				if (lettre == listeCaracteresAutorises[i])
				{
					trouve = true;
					break;
				}
		}
		else
			trouve = true;

		// Test des caract�res non autoris�s
		if ((listeCaracteresNonAutorises != null) && (trouve))
		{
//System.out.println("-NA-> " + listeCaracteresNonAutorises.length);
			for (int i=listeCaracteresNonAutorises.length; --i>=0;)
				if (lettre == listeCaracteresNonAutorises[i])
				{
					trouve = false;
					break;
				}
		}
//System.out.println("-res-> la lettre " + lettre + " est authoris�=" + trouve);

		return trouve;
	}*/

	/**
	 * V�rifie que le caract�re saisie soit autoris�
	 * @param lettre
	 * @return
	 */
	private boolean isAutorized(String texte)
	{
		boolean trouve=true;
		char[] lettres = texte.toCharArray();
		
		// Test des caract�res autoris�s
		if (listeCaracteresAutorises != null)
		{
			for (char lettre: lettres)
			{
				trouve = false;
				for (int i=listeCaracteresAutorises.length; --i>=0;)
					if (lettre == listeCaracteresAutorises[i])
					{
						trouve = true;
						break;
					}
				if (!trouve) break;
			}
		}
		else
			trouve = true;

		// Test des caract�res non autoris�s
		if ((listeCaracteresNonAutorises != null) && (trouve))
		{
//System.out.println("-NA-> " + listeCaracteresNonAutorises.length);
			for (char lettre: lettres)
			{
				for (int i=listeCaracteresNonAutorises.length; --i>=0;)
					if (lettre == listeCaracteresNonAutorises[i])
					{
						trouve = false;
						break;
					}
				if (!trouve) break;
			}
		}

		return trouve;
	}

	/**
	 * Teste le contenu d'une zone num�rique 
	 * @param asb
	 * @return
	 */
	private boolean valideNumerique(StringBuilder asb)
	{
		if ((asb.indexOf("-+") != -1) || (asb.indexOf("+-") != -1)) return false;
		if ((asb.indexOf("-,") != -1) || (asb.indexOf(",-") != -1)) return false;
		if ((asb.indexOf("+,") != -1) || (asb.indexOf(",+") != -1)) return false;
		if ((asb.indexOf(" ,") != -1) || (asb.indexOf(", ") != -1)) return false;
		if ((asb.indexOf(" +") != -1) || (asb.indexOf("+ ") != -1)) return false;
		if ((asb.indexOf(" -") != -1) || (asb.indexOf("- ") != -1)) return false;
		
		return true;
	}
	
	/**
	 * En insertion
	 * @param fb
	 * @param offset
	 * @param length
	 * @param text
	 * @param attrs
	 * @throws BadLocationException
	 */
    public void insertString(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException
    {
    	// V�rification du caract�re
    	if (!isAutorized(text)) return;
    	if (!isAlpha) // On affine le test avec les zones num�riques
    	{
    		sb.setLength(0);
    		sb.append(fb.getDocument().getText(0, fb.getDocument().getLength())).insert(offset, text);
    		if (!valideNumerique(sb)) return;
    	}
    	
    	// Insertion
    	if ((fb.getDocument().getLength() + text.length() - length) <= longueur)
    		super.insertString(fb, offset, isUpperCase?getUpperCase(text):text, attrs);
    	else
			Toolkit.getDefaultToolkit().beep();
       //super.insertString(fb, offset, string.toUpperCase(), attr);
    }
    
    /**
     * En remplacement
     */
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException
    {
    	// V�rification du caract�re
    	if (!isAutorized(text)) return;
		if (!isAlpha) // On affine le test avec les zones num�riques
		{
			sb.setLength(0);
			sb.append(fb.getDocument().getText(0, fb.getDocument().getLength())).replace(offset, offset+length, text);
			if (!valideNumerique(sb)) return;
		}

    	// Remplacement
    	if ((fb.getDocument().getLength()-length) < longueur)
    	{
    		//traitement du point rempla�� par la virgule en format num�rique
    		if(!isAlpha && text.equals("."))
    			super.replace(fb, offset, length, ",", attrs);
    		//sinon mise en majuscule
    		else
    			super.replace(fb, offset, length, isUpperCase?getUpperCase(text):text, attrs);
    		
    		if (fb.getDocument().getLength() == longueur) tabulation();
    	}
    	else
    		Toolkit.getDefaultToolkit().beep();
       //super.replace(fb, offset, length, text.toUpperCase(), attrs);
    }
    
    //private 
    /**
     * Traitement pour la transformation d'un chaine en majuscule 
     * @param chaine
     * @return
     */
    private String getUpperCase(String chaine)
    {
    	// Traitement sp�cial sur le � qui est transform� en M lors du passage en Majuscule
    	int pos=chaine.indexOf('�');
    	if (pos == -1)
    		return chaine.toUpperCase();
    	else
    		return chaine.replaceAll("�", "�").toUpperCase().replaceAll("�", "�");
    }
    
    /**
     * Simulation de la Tabulation 
     */
    private static void tabulation()
    {
    	// Ajouter encore la d�sactivation possible de la tabulation automatique
		try
    	{
			//if (composant != null) composant.requestFocus();
//System.out.println("Focus : " + composant);
			if( tab == null)
				tab = new Robot();
			// On fait attention au Shift sinon on effectue une tabulation arri�re (avec les portables)
			tab.keyPress(KeyEvent.VK_TAB);
			tab.keyRelease(KeyEvent.VK_TAB);
			//System.out.println("-SaisieDefinition-> Tabulation envoy�e.");
    	}
    	catch (AWTException e)
    	{
    		Toolkit.getDefaultToolkit().beep();
    		e.printStackTrace();
    	}
    }

	/**
	 * @return the composant
	 *
	public JComponent getComposant()
	{
		return composant;
	}

	/**
	 * @param composant the composant to set
	 *
	public void setComposant(JComponent composant)
	{
		this.composant = composant;
	}*/

 }
