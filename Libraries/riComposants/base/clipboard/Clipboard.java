//=================================================================================================
//==>                                                                       25/02/2010 - 30/03/2010
//==> Gestion du clipboard
//=================================================================================================
package base.clipboard;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class Clipboard
{

	/**
	 * Envoi une image dans le presse papier
	 * @param img
	 */
	public static void sendImageToClipBoard(Image img)
	{
		//if (img == null) return;
		ImageSelection imgSel = new ImageSelection(img);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
	}
	
	/**
	 * Retourne une image stock� dans le presse papier
	 * @return
	 */
	public static Image getImageFromClipboard()
	{
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		try
		{
			if (t != null && t.isDataFlavorSupported(DataFlavor.imageFlavor))
			{
				Image text = (Image)t.getTransferData(DataFlavor.imageFlavor);
				return text;
			}
		}
		catch (UnsupportedFlavorException e) { }
		catch (IOException e) { }
		return null;
	} 

	/**
	 * Envoi un texte dans le presse papier
	 * @param img
	 */
	public static void sendTextToClipBoard(String text)
	{
		//if (text == null) return;
		StringSelection data = new StringSelection(text);
		//Toolkit.getDefaultToolkit().getSystemClipboard().setContents(data, data);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(data, null);
	}
	
	/**
	 * Retourne un texte stock� dans le presse papier
	 * @return
	 */
	public static String getTextFromClipboard()
	{
		/*
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		try
		{
			if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor))
			{
				String text = (String)t.getTransferData(DataFlavor.stringFlavor);
				return text;
			}
		}
		catch (UnsupportedFlavorException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
		*/
		//String result = "";
		Transferable contents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if ( hasTransferableText )
		{
			try
			{
				return (String)contents.getTransferData(DataFlavor.stringFlavor);
			}
			catch (UnsupportedFlavorException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}
	
}
