//=================================================================================================
//==>                                                                       25/02/2010 - 25/02/2010
//==> Envoi une image dans le clipboard
//=================================================================================================

package base.clipboard;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class ImageSelection implements Transferable
{
	// Variables
	private Image image;

	/**
	 * Constructeur
	 * @param image
	 */
	public ImageSelection(Image image)
	{
		this.image = image;
	}

	// Returns supported flavors
	public DataFlavor[] getTransferDataFlavors()
	{
		return new DataFlavor[] { DataFlavor.imageFlavor };
	}

	// Returns true if flavor is supported
	public boolean isDataFlavorSupported(DataFlavor flavor)
	{
		return DataFlavor.imageFlavor.equals(flavor);
	}

	// Returns image
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
	{
		if (!DataFlavor.imageFlavor.equals(flavor))
		{
			throw new UnsupportedFlavorException(flavor);
		}

		return image;
	}
}
