//=================================================================================================
//==>                                                                       16/03/2007 - 05/06/2013
//==> Liste des constantes g�n�riques utilis�es dans le projet 
//==> A faire:
//==>	R�cup�rer le s�parateur d�cimal & millier de mani�re dynamique pour la localisation 
//==> ATTENTION: Ne pas utiliser d'instruction au dela de java 1.5
//=================================================================================================
package base;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.UIManager;

//import composantSwing.CustomFont;

public class Constantes
{
	// G�n�riques
	public final static int ON=1;
	public final static int OFF=0;
	public final static int OK = 0;
	public final static int ERREUR = -1;
	public final static int TRUE = 0;
	public final static int FALSE = -1;
	public final static int TRAITEE=-2;
	public final static int EN_COURS_TRAITEMENT=-3;
	public final static int BOTH=3;
	public final static int HORIZONTAL=2;
	public final static int VERTICAL = 1;
	public final static int NONE=-1;
	public final static int LEFT = 0;
	public final static int CENTER = 1;
	public final static int RIGHT = 2;
	public final static int TOP = 3;
	public final static int BOTTOM = 4;
	public final static int TAILLE_BUFFER = 32768;
	public final static int DATAQLONG = 4096;
	public final static int MAXVAR = 1000;
	public final static int DEBUG = TRUE;
	public final static String SEPARATEUR = "\t";
	public final static String SEPARATEUR_CHAINE = "|";
	public final static String SEPARATEUR_DOSSIER = "&";
	public final static char SEPARATEUR_CHAR = '\t';
	public final static char SEPARATEUR_CHAINE_CHAR = '|';
	public final static char SEPARATEUR_DOSSIER_CHAR = '&';
	//public final static String CODEPAGE = "Cp1252"; //"ISO-8859-1";//"UTF-8";
	public final static String COD_CRLF="#CRLF#";
	public final static String crlf = System.getProperty("line.separator");
	public final static char[] SAUTLIGNE_CHAR=crlf.toCharArray();
	public final static String SAUTPAGE = "�&&";
	public final static char[] SAUTPAGE_CHAR=SAUTPAGE.toCharArray();

	public final static char SEPARATEUR_DECIMAL_CHAR=',';
	public final static String SEPARATEUR_DECIMAL=",";
	public final static char SEPARATEUR_MILLIER_CHAR='.';
	public final static String SEPARATEUR_MILLIER=".";
	public final static int IMG_SYS=0;
	public final static int IMG_EQU=1;
	public final static int IMG_VAR=2;
	public final static int PLAIN=0;
	public final static int BOLD=1;
	public final static int ITALIC=2;
	public final static String THUMBAIL="p_";
	public final static char SEPARATEUR_DATE_CHAR = '/';

	public final static char TYPE_ERR_SIMPLE='0';
	
	// Num�ros des requ�tes
	public final static int STOP = 0;
	public final static int CONNEXION_OK = 1;
	public final static int DEMANDE_OUVERTURE_PANEL_SESSION = 2;
	public final static int DEMANDE_LISTE_RT = 3;
	public final static int ENVOI_LISTE_RT = 4;
	public final static int DEMANDE_FICHIER_RT = 5;
	public final static int ENVOI_FICHIER_RT = 6;
	public final static int RECOIT_FICHIER_RT = ENVOI_FICHIER_RT;
	public final static int ENVOI_BUFFER_GFX = 7;
	public final static int RECOIT_BUFFER_GFX = ENVOI_BUFFER_GFX;
	public final static int ENVOI_BUFFER_RPG = 8;
	public final static int RECOIT_BUFFER_RPG = ENVOI_BUFFER_RPG;
	public final static int DEMANDE_LISTE_RTCONFIG = 9;
	public final static int ENVOI_LISTE_RTCONFIG = 10;
	public final static int OUVERTURE_SESSION_OK = 11;
	public final static int DEMANDE_DEMARRE_SESSION = 12;
	public final static int DEMANDE_OUVERTURE_TRANSFERT_SESSION = 19;
	public final static int FORCE_AFFICHE_PANEL = 20;
	public final static int TRF_LIGNE_FIC_CSV = 21;
	public final static int ENVOI_ENREGISTREMENT_MENU=22;
	public final static int RECOIT_ENREGISTREMENT_MENU=ENVOI_ENREGISTREMENT_MENU;
	public final static int RECOIT_CHOIX_MENU=23;
	public final static int ENVOI_CHOIX_MENU=RECOIT_CHOIX_MENU;
	public final static int DEMANDE_STOP_SESSION=24;
	public final static int RECOIT_INFOS_USER=25;
	public final static int ENVOI_INFOS_USER=RECOIT_INFOS_USER;
	public final static int DEMANDE_OUVERTURE_MIRE_SESSION=26;
	public final static int DEMANDE_FICHIER = 27;
	public final static int ENVOI_FICHIER = 28;
	public final static int RECOIT_FICHIER = ENVOI_FICHIER;
	public final static int DEMANDE_CONTENU_DOSSIER = 29;
	public final static int ENVOI_CONTENU_DOSSIER = 30;
	public final static int RECOIT_CONTENU_DOSSIER = ENVOI_CONTENU_DOSSIER;
	public final static int DEMANDE_PROTOCOLE_SERVEUR = 31;
	public final static int RECOIT_PROTOCOLE_SERVEUR = DEMANDE_PROTOCOLE_SERVEUR;
	public final static int ENVOI_DSPF = 32;
	public final static int RECOIT_DSPF = ENVOI_DSPF;
	public final static int ENVOI_REQUETE_EDITION = 33;
	public final static int RECOIT_REQUETE_EDITION = ENVOI_REQUETE_EDITION;
	public final static int PING = 34;					// Permet de tester la validit� d'une connexion de la part du serveur
	public final static int PONG = PING;
	public final static int UPLOAD_FICHIER = 35;
	public final static int SUPPRIME_FICHIER = 36;
	public final static int ENVOI_REQUETE_SYSTEME = 37;
	public final static int RECOIT_REQUETE_SYSTEME = ENVOI_REQUETE_SYSTEME;

	public final static int ANALYSE_DSPF = 1000000;
	public final static int HTTP_GET = 1195725856;
	public final static int HTTP_POST = 1347375956;

	// Propres au projet
	public final static String MAILTO="seriem.toulouse@seriem.com";
	public final static int SOCKET_TIMEOUT=0; // en ms = 1H si =0 alors ne coupe jamais
	public final static int DATAQ_TIMEOUT=3600000;

	public final static String VERSION_PROTOCOLE="01.02";
	public final static String VERSION_SERVER=VERSION_PROTOCOLE+".01";
	public final static String VERSION_CLIENT=VERSION_PROTOCOLE+".01";
	public final static int SERVER_PORT = 80;
	public final static String INIT_CONFIG = "config.equ";
	public final static String INIT_LIBIMG = "libsimages.equ";
	public final static String INIT_TRADUCTION = "traduction_fr.equ";
	public final static String DOSSIER_IMAGE="images";
	public final static String DOSSIER_RACINE="sgm";
	public final static String DOSSIER_LOG="logs";
	public final static String DOSSIER_RT = "rt";
	public final static String DOSSIER_WEB="www";
	public final static String DOSSIER_TMP="tmp";
	public final static String DOSSIER_CONFIG="config";
	public final static String DOSSIER_CTRL="ctrl";
	public final static String DOSSIER_METIER = "metier";
	public final static String FIC_CONFIG="rt.ini";
	
	public final static String FICHIER_ID="/console/conf.xml";
	
	//couleurs d�finies dans le nouveau look.
	public final static Color COULEUR_CONTENU = new Color(239,239,222);
	public final static Color COULEUR_POP_CONTENU = new Color(238,238,210);
	public final static Color COULEUR_MENUS = new Color(238,239,241);
	public final static int[] COULEUR_CENTRAGE_FONCE = {90,90,90};
	public final static int[] COULEUR_CENTRAGE_CLAIR = {198,198,200};
	public final static Color COULEUR_ENABLED = new Color(125,125,125);
	public final static Color COULEUR_F1 = new Color(90,90,90);
	public final static Color COULEUR_ERREURS = new Color(106, 23, 21);
	public final static Color COULEUR_NEGATIF = new Color(204, 0, 0);
	public final static Color COULEUR_LISTE_COMMENTAIRE = new Color(0,102,255);
	public final static Color COULEUR_LISTE_ANNULATION = new Color(140,140,140);
	public final static Color COULEUR_LISTE_FOND_BILAN = new Color(57,105,138);
	public final static Color COULEUR_LISTE_BILAN = Color.white;
	public final static Color COULEUR_LISTE_ERREUR = new Color(255,44,47);
	public final static Color COULEUR_TEXTE_DESACTIVE = new Color(77,77,80);
	public final static Color CL_DEGRADE_CLAIR = new Color(198,198,198);
	public final static Color CL_DEGRADE_FONCE = new Color(130,130,130);
	public final static Color CL_BT_CLAIR = new Color(247,248,248);
	public final static Color CL_BT_FONCE = new Color(212,215,221);
	public final static Color CL_BT_BORD = new Color(110,110,110);
	public final static Color CL_ZONE_SORTIE = new Color(244,244,237);
	
	public final static Color CL_TEXT_SORTIE = new Color(0,0,0);

	public final static String EXT_BIN=".dat";
	
	//polices de XRITABLE
	//public final static Font POLICE_HEAD_LIST = CustomFont.loadFont("fonts/VeraMono.ttf", 12); //new Font("Monospaced", Font.PLAIN, 12); // Ne pas mettre Courier New car pose soucis dans les listes
	//public final static Font POLICE_LIST = CustomFont.loadFont("fonts/VeraMono.ttf", 12);      // notamment VCGM73
	public final static Font POLICE_HEAD_LIST_SS = new Font("SansSerif", Font.PLAIN, 12);
	public final static Font POLICE_LIST_SS = new Font("SansSerif", Font.PLAIN, 12);
	

	// Varaiables pour la convertion des caract�res unicodes
	private static String code[]={"\\u00e9", "\\u00e8", "\\u00e0", "\\u00f4", "\\u00e2", "\\u00e4", "\\u00e7", "\\u00ea", "\\u00eb", "\\u00ee", "\\u00ef", "\\u00f6", "\\u00f9", "\\u00fb", "\\u00fc", "\\u00b0", "\\u00f3", "\\u20ac"};
	private static String lettre[]={"\u00e9", "\u00e8", "\u00e0", "\u00F4", "\u00e2", "\u00e4", "\u00e7", "\u00ea", "\u00eb", "\u00ee", "\u00ef", "\u00f6", "\u00f9", "\u00fb", "\u00fc", "\u00b0", "\u00f3", "\u20ac"};
	private static String lettreori[]={"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�"};
	public static char[] spaces={' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', // 260 espaces
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
	};

	/**
	 * Conversion des caract�res unicodes
	 * @param liste
	 * @return
	 */
	public static StringBuffer code2unicode(StringBuffer bchaine)
	{
		// Analyse de la liste
		for (int j=0; j< code.length; j++)
		{
			int k=bchaine.indexOf(code[j]);
			while (k != -1)
			{
				bchaine.replace(k, k+6, lettre[j]);
				k=bchaine.indexOf(code[j], k);	
			}
		}
		return bchaine;
	}

	/**
	 * Conversion des caract�res unicodes
	 * @param liste
	 * @return
	 */
	public static String[] code2unicode(String liste[])
	{
		//StringBuffer bchaine=null; 

		// analyse de la liste
		for (int i=0; i<liste.length; i++)
		{
			//bchaine = new StringBuffer(liste[i]);
			liste[i] = code2unicode(new StringBuffer(liste[i])).toString();
			/*
			for (int j=0; j< code.length; j++)
			{
				int k=bchaine.indexOf(code[j]);
				while (k != -1)
				{
					bchaine.replace(k, k+6, lettre[j]);
					k=bchaine.indexOf(code[j], k);	
				}
			}
			liste[i] = bchaine.toString();
			*/
		}
		return liste;
	}

	/**
	 * Conversion des caract�res unicodes
	 * @param chaine
	 * @return
	 */
	public static String code2unicode(String chaine)
	{
		//StringBuffer bchaine=null; 

		// analyse da la chaine
		//bchaine = new StringBuffer(chaine);
		return code2unicode(new StringBuffer(chaine)).toString();
		/*
		for (int j=0; j< code.length; j++)
		{
			int k=bchaine.indexOf(code[j]);
			while (k != -1)
			{
//				System.out.println("->" + bchaine.toString());
				bchaine.replace(k, k+6, lettre[j]);
				k=bchaine.indexOf(code[j], k);	
			}
		}

		return bchaine.toString();
		*/
	}

	/** TODO renommer la m�thode en ascii2code ou mieux
	 * Conversion des caract�res ascii en code
	 * @param chaine
	 * @return
	 */
	public static StringBuffer lettreori2code(StringBuffer bchaine)
	{
		// analyse da la chaine
		for (int j=0; j< code.length; j++)
		{
			int k=bchaine.indexOf(lettreori[j]);
			while (k != -1)
			{
				bchaine.replace(k, k+1, code[j]);
				k=bchaine.indexOf(lettreori[j], k);	
			}
		}

		return bchaine;
	}

	/** TODO renommer la m�thode en ascii2code ou mieux
	 * Convertion des caract�res ascii en code
	 * @param chaine
	 * @return
	 */
	public static String lettreori2code(String chaine)
	{
		return lettreori2code(new StringBuffer(chaine)).toString();
		/*
		StringBuffer bchaine=null; 

		// analyse da la chaine
		bchaine = new StringBuffer(chaine);
		for (int j=0; j< code.length; j++)
		{
			int k=bchaine.indexOf(lettreori[j]);
			while (k != -1)
			{
				bchaine.replace(k, k+1, code[j]);
				k=bchaine.indexOf(lettreori[j], k);	
			}
		}

		return bchaine.toString();
		*/
	}

	/**
	 * Retourne une liste g�n�r� depuis une chaine
	 */
	public static String[] splitString(String chaine, char separateur)
	{
		ArrayList<String> vlisting = splitString2List(chaine, separateur);
		String[] liste = new String[vlisting.size()];
		for (int i=0; i<vlisting.size(); i++)
			liste[i] = (String)vlisting.get(i);

		return liste;
	}	

	/**
	 * Retourne une liste g�n�r� depuis une chaine
	 */
	public static String[] splitString(String chaine, String separateur)
	{
		ArrayList<String> vlisting = splitString2List(chaine, separateur);
		String[] liste = new String[vlisting.size()];
		for (int i=0; i<vlisting.size(); i++)
			liste[i] = (String)vlisting.get(i);

		return liste;
	}	

	/**
	 * Retourne une liste g�n�r� depuis une chaine
	 */
	public static ArrayList<String> splitString2List(String chaine, char separateur)
	{
		int position=0, i=0;
		ArrayList<String> vlisting = new ArrayList<String>(50);

		position=chaine.indexOf(separateur, i);
		while (position != -1)
		{
//			System.out.println(" ---> liste1 : " + position + " " +chaine.substring(i, position));
			if (i == position)
				vlisting.add("");
			else
				vlisting.add(chaine.substring(i, position));
			i = position+1;
			position=chaine.indexOf(separateur, i);
		}
//		System.out.println(" ---> liste1 : " + position + " " +chaine.substring(i));
		vlisting.add(chaine.substring(i));

		return vlisting;
	}	

	/**
	 * Retourne une liste g�n�r� depuis une chaine
	 */
	public static ArrayList<String> splitString2List(String chaine, String separateur)
	{
		int position=0, i=0, lg_sep=separateur.length();
		ArrayList<String> vlisting = new ArrayList<String>(50);

		position=chaine.indexOf(separateur, i);
		while (position != -1)
		{
//			System.out.println(" ---> liste1 : " + position + " " +chaine.substring(i, position));
			if (i == position)
				vlisting.add("");
			else
				vlisting.add(chaine.substring(i, position));
			i = position+lg_sep;
			position=chaine.indexOf(separateur, i);
		}
//		System.out.println(" ---> liste1 : " + position + " " +chaine.substring(i));
		vlisting.add(chaine.substring(i));

		return vlisting;
	}	

	/**
	 * Conversion des couleurs RGB en entier
	 * @param r,g,b
	 * @return
	 */
	public static int RGB2int(int r, int g, int b)
	{
		return (r << 16) + (g << 8) + b;
	}

	/**
	 * Permet de mettre le look du syst�me par d�fault 
	 * @param panel
	 */
	public static void setLook(String lf)
	{
		// Ou en argument sur la ligne de commande 
		// java -Dswing.defaultlaf=com.sun.java.swing.plaf.windows.WindowsLookAndFeel ...
		// -> javax.swing.plaf.metal.MetalLookAndFeel
		// -> com.sun.java.swing.plaf.motif.MotifLookAndFeel
		// -> com.sun.java.swing.plaf.windows.WindowsLookAndFeel (� priori bug sous Vista avec les JFilechooser [6.03])

		// FIXME Probl�me avec Vista au niveau des JFileChooser (ca crache) si execution depuis Eclipse
		// par contre en ligne de commande aucun pb
		//if ((System.getProperty("os.name") + " " +System.getProperty ( "os.version" )).trim().equals("Windows XP 5.1"))
		//    if ((lf == null) || ((lf != null) && (lf.equals("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"))))
		//	return;

		if (lf != null)
		{
			try
			{
				UIManager.setLookAndFeel(lf);
			}
			catch(Exception ex) {}
		}
		else
		{
			if (!System.getProperty("os.name").equalsIgnoreCase("Linux"))
			{	
				try
				{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					//SwingUtilities.updateComponentTreeUI(panel);
				}
				catch(Exception e) 
				{ 
					System.out.println("PB de look."); 
				}
			}
			else // Sinon on enleve le gras de la police pour le th�me Metal
				UIManager.put("swing.boldMetal", Boolean.FALSE);
		}
	}

	/**
	 * Retourne la date avec gestion d'un d�lai en jour (en plus ou moins)
	 * @param delai
	 * @return
	 */
	public static int getDate(int delai)
	{
		Calendar c = GregorianCalendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_MONTH, delai);

		return c.get(Calendar.YEAR)*10000 + (c.get(Calendar.MONTH)+1)*100 + c.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Retourne la date
	 * @return
	 */
	public static int getDate()
	{
		return getDate(0);
	}

	/**
	 * Retourne l'heure avec gestion d'un d�lai en seconde (en plus ou moins)
	 * @param delai
	 * @return
	 */
	public static int getHeure(int delai)
	{
		Calendar c = GregorianCalendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.SECOND, delai);

		return c.get(Calendar.HOUR_OF_DAY)*10000 + (c.get(Calendar.MINUTE)+1)*100 + c.get(Calendar.SECOND);
	}

	/**
	 * Retourne l'heure
	 * @return
	 */
	public static int getHeure()
	{
		return getHeure(0);
	}

	/**
	 * Supprime les fichiers de log trop anciens
	 * @param dossier
	 * @param tpsconser
	 */
    public static void SuppressionLogMessage(String dossier, int tpsconser)
    {
        int i=0;
        if (dossier == null) return;

        // Construction du nom du fichier de log
        int intfichier = getDate(-tpsconser);

        File dossierlog = new File(dossier);
        if (!dossierlog.exists()) return;

        // On liste le contenu du dossier & on le tri
        File[] listefichierlog = dossierlog.listFiles();
        int[] listefichierlogint = new int[listefichierlog.length];
        for (i=0; i<listefichierlog.length; i++)
        	if (listefichierlog[i].isFile())
        		try
		        {
		        	listefichierlogint[i] = Integer.parseInt(listefichierlog[i].getName().substring(0, 8));
		        }
		        catch(Exception e){}

        // On supprime les fichiers de log sup�rieurs au d�lai
        i = 0;
        while (i<listefichierlog.length)
        {
        	if (intfichier >= listefichierlogint[i])
        		listefichierlog[i].delete();
        	i++;
        }
    }
    
}
