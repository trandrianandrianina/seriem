//=================================================================================================
//==>                                                                       07/07/2014 - 07/07/2014
//==> Chargement des polices personnalisées
//==> A faire:
//==> Site: http://ftp.gnome.org/pub/GNOME/sources/ttf-bitstream-vera/1.10/
//=================================================================================================

package base.outils;

import java.awt.Font;

public class CustomFont
{
	/**
     * Charge des polices personnalisées
     * @param path
     * @param size
     * @return
     */
    static public Font loadFont(String path, float size)
    {
    	try
		{
    		Font font = Font.createFont(Font.TRUETYPE_FONT, CustomFont.class.getClassLoader().getResourceAsStream(path));
    		return font.deriveFont(size);
		}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	/*
		File font_monaco = new File("C:\\Documents and Settings\\Administrateur\\Bureau\\liberation-fonts-ttf-2.00.1\\VeraMono.ttf");
		try
		{
			Font font2 = Font.createFont(Font.TRUETYPE_FONT, font_monaco);
			return font2.deriveFont(size);
		}
		catch (FontFormatException e)
		{
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}*/
		return new Font("Courier New", Font.PLAIN, (int)size);
    }
}
