//=================================================================================================
//==>                                                                       12/01/2010 - 16/02/2010
//==> G�re la saisie de la variable typeSaisie de RiCalendrier
//==> A faire:
//=================================================================================================

package gfx;

import java.beans.PropertyEditorSupport;

public class TypeSaisieEditor extends PropertyEditorSupport
{
	private String[] options={RiCalendrier.JJMMAA, RiCalendrier.MMAA};
	private int[] values={RiCalendrier.SAISIE_JJMMAA, RiCalendrier.SAISIE_MMAA};
	protected int typeSaisie=RiCalendrier.SAISIE_JJMMAA;

	public String getJavaInitializationString()
	{ 
       return ""+typeSaisie; 
	}

	public void setValue(Object obj)
	{
		typeSaisie = ((Integer) obj).intValue();
	}
 
	public Object getValue()
	{
		return new Integer(typeSaisie);
	}

	public String[] getTags()
	{
		return options;
	}
	
	public String getAsText()
	{
		int valeur=((Integer)getValue()).intValue();
		for (int i=0; i<values.length; i++)
			if (values[i] == valeur)
				return options[i];
		return options[0];
	}
	
	public void setAsText(String s)
	{
		for (int i=0; i<options.length; i++)
			if (options[i].equals(s))
			{
				setValue(values[i]);
				return;
			}
	}
	
}
