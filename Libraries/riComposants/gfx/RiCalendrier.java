//=================================================================================================
//==>                                                                       11/01/2010 - 09/11/2011
//==> Composant datepicker am�nag� pour nos besoins
//==> A faire:
//==>    Probl�me avec typesaisie (dans les propri�t�s) le TypeSaisieEditor n'est pas pris en compte
//==>	 Emp�cher la saisie du bouton lorsque la zone est jug�e NonEditable()
//=================================================================================================

package gfx;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFormattedTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.JXDatePicker;

/**
 * Cette classe permet d'instancier un calendrier de type JXDatePicker format� pour les dates de S�rie M.
 * @author David Biason
 * @version 2.1
 * */
public class RiCalendrier extends JXDatePicker
{
	// Constantes
	private static final long serialVersionUID = 1L;
	public static final String JJMMAA = "JJMMAA";
	public static final String MMAA = "MMAA";
	public static final int SAISIE_JJMMAA = 8;
	public static final int SAISIE_MMAA = 6;
	
	//Variables
	private int typeSaisie=SAISIE_JJMMAA;
	private boolean selectionComplete = true;
	
	/**
	 * Cet attribut est un tableau qui repr�sente les chaines de caract�res autoris�es lors de la saisie de la date. La premi�re chaine est le format par d�faut. 
	 * */
	private String[][] formatsDates={ {"dd.MM.yy","ddMMyy","MMyy",".MM.yy"},{"MM.yy","MMyy"}};
	
	/**
	 * Cet attribut est un masque de saisie permettant de forcer l'utilsateur � saisir certains types de caract�res dans un ordre donn�. Attention,actif seulement lors de la premi�re saisie !!!
	 * */
	MaskFormatter moule = null;
	
	/**
	 * Cet attribut est l'�diteur permettant � l'utilisateur de saisir manuellement une date dans une zone format�e. Voir <a href="http://java.sun.com/j2se/1.4.2/docs/api/javax/swing/JFormattedTextField.html">JFormattedTextField</a>
	 * */
	JFormattedTextField zoneSaisie = new JFormattedTextField();
	
	/**
	 * Constructeur prenant le type de date (<b>JJ.MM.AA (8)</b> ou <b>MM.AA(5)</b>) en param�tre.
	 * @param type ce param�tre repr�sente le type de date donn� ( c'est � dire 8 caract�res pour les dates compl�tes JJ.MM.AA ou 5 pour les dates MM.AA
	 * */
	public RiCalendrier(int type)
	{
		super();
		setInit();
		setTypeSaisie(type);
	}
	
	/**
	 * Constructeur � vide -> il appelle le constructeur de type 8 par d�faut
	 * */
	public RiCalendrier()
	{
		this(SAISIE_JJMMAA);
	}
	
	/**
	 * Valider automatiquement la s�lection saisie dans le textfield
	 */
	public void validerSelection()
	{
		// Envoyer la saisie au bloc Calendrier
		try
		{
			this.commitEdit();
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	/**
	 * permet de renvoyer le type de saisie (MMAA ou JJMMAA)
	 * @return typeSaisie
	 */
	public int getTypeSaisie()
	{
		return typeSaisie;
	}
	
	/**
	 * permet de modifier le type de saisie (MMAA ou JJMMAA)
	 * */
	public void setTypeSaisie(int typeSaisie)
	{
		this.typeSaisie = typeSaisie;
		if (typeSaisie == SAISIE_JJMMAA)
		{
			this.setFormats(formatsDates[0]);
			this.setSize(115, this.getPreferredSize().height);
			//modele = "##.##.##";
		}
		else
			if (typeSaisie == SAISIE_MMAA)
			{
				this.setFormats(formatsDates[1]);
				this.setSize(100, this.getPreferredSize().height);
				//modele = "##.##";
			}
	}
	
	/**
	 * Initialise le format de saisie
	 */
	private void setInit()
	{
		// Interception de la touche ENTER (pour l'activation du d�faut bouton)
		zoneSaisie.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e)
			{
				KeyEvent key = (KeyEvent)e;
				if (key.getKeyCode()== KeyEvent.VK_ENTER)
					seBarrer();	
			}
		});

		// G�rer les pertes et les gains de focus de la zone de saisie
		zoneSaisie.addFocusListener(new FocusListener()	{
			public void focusGained(FocusEvent arg0) 
			{
				if(selectionComplete)
				{
					if((!zoneSaisie.getText().equals("  .  "))&&(!zoneSaisie.getText().equals(""))&&(!zoneSaisie.getText().equals("  .  .  ")))
					{
						if(typeSaisie == SAISIE_JJMMAA && zoneSaisie.getText().length()==8)
							SwingUtilities.invokeLater(new Runnable() { public void run(){ zoneSaisie.selectAll();}});
						else SwingUtilities.invokeLater(new Runnable() { public void run(){ zoneSaisie.setCaretPosition(0);}});
					}
				}
				else
				{
					if((!zoneSaisie.getText().equals("  .  "))&&(!zoneSaisie.getText().equals(""))&&(!zoneSaisie.getText().equals("  .  .  ")))
						SwingUtilities.invokeLater(new Runnable() { public void run(){ zoneSaisie.setCaretPosition(0);}});
				}
			}

			public void focusLost(FocusEvent arg0) 
			{
				validerSelection();
			}
		});

		
		this.setEditor(zoneSaisie);
	}
	
	/**
	 * Modifie le type de s�lection automatique sur prise de focus de la zone de saisie
	 *
	 */
	public void setSelectionComplete(boolean select)
	{
		selectionComplete = select;
	}
	
	/**
	 * Acc�de au type de s�lection automatique sur prise de focus de la zone de saisie
	 *
	 */
	public boolean isSelectionComplete()
	{
		return selectionComplete;
	}
	
	public void seBarrer()
	{
		try
		{
			getRootPane().getDefaultButton().doClick();
		}
		catch(Exception e)
		{
			System.out.println("[RiCalendrier] Pas de d�faut Bouton dans ce panel.");
		}
	}

}
