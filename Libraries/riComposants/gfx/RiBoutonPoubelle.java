package gfx;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class RiBoutonPoubelle extends JButton 
{
	private static final long serialVersionUID = -356312598814057138L;

	public RiBoutonPoubelle()
	{
		super();
		
		setPreferredSize(new Dimension(28,28));
		setMinimumSize(new Dimension(28,28));
		setMaximumSize(new Dimension(28,28));
		setBorderPainted(false);
		setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/poubelle_gris.png")));
		setToolTipText("Annulation");
		
		addMouseListener(new MouseListener() {
			private ImageIcon poubelle = new ImageIcon(getClass().getClassLoader().getResource("images/poubelle_bleue.png"));
			private ImageIcon poubelleBleue = new ImageIcon(getClass().getClassLoader().getResource("images/poubelle_gris.png"));
			
			public void mouseReleased(MouseEvent arg0){}
			
			public void mousePressed(MouseEvent arg0){}
			
			public void mouseExited(MouseEvent arg0){
				setIcon(poubelle);	
			}
			
			public void mouseEntered(MouseEvent arg0){
				setIcon(poubelleBleue);	
			}
			
			public void mouseClicked(MouseEvent arg0){}
		});
	}
}
