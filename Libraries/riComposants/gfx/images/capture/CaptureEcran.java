//=================================================================================================
//==>                                                                       23/02/2010 - 18/12/2014
//==>  Gestion des catures d'�cran
//==> A faire:
//=================================================================================================

package gfx.images.capture;

import java.awt.AWTException;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import base.clipboard.Clipboard;
import base.outils.GestionFiltre;

public class CaptureEcran
{
	// Variables
	private Robot robot=null;
	private Rectangle zoneCapture=new Rectangle(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
	private BufferedImage image=null;
	private File dernierPath=null;
	private JFileChooser choixFichier=null;
	private int compteurImg=0;
	private JFrame fenetreApplication=null;


	/**
	 * Constructeur
	 * @param type
	 */
	public CaptureEcran()
	{
		try
		{
			robot = new Robot();
		}
		catch (AWTException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Constructeur
	 * @param type
	 */
	public CaptureEcran(JFrame frame)
	{
		this();
		fenetreApplication = frame;
	}

	/**
	 * Constructeur
	 * @param type
	 */
	public CaptureEcran(Rectangle zone)
	{
		this();
		setZoneCapture(zone);
	}

	/**
	 * Initialise la zone de capture �cran
	 * @param zone
	 */
	public void setZoneCapture(Rectangle zone)
	{
		if (zone == null)
			zoneCapture=new Rectangle(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
		else
			zoneCapture = zone; 
	}

	/**
	 * Capture une zone �cran
	 * @param zone
	 */
	public BufferedImage capture(Rectangle zone)
	{
		if (robot == null) return null;
		setZoneCapture(zone);
		
		// Capture de la zone
		image = robot.createScreenCapture(zoneCapture);
		return image;
	}

	/**
	 * Capture une zone �cran
	 * @param zone
	 */
	public BufferedImage capture(JPanel panel)
	{
		if (robot == null) return null;
		
		// Capture de la zone
		image = new BufferedImage(panel.getSize().width, panel.getSize().height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		panel.paint(g);
		g.dispose();
		
		return image;
	}

	/**
	 * Capture en plein �cran 
	 *
	 */
	public BufferedImage capture()
	{
		//capture(null);
		zoneCapture=new Rectangle(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
		// Capture de la zone
		image = robot.createScreenCapture(zoneCapture);
		return image;
	}
	
	/**
	 * Enregistre l'image captur�e � l'aide d'une boite de dialogue
	 *
	 */
	public void enregistreCapture()
	{
		if (choixFichier == null)
		{
			choixFichier = new JFileChooser();
			choixFichier.addChoosableFileFilter(new GestionFiltre(new String[]{"gif"}, "Fichier image (*.gif)"));
			choixFichier.addChoosableFileFilter(new GestionFiltre(new String[]{"png"}, "Fichier image (*.png)"));
			choixFichier.addChoosableFileFilter(new GestionFiltre(new String[]{"jpg"}, "Fichier image (*.jpg)"));
		}

		//	On se place dans le dernier dossier courant s'il y en a un
		if (dernierPath != null)
			choixFichier.setCurrentDirectory(dernierPath);
		
		File fichier = new File("image" + (compteurImg++<10?"0"+compteurImg:compteurImg) + ".jpg");
		choixFichier.setSelectedFile(fichier);
		if (choixFichier.showSaveDialog(fenetreApplication) == JFileChooser.APPROVE_OPTION)
			saveImage(choixFichier.getSelectedFile());
		
		// On stocke comme le dossier courant
		dernierPath = choixFichier.getCurrentDirectory();
	}
	
	/**
	 * Permet de redimentionner la capture  
	 * @param ratio
	 */
	public void resize(float ratio)
	{
		int w = (int) (image.getWidth() * ratio);
		int h = (int) (image.getHeight() * ratio);

		BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = newImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(image, 0, 0, w, h, null);
		g.dispose();
		image = newImage;	
	}
	
	/**
	 * Enregistre l'image sur disque
	 * @param extension
	 * @param fichier
	 */
	public void saveImage(File fichier)
	{
		if (fichier == null) return;
		
		// R�cup�ration de l'extension
		String extension = fichier.getName().toLowerCase();
		int pos = extension.lastIndexOf('.');
		if (pos != -1)
			extension = extension.substring(pos+1);
		
		if ( extension.equals("gif") || extension.equals("png") || extension.equals("jpg"))
		{
			// Ecriture du fichier
			try
			{
				ImageIO.write(image, extension, fichier);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
			JOptionPane.showMessageDialog(null, "Extension de fichier non reconnue", "Capture �cran", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Envoi la capture dans le presse papier
	 * @param width
	 * @param height
	 */
	public void sendToClipBoard()
	{
		if (image != null)
			Clipboard.sendImageToClipBoard(image);
	}
	
	/**
	 * Nettoyage des variables
	 */
	public void dispose()
	{
		image = null;
		fenetreApplication = null;
	}
}
