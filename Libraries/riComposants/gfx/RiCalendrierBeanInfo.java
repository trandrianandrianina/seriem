//=================================================================================================
//==>                                                                       11/01/2010 - 26/02/2010
//==> BeanInfo pour le composant RiCalendrier
//==> A faire:
//=================================================================================================

package gfx;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import javax.swing.JOptionPane;

import org.jdesktop.swingx.JXDatePickerBeanInfo;

public class RiCalendrierBeanInfo extends JXDatePickerBeanInfo
{
	public PropertyDescriptor[] getPropertyDescriptors()
	{
		try 
		{
			// Type de saisie du calendrier
			PropertyDescriptor saisie = new PropertyDescriptor("typeSaisie", RiCalendrier.class );
			saisie.setPropertyEditorClass(TypeSaisieEditor.class);

			// Active la selection complete du calendrier
			PropertyDescriptor selection = new PropertyDescriptor("selectionComplete", RiCalendrier.class );

			PropertyDescriptor[] superProp = super.getPropertyDescriptors();
	  		PropertyDescriptor[] Prop = new PropertyDescriptor[superProp.length+2];
	  		System.arraycopy(superProp, 0, Prop, 0, superProp.length);
	  		Prop[Prop.length-2] = saisie;
	  		Prop[Prop.length-1] = selection;
			return Prop;
			//return new PropertyDescriptor[]{saisie};
		}
		catch (IntrospectionException e)
		{
			JOptionPane.showMessageDialog(null, e, "Debug", JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}

}
