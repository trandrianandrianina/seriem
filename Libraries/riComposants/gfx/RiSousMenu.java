package gfx;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JMenuBar;

public class RiSousMenu extends JMenuBar 
{
	public RiSousMenu()
	{
		super();
		setMargin(new Insets(0, -5, 0, 0));
		setPreferredSize(new Dimension(170, 26));
		
	}
}
