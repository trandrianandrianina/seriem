package gfx;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;

import base.Constantes;

public class RiBouton extends JButton 
{
	private static final long serialVersionUID = 1L;
	private Color topColor;
    private Color bottomColor;
    private Color borderColor;
    private Color foreGroundColor;
    private boolean isSelectionne = false;
    
    public RiBouton() {
        this(Constantes.CL_BT_CLAIR, Constantes.CL_BT_FONCE);
        //inset top/gauche/bottom/droite
        setMargin(null); 
    }
    
    public RiBouton(Color c1, Color c2) 
    {
    	this.foreGroundColor = getForeground();
        this.topColor    = c1;
        this.bottomColor = c2;
        this.borderColor = Constantes.CL_BT_BORD;
        this.setBorder(BorderFactory.createLineBorder(borderColor));
        setBackground(bottomColor);
        this.setContentAreaFilled(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        
        this.addFocusListener(new FocusListener() {
			 
            public void focusGained(FocusEvent ev1) {
            
            	JButton bt = (JButton) ev1.getSource();
            	bt.setOpaque(true);
            }
 
            public void focusLost(FocusEvent ev2) 
            {
            	JButton bt = (JButton) ev2.getSource();
            	bt.setOpaque(isSelectionne);
            }
        });
        
        this.addMouseListener(new MouseListener() {
			
			public void mouseReleased(MouseEvent arg0) {}
			
			public void mousePressed(MouseEvent arg0) {}
			
			public void mouseExited(MouseEvent arg0) {
				JButton bt = (JButton) arg0.getSource();
            	bt.setOpaque(isSelectionne);	
			}
			
			public void mouseEntered(MouseEvent arg0) {
				JButton bt = (JButton) arg0.getSource();
            	bt.setOpaque(true);		
			}
			
			public void mouseClicked(MouseEvent arg0) {}
		});
    }
    
    public void rendreSelectionne(boolean selectionne)
    {
    	isSelectionne = selectionne;
    	setOpaque(isSelectionne);
    	repaint();
    }
    
    public void setTopColor(Color c1) {
        this.topColor = c1;
        repaint();
    }
    
    public void setBottomColor(Color c2) {
        this.bottomColor = c2;
        repaint();
    }
    
    public void setBorderColor(Color color) {
        this.borderColor = color;
        repaint();
    }
    
    public boolean isSelectionne()
    {
    	return isSelectionne;
    }
    
    public void setEnabled(boolean enabled)
    {
    	super.setEnabled(enabled);

    	this.setForeground(enabled?foreGroundColor:Constantes.COULEUR_ENABLED);
    	repaint();
    }
    
    public void setLock(boolean locke)
    {
    	this.setEnabled(!locke);
    }
    
    protected void paintComponent(Graphics g) {
        final Graphics2D g2 = (Graphics2D) g;
        int w = getWidth();
        int h = getHeight();
        
        GradientPaint gradient = new GradientPaint(0, 0, topColor, 0, h, bottomColor, false);
        g2.setPaint(gradient);
        g2.fillRect(0, 0, w, h);
        super.paintComponent(g);
    }
}
