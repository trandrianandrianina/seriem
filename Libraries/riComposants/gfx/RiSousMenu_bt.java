package gfx;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class RiSousMenu_bt extends JButton 
{
	
	public RiSousMenu_bt()
	{
		super();
		
		setBackground(new Color(238,239,241));
		setPreferredSize(new Dimension(167, 26));
		setMinimumSize(new Dimension(167, 26));
		setBorderPainted(false);
		setContentAreaFilled(false);
		setMaximumSize(new Dimension(167, 26));
		//setFont(getFont().deriveFont(getFont().getStyle() - 1f));
		setFont(new Font(getFont().getFamily(),0,11));
		setHorizontalAlignment(SwingConstants.LEADING);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		setMargin(new Insets(0, 5, 0, -4));
		setIconTextGap(5);
		setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/puce.png")));
		
		/* 24/03/2014 - Comment� tant qu'on ne sait pas � quoi �a sert (Stef) 			
		this.addFocusListener(new FocusListener() {
			 
            public void focusGained(FocusEvent ev1) {
            
            	JButton bt = (JButton) ev1.getSource();
            	bt.setOpaque(true);
            }
 
            public void focusLost(FocusEvent ev2) 
            {
            	JButton bt = (JButton) ev2.getSource();
            	bt.setOpaque(false);
            }
        });*/
		
		this.addMouseListener(new MouseListener() {
			
			public void mouseReleased(MouseEvent arg0) {}
			
			public void mousePressed(MouseEvent arg0) {}
			
			public void mouseExited(MouseEvent arg0) {
				JButton bt = (JButton) arg0.getSource();
            	bt.setOpaque(false);	
			}
			
			public void mouseEntered(MouseEvent arg0) {
				JButton bt = (JButton) arg0.getSource();
            	bt.setOpaque(true);		
			}
			
			public void mouseClicked(MouseEvent arg0) {}
		});
	}
}
