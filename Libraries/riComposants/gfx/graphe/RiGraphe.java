//=================================================================================================
//==>                                                                       24/02/2010 - 08/11/2013
//==> Interface pour g�n�rer des graphes
//=================================================================================================

package gfx.graphe;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberAxis3D;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.LineRenderer3D;
import org.jfree.data.DefaultKeyedValues;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import base.Constantes;
import base.clipboard.Clipboard;
import base.outils.GestionFiltre;

public class RiGraphe {
	// Constantes
	public static final int GRAPHE_PIE3D = 0;
	public static final int GRAPHE_BAR3D = 1;
	public static final int GRAPHE_XY = 2;
	public static final int GRAPHE_LINE = 3;
	public static final int GRAPHE_LINE3D = 4;
	public static final int GRAPHE_COMBINE = 5;

	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;

	// Variables
	private int typeGraphe = GRAPHE_PIE3D;
	private JFreeChart graphe = null;
	private DefaultPieDataset pieDataset = null;
	private DefaultCategoryDataset catDataset = null;
	private CategoryDataset[] combiDataset = null;
	private XYDataset xyDataset = null;
	private String titreabs = null;
	private String titreord = null;
	private int orientation = VERTICAL;

	private File dernierPath = null;
	private JFileChooser choixFichier = null;
	private int compteurImg = 0;

	// couleurs
	private Color couleurDeFond = Constantes.COULEUR_POP_CONTENU;
	private Color couleurGraph = new Color(25, 100, 184);

	// pour graphe combin�
	private boolean[] histoOuligne = null;
	private Color[] couleursCombi = null;
	private boolean[] rgeAxis = null;

	private Font policeLegende = new Font("Tahoma", Font.BOLD, 9);
	private Font policeEtiquette = new Font("Tahoma", Font.PLAIN, 10);

	/**
	 * Constructeur
	 * 
	 * @param typegraphe
	 * @param donnee
	 */
	public RiGraphe(int typegraphe) {
		typeGraphe = typegraphe;

	}

	/**
	 * Constructeur
	 * 
	 * @param typegraphe
	 * @param donnee
	 * @param titrelegende
	 */
	public RiGraphe(int typegraphe, Object[][] donnee, String titrelegende) {
		typeGraphe = typegraphe;
		setDonnee(donnee, titrelegende, false);

	}

	/**
	 * Constructeur
	 * 
	 * @param typegraphe
	 * @param donnee
	 */
	public RiGraphe(int typegraphe, Object[][] donnee) {
		this(typegraphe, donnee, null);

	}

	/**
	 * Initialise les donn�es pour le graphe
	 * 
	 * @param typegraphe
	 * @param donnee
	 * @param titre
	 */
	public void setDonnee(Object[][] donnee, String titrelegende, boolean clear) {
		switch (typeGraphe) {
		case GRAPHE_PIE3D:
			setDataPie3D(donnee, clear);
			break;
		case GRAPHE_BAR3D:
			setDataBar3D(donnee, titrelegende, clear);
			break;
		case GRAPHE_XY:
			setDataXY(donnee, titrelegende, clear);
			break;
		case GRAPHE_LINE3D:
		case GRAPHE_LINE:
			setDataLine(donnee, titrelegende, clear);
			break;
		case GRAPHE_COMBINE:
			setDataCombi(donnee, titrelegende, clear);
			break;
		}
	}

	/**
	 * G�n�re un graphe
	 * 
	 * @param titre
	 * @param legend
	 * @return
	 */
	public JFreeChart getGraphe(String titre, boolean legend) {
		return getGraphe(titre, legend, true);
	}

	/**
	 * G�n�re un graphe
	 * 
	 * @param titre
	 * @param legend
	 * @return
	 */
	public JFreeChart getGraphe(String titre, boolean legend, boolean miseenforme) {
		switch (typeGraphe) {
		case GRAPHE_PIE3D:
			return getGraphePie3D(titre, legend, miseenforme);
		case GRAPHE_BAR3D:
			return getGrapheBar3D(titre, titreabs, titreord, orientation,
					legend);
		case GRAPHE_XY:
			return getGrapheXY(titre, titreabs, titreord, orientation, legend);
		case GRAPHE_LINE:
			return getGrapheLine(titre, titreabs, titreord, orientation, legend);
		case GRAPHE_LINE3D:
			return getGrapheLine3D(titre, titreabs, titreord, orientation,
					legend);
		case GRAPHE_COMBINE:
			return getGrapheCombine(titre, titreabs, titreord, orientation,
					legend);
		}
		return null;
	}

	/**
	 * Initialise les donn�es pour les graphes de type camembert 3D
	 * 
	 * @param donnee
	 * @param titre
	 */
	public void setDataPie3D(Object[][] donnee, boolean clear) {
		if (pieDataset == null)
			pieDataset = new DefaultPieDataset();
		if (clear)
			pieDataset.clear();
		for (int i = 0; i < donnee.length; i++)
			pieDataset.setValue((String) donnee[i][0], (Double) donnee[i][1]);
	}

	/**
	 * Initialise les donn�es pour les graphes de type barre 3D
	 * 
	 * @param donnee
	 * @param titre
	 */
	public void setDataBar3D(Object[][] donnee, String titrelegende,
			boolean clear) {
		if (catDataset == null)
			catDataset = new DefaultCategoryDataset();
		if (clear)
			catDataset.clear();
		for (int i = 0; i < donnee.length; i++)
			catDataset.addValue((Double) donnee[i][1], titrelegende,
					(String) donnee[i][0]);
	}

	/**
	 * Initialise les donn�es pour les graphes de type ligne TODO non v�rifi�
	 * 
	 * @param donnee
	 * @param titre
	 */
	public void setDataXY(Object[][] donnee, String titrelegende, boolean clear) {
		XYSeries series = new XYSeries("Average Size");
		for (int i = 0; i < donnee.length; i++)
			series.add((Double) donnee[i][0], (Double) donnee[i][1]);
		xyDataset = new XYSeriesCollection(series);
	}

	/**
	 * Initialise les donn�es pour les graphes de type ligne
	 * 
	 * @param donnee
	 * @param titre
	 */
	public void setDataLine(Object[][] donnee, String titrelegende,
			boolean clear) {
		if (catDataset == null)
			catDataset = new DefaultCategoryDataset();
		if (clear)
			catDataset.clear();
		for (int i = 0; i < donnee.length; i++)
			catDataset.addValue((Double) donnee[i][1], titrelegende,
					(String) donnee[i][0]);
	}

	/**
	 * Initialise les donn�es pour les graphes de type combin�
	 * 
	 * @param donnee
	 * @param titre
	 */
	public void setDataCombi(Object[][] donnee, String titrelegende,
			boolean clear) {
		DefaultKeyedValues data = new DefaultKeyedValues();
		if (combiDataset == null)
			combiDataset = new CategoryDataset[donnee.length];
		for (int i = 0; i < donnee.length; i++) {
			data = (DefaultKeyedValues) donnee[i][1];
			CategoryDataset dataset = DatasetUtilities.createCategoryDataset(
					(String) donnee[i][0], data);
			combiDataset[i] = (dataset);
		}
	}

	/**
	 * G�n�re un graphe de type Camembert en 3D
	 * 
	 * @param titre
	 * @param legend
	 * @return
	 */
	public JFreeChart getGraphePie3D(String titre, boolean legend) {
		return getGraphePie3D(titre, legend, true);
	}

	/**
	 * G�n�re un graphe de type Camembert en 3D
	 * 
	 * @param titre
	 * @param legend
	 * @return
	 */
	public JFreeChart getGraphePie3D(String titre, boolean legend, boolean miseenforme) {
		if (pieDataset == null)
			return null;
		// Cr�er un type de graphe
		graphe = ChartFactory.createPieChart3D(titre, pieDataset, legend, false, false);
		if (miseenforme)
			return miseEnFormeGraphe(graphe);
		else
			return graphe;
	}

	/**
	 * G�n�re un graphe de type barre 3D
	 * 
	 * @param titre
	 * @param titreabs
	 * @param titreord
	 * @param catDataset
	 * @param orientation
	 * @param legend
	 * @return
	 */
	public JFreeChart getGrapheBar3D(String titre, String titreabs,
			String titreord, int orientation, boolean legend) {
		if (catDataset == null)
			return null;
		graphe = ChartFactory.createBarChart3D(titre, titreabs, titreord,
				catDataset,
				orientation == HORIZONTAL ? PlotOrientation.HORIZONTAL
						: PlotOrientation.VERTICAL, legend, true, true);

		return miseEnFormeGraphe(graphe);
	}

	/**
	 * G�n�re un graphe de type ??? TODO � v�rifier
	 * 
	 * @param titre
	 * @param legend
	 * @return
	 */
	public JFreeChart getGrapheXY(String titre, String titreabs,
			String titreord, int orientation, boolean legend) {
		if (xyDataset == null)
			return null;
		// Cr�er un type de graphe
		graphe = ChartFactory.createXYAreaChart(titre, titreabs, titreord,
				xyDataset,
				orientation == HORIZONTAL ? PlotOrientation.HORIZONTAL
						: PlotOrientation.VERTICAL, legend, false, false);
		graphe.setBackgroundPaint(couleurDeFond);
		return miseEnFormeGraphe(graphe);
	}

	/**
	 * G�n�re un graphe de type ligne
	 * 
	 * @param titre
	 * @param titreabs
	 * @param titreord
	 * @param catDataset
	 * @param orientation
	 * @param legend
	 * @return
	 */
	public JFreeChart getGrapheLine(String titre, String titreabs,
			String titreord, int orientation, boolean legend) {
		if (catDataset == null)
			return null;
		graphe = ChartFactory.createLineChart(titre, titreabs, titreord,
				catDataset,
				orientation == HORIZONTAL ? PlotOrientation.HORIZONTAL
						: PlotOrientation.VERTICAL, legend, true, true);
		return miseEnFormeGraphe(graphe);
	}

	/**
	 * G�n�re un graphe de type ligne 3D
	 * 
	 * @param titre
	 * @param titreabs
	 * @param titreord
	 * @param catDataset
	 * @param orientation
	 * @param legend
	 * @return
	 */
	public JFreeChart getGrapheLine3D(String titre, String titreabs,
			String titreord, int orientation, boolean legend) {
		if (catDataset == null)
			return null;
		graphe = ChartFactory.createLineChart3D(titre, titreabs, titreord,
				catDataset,
				orientation == HORIZONTAL ? PlotOrientation.HORIZONTAL
						: PlotOrientation.VERTICAL, legend, true, true);
		return miseEnFormeGraphe(graphe);
	}

	/**
	 * G�n�re un graphe qui combine histograme et courbes...
	 * 
	 * @param titre
	 * @param titreabs
	 * @param titreord
	 * @param catDataset
	 * @param orientation
	 * @param legend
	 * @return
	 */
	public JFreeChart getGrapheCombine(String titre, String titreabs,
			String titreord, int orientation, boolean legend) {
		if (combiDataset == null)
			return null;
		graphe = ChartFactory.createBarChart3D(titre, titreabs, titreord,
				combiDataset[0],
				orientation == HORIZONTAL ? PlotOrientation.HORIZONTAL
						: PlotOrientation.VERTICAL, legend, true, true);

		graphe.setBackgroundPaint(couleurDeFond);

		// get a reference to the plot for further customisation...
		CategoryPlot plot = (CategoryPlot) graphe.getPlot();
		plot.setBackgroundPaint(Color.white);
		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setLowerMargin(0.02);
		domainAxis.setUpperMargin(0.02);
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		
		

		plot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
		plot.setRangeAxisLocation(AxisLocation.TOP_OR_LEFT);
		
		for (int i = 0; i < histoOuligne.length; i++) {
			if (histoOuligne[i] == true) {
				CategoryItemRenderer renderer1 = plot.getRenderer();
				//etiquettes valeur
				CategoryItemLabelGenerator generatorH = new StandardCategoryItemLabelGenerator();
				NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
				plot.setDataset(i, combiDataset[i]);
				rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
				renderer1.setSeriesItemLabelGenerator(i, generatorH);
				renderer1.setSeriesItemLabelsVisible(i, true);
				renderer1.setSeriesItemLabelFont(i, policeEtiquette);
				//rendu des barres
				renderer1.setSeriesPaint(i, couleursCombi[i]);
			} else {
				CategoryItemRenderer renderer2 = new LineAndShapeRenderer();
				//etiquettes valeur
				CategoryItemLabelGenerator generatorL = new StandardCategoryItemLabelGenerator();
				NumberAxis rangeAxisL = (NumberAxis) plot.getRangeAxis();
				rangeAxisL.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
				renderer2.setSeriesItemLabelGenerator(i, generatorL);
				renderer2.setSeriesItemLabelsVisible(i, true);
				renderer2.setSeriesItemLabelFont(i, policeEtiquette);
				//axe ?
				if (rgeAxis[i])
				{
					ValueAxis axis2 = new NumberAxis3D("");
					plot.setRangeAxis(i, axis2);
					plot.getRangeAxis(i).setVisible(true);
					plot.mapDatasetToRangeAxis(i, i);
				}

				//rendu des lignes
				plot.setDataset(i, combiDataset[i]);
				renderer2.setSeriesPaint(i, couleursCombi[i]);
				plot.setRenderer(i, renderer2);


			}
		}
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);

		
		return graphe;
	}

	/**
	 * G�re la mise en forme graphique des graphes (couleurs, polices, etc)
	 * 
	 * @param graphe
	 * @return graphe
	 */
	private JFreeChart miseEnFormeGraphe(JFreeChart grf) {
		// Couleurs g�n�rales du graphe
		grf.setBackgroundPaint(couleurDeFond);

		// cas particulier : camembert
		if (typeGraphe == GRAPHE_PIE3D)
		{
			PiePlot3D plotPie = (PiePlot3D) grf.getPlot();
			// couleur arri�re plan
			plotPie.setBackgroundPaint(Color.white);
			// pas de valeur nulle
			plotPie.setIgnoreNullValues(true);
			// �tiquettes avec libell� + pourcentage
			plotPie.setLabelLinksVisible(true);
			plotPie.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}" + " ({2})", new DecimalFormat("#0.00"), new DecimalFormat("#0.00%")));
		}
		else {

			// couleur arri�re plan (blanc)
			CategoryPlot plot = (CategoryPlot) grf.getPlot();
			plot.setBackgroundPaint(Color.white);

			// Couleur des graphes
			switch (typeGraphe) {
			case GRAPHE_PIE3D:
				break;
			case GRAPHE_BAR3D:
				BarRenderer rendererBar = (BarRenderer) plot.getRenderer();
				rendererBar.setSeriesPaint(0, couleurGraph);
				break;
			case GRAPHE_XY:
				break;
			case GRAPHE_LINE3D:
				LineRenderer3D rendererLigne3d = (LineRenderer3D) plot
						.getRenderer();
				rendererLigne3d.setSeriesPaint(0, couleurGraph);
			case GRAPHE_LINE:
				LineRenderer3D rendererLigne = (LineRenderer3D) plot
						.getRenderer();
				rendererLigne.setSeriesPaint(0, couleurGraph);
				break;
			}

			// Etiquettes de valeur � l'interieur du graphe

			CategoryItemRenderer rendcat = plot.getRenderer();
			CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator();
			rendcat.setSeriesItemLabelGenerator(0, generator);
			rendcat.setSeriesItemLabelsVisible(0, true);
			rendcat.setSeriesItemLabelFont(0, policeEtiquette);

			// L�gendes
			CategoryAxis categoryAxis = plot.getDomainAxis();
			ValueAxis valueAxis = plot.getRangeAxis();
			CategoryLabelPositions labelPositions = CategoryLabelPositions.UP_45;
			categoryAxis.setCategoryLabelPositions(labelPositions);
			categoryAxis.setTickLabelFont(policeLegende);
			valueAxis.setTickLabelFont(policeLegende);
		}

		return grf;
	}

	/**
	 * Retourne une image m�moire du graphe
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public BufferedImage getImage(int width, int height) {
		if (graphe == null)
			return null;
		// transformer le graphe en Image
		return graphe.createBufferedImage(width, height);
	}

	/**
	 * Retourne une image/icone pr�te a �tre utillis� dans un composant
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public ImageIcon getPicture(int width, int height) {
		return new ImageIcon(getImage(width, height));
	}

	/**
	 * Retourne l'orientation du graphe
	 * 
	 * @return
	 */
	public int getOrientation() {
		return orientation;
	}

	/**
	 * Initialise l'orientation du graphe
	 * 
	 * @param orientation
	 */
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	/**
	 * Initialise les titres des axes
	 * 
	 * @param titreabs
	 * @param titreord
	 */
	public void setTitreabs(String titreabs, String titreord) {
		this.titreabs = titreabs;
		this.titreord = titreord;
	}

	/**
	 * Initialise la couleur de fond d'un graphe
	 * 
	 * @param couleur
	 */
	public void setBackgroundColor(Color couleurFond) {
		couleurDeFond = couleurFond;
	}

	/**
	 * Initialise la police de l�gende
	 * 
	 * @param police
	 */
	public void setLegendFont(Font police) {
		policeLegende = police;
	}

	/**
	 * Initialise la couleur du trac� des graphes
	 * 
	 * @param couleur
	 */
	public void setGraphColor(Color couleurDuGraphe) {
		couleurGraph = couleurDuGraphe;
	}

	/**
	 * Initialise la couleur du trac� des graphes pour graphe combin�
	 * 
	 * @param couleur
	 *            []
	 */
	public void setGraphCombiColor(Color[] couleurDuGraphe) {
		couleursCombi = couleurDuGraphe;
	}

	/**
	 * D�termine si on affiche un histograme ou une ligne dans un graphe combin�
	 * 
	 * @param boolean[]
	 */
	public void setGraphCombiForme(boolean[] hOrl) {
		histoOuligne = hOrl;
	}

	/**
	 * D�termine si on affiche un axe gradu� pour la courbe
	 * 
	 * @param boolean[]
	 */
	public void setGraphCombiRangeAxis(boolean[] rangeAx) {
		rgeAxis = rangeAx;
	}

	/**
	 * Envoi le graphe dans le presse papier
	 * 
	 * @param width
	 * @param height
	 */
	public void sendToClipBoard(int width, int height) {
		Clipboard.sendImageToClipBoard(getImage(width, height));
	}

	/**
	 * Sauvegarde du graphe dans un fichier
	 * 
	 * @param width
	 * @param height
	 */
	public void saveGraphe(File fichier, int width, int height) {
		if (graphe == null)
			return;
		// Ouverture de la boite de dialogue si besoin
		if (fichier == null) {
			fichier = openSaveDialog();
			if (fichier == null)
				return;
		}

		// Controle de l'extension
		String extension = fichier.getName().toLowerCase();
		int pos = extension.lastIndexOf('.');
		if (pos != -1)
			extension = extension.substring(pos + 1);

		// Enregistrement du fichier
		try {
			if (extension.equals("jpg"))
				ChartUtilities.saveChartAsJPEG(fichier, graphe, width, height);
			else if (extension.equals("png"))
				ChartUtilities.saveChartAsPNG(fichier, graphe, width, height);
		} catch (IOException e) {
			// TODO Bloc catch auto-g�n�r�
			e.printStackTrace();
		}
	}

	/**
	 * Ouvre la boite de dialogue pour enregistrer le graphe
	 * 
	 * @return
	 */
	public File openSaveDialog() {
		if (choixFichier == null) {
			choixFichier = new JFileChooser();
			choixFichier.addChoosableFileFilter(new GestionFiltre(
					new String[] { "png" }, "Fichier image (*.png)"));
			choixFichier.addChoosableFileFilter(new GestionFiltre(
					new String[] { "jpg" }, "Fichier image (*.jpg)"));
		}

		// On se place dans le dernier dossier courant s'il y en a un
		if (dernierPath != null)
			choixFichier.setCurrentDirectory(dernierPath);

		File fichier = new File("image"
				+ (compteurImg++ < 10 ? "0" + compteurImg : compteurImg)
				+ ".jpg");
		choixFichier.setSelectedFile(fichier);
		if (choixFichier.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			// On stocke comme le dossier courant
			dernierPath = choixFichier.getCurrentDirectory();
			return choixFichier.getSelectedFile();
		}
		return null;
	}

}
