package gfx;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class RiBoutonRecherche extends JButton 
{
	private static final long serialVersionUID = -356312598814057138L;
	private ImageIcon loupeBleue = new ImageIcon(getClass().getClassLoader().getResource("images/loupe_new.png"));
	private ImageIcon loupe = new ImageIcon(getClass().getClassLoader().getResource("images/loupe_new_g.png"));
	
	public RiBoutonRecherche()
	{
		super();
		
		setPreferredSize(new Dimension(28,28));
		setMinimumSize(new Dimension(28,28));
		setMaximumSize(new Dimension(28,28));
		setBorderPainted(false);
		setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		setIcon(loupeBleue);
		setToolTipText("Recherche");
		
		addMouseListener(new MouseListener() {
			private ImageIcon loupe = new ImageIcon(getClass().getClassLoader().getResource("images/loupe_new_g.png"));
			private ImageIcon loupeBleue = new ImageIcon(getClass().getClassLoader().getResource("images/loupe_new.png"));
			
			public void mouseReleased(MouseEvent arg0){}
			
			public void mousePressed(MouseEvent arg0){}
			
			public void mouseExited(MouseEvent arg0){
				setIcon(loupeBleue);	
			}
			
			public void mouseEntered(MouseEvent arg0){
				setIcon(loupe);	
			}
			
			public void mouseClicked(MouseEvent arg0){}
		});
	}
	
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		if(!enabled)
		{
			this.setDisabledIcon(loupe);
			repaint();
		}
	}
}
