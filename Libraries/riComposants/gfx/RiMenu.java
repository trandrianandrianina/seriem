package gfx;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JMenuBar;

public class RiMenu extends JMenuBar 
{
	private static final long serialVersionUID = -6562933132774972464L;
	private boolean isOuvert = false;

	public RiMenu()
	{
		super();
		setMargin(new Insets(0, -5, 0, 0));
		setPreferredSize(new Dimension(170,36));
		
	}
	
	
	// Les ascesseurs ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public boolean isOuvert() {
		return isOuvert;
	}

	public void setOuvert(boolean isOuvert) {
		this.isOuvert = isOuvert;
	}

}
