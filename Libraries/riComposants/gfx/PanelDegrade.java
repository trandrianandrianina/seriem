package gfx;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;

import javax.swing.JPanel;

import base.Constantes;

public class PanelDegrade extends JPanel 
{
	// Constantes
	private static final long serialVersionUID = -427585144367208649L;
	public static final int SENS_HORIZONTAL = 0;
	public static final int SENS_VERTICAL = 1;
	
	// Variables
	private Color couleurDegradeC =  Constantes.CL_DEGRADE_CLAIR;
	private Color couleurDegradeF =  Constantes.CL_DEGRADE_FONCE;
	private int sens = SENS_VERTICAL;
	private Paint gradientPaint = null;
	private int longueurDegrade = 900;		// Valeur fix�e pour les bandeaux des panels de S�rie N
	
	/**
	 * Constructeur
	 */
	public PanelDegrade() 
	{
        super();
    }

	/**
	 * Dessine le composant
	 */
    protected void paintComponent(Graphics _g) 
    {
        super.paintComponent(_g);
        
        Graphics2D g = (Graphics2D) _g;
        
        Rectangle bounds = getBounds();
        if( sens == SENS_VERTICAL ) // A modifier plus tard car utilis�e dans p_Centrage (afin de rendre cette classe g�n�rique)
        	gradientPaint = new GradientPaint(0, bounds.y+ bounds.width, couleurDegradeF,0, bounds.y , couleurDegradeC);
        
        g.setPaint(gradientPaint);
        g.fillRect(0, 0, bounds.width, bounds.height);
    }
    

    /**
     * Changer la couleur claire du d�grad�
     * @param couleurs
     */
    public void setCouleurDegradeC(Color couleurs)
    {
    	couleurDegradeC = couleurs;
       	if( sens == SENS_HORIZONTAL )
           	gradientPaint = new GradientPaint(0, 0, couleurDegradeF, longueurDegrade, 0, couleurDegradeC);
    	this.repaint();
    }

    /**
     * Retourne la couleur claire 
	 * @return le couleurDegradeC
	 */
	public Color getCouleurDegradeC()
	{
		return couleurDegradeC;
	}


	/**
     * Changer la couleur claire du d�grad�
     * @param couleurs
     */
    public void changerCouleurClaire(int[] couleurs)
    {
    	setCouleurDegradeC(new Color(couleurs[0],couleurs[1],couleurs[2]));
    }

    /**
     * Changer la couleur fonc�e du d�grad�
     * @param couleurs
     */
    public void setCouleurDegradeF(Color couleurs)
    {
    	couleurDegradeF = couleurs;
       	if( sens == SENS_HORIZONTAL )
           	gradientPaint = new GradientPaint(0, 0, couleurDegradeF, longueurDegrade, 0, couleurDegradeC);
    	this.repaint();
    }

    /**
     * Retourne la couleur fonc�e
	 * @return le couleurDegradeF
	 */
	public Color getCouleurDegradeF()
	{
		return couleurDegradeF;
	}

	/**
     * Changer la couleur fonc�e du d�grad�
     * @param couleurs
     */
    public void changerCouleurFoncee(int[] couleurs)
    {
    	setCouleurDegradeF(new Color(couleurs[0],couleurs[1],couleurs[2]));
    }

	/**
	 * Retourne le sens du d�grad�
	 * @return le sens
	 */
	public int getSens()
	{
		return sens;
	}

	/**
	 * Initialise le sens du d�grad�
	 * @param sens le sens � d�finir
	 */
	public void setSens(int sens)
	{
		this.sens = sens;
       	if( sens == SENS_HORIZONTAL )
           	gradientPaint = new GradientPaint(0, 0, couleurDegradeF, longueurDegrade, 0, couleurDegradeC);
       	this.repaint();
	}

	/**
	 * @return le longueurDegrade
	 */
	public int getLongueurDegrade()
	{
		return longueurDegrade;
	}

	/**
	 * @param longueurDegrade le longueurDegrade � d�finir
	 */
	public void setLongueurDegrade(int longueurDegrade)
	{
		this.longueurDegrade = longueurDegrade;
		this.repaint();
	}
    
}
