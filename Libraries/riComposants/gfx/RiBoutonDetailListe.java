package gfx;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class RiBoutonDetailListe extends JButton 
{
	private static final long serialVersionUID = -356312598816057138L;

	public RiBoutonDetailListe()
	{
		super();
		
		setPreferredSize(new Dimension(15,15));
		setMinimumSize(new Dimension(15,15));
		setMaximumSize(new Dimension(15,15));
		setBorderPainted(false);
		setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/detailsMini.png")));
		
		addMouseListener(new MouseListener() {
			private ImageIcon detail = new ImageIcon(getClass().getClassLoader().getResource("images/detailsMini.png"));
			private ImageIcon detailBleu = new ImageIcon(getClass().getClassLoader().getResource("images/detailsMini2.png"));
			public void mouseReleased(MouseEvent arg0) {}
			
			public void mousePressed(MouseEvent arg0){}
			
			public void mouseExited(MouseEvent arg0){
				setIcon(detail);	
			}
			
			public void mouseEntered(MouseEvent arg0){
				setIcon(detailBleu);	
			}
			
			public void mouseClicked(MouseEvent arg0) {}
		});
	}
}
