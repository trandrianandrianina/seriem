package gfx;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class RiBoutonInfo extends JButton 
{
	private static final long serialVersionUID = -356312578816057138L;
	private ImageIcon infos = new ImageIcon(getClass().getClassLoader().getResource("images/picto_infos.png"));
	private ImageIcon infosBleues = new ImageIcon(getClass().getClassLoader().getResource("images/picto_infos_c.png"));
	
	public RiBoutonInfo()
	{
		super();
		
		setPreferredSize(new Dimension(22,22));
		setMinimumSize(new Dimension(22,22));
		setMaximumSize(new Dimension(22,22));
		setBorderPainted(false);
		setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		setIcon(infosBleues);
		setToolTipText("@V03F@");
		
		addMouseListener(new MouseListener() {
			private ImageIcon infos = new ImageIcon(getClass().getClassLoader().getResource("images/picto_infos.png"));
			private ImageIcon infosBleues = new ImageIcon(getClass().getClassLoader().getResource("images/picto_infos_c.png"));
			public void mouseReleased(MouseEvent arg0) {}
			
			public void mousePressed(MouseEvent arg0){}
			
			public void mouseExited(MouseEvent arg0){
				setIcon(infosBleues);	
			}
			
			public void mouseEntered(MouseEvent arg0){
				setIcon(infos);	
			}
			
			public void mouseClicked(MouseEvent arg0) {}
		});
	}
	
	/**
	 * permet de g�rer l'affichage du tooltipText du bouton
	 * @param isInfo
	 */
	public void gererAffichageV03F(boolean isErreur,String info)
	{
		if(!isErreur) setToolTipText(info);
		this.setVisible(!info.equals(""));
	}
	
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		if(!enabled)
		{
			this.setDisabledIcon(infos);
			repaint();
		}
	}
}
