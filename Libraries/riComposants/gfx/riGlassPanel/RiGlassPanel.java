//=================================================================================================
//==>                                                                       17/06/2010 - 04/05/2015
//==>  Panel permettant la gestion du GlassPane
//==> A faire:
//=================================================================================================
package gfx.riGlassPanel;

import java.awt.Component;
import java.awt.LayoutManager;

import javax.swing.JPanel;
import javax.swing.OverlayLayout;

public class RiGlassPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private JPanel m_pnlContentPane = new JPanel();
	private Component m_pnlGlassPane = new JPanel();
	// This boolean indicate when to add components to the contentpane to to the main panel
	boolean m_isContentPaneInteract = false;
	private OverlayLayout m_layout = new OverlayLayout(RiGlassPanel.this);

	public RiGlassPanel()
	{
		super();
		m_isContentPaneInteract = false;
		setLayout(m_layout);
		add(m_pnlGlassPane, 0);
		add(m_pnlContentPane, 1);
		m_pnlContentPane.setOpaque(false);
		m_pnlGlassPane.setVisible(false);
		m_isContentPaneInteract = true;
	}

	public void setLayout(LayoutManager manager)
	{
		synchronized (this)
		{
			if (m_isContentPaneInteract)
			{
				m_pnlContentPane.setLayout(manager);
			}
			else
			{
				super.setLayout(manager);
			}
		}
	}

	public boolean isOptimizedDrawingEnabled()
	{
		return false;
	}

	protected void addImpl(Component comp, Object constraints, int index)
	{
		synchronized (this)
		{
			if (m_isContentPaneInteract)
			{
				m_pnlContentPane.add(comp, constraints, index);
			}
			else
			{
				super.addImpl(comp, constraints, index);
			}
		}
	}

	public void setGlassPane(Component glass)
	{
		synchronized (this)
		{
			if (glass == null)
			{
				throw new NullPointerException(
						"glassPane cannot be set to null.");
			}
			/**
			 * Must set 'm_isContentPaneInteract' do false , so
			 * add/remove/laymethod will not make effect the contentPane this
			 * why all add/remove/setLayout are synchronized
			 */
			m_isContentPaneInteract = false;

			if (m_pnlGlassPane != null)
			{ // ) && m_pnlGlassPane.getParent() == this) {
				remove(m_pnlGlassPane);
			}
			/**
			 * Set always to visable , more comptarble is think
			 */
			glass.setVisible(true);
			add(glass, 0);
			m_pnlGlassPane = glass;

			m_isContentPaneInteract = true;
		}
		//System.out.println("--> p.w:" + getPreferredSize().width + " w:" + getWidth() );
	}

	public void remove(Component comp)
	{
		synchronized (this)
		{
			if (m_isContentPaneInteract)
			{
				m_pnlContentPane.remove(comp);
			}
			else
			{
				super.remove(comp);
			}
		}
	}

}
