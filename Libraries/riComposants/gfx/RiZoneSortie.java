/**
 * 
 */
package gfx;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import base.Constantes;

/**
 * @author ritoudb
 *
 */
public class RiZoneSortie extends JLabel {

	private static final long serialVersionUID = -2414806630888477257L;
	private JPopupMenu popOptions = new JPopupMenu();
	private Clipboard pressePapiers = Toolkit.getDefaultToolkit().getSystemClipboard();

	public RiZoneSortie() 
	{
		super();
		
		//gestion du presse papiers
		//pressePapiers = Toolkit.getDefaultToolkit().getSystemClipboard(); 
		//popOptions = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Copier vers le presse papiers");
		menuItem.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				pressePapiers.setContents(new StringSelection(((JLabel)popOptions.getInvoker()).getText()), null);
			}
		});
		popOptions.add(menuItem);
		setComponentPopupMenu(popOptions);
		//fin du presse papiers
		
		Border paddingBorder = BorderFactory.createEmptyBorder(0,3,0,3);
		Border border = BorderFactory.createBevelBorder(BevelBorder.LOWERED);
		setBorder(BorderFactory.createCompoundBorder(border,paddingBorder));
		
		setHorizontalAlignment(SwingConstants.LEFT);
		setBackground(Constantes.CL_ZONE_SORTIE);
		setOpaque(true);
		setPreferredSize(new Dimension(100, 24));
		setForeground(Constantes.CL_TEXT_SORTIE);
	}
	
}
