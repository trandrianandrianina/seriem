//=================================================================================================
//==>                                                                       ??/??/2011 - 29/05/2012
//==> Composant servant � g�rer les infos suppl�mentaires sur une zone (F4 en g�n�ral)
//==> A faire:
//=================================================================================================
package gfx;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class RiBoutonDetail extends JButton
{
	// Constantes
	private static final long serialVersionUID = -356312598816057138L;

	// Variables
	private final ImageIcon detail = new ImageIcon(getClass().getClassLoader().getResource("images/details.png"));
	private final ImageIcon detailBleu = new ImageIcon(getClass().getClassLoader().getResource("images/details2.png"));
	 
	/**
	 * Constructeur
	 */
	public RiBoutonDetail()
	{
		super();
		
		setPreferredSize(new Dimension(18,18));
		setMinimumSize(new Dimension(18,18));
		setMaximumSize(new Dimension(18,18));
		setBorderPainted(false);
		setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		setIcon(detailBleu);
		
		addMouseListener(new MouseListener()
		{
			public void mouseReleased(MouseEvent arg0) {}
			
			public void mousePressed(MouseEvent arg0){}
			
			public void mouseExited(MouseEvent arg0){
				setIcon(detailBleu);	
			}
			
			public void mouseEntered(MouseEvent arg0){
				setIcon(detail);	
			}
			
			public void mouseClicked(MouseEvent arg0){}
		});
	}
	
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		if(!enabled)
		{
			this.setDisabledIcon(detail);
			repaint();
		}
	}

}
