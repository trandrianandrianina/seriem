//------------------------------------------------------------------------------
//--> Version qui gere les 4 langues FR, ES, AL et GB
//------------------------------------------------------------------------------

unit parametre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TParametrage = class(TForm)
    OD_Chercher: TOpenDialog;
    BT_tableur: TButton;
    E_tableur: TEdit;           
    E_texte: TEdit;
    E_calc: TEdit;
    L_Tableur: TLabel;
    L_texte: TLabel;
    L_calc: TLabel;
    L_navi: TLabel;
    E_navi: TEdit;
    BT_annuler: TButton;
    BT_ok: TButton;
    BT_calc: TButton;
    BT_navi: TButton;
    BT_texte: TButton;
    L_photo: TLabel;
    E_photo: TEdit;
    L_mail: TLabel;
    E_mail: TEdit;
    BT_mail: TButton;
    L_import: TLabel;
    E_import: TEdit;
    BT_import: TButton;
    L_Logimg: TLabel;
    E_Logimg: TEdit;
    L_prefixe: TLabel;
    E_prefixe: TEdit;
    BT_Restauration: TButton;
    E_temp: TEdit;
    L_Temp: TLabel;
    L_langue: TLabel;
    CB_langue: TComboBox;
    E_Twain: TEdit;
    L_Twain: TLabel;
    BT_twain: TButton;
    CKB_Soumission: TCheckBox;
    BT_logimg: TButton;
    procedure BT_tableurClick(Sender: TObject);
    procedure BT_texteClick(Sender: TObject);
    procedure BT_calcClick(Sender: TObject);
    procedure BT_naviClick(Sender: TObject);
    procedure BT_photoClick(Sender: TObject);
    procedure BT_mailClick(Sender: TObject);
    procedure BT_importClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BT_annulerClick(Sender: TObject);
    procedure BT_okClick(Sender: TObject);
    procedure BT_RestaurationClick(Sender: TObject);
    procedure BT_twainClick(Sender: TObject);
    procedure CKB_SoumissionClick(Sender: TObject);
    procedure BT_LogimgClick(Sender: TObject);
  private
    { D�clarations priv�es}
    function ExtractionCodenonMulan(nomfichier : string) : Word;
    function ExtractionLangue(module : string) : Boolean;
    function LectureParam : boolean;
  public
    { D�clarations publiques}
    Batch : String;
  end;

const
    nbrlangue = 4;
var
  Parametrage: TParametrage;
  repertoire : string;

  listelangue : array[0..nbrlangue-1] of string = ('Fran�ais', 'Espagnol', 'Allemand', 'Anglais');
  listecodelangue : array[0..nbrlangue-1] of string = ('FR', 'ES', 'AL', 'GB');
  extrait : Array[1..1000] of string;


implementation

{$R *.DFM}

function TParametrage.ExtractionCodenonMulan(nomfichier : string) : Word;
var
   F : TextFile;
   i, j, k, entier : integer;
   chaine : string;
begin
     // Initialisation
     For i := 1 to 1000 do
         extrait[i] := '';

{$I-}
    AssignFile(F, nomfichier);
    FileMode := 0;
    Reset(F);
    i := 0;
    // On lit le fichier
    while not eof(F) do begin
        ReadLn(F,chaine);
        try
            entier := StrToInt(Copy(chaine, 2, 2));
        except
            on EConvertError do begin
                Inc(i);
                extrait[i] := chaine;
                Continue;
            end;
        end;
        if (chaine[1] < 'A') or (chaine[1] > 'Z') then begin
            Inc(i);
            extrait[i] := chaine;
        end;
    end;
    CloseFile(F);
{$I+}

(*    AssignFile(F, 'toto.txt');
    rewrite(f);
    for j := 1 to i do
        writeln(f, extrait[j]);
    closefile(f);
*)
    ExtractionCodenonMulan := i;
end;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
function TParametrage.ExtractionLangue(module : string) : Boolean;
var
   F : TextFile;
   FMod : TextFile;
   FWeb : TextFile;
   Attributes : word;
   chaine, chaine1a, chaine1b, chaine2a, chaine2b : string;
   i,j, position : integer;
   ret : boolean;
   ext : string;
   trouve, identique : boolean;
   nbrextrait : integer;
begin
    ret := true;
    ext := 'jwm';

    i := 1;
    // 1� passe on fait le textfile, 2� passe on fait la translation table
    while i <= 2  do begin
        // V�rification des attributs du fichier #module#.jwm (lecture seule)
        if FileExists(repertoire+'\'+module+'.'+ ext) then begin
            Attributes := FileGetAttr(repertoire+'\'+module+'.'+ ext);
            if Attributes and faReadOnly <> 0 then
                FileSetAttr(repertoire+'\'+module+'.'+ ext, Attributes and not faReadOnly);
        end;

{$I-}
    try
        if FileExists(repertoire+'\'+ext+Trim(listecodelangue[CB_langue.ItemIndex])+'.txt') = false then begin
            ret := false;
            showmessage('Fichier '+repertoire+'\'+ext+Trim(listecodelangue[CB_langue.ItemIndex])+'.txt'+' introuvable.');
            Continue;
        end;

        // On extrait les codes non mulans
        if ext = 'jwx' then
            nbrextrait := ExtractionCodenonMulan(repertoire+'\'+module+'.'+ ext);

        // Ouverture du fichier #module#.JWM en �criture
        AssignFile(F, repertoire+'\'+module+'.'+ ext);
        Rewrite(F);

        // Recherche dans jwmXX.txt de #module#
        AssignFile(FMod, repertoire+'\'+ext+Trim(listecodelangue[CB_langue.ItemIndex])+'.txt');
        FileMode := 0;
        Reset(FMod);
        // On scanne le fichier � la recherche du marqueur
        trouve := false;
        while not eof(FMod) do begin
            ReadLn(FMod, chaine);
            if (StrLComp(PChar(chaine), PChar(UpperCase(module)), 3) = 0) then begin
                trouve := true;
                break;
            end;
        end;

        // On �crit les lignes extraites
        if ext = 'jwx' then begin
            for j := 1 to nbrextrait do
                Writeln(F, extrait[j]);
        end;

        // On recopie le fichier jusqu'au marqueur (===)
        if trouve = true then begin
            ReadLn(FMod, chaine);
            repeat
                // On recherche une equivalence dans les codes non Mulans
                position := Pos(#9, chaine) + 1;
                chaine1a := Copy(chaine, 1, position-2);
                chaine1b := Copy(chaine, position, Length(chaine)-position+1);
                identique := false;
                j := 1;
                repeat
                    position := Pos(#9, extrait[j]) + 1;
                    chaine2a := Copy(extrait[j], 1, position-2);
                    chaine2b := Copy(extrait[j], position, Length(extrait[j])-position+1);
                    if chaine1b = chaine2a then begin
                        identique := true;
                        chaine := chaine1a + #9 + chaine2b;
                    end;
                    j := j + 1;
                until (identique=true) or (j>nbrextrait);
                // On �crit la chaine
                WriteLn(F, chaine);
                ReadLn(FMod, chaine);
            until StrLComp(PChar(chaine), '===', 3) = 0;
        end;

        // On termine les textfiles avec le param et le paramweb
        if ext = 'jwm' then begin
            // On recopie Param.jwm uniquement pour les textfiles
            WriteLn(F, 'LANGUE    ' + #9 + Trim(listecodelangue[CB_langue.ItemIndex]));
            WriteLn(F, 'TABLEUR   ' + #9 + Trim(E_tableur.text));
            WriteLn(F, 'TEXTE     ' + #9 + Trim(E_texte.text));
            WriteLn(F, 'CALC      ' + #9 + Trim(E_calc.text));
            WriteLn(F, 'NAVI      ' + #9 + Trim(E_navi.text));
            WriteLn(F, 'PHOTO     ' + #9 + Trim(E_photo.text));
            WriteLn(F, 'TWAIN     ' + #9 + Trim(E_twain.text));
            WriteLn(F, 'MAIL      ' + #9 + Trim(E_mail.text));
            WriteLn(F, 'IMPORT    ' + #9 + Trim(E_import.text));
            WriteLn(F, 'TEMP      ' + #9 + Trim(E_temp.text));
            WriteLn(F, 'LOGIMG    ' + #9 + Trim(E_logimg.text));
            WriteLn(F, 'PREFIXE   ' + #9 + Trim(E_prefixe.text));
            WriteLn(F, 'BATCH     ' + #9 + Batch);

            // On recopie Paramweb.jwm uniquement pour les textfiles (s'il existe)
            if FileExists(repertoire+'\paramweb.jwm') = True then begin
                AssignFile(FWeb, repertoire+'\paramweb.jwm');
                FileMode := 0;
                Reset(FWeb);
                // On lit le fichier et on �crit dans l'autre
                while not eof(FWeb) do begin
                    ReadLn(FWeb, chaine);
                    WriteLn(F, chaine);
                end;
                CloseFile(FWeb);
            end;
        end;
        CloseFile(F);
        CloseFile(FMod);
{$I+}
        // Si le fichier n'existe pas
        if IOResult <> 0 then begin
            ShowMessage('Erreur lors de la lecture du fichier.');
            ret := false;
        end;
    finally
        ext := 'jwx';
        Inc(i);
    end;
    end;

    ExtractionLangue := ret;
end;


function TParametrage.LectureParam : Boolean;
var
   F : TextFile;
   s : Array[1..100] of string;
   i : byte;
   j, k : word;
   chaine : Array[0..300] of char;
begin
     // Initialisation
     For i := 1 to 100 do
         s[i] := '';

     CB_langue.Clear;
     for i := 0 to nbrlangue-1 do
        CB_langue.Items.Add(listelangue[i]);
    CB_langue.ItemIndex := 0;

{$I-}
    AssignFile(F, repertoire+'\param.jwm');
    FileMode := 0;
    Reset(F);
    i := 1;
    // On lit le fichier
    while not eof(F) do begin
          ReadLn(F,s[i]);
          Inc(i);
    end;
    CloseFile(F);

    // Initialisation
    E_tableur.text := '';
    E_texte.text := '';
    E_calc.text := '';
    E_navi.text := '';
    E_photo.text := '';
    E_twain.Text := '';
    E_mail.text := '';
    E_import.text := '';
    E_temp.text := '';
    E_logimg.text := 'calc.gif';
    E_prefixe.text := '';
    Batch := ' ';

    // On analyse le tableau
    for i := 1 to 100 do begin
        StrPCopy(chaine, s[i]);
        if (StrLComp(chaine, 'LANGUE', 6) = 0) then begin
           k := 0;
           for j := 8 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           for j := 0 to nbrlangue-1 do
               if listecodelangue[j] = Trim(chaine) then
                   CB_langue.ItemIndex := j;
        end;
        StrPCopy(chaine, s[i]);
        if (StrLComp(chaine, 'TABLEUR', 7) = 0) then begin
           k := 0;
           for j := 8 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_tableur.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'TEXTE', 5) = 0) then begin
           k := 0;
           for j := 6 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_texte.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'CALC', 4) = 0) then begin
           k := 0;
           for j := 5 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_calc.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'NAVI', 4) = 0) then begin
           k := 0;
           for j := 5 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_navi.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'PHOTO', 5) = 0) then begin
           k := 0;
           for j := 5 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_photo.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'TWAIN', 5) = 0) then begin
           k := 0;
           for j := 5 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_twain.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'MAIL', 4) = 0) then begin
           k := 0;
           for j := 5 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_mail.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'IMPORT', 6) = 0) then begin
           k := 0;
           for j := 6 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_import.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'TEMP', 4) = 0) then begin
           k := 0;
           for j := 4 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_temp.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'LOGIMG', 6) = 0) then begin
           k := 0;
           for j := 7 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_logimg.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'PREFIXE', 7) = 0) then begin
           k := 0;
           for j := 7 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           E_prefixe.text := Trim(chaine);
        end;
        if (StrLComp(chaine, 'BATCH', 5) = 0) then begin
           k := 0;
           for j := 5 to StrLen(chaine) do begin
               chaine[k] := chaine[j];
               Inc(k);
           end;
           if Trim(chaine) <> '' then
               CKB_Soumission.Checked := True
           else
               CKB_Soumission.Checked := False;
        end;
    end;

{$I+}
    // Si le fichier n'existe pas
    if IOResult <> 0 then
       LectureParam := False;

    LectureParam := True;
end;

procedure TParametrage.BT_tableurClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le tableur';
    OD_Chercher.Filter := 'Programmes|*.exe;*.bat|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then
       E_tableur.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_texteClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le traitement de texte';
    OD_Chercher.Filter := 'Programmes|*.exe;*.bat|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then
        E_texte.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_calcClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher la calculatrice';
    OD_Chercher.Filter := 'Programme|*.exe|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then
        E_calc.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_naviClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le navigateur internet';
    OD_Chercher.Filter := 'Programme|*.exe|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then
        E_navi.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_photoClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher les photos';
    OD_Chercher.Filter := '';
    OD_Chercher.Execute;
    E_photo.Text := OD_Chercher.FileName;
    OD_Chercher.Filter := 'Programmes|*.exe;*.bat|Tous les fichiers|*.*';
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_twainClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher l''outil de num�risation';
    OD_Chercher.Filter := 'Programmes|*.exe;*.bat|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    E_twain.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_mailClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le soft de messagerie';
    OD_Chercher.Filter := 'Programmes|*.exe;*.bat|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    E_mail.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.BT_importClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le soft d''importation';
    OD_Chercher.Filter := 'Programmes|*.exe;*.bat|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    E_import.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

procedure TParametrage.FormCreate(Sender: TObject);
begin
     repertoire := GetCurrentDir;
     LectureParam;
end;

procedure TParametrage.BT_annulerClick(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TParametrage.BT_okClick(Sender: TObject);
var
   F : TextFile;
   FWeb : TextFile;
   Attributes : word;
   chaine : string;
   ret : boolean;
   chaine1 : array[0..255] of char;
   chaine2 : array[0..255] of char;
begin
//--> PARAM <-------------------------------------------------------------------
    // V�rification des attributs du fichier Param.jwm (lecture seule)
    Attributes := FileGetAttr(repertoire+'\param.jwm');
    if Attributes and faReadOnly <> 0 then
        FileSetAttr(repertoire+'\param.jwm', Attributes and not faReadOnly);
{$I-}
    if Trim(E_logimg.text) = '' then
        E_logimg.text := 'calc.gif';
    // Mise � jour du fichier
    AssignFile(F, repertoire+'\param.jwm');
    Rewrite(F);
    WriteLn(F, 'LANGUE    ' + #9 + Trim(listecodelangue[CB_langue.ItemIndex]));
    WriteLn(F, 'TABLEUR   ' + #9 + Trim(E_tableur.text));
    WriteLn(F, 'TEXTE     ' + #9 + Trim(E_texte.text));
    WriteLn(F, 'CALC      ' + #9 + Trim(E_calc.text));
    WriteLn(F, 'NAVI      ' + #9 + Trim(E_navi.text));
    WriteLn(F, 'PHOTO     ' + #9 + Trim(E_photo.text));
    WriteLn(F, 'TWAIN     ' + #9 + Trim(E_twain.text));
    WriteLn(F, 'MAIL      ' + #9 + Trim(E_mail.text));
    WriteLn(F, 'IMPORT    ' + #9 + Trim(E_import.text));
    WriteLn(F, 'TEMP      ' + #9 + Trim(E_temp.text));
    WriteLn(F, 'LOGIMG    ' + #9 + Trim(E_logimg.text));
    WriteLn(F, 'PREFIXE   ' + #9 + Trim(E_prefixe.text));
    WriteLn(F, 'BATCH     ' + #9 + Trim(Batch));

    // On recopie Paramweb.jwm uniquement pour les textfiles (s'il existe)
    if FileExists(repertoire+'\paramweb.jwm') = True then begin
        AssignFile(FWeb, repertoire+'\paramweb.jwm');
        FileMode := 0;
        Reset(FWeb);
        // On lit le fichier et on �crit dans l'autre
        while not eof(FWeb) do begin
            ReadLn(FWeb, chaine);
            WriteLn(F, chaine);
        end;
        CloseFile(FWeb);
    end;

    CloseFile(F);
    if FileExists('..\param.jwm') = True then
        if DeleteFile('..\param.jwm') = False then
            Showmessage('PB destruction sauvegarde');
    if CopyFile('param.jwm', '..\param.jwm', False) = False then
        Showmessage('PB copie sauvegarde');
{$I+}
    // Si le fichier n'existe pas
    if IOResult <> 0 then
       ShowMessage('Fichier non trouv�.');

//--> GVM <---------------------------------------------------------------------
    // Generation du gvm.jwm
    ExtractionLangue('gvm');

//--> EXP <---------------------------------------------------------------------
    // Generation du exp.jwm
    ExtractionLangue('exp');

//--> GMM <---------------------------------------------------------------------
    // Generation du gmm.jwm
    ExtractionLangue('gmm');

//--> GVX <---------------------------------------------------------------------
    // Generation du gvx.jwm
    ExtractionLangue('gvx');

//--> GAM <---------------------------------------------------------------------
    // Generation du gam.jwm
    ExtractionLangue('gam');

//--> FIN <---------------------------------------------------------------------
    Application.Terminate;
end;

procedure TParametrage.BT_RestaurationClick(Sender: TObject);
begin
    CopyFile('..\param.jwm', 'param.jwm', False);
    LectureParam;
end;

procedure TParametrage.CKB_SoumissionClick(Sender: TObject);
begin
    if CKB_Soumission.Checked then
        Batch := 'X'
    else
        Batch := ' ';
end;

procedure TParametrage.BT_logimgClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le Logo';
    OD_Chercher.Filter := 'Logo|*.jpg;*.gif;*.ico|Tous les fichiers|*.*';
    OD_Chercher.Execute;
    E_logimg.Text := OD_Chercher.FileName;
    SetCurrentDir(repertoire);
end;

end.
