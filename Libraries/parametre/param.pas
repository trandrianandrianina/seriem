unit param;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    OD_Chercher: TOpenDialog;
    BT_tableur: TButton;
    E_tableur: TEdit;
    E_texte: TEdit;
    E_calc: TEdit;
    L_Tableur: TLabel;
    L_texte: TLabel;
    L_calc: TLabel;
    L_navi: TLabel;
    E_navi: TEdit;
    BT_texte: TButton;
    BT_calc: TButton;
    BT_navi: TButton;
    Button1: TButton;
    Button2: TButton;
    procedure BT_tableurClick(Sender: TObject);
  private
    { Déclarations privées}
  public
    { Déclarations publiques}
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.BT_tableurClick(Sender: TObject);
begin
    OD_Chercher.Title := 'Chercher le fichier';
    OD_Chercher.Execute;
    E_navi.Text := OD_Chercher.FileName;
end;

end.
