//==============================================================================
//==> ATTENTION ce programme est intimement li� au programme Param.exe  !!!
//==============================================================================
unit parametre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TParametrage = class(TForm)
    OD_Chercher: TOpenDialog;
    BT_BarreTitre: TButton;
    E_BarreTitre: TEdit;
    E_BarreBoutons: TEdit;
    E_Logo: TEdit;
    L_BarreTitre: TLabel;
    L_BarreBoutons: TLabel;
    L_Logo: TLabel;
    L_PageGarde: TLabel;
    E_PageGarde: TEdit;
    BT_annuler: TButton;
    BT_ok: TButton;
    BT_Logo: TButton;
    BT_PageGarde: TButton;
    BT_BarreBoutons: TButton;
    L_FondPrincipal: TLabel;
    E_FondPrincipal: TEdit;
    L_FondSaisie: TLabel;
    E_FondSaisie: TEdit;
    BT_FondSaisie: TButton;
    L_FondCmd: TLabel;
    E_FondCmd: TEdit;
    BT_FondCmd: TButton;
    BT_Restauration: TButton;
    E_FondValidation: TEdit;
    L_FondValidation: TLabel;
    BT_FondPrincipal: TButton;
    BT_FondValidation: TButton;
    CB_Modele: TComboBox;
    L_Modele: TLabel;
    E_Groupe: TEdit;
    L_Groupe: TLabel;
    CB_Groupe: TComboBox;
    BT_Ajout: TButton;
    BT_Suppr: TButton;
    BT_ModifGroupe: TButton;
    procedure BT_BarreTitreClick(Sender: TObject);
    procedure BT_BarreBoutonsClick(Sender: TObject);
    procedure BT_LogoClick(Sender: TObject);
    procedure BT_PageGardeClick(Sender: TObject);
    procedure BT_FondSaisieClick(Sender: TObject);
    procedure BT_FondCmdClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BT_annulerClick(Sender: TObject);
    procedure BT_okClick(Sender: TObject);
    procedure BT_RestaurationClick(Sender: TObject);
    procedure BT_FondPrincipalClick(Sender: TObject);
    procedure BT_FondValidationClick(Sender: TObject);
    procedure CB_ModeleChange(Sender: TObject);
    procedure BT_ModifGroupeClick(Sender: TObject);
    procedure BT_AjoutClick(Sender: TObject);
    procedure BT_SupprClick(Sender: TObject);
    procedure CB_GroupeClick(Sender: TObject);
    procedure CB_GroupeChange(Sender: TObject);
  private
    { D�clarations priv�es}
    function LectureParam : boolean;
    procedure MajInterface;
    procedure ChargementImage;
    procedure MiseEnTable(indice : integer);
  public
    { D�clarations publiques}
    indiceGroupe : integer;
  end;

const
    nbrmodule = 5;
    nbrmax = 250;
    nbrmodele = 4;
    nbrgroupe = 100;
    nbrparam = 11;
var
    Parametrage: TParametrage;
    Param : array[1..nbrparam] of string = ('LANGUE', 'TABLEUR', 'TEXTE', 'CALC', 'NAVI', 'PHOTO', 'MAIL', 'IMPORT', 'TEMP', 'DOSSIER', 'PREFIXE');
    Module : Array[1..nbrmodule] of string = ('PARAM', 'GVM', 'EXP', 'GVX', 'GMM');
    repertoire : string;
    Modele : Array[1..nbrmodele] of string = ('Personnalis�e', 'Modele1', 'Modele2', 'Modele3');
    Groupe : Array[1..nbrgroupe] of string;
    GroupeModele : Array[1..nbrgroupe] of string;
    GroupeImage : Array[1..nbrgroupe, 1..8] of string;

implementation

{$R *.DFM}

procedure TParametrage.MajInterface;
var
    etat : boolean;
begin
    if CB_Modele.Text = 'Personnalis�e' then
        etat := true
    else
        etat := false;

    E_BarreTitre.Enabled := etat;
    E_BarreBoutons.Enabled := etat;
    E_Logo.Enabled := etat;
    E_PageGarde.Enabled := etat;
    E_FondPrincipal.Enabled := etat;
    E_FondSaisie.Enabled := etat;
    E_FondCmd.Enabled := etat;
    E_FondValidation.Enabled := etat;
    BT_BarreTitre.Enabled := etat;
    BT_BarreBoutons.Enabled := etat;
    BT_Logo.Enabled := etat;
    BT_PageGarde.Enabled := etat;
    BT_FondPrincipal.Enabled := etat;
    BT_FondSaisie.Enabled := etat;
    BT_FondCmd.Enabled := etat;
    BT_FondValidation.Enabled := etat;
end;

procedure TParametrage.MiseEnTable(indice : integer);
begin
    GroupeImage[indice+1, 1] := Trim(E_BarreTitre.Text);
    GroupeImage[indice+1, 2] := Trim(E_BarreBoutons.Text);
    GroupeImage[indice+1, 3] := Trim(E_Logo.Text);
    GroupeImage[indice+1, 4] := Trim(E_PageGarde.Text);
    GroupeImage[indice+1, 5] := Trim(E_FondPrincipal.Text);
    GroupeImage[indice+1, 6] := Trim(E_FondSaisie.Text);
    GroupeImage[indice+1, 7] := Trim(E_FondCmd.Text);
    GroupeImage[indice+1, 8] := Trim(E_FondValidation.Text);
end;

procedure TParametrage.ChargementImage;
var
    listef: TSearchRec;
    retour : integer;
    chaine : Array[0..300] of char;
    chaine2 : Array[0..300] of char;
    position : integer;
    trouve : boolean;
begin
    trouve := false;
    retour := FindFirst(repertoire+'\modeles\*.*', faAnyFile, listef);
    while retour = 0 do begin
        StrPCopy(chaine, LowerCase(listef.name));
        StrPCopy(chaine2, LowerCase(CB_Modele.Text));
        position := Pos('_', listef.name);
        if (StrLComp(chaine, chaine2, position-1) = 0) then begin
            if (StrLComp(@chaine[position], 'webtit', 6) = 0) then
                E_BarreTitre.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'webbou', 6) = 0) then
                E_BarreBoutons.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'weblog', 6) = 0) then
                E_Logo.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'webfpg', 6) = 0) then
                E_PageGarde.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'webfpr', 6) = 0) then
                E_FondPrincipal.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'webfsa', 6) = 0) then
                E_FondSaisie.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'webfva', 6) = 0) then
                E_FondValidation.Text := '.\modeles\' + listef.name;
            if (StrLComp(@chaine[position], 'webfcm', 6) = 0) then
                E_FondCmd.Text := '.\modeles\' + listef.name;
            trouve := true;
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);

    // Si le modele selectionn� n'a pas d'image alors on blanchi
    if trouve = false then begin
        E_BarreTitre.Text := '';
        E_BarreBoutons.Text := '';
        E_Logo.Text := '';
        E_PageGarde.Text := '';
        E_FondPrincipal.Text := '';
        E_FondSaisie.Text := '';
        E_FondValidation.Text := '';
        E_FondCmd.Text := '';
    end;
end;

function TParametrage.LectureParam : Boolean;
var
   F : TextFile;
   i : byte;
   j, k : word;
   s : Array[1..nbrmax] of string;
   chaine : Array[0..300] of char;
   chaine2 : string;
begin
     // Initialisation
     for i := 1 to nbrmax do
         s[i] := '';
     ChDir(repertoire);

{$I-}
    AssignFile(F, 'paramweb.jwm');
    FileMode := 0;
    Reset(F);
    i := 1;
    // On lit le fichier
    while not eof(F) do begin
        for j := 0 to 8 do begin
            ReadLn(F,s[i]);
            if j = 0 then begin
                // On recupere le groupe
                Groupe[i] := Copy(s[i], 1, 3);
                // On recupere le modele
                GroupeModele[i] := Trim(Copy(s[i], 7, Length(s[i])-6)); end
            else
                GroupeImage[i, j] := Trim(Copy(s[i], 7, Length(s[i])-6));
        end;
        Inc(i);
    end;
    CloseFile(F);

    // Initialisation
    E_BarreTitre.text := '';
    E_BarreBoutons.text := '';
    E_Logo.text := '';
    E_PageGarde.text := '';
    E_FondPrincipal.text := '';
    E_FondSaisie.text := '';
    E_FondCmd.text := '';
    E_FondValidation.text := '';


{$I+}
    // Si le fichier n'existe pas
    if IOResult <> 0 then
       LectureParam := False;

    LectureParam := True;
end;

procedure TParametrage.BT_BarreTitreClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Barre de titre';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_BarreTitre.Text := ''; end
        else
            E_BarreTitre.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_BarreBoutonsClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Barre des boutons';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_BarreBoutons.Text := ''; end
        else
            E_BarreBoutons.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_LogoClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Logo';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_Logo.Text := ''; end
        else
            E_Logo.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_PageGardeClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Page de garde';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_PageGarde.Text := ''; end
        else
            E_PageGarde.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_FondPrincipalClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Fond principal';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_FondPrincipal.Text := ''; end
        else
            E_FondPrincipal.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_FondSaisieClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Fond de saiie';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_FondSaisie.Text := ''; end
        else
            E_FondSaisie.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_FondCmdClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Fond de commande';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_FondCmd.Text := ''; end
        else
            E_FondCmd.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

procedure TParametrage.BT_FondValidationClick(Sender: TObject);
var
    position : integer;
begin
    OD_Chercher.Title := 'Fond de validation';
    OD_Chercher.Execute;
    if Trim(OD_Chercher.FileName) <> '' then begin
        position := Pos('\modeles', LowerCase(OD_Chercher.FileName));
        if position = 0 then begin
            MessageDlg('Chemin incorrect.', mtWarning, [mbOk], 0);
            E_FondValidation.Text := ''; end
        else
            E_FondValidation.Text := '.' + Copy(OD_Chercher.FileName, position, Length(OD_Chercher.FileName)-position+1);
    end;
end;

//------------------------------------------------------------------------------
//--> Fonction Create
//------------------------------------------------------------------------------
procedure TParametrage.FormCreate(Sender: TObject);
var
    i : integer;
    typemodele : string;
begin
     repertoire := GetCurrentDir;
     E_Groupe.Visible := False;
     BT_Ajout.Visible := False;
     BT_Suppr.Visible := False;
     CB_Modele.Enabled := False;

     // Chargement de la combo Modele
     CB_Modele.Clear;
     for i := 0 to nbrmodele-1 do
        CB_Modele.Items.Add(Modele[i+1]);
     CB_Modele.ItemIndex := 0;

     // Chargement de la config
     LectureParam;

     // Chargement de la combo Groupe
     CB_Groupe.Clear;
     i := 0;
     while Groupe[i+1] <> '' do begin
        CB_Groupe.Items.Add(Groupe[i+1]);
        Inc(i);
     end;
     CB_Groupe.ItemIndex := 0;
     typemodele := GroupeModele[CB_Groupe.ItemIndex+1];
     CB_Modele.ItemIndex := CB_Modele.Items.IndexOf(typemodele);
     indiceGroupe := CB_Groupe.ItemIndex;

     E_BarreTitre.Text := GroupeImage[CB_groupe.ItemIndex+1, 1];
     E_BarreBoutons.Text := GroupeImage[CB_groupe.ItemIndex+1, 2];
     E_Logo.Text := GroupeImage[CB_groupe.ItemIndex+1, 3];
     E_PageGarde.Text := GroupeImage[CB_groupe.ItemIndex+1, 4];
     E_FondPrincipal.Text := GroupeImage[CB_groupe.ItemIndex+1, 5];
     E_FondSaisie.Text := GroupeImage[CB_groupe.ItemIndex+1, 6];
     E_FondValidation.Text := GroupeImage[CB_groupe.ItemIndex+1, 7];
     E_FondCmd.Text := GroupeImage[CB_groupe.ItemIndex+1, 8];
     MajInterface;
end;

procedure TParametrage.BT_annulerClick(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TParametrage.BT_okClick(Sender: TObject);
var
   F : TextFile;
   FMod : TextFile;
   i, j : integer;
   Attributes : word;
   trouve, ret : boolean;
   s : Array[1..nbrmax] of string;
   chaine : Array[0..300] of char;
   chaine2 : Array[0..300] of char;
   nbrparamlu : integer;
   langue : string;
begin
//--> PARAMWEB <----------------------------------------------------------------
    // V�rification obligatoire
    if not(FileExists(repertoire+'\Modulesfr.txt')) or not(FileExists(repertoire+'\Moduleses.txt')) then begin
        MessageDlg('Fichiers Moduleses.txt ou Modulesfr.txt manquants.', mtWarning, [mbOk], 0);
        Exit;
    end;
    //
    ChDir(repertoire);
    if CB_Modele.Text = Modele[1] then
        MiseEnTable(CB_Groupe.ItemIndex);

    // V�rification des attributs du fichier Paramweb.jwm (lecture seule)
    Attributes := FileGetAttr('paramweb.jwm');
    if Attributes and faReadOnly <> 0 then
        FileSetAttr('paramweb.jwm', Attributes and not faReadOnly);

    // Mise � jour du fichier
    AssignFile(F, 'paramweb.jwm');
    Rewrite(F);
    for i := 1 to nbrgroupe do begin
        if Groupe[i] = '' then break;
        WriteLn(F, Groupe[i]+'MOD    ' + #9 + Trim(GroupeModele[i]));
        WriteLn(F, Groupe[i]+'TIT    ' + #9 + Trim(GroupeImage[i, 1]));
        WriteLn(F, Groupe[i]+'BOU    ' + #9 + Trim(GroupeImage[i, 2]));
        WriteLn(F, Groupe[i]+'LOG    ' + #9 + Trim(GroupeImage[i, 3]));
        WriteLn(F, Groupe[i]+'FPG    ' + #9 + Trim(GroupeImage[i, 4]));
        WriteLn(F, Groupe[i]+'FPR    ' + #9 + Trim(GroupeImage[i, 5]));
        WriteLn(F, Groupe[i]+'FSA    ' + #9 + Trim(GroupeImage[i, 6]));
        WriteLn(F, Groupe[i]+'FCM    ' + #9 + Trim(GroupeImage[i, 8]));
        WriteLn(F, Groupe[i]+'FVA    ' + #9 + Trim(GroupeImage[i, 7]));
    end;
    CloseFile(F);
    ret := CopyFile('paramweb.jwm', '..\paramweb.jwm', False);

//--> MOD <---------------------------------------------------------------------
    // Le fichier Param
{$I-}
    // Ouverture du fichier PARAM.JWM en lecture
    AssignFile(FMod, 'Param.jwm');
    FileMode := 0;
    Reset(FMod);
    i := 0;
    // On scanne le fichier � la recherche des constantes de Param
    while not eof(FMod) do begin
        ReadLn(FMod, chaine);
        for j := 1 to nbrparam do
            // test sur la 1� position (a voir)
            if Pos(Param[j], chaine) <> 0 then begin
                if j = 1 then langue := Trim(Copy(chaine, 7, Length(chaine)-6));
                s[i] := chaine;
                Inc(i);
            end;
    end;
    nbrparamlu := i;
    CloseFile(FMod);

    // Re�criture du fichier Param.jwm
    AssignFile(F, 'param.jwm');
    Rewrite(F);
    for i := 0 to nbrparamlu-1 do
        WriteLn(F, s[i]);
    for i := 1 to nbrgroupe do begin
        if Groupe[i] = '' then break;
        WriteLn(F, Groupe[i]+'MOD    ' + #9 + Trim(GroupeModele[i]));
        WriteLn(F, Groupe[i]+'TIT    ' + #9 + Trim(GroupeImage[i, 1]));
        WriteLn(F, Groupe[i]+'BOU    ' + #9 + Trim(GroupeImage[i, 2]));
        WriteLn(F, Groupe[i]+'LOG    ' + #9 + Trim(GroupeImage[i, 3]));
        WriteLn(F, Groupe[i]+'FPG    ' + #9 + Trim(GroupeImage[i, 4]));
        WriteLn(F, Groupe[i]+'FPR    ' + #9 + Trim(GroupeImage[i, 5]));
        WriteLn(F, Groupe[i]+'FSA    ' + #9 + Trim(GroupeImage[i, 6]));
        WriteLn(F, Groupe[i]+'FCM    ' + #9 + Trim(GroupeImage[i, 8]));
        WriteLn(F, Groupe[i]+'FVA    ' + #9 + Trim(GroupeImage[i, 7]));
    end;
    CloseFile(F);
{$I+}

    // Les modules classiques
    for j:= 2 to nbrmodule do begin
        // V�rification des attributs du fichier XXX.jwm (lecture seule)
        Attributes := FileGetAttr(Module[j] +'.jwm');
        if Attributes and faReadOnly <> 0 then
            FileSetAttr(Module[j]+'.jwm', Attributes and not faReadOnly);
{$I-}
        // Ouverture du fichier XXX.JWM en �criture
        AssignFile(F, Module[j]+'.jwm');
        Rewrite(F);

        // Recherche dans Modules.jwm de GVM
        AssignFile(FMod, 'Modules'+langue+'.txt');
        FileMode := 0;
        Reset(FMod);
        // On scanne le fichier � la recherche du marqueur
        while not eof(FMod) do begin
            ReadLn(FMod, chaine);
            StrPCopy(chaine2, Module[j]);
            if (StrLComp(PChar(@chaine), PChar(@chaine2), 3) = 0) then
                break;
        end;

        // On recopie le fichier Modules.jwm jusqu'au marqueur (===)
        ReadLn(FMod, chaine);
        repeat
            WriteLn(F, chaine);
            ReadLn(FMod, chaine);
        until StrLComp(PChar(@chaine), '===', 3) = 0;

        // On recopie Param.jwm et le Paramweb.jwm
        for i := 0 to nbrparamlu-1 do
            WriteLn(F, s[i]);
        for i := 1 to nbrgroupe do begin
            if Groupe[i] = '' then break;
            WriteLn(F, Groupe[i]+'MOD    ' + #9 + Trim(GroupeModele[i]));
            WriteLn(F, Groupe[i]+'TIT    ' + #9 + Trim(GroupeImage[i, 1]));
            WriteLn(F, Groupe[i]+'BOU    ' + #9 + Trim(GroupeImage[i, 2]));
            WriteLn(F, Groupe[i]+'LOG    ' + #9 + Trim(GroupeImage[i, 3]));
            WriteLn(F, Groupe[i]+'FPG    ' + #9 + Trim(GroupeImage[i, 4]));
            WriteLn(F, Groupe[i]+'FPR    ' + #9 + Trim(GroupeImage[i, 5]));
            WriteLn(F, Groupe[i]+'FSA    ' + #9 + Trim(GroupeImage[i, 6]));
            WriteLn(F, Groupe[i]+'FCM    ' + #9 + Trim(GroupeImage[i, 8]));
            WriteLn(F, Groupe[i]+'FVA    ' + #9 + Trim(GroupeImage[i, 7]));
        end;
        CloseFile(F);
        CloseFile(FMod);
{$I+}
        // Si le fichier n'existe pas
        if IOResult <> 0 then begin
            ShowMessage('Erreur lors de la lecture du fichier.');
            Exit;
        end;
    end;

//--> FIN <---------------------------------------------------------------------
    Application.Terminate;
end;

procedure TParametrage.BT_RestaurationClick(Sender: TObject);
begin
    ChDir(repertoire);
    CopyFile('..\paramweb.jwm', 'paramweb.jwm', False);
    LectureParam;
end;

procedure TParametrage.CB_ModeleChange(Sender: TObject);
var
    typemodele : string;
begin
    // Recherche du modele associ� au groupe
    typemodele := CB_Modele.Text;
    GroupeModele[CB_Groupe.ItemIndex+1] := typemodele;

    // Rafraichissement des infos
    ChargementImage;
    MajInterface;
end;

procedure TParametrage.BT_ModifGroupeClick(Sender: TObject);
begin
    if BT_ModifGroupe.Caption = 'Modification' then begin
        E_Groupe.Visible := True;
        BT_Ajout.Visible := True;
        BT_Suppr.Visible := True;
        CB_Modele.Enabled := True;
        E_Groupe.setFocus;
        BT_ModifGroupe.Caption := 'Stop modif'; end
    else begin
        E_Groupe.Visible := False;
        BT_Ajout.Visible := False;
        BT_Suppr.Visible := False;
        CB_Modele.Enabled := False;
        BT_ModifGroupe.Caption := 'Modification';
    end;
end;

procedure TParametrage.BT_AjoutClick(Sender: TObject);
var
    i : integer;
begin
    // Si la zone de saisie est vide
    if Trim(E_Groupe.Text) = '' then Exit;

    // Ajout du groupe dans la combo
    if CB_Groupe.Items.IndexOf(Trim(E_Groupe.Text)) <> -1 then begin
        ShowMessage('Groupe d�j� existant.');
        E_Groupe.Text := '';
        Exit;
    end;

    // Tout est Ok
    CB_Groupe.Items.Add(Trim(UpperCase(E_Groupe.Text)));
    i := CB_Groupe.Items.IndexOf(Trim(E_Groupe.Text));
    CB_Groupe.ItemIndex := i;
    Groupe[i+1] := Trim(UpperCase(E_Groupe.Text));
    GroupeModele[i+1] := Modele[2];
    CB_Modele.ItemIndex := CB_Modele.Items.IndexOf(Modele[2]);
    E_Groupe.Text := '';
    ChargementImage;
    MajInterface;
    MiseEnTable(CB_groupe.ItemIndex);
end;

procedure TParametrage.BT_SupprClick(Sender: TObject);
var
    i, j, k : integer;
begin
    // Si la zone de saisie est vide
    if Trim(E_Groupe.Text) = '' then Exit;

    // Demande de confirmation de la suppression
    if MessageDlg('Confirmez-vous la suppression ?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
        Exit;

    // Tout est Ok
    i := CB_Groupe.ItemIndex+1;
    CB_Groupe.Items.Delete(CB_Groupe.ItemIndex);
    Groupe[i] := '';
    GroupeModele[i] := '';
    E_Groupe.Text := '';
    // Remise en forme des tables
    for j := i to CB_Groupe.Items.Count do begin
        Groupe[j] := Groupe[j+1];
        GroupeModele[j] := GroupeModele[j+1];
        for k := 1 to 8 do
            GroupeImage[j, k] := GroupeImage[j+1, k];
    end;
    for j := CB_Groupe.Items.Count+1 to nbrgroupe do begin
        Groupe[j] := '';
        GroupeModele[j] := '';
        for k := 1 to 8 do
            GroupeImage[j, k] := '';
    end;

    CB_Groupe.ItemIndex := 0;
    CB_Modele.ItemIndex := CB_Modele.Items.IndexOf(GroupeModele[1]);
end;

procedure TParametrage.CB_GroupeClick(Sender: TObject);
var
    indice : integer;
    typemodele : string;
begin
    E_Groupe.Text := CB_Groupe.Text;
    if CB_Modele.Text = Modele[1] then
        MiseEnTable(indiceGroupe);

    // Recherche du modele associ� au groupe
    typemodele := GroupeModele[CB_Groupe.ItemIndex+1];
    CB_Modele.ItemIndex := CB_Modele.Items.IndexOf(typemodele);
    indiceGroupe := CB_Groupe.ItemIndex;
end;

procedure TParametrage.CB_GroupeChange(Sender: TObject);
var
    i : integer;
begin
    // Rafraichissement des infos
    E_BarreTitre.Text := GroupeImage[CB_groupe.ItemIndex+1, 1];
    E_BarreBoutons.Text := GroupeImage[CB_groupe.ItemIndex+1, 2];
    E_Logo.Text := GroupeImage[CB_groupe.ItemIndex+1, 3];
    E_PageGarde.Text := GroupeImage[CB_groupe.ItemIndex+1, 4];
    E_FondPrincipal.Text := GroupeImage[CB_groupe.ItemIndex+1, 5];
    E_FondSaisie.Text := GroupeImage[CB_groupe.ItemIndex+1, 6];
    E_FondValidation.Text := GroupeImage[CB_groupe.ItemIndex+1, 7];
    E_FondCmd.Text := GroupeImage[CB_groupe.ItemIndex+1, 8];
    MajInterface;
end;

end.
