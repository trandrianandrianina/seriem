program nouvdo;
{$APPTYPE GUI}

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  ShellApi,
  StdCtrls,
  ShlObj;
(*  , Graphics, Controls, Forms, Dialogs;*)

{$R *.RES}

var
    dossier : array[1..20] of string;
    tab : array[1..20] of integer;
    i, j : integer;
    chemin, chaine1, chaine2 : string;
    position : integer;
    lecteur : boolean;
begin
    if ParamCount <> 1 then
        Halt(1);

    // Initialisation des variables
    lecteur := true;
    chemin := Trim(ParamStr(1));
    for i:= 1 to 20 do begin
        dossier[i] :=  '';
        tab[i] := 0;
    end;

    // Extraction des dossier;
    // On recupere la position des "\"
    i := 1;
    j := 1;
    while i<=Length(chemin) do begin
        if chemin[i] = '\' then begin
            tab[j] := i;
            Inc(j);
        end;
        Inc(i);
    end;

    // On recupere les nom des dossiers
    if (tab[1] = 1) and (tab[2] = 2) then lecteur := false;
    i := 1;
    j := 1;
    while (i<20) and (tab[i]<>0) do begin
        if tab[i] <= 4 then begin
            Inc(i);
            continue;
        end;
        dossier[j] := Copy(chemin, 1, tab[i]-1);
        Inc(j);
        Inc(i);
    end;
    dossier[j] := chemin;
    Inc(j);

    // On cr�� l'arborescence
    if lecteur = false then i := 2
    else i := 1;
    while (i<=20) and (dossier[i]<>'') do begin
        CreateDir(dossier[i]);
        Inc(i);
    end;

end.
