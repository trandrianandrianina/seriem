//------------------------------------------------------------------------------
//--> Outil pour transf�rer des objets SAVF de l'AS/400 vers le PC
//--> Param�tres:
//-->     Nom du fichier SAVF (avec l'extension .savf)     [Facultatif]
//-->     Chemin destination sur le PC                     [Facultatif]
//-->     Nom de la biblioth�que source sur l'AS/400       [Facultatif]
//-->     Adresse IP de l'AS/400                           [Facultatif]
//-->     Titre que l'on souhaite donner � l'application   [Facultatif]
//------------------------------------------------------------------------------

unit transftpaspc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Psock, NMFtp, ComCtrls;

type
  TF_Transfert = class(TForm)
    GB_Connection: TGroupBox;
    L_Ip: TLabel;
    L_Port: TLabel;
    E_Ip: TEdit;
    E_Port: TEdit;
    GB_Proxy: TGroupBox;
    CB_Proxy: TCheckBox;
    L_Proxy: TLabel;
    L_PortProxy: TLabel;
    E_Proxy: TEdit;
    E_PortProxy: TEdit;
    L_Login: TLabel;
    L_Pwd: TLabel;
    E_Login: TEdit;
    E_Pwd: TEdit;
    BT_Connection: TButton;
    GB_Transfert: TGroupBox;
    E_FichierPC: TEdit;
    L_FichierPC: TLabel;
    BT_Transfert: TButton;
    GB_Status: TGroupBox;
    L_Status: TLabel;
    BT_Fermer: TButton;
    PB_Transfert: TProgressBar;
    NMFTP_Transfert: TNMFTP;
    procedure BT_connectionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BT_transfertClick(Sender: TObject);
    procedure NMFTP_TransfertSuccess(Trans_Type: TCmdType);
    procedure NMFTP_TransfertConnectionFailed(Sender: TObject);
    procedure NMFTP_TransfertConnect(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CB_ProxyClick(Sender: TObject);
    procedure BT_FermerClick(Sender: TObject);
    procedure NMFTP_TransfertDisconnect(Sender: TObject);
    procedure NMFTP_TransfertFailure(var Handled: Boolean; Trans_Type: TCmdType);
    procedure NMFTP_TransfertError(Sender: TComponent; Errno: Word; Errmsg: String);
    procedure NMFTP_TransfertStatus(Sender: TComponent; Status: String);
    procedure NMFTP_TransfertAuthenticationFailed(var Handled: Boolean);
    procedure NMFTP_TransfertAuthenticationNeeded(var Handled: Boolean);
    procedure E_PwdKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure NMFTP_TransfertPacketRecvd(Sender: TObject);
  private
    { D�clarations priv�es}
    NomFichier : string;
    DossierDest : string;
    Bibliotheque : string;
  public
    { D�clarations publiques}
  end;

var
  F_Transfert: TF_Transfert;
  erreur, danslaplace : boolean;
  erreurmsg : string;

implementation

{$R *.DFM}

procedure TF_Transfert.BT_connectionClick(Sender: TObject);
begin
    if BT_connection.Caption = '&Connexion' then begin
        L_Status.caption := 'Connexion en cours ...';
        BT_Fermer.Enabled := False;
        if CB_Proxy.Checked = True  then begin
            NMFTP_Transfert.Proxy := E_proxy.text;
            NMFTP_Transfert.ProxyPort := StrToInt(E_portproxy.text);
        end;
        NMFTP_Transfert.Host := E_ip.text;
        NMFTP_Transfert.Port := StrToInt(E_port.text);
        NMFTP_Transfert.TimeOut :=  20000;
        NMFTP_Transfert.UserID := E_login.text;
        NMFTP_Transfert.Password := E_pwd.text;
        NMFTP_Transfert.Connect;
        GB_Transfert.Visible := True;
        BT_Connection.Caption := '&D�connexion';
        BT_Fermer.Caption := 'D�connecter et &Fermer'; end
    else begin
        NMFTP_Transfert.Disconnect;
        E_Pwd.Text := '';
        BT_connection.Caption := '&Connexion';
        BT_Fermer.Caption := '&Fermer';
        L_Status.caption := 'D�connexion r�ussie';
        GB_Transfert.Visible := False;
    end;
end;

procedure TF_Transfert.FormCreate(Sender: TObject);
begin
    CB_Proxy.Checked := False;
    GB_Proxy.Visible := False;
    GB_Transfert.Visible := False;
    erreurmsg := '';
    danslaplace := False;

    case ParamCount of
        0 : begin
                NomFichier := '';
                DossierDest := '';
                Bibliotheque := 'seriemftp';
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert AS/400 -> PC';
            end;
        1 : begin

                NomFichier := ParamStr(1);
                DossierDest := '';
                Bibliotheque := 'seriemftp';
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert AS/400 -> PC';
            end;
        2 : begin
                NomFichier := ParamStr(1);
                DossierDest := ParamStr(2);
                Bibliotheque := 'seriemftp';
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert AS/400 -> PC';
            end;
        3 : begin
                NomFichier := ParamStr(1);
                DossierDest := ParamStr(2);
                Bibliotheque := ParamStr(3);
                E_ip.text := '127.0.0.1';
                F_Transfert.Caption := 'Transfert AS/400 -> PC';
            end;
        4 : begin
                NomFichier := ParamStr(1);
                DossierDest := ParamStr(2);
                Bibliotheque := ParamStr(3);
                E_ip.text := ParamStr(4);
                F_Transfert.Caption := 'Transfert AS/400 -> PC';
            end;
        5 : begin
                NomFichier := ParamStr(1);
                DossierDest := ParamStr(2);
                Bibliotheque := ParamStr(3);
                E_ip.text := ParamStr(4);
                F_Transfert.Caption := Paramstr(5);
            end;
    end;
    if DossierDest <> '' then
        E_FichierPC.Text := Trim(DossierDest) + '\' + Trim(NomFichier)
    else
        E_FichierPC.Text := Trim(NomFichier);
end;

procedure TF_Transfert.BT_transfertClick(Sender: TObject);
begin
    // On pr�vient que le transfert � �t� lanc�
    BT_Fermer.Enabled := False;
    BT_Connection.Enabled := False;
    PB_Transfert.Position := 0;

    // On se met dans la bibiloth�que SERIEMFTP
    if danslaplace = False then
        NMFTP_Transfert.ChangeDir(Bibliotheque);

    // On initialise le mode pour l'AS/400
    if erreur = False then begin
        L_Status.Caption := 'Transfert en cours ...';
        danslaplace := True;
        erreurmsg := 'Echec lors de SITE NAMEFMT 1';
        NMFTP_Transfert.DoCommand('site namefmt 1');
        NMFTP_Transfert.Download(NomFichier, E_FichierPC.Text); end
    else begin
        BT_Fermer.Enabled := True;
        BT_Connection.Enabled := True;
    end;
end;

procedure TF_Transfert.NMFTP_TransfertSuccess(Trans_Type: TCmdType);
begin
    case Trans_Type of
        cmdDownload : begin
                      end;
        cmdUpload: begin
                       L_Status.caption := 'Transfert ex�cut� avec succ�s';
                       BT_Fermer.Enabled := True;
                       BT_Connection.Enabled := True;
                       BT_Fermer.SetFocus;
                   end;
    end;
end;

procedure TF_Transfert.NMFTP_TransfertConnectionFailed(Sender: TObject);
begin
    L_Status.caption := 'Echec � la connexion';
end;

procedure TF_Transfert.NMFTP_TransfertConnect(Sender: TObject);
begin
    BT_Fermer.Enabled := True;
    L_Status.caption := 'Connexion r�ussie';
    NMFTP_Transfert.Mode(MODE_IMAGE);
end;


procedure TF_Transfert.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    NMFTP_Transfert.Disconnect;
end;

procedure TF_Transfert.CB_ProxyClick(Sender: TObject);
begin
    if CB_Proxy.Checked = True then
        GB_Proxy.Visible := True
    else
        GB_Proxy.Visible := False;
end;

procedure TF_Transfert.BT_FermerClick(Sender: TObject);
begin
    NMFTP_Transfert.Disconnect;
    Close;
end;

procedure TF_Transfert.NMFTP_TransfertDisconnect(Sender: TObject);
begin
    L_Status.Caption := 'D�connect�';
    GB_Transfert.Visible := False;
    BT_Fermer.Enabled := True;
    BT_Connection.Enabled := True;
    BT_connection.Caption := '&Connexion';
end;

procedure TF_Transfert.NMFTP_TransfertFailure(var Handled: Boolean;
  Trans_Type: TCmdType);
begin
    Handled := True;
    erreur := True;
    Case Trans_Type of
        cmdChangeDir: L_Status.Caption := 'Echec du ChangeDir';
        cmdUpload: begin
                       L_Status.Caption := 'Echec du transfert';
                       BT_Fermer.Enabled := True;
                       BT_Connection.Enabled := True;
                   end;
        cmdDownload: begin
                       L_Status.Caption := 'Echec du transfert';
                       BT_Fermer.Enabled := True;
                       BT_Connection.Enabled := True;
                   end;
        cmdDoCommand: L_Status.Caption := erreurmsg;
    else
        erreur := False;
    end;

end;

procedure TF_Transfert.NMFTP_TransfertError(Sender: TComponent; Errno: Word;
  Errmsg: String);
begin
    L_Status.Caption := 'Erreur n�' + IntToStr(Errno);
    BT_Fermer.Enabled := True;
    BT_Connection.Enabled := True;
end;

procedure TF_Transfert.NMFTP_TransfertStatus(Sender: TComponent; Status: String);
begin
     L_Status.Caption := Status + '('+ IntToStr(NMFTP_Transfert.ReplyNumber) +')';
     BT_Fermer.Enabled := True;
     BT_Connection.Enabled := True;
end;

procedure TF_Transfert.NMFTP_TransfertAuthenticationFailed(var Handled: Boolean);
begin
     L_Status.Caption := 'Erreur lors de l''identification';
end;

procedure TF_Transfert.NMFTP_TransfertAuthenticationNeeded(var Handled: Boolean);
begin
     L_Status.Caption := 'Erreur lors de l''identification';
end;


procedure TF_Transfert.E_PwdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then BT_connectionClick(BT_Connection);
end;


procedure TF_Transfert.FormShow(Sender: TObject);
begin
    if E_Ip.Text <> '127.0.0.1' then
        E_Pwd.SetFocus;
end;

procedure TF_Transfert.NMFTP_TransfertPacketRecvd(Sender: TObject);
begin
    PB_Transfert.Max := NMFTP_Transfert.BytesTotal;
    PB_Transfert.Position := Round(NMFTP_Transfert.BytesRecvd);
end;

end.
