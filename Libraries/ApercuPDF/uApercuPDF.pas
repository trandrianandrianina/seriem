unit uApercuPDF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PReport, ExtCtrls, StdCtrls, Buttons, PRJpegImage, ExtDlgs, PdfDoc, ShellApi;

type
  TF_PDFPreview = class(TForm)
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    PRPage1: TPRPage;
    PRLayoutPanel1: TPRLayoutPanel;
    PRJpegImage1: TPRJpegImage;
    BT_Enregistrer: TSpeedButton;
    PReport1: TPReport;
    SaveDialog1: TSaveDialog;
    BT_Fermer: TSpeedButton;
    PRJpegImage2: TPRJpegImage;
    BT_Acrobat: TSpeedButton;
    function EcritLigne(chaine : String; taillepolice:integer; bold:Boolean; lignecourante, indice: integer) : integer;
    procedure GenereDocPortrait(texte : TStringList; coef : real);
    procedure GenereDocPaysage(texte : TStringList; coef : real);
    procedure BT_EnregistrerClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure PRPage1PrintPage(Sender: TObject; ACanvas: TPRCanvas);
    procedure FormCreate(Sender: TObject);
    procedure BT_FermerClick(Sender: TObject);
    procedure BT_AcrobatClick(Sender: TObject);
  private
    { Private}
    isAffichable : string;          // 0:non affichable 1:affichable  2:affichable dans Acrobat 3:Impression avec Acrobat
    nomFichierPDF : string;
    nomFichierTXT : string;
    nomFichierLCK : string;
    imageFond : string;
    logo : string;
    format : string;  // 0:Portrait 1:Paysage
    createur : string;
    nbrfdp : integer;
    deblabel, finlabel : integer;
    debImage : integer;
  public
    { Public}
  end;

var
  F_PDFPreview: TF_PDFPreview;

const
  PORTRAIT : string='1';
  PAYSAGE : string='0';

implementation

{$R *.DFM}

//------------------------------------------------------------------------------
function TF_PDFPreview.EcritLigne(chaine : String; taillepolice:integer; bold:Boolean; lignecourante, indice: integer) : integer;
var
    i, j  : integer;
    Label1 : TPRLabel;
    posdeb, posfin : integer;
    trouveTag : boolean;
    nbrlabel : integer;
    tabTexte : array[1..20] of String;
    tabBold : array[1..20] of boolean;
    tabTaille : array[1..20] of integer;
    isBold : boolean;
    taille, coeftaille : integer;
    tabTok : array[1..256] of integer;
begin
    nbrLabel := 0;

    // On initialise la table
    for i:=1 to 256 do
        tabTok[i] := 0;

    // Recherche de Tag
    isBold := false;
    taille := taillepolice;
    trouveTag := false;
    repeat
        posdeb := Pos('�', Copy(chaine, 1, Length(chaine)));
        // On a trouv� un tag
        if posdeb <> 0 then begin
            trouveTag := true;

            if chaine[posdeb+1] = 'G' then begin
                isbold := not isBold;
                if isBold then tabTok[posdeb] := 1
                else tabTok[posdeb] := -1; end
            else
            if chaine[posdeb+1] = '6' then begin
                if taille = 6 then tabTok[posdeb] := 6
                else begin
                    tabTok[posdeb] := -6;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = '7' then begin
                if taille = 7 then tabTok[posdeb] := 7
                else begin
                    tabTok[posdeb] := -7;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = '8' then begin
                if taille = 8 then tabTok[posdeb] := 8
                else begin
                    tabTok[posdeb] := -8;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = '9' then begin
                if taille = 9 then tabTok[posdeb] := 9
                else begin
                    tabTok[posdeb] := -9;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = 'A' then begin
                if taille = 10 then tabTok[posdeb] := 10
                else begin
                    tabTok[posdeb] := -10;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = 'B' then begin
                if taille = 11 then tabTok[posdeb] := 11
                else begin
                    tabTok[posdeb] := -11;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = 'C' then begin
                if taille = 12 then tabTok[posdeb] := 12
                else begin
                    tabTok[posdeb] := -12;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = 'D' then begin
                if taille = 13 then tabTok[posdeb] := 13
                else begin
                    tabTok[posdeb] := -13;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = 'E' then begin
                if taille = 14 then tabTok[posdeb] := 14
                else begin
                    tabTok[posdeb] := -14;
                    taille := taillepolice;
                end; end
            else
            if chaine[posdeb+1] = 'F' then begin
                if taille = 16 then tabTok[posdeb] := 16
                else begin
                    tabTok[posdeb] := -16;
                    taille := taillepolice;
                end;
            end;

            // On efface le marqueur dans la chaine
            chaine[posdeb] := ' ';
            chaine[posdeb+1] := ' ';
        end;
    until posdeb = 0;

    // si on a trouv� des tags
    if trouveTag then begin
        // On parcourt tabTok
        posdeb := 1;
        posfin := 1;
        while posfin <= Length(chaine) do begin
            // On a trouv� le d�but d'un marqueur
            if tabTok[posfin] > 0 then begin
                // Si debut de ligne
                if posfin = 1 then posdeb := posfin
                else
                    // Si c'est le debut de la chaine
                    if posdeb = 1 then begin
                        Inc(nbrLabel);
                        tabTexte[nbrLabel] := Copy(chaine, posdeb, posfin+2-posdeb);
                        tabBold[nbrLabel] := false;
                        tabTaille[nbrLabel] := taillepolice;
                        posdeb := posfin+2;
                    end
                    else
                        // Si 2 marqueurs c�te � c�te
                        if tabTok[posfin-2] <= 0 then posdeb := posfin;
            end
            else begin
                // On a trouv� la fin d'un marqueur
                if tabTok[posfin] < 0 then begin
                    // On d�coupe la chaine
                    Inc(nbrLabel);
                    tabTexte[nbrLabel] := Copy(chaine, posdeb, posfin-posdeb);
                    tabBold[nbrLabel] := false;
                    tabTaille[nbrLabel] := taillepolice;

                    if tabTok[posfin] = -1 then
                        tabBold[nbrLabel] := true
                    else
                        if tabTok[posfin] < 0 then
                            tabTaille[nbrLabel] := abs(tabTok[posfin]);

                    if tabTok[posfin+2] = -1 then
                        tabBold[nbrLabel] := true
                    else
                        if tabTok[posfin+2] < 0 then
                            tabTaille[nbrLabel] := abs(tabTok[posfin+2]);

                    posdeb := posfin;
                end;
            end;
            Inc(posfin);
        end;
        Inc(nbrLabel);
        tabTexte[nbrLabel] := Copy(chaine, posdeb, Length(chaine)-posdeb+1);
        tabBold[nbrLabel] := false;
        tabTaille[nbrLabel] := taillepolice;
        end
    else begin
        nbrLabel :=1;
        tabTexte[1] := chaine;
        tabBold[1] := false;
        tabTaille[1] := taillepolice;
    end;

    // Ecriture du texte dans la page
    posdeb := 3;
    for i:=1 to nbrlabel do begin
        Label1 := TPRLabel.Create(Self);
        Label1.Parent := PRLayoutPanel1;
        Label1.Name := 'Label' + IntToStr(indice);
        Label1.FontBold := tabBold[i];
        Label1.FontName := fnFixedWidth;
        Label1.FontSize := tabTaille[i];
        if taillepolice = 10 then begin
            Label1.Top := (lignecourante-1) * (taillepolice-1);
            coeftaille := 6; end
        else
            if taillepolice = 8 then begin
                Label1.Top := (lignecourante-1) * (taillepolice+4);
                coeftaille := 5; end
            else
                if taillepolice = 7 then begin
                    Label1.Top := (lignecourante-1) * (taillepolice+5); // Avt 8 mais pb sur edition des comptes auxiliaires
                    coeftaille := 4; end
                else begin
                    Label1.Top := (lignecourante-1) * (taillepolice+6); // Avt 9 mais pb sur edition des comptes auxiliaires
                    coeftaille := 3;
                end;
         if i > 1 then
             posdeb := posdeb + Length(tabTexte[i-1]) * coeftaille;
         Label1.Left := posdeb;
         Label1.Width := 595;
         Label1.Caption := tabTexte[i];
         Inc(indice);
    end;

    EcritLigne := indice;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.GenereDocPortrait(texte : TStringList; coef : real);
var
    i, j : integer;
    chaine : String;
    ligne, saut, lignecourante, taillepolice : integer;
    Label1 : TPRLabel;
    nbrcolmax : integer;
    bold : boolean;
    indexligne : integer;
    imagefdp : String;
    indexfdp : integer;
    cptfdp: integer;
begin
    // On v�rifie le nombre de colonne du spool
    // On parcourt le spool afin de rechercher la colonne la plus longue
    // et on calcule aussi le nombre de pages
    nbrcolmax := 0;
    for i:=0 to texte.Count-1 do begin
        if nbrcolmax < Length(texte[i]) then
            nbrcolmax := Length(texte[i]);
    end;
    // Choix de la police en fonction du nombre de colonne (test avec caract�res de controle)
        if nbrcolmax <= 103 then // 99
        taillepolice := 10
    else
        if nbrcolmax <= 131 then // 127 taille spool
            taillepolice := 8
        else
           if nbrcolmax <= 144 then // 140
            taillepolice := 7
           else
            taillepolice := 6;


    // Ecriture page par page
    PReport1.FileName := nomFichierPDF;
    PReport1.Creator := createur;
    PReport1.BeginDoc;

    // V�rification du nombre de fond de pages & chargement si besoin
    if nbrfdp <> 0 then begin
       if PRJpegImage1 <> nil then PRJpegImage1.Free;
       indexfdp := 1;
       cptfdp := 1;
       chaine := '_' + IntToStr(indexfdp) + '.';
       imagefdp := StringReplace(imageFond, '.', chaine, []);
       if FileExists(imagefdp) then begin
             PRJpegImage1 := TPRJpegImage.Create(Self);
             PRJpegImage1.Parent := PRLayoutPanel1;
             PRJpegImage1.Name := 'PRJpegImage_' + IntToStr(cptfdp);
             PRJpegImage1.Width := 596;
             PRJpegImage1.Height := 842;
             PRJpegImage1.Stretch := True;
             PRJpegImage1.Picture.LoadFromFile(imagefdp);
        end;
    end;

    // Ecriture du texte dans l'image de fond
    lignecourante := 1;
    indexligne := 0;
    deblabel := 0;
    finlabel := 0;
    for i:=0 to texte.Count-1 do begin
        bold := False;
        chaine := Trim(Copy(texte[i], 1, 3));
        if chaine <> '' then begin
            ligne := Round(StrToInt(chaine) * coef);
            // D�tection d'une nouvelle page
            if lignecourante > ligne then begin
                PReport1.Print(PRPage1);
                for j := deblabel to finlabel do begin
                    Label1 := TPRLabel(FindComponent('Label' + IntToStr(j)));
                    if Label1 <> nil then Label1.Free;
                end;
                indexligne := i;
               // Changement du fond de pages si n�cessaire
               if nbrfdp <> 0 then begin
                  Inc(indexfdp);
                  if indexfdp > nbrfdp then indexfdp := 1;
                  Inc(cptfdp);
                  imagefdp := StringReplace(imageFond, '.', '_' + IntToStr(indexfdp) + '.', []);
                  if FileExists(imagefdp) then begin
                      PRJpegImage1 := TPRJpegImage.Create(Self);
                      PRJpegImage1.Parent := PRLayoutPanel1;
                      PRJpegImage1.Name := 'PRJpegImage_' + IntToStr(cptfdp);
                      PRJpegImage1.Width := 596;
                      PRJpegImage1.Height := 842;
                      PRJpegImage1.Stretch := True;
                      PRJpegImage1.Picture.LoadFromFile(imagefdp);
                  end;
               end;

            end;
            saut := ligne-lignecourante;
            lignecourante := ligne; end
        else begin
            chaine := Trim(Copy(texte[i], 4, 1));
            saut := StrToInt(chaine);
            //if saut = 0 then bold := True;
            lignecourante := lignecourante + saut;
        end;
        finlabel := EcritLigne(Copy(texte[i], 5, Length(texte[i])-4), taillepolice, bold, lignecourante, finlabel);
    end;
    // On �crit la derni�re page avant de cloturer le doc
    PReport1.Print(PRPage1);
    PReport1.EndDoc;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.GenereDocPaysage(texte : TStringList; coef : real);
var
    i, j : integer;
    chaine : String;
    ligne, saut, lignecourante, taillepolice : integer;
    Label1 : TPRLabel;
    nbrcolmax : integer;
    bold : boolean;
    indexligne : integer;
    imagefdp : String;
    indexfdp : integer;
    cptfdp: integer;
begin
    // On v�rifie le nombre de colonne du spool
    // On parcourt le spool afin de rechercher la colonne la plus longue
    // et on calcule aussi le nombre de pages
    nbrcolmax := 0;
    for i:=0 to texte.Count-1 do begin
        if nbrcolmax < Length(texte[i]) then
            nbrcolmax := Length(texte[i]);
    end;
    // Choix de la police en fonction du nombre de colonne (test avec caract�res de controle)
    // Choix de la police en fonction du nombre de colonne (test avec caract�res de controle)
    if nbrcolmax <= 142 then // 138
        taillepolice := 10
    else
        if nbrcolmax <= 176 then // 172 taille spool
            taillepolice := 8
        else
           if nbrcolmax <= 200 then // 196
            taillepolice := 7
           else
            taillepolice := 6;
//ShowMessage(inttostr(nbrcolmax) + '/'+ inttostr(taillepolice));

    // Ecriture page par page
    PReport1.FileName := nomFichierPDF;
    PReport1.Creator := createur;
    PReport1.BeginDoc;

    // V�rification du nombre de fond de pages & chargement si besoin
    if nbrfdp <> 0 then begin
       if PRJpegImage1 <> nil then PRJpegImage1.Free;
       indexfdp := 1;
       cptfdp := 1;
       chaine := '_' + IntToStr(indexfdp) + '.';
       imagefdp := StringReplace(imageFond, '.', chaine, []);
       if FileExists(imagefdp) then begin
             PRJpegImage1 := TPRJpegImage.Create(Self);
             PRJpegImage1.Parent := PRLayoutPanel1;
             PRJpegImage1.Name := 'PRJpegImage_' + IntToStr(cptfdp);
             PRJpegImage1.Width := 842;
             PRJpegImage1.Height := 596;
             PRJpegImage1.Stretch := True;
             PRJpegImage1.Picture.LoadFromFile(imagefdp);
        end;
    end;

    // Ecriture du texte dans l'image de fond
    lignecourante := 1;
    indexligne := 0;
    deblabel := 0;
    finlabel := 0;
    for i:=0 to texte.Count-1 do begin
        bold := False;
        chaine := Trim(Copy(texte[i], 1, 3));
        if chaine <> '' then begin
            ligne := Round(StrToInt(chaine) * coef);
            // D�tection d'une nouvelle page
            if lignecourante > ligne then begin
                PReport1.Print(PRPage1);
                for j := deblabel to finlabel do begin
                    Label1 := TPRLabel(FindComponent('Label' + IntToStr(j)));
                    if Label1 <> nil then Label1.Free;
                end;
                indexligne := i;
               // Changement du fond de pages si n�cessaire
               if nbrfdp <> 0 then begin
                  Inc(indexfdp);
                  if indexfdp > nbrfdp then indexfdp := 1;
                  Inc(cptfdp);
                  imagefdp := StringReplace(imageFond, '.', '_' + IntToStr(indexfdp) + '.', []);
                  if FileExists(imagefdp) then begin
                      PRJpegImage1 := TPRJpegImage.Create(Self);
                      PRJpegImage1.Parent := PRLayoutPanel1;
                      PRJpegImage1.Name := 'PRJpegImage_' + IntToStr(cptfdp);
                      PRJpegImage1.Width := 596;
                      PRJpegImage1.Height := 842;
                      PRJpegImage1.Stretch := True;
                      PRJpegImage1.Picture.LoadFromFile(imagefdp);
                  end;
               end;

            end;
            saut := ligne-lignecourante;
            lignecourante := ligne; end
        else begin
            chaine := Trim(Copy(texte[i], 4, 1));
            saut := StrToInt(chaine);
            //if saut = 0 then bold := True;
            lignecourante := lignecourante + saut;
        end;
        finlabel := EcritLigne(Copy(texte[i], 5, Length(texte[i])-4), taillepolice, bold, lignecourante, finlabel);
    end;
    // On �crit la derni�re page avant de cloturer le doc
    PReport1.Print(PRPage1);
    PReport1.EndDoc;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.BT_EnregistrerClick(Sender: TObject);
begin
  SaveDialog1.FileName := nomFichierPDF;
  if SaveDialog1.Execute then
    with PReport1 do
    begin
      FileName := SaveDialog1.FileName;
      BeginDoc;
      Print(PRPage1);
      EndDoc;
    end;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.SpeedButton2Click(Sender: TObject);
begin
  ShellExecute(Self.Handle, 'Print', PChar(nomFichierPDF), '', '', SW_SHOW);
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.PRPage1PrintPage(Sender: TObject; ACanvas: TPRCanvas);
var
  Dest: TPRDestination;
begin
  // create a new destination for the current page.
  Dest := PReport1.CreateDestination;

  // setting the properties for the destination object.
  with Dest do
  begin
    DestinationType := dtXYZ;
    Left := -10;
    Top := -10;
    Zoom := 1;
  end;

  // set the destination object as the open-action.
  PReport1.OpenAction := Dest;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.FormCreate(Sender: TObject);
var
    texte : TStringList;
    i, j : integer;
    chaine : String;
    ligne, saut, lignecourante, taillepolice : integer;
    Label1 : TPRLabel;
    nbrcolmax : integer;
    bold : boolean;
    indexligne : integer;
    F : TextFile;
begin

    if ParamCount < 1 then begin
        ShowMessage('Il manque des param�tres.');
        Application.Terminate;
    end;
    isAffichable := ParamStr(1);
    nomFichierPDF := StringReplace(ParamStr(2), '/', '\', [rfReplaceAll]);
    nomFichierTXT := StringReplace(ParamStr(3), '/', '\', [rfReplaceAll]);
    imageFond := ParamStr(4);
    logo := ParamStr(5);
    format := ParamStr(6);  // 0:Portrait 1:Paysage
    createur := ParamStr(7);
    nbrfdp := StrToInt(ParamStr(8));

    // Validit� des param�tres
    if not FileExists(nomFichierTXT) then begin
        ShowMessage('Le fichier spool est introuvable.');
        Application.Terminate;
    end;

    // Creation du fichier de lock (� partir du fichier spool)
    nomFichierLCK := Trim(nomFichierTXT) + '.lck';

    // Chargement de l'image de fond
    if FileExists(imageFond) then
        PRJpegImage1.Picture.LoadFromFile(imageFond);

    // Chargement du logo (270x98)
    if FileExists(logo) then
        PRJpegImage2.Picture.LoadFromFile(logo);

    // V�rification si on enregistre le PDF en local ou pas
    if (isAffichable = '4') Or (isAffichable = '5') then begin
        chaine := nomFichierPDF;
        i := LastDelimiter('\/',chaine);
        chaine := Copy(chaine, i+1, Length(chaine)-i) ;
        SaveDialog1.FileName := chaine;
        if SaveDialog1.Execute then
            nomFichierPDF := SaveDialog1.FileName;
        if isAffichable = '4' then isAffichable := '2'
        else
            isAffichable := '3'
    end;

    // Chargement du fichier spool
    texte := TStringList.Create;
    texte.LoadFromFile(nomFichierTXT);

    // G�n�re le document au format Portrait
    if format = PORTRAIT then
        GenereDocPortrait(texte, 1)
    else begin // G�n�re le document au format Paysage
        PRPage1.Width := 842;
        PRPage1.Height := 596;
        PRJpegImage1.Width := 842;
        PRJpegImage1.Height := 596;
        PRLayoutPanel1.Width := 842;
        PRLayoutPanel1.Height := 596;
        GenereDocPaysage(texte, 1);
    end;

    // On d�truit le fichier de lock
    //ShowMessage(nomFichierLCK);
    DeleteFile(nomFichierLCK);

    // On n'affiche pas la fen�tre
    if isAffichable = '0' then
        Application.Terminate
    else
        // On affiche avec acrobat
        if isAffichable = '2' then begin
            ShellExecute(Self.Handle, 'Open', PChar(nomFichierPDF), '', '', SW_SHOW);
            Application.Terminate; end
        else
        // On imprime direct
        if isAffichable = '3' then begin
            ShellExecute(Self.Handle, 'Print', PChar(nomFichierPDF), '', '', SW_HIDE);
            Application.Terminate;
        end;

    F_PDFPreview.Caption := 'Aper�u du PDF - ' + nomFichierPDF;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.BT_FermerClick(Sender: TObject);
begin
    Application.Terminate;
end;

//------------------------------------------------------------------------------
procedure TF_PDFPreview.BT_AcrobatClick(Sender: TObject);
begin
    ShellExecute(Self.Handle, 'Open', PChar(nomFichierPDF), '', '', SW_SHOW);
end;

end.
