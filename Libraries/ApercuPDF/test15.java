import java.awt.*;
import java.util.*;
import java.lang.*;
import java.io.*;

/**
 * With this class you can test the iText library.
 *
 * @author  bruno@lowagie.com
 *
 * @since   iText0.30
 */

public class test15
{

// main method

    /**
     * This method generates all kinds of files. 
     *
     * @since   iText0.30
     */

	public static void main(String[] args)
    {
        int i=0, j=0;
        BufferedReader f=null;
        String chaine=null;
        int ligne=0, saut=0, lignecourante=0;
        ArrayList vliste=null;
        int taillepolice=10, debX =5;

        vliste = new ArrayList();
        try
        {
            f = new BufferedReader(new FileReader("testsplf.txt"));
            
            // Lecture du fichier
            chaine = f.readLine();
            while(chaine != null)
            {
                vliste.add(chaine);
                chaine = f.readLine();
            }
            f.close();
        }
        catch (Exception e)
        {
            System.out.println("Erreur lecture fichier " + e);
        }

		// creation of the document with a certain size and certain margins
        FileOutputStream pdf=null;
        try
        {
            pdf = new FileOutputStream(new File("test15.pdf"));
        }
        catch(Exception e)
        {
        }
        // Get the Graphics object for pdf writing
        PDFJob job = new PDFJob(pdf, "Test facture");
        Graphics pdfGraphics = job.getGraphics();
        Dimension d = job.getPageDimension();

    try 
    {
      // chargement image
      Panel drawingArea = new Panel();
      Toolkit toolkit = Toolkit.getDefaultToolkit();
      Image img = toolkit.createImage("fond09.gif");
      MediaTracker tracker = new MediaTracker(drawingArea);
      tracker.addImage(img, 0);
      try
      {
        tracker.waitForID(0);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
      pdfGraphics.drawImage(img, 0, 0, 
                  img.getWidth(drawingArea), 
                  img.getHeight(drawingArea), 
                  Color.green, 
                  null);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
            
        // Ecriture du texte
        pdfGraphics.setFont(new Font("Courier", Font.PLAIN, taillepolice));
           for (i=0; i<vliste.size(); i++)
            {
                chaine = ((String)vliste.get(i)).substring(0, 3);
                if (!chaine.trim().equals(""))
                {
                    ligne = Integer.parseInt(chaine);
                    saut = ligne-lignecourante;
                    lignecourante = ligne;
                }
                else
                {
                    chaine = ((String)vliste.get(i)).substring(3, 4);
                    saut = Integer.parseInt(chaine);
                    lignecourante = lignecourante + saut;
                }
                for (j=1; j<=saut; j++)
                    if (j == saut)
                        pdfGraphics.drawString(((String)vliste.get(i)).substring(4), debX, lignecourante * (taillepolice-1));
            }
        pdfGraphics.dispose();
        job.end();
		System.out.println("Termin� !");
	}
}