package ri.seriem.mail.envoi;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;

public class Securisationfaxnet {

	String fichierkey="";
	String alias="";
	String passwordfichier="";
	String passwordkey="";
	FileWriter fw_fichier=null;
	
	public Securisationfaxnet(String qfichierkey,String qpasswordstorage,String qalias,String qpasswordkey, FileWriter afw_fichier) 
	{
		fichierkey=qfichierkey;
		alias=qalias;
		passwordfichier=qpasswordstorage;
		passwordkey=qpasswordkey;

		fw_fichier = afw_fichier;
	}

	protected String secure(byte[] message) //throws Exception
	{
		FileInputStream fin=null;
		String retour=null;
		try
		{
			//if( DEBUG ) trace(message);

			fin=new FileInputStream(fichierkey);
			java.security.KeyStore ks =KeyStore.getInstance("JKS");

			ks.load(fin, passwordfichier.toCharArray());
			PrivateKey pk = (PrivateKey)ks.getKey(alias, passwordkey.toCharArray());
			Certificate c=ks.getCertificate(alias);
			Signature signature = Signature.getInstance("SHA1withRSA");
			//Signature signature = Signature.getInstance("SHA-1");
			signature.initSign(pk);
//writeLog("secure, message|"+new String(message)+"|");
			signature.update(message);
			byte[] sigBytes = signature.sign();
			byte[] ba= org.apache.commons.codec.binary.Base64.encodeBase64(sigBytes);
			
			retour= new String(ba);
		}
		catch(Exception et)
		{
			//throw et;
			et.printStackTrace();
			retour = null;
		}
		finally
		{
			if(fin != null)
			{
				try
				{
					fin.close();
				}
				catch(Exception et)
				{
					
				}
			}
		}
		
		return retour;
	}

	private void trace(byte[] data) throws NoSuchAlgorithmException
	{
		MessageDigest hash = MessageDigest.getInstance("SHA-1");
		hash.update(data);

		byte[] b= org.apache.commons.codec.binary.Base64.encodeBase64(hash.digest());
		String s=new String(b);
		writeLog("Hash envoi:" + s + " longueur data:" + data.length);

	}
	
	 private void writeLog(String text) {
		 try { fw_fichier.write(text +
				 System.getProperty("line.separator")); fw_fichier.flush();
		} catch(IOException e) {
			e.printStackTrace(); }
		}

}
