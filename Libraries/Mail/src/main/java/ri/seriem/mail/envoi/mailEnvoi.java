package ri.seriem.mail.envoi;

// javac -classpath -O /tmp/.:/tmp/classes.zip:/tmp/activation.jar:/tmp/mail.jar envoifichier.java 
//=============================================================================
// Utilis� par S�rie N et stock� dans /???/lib/
// Le fichier faxnet.jks doit se trouver au niveau du jar lors de l'execution
//==> Permet d'envoyer des mails avec des fichiers en jointures
//==> Param�tres:
//==>     - security
//==>     - destinataire
//==>     - emetteur
//==>     - objet
//==>     - message1 (256)
//==>     - message2 (256)
//==>     - message3 (256)	
//==>     - message4 (207)	
//==>     - priorit�
//==>     - accus� de r�ception
//==>     - serveur SMTP
//==>     - login (null sinon)
//==>     - mot de passe (null sinon)
//==>     - adresse IP de l'emetteur du message
//==>     - dossier temporaire
//==>     - 0 ou nbr de fichiers � envoyer
//==>     - fichier 1
//==>     ... 
//=============================================================================

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class mailEnvoi {
	// Constantes
	final static int portDefaut = 25;
	final static boolean DEBUG = true;

	// Variables
	private static int port;
	private Session session = null;
	private String messageErreur = null;
	private String signature = null;
	private String destinataire = null;
	private String from = null;
	private String subject = null;
	private String msgText1 = null;
	private String priorite = null;
	private String accusereception = null;
	private String host = null;
	private String login = null;
	private String mdp = null;
	private String adresseip = null;
	private String dossier = null;
	private int nbrfic;
	private String[] listefichier = null;
	private String[] listeCC = null;
	private String fichierTexte = null;
	private String detruireFichiers = null;
	private boolean isValide = true;

	private FileWriter fw_fichier = null;

	public mailEnvoi(String[] parametres) {
		if (DEBUG) {
			try {
				fw_fichier = new FileWriter("/sgm/logs/envoimail.txt", true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// message d'erreur en cas de manque de param�tres
		if (parametres.length < 15) {
			envoimessage("Il manque des param�tres pour que le mail puisse �tre envoy� � <" + parametres[0] + ">",
					parametres[10]);
			System.out.println("Il manque des param�tres.");
			// writeLog("Il manque des param�tres.");
			System.exit(1);
		}
		// System.out.println("Veuillez patienter, pr�paration et envoi du mail en cours
		// ...");

		// initialisation des param�tres
		destinataire = parametres[0];
		from = parametres[1];
		subject = parametres[2];
		msgText1 = parametres[3] + "\n" + parametres[4] + "\n" + parametres[5] + "\n" + parametres[6];
		priorite = parametres[7];
		accusereception = parametres[8];
		host = parametres[9];
		login = parametres[10];
		mdp = parametres[11];
		adresseip = parametres[12];
		dossier = parametres[13];
		listeCC = retournerLesCC(parametres[22]);
		if (fw_fichier != null)
			fichierTexte = retournerTexteFichier(parametres[23]); // <-- C'est le
																	// contenu du
																	// message �
																	// envoyer qui
																	// se trouve
																	// dans un
																	// fichier texte
																	// (!!!!!)
		messageErreur = "Le mail pour <" + destinataire + "> a bien �t� transmis au serveur de mail";
		if (!parametres[24].trim().equals(""))
			port = Integer.parseInt(parametres[24]);
		else
			port = portDefaut;
		// mise en place des pi�ces jointes
		if (nbrfic > 0) {
			listefichier = new String[nbrfic];
			for (int i = 0; i < nbrfic; i++)
				listefichier[i] = parametres[15 + i];
		}

		detruireFichiers = parametres[25].trim();

		if (parametres.length > 26) {
			signature = parametres[26].trim();
		}
		if (DEBUG)
			writeLog("Signature = " + signature + " longueur du tableau: " + parametres.length);
		// System.out.println("Signature = " + signature + " longueur du tableau: " +
		// parametres.length);

		// * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

		// g�rer les pi�ces jointes
		gererPiecesJointes(parametres);

		// g�rer les propri�t�s par d�faut
		gererProprietes();

		// cr�er le message
		creerLeMessage();

		// d�truit les fichiers et dossiers
		detruireFichiers();

		// Message de fin
		// System.out.println("Appuyez sur ENTREE pour continuer.");
		if (isValide)
			envoimessage(messageErreur, adresseip);
		else
			System.out.println("ENVOI MAIL INVALIDE : " + messageErreur + " - " + msgText1);
	}

	// ------------------------------------------------------------------------
	// --> Envoi un message � la Message RI
	// ------------------------------------------------------------------------
	public void envoimessage(String mes, String adrip) {
		Socket client;
		BufferedWriter toServer;

		Date dt = new Date();
		String date = DateFormat.getDateInstance(DateFormat.SHORT).format(dt) + " � "
				+ DateFormat.getTimeInstance(DateFormat.MEDIUM).format(dt);
		System.out.println("Envoi de message ");
		try {
			client = new Socket(adrip.trim(), port);
			toServer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
			toServer.write(date);
			toServer.flush();
			toServer.write(mes);
			toServer.flush();
			client.close();
			isValide = true;
		} catch (IOException ioe) {
			System.out.println("[mailEnvoi] PB envoimessage(): " + ioe.getMessage());
			isValide = false;
		}
	}

	// g�rer les pi�ces jointes
	private void gererPiecesJointes(String[] parametres) {
		nbrfic = Integer.parseInt(parametres[14]);

		// V�rifie si il y a des jointures
		if (nbrfic != 0) {
			listefichier = new String[nbrfic];
			// On v�rifie une derni�re fois le nombre de param�tres
			if (parametres.length < (15 + nbrfic)) {
				envoimessage("Il manque des param�tres pour que le mail puisse �tre envoy� � <" + destinataire + ">",
						adresseip);
				System.out.println("Il manque des param�tres.");
				System.exit(1);
			}
			// On charge le tableau avecla listes des fichiers
			for (int i = 0; i < nbrfic; i++)
				listefichier[i] = parametres[15 + i];
		}
	}

	// g�rer les propri�t�s par d�faut
	private void gererProprietes() {
		if (host != null) {
			try {
				// create some properties and get the default Session
				Properties props = System.getProperties();
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.auth", "true");
				session = Session.getDefaultInstance(props);
				isValide = true;
			} catch (Exception e) {
				System.out.println("[mailEnvoi] PB gererProprietes(): " + e.getMessage());
				isValide = false;
			}
		}
	}

	private void creerLeMessage() {

		try {
			// Cr�e un message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			InternetAddress[] addresses = { new InternetAddress(destinataire) };
			// copies cach�es
			if (listeCC != null) {
				InternetAddress[] addrCC = new InternetAddress[listeCC.length];
				for (int i = 0; i < listeCC.length; i++) {
					addrCC[i] = new InternetAddress(listeCC[i]);
					System.out.println(listeCC[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, addrCC);
			}
			msg.setRecipients(Message.RecipientType.TO, addresses);

			msg.setSubject(subject);
			msg.setHeader("X-Priority", priorite);
			if (!accusereception.trim().equals(""))
				msg.setHeader("Disposition-Notification-To", accusereception.trim());

			// create the Multipart and its parts to it
			Multipart mp = new MimeMultipart("mixed");

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();

			// Rajout de David 04/09/15 pour passer le fichier texte en param�tre au cas o�
			// le contenu d�passe les 4 lignes de S�rie M
			if (fichierTexte != null && !fichierTexte.equals(""))
				msgText1 = fichierTexte;

			// Ajout de la signature pour les SMS (proc�dure GMI)
			if (addSignature()) {
				// TODO A corriger car le chemin /sgm/lib est en dur
				if (fw_fichier != null) {
					Securisationfaxnet secur = new Securisationfaxnet("/sgm/lib/faxnet.jks", "faxnet", "faxnet",
							"faxnet", fw_fichier);
					if ((msgText1 == null) || msgText1.trim().equals(""))
						msgText1 = "SMS";
					byte[] txt = msgText1.getBytes();
					String sig = secur.secure(txt);
					if (DEBUG) {
						writeLog("sig = " + sig);
						writeLog("msgText1|" + new String(msgText1) + "|");
					}
					msg.addHeader("faxnetkey", sig);
				}
			}

			mbp1.setText(msgText1);

			mp.addBodyPart(mbp1);

			// Attache les fichiers au mail
			if (listefichier != null) {
				for (int i = 0; i < listefichier.length; i++) {
					MimeBodyPart mbp = new MimeBodyPart();
					File file = new File(listefichier[i]);
					if (file.isFile()) {
						try {
							// System.out.println("C'est un fichier qui m�rite le d�tour");
							mbp.attachFile(file);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							isValide = false;
						}
					} else {
						System.out.println("Ce n'est pas un fichier");
					}
					// FileDataSource fds = new FileDataSource(listefichier[i]);
					// //A PARTIR DE LA JRE6
					// mbp.setDataHandler(new DataHandler(fds));
					// mbp.setFileName(fds.getName());
					mp.addBodyPart(mbp);
				}
			}

			// add the Multipart to the message
			msg.setContent(mp);

			// Initialise la date
			msg.setSentDate(new Date());

			// envoi le mail
			// Transport.send(msg);
			Transport tr = session.getTransport("smtp");
			tr.connect(host, login, mdp);
			msg.saveChanges();
			tr.sendMessage(msg, msg.getAllRecipients());
			tr.close();
			isValide = true;
		} catch (MessagingException mex) {
			messageErreur = "Erreur lors de la pr�paration ou de l'envoi du mail pour <" + destinataire + ">";
			mex.printStackTrace();
			System.out.println("[mailEnvoi] PB creerLeMessage() MEX: " + mex.getMessage());
			Exception ex = null;
			if ((ex = mex.getNextException()) != null) {
				System.out.println("[mailEnvoi] PB creerLeMessage() EX: " + ex.getMessage());
			}
			isValide = false;
		}
	}

	private void detruireFichiers() {
		if (listefichier == null || !detruireFichiers.equals("1"))
			return;
		File jointure = null;
		try {
			for (int i = 0; i < listefichier.length; i++) {
				jointure = new File(listefichier[i]);
				if (jointure.exists() && !jointure.getName().toLowerCase().endsWith("exe")
						&& !jointure.getName().toLowerCase().endsWith("zip")
						&& !jointure.getName().toLowerCase().endsWith("doc"))
					jointure.delete();
			}
			// et le dossier temporaire
			System.out.println("Destruction de " + dossier);
			jointure = new File(dossier);
			jointure.delete();
		} catch (Exception e) {
			messageErreur = "Erreur lors de la destruction des pi�ces jointes <" + destinataire + ">";
			e.printStackTrace();
		}
	}

	private String[] retournerLesCC(String cc) {
		String[] tab = null;
		if (cc == null)
			return null;
		else {
			if (cc.trim().equals(""))
				return null;
			else {
				tab = cc.split(";");
				return tab;
			}
		}
	}

	/**
	 * Retourne le contenu d'un fichier texte
	 * 
	 * @param fichier
	 * @return
	 */
	private String retournerTexteFichier(String fichier) {
		String texte = null;

		if (fichier == null)
			return null;
		else {
			if (fichier.trim().equals(""))
				return null;
			else {
				LireFichier extra = new LireFichier(fichier);
				texte = extra.retournerChaine();
				return texte;
			}
		}
	}

	/**
	 * On confirme ou pas l'ajout d'une signature
	 * 
	 * @return
	 */
	private boolean addSignature() {
		if ((signature == null) || (signature.equals("")) || (signature.equals("0")))
			return false;
		return true;
	}

	private void writeLog(String text) {
		if (fw_fichier == null)
			return;
		try {
			fw_fichier.write(text + System.getProperty("line.separator"));
			fw_fichier.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------------------------------
	// --> Programme principal
	// ------------------------------------------------------------------------
	public static void main(String[] args) {
		// String[] args2 =
		// {"d.biason@wanadoo.fr","d.biason@resolution-informatique.com","Situation de
		// compte","blabla1","blabla2","blabla3","blabla4","3","","smtp.resolution-informatique.com","assistance","gar1972","","","0","src/envoi/fichiertexte.txt","src/envoi/tests.pdf","","","","","","","","587"};

		/*
		 * String[] args2 = {"d.biason@wanadoo.fr",
		 * "s.veneri@resolution-informatique.com", "Situation de compte", "blabla1",
		 * "blabla2", "blabla3", "blabla4", "3", "", "smtp.resolution-informatique.com",
		 * "s.veneri", "gar1972", "", "", "0", "src/envoi/fichiertexte.txt",
		 * "src/envoi/tests.pdf", "", "", "", "", "", "", "src/envoi/fichiertexte.txt",
		 * "587", "0", "0"};
		 */

		new mailEnvoi(args);
	}
}