package ri.seriem.mail.envoi;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class LireFichier {
	String chaine = null;

	public LireFichier(String fichier) {
		if (fichier == null)
			return;

		File ffichier = new File(fichier);
		byte[] buffer = new byte[(int) ffichier.length()];
		chaine = "";

		// lecture du fichier texte
		try {
			InputStream ips = new FileInputStream(ffichier);
			int n = ips.read(buffer);
			chaine = new String(buffer);
			ips.close();
		} catch (Exception e) {
			chaine = null;
		}
	}

	/*
	 * // Lecture du fichier (fonctionne mais moins bo) public LireFichier(String
	 * fichier) {
	 * 
	 * if(fichier==null) return;
	 * 
	 * //lecture du fichier texte try { chaine = ""; InputStream ips=new
	 * FileInputStream(fichier); InputStreamReader ipsr=new InputStreamReader(ips);
	 * BufferedReader br=new BufferedReader(ipsr); String ligne; while
	 * ((ligne=br.readLine())!=null) { //System.out.println(ligne);
	 * chaine+=ligne+"\r\n"; // <- le \r est obligatoire car le serveur de SMS est
	 * sous Windows et donc lors du recalcul de la clef est fau si on l'oubli }
	 * br.close(); } catch (Exception e) { chaine = null;
	 * System.out.println(e.toString()); } }
	 */

	public String retournerChaine() {
		return chaine;
	}

}
