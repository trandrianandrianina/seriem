object F_Etiquette: TF_Etiquette
  Left = 0
  Top = 201
  BorderStyle = bsDialog
  Caption = 'Etiquettes'
  ClientHeight = 456
  ClientWidth = 205
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 6
    Top = 344
    Width = 193
    Height = 105
    Caption = ' Position sur la premi�re page '
    TabOrder = 0
    object CKB_11: TCheckBox
      Left = 64
      Top = 24
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CKB_12: TCheckBox
      Left = 80
      Top = 24
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object CKB_21: TCheckBox
      Left = 64
      Top = 40
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object CKB_22: TCheckBox
      Left = 80
      Top = 40
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object CKB_31: TCheckBox
      Left = 64
      Top = 56
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object CKB_32: TCheckBox
      Left = 80
      Top = 56
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object CKB_41: TCheckBox
      Left = 64
      Top = 72
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
    object CKB_42: TCheckBox
      Left = 80
      Top = 72
      Width = 17
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
  end
  object BT_Generer: TButton
    Left = 115
    Top = 376
    Width = 75
    Height = 25
    Caption = 'G�n�rer'
    TabOrder = 1
    OnClick = BT_GenererClick
  end
  object BT_Annuler: TButton
    Left = 115
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Annuler'
    TabOrder = 2
    OnClick = BT_AnnulerClick
  end
  object GroupBox2: TGroupBox
    Left = 6
    Top = 4
    Width = 193
    Height = 337
    Caption = ' Liste des adresses '
    TabOrder = 3
    object LB_ListeAdresse: TListBox
      Left = 8
      Top = 16
      Width = 176
      Height = 313
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnKeyDown = LB_ListeAdresseKeyDown
    end
  end
  object DDESC_Etiquette: TDdeServerConv
    Left = 16
    Top = 24
  end
  object DDESI_Adresse: TDdeServerItem
    ServerConv = DDESC_Etiquette
    OnChange = DDESI_AdresseChange
    Left = 16
    Top = 56
  end
end
