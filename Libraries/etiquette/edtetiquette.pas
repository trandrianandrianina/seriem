unit edtetiquette;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Shlobj, DdeMan;

type
  TAdresse = record
    numclient: integer;
    nbrligne: integer;
    DetailAdresse : array[1..6] of string;
  end;


  TF_Etiquette = class(TForm)
    GroupBox1: TGroupBox;
    CKB_11: TCheckBox;
    CKB_12: TCheckBox;
    CKB_21: TCheckBox;
    CKB_22: TCheckBox;
    CKB_31: TCheckBox;
    CKB_32: TCheckBox;
    CKB_41: TCheckBox;
    CKB_42: TCheckBox;
    BT_Generer: TButton;
    BT_Annuler: TButton;
    DDESC_Etiquette: TDdeServerConv;
    DDESI_Adresse: TDdeServerItem;
    GroupBox2: TGroupBox;
    LB_ListeAdresse: TListBox;
    procedure BT_AnnulerClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DDESI_AdresseChange(Sender: TObject);
    procedure BT_GenererClick(Sender: TObject);
    procedure LB_ListeAdresseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { D�clarations priv�es }
    Chemin_Bureau : string;
    nbradr : integer;
    Adresse : array[1..96] of TAdresse; // dans l'absolue 12 pages d'etiquettes
    ListeIndice : array[1..96] of byte;
    Etiquette : array[1..48, 1..2] of byte;
    function GetSpecialFolder(Folder: integer): string;
  public
    { D�clarations publiques }
  end;

var
  F_Etiquette: TF_Etiquette;

implementation

{$R *.DFM}

//------------------------------------------------------------------------------
// Permet de r�cup�rer le chemin vers les dossiers Windows
//
// CSIDL_BITBUCKET : ???
// CSIDL_CONTROLS : ???
// CSIDL_DESKTOP : R�pertoire Bureau (pour ajouter des icones sur le bureau par exemple)
// CSIDL_DESKTOPDIRECTORY : idem
// CSIDL_DRIVES : ???
// CSIDL_FONTS : R�pertoire Fonts (pour r�cup�rer les polices de caract�res)
// CSIDL_NETHOOD : R�pertoire Voisinage R�seau
// CSIDL_NETWORK : ???
// CSIDL_PERSONAL : R�pertoire personnel
// CSIDL_PRINTERS : ???
// CSIDL_PROGRAMS : R�pertoire Menu D�marrer / Programmes
// CSIDL_RECENT : R�pertoire Fichiers R�cents
// CSIDL_SENDTO : R�pertoire Send to / Envoyer vers
// CSIDL_STARTMENU : R�pertoire Menu D�marrer
// CSIDL_STARTUP : R�pertoire Menu D�marrer / Programme / D�marrage
// CSIDL_TEMPLATES : R�pertoire Templates
//------------------------------------------------------------------------------
function TF_Etiquette.GetSpecialFolder(Folder: integer): string;
var
    p  : pitemidlist;
    pc : pchar;
begin
    SHGetSpecialFolderLocation (Self.Handle, Folder, p);
    GetMem(pc, MAX_PATH);
    if SHGetPathFromIDList (p, pc) then
        Result := String(pc)
    else
        Result := 'Error';
    FreeMem(pc, MAX_PATH);
end;

//------------------------------------------------------------------------------
//  On clique sur le bouton annuler
//------------------------------------------------------------------------------
procedure TF_Etiquette.BT_AnnulerClick(Sender: TObject);
begin
    Application.Terminate;
end;

//------------------------------------------------------------------------------
//  Op�ration lors du lancement du programme
//------------------------------------------------------------------------------
procedure TF_Etiquette.FormCreate(Sender: TObject);
begin
    nbradr := 0;
    // R�cup�ration des chemins du bureau
    try
        Chemin_Bureau := GetSpecialFolder(CSIDL_DESKTOPDIRECTORY);
    except
       on E: Exception do Chemin_Bureau := 'c:\windows\bureau';
    end;
end;

//------------------------------------------------------------------------------
//   Reception d'un message via lien DDE
//------------------------------------------------------------------------------
procedure TF_Etiquette.DDESI_AdresseChange(Sender: TObject);
var
    fin, lng, i, j, k : integer;
    chaine, chaine2 : string;
begin
     j := 0;
     k := 0;
     Inc(nbradr);
     chaine := DDESI_Adresse.Text;
     fin := Pos('|', chaine);
     while fin <> 0 do begin
         lng := Length(chaine);
         Inc(k);
         chaine2 := Copy(chaine, 1, fin-1);
         case k of
             // R�cup�ration du num�ro client
             1: Adresse[nbradr].numclient := StrToInt(chaine2)
         else
             Inc(j);
             Adresse[nbradr].DetailAdresse[j] := Copy(chaine, 1, fin-1);
             // Ajout dans la liste
             if j = 1 then
                 LB_ListeAdresse.Items.Add(Adresse[nbradr].DetailAdresse[1]);
         end;
         chaine := Copy(chaine, fin+1, lng-fin);
         fin := Pos('|', chaine);
     end;
     Adresse[nbradr].nbrligne := j;
end;

//------------------------------------------------------------------------------
//  On clique sur le bouton g�n�rer
//------------------------------------------------------------------------------
procedure TF_Etiquette.BT_GenererClick(Sender: TObject);
var
    i, j, k, l : integer;
    F: TextFile;
    Arret : Boolean;
    chaine : string;
    hauteur : integer;
begin
    // On v�rifie que le fichier existe dans ce cas on le d�truit
    if FileExists(Chemin_Bureau + '\Etiquettes.rtf') then
        DeleteFile(Chemin_Bureau + '\Etiquettes.rtf');

    // Nettoyage de la table des adresses (on enl�ve les adresses supprim�es)
    j := 0;
    for i:= 1 to nbradr do
        if Adresse[i].numclient <> -1 then begin
            Inc(j);
            ListeIndice[j] := i;
        end;
    nbradr := j;
    for i := 1 to nbradr do begin
        Adresse[i].numclient := Adresse[ListeIndice[i]].numclient;
        Adresse[i].nbrligne := Adresse[ListeIndice[i]].nbrligne;
        for j := 1 to Adresse[i].nbrligne do
            Adresse[i].DetailAdresse[j] := Adresse[ListeIndice[i]].DetailAdresse[j];
    end;

    // Initialisation du tableau Etiquette
    // Analyse pour la premi�re page
    k := 1;
    if k <= nbradr then
        if CKB_11.Checked then begin
            Etiquette[1,1] := k;
            Inc(k); end
        else Etiquette[1,1] := 0
    else Etiquette[1,1] := 0;
    if k <= nbradr then
        if CKB_12.Checked then begin
            Etiquette[1,2] := k;
            Inc(k); end
        else Etiquette[1,2] := 0
    else  Etiquette[1,2] := 0;
    if k <= nbradr then
        if CKB_21.Checked then begin
            Etiquette[2,1] := k;
            Inc(k); end
        else Etiquette[2,1] := 0
    else Etiquette[2,1] := 0;
    if k <= nbradr then
        if CKB_22.Checked then begin
            Etiquette[2,2] := k;
            Inc(k); end
        else Etiquette[2,2] := 0
    else Etiquette[2,2] := 0;
    if k <= nbradr then
        if CKB_31.Checked then begin
            Etiquette[3,1] := k;
            Inc(k); end
        else Etiquette[3,1] := 0
    else Etiquette[3,1] := 0;
    if k <= nbradr then
        if CKB_32.Checked then begin
            Etiquette[3,2] := k;
            Inc(k); end
        else Etiquette[3,2] := 0
    else Etiquette[3,2] := 0;
    if k <= nbradr then
        if CKB_41.Checked then begin
            Etiquette[4,1] := k;
            Inc(k); end
        else Etiquette[4,1] := 0
    else Etiquette[4,1] := 0;
    if k <= nbradr then
        if CKB_42.Checked then begin
            Etiquette[4,2] := k;
            Inc(k); end
        else Etiquette[4,2] := 0
    else Etiquette[4,2] := 0;
    // Analyse pour les autres pages
    for i := 5 to 48 do
        for j := 1 to 2 do begin
            if k <= nbradr then begin
                Etiquette[i,j] := k;
                Inc(k); end
            else
                Etiquette[i,j] := 0;
        end;

    // Ecriture de l'ent�te du fichier
    AssignFile(F, Chemin_Bureau + '\Etiquettes.rtf');
    ReWrite(F);
    WriteLn(F,'{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1036\deflangfe1036');
    WriteLn(F,'{\fonttbl');
    WriteLn(F,'{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}');
    WriteLn(F,'{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}');
    WriteLn(F,'{\f57\froman\fcharset238\fprq2 Times New Roman CE;}');
    WriteLn(F,'{\f58\froman\fcharset204\fprq2 Times New Roman Cyr;}');
    WriteLn(F,'{\f60\froman\fcharset161\fprq2 Times New Roman Greek;}');
    WriteLn(F,'{\f61\froman\fcharset162\fprq2 Times New Roman Tur;}');
    WriteLn(F,'{\f62\froman\fcharset177\fprq2 Times New Roman (Hebrew);}');
    WriteLn(F,'{\f63\froman\fcharset178\fprq2 Times New Roman (Arabic);}');
    WriteLn(F,'{\f64\froman\fcharset186\fprq2 Times New Roman Baltic;}');
    WriteLn(F,'{\f65\fswiss\fcharset238\fprq2 Arial CE;}');
    WriteLn(F,'{\f66\fswiss\fcharset204\fprq2 Arial Cyr;}');
    WriteLn(F,'{\f68\fswiss\fcharset161\fprq2 Arial Greek;}');
    WriteLn(F,'{\f69\fswiss\fcharset162\fprq2 Arial Tur;}');
    WriteLn(F,'{\f70\fswiss\fcharset177\fprq2 Arial (Hebrew);}');
    WriteLn(F,'{\f71\fswiss\fcharset178\fprq2 Arial (Arabic);}');
    WriteLn(F,'{\f72\fswiss\fcharset186\fprq2 Arial Baltic;}}');
    WriteLn(F,'');
    WriteLn(F,'\paperw11906\paperh16838');
    WriteLn(F,'\margl284\margr284\margt300\margb0');
    WriteLn(F,'\deftab708\widowctrl\ftnbj\aenddoc\hyphhotz425\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc');
    WriteLn(F,'\dgmargin\dghspace180\dgvspace180\dghorigin284\dgvorigin340\dghshow1\dgvshow1\jexpand\viewkind1\viewscale75');
    WriteLn(F,'\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd');
    WriteLn(F,'\linex0\headery709\footery709\colsx708\endnhere\sectlinegrid360\sectdefaultcl');

    // Ecriture du corps
    Arret := False;
    for i := 1 to 48 do
      if Arret <> True then
        for j := 1 to 2 do begin
            // Est on en bas de page ??
            hauteur := i mod 4;

            // Calcul du positionnement de l'adresse dans la cellule (ligne)
            l := ((12 - Adresse[Etiquette[i,j]].nbrligne) div 2) - 1;
            chaine := '{ ';
            while l > 0 do begin
                chaine := chaine + '\par ';
                Dec(l);
            end;
            chaine := chaine + '}';

            // Ecriture des infos dans le fichier
            WriteLn(F,'\trowd');
            if hauteur = 0 then
                WriteLn(F,'\trgaph70\trrh3570\trleft-70\trftsWidth1\trautofit1\trpaddl70\trpaddr70\trpaddfl3\trpaddfr3')
            else
                WriteLn(F,'\trgaph70\trrh4133\trleft-70\trftsWidth1\trautofit1\trpaddl70\trpaddr70\trpaddfl3\trpaddfr3');
            WriteLn(F,'\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth5739 \cellx5669');
            WriteLn(F,'\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth5739 \cellx11408');
            WriteLn(F,'');
            WriteLn(F,'\pard\plain \qc \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \fs24\lang1036\langfe1036\cgrid\langnp1036\langfenp1036 '); // {\fs28' + chaine + '}');
            WriteLn(F,'\pard\plain \s2 \ql \li0\ri0\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel1\adjustright\rin0\lin0 \b\f1\fs28\lang1036\langfe1036\cgrid\langnp1036\langfenp1036');
            if Etiquette[i,j] <> 0 then begin
                WriteLn(F,'{\f1\fs16\ul EXPEDITEUR\~: \par }');
                WriteLn(F,'{\f1\fs16 RESOLUTION INFORMATIQUE \par 1 rue Paul Mespl� - M�tropark B\par 31100 Toulouse}');
                WriteLn(F,'{\i0  \par }');
                WriteLn(F,'{\i0\fs20\lang1024\langfe1024\noproof');
                WriteLn(F,'{\shp{\*\shpinst\shpleft0\shptop-20\shpright5580\shpbottom-20\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz0\shplid1026');
                WriteLn(F,'{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}');
                WriteLn(F,'{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8192\dpline\dpptx0\dppty0\dpptx5580\dppty0\dpx0\dpy-20\dpxsize5580\dpysize0\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}}');
                WriteLn(F,chaine);
                for k := 1 to Adresse[Etiquette[i,j]].nbrligne-1 do
                    WriteLn(F,'{'+ Adresse[Etiquette[i,j]].DetailAdresse[k] + ' \par }');
                WriteLn(F,'{'+ Adresse[Etiquette[i,j]].DetailAdresse[Adresse[Etiquette[i,j]].nbrligne] + ' \cell }');
                Dec(nbradr); end
            else
                WriteLn(F,'{  \cell }');
            WriteLn(F,'');
            if j = 2 then begin
                WriteLn(F,'{');
                if hauteur = 0 then
                    WriteLn(F,'\trowd \trgaph70\trrh3570\trleft-70\trftsWidth1\trautofit1\trpaddl70\trpaddr70\trpaddfl3\trpaddfr3')
                else
                    WriteLn(F,'\trowd \trgaph70\trrh4133\trleft-70\trftsWidth1\trautofit1\trpaddl70\trpaddr70\trpaddfl3\trpaddfr3');
                WriteLn(F,'\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth5739 \cellx5669');
                WriteLn(F,'\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth5739 \cellx11408');
                WriteLn(F,'\row');
                WriteLn(F,'}');
                if nbradr <= 0 then Arret := True;
            end;
        end;

    // Ecriture du pied
    WriteLn(F,'}');
    CloseFile(F);
    ShowMessage('G�n�ration du fichier termin�e.'+#13+#10+'Le fichier ETIQUETTES.RTF se trouve sur votre bureau.');
    Application.Terminate;
end;

//------------------------------------------------------------------------------
// On souhaite supprimer l'adresse s�lectionn�e dans la liste
//------------------------------------------------------------------------------
procedure TF_Etiquette.LB_ListeAdresseKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    i, j : integer;
    chaine : string;
    trouver : boolean;
begin
    if Key <> VK_DELETE then Exit;

    // Recherche de l'item s�lectionn�
    chaine := '';
    for i := 0 to (LB_ListeAdresse.Items.Count - 1) do
        if LB_ListeAdresse.Selected[i] then
            chaine := LB_ListeAdresse.Items.Strings[i];

    // Aucun item s�lectionn� alors on sort
    if chaine = '' then Exit;

    // Recherche de cet item dans la liste des adresses
    trouver := False;
    for i:= 1 to nbradr do
      if not trouver then
        if Adresse[i].DetailAdresse[1] = chaine then begin
            Adresse[i].numclient := -1;
            trouver := True;
            // Maj de la liste
            LB_ListeAdresse.Clear;
            LB_ListeAdresse.ItemIndex := 0;
            for j:= 1 to nbradr do
                if Adresse[j].numclient <> -1 then
                LB_ListeAdresse.Items.Add(Adresse[j].DetailAdresse[1]);
        end;
end;

end.
