program etiquette;

uses
  Windows,
  Forms,
  edtetiquette in 'edtetiquette.pas' {F_Etiquette};

{$R *.RES}

begin
  // Evite le lancement de plusieurs instance de ce programme
  SetLastError(NO_ERROR);
  CreateMutex (nil, False, 'Etiquette');
  if GetLastError = ERROR_ALREADY_EXISTS then
      Exit;

  Application.Initialize;
  Application.CreateForm(TF_Etiquette, F_Etiquette);
  Application.Run;
end.
