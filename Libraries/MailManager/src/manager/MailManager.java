package manager;

import java.sql.Connection;
import java.util.ArrayList;

import database.record.GenericRecord;
import system.SystemManager;

public class MailManager extends Thread {

	protected static AccesDB2 accesDb2 = null;
	public static String NOM_COMMERCIAL_SERVEUR = null;
	public static int FREQUENCE_SCAN_BIBS = 20000;
	public static boolean ISDEBUG = false;
	public static String URL_PHOTO_MAILS = null;
	public static boolean ISMODETEST = false;
	public static String ADRESSE_TEST = null;
	public static String LETTREENV = null;

	private String adresseAS400 = null;
	private String profilAS = null;
	private String mpProfilAS = null;

	private ArrayList<String> listeBiblis = null;
	private ArrayList<BibliMailManager> listeManagers = null;

	public MailManager(String pAdresseAS, String pLettre, String pProfil, String pMotPasse, String pNomServeur,
			String pFrequence, boolean pDebug, String pUrlMails, boolean pIsTest, String pAdresseTest) {
		adresseAS400 = pAdresseAS;
		LETTREENV = pLettre;
		profilAS = pProfil;
		mpProfilAS = pMotPasse;
		NOM_COMMERCIAL_SERVEUR = pNomServeur;
		try {
			FREQUENCE_SCAN_BIBS = Integer.parseInt(pFrequence);
		} catch (Exception e) {

		}
		ISDEBUG = pDebug;
		URL_PHOTO_MAILS = pUrlMails;
		ISMODETEST = pIsTest;
		ADRESSE_TEST = pAdresseTest;
		setName("MailManager env:" + LETTREENV);
	}

	@Override
	public void run() {
		super.run();
	}

	@Override
	public void start() {
		super.start();
		System.out.println("[MailManager] Le MailManager est d�marr�");
		if (initConnexionDB2()) {
			System.out.println("[MailManager] connexion � DB2 OK");
			listeBiblis = retournerListeBiblisClients();
			if (listeBiblis != null) {
				listeManagers = new ArrayList<BibliMailManager>();
				for (String bib : listeBiblis) {
					BibliMailManager manager = new BibliMailManager(bib);
					if (manager.isActif()) {
						manager.start();
						listeManagers.add(manager);
					} else {
						manager.interrupt();
					}
					try {
						Thread.sleep(20000);
					} catch (InterruptedException ex) {
					}
				}
				if (listeManagers.size() == 0) {
					System.out
							.println("[MailManager] Aucune configuration n'est active. Le MailManager est d�sactiv�.");
					interrupt();
				}
			}
		}
	}

	@Override
	public void interrupt() {
		if (listeManagers != null) {
			for (BibliMailManager manager : listeManagers) {
				manager.interrupt();
			}
		}
		System.out.println("[MailManager] Le MailManager est arr�t�");
		super.interrupt();
	}

	/**
	 * On initialise la connexion � DB2
	 */
	private boolean initConnexionDB2() {
		SystemManager systeme = new SystemManager(adresseAS400, profilAS, mpProfilAS, true);
		if (systeme != null && systeme.getSystem() != null) {
			Connection maconnectionAS = systeme.getdatabase().getConnection();
			accesDb2 = new AccesDB2(maconnectionAS);
		} else {
			System.out.println("[MailManager] Erreur de connexion � DB2 avec " + adresseAS400 + " / " + profilAS);
		}

		return accesDb2 != null;
	}

	/**
	 * Retourner la liste des biblioth�ques clients pr�sentes sur l'AS400
	 */
	private ArrayList<String> retournerListeBiblisClients() {
		ArrayList<String> liste = null;

		if (LETTREENV == null) {
			return null;
		}

		ArrayList<GenericRecord> records = accesDb2.select(
				" SELECT FMBIB " + " FROM QGPL.PSEMFMVM " + " WHERE FMVER = '" + LETTREENV + "' " + " ORDER BY FMBIB ");

		if (records != null) {
			if (records.size() > 0) {
				liste = new ArrayList<String>();
				for (GenericRecord record : records) {
					ArrayList<GenericRecord> recs = accesDb2.select(" SELECT DISTINCT(SYSTEM_TABLE_SCHEMA) AS BIB "
							+ " FROM QSYS2.SYSTABLES " + " WHERE SYSTEM_TABLE_SCHEMA = '"
							+ record.getField("FMBIB").toString().trim() + "' AND TABLE_NAME = '"
							+ BibliMailManager.TABLE_MANAGER_MAIL + "' " + " ORDER BY BIB ");
					if (recs != null) {
						if (recs.size() == 1) {
							liste.add(record.getField("FMBIB").toString().trim());
						} else {
							System.out.println(
									"[MailManager] La biblioth�que " + record.getField("FMBIB").toString().trim()
											+ " ne contient pas la table " + BibliMailManager.TABLE_MANAGER_MAIL);
						}
					} else {
						System.out.println("[MailManager] Erreur de r�cup�ration de la table des Mails "
								+ BibliMailManager.TABLE_MANAGER_MAIL + " pour l'environnement " + LETTREENV);
					}
				}
			} else {
				System.out.println("[MailManager] Aucune Biblioth�que client attribu� � l'environnement " + LETTREENV);
			}
		} else {
			System.out.println("[MailManager] Erreur de r�cup�ration des biblioth�ques client pour l'environnement " + LETTREENV);
		}

		return liste;
	}
}
