
package manager;

import java.sql.Connection;
import database.record.GenericRecordManager;


/**
 * Classe Mod�le permettant d'acc�der aux donn�es de DB2 Serie M et XWEBSHOP
 */
public class AccesDB2 extends GenericRecordManager {

	Connection db2 = null;

	/**
	 * Constructeur de la classe AccesDB2
	 */
	public AccesDB2(Connection database) {
		super(database);
		db2 = database;
		
	}
}
