unit Apropos;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TF_Apropos = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Comments: TLabel;
    OKButton: TButton;
    procedure OKButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Apropos: TF_Apropos;

implementation

uses img;

{$R *.DFM}

procedure TF_Apropos.OKButtonClick(Sender: TObject);
begin
    Close;
end;

end.
 
