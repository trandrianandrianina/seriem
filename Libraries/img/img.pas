unit img;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Jpeg, Controls, Forms, Dialogs,
  Menus, StdCtrls, ExtCtrls, ComCtrls, Printers;

type
  TF_Img = class(TForm)
    MM_Menu: TMainMenu;
    M_Fichier: TMenuItem;
    M_h: TMenuItem;
    M_Aide: TMenuItem;
    M_Apropos: TMenuItem;
    Memo_Aide: TMemo;
    I_img: TImage;
    PrintDialog: TPrintDialog;
    procedure RedimClient;
    procedure RotatClient;
    procedure RotatClient2;
    procedure AppDeactivate(Sender: TObject);
    procedure MenuEvent(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure M_AideClick(Sender: TObject);
    procedure M_AproposClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure M_RotationClick(Sender: TObject);
    //procedure Paint; override;
  private
    { D�clarations priv�es}
  public
    { D�clarations publiques}
  end;

var
  F_Img: TF_Img;
  M_Img: array[0..49] of TMenuItem;
  M_Separateur1, M_Impression, M_Quitter : TMenuItem;
  nbrfich : integer;
  tabfich : array[0..49] of string;
  bib : string;
  actif : integer;

implementation

uses Apropos;

{$R *.DFM}

procedure TF_Img.RedimClient;
begin
    if (Screen.Height-50) <= I_Img.Picture.Height then begin
        ClientHeight := Round(Screen.Height * 0.9);
        VertScrollBar.Range := I_Img.Picture.Height;
        VertScrollBar.Visible := true; end
     else begin
        ClientHeight := I_Img.Picture.Height;
        VertScrollBar.Visible := false;
     end;

     if (Screen.Width-50) <= I_Img.Picture.Width then begin
        ClientWidth := Round(Screen.Width * 0.9);
        HorzScrollBar.Range := I_Img.Picture.Width;
        HorzScrollBar.Visible := true; end
     else begin
        ClientWidth := I_Img.Picture.Width;
        HorzScrollBar.Visible := false;
     end;
end;

procedure TF_Img.RotatClient;
var i, j, k: integer;
    buffer1, buffer2 : TBitmap;
    P1, P2 : PByteArray;
begin
     buffer1 := TBitmap.Create;
     buffer2 := TBitmap.Create;
     buffer1.Height := ClientWidth;
     buffer1.Width := ClientHeight;
     buffer2.Height := ClientHeight;
     buffer2.Width := ClientWidth;
     buffer2.Canvas.CopyMode := cmSrcCopy;
     buffer2.Canvas.Draw(0,0,I_Img.Picture.Graphic);
     P2 := buffer2.Scanline[50];
     P1 := buffer1.Scanline[10];
     for j := 0 to (buffer1.Height-1) do begin
         P1 := buffer1.Scanline[j];
         for i := 0 to (buffer1.Width-1) do
             P1[i] := 10;
     end;
 {    for j := 0 to (buffer2.Height-1) do begin
         P2 := buffer2.Scanline[j];
         k := ClientHeight-1;
         dec(k, j);
         for i := 0 to (buffer1.Height-1) do begin
             P1 := buffer1.Scanline[i];
             P1[k] := P2[i];
         end;
     end;}
     i := ClientWidth;
     ClientWidth := ClientHeight;
     ClientHeight := i;
     F_Img.Canvas.CopyMode := cmSrcCopy;
     F_Img.Canvas.Draw(0,0,buffer1);

     //RedimClient;
     buffer1.free;
     buffer2.free;
end;

procedure TF_Img.RotatClient2;
var i, j, k: integer;
    buffer1, buffer2 : TBitmap;
begin
     buffer1 := TBitmap.Create;
     buffer2 := TBitmap.Create;
     buffer1.Height := ClientWidth;
     buffer1.Width := ClientHeight;
     buffer2.Height := ClientHeight;
     buffer2.Width := ClientWidth;
     buffer2.Canvas.CopyMode := cmSrcCopy;
     buffer2.Canvas.Draw(0,0,I_Img.Picture.Graphic);
     for j := 0 to (ClientHeight-1) do begin
         k := ClientHeight-1;
         dec(k, j);
         for i := 0 to (ClientWidth-1) do
             buffer1.Canvas.Pixels[k, i] := buffer2.Canvas.Pixels[i,j];
     end;
     i := ClientWidth;
     ClientWidth := ClientHeight;
     ClientHeight := i;
     //I_Img.Visible := false;
     F_Img.Canvas.CopyMode := cmSrcCopy;
     F_Img.Canvas.Draw(0,0,buffer1);

     //RedimClient;
     buffer1.free;
     buffer2.free;
end;

procedure TF_Img.AppDeactivate(Sender: TObject);
begin
    if actif <> 0 then Dec(actif);
    if actif = 0 then Close;
end;

procedure TF_Img.MenuEvent(Sender: TObject);
var
    i, j : integer;
begin
  with Sender as TMenuItem do begin
      if Caption = '&Quitter' then
          Close;
{      if Caption = '&Imprimer' then begin
          Inc(actif);
          PrintDialog.Execute;
          with Printer do begin
              BeginDoc;
              if I_Img.Visible = True then
                  Canvas.Draw((PageWidth - I_Img.Picture.Width) div 2,(PageHeight - I_Img.Picture.Height) div 2, I_Img.Picture.Graphic)
              else
                  Canvas.TextOut(0, 0, Memo_Aide.Text);
              EndDoc;
          end;
      end;}
      for i := 0 to nbrfich-1 do
          if Caption = tabfich[i] then begin
                Checked := True;
                I_Img.Picture.LoadFromFile(bib+tabfich[i]);
                F_img.Caption := tabfich[i];
                F_img.RedimClient;
                if I_Img.Visible = False then begin
                    Memo_Aide.Visible := False;
                    I_Img.Visible := True;
                end;
          end;
  end;
end;

procedure TF_Img.FormCreate(Sender: TObject);
var
    i, j : integer;
    nomimg : array[0..7] of char;
    typ : char;
    bibli : array[0..2] of char;
    etb : array[0..2] of char;
    parametre : array[0..16] of char;
    listefich: TSearchRec;

begin
    // Mise en place de l'event Desactivation pour l'application
     Application.OnDeactivate := AppDeactivate;
     actif := 0;

    // R�cup�ration des param�tres
    if ParamCount <> 2 then
        Close;

    StrPCopy(@parametre, ParamStr(1));
    typ := parametre[3];
    for i := 4 to 6 do
        bibli[i-4] := parametre[i];
    if typ <> 'A' then begin
       for i := 7 to 9 do
           etb[i-7] := parametre[i];
       bib := Trim(ParamStr(2))+'\'+string(bibli)+'\'+string(etb)+'\'+string(typ)+'\';
       i := 10;
       j := 0;
       repeat
             nomimg[j] := parametre[i];
             inc(i);
             inc(j);
       until j = 7; end
    else begin
       bib := Trim(ParamStr(2))+'\'+string(bibli)+'\'+string(typ)+'\';
       i := 7;
       j := 0;
       repeat
             nomimg[j] := parametre[i];
             inc(i);
             inc(j);
       until j = 7;
    end;

    // Recherche du ou des fichiers images et s�lection
    nbrfich := 0;
    i := FindFirst(bib+string(nomimg)+'*.jpg', faAnyFile, listefich);
    while i = 0 do begin
        tabfich[nbrfich] := listefich.name;
        Inc(nbrfich);
        i := FindNext(listefich)
    end;
    FindClose(listefich);

    i := FindFirst(bib+string(nomimg)+'*.bmp', faAnyFile, listefich);
    while i = 0 do begin
        tabfich[nbrfich] := listefich.name;
        Inc(nbrfich);
        i := FindNext(listefich)
    end;
    FindClose(listefich);

    // Cr�ation dynamique du menu
    if nbrfich = 1 then
        MM_Menu.destroy
    else begin
        if nbrfich > 1 then begin
            for i := 0 to nbrfich-1 do begin
                M_Img[i] := TMenuItem.Create(Self);
                M_Img[i].Caption := tabfich[i];
                M_Img[i].GroupIndex := 1;
                M_Img[i].RadioItem := True;
                M_Img[i].OnClick := MenuEvent;
                M_Fichier.add(M_Img[i]);
            end;
            Inc(i);
            M_Img[i] := TMenuItem.Create(Self);
            M_Img[i].Caption := '-';
            M_Fichier.add(M_Img[i]);
        end;
        {M_Impression := TMenuItem.Create(Self);
        M_Impression.Caption := '&Imprimer';
        M_Impression.OnClick := MenuEvent;
        M_Fichier.add(M_Impression);

        M_Separateur1 := TMenuItem.Create(Self);
        M_Separateur1.Caption := '-';
        M_Fichier.add(M_Separateur1);}

        M_Quitter := TMenuItem.Create(Self);
        M_Quitter.Caption := '&Quitter';
        M_Quitter.OnClick := MenuEvent;
        M_Fichier.add(M_Quitter);
     end;

    // Affichage image
    if nbrfich = 0 then begin
        Caption := string(nomimg);
        I_Img.Visible := False;
        Memo_Aide.Visible := True; end
    else begin
        try
            I_Img.Picture.LoadFromFile(bib+tabfich[0]);
            F_img.RedimClient;
            Caption := tabfich[0];
        except
        on EInvalidGraphic do begin
            I_Img.Picture.Graphic := nil;
            Caption := 'Erreur'; end;
        end;
    end;

    // Position de la fen�tre
    Left := 315;
    Top := 77;
end;

procedure TF_Img.M_AideClick(Sender: TObject);
begin
    I_Img.Visible := False;
    Memo_Aide.Visible := True;
end;

procedure TF_Img.M_AproposClick(Sender: TObject);
begin
     Inc(actif);
     F_Apropos.ShowModal;
end;

procedure TF_Img.FormActivate(Sender: TObject);
begin
    if nbrfich = 0 then
        MessageDlg('Image non trouv�e, 2 possibilit�s: soit l''image n''existe pas, soit le chemin menant aux photos n''est pas bon', mtInformation, [mbOk], 0);
    if actif = 0 then Inc(actif);
end;


procedure TF_Img.M_RotationClick(Sender: TObject);
begin
     RotatClient;
end;

end.
