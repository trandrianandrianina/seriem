//=================================================================================================
//==>                                                                       24/11/2014 - 24/11/2014
//==> R�cup�re les informations pour la connexion au site de mise � jour
//==> A faire:
//=================================================================================================
package ri.seriem.installweb.installation;

import java.io.File;

import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libas400.system.SystemManager;

public class SiteInformations
{
	// Constantes
	private static final int LONG_DTAARA = 500; 
	private static final String DTA_NAME = "DWN_SERIEM";

	// Variables
	private String login=null;
	private String mdp=null;
	private String adresse=null;
	private String repertoireFTP = null;
	
	private File cheminRacine=null;

	private SystemManager system = null;
	private char letter = 'X';
	private String libExpas = letter + "EXPAS";

	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 

	
	/**
	 * Constructeur
	 * @param asystem
	 * @param aletter
	 */
	public SiteInformations(SystemManager asystem, char aletter, String pProfil, String pMdp, String pAdresse, String pRepertoire)
	{
		system = asystem;
		if( aletter != letter ){
			letter = aletter;
			libExpas = letter + libExpas.substring(1);
		}
		
		login = pProfil;
		mdp = pMdp;
		adresse = pAdresse;
		repertoireFTP = pRepertoire;
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * R�cup�re les informations afin de pouvoir se connecter au site
	 * @return
	 */
	public boolean readInformations()
	{
		QSYSObjectPathName path = new QSYSObjectPathName(libExpas, DTA_NAME, "DTAARA");
		CharacterDataArea dataArea = new CharacterDataArea(system.getSystem(), path.getPath());
		String data = readData(dataArea);
		if( data == null ){
			return false;
		}

		return analyzeData( data );
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Analyse les donn�es de la dataarea
	 * @param data
	 * @return
	 */
	private boolean analyzeData(String data)
	{
		if( data.length() < LONG_DTAARA ){
			msgErreur += "\n[SiteInformations] (analyzeData) Longueur de dataarea inf�rieure � " + LONG_DTAARA + " octets.";
			return false;
		}
		
		login = data.substring( 0, 20 ).trim();
		mdp = data.substring( 20, 40 ).trim();
		adresse = data.substring(40, 79).trim();
		cheminRacine = new File( data.substring( 300, 399 ).trim() ).getParentFile();
		
		return true;
	}
	
	/**
	 * Lit et retourne le contenu de la DTAARA
	 * @return
	 */
	private String readData(CharacterDataArea dataarea)
	{
		String data = null;
		
		try
		{
			data = dataarea.read();
		}
		catch (Exception e)
		{
			msgErreur += "\n[SiteInformations] (readData) Erreur : " + e;
		}
		
		return data;
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le login
	 */
	public String getLogin()
	{
		return login;
	}

	/**
	 * @param login le login � d�finir
	 */
	public void setLogin(String login)
	{
		this.login = login;
	}

	/**
	 * @return le mdp
	 */
	public String getMdp()
	{
		return mdp;
	}

	/**
	 * @param mdp le mdp � d�finir
	 */
	public void setMdp(String mdp)
	{
		this.mdp = mdp;
	}

	/**
	 * @return le adresse
	 */
	public String getAdresse()
	{
		return adresse;
	}

	/**
	 * @param adresse le adresse � d�finir
	 */
	public void setAdresse(String adresse)
	{
		this.adresse = adresse;
	}

	/**
	 * @return le cheminRacine
	 */
	public File getCheminRacine()
	{
		return cheminRacine;
	}

	/**
	 * @param cheminRacine le cheminRacine � d�finir
	 */
	public void setCheminRacine(File cheminRacine)
	{
		this.cheminRacine = cheminRacine;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	public String getRepertoireFTP() {
		return repertoireFTP;
	}

	
}
