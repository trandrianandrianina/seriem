//=================================================================================================
//==>                                                                       26/11/2014 - 05/12/2014
//==> R�cup�re les informations sur les applis Web
//==> A faire:
//=================================================================================================
package ri.seriem.installweb.installation;

import java.util.ArrayList;
import java.util.Arrays;

import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libas400.system.SystemManager;

public class WebInformations
{
	// Constantes
	private static final int LONG_DTAARA = 1024; 
	private static final int OFFSET_APPLIS = 0; 
	private static final int LONG_APPLIS = 200; 
	private static final int OFFSET_TODO = 200; 
	private static final int LONG_TODO = 100; 
	private static final int OFFSET_VERSION_TOMCAT = 998; 
	private static final int LONG_VERSION_TOMCAT = 25; 
	private static final int OFFSET_DIGIT = 1023; 
	private static final String KEY_INSTALLATION = "#INSTALLATION#"; 
	
	// Variables
	private SystemManager system = null;
	private char letter = 'X';
	private String library = null;
	private CharacterDataArea dataArea = null;
	private ArrayList<Application> listapp = new ArrayList<Application>();
	private String versionTomcat = null;
	private char digit = '7';
	private boolean isInstallation = false;
	private boolean isMajTomcat = false;
	private boolean isMajApplis = false;

	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 


	/**
	 * Constructeur
	 */
	public WebInformations(SystemManager asystem, String lib, char aletter)
	{
		system = asystem;
		if( aletter != letter ){
			letter = aletter;
		}
		if( lib == null){
			library = letter + "WEB";
		} else{
			library = lib;
		}
	}

	// -- M�thodes priv�es

	/**
	 * R�cup�re les informations sur la configuration de l'AS400
	 * @return
	 */
	public boolean readInformations()
	{
		String data = readData( getDataArea() );
		if( data == null ){
			return false;
		}

		return analyzeData( data );
	}

	/**
	 * Ecrit la date pour une application web
	 * @param data
	 * @param off
	 * @return
	 */
	public boolean writeDate(String data, int length, int off)
	{
		if( (data == null) || ( off > LONG_DTAARA) || ( off+data.length() > LONG_DTAARA) ){
			return false;
		}
		
		char[] tab = new char[length];
		Arrays.fill(tab, ' ');
		System.arraycopy(data.toCharArray(), 0, tab, 0, data.length());

		return writeData(getDataArea(), new String(tab), off);
	}
	
	/**
	 * Ecrit la version de Tomcat
	 * @return
	 */
	public boolean writeVersionTomcat()
	{
		String data = getVersionTomcat();
		if( (data == null) ){
			return false;
		}
		
		char[] tab = new char[LONG_VERSION_TOMCAT];
		Arrays.fill(tab, ' ');
		System.arraycopy(data.toCharArray(), 0, tab, 0, data.length());

		return writeData(getDataArea(), new String(tab), OFFSET_VERSION_TOMCAT);
	}

	/**
	 * Efface la partie TODO de la dataarea
	 * @return
	 */
	public boolean resetToDo()
	{
		char[] tab = new char[LONG_TODO];
		Arrays.fill(tab, ' ');

		return writeData(getDataArea(), new String(tab), OFFSET_TODO);
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Retourne la dataarea qui stocke les informations o� l'on stocke les informations des applis web
	 * @return
	 */
	private CharacterDataArea getDataArea()
	{
		if( dataArea == null ){ 
			QSYSObjectPathName path = new QSYSObjectPathName(library, library, "DTAARA");
			dataArea = new CharacterDataArea(system.getSystem(), path.getPath());
		}
		return dataArea;
	}

	/**
	 * Analyse les donn�es de la dataarea
	 */
	private boolean analyzeData(String data)
	{
		if( data.length() < LONG_DTAARA ){
			msgErreur += "\n[WebInformations] (analyzeData) Longueur de dataarea inf�rieure � " + LONG_DTAARA + " octets.";
			return false;
		}
		
		// Ce que l'on doit faire
		String chaine = data.substring( OFFSET_TODO, OFFSET_TODO+LONG_TODO );
		if( chaine.startsWith( KEY_INSTALLATION ) ){
			isInstallation = true;
		}
		
		// R�cup�ration des informations sur les applications
		String applis = data.substring( OFFSET_APPLIS, LONG_APPLIS );
		int index = 1;
		for( int i = OFFSET_APPLIS; i < LONG_APPLIS; i+=40 ){
			if( !applis.substring(i, i+20).trim().equals( "" ) ){
				Application app = new Application( applis.substring(i, i+20), applis.substring(i+20, i+40) );
				app.setOffsetName( i );
				app.setOffsetLastModified( i+20 );
				if( !isInstallation ){
					app.setMaj( chaine.charAt( index ) != ' ' );
					if( app.isMaj() ){
						setMajApplis( true );
					}
					index++;
				}
				listapp.add( app );
			}
		}

		// Sur le port (le chiffre des dizaines)
		setDigit(data.charAt( OFFSET_DIGIT ));
		
		// La version de tomcat
		setVersionTomcat( data.substring( OFFSET_VERSION_TOMCAT, OFFSET_VERSION_TOMCAT+LONG_VERSION_TOMCAT ).trim() );
		setMajTomcat(chaine.charAt( 0 ) != ' ');
		
		return true;
	}
	
	/**
	 * Lit et retourne le contenu de la DTAARA
	 * @return
	 */
	private String readData(CharacterDataArea dataarea)
	{
		String data = null;
		
		try
		{
			data = dataarea.read();
		}
		catch (Exception e)
		{
			msgErreur += "\n[WebInformations] (readData) Erreur : " + e;
		}
		
		return data;
	}

	/**
	 * Ecrit des donn�es dans la DTAARA 
	 * @param dataarea
	 * @param data
	 * @param off
	 * @return
	 */
	private boolean writeData(CharacterDataArea dataarea, String data, int off)
	{
		if( data == null){
			return false;
		}
		try
		{
			dataarea.write( data, off );
			return true;
		}
		catch (Exception e)
		{
			msgErreur += "\n[WebInformations] (writeData) Erreur : " + e;
		}
		return false;
	}
	
	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le listapp
	 */
	public ArrayList<Application> getListapp()
	{
		return listapp;
	}

	/**
	 * @param listapp le listapp � d�finir
	 */
	public void setListapp(ArrayList<Application> listapp)
	{
		this.listapp = listapp;
	}

	/**
	 * @return la versionTomcat
	 */
	public String getVersionTomcat()
	{
		return versionTomcat;
	}

	/**
	 * @param versionTomcat le versionTomcat � d�finir
	 */
	public void setVersionTomcat(String versionTomcat)
	{
		this.versionTomcat = versionTomcat;
	}

	/**
	 * @return le digit
	 */
	public char getDigit()
	{
		return digit;
	}

	/**
	 * @param digit le digit � d�finir
	 */
	public void setDigit(char digit)
	{
		if( digit != ' ')
			this.digit = digit;
	}

	/**
	 * @return le isInstallation
	 */
	public boolean isInstallation()
	{
		return isInstallation;
	}

	/**
	 * @param isInstallation le isInstallation � d�finir
	 */
	public void setInstallation(boolean isInstallation)
	{
		this.isInstallation = isInstallation;
	}

	/**
	 * @return le isMajTomcat
	 */
	public boolean isMajTomcat()
	{
		return isMajTomcat;
	}

	/**
	 * @param isMajTomcat le isMajTomcat � d�finir
	 */
	public void setMajTomcat(boolean isMajTomcat)
	{
		this.isMajTomcat = isMajTomcat;
	}

	/**
	 * @return le isMajApplis
	 */
	public boolean isMajApplis()
	{
		return isMajApplis;
	}

	/**
	 * @param isMajApplis le isMajApplis � d�finir
	 */
	public void setMajApplis(boolean isMajApplis)
	{
		this.isMajApplis = isMajApplis;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
