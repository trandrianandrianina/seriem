//=================================================================================================
//==>                                                                       25/11/2014 - 25/11/2014
//==> R�cup�re les informations sur l'AS400
//==> A faire:
//=================================================================================================
package ri.seriem.installweb.installation;

import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libas400.system.SystemManager;

public class AS400Informations
{
	// Constantes
	private static final int LONG_DTAARA = 64; 
	private static final String DTA_NAME = "VGMDTA";
	private static final String LIB_NAME = "QGPL";

	// Variables
	private String adresseIP=null;

	private SystemManager system = null;
	private char letter = 'X';

	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 

	
	/**
	 * Constructeur
	 * @param asystem
	 * @param aletter
	 */
	public AS400Informations(SystemManager asystem, char aletter)
	{
		system = asystem;
		if( aletter != letter ){
			letter = aletter;
		}
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * R�cup�re les informations sur la configuration de l'AS400
	 * @return
	 */
	public boolean readInformations()
	{
		QSYSObjectPathName path = new QSYSObjectPathName(LIB_NAME, DTA_NAME, "DTAARA");
		CharacterDataArea dataArea = new CharacterDataArea(system.getSystem(), path.getPath());
		String data = readData(dataArea);
		if( data == null ){
			return false;
		}

		return analyzeData( data );
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Analyse les donn�es de la dataarea
	 * @param data
	 * @return
	 */
	private boolean analyzeData(String data)
	{
		if( data.length() < LONG_DTAARA ){
			msgErreur += "\n[AS400Informations] (analyzeData) Longueur de dataarea inf�rieure � " + LONG_DTAARA + " octets.";
			return false;
		}
		
		adresseIP = data.substring( 0, 15 ).trim();
		
		return true;
	}
	
	/**
	 * Lit et retourne le contenu de la DTAARA
	 * @return
	 */
	private String readData(CharacterDataArea dataarea)
	{
		String data = null;
		
		try
		{
			data = dataarea.read();
		}
		catch (Exception e)
		{
			msgErreur += "\n[AS400Informations] (readData) Erreur : " + e;
		}
		
		return data;
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le adresse
	 */
	public String getAdresseIP()
	{
		return adresseIP;
	}

	/**
	 * @param adresse le adresse � d�finir
	 */
	public void setAdresseIP(String adresse)
	{
		this.adresseIP = adresse;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
