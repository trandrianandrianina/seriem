//=================================================================================================
//==>                                                                       21/11/2014 - 08/12/2014
//==> Gestion de l'installation pour les applications WEB
//==> A faire:
//=================================================================================================
package ri.seriem.installweb.installation;

import java.io.File;
import java.util.ArrayList;

import com.ibm.as400.access.ISeriesNetServer;
import com.ibm.as400.access.ISeriesNetServerFileShare;

import ri.seriem.libas400.system.AS400FTPManager;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.FileNG;
import ri.seriem.libcommun.outils.GestionFichierTexte;
import ri.seriem.libcommun.outils.GestionFichierZip;

public class Installation
{
	// Constantes
	private static final String VERSION = "1.01";
	private static final String TEMP_IFS_FOLDER = "/tmp";
	private static final String RADICAL_TOMCAT = "apache-tomcat";
	private static final String TOMCAT = "tomcat";
	private static final String NAME_WEBSHOP = "WebShop";
	//private static final String NAME_MOBILITE = "mobilite";
	private static final ArrayList<Character> MONTH = new ArrayList<Character>(){
		private static final long serialVersionUID = 1L;
		{ add('J'); }
		{ add('F'); }
		{ add('M'); }
		{ add('A'); }
		{ add('S'); }
		{ add('O'); }
		{ add('N'); }
		{ add('D'); }
	};
	
	// Variables
	private SystemManager system = new SystemManager(false);
	private char letter = 'X';					// La lettre de l'environnement 
	private String installfolder = "/xweb";		// Le dossier racine d'installation
	private String library = "xweb";			// Le nom de la biblioth�que
	private String profilFTP = null;
	private String mdpFTP = null;
	private String adresseFTP = null;
	private String repertoireFTP = null;
	private char digit = '7';					// Correspond au chiffre des dizaines  8080 devient 8070 par exemple
	private String adresseIPlocale = "localhost"; 
	private AS400FTPManager ftp = null;
	private String fileDlTomcat = null;
	private String folderwebapps = null;
	private WebInformations webinfos = null;
	private SiteInformations infosFTP = null;
	private String profilManager = null;
	private String mdpManager = null;

	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 

	
	/**
	 * Constructeur.
	 */
	public Installation(char pletter, String plib, String pProfilFtp, String pMdpFtp, String pAdress, String pRepertoire,
			String pProfilManager, String pMdpManager)
	{
		letter = pletter;
		if( plib != null ){
			library = plib.trim();
		}
		if( library != null ){
			installfolder = File.separatorChar + library.toLowerCase();
		}
		
		if( pProfilFtp != null ){
			profilFTP = pProfilFtp.trim();
		}
		
		if( pMdpFtp != null ){
			mdpFTP = pMdpFtp.trim();
		}
		
		if( pAdress != null ){
			adresseFTP = pAdress.trim();
		}
		
		if( pRepertoire != null ){
			repertoireFTP = pRepertoire.trim();
			if (!repertoireFTP.endsWith("/")) {
				repertoireFTP = repertoireFTP + "/";
			}
		}

		if( pProfilManager != null ){
			profilManager = pProfilManager.trim();
		}
		
		if( pMdpManager != null ){
			mdpManager = pMdpManager.trim();
		}

		// R�cup�ration des donn�es sur les applis Web 
		webinfos = new WebInformations(system, library, letter);
		if( !webinfos.readInformations() ){
			msgErreur += "\n[Installation] (Installation) " + webinfos.getMsgError();
		} else{
			digit = webinfos.getDigit();
		}
	}

	/**
	 * Traitement pour l'installation
	 * @return
	 */
	public boolean treatment()
	{
		if( webinfos.isInstallation() ){
			return treatmentInstallation();
		} else{
			return treatmentUpdate();
		}
	}
	
	/**
	 * Traitement pour l'installation
	 * @return
	 */
	private boolean treatmentInstallation()
	{
		boolean tomcatIsInstalled = false;
		
		System.out.println("== Installation pour les applications mobiles ("+VERSION+") ==\n");
		System.out.println("\t-- Partage du dossier de l'IFS");
		shareIFSFolder();
		System.out.println( getMsgError() );

		System.out.println("\t-- Contr�le de l'installation");
		if( !executeInstallation()  ){
			msgErreur += "\n\t-- Tomcat est d�j� install�.";
			System.out.println( getMsgError() );
			tomcatIsInstalled = true;
		}
		
		System.out.println("\t-- Connexion au serveur FTP");
		if( !connexion() ){
			return false;
		}
		
		if( !tomcatIsInstalled ){
			System.out.println("\t-- T�l�chargement de Tomcat");
			if( !downloadTomcat() ){
				return false;
			}

			System.out.println("\t-- D�compression de Tomcat");
			if( !unzipTomcat() ){
				return false;
			}

			System.out.println("\t-- Configuration de Tomcat");
			if( !configureTomcat() ){
				return false;
			}
		}
		
		System.out.println("\t-- T�l�chargement des applications web");
		if( !downloadApplis() ){
			return false;
		}

		String cmd = "QSH CMD('" + installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "startup.sh')";
		String url = "http://ADRESSE_IP_AS400:80" + digit + "0/" + NAME_WEBSHOP;
		msgErreur += "\t-- Installation termin�e.\n\t\t- Il ne vous reste plus qu'� d�marrer le serveur Tomcat avec la commande:\n\t\t\t"+cmd;
		msgErreur += "\n\t\t- Puis � vous connecter localement avec l'URL suivante :\n\t\t\t"+url;
		msgErreur += "\n\n\tNote: Attention � la casse du chemin pour d�marrer le serveur Tomcat (Erreur: ...is malformed and will be ignored...).";
		return true;
	}
	
	/**
	 * Traitement pour la mise � jour
	 * @return
	 */
	private boolean treatmentUpdate()
	{
		System.out.println("== Mise � jour des applications mobiles ("+VERSION+") ==\n");
		
		if( !webinfos.isMajTomcat() && !webinfos.isMajApplis() ){
			System.out.println("\t-- Aucune mise � jour demand�e");
			return true;
		}

		System.out.println("\t-- Connexion au serveur FTP");
		if( !connexion() ){
			return false;
		}
		
		if( !updateTomcat() ){
			return false;
		}
		
		System.out.println("\t-- T�l�chargement des applications web");
		if( !downloadApplis() ){
			return false;
		}

		String cmd = "QSH CMD('" + installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "startup.sh')";
		String url = "http://ADRESSE_IP_AS400:80" + digit + "0/" + NAME_WEBSHOP;
		if( webinfos.isMajTomcat() ){
			msgErreur += "\n\t-- Installation termin�e.\n\t\t- Il ne vous reste plus qu'� d�marrer le serveur Tomcat avec la commande:\n\t\t\t"+cmd;
			msgErreur += "\n\t\t- Puis � vous connecter localement avec l'URL suivante :\n\t\t\t"+url;
		} else{
			msgErreur += "\n\t-- Installation termin�e.\n\t\t- Il ne vous reste plus qu'� vous connecter localement avec l'URL suivante :\n\t\t\t"+url;
		}
		msgErreur += "\n\n\tNote: Attention � la casse du chemin pour d�marrer le serveur Tomcat (Erreur: ...is malformed and will be ignored...).";
		return true;
	}
	
	/**
	 * Mise � jour de Tomcat
	 * @return
	 */
	private boolean updateTomcat()
	{
		// On v�rifie si l'on doit mettre � jour Tomcat (demande depuis InstWebCL)
		if ( !webinfos.isMajTomcat() ){
			return true;
		}

		System.out.println("\t-- T�l�chargement de Tomcat");
		if( !downloadTomcat() ){
			return false;
		}

		// On v�rifie si l'on doit mettre � jour Tomcat (download a v�rifi� si c'�tait vraiment n�cessaire)
		if ( !webinfos.isMajTomcat() ){
			return true;
		}

		System.out.println("\t-- Sauvegarde des fichiers de configuration de Tomcat");
		if( !saveConfTomcat() ){
			return false;
		}
		
		System.out.println("\t-- Suppression du dossier de Tomcat");
		File tomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
		FileNG.remove( tomcatfolder );
		if( tomcatfolder.exists() ){
			msgErreur += "\nLe dossier de Tomcat n'a pas �t� supprim� correctement";
			return false;
		}
		
		System.out.println("\t-- D�compression de Tomcat");
		if( !unzipTomcat() ){
			return false;
		}

		System.out.println("\t-- Configuration de Tomcat");
		if( !configureTomcat() ){
			return false;
		}
		
		return true;
	}
	 
	/**
	 * Sauvegarde des fichiers de configuration de Tomcat 
	 * @return
	 */
	private boolean saveConfTomcat()
	{
		FileNG context = new FileNG( installfolder + File.separatorChar + TOMCAT + File.separatorChar + "conf" + File.separatorChar + "context.xml" ); 
		FileNG server = new FileNG( installfolder + File.separatorChar + TOMCAT + File.separatorChar + "conf" + File.separatorChar + "server.xml" );
		FileNG setenv = new FileNG( installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "setenv.sh" );
		FileNG tomcat_users = new FileNG( installfolder + File.separatorChar + TOMCAT + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.xml" );
		File savefolder = new File(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT);
		
		if( savefolder.exists() ){
			FileNG.removeContents( savefolder );
		} else{
			savefolder.mkdirs();
		}
		
		boolean ret1 = context.copyTo( savefolder.getAbsolutePath() + File.separatorChar + "context.xml" );
		if( !ret1 ){
			msgErreur += "\nEchec de la sauvegarde du fichier " + context.getAbsolutePath();
		}
		boolean ret2 = server.copyTo( savefolder.getAbsolutePath() + File.separatorChar + "server.xml" );
		if( !ret2 ){
			msgErreur += "\nEchec de la sauvegarde du fichier " + server.getAbsolutePath();
		}
		boolean ret3 = setenv.copyTo( savefolder.getAbsolutePath() + File.separatorChar + "setenv.sh" );
		if( !ret3 ){
			msgErreur += "\nEchec de la sauvegarde du fichier " + setenv.getAbsolutePath();
		}
		boolean ret4 = tomcat_users.copyTo( savefolder.getAbsolutePath() + File.separatorChar + "tomcat-users.xml" );
		if( !ret4 ){
			msgErreur += "\nEchec de la sauvegarde du fichier " + tomcat_users.getAbsolutePath();
		}
		return ret1 & ret2 & ret3 & ret4;
	}

	/**
	 * Restaure les fichiers de configuration de Tomcat 
	 */
	private boolean restoreConfTomcat()
	{
		File finstalltomcatfolder = new File( installfolder + File.separatorChar + TOMCAT );
		
		// Sauvegarde les nouveaux fichiers
		boolean ret1 = false;
		FileNG context = new FileNG( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "context.xml" ); 
		FileNG server = new FileNG( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "server.xml" );
		FileNG tomcat_users = new FileNG( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.xml" );
		ret1 = context.renameTo( new File( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "context.new" ) );
		ret1 = server.renameTo( new File( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "server.new" ) );
		ret1 = tomcat_users.renameTo( new File( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.new" ) );
		
		// Restaure les anciens
		context = new FileNG( TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "context.xml" ); 
		server = new FileNG( TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "server.xml" );
		tomcat_users = new FileNG( TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "tomcat-users.xml" );
		FileNG setenv = new FileNG( TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "setenv.sh" );
		File rstfolderconf = new File( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" );
		File rstfolderbin = new File( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "bin" );
		
		ret1 = context.copyTo( rstfolderconf.getAbsolutePath() + File.separatorChar + "context.xml" );
		if( !ret1 ){
			msgErreur += "\nEchec de la restauration du fichier " + context.getAbsolutePath();
		}
		boolean ret2 = server.copyTo( rstfolderconf.getAbsolutePath() + File.separatorChar + "server.xml" );
		if( !ret2 ){
			msgErreur += "\nEchec de la restauration du fichier " + server.getAbsolutePath();
		}
		boolean ret3 = setenv.copyTo( rstfolderbin.getAbsolutePath() + File.separatorChar + "setenv.sh" );
		if( !ret3 ){
			msgErreur += "\nEchec de la restauration du fichier " + setenv.getAbsolutePath();
		}
		boolean ret4 = tomcat_users.copyTo( rstfolderconf.getAbsolutePath() + File.separatorChar + "tomcat-users.xml" );
		if( !ret4 ){
			msgErreur += "\nEchec de la restauration du fichier " + tomcat_users.getAbsolutePath();
		}
		return ret1 & ret2 & ret3 & ret4;
	}

	
	/**
	 * Controle l'installation de Tomcat afin de d�terminer la suite de l'installation
	 * @return
	 */
	private boolean executeInstallation()
	{
		File tomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
		if( !tomcatfolder.exists() ){
			return true;
		}
		
		File tomcatstartup = new File(tomcatfolder.getAbsolutePath() + File.separatorChar + "bin" + File.separatorChar + "startup.sh");
		if( !tomcatstartup.exists() ){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Connexion au serveur FTP
	 * @return
	 */
	private boolean connexion()
	{
		// R�cup�ration des donn�es pour la connexion
		infosFTP = new SiteInformations(system, letter, profilFTP, mdpFTP, adresseFTP, repertoireFTP);

		// Connexion au serveur FTP
		ftp = new AS400FTPManager();
		boolean ret = ftp.connexion( infosFTP.getAdresse(), infosFTP.getLogin(), infosFTP.getMdp() );
		if( !ret ){
			msgErreur += "\n[Installation] (connexion) " + ftp.getMsgError();
		}
		else msgErreur+= "Ok connexion FTP";
		
		System.out.println(msgErreur);
		msgErreur = "";
		
		return ret;
	}
	
	/**
	 * T�l�chargement du fichier zip de Tomcat
	 * @return
	 */
	private boolean downloadTomcat()
	{
		// R�cup�ration du dossier racine sur le serveur FTP
		String dossierTomcat = infosFTP.getRepertoireFTP();
		int pos = dossierTomcat.indexOf("/", 1);
		if (pos != -1) {
			dossierTomcat = dossierTomcat.substring(0, pos);
		}
		if (!dossierTomcat.endsWith("/")) {
			dossierTomcat = dossierTomcat + "/tomcat/";
		}
		else {
			dossierTomcat = dossierTomcat + "tomcat/";
		}

		// Listage des fichiers sur le serveur FTP
		String[] list = ftp.listFolder(dossierTomcat);
		if( list == null){
			msgErreur += "\n[Installation] (downloadTomcat) Erreur lors du listage du dossier " + dossierTomcat;
			return false;
		}
		// On s'assure qu'il s'y trouve
		String fileServerTomcat = null;
		for( String file : list){
			if( ( file.indexOf( RADICAL_TOMCAT ) != -1 ) && file.endsWith( ".zip" ) ){
				// On v�rifie que le dossier de Tomcat soit bien pr�sent
				File tomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
				if (!tomcatfolder.exists()) {
					fileServerTomcat = file;
					webinfos.setMajTomcat( true );
				}
				// On v�rifie la version (si c'est la m�me on ne fait rien) 
				else if( webinfos.getVersionTomcat().length() == 0 || file.indexOf( webinfos.getVersionTomcat() ) == -1 ){
					fileServerTomcat = file;
					webinfos.setMajTomcat( true );
				} else {
					System.out.println("\t\t- Tomcat est � jour (" + webinfos.getVersionTomcat() +")" );
					webinfos.setMajTomcat( false );
					return true;
				}
				break;
			}
		}
		if( fileServerTomcat == null ){
			msgErreur += "\n[Installation] (downloadTomcat) Erreur le fichier zip de tomcat n'a pas �t� trouv�.";
			return false;
		}

		// T�l�chargement du fichier
		pos = fileServerTomcat.indexOf( RADICAL_TOMCAT );
		if( pos != -1 ){
			fileDlTomcat = TEMP_IFS_FOLDER + File.separatorChar + fileServerTomcat.substring(pos);
		}
		boolean ret = ftp.downloadFile( fileServerTomcat, fileDlTomcat, true );
		if( !ret ){
			msgErreur += "\n" + ftp.getMsgError();
		}
		return ret;
	}
	
	/**
	 * D�zippe le fichier zip de Tomcat dans le dossier voulu
	 * @return
	 */
	private boolean unzipTomcat()
	{
		if( fileDlTomcat == null ){
			return false;
		}
		File zip = new File(fileDlTomcat);
		if( !GestionFichierZip.unzip( zip, new File(installfolder) ) ){
			msgErreur += "\n[Installation] (unzipTomcat) Erreur lors du d�zippage du fichier " + fileDlTomcat + " dans le dossier " + installfolder;
			return false;
		} else{ // On supprime le fichier zip
			zip.delete();
			fileDlTomcat = zip.getName().substring(0, zip.getName().length()-4);
		}

		return true;
	}
	
	/**
	 * Configure Tomcat
	 * @return
	 */
	private boolean configureTomcat()
	{
		// On renomme le dossier Tomcat
		File finstallfolder = new File(installfolder);
		File finstalltomcatfolder = new File( installfolder + File.separatorChar + TOMCAT );
		File[] listFolder = finstallfolder.listFiles();
		for( File folder : listFolder ){
			if( folder.getName().startsWith( RADICAL_TOMCAT ) ){
				folder.renameTo( finstalltomcatfolder );
				webinfos.setVersionTomcat( folder.getName() );
				webinfos.writeVersionTomcat();
				break;
			}
		}
		
		// Cr�ation du fichier de version dans le dossier Tomcat
		GestionFichierTexte gft = new GestionFichierTexte();
		gft.setNomFichier(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "tomcat.version");
		gft.setContenuFichier(fileDlTomcat);
		gft.ecritureFichier();
		gft.dispose();
		
		// Configuration des fichiers
		if( webinfos.isInstallation() ){
			// Lors de l'installation
			configureServer_XML( finstalltomcatfolder );
			configureContext_XML( finstalltomcatfolder );
			configureTomcatUsers_XML( finstalltomcatfolder );
			configureLogging_properties(finstalltomcatfolder);
			createSetEnv();

		} else{
			// Lors de la mise � jour
			if( !restoreConfTomcat() ){
				msgErreur += "\nEchec de la restauration des fichiers de configuration de Tomcat";
				return false;
			} else{
				FileNG.remove( new File(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT ) ); 
			}
		}

		return true;
	}

	/**
	 * Modifie le fichier server.xml.
	 */
	private boolean configureServer_XML(File finstalltomcatfolder)
	{
		GestionFichierTexte server = new GestionFichierTexte( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "server.xml" );
		if( server.lectureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + server.getMsgErreur();
			return false;
		}

		boolean isComment = false;
		String[] source = server.getContenuFichierTab();
		for (int i = 0; i < source.length; i++){
			// Gestion des commentaires
			if( source[i].trim().startsWith( "<!--" ) ){
				isComment = true;
			}
			if( isComment ){
				if( source[i].trim().endsWith( "-->" ) ){
					isComment = false;
				}
				if( isComment){
					continue;
				}
			}
			// Gestion des lines � modifier 
			String line = modifiePortLine(source[i], "port=\"", "SHUTDOWN");
			if( line != null ){
				source[i] = line;
			} else{
				line = modifiePortLine(source[i], "port=\"", "HTTP/1.1");
				if( line != null ){
					source[i] = line;
				} else{
					line = modifiePortLine(source[i], "port=\"", "AJP/1.3");
					if( line != null ){
						source[i] = line;
					}
				}
			}
		}

		// On r��crit le fichier modifi�
		server.setContenuFichier(source);
		if( server.ecritureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + server.getMsgErreur();
			return false;
		}
		return true;
	}
	
	/**
	 * Modifie le fichier context.xml.
	 */
	private boolean configureContext_XML(File finstalltomcatfolder)
	{
		// Chargement du fichier context.xml
		GestionFichierTexte context = new GestionFichierTexte( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "context.xml" );
		if( context.lectureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + context.getMsgErreur();
			return false;
		}

		ArrayList<String> source = context.getContenuFichier();
		// On ins�re les lignes � la fin du fichier
		int avtderniere = source.size() - 2;
		source.add( avtderniere++, "\t<!-- MOBILITE & WEBSHOP -->");
		source.add( avtderniere++, String.format( "\t<Parameter name=\"LIB_RACINE\" value=\"%s\" override=\"false\"/>", library ) );
		source.add( avtderniere++, "\t<Parameter name=\"IS_ECLIPSE\" value=\"false\" override=\"false\"/>" );
		source.add( avtderniere++, "\t<!-- " + String.format( "<Parameter name=\"LIBELLE_SERVEUR\" value=\"%s\" override=\"false\"/>" , "#A COMPLETER#") + " -->");
		source.add( avtderniere++, String.format( "\t<Parameter name=\"DOSSIER_RACINE_TOMCAT\" value=\"%s\" override=\"false\"/>", finstalltomcatfolder.getAbsolutePath() + File.separatorChar ) );
		source.add( avtderniere++, String.format( "\t<Parameter name=\"DOSSIER_RACINE_PUBLIC\" value=\"%s\" override=\"false\"/>", finstalltomcatfolder.getParent() + File.separatorChar + "public" + File.separatorChar ) );
		source.add( avtderniere++, String.format( "\t<Parameter name=\"DOSSIER_RACINE_PRIVATE\" value=\"%s\" override=\"false\"/>",  finstalltomcatfolder.getParent() + File.separatorChar + "private" + File.separatorChar ) );
		source.add( avtderniere++, "");
		source.add( avtderniere++, "\t<!-- WEBSHOP -->");
		source.add( avtderniere++, String.format( "\t<Parameter name=\"ADRESSE_AS400\" value=\"%s\" override=\"false\"/>" , adresseIPlocale ) );
		source.add( avtderniere++, String.format( "\t<Parameter name=\"DOSSIER_RACINE_XWEBSHOP\" value=\"%s\" override=\"false\"/>", finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "webapps" + File.separatorChar + NAME_WEBSHOP + File.separatorChar ) );

		// On r��crit le fichier modifi�
		context.setContenuFichier(source);
		if( context.ecritureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + context.getMsgErreur();
			return false;
		}
		return true;
	}
	
	/**
	 * Modifie le fichier tomcat-users.xml.
	 */
	private boolean configureTomcatUsers_XML(File finstalltomcatfolder)
	{
		if (profilManager == null || profilManager.isEmpty() || mdpManager == null || mdpManager.isEmpty()) {
			return true;
		}
		GestionFichierTexte tomcat_users = new GestionFichierTexte( finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.xml" );
		if( tomcat_users.lectureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + tomcat_users.getMsgErreur();
			return false;
		}
		
		int indiceInsertion = -1; 
		ArrayList<String> source = tomcat_users.getContenuFichier();
		// On parcoourt le source de la fin ver sle d�but afin de trouver le marqueur </tomcat-users>
		for (int ligne=source.size(); --ligne>=0; ) {
			if (source.get(ligne).trim().equals("</tomcat-users>")) {
				indiceInsertion = ligne--;
				break;
			}
		}
		// Si on a trouv� le marqueur alors on ins�re les lignes qui nous int�resse 
		if (indiceInsertion > -1) {
			// Construction de la liste des information � ins�rer
			ArrayList<String> donneesManager = new ArrayList<String>();
			donneesManager.add(" <role rolename=\"manager\"/>");
			donneesManager.add(" <user username=\"" + profilManager + "\" password=\"" + mdpManager + "\" roles=\"manager\"/>");
			source.addAll(indiceInsertion, donneesManager);

		// On r��crit le fichier modifi�
		tomcat_users.setContenuFichier(source);
		if( tomcat_users.ecritureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + tomcat_users.getMsgErreur();
			return false;
		}
		}
		return true;
	}

	/**
	 * Modifie le fichier logging.properties.
	 */
	private boolean configureLogging_properties(File finstalltomcatfolder)
	{
		GestionFichierTexte logging = new GestionFichierTexte(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar +"logging.properties");
		if (logging.lectureFichier()==Constantes.ERREUR )
		{
			msgErreur += "\n" + logging.getMsgErreur();
			return false;
		}
		
		String[] contenu = logging.getContenuFichierTab();
		for (int i=0;i<contenu.length;i++)
		{
			if (contenu[i].startsWith("handlers"))
				contenu[i]=contenu[i].replace("handlers", "# handlers");
			else if (contenu[i].startsWith(".handlers"))
				contenu[i]=contenu[i].replace(".handlers", "handlers");
			else if (contenu[i].startsWith("1catalina"))
				contenu[i]=contenu[i].replace("1catalina", "#1catalina");
			else if(contenu[i].startsWith("2localhost"))
				contenu[i]= contenu[i].replace("2localhost", "#2localhost");
			else if (contenu[i].startsWith("3manager"))
				contenu[i]= contenu[i].replace("3manager", "#3manager");
			else if (contenu[i].startsWith("4host"))
				contenu[i]= contenu[i].replace("4host", "#4host");
			else if (contenu[i].startsWith("org."))
				contenu[i]=contenu[i].replace("org.", "#org.");
		}
		// On r��crit le fichier modifi�
		logging.setContenuFichier(contenu);
		if( logging.ecritureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + logging.getMsgErreur();
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * Modifie une ligne si n�cessaire 
	 * @param line
	 * @param token
	 * @param confirm
	 * @return
	 */
	private String modifiePortLine(String line, String token, String confirm)
	{
		int pos = line.indexOf( token );
		if( pos == -1 ){
			return null;
		}
		if( line.indexOf( confirm ) == -1){
			return null;
		}

		pos += token.length() + 2; // 3 pour la position des dizaines
		line = line.substring( 0, pos ) + digit + line.substring( pos+1 );
		
		return line; 
	}
	
	/**
	 * Cr�er le fichier setenv.sh dans le dossier bin du serveur Tomcat
	 * @return
	 */
	private boolean createSetEnv()
	{
		GestionFichierTexte gft = new GestionFichierTexte(installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "setenv.sh");
		if( gft.isPresent() ) return true;
		
		String[] data = new String[3];
		data[0] = "#!/bin/sh";
		data[1] = "JAVA_HOME=/QOpensys/QIBM/ProdData/JavaVM/jdk60/32bit";
		data[2] = "JRE_HOME=/QOpensys/QIBM/ProdData/JavaVM/jdk60/32bit/jre";
		gft.setContenuFichier(data);
		if( gft.ecritureFichier() == Constantes.ERREUR ){
			msgErreur += "\n" + gft.getMsgErreur();
			return false;
		}
		return true;
	}
	
	/**
	 * Partage du dossier dans l'IFS
	 * @return
	 */
	private boolean shareIFSFolder()
	{
		ISeriesNetServer ns = new ISeriesNetServer(system.getSystem());

		// On recherche si le dossier est d�j� partag�
		String nameshare = library.toLowerCase();
		String namepath = null;
		boolean foundshare = false;
		boolean foundpath = false;
		try{
			ISeriesNetServerFileShare[] liste = ns.listFileShares();
			for( ISeriesNetServerFileShare elt : liste ){
				if( elt.getPath().equalsIgnoreCase( installfolder ) ){
					foundpath = true;
				}
				if( elt.getName().equalsIgnoreCase( nameshare ) ){
					foundshare = true;
					namepath = elt.getPath();
				}
			}
			if( !foundpath && !foundshare ){
				ns.createFileShare( nameshare, installfolder );
				msgErreur += "\t\t- Le dossier '" + installfolder + "' est partag� sous le nom '" + nameshare + "'";
			} else if( foundshare &&  foundpath ){
				msgErreur += "\t\t- Le dossier '" + installfolder + "' est d�j� partag� sous le nom '" + nameshare + "'";
			} else if( foundshare ){
				msgErreur += "\t\t- Le dossier '" + namepath + "' utilise d�j� le nom '" + nameshare + "'";
			}
			return true;
		}
		catch(Exception e){
			msgErreur += "\n[Installation] (shareIFSFolder) Erreur : " + e;
			return false;
		}
	}
	
	/**
	 * T�l�chargement des fichiers war n�cessaires. 
	 */
	private boolean downloadApplis()
	{
		ArrayList<Application> listapp = webinfos.getListapp();
		if( listapp.isEmpty() ){
			return true;
		}
		
		// Listage des fichiers war sur le serveur FTP
		String[] list = ftp.listFolderWithAttribute(infosFTP.getRepertoireFTP());
		if( list == null){
			msgErreur += "\n[Installation] (downloadApplis) Erreur lors du listage du dossier " + infosFTP.getRepertoireFTP();
			return false;
		}
		// On d�coupe les attributs du listage (du serveur FTP)
		String[] date = retrieveInfos( list );

		// On compare la liste du serveur avec celle des applis � t�l�charger et on alimente les attributs
		int i = 0;
		while( i < listapp.size() ){
			int index = isFound(list, listapp.get(i).getName() );
			if( index > -1 ){
				listapp.get(i).setFile( list[index] );
				listapp.get(i).setLastModifiedOnFTP( date[index] );
				i++;
			} else{
				System.out.println("\t\t- " + listapp.get(i).getName() + " non trouv� sur le serveur FTP");
				listapp.remove(i);
			}
		}
		if( listapp.isEmpty() ){
			System.out.println("\t\t- Aucune application � t�l�charger");
			msgErreur += "\n[Installation] (downloadApplis) Erreur les applis demand�es ne sont pas sur le site";
			return true;
		}

		// T�l�chargement des fichiers war
		folderwebapps = installfolder + File.separatorChar + TOMCAT + File.separatorChar + "webapps";
		boolean ret = true;
		File destination = null;
		for( Application war : listapp ){
			destination = new File( folderwebapps + File.separator + war.getFile() );
			if( !war.getLastModified().equalsIgnoreCase( war.getLastModifiedOnFTP() ) ||  !destination.exists() ){ // TODO � am�liorer comparaison juste 
				System.out.println("\t\t- " + destination + " � t�l�charger");
				ret = ftp.downloadFile(infosFTP.getRepertoireFTP() + war.getFile(), destination.getAbsolutePath(), true );
				if( !ret ){
					msgErreur += "\n" + ftp.getMsgError();
				} else{ // Mise � jour des infos dans la Dtaara
					webinfos.writeDate(war.getLastModifiedOnFTP(), 20, war.getOffsetLastModified());
				}
			} else{
				System.out.println("\t\t- " + war.getName() + " est � jour");
			}
		}

		return ret;
	}

	/**
	 * Recherche un fichier dans une liste.
	 */
	private int isFound(String[] listremote, String filetofind)
	{
		filetofind = filetofind.toUpperCase();
		int i = 0;
		for( String war : listremote){
			if( war.toUpperCase().equals( filetofind ) ){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	/**
	 * Retrouve le nom du fichier et la date dans les attributs
	 * @param attribut
	 * @return
	 */
	private String[] retrieveInfos(String[] list)
	{
		String[] attributs = new String[list.length];
		
		for(int i=0; i<list.length; i++){
			// Recherche du nom
			int pos = list[i].lastIndexOf( ' ' );
			int posnom = list[i].indexOf( ' ', pos ); // TODO a corrig�
			String chaine = list[i].substring( posnom + 1 );
			// Recherche de la date
			for (int j = pos-1; j !=0; j--){
				if( MONTH.contains(list[i].charAt( j ) ) ){
					pos = j;
					break;
				}
			}
			attributs[i] = list[i].substring( pos, posnom );
			list[i] = chaine;
		}
		return attributs;
	}
	
	/**
	 * D�connexion de tout
	 */
	public void dispose()
	{
		webinfos.resetToDo();
		system.disconnect();
		if( ftp != null){
			ftp.disconnect();
		}
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if( args.length != 8 ){
			System.out.println("Erreur : il manque des param�tres (8) " + args.length);
			System.exit(-1);
		}
		
		Installation inst = new Installation( args[0].charAt(0), args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
		inst.treatment();
		inst.dispose();

		System.out.println( inst.getMsgError() + "\n" );
	}

}
