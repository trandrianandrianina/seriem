unit InstallSiteDistant;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,FileCtrl, ShellApi, ComObj, Registry, ShlObj, ActiveX, ZipMstr,
  ComCtrls, ExtCtrls;

type
  ShortcutType = (_DESKTOP, _QUICKLAUNCH, _SENDTO, _STARTMENU);

  TF_InstallSD = class(TForm)
    BT_Installer: TButton;
    GroupBox1: TGroupBox;
    BT_Modifier: TButton;
    L_Chemin: TLabel;
    BT_Quitter: TButton;
    OD_Parcourir: TOpenDialog;
    ZM_Module: TZipMaster;
    PB_Transfert: TProgressBar;
    L_Status: TLabel;
    P_Menu: TPanel;
    BT_Doc: TButton;
    BT_Install: TButton;
    BT_QuitterP: TButton;
    L_Copyright: TLabel;
    Image1: TImage;
    BT_EIS2: TButton;
    BT_MajSRM: TButton;
    procedure BT_QuitterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BT_ModifierClick(Sender: TObject);
    procedure BT_InstallerClick(Sender: TObject);
    procedure ZM_ModuleProgress(Sender: TObject; ProgrType: ProgressType;
      Filename: String; FileSize: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure BT_QuitterPClick(Sender: TObject);
    procedure BT_InstallClick(Sender: TObject);
    procedure BT_DocClick(Sender: TObject);
    procedure BT_EIS2Click(Sender: TObject);
    procedure BT_MajSRMClick(Sender: TObject);
  private
    VersionSaj : String;
    MajSite : boolean;
    repertoire : string;
    { D�clarations priv�es }
    procedure CreateShortcut(titre: string; SourceFileName: string; Location: ShortcutType; SubDirectory : string);
    function CreateFolder(aFolderName: string; aLocation: integer): boolean;
    procedure EraseKey(key : string);
    function RegRead (Mykey,MyField : String) : string;
    procedure RegWrite (Mykey,MyField,MyValue : String);
    function ChooseFolder(const Title: string): string;
    function CopierFichier(src, dest : String; conserve : boolean) : boolean;
    function SuppressionDossier : boolean;
    function Deconcat(archive:string; repdest:string) : integer;
    function Dezippage(nomfichier, nomdossier : string) : boolean;
    function ExecuteFile(const FileName, Params, DefaultDir: string):THandle;
    function SuppressionFichier(nomdossier : String) : boolean;
  public
    { D�clarations publiques }
    function ROFichier(nomdossier : string) : boolean;
  end;

var
  F_InstallSD: TF_InstallSD;

implementation

uses informations;

{$R *.DFM}

//------------------------------------------------------------------------------
//--> Cr�� un groupe dans la barre de t�ches
//------------------------------------------------------------------------------
procedure TF_InstallSD.CreateShortcut(titre: string; SourceFileName: string; Location: ShortcutType; SubDirectory : string);
var
  MyObject : IUnknown;
  MySLink : IShellLink;
  MyPFile : IPersistFile;
  Directory, LinkName : string;
  WFileName : WideString;
  MyReg, QuickLaunchReg : TRegIniFile;
begin
  MyObject := CreateComObject(CLSID_ShellLink);
  MySLink := MyObject as IShellLink;
  MyPFile := MyObject as IPersistFile;
  MySLink.SetPath(PChar(SourceFileName));
  MyReg := TRegIniFile.Create('Software\MicroSoft\Windows\CurrentVersion\Explorer');
  try
    LinkName := ChangeFileExt(titre, '.lnk');
    LinkName := ExtractFileName(LinkName);
    case Location of
    _DESKTOP : Directory := MyReg.ReadString('Shell Folders', 'Desktop', '');
    _STARTMENU : Directory := MyReg.ReadString('Shell Folders', 'Start Menu', '');
    _SENDTO : Directory := MyReg.ReadString('Shell Folders', 'SendTo', '');
    _QUICKLAUNCH:
    begin
     QuickLaunchReg := TRegIniFile.Create('Software\MicroSoft\Windows\CurrentVersion\GrpConv');
     try
      Directory := QuickLaunchReg.ReadString('MapGroups', 'Quick Launch', '');
      finally
      QuickLaunchReg.Free;
      end;
   end;
  end;
   if Directory <> '' then begin
       if SubDirectory <> '' then
           WFileName := Directory + '\'+ SubDirectory +'\' + LinkName
       else
           WFileName := Directory + '\' + LinkName;
       MyPFile.Save(PWChar(WFileName), False);
   end;
   finally
       MyReg.Free;
  end;
end;

//------------------------------------------------------------------------------
//--> Cr�� un groupe dans la barre de t�ches
//------------------------------------------------------------------------------
function TF_InstallSD.CreateFolder(aFolderName: string; aLocation: integer): boolean;
var
  pIdl: PItemIDList;
  hPath: PChar;
begin
  Result := false;
  if SUCCEEDED(SHGetSpecialFolderLocation(0, aLocation, pidl)) then begin
    hPath := StrAlloc(max_path);
    SHGetPathFromIDList(pIdl, hPath);
    SetLastError(0);
    CreateDirectory(PChar(hPath + '\\' + aFolderName), nil );
    if (GetLastError=0) or (GetLastError=ERROR_ALREADY_EXISTS) then
        Result := true;
        StrDispose(hPath);
  end;
end;

//------------------------------------------------------------------------------
//--> Efface une cl� dans la base de registre
//------------------------------------------------------------------------------
procedure TF_InstallSD.EraseKey(key : string);
var
  Registre : TRegistry;
begin
    Registre:=TRegistry.Create;
    try
        Registre.RootKey:=HKEY_LOCAL_MACHINE;
        Registre.DeleteKey(key);
        Registre.Free;
    except
        Showmessage('Erreur lors destructions informations dans la base de registre.');
        Registre.Free;
    end;
end;

//------------------------------------------------------------------------------
//--> Lit une chaine dans la base de registre
//------------------------------------------------------------------------------
function TF_InstallSD.RegRead (Mykey,MyField : String) : string;
begin
  //Create the Object
  with (TRegistry.Create) do begin
    //Sets the destination for our requests
    RootKey:=HKEY_LOCAL_MACHINE;
    //Check if whe can open our key, if the key dosn't exist, we create it
    if OpenKey(MyKey, false) then begin
      //Is our field availbe
      if ValueExists(MyField) then
          //Read the value from the field
          result := ReadString(MyField)
      else
          //ShowMessage(MyField+' does not exists under '+MyKey);
          result := ''; end
    else
        //There is a big error if we get an errormessage by
        //opening/creating the key
        result := '(Cl� non trouv�)';
  end;
end;

//------------------------------------------------------------------------------
//--> Ecrit une chaine dans la base de registre
//------------------------------------------------------------------------------
procedure TF_InstallSD.RegWrite (Mykey,MyField,MyValue : String);
begin
    //Create the Object
    with (TRegistry.Create) do begin
        //Sets the destination for our requests
        RootKey:=HKEY_LOCAL_MACHINE;
        //Check if we can open our key, if the key doesn't exist, we create it
        if OpenKey(MyKey,true) then
            //We don't need to check if the field is available because the
            //field is created by writing the value
            Writestring(MyField,MyValue)
        else
            //There is a big error if we gets an errormessage by
            //opening/creating the key
            ShowMessage('Error opening/creating key : '+MyKey);
    end;
end;

//------------------------------------------------------------------------------
//--> Affiche la boite de Windows Selectionner Dossier
//------------------------------------------------------------------------------
function TF_InstallSD.ChooseFolder(const Title: string): string;
var
 Path: array [0..MAX_PATH] of char;
 pidl: PItemIDList;
 bi: TBrowseInfo;
begin
 result := '';
 with bi do begin
   hwndOwner := Handle;
   pidlRoot  := nil;
   pszDisplayName := Path;
   lpszTitle := PChar(Title);
   ulFlags := BIF_VALIDATE or BIF_RETURNONLYFSDIRS;
   lpfn := nil;
   lParam := 0;
 end;
 pidl := ShBrowseForFolder(bi);
 if (pidl <> nil) and (SHGetPathFromIDList(pidl, path)) then
   result := path;
end;

//------------------------------------------------------------------------------
//--> Copie un fichier
//-->     src : chemin + nom du fichier a copier
//-->     dest : chemin + nom du fichier une fois copi�
//-->     conserve : False si l'on veut ecraser un fichier du m�me nom
//--> En sortie:
//-->     True : si �a c'est bien pass�, False sinon
//------------------------------------------------------------------------------
function TF_InstallSD.CopierFichier(src, dest : String; conserve : boolean) : boolean;
var
    retour : boolean;
    lpMsgBuf : PChar;
    srcn: array[0..256] of Char;
    destn: array[0..256] of Char;
begin
    StrPCopy(srcn, src);
    StrPCopy(destn, dest);
    retour := CopyFile(srcn, destn, conserve);
    if retour = True then
        CopierFichier := True
    else begin
        GetMem(lpMsgBuf, 1000);
        FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil,GetLastError, FORMAT_MESSAGE_FROM_STRING, lpMsgBuf, 1000, nil);
        MessageDlg(lpMsgBuf, mtError, [mbOk], 0);
        FreeMem(lpMsgBuf);
        CopierFichier := False;
    end;
end;

//------------------------------------------------------------------------------
//--> Quitte l'application
//------------------------------------------------------------------------------
procedure TF_InstallSD.BT_QuitterClick(Sender: TObject);
begin
    Application.Terminate;
end;

//------------------------------------------------------------------------------
//--> Enleve le ReadOnly des fichiers
//-->     nomdossier : nom du dossier
//--> En sortie:
//-->     True : si �a c'est bien pass�, False sinon
//------------------------------------------------------------------------------
function TF_InstallSD.ROFichier(nomdossier : string) : boolean;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
begin
    retour := FindFirst(nomdossier + '\*.*', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            // On s'assure que les fichiers ne soient pas en lecture seule
            attrs := FileGetAttr(nomdossier + '\'+listef.name);
            if attrs and faReadOnly <> 0 then
                FileSetAttr(nomdossier + '\'+listef.name, attrs - faReadOnly);
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);

    ROFichier := ret;
end;

//------------------------------------------------------------------------------
//--> Supprime le contenu d'un dossier
//-->     nomdossier : nom du dossier dont il faut supprimer les fichiers
//--> En sortie:
//-->     True : si �a c'est bien pass�, False sinon
//------------------------------------------------------------------------------
function TF_InstallSD.SuppressionFichier(nomdossier : string) : boolean;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
begin
    retour := FindFirst(nomdossier + '\*.*', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            // On s'assure que les fichiers ne soient pas en lecture seule
            attrs := FileGetAttr(nomdossier + '\'+listef.name);
            if attrs and faReadOnly <> 0 then
                FileSetAttr(nomdossier + '\'+listef.name, attrs - faReadOnly);
            // Puis on les d�truit
            if (DeleteFile(nomdossier+'\'+listef.name) = False) then begin
                MessageDlg('Impossible de supprimer '+listef.name, mtError, [mbOk], 0);
                ret := False;
            end;
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);

    SuppressionFichier := ret;
end;

//------------------------------------------------------------------------------
//--> En sortie:
//-->     True : si �a c'est bien pass�, False sinon
//------------------------------------------------------------------------------
function TF_InstallSD.SuppressionDossier : boolean;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
begin
    retour := FindFirst('c:\tempsavf\*.*', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            // On s'assure que les fichiers ne soient pas en lecture seule
            attrs := FileGetAttr('c:\tempsavf\'+listef.name);
            if attrs and faReadOnly <> 0 then
                FileSetAttr('c:\tempsavf\'+listef.name, attrs - faReadOnly);
            // Puis on les d�truit
            if (DeleteFile('c:\tempsavf\'+listef.name) = False) then begin
                MessageDlg('Impossible de supprimer '+listef.name, mtError, [mbOk], 0);
                ret := False;
            end;
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);
    ChDir('c:\');
    ret := RemoveDir('c:\tempsavf');

    SuppressionDossier := ret;
end;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Proc�dure de "d�concat�nation"
//           archive : chemin+nom du fichier � d�compresser
//           repdest : chemin du repertoire ou seront cr��s les fichiers
//                     de l'archive (sans le "\" final)
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function TF_InstallSD.Deconcat(archive:string; repdest:string) : integer;
var
ms,msWrite : TMemoryStream;
buf : array of Byte;
iFileLength,i,j,k, PosMarkDeb, PosMarkFin, PosMarkSuiv : Integer;
marqueur, nomFic: String;
test : Boolean;
nbrfic : integer;
begin
     Screen.Cursor:= CrHourGlass;
     PB_Transfert.Max := 10;
     PB_Transfert.Step := 1;
     nbrfic := 0;
     marqueur := '???!!!#/\';
     ms := TMemoryStream.Create;
 try
     ms.LoadFromfile(archive);
     buf := @ms.memory^;


    //gestion du r�pertoire de destination
    if not DirectoryExists(repdest) then
           if not CreateDir(repdest) then
              raise Exception.Create('Impossible de cr�er '+ repdest);
    ifileLength := ms.size;
    k := 0;
    i := 0;
    while i< ifileLength do begin
        test := false;
        if (buf[i] = 63)and (buf[i+1] = 63) and (buf[i+2] = 63) and (buf[i+3] = 33) and (buf[i+4] = 33)and (buf[i+5] = 33)and (buf[i+6] = 35)and (buf[i+7] = 47)and (buf[i+8] = 92) then
           test := true;
        if (test) then
           inc(k);

        if (k=1)and(test) then
           PosMarkDeb := i;
        if (k=2)and(test) then
           PosMarkFin := i;
        if (k=3)and(test) then
           PosMarkSuiv := i;

        if (k=3) and (test) Then
        begin
             //recup le nom du fichier
             for j := PosMarkDeb + 9 to PosMarkFin - 1  do
                 nomfic := nomfic + Chr(Buf[j]);

             //Ecriture des donn�es
             msWrite := TMemoryStream.Create;
             for j := PosMarkFin+9 to PosMarkSuiv-1 do
                  msWrite.Write(buf[j],1);
             msWrite.SaveToFile(repdest+'\'+nomfic);
             nbrfic := nbrfic + 1;
             if PB_Transfert.position >= 10 then
                 PB_Transfert.position := 0;
             PB_Transfert.stepIt;
             Repaint;
             msWrite.Free;

             k := 0;
             nomfic := '';
             dec(i);
        end;

        Inc(i);
    end;
    //recup le nom du fichier
    for j := PosMarkDeb + 9 to PosMarkFin - 1  do
        nomfic := nomfic + Chr(Buf[j]);

    //Ecriture des donn�es
    msWrite := TMemoryStream.Create;
    try
       For j := PosMarkFin+9 to iFileLength-1 do
           msWrite.Write(buf[j],1);
       msWrite.SaveToFile(repdest+'\'+nomfic);
       nbrfic := nbrfic + 1;
       if PB_Transfert.position >= 10 then
           PB_Transfert.position := 0;
       PB_Transfert.stepIt

    finally
       msWrite.Free;
    end;
 finally
    buf := nil;
    ms.free;
 end;
 Screen.Cursor:= CrDefault;
 Deconcat := nbrfic;
end;

//------------------------------------------------------------------------------
//--> D�concat�ne et d�zippe un fichier
//-->     nomfichier : nom du fichier � d�zipper
//--> En sortie:
//-->     True : si �a c'est bien pass�, False sinon
//------------------------------------------------------------------------------
function TF_InstallSD.Dezippage(nomfichier, nomdossier : string) : boolean;
var
    nbrfichier : integer;
    listef: TSearchRec;
    i, j, retour, attrs : integer;
    ret : boolean;
    chaine : String;
    F : TextFile;
begin
    // Cr�ation du r�pertoire temporaire
    if not DirectoryExists('C:\tempsavf') then
        if not CreateDir('C:\tempsavf') then begin
            raise Exception.Create('Impossible de cr�er c:\tempsavf');
            Dezippage := false;
        end;

    // D�concat�nation
    PB_Transfert.Position := 0;
    L_Status.Caption := 'Extraction en cours ...';
    L_Status.refresh;
    nbrfichier := Deconcat(nomfichier, 'C:\tempsavf');
    L_Status.Caption := 'Extraction termin�e';
    // On v�rifie que le SAJ ne soit pas un vieux fichier qui trainait
    if not FileExists('C:\tempsavf\version.txt') then begin
        ShowMessage('Fichier version non trouv� dans le fichier SAJ.');
        DeleteFile(nomfichier);
        Dezippage := False;
        Exit; end
    else begin
        AssignFile(F, 'C:\tempsavf\version.txt');
        Reset(F);
        Readln(F, chaine);
        CloseFile(F);
        if Trim(chaine) <> VersionSaj then begin
            ShowMessage('La version du fichier SAJ ne correspond pas � celle du si�ge.');
            DeleteFile(nomfichier);
            Dezippage := False;
            Exit;
        end;
    end;


    // D�compression
    L_Status.Caption := 'D�compression en cours ...';
    PB_Transfert.Max := nbrfichier;
    PB_Transfert.Position := 0;
    PB_Transfert.Step := 1;
    ROFichier(nomdossier);
    retour := FindFirst('c:\tempsavf\*.zip', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') then begin
            ZM_Module.ZipFileName:= 'C:\tempsavf\'+listef.name;
            ZM_Module.FSpecArgs.Add('*.*');
            ZM_Module.ExtrBaseDir := nomdossier;
            ZM_Module.ExtrOptions := ZM_Module.ExtrOptions+[ExtrOverwrite];
            ZM_Module.Extract;
            PB_Transfert.stepIt;
            DeleteFile('c:\tempsavf\'+listef.name);
            Dec(nbrfichier);
            L_Status.Caption := 'Fichier(s) restant(s): ' + IntToStr(nbrfichier);
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);
    SuppressionDossier;
    L_Status.Caption := 'D�compression termin�e';

    Dezippage := True;
end;

//------------------------------------------------------------------------------
//--> Lance une application
//------------------------------------------------------------------------------
function TF_InstallSD.ExecuteFile(const FileName, Params, DefaultDir: string):THandle;
   var
    zFileName, zParams, zDir: array[0..79] of Char;
begin
  Result := ShellExecute(Application.Mainform.Handle, nil ,
  StrPCopy(zFileName, FileName), StrPCopy(zParams,Params),
  StrPCopy(zDir, DefaultDir), SW_SHOW);
end ;


//------------------------------------------------------------------------------
//--> Proc�dure de d�marrage
//------------------------------------------------------------------------------
procedure TF_InstallSD.FormCreate(Sender: TObject);
begin
    repertoire := GetCurrentDir;
    L_Status.Caption := '';
    //ZM_Module.Load_Zip_Dll;
    //ZM_Module.Load_Unz_Dll;

    // Y a t il des param�tres ??
    if ParamCount >= 1 then begin
        VersionSaj := Trim(ParamStr(1));
        MajSite := True;
        P_Menu.Visible := False; end
    else
        MajSite := False;

    // Lecture de la base de registre
    L_Chemin.Caption := RegRead('SOFTWARE\RI\vgm', 'cheminSD');
    if (L_Chemin.Caption = '(Cl� non trouv�)') or (Trim(L_Chemin.Caption) = '') then
        L_Chemin.Caption := 'c:\vgm';
end;

//------------------------------------------------------------------------------
//--> Modification du chemin destination des RunTimes
//------------------------------------------------------------------------------
procedure TF_InstallSD.BT_ModifierClick(Sender: TObject);
var
    chemin : string;
begin
    chemin := Trim(ChooseFolder('S�lection du dossier'));
    if chemin <> '' then
        L_Chemin.Caption := chemin;
end;

//------------------------------------------------------------------------------
//--> Installation des RunTimes
//------------------------------------------------------------------------------
procedure TF_InstallSD.BT_InstallerClick(Sender: TObject);
var
    Attributes : word;
    sr : TSearchRec;
    retour, i : Integer;
    chsrc : string;

begin
    BT_Quitter.Enabled := False;
    BT_Installer.Enabled := False;
    chsrc := 'vgm';

    // On met � jour la base de registre
    RegWrite('SOFTWARE\RI\vgm', 'cheminSD', L_Chemin.Caption);

    // Dans le cas d'une mise � jour depuis le site
    if MajSite then begin
        // Recherche du dossier de mise � jour
        if not FileExists(L_Chemin.Caption + '\aide.csh') then begin
            ShowMessage('Dossier de mise � jour introuvable.');
            BT_Quitter.Enabled := True;
            BT_Installer.Enabled := True;
            Exit;
        end;
        // Recherche du fichier SAJ
        OD_Parcourir.Title := 'Chercher le fichier SAJ';
        OD_Parcourir.Execute;
        if Trim(OD_Parcourir.FileName) <> '' then begin
            SetCurrentDir(repertoire);
            // Extraction des fichiers du ZIP
            if Dezippage(OD_Parcourir.FileName, L_Chemin.Caption) = false then begin
                ShowMessage('Probl�me lors de l''extraction des fichiers du SAJ.');
                SuppressionDossier;
                BT_Quitter.Enabled := True;
                BT_Installer.Enabled := True;
                Exit;
            end;
            // Suppression du fichier SAJ
            if MessageDlg('Mise � jour termin�e, souhaitez vous supprimer le fichier SAJ ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
                DeleteFile(OD_Parcourir.FileName);
            end
        else begin
            ShowMessage('Vous devez s�lectionner le fichier zip pour commencer la mise � jour.');
            BT_Quitter.Enabled := True;
            BT_Installer.Enabled := True;
            Exit;
        end; end
    else begin
    // Dans le cas d'une mise � jour depuis CD
      if not DirectoryExists(L_Chemin.Caption) then begin
        // Cr�ation du dossier
        if not CreateDir(L_Chemin.Caption) then begin
            ShowMessage('Impossible de cr�er le dossier');
            BT_Quitter.Enabled := True;
            BT_Installer.Enabled := True;
            Exit;
        end;
      end;
      // Copie des fichiers
      L_Status.Caption := 'Suppression des fichiers ...';
      SuppressionFichier(L_Chemin.Caption);
      L_Status.Caption := 'Copie des fichiers en cours...';
      Screen.Cursor:= CrHourGlass;
      ZM_Module.ZipFileName:= repertoire + '\vgm.zip';
      ZM_Module.FSpecArgs.Add('*.*');
      ZM_Module.ExtrBaseDir := L_Chemin.Caption;
      ZM_Module.ExtrOptions := ZM_Module.ExtrOptions+[ExtrOverwrite];
      ZM_Module.Extract;
      Screen.Cursor:= CrDefault;
      L_Status.Caption := 'Copie termin�e.';
      ShowMessage('Installation des Runtimes termin�e.');
      BT_Quitter.Enabled := True;
      Exit;
    end;
(*
// **** Ne sert � rien ****
    // Copie des fichiers
    L_Status.Caption := 'Suppression des fichiers ...';
    SuppressionFichier(L_Chemin.Caption);
    L_Status.Caption := 'Copie des fichiers ...';
    repaint;
    retour := FindFirst(chsrc+'\*', faAnyFile, sr);
    i := 0;
    while retour = 0 do begin
        if (sr.name <> '.') and (sr.name <> '..') then
            Inc(i);
        retour := FindNext(sr);
    end;
    PB_Transfert.Step := 1;
    PB_Transfert.Max := i;
    retour := FindFirst(chsrc+'\*', faAnyFile, sr);
    while retour = 0 do begin
        if (sr.name <> '.') and (sr.name <> '..') then begin
            if CopierFichier(chsrc+'\' + sr.name, L_Chemin.Caption + '\' + sr.name, False) = False then begin
                ShowMessage('Probl�me lors de la copie des fichiers.');
                BT_Quitter.Enabled := True;
                BT_Installer.Enabled := True;
                Exit;
            end;
            Attributes := FileGetAttr(L_Chemin.Caption + '\' + sr.name);
            if Attributes and faReadOnly <> 0 then
                FileSetAttr(L_Chemin.Caption + '\' + sr.name, Attributes and not faReadOnly);
            PB_Transfert.stepIt;
        end;
        retour := FindNext(sr);
    end;
    FindClose(sr);
    L_Status.Caption := 'Copie termin�e.';
    end;
// **** Ne sert � rien **** (fin)
*)

    ShowMessage('Installation des Runtimes termin�e.');
    BT_Quitter.Enabled := True;
end;

//------------------------------------------------------------------------------
//--> Gestion des barres de progression
//------------------------------------------------------------------------------
procedure TF_InstallSD.ZM_ModuleProgress(Sender: TObject;
  ProgrType: ProgressType; Filename: String; FileSize: Integer);
begin
   case ProgrType of
        TotalFiles2Process: begin
                                //PB_Transfert.Max := FileSize;
                                //PB_Transfert.Step := 1;
                            end;
        NewFile:            begin
                                //PB_Transfert.stepIt;
                            end;
    end;

end;

procedure TF_InstallSD.FormDestroy(Sender: TObject);
begin
   //ZM_Module.Unload_Zip_Dll;
   //ZM_Module.Unload_Unz_Dll;
end;

procedure TF_InstallSD.BT_QuitterPClick(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TF_InstallSD.BT_InstallClick(Sender: TObject);
begin
    P_Menu.Visible := False;
end;

procedure TF_InstallSD.BT_DocClick(Sender: TObject);
begin
    ExecuteFile('LODRUN.doc', '', repertoire);
end;

//---------------------
function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam: LPARAM; lpData: LPARAM): Integer; stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) then
    SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);
  BrowseCallbackProc := 0;
end;

function GetFolderDialog(Handle: Integer; Caption: string; var strFolder: string): Boolean;
const
  BIF_STATUSTEXT           = $0004;
  BIF_NEWDIALOGSTYLE       = $0040;
  BIF_RETURNONLYFSDIRS     = $0080;
  BIF_SHAREABLE            = $0100;
  BIF_USENEWUI             = BIF_EDITBOX or BIF_NEWDIALOGSTYLE;

var
  BrowseInfo: TBrowseInfo;
  ItemIDList: PItemIDList;
  JtemIDList: PItemIDList;
  Path: PAnsiChar;
begin
  Result := False;
  Path := StrAlloc(MAX_PATH);
  SHGetSpecialFolderLocation(Handle, CSIDL_DRIVES, JtemIDList);
  with BrowseInfo do
  begin
    hwndOwner := GetActiveWindow;
    pidlRoot := JtemIDList;
    SHGetSpecialFolderLocation(hwndOwner, CSIDL_DRIVES, JtemIDList);

    { return display name of item selected }
    pszDisplayName := StrAlloc(MAX_PATH);

    { set the title of dialog }
    lpszTitle := PChar(Caption);//'Select the folder';
    { flags that control the return stuff }
    lpfn := @BrowseCallbackProc;
    { extra info that's passed back in callbacks }
    lParam := LongInt(PChar(strFolder));
  end;

  ItemIDList := SHBrowseForFolder(BrowseInfo);

  if (ItemIDList <> nil) then
    if SHGetPathFromIDList(ItemIDList, Path) then
    begin
      strFolder := Path;
      Result := True
    end;
end;

//------------------------------------------------------------------------------
procedure TF_InstallSD.BT_EIS2Click(Sender: TObject);
begin
    F_Informations.Show;
end;

procedure TF_InstallSD.BT_MajSRMClick(Sender: TObject);
var
  F: TextFile;
  sr: TSearchRec;
  retour : integer;
  position, i : integer;
  fichier, chemin : string;
begin
    // S�lection du dossire � mettre � jour
    chemin := 'C:\Program Files\Symtrax\StarQuery Excel\srm';
    if not GetFolderDialog(Application.Handle, 'S�lection du dossier de MAJ', chemin) then begin
        ShowMessage('Dossier de MAJ non s�lectionn�.');
        Exit;
    end;

    // Pour chaque fichiers SQX
    retour := FindFirst('.\starquery\*.sqx', faAnyFile, sr);
    while retour = 0 do begin
        CopyFile(PChar('.\starquery\'+sr.name), PChar(chemin + '\' + sr.name), False);
        retour := FindNext(sr);
    end;
    FindClose(sr);

    // Pour chaque fichiers SQM
    retour := FindFirst('.\starquery\*.sqm', faAnyFile, sr);
    while retour = 0 do begin
        CopyFile(PChar('.\starquery\'+sr.name), PChar(chemin + '\' + sr.name), False);
        retour := FindNext(sr);
    end;
    FindClose(sr);

    // Pour chaque fichiers XLS
    retour := FindFirst('.\starquery\*.xls', faAnyFile, sr);
    while retour = 0 do begin
        CopyFile(PChar('.\starquery\'+sr.name), PChar(chemin + '\' + sr.name), False);
        retour := FindNext(sr);
    end;
    FindClose(sr);

    // Enleve l'attribut lecture seule
    ROFichier(chemin);

    ShowMessage('Mise � jour du dossier SRM termin�e.');
end;

end.
