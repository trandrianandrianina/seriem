program InstallSD;

uses
  Forms,
  InstallSiteDistant in 'InstallSiteDistant.pas' {F_InstallSD},
  informations in 'informations.pas' {F_Informations};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TF_InstallSD, F_InstallSD);
  Application.CreateForm(TF_Informations, F_Informations);
  Application.Run;
end.
