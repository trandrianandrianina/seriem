object F_Informations: TF_Informations
  Left = 256
  Top = 52
  Width = 322
  Height = 497
  Caption = 'Informations'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox5: TGroupBox
    Left = 8
    Top = 72
    Width = 297
    Height = 361
    Caption = ' Informations pour StarQuery uniquement'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 6
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 88
    Width = 281
    Height = 81
    Caption = ' Enregistrement '
    TabOrder = 1
    object L_License: TLabel
      Left = 8
      Top = 28
      Width = 37
      Height = 13
      Caption = 'License'
    end
    object L_Company: TLabel
      Left = 8
      Top = 52
      Width = 44
      Height = 13
      Caption = 'Company'
    end
    object E_License: TEdit
      Left = 88
      Top = 24
      Width = 185
      Height = 21
      TabOrder = 0
      OnChange = E_LicenseChange
    end
    object E_Company: TEdit
      Left = 88
      Top = 48
      Width = 185
      Height = 21
      TabOrder = 1
      OnChange = E_CompanyChange
    end
  end
  object GroupBox2: TGroupBox
    Left = 16
    Top = 232
    Width = 281
    Height = 185
    Caption = ' Informations principales '
    TabOrder = 3
    object L_AdrIP: TLabel
      Left = 8
      Top = 20
      Width = 102
      Height = 13
      Caption = 'Adresse(s) IP AS/400'
    end
    object L_Bib: TLabel
      Left = 8
      Top = 102
      Width = 69
      Height = 13
      Caption = 'Biblioth�que(s)'
    end
    object LB_AdrIP: TListBox
      Left = 136
      Top = 24
      Width = 137
      Height = 73
      ItemHeight = 13
      TabOrder = 3
    end
    object LB_Bibliotheque: TListBox
      Left = 136
      Top = 104
      Width = 137
      Height = 73
      ItemHeight = 13
      TabOrder = 7
    end
    object E_SAdrIP: TEdit
      Left = 16
      Top = 40
      Width = 113
      Height = 21
      TabOrder = 0
    end
    object E_SBib: TEdit
      Left = 16
      Top = 120
      Width = 113
      Height = 21
      TabOrder = 4
    end
    object B_AjoutSAdrIP: TButton
      Left = 56
      Top = 64
      Width = 75
      Height = 17
      Caption = 'Ajouter'
      TabOrder = 1
      OnClick = B_AjoutSAdrIPClick
    end
    object B_SupprimerSAdrIP: TButton
      Left = 56
      Top = 80
      Width = 75
      Height = 17
      Caption = 'Supprimer'
      TabOrder = 2
      OnClick = B_SupprimerSAdrIPClick
    end
    object B_AjoutSBib: TButton
      Left = 56
      Top = 144
      Width = 75
      Height = 17
      Caption = 'Ajouter'
      TabOrder = 5
      OnClick = B_AjoutSBibClick
    end
    object B_SupprimerSBib: TButton
      Left = 56
      Top = 160
      Width = 75
      Height = 17
      Caption = 'Supprimer'
      TabOrder = 6
      OnClick = B_SupprimerSBibClick
    end
  end
  object B_Annuler1: TButton
    Left = 232
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Annuler'
    TabOrder = 5
    OnClick = B_Annuler1Click
  end
  object B_Suivant1: TButton
    Left = 152
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Suivant'
    Enabled = False
    TabOrder = 4
    OnClick = B_Suivant1Click
  end
  object GroupBox3: TGroupBox
    Left = 16
    Top = 176
    Width = 281
    Height = 49
    Caption = 'Dossier d'#39'installation'
    TabOrder = 2
    object E_DossierInstall: TEdit
      Left = 16
      Top = 16
      Width = 249
      Height = 21
      TabOrder = 0
      Text = 'C:\Program Files\Symtrax\StarQuery Excel'
    end
  end
  object GroupBox4: TGroupBox
    Left = 8
    Top = 0
    Width = 297
    Height = 65
    Caption = ' Produit(s) � installer '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object CB_StarQuery: TCheckBox
      Left = 8
      Top = 24
      Width = 73
      Height = 17
      Caption = 'StarQuery'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = CB_StarQueryClick
    end
    object CB_MapDesigner: TCheckBox
      Left = 96
      Top = 24
      Width = 89
      Height = 17
      Caption = 'MapDesigner'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object CB_SymOLEDB: TCheckBox
      Left = 200
      Top = 24
      Width = 81
      Height = 17
      Caption = 'SymOLEDB'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
end
