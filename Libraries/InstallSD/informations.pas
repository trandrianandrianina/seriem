unit informations;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ShlObj, ShellApi;

type
  TF_Informations = class(TForm)
    GroupBox1: TGroupBox;
    L_License: TLabel;
    L_Company: TLabel;
    E_License: TEdit;
    E_Company: TEdit;
    GroupBox2: TGroupBox;
    LB_AdrIP: TListBox;
    L_AdrIP: TLabel;
    L_Bib: TLabel;
    LB_Bibliotheque: TListBox;
    B_Annuler1: TButton;
    B_Suivant1: TButton;
    E_SAdrIP: TEdit;
    E_SBib: TEdit;
    B_AjoutSAdrIP: TButton;
    B_SupprimerSAdrIP: TButton;
    B_AjoutSBib: TButton;
    B_SupprimerSBib: TButton;
    GroupBox3: TGroupBox;
    E_DossierInstall: TEdit;
    GroupBox4: TGroupBox;
    CB_StarQuery: TCheckBox;
    CB_MapDesigner: TCheckBox;
    CB_SymOLEDB: TCheckBox;
    GroupBox5: TGroupBox;
    function ExecAppWait(AppName, Params: string): Boolean;
    procedure GenereCFG;
    procedure CopieEIS2;
    procedure B_Annuler1Click(Sender: TObject);
    procedure B_AjoutSAdrIPClick(Sender: TObject);
    procedure B_SupprimerSAdrIPClick(Sender: TObject);
    procedure B_AjoutSBibClick(Sender: TObject);
    procedure B_SupprimerSBibClick(Sender: TObject);
    procedure E_CompanyChange(Sender: TObject);
    procedure E_LicenseChange(Sender: TObject);
    procedure B_Suivant1Click(Sender: TObject);
    procedure CB_StarQueryClick(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

var
  F_Informations: TF_Informations;

implementation

uses InstallSiteDistant;

{$R *.DFM}

//------------------------------------------------------------------------------
//--> Execute une application avec attente de fin
//------------------------------------------------------------------------------
function TF_Informations.ExecAppWait(AppName, Params: string): Boolean;
var
  // Structure containing and receiving info about application to start
  ShellExInfo: TShellExecuteInfo;
begin
      FillChar(ShellExInfo, SizeOf(ShellExInfo), 0);
      with ShellExInfo do begin
        cbSize := SizeOf(ShellExInfo);
        fMask := see_Mask_NoCloseProcess;
        Wnd := Application.Handle;
        lpFile := PChar(AppName);
        lpParameters := PChar(Params);
        nShow := sw_ShowNormal;
      end;
      Result := ShellExecuteEx(@ShellExInfo);
      if Result then
          while WaitForSingleObject(ShellExInfo.HProcess, 1000) = WAIT_TIMEOUT do begin
              Application.ProcessMessages;
              if Application.Terminated then Break;
          end;
end;

procedure TF_Informations.GenereCFG;
var
  F: TextFile;
  sr: TSearchRec;
  retour : integer;
  position, i : integer;
  fichier : string;
begin
    // Pour chaque projets SQX
//    retour := FindFirst('.\starquery\eis2\*.sqx', faAnyFile, sr);
//    while retour = 0 do begin
        // Extraction du nom du projet
//        position := LastDelimiter('.', sr.Name);
//        fichier := Copy(sr.Name, 1, position);
        fichier := 'mapfiles.';

        CreateDir(E_DossierInstall.Text + '\srm');

        // G�n�ration du fichier de config
        AssignFile(F, E_DossierInstall.Text + '\srm\' + fichier + 'cfg');
        ReWrite(F);
        WriteLn(F, '[AdresseIP]');
        for i := 0 to LB_AdrIP.Items.Count - 1 do
            WriteLn(F, 'Value' + IntToStr(i+1) + '=' + LB_AdrIP.Items.Strings[i]);
        WriteLn(F, 'DefaultValue=Value1');
        WriteLn(F, 'AllowUserValue=1');
        WriteLn(F, '');
        WriteLn(F, '[Base]');
        for i := 0 to LB_Bibliotheque.Items.Count - 1 do
            WriteLn(F, 'Value' + IntToStr(i) + '=' + LB_Bibliotheque.Items.Strings[i]);
        WriteLn(F, 'DefaultValue=Value1');
        WriteLn(F, 'AllowUserValue=1');
        CloseFile(F);
//        retour := FindNext(sr);
//    end;
//    FindClose(sr);
end;

procedure TF_Informations.CopieEIS2;
var
  F: TextFile;
  sr: TSearchRec;
  retour : integer;
  position, i : integer;
  fichier : string;
begin
    // Pour chaque fichiers SQX
    retour := FindFirst('.\starquery\*.sqx', faAnyFile, sr);
    while retour = 0 do begin
        CopyFile(PChar('.\starquery\'+sr.name), PChar(E_DossierInstall.Text + '\srm\' + sr.name), False);
        retour := FindNext(sr);
    end;
    FindClose(sr);

    // Pour chaque fichiers SQM
    retour := FindFirst('.\starquery\*.sqm', faAnyFile, sr);
    while retour = 0 do begin
        CopyFile(PChar('.\starquery\'+sr.name), PChar(E_DossierInstall.Text + '\srm\' + sr.name), False);
        retour := FindNext(sr);
    end;
    FindClose(sr);

    // Pour chaque fichiers XLS
    retour := FindFirst('.\starquery\*.xls', faAnyFile, sr);
    while retour = 0 do begin
        CopyFile(PChar('.\starquery\'+sr.name), PChar(E_DossierInstall.Text + '\srm\' + sr.name), False);
        retour := FindNext(sr);
    end;
    FindClose(sr);
end;

procedure TF_Informations.B_Annuler1Click(Sender: TObject);
begin
    Close;
end;

procedure TF_Informations.B_AjoutSAdrIPClick(Sender: TObject);
begin
    LB_AdrIP.Items.Add(Trim(E_SAdrIP.Text));
    E_SAdrIP.Text := '';
end;

procedure TF_Informations.B_SupprimerSAdrIPClick(Sender: TObject);
begin
    LB_AdrIP.Items.Delete(LB_AdrIP.ItemIndex);
end;

procedure TF_Informations.B_AjoutSBibClick(Sender: TObject);
begin
    LB_Bibliotheque.Items.Add(Uppercase(Trim(E_SBib.Text)));
    E_SBib.Text := '';
end;

procedure TF_Informations.B_SupprimerSBibClick(Sender: TObject);
begin
    LB_Bibliotheque.Items.Delete(LB_Bibliotheque.ItemIndex);
end;

procedure TF_Informations.E_CompanyChange(Sender: TObject);
begin
    if (CB_StarQuery.Checked = True) and ((Trim(E_License.Text) = '') or (Trim(E_Company.Text) = '')) then
        B_Suivant1.Enabled := False
    else
        B_Suivant1.Enabled := True;
end;

procedure TF_Informations.E_LicenseChange(Sender: TObject);
begin
    if (CB_StarQuery.Checked = True) and ((Trim(E_License.Text) = '') or (Trim(E_Company.Text) = '')) then
        B_Suivant1.Enabled := False
    else
        B_Suivant1.Enabled := True;
end;

procedure TF_Informations.B_Suivant1Click(Sender: TObject);
var
    parametre : String;
    chaine : String;
begin
     if CB_StarQuery.Checked = True then begin
       // V�rification avant continuation
       if Trim(LB_AdrIP.Items.Text) = '' then begin
           ShowMessage('Veuillez saisir au moins une adresse IP.');
           Exit;
       end;
       if Trim(LB_Bibliotheque.Items.Text) = '' then begin
           ShowMessage('Veuillez saisir au moins un nom de biblioth�que.');
           Exit;
       end;
     end;

     // On verrouille le bouton Suivant
     B_Suivant1.Enabled := False;

     // Installation de Starquery
     if CB_StarQuery.Checked = True then begin
       parametre := '/s /path="' + Trim(E_DossierInstall.Text) + '" /AllowVBAccess /license=' + Trim(E_License.Text) + ' /company="' + Trim(UpperCase(E_Company.Text)) + '"';
       if ExecAppWait('.\starquery\starquery_fr.exe', parametre) = False then
           ShowMessage('Programme non trouv� sur le CD.');

       // On g�n�re le point CFG du ou des projets
       GenereCFG;
       CopieEIS2;
       F_InstallSD.ROFichier(Trim(E_DossierInstall.Text) + '\srm');
     end;

     // Installation du MapDesigner
     if CB_MapDesigner.Checked = True then begin
       if ExecAppWait('.\starquery\mapdesigner_fr.exe', '') = False then
           ShowMessage('Programme non trouv� sur le CD.');
     end;

     // Installation du SymOleDB
     if CB_SymOleDB.Checked = True then begin
       chaine := GetCurrentDir;
       SetCurrentDir(chaine + '\starquery');
       if ExecAppWait('SymOleDB_as.exe', '') = False then
           ShowMessage('Programme non trouv� sur le CD.');
       SetCurrentDir(chaine);
     end;

     // On ferme la fenetre
     Close;
end;

procedure TF_Informations.CB_StarQueryClick(Sender: TObject);
begin
    if CB_StarQuery.Checked then begin
        L_License.Enabled := True;
        E_License.Enabled := True;
        L_Company.Enabled := True;
        E_Company.Enabled := True;
        E_DossierInstall.Enabled := True;
        L_AdrIP.Enabled := True;
        E_SAdrIP.Enabled := True;
        B_AjoutSAdrIP.Enabled := True;
        B_SupprimerSAdrIP.Enabled := True;
        LB_AdrIP.Enabled := True;
        L_Bib.Enabled := True;
        E_SBib.Enabled := True;
        B_AjoutSBib.Enabled := True;
        B_SupprimerSBib.Enabled := True;
        LB_Bibliotheque.Enabled := True;
    end
    else begin
        L_License.Enabled := False;
        E_License.Enabled := False;
        L_Company.Enabled := False;
        E_Company.Enabled := False;
        E_DossierInstall.Enabled := False;
        L_AdrIP.Enabled := False;
        E_SAdrIP.Enabled := False;
        B_AjoutSAdrIP.Enabled := False;
        B_SupprimerSAdrIP.Enabled := False;
        LB_AdrIP.Enabled := False;
        L_Bib.Enabled := False;
        E_SBib.Enabled := False;
        B_AjoutSBib.Enabled := False;
        B_SupprimerSBib.Enabled := False;
        LB_Bibliotheque.Enabled := False;
    end;
    E_CompanyChange(sender);
    E_LicenseChange(sender);
end;

end.
