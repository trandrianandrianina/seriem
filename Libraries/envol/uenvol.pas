// A trouver le moyen de piloter Outlook afin d'avoir la confirmation de lecture

// autre methode
//pCh := 'mailto:mshkolnik@scalabium.com?subject=your_subject&body=your_body&file="c:\autoexec.bat"';
//  ShellExecute(0, 'open', pCh, nil, nil, SW_SHOWNORMAL);

unit uenvol;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleServer, StdCtrls, ComObj, DdeMan;

type
  TForm1 = class(TForm)
    Mailer: TDdeServerConv;
    donnees: TDdeServerItem;
    ST_Transfert: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure donneesChange(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

const
    olMailItem = 0;
    olOriginator = 0;
    olFolderInbox = $00000006;
    olTo = 1;
    olCC = 2;
    olBCC = 3;

var
  Form1: TForm1;
  indice : integer;
  dest, sujet : ansistring;
  wcc1, wcc2, wcc3, wcc4 : ansistring;
  mes01, mes02, mes03, mes04, mes05, mes06, mes07, mes08, mes09, mes10,
  mes11, mes12, mes13, mes14, mes15, mes16, mes17, mes18, mes19, mes20, mes21, mes22: ansistring;
  affichage : boolean;
  importance : integer;
  joints : array [1..5] of string;

implementation

{$R *.DFM}

procedure Log(texte : string);
var
    F : TextFile;
begin
    AssignFile(F, '..\log.txt');
    if not FileExists('..\log.txt') then
        Rewrite(F)
    else
        Append(F);
    Writeln(F, texte);
    Flush(F);
    CloseFile(F);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    indice := 0;
    affichage := False;
    ST_Transfert.Caption := 'Transfert en cours ...';
end;

procedure TForm1.donneesChange(Sender: TObject);
var
  Outlook, Recipient: OleVariant;
  NameSpace, Folder : OleVariant;
  vMailItem: variant;
  i, j: integer;
  chaine : string;
  mes : ansistring;
  position : integer;
begin
    //Log(donnees.Text);
    position := LastDelimiter('|', donnees.Text);
    chaine := Copy(donnees.Text, position+1, Length(donnees.Text)-position);
    Inc(indice);
    case indice of
    1 : dest := chaine;
    2 : wcc1 := chaine;
    3 : wcc2 := chaine;
    4 : wcc3 := chaine;
    5 : wcc4 := chaine;
    6 : sujet := chaine;
    7 : mes01 := chaine;
    8 : mes02 := chaine;
    9 : mes03 := chaine;
   10 : mes04 := chaine;
   11 : mes05 := chaine;
   12 : mes06 := chaine;
   13 : mes07 := chaine;
   14 : mes08 := chaine;
   15 : mes09 := chaine;
   16 : mes10 := chaine;
   17 : mes11 := chaine;
   18 : mes12 := chaine;
   19 : mes13 := chaine;
   20 : mes14 := chaine;
   21 : mes15 := chaine;
   22 : mes16 := chaine;
   23 : mes17 := chaine;
   24 : mes18 := chaine;
   25 : mes19 := chaine;
   26 : mes20 := chaine;
   27 : mes21 := chaine;
   28 : mes22 := chaine;
   29 : joints[1] := chaine;
   30 : joints[2] := chaine;
   31 : joints[3] := chaine;
   32 : joints[4] := chaine;
   33 : joints[5] := chaine;
   34 : importance := StrToInt(Trim(chaine));
   35 : if chaine = 'TRUE' then
            affichage := true
        else
            affichage := false;
    end;

    if indice = 35 then begin
        mes := mes01+#10+mes02+#10+mes03+#10+mes04+#10+mes05+#10+mes06+#10+mes07+#10+mes08+#10+mes09+#10+mes10+#10+mes11+#10+mes12+#10+mes13+#10+mes14+#10+mes15+#10+mes16+#10+mes17+#10+mes18+#10+mes19+#10+mes20+#10+mes21+#10+mes22;
//showmessage(mes);
        try
            Outlook := GetActiveOleObject('Outlook.Application');
        except
            Outlook := CreateOleObject('Outlook.Application');
        end;

        NameSpace := Outlook.GetNamespace('MAPI');
        NameSpace.Logon('', '', False, False);
        Folder := NameSpace.GetDefaultFolder(olFolderInbox);
        Folder.Display;

        try
            vMailItem := Outlook.CreateItem(olMailItem);
        except
            On E : Exception do ShowMessage(E.Message);
        end;
        Recipient := vMailItem.Recipients.Add(dest);
        Recipient.Type := olTo;
        if Trim(wcc1) <> '' then begin
            Recipient := vMailItem.Recipients.Add(wcc1);
            Recipient.Type := olCC;
        end;
        if Trim(wcc2) <> '' then begin
            Recipient := vMailItem.Recipients.Add(wcc2);
            Recipient.Type := olCC;
        end;
        if Trim(wcc3) <> '' then begin
            Recipient := vMailItem.Recipients.Add(wcc3);
            Recipient.Type := olCC;
        end;
        if Trim(wcc4) <> '' then begin
            Recipient := vMailItem.Recipients.Add(wcc4);
            Recipient.Type := olCC;
        end;
        vMailItem.Subject := sujet;
        vMailItem.Body := TrimRight(mes) + #10 + #10;
        vMailItem.Importance := importance; //(0, 1, 2 = low, nor, high)
        i := 1;
        while joints[i] <> '' do begin
            if FileExists(joints[i]) then
                vMailItem.Attachments.Add(joints[i])
            else
                ShowMessage(joints[i] + ' -> Fichier non trouv�.');
            i := i + 1;
        end;
        vMailItem.Display(affichage);
        vMailItem.Send;
        ST_Transfert.Caption := 'Transfert termin�.';
        NameSpace.Logoff;
        //Outlook.Quit;
        Outlook := Unassigned;
        VarClear(Outlook);
        Application.Terminate;
    end;
end;

end.
