// Lance le crunch lors de la sauvegarde
// Param�tre:
//    entree : le chemin complet pour le bat

program cruncheur;
{$APPTYPE GUI}
uses
  Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs;

{$R *.RES}

//------------------------------------------------------------------------------
//--> Programme principal
//------------------------------------------------------------------------------
begin
    // Lecture des param�tres
    if ParamCount = 0 then begin
        ShowMessage('Chemin du fichier bat ??');
        Application.Terminate;
    end;

    if FileEXists(ParamStr(1)) then
        WinExec(Pchar(ParamStr(1)), SW_MINIMIZE)
    else
        ShowMessage('Fichier bat non trouv�.');
end.
