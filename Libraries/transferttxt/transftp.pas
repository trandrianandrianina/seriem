unit transftp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Psock, NMFtp, ComCtrls;

type
  TF_Transfert = class(TForm)
    NMFTP1: TNMFTP;
    OD_Chercher: TOpenDialog;
    GB_Connection: TGroupBox;
    L_Ip: TLabel;
    L_Port: TLabel;
    E_Ip: TEdit;
    E_Port: TEdit;
    GB_Proxy: TGroupBox;
    CB_Proxy: TCheckBox;
    L_Proxy: TLabel;
    L_PortProxy: TLabel;
    E_Proxy: TEdit;
    E_PortProxy: TEdit;
    L_Login: TLabel;
    L_Pwd: TLabel;
    E_Login: TEdit;
    E_Pwd: TEdit;
    BT_Connection: TButton;
    GB_Transfert: TGroupBox;
    E_FichierPC: TEdit;
    L_FichierPC: TLabel;
    BT_Transfert: TButton;
    BT_Chercher: TButton;
    GB_Status: TGroupBox;
    L_Status: TLabel;
    BT_Fermer: TButton;
    E_Biblio: TEdit;
    L_Biblio: TLabel;
    Label1: TLabel;
    procedure BT_connectionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BT_transfertClick(Sender: TObject);
    procedure NMFTP1Success(Trans_Type: TCmdType);
    procedure NMFTP1ConnectionFailed(Sender: TObject);
    procedure NMFTP1Connect(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CB_ProxyClick(Sender: TObject);
    procedure BT_FermerClick(Sender: TObject);
    procedure BT_ChercherClick(Sender: TObject);
    procedure NMFTP1Disconnect(Sender: TObject);
    procedure NMFTP1Failure(var Handled: Boolean; Trans_Type: TCmdType);
    procedure NMFTP1Error(Sender: TComponent; Errno: Word; Errmsg: String);
    procedure NMFTP1Status(Sender: TComponent; Status: String);
    procedure NMFTP1AuthenticationFailed(var Handled: Boolean);
    procedure NMFTP1AuthenticationNeeded(var Handled: Boolean);
    procedure E_PwdKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
  private
    { D�clarations priv�es}
  public
    { D�clarations publiques}
  end;

var
  F_Transfert: TF_Transfert;
  Erreur : string;
  passe : integer;
  errftp : boolean;

implementation

{$R *.DFM}

procedure TF_Transfert.BT_connectionClick(Sender: TObject);
begin
    if BT_connection.Caption = '&Connexion' then begin
        L_Status.caption := 'Connexion en cours ...';
        BT_Fermer.Enabled := False;
        if CB_Proxy.Checked = True  then begin
            NMFTP1.Proxy := E_proxy.text;
            NMFTP1.ProxyPort := StrToInt(E_portproxy.text);
        end;
        NMFTP1.Host := E_ip.text;
        NMFTP1.Port := StrToInt(E_port.text);
        NMFTP1.TimeOut :=  20000;
        NMFTP1.UserID := E_login.text;
        NMFTP1.Password := E_pwd.text;
        NMFTP1.Connect;
        GB_Transfert.Visible := True;
        BT_Connection.Caption := '&D�connexion';
        BT_Fermer.Caption := 'D�connecter et &Fermer'; end
    else begin
        NMFTP1.Disconnect;
        BT_connection.Caption := '&Connexion';
        BT_Fermer.Caption := '&Fermer';
        L_Status.caption := 'D�connexion r�ussie';
        GB_Transfert.Visible := False;
    end;
end;

procedure TF_Transfert.FormCreate(Sender: TObject);
begin
    CB_Proxy.Checked := False;
    GB_Proxy.Visible := False;
    GB_Transfert.Visible := False;
    Erreur := '';
    passe := 0;

    // R�cup�ration de l'adresse IP en parametre
    if ParamCount = 1 then
        E_Ip.Text := Trim(ParamStr(1))
    else
        if ParamCount = 2 then begin
            E_Ip.Text := Trim(ParamStr(1));
            E_Biblio.Text := Trim(ParamStr(2));
        end;

end;

procedure TF_Transfert.BT_transfertClick(Sender: TObject);
var
    posd, posf : integer;
    fichier, chaine : ansistring;
    liste : array[0..100] of ansistring;
    i : integer;
begin
    // V�rification avant transfert
    if (Trim(E_Biblio.Text) = '') or (Trim(E_FichierPC.Text) = '') then begin
        ShowMessage('Toutes les zones ne sont pas renseign�es.');
        Exit;
    end;
    errftp := false;

    // On pr�vient que le transfert � �t� lanc�
    BT_Fermer.Enabled := False;
    BT_Connection.Enabled := False;
    L_Status.Caption := 'Transfert en cours ...';

    // On se met dans la bibiloth�que *CURLIB
    NMFTP1.ChangeDir(E_Biblio.Text);
    if errftp = true then Exit;

    // On liste les fichiers � transferer
    for i:= 0 to 100 do
        liste[i] := '';

    chaine := Trim(E_FichierPC.Text);
    posd := Pos('"', chaine);
    i := 0;
    while posd <> 0 do begin
        chaine[posd] := '!';
        posf := Pos('"', chaine);
        if posf = 0 then begin
            showmessage('Erreur sur nom fichier.');
            Exit;
        end;
        chaine[posf] := '!';
        liste[i] := Copy(chaine, posd+1, posf-posd-1);
        Inc(i);
        posd := Pos('"', chaine);
    end;

    // On envoi le ou les fichiers vers l'AS/400
    if i <> 0 then begin
        i := 0;
        while liste[i] <> '' do begin
            NMFTP1.Upload(liste[i], '');
            //showmessage(liste[i]);
            Inc(i);
        end; end
    else
        NMFTP1.Upload(Trim(E_FichierPC.Text), '');
        //showmessage(Trim(E_FichierPC.Text));
end;

procedure TF_Transfert.NMFTP1Success(Trans_Type: TCmdType);
begin
    case Trans_Type of
        cmdUpload: begin
            L_Status.caption := 'Transfert ex�cut� avec succ�s';
            BT_Fermer.Enabled := True;
            BT_Connection.Enabled := True;
        end;
    end;
end;

procedure TF_Transfert.NMFTP1ConnectionFailed(Sender: TObject);
begin
    L_Status.caption := 'Echec � la connexion';
end;

procedure TF_Transfert.NMFTP1Connect(Sender: TObject);
begin
    BT_Fermer.Enabled := True;
    L_Status.caption := 'Connexion r�ussie';
end;


procedure TF_Transfert.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    NMFTP1.Disconnect;
end;

procedure TF_Transfert.CB_ProxyClick(Sender: TObject);
begin
    if CB_Proxy.Checked = True then
        GB_Proxy.Visible := True
    else
        GB_Proxy.Visible := False;
end;

procedure TF_Transfert.BT_FermerClick(Sender: TObject);
begin
    NMFTP1.Disconnect;
    Close;
end;

procedure TF_Transfert.BT_ChercherClick(Sender: TObject);
var
    i : integer;
begin
    OD_Chercher.Title := 'Chercher le ou les fichier(s) � transf�rer';
    OD_Chercher.Execute;
    with OD_Chercher.Files do begin
        E_FichierPC.Text := '';
        for i := 0 to Count - 1 do
            E_FichierPC.Text := E_FichierPC.Text + '"' + Strings[i] +'" ';
    end;
ShowMessage(E_FichierPC.Text);
end;

procedure TF_Transfert.NMFTP1Disconnect(Sender: TObject);
begin
    L_Status.Caption := 'D�connect�';
    //BT_Transfert.Enabled := False;
    GB_Transfert.Visible := False;
    BT_Fermer.Enabled := True;
    BT_Connection.Enabled := True;
    BT_connection.Caption := '&Connexion';
end;

procedure TF_Transfert.NMFTP1Failure(var Handled: Boolean;
  Trans_Type: TCmdType);
begin
    Handled := True;
    Case Trans_Type of
        cmdChangeDir: begin
                          L_Status.Caption := 'Echec de ChangeDir';
                          errftp := true;
                      end;
        cmdMakeDir: L_Status.Caption := 'Echec de MakeDir';
        cmdRemoveDir: L_Status.Caption := 'Echec de RemoveDir';
        cmdUpload: begin
                       L_Status.Caption := 'Echec du transfert';
                       BT_Fermer.Enabled := True;
                       BT_Connection.Enabled := True;
                       errftp := true;
                    end;
        cmdDoCommand: L_Status.Caption := Erreur;
    end;

end;

procedure TF_Transfert.NMFTP1Error(Sender: TComponent; Errno: Word;
  Errmsg: String);
begin
    L_Status.Caption := 'Erreur n�' + IntToStr(Errno);
    BT_Fermer.Enabled := True;
    BT_Connection.Enabled := True;
end;

procedure TF_Transfert.NMFTP1Status(Sender: TComponent; Status: String);
begin
     L_Status.Caption := Status + '('+ IntToStr(NMFTP1.ReplyNumber) +')';
     BT_Fermer.Enabled := True;
     BT_Connection.Enabled := True;
end;

procedure TF_Transfert.NMFTP1AuthenticationFailed(var Handled: Boolean);
begin
     L_Status.Caption := 'Erreur lors de l''identification';
end;

procedure TF_Transfert.NMFTP1AuthenticationNeeded(var Handled: Boolean);
begin
     L_Status.Caption := 'Erreur lors de l''identification';
end;

procedure TF_Transfert.E_PwdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then begin
        BT_Connection.SetFocus;
        BT_connectionClick(BT_Connection);
    end;
end;

procedure TF_Transfert.FormActivate(Sender: TObject);
begin
    if (ParamCount = 1) and (passe = 0) then
        E_Pwd.setFocus;
    Inc(passe);
end;


end.
