�
 TF_TRANSFERT 0�  TPF0TF_TransfertF_TransfertLeft� TopmWidth�Height�Caption*Transfert de fichiers textes vers l'AS/400Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	Icon.Data
�             �     (       @         �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������������������� ��������������p ��������������� ��������������� ��������������� ������������� � ����������� � ������������  p �������� �  � ��������� �  �� ��������    �� ���������    � ���������     ���������     ����������     ���������p  �  ����������  p  ����������  �  ����������  �  ����������  �  ����������  �  ����������� �  ����������� �� ����������� �� ����������� �� �������������� �����������p�� ����������������������������������������������p �������������������                                                                                                                                OldCreateOrder
OnActivateFormActivateOnCloseQueryFormCloseQueryOnCreate
FormCreatePixelsPerInch`
TextHeight 	TGroupBoxGB_ConnectionLeftTopWidth�Height� Caption Connexion � l'AS/400 TabOrder  TLabelL_IpLeftTopWidth[HeightCaptionAdresse du serveur  TLabelL_PortLeftTop0WidthHeightCaptionPort  TLabelL_LoginLeftTopWidthHeightCaptionLogin  TLabelL_PwdLeftTop<Width@HeightCaptionMot de passe  TEditE_IpLeft� TopWidthYHeightTabOrder Text	127.0.0.1  TEditE_PortLeft� Top0WidthHeightBiDiModebdLeftToRightFont.CharsetGREEK_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentBiDiMode
ParentFontTabOrderText21  	TGroupBoxGB_ProxyLeftTop`Width� HeightICaptionProxyTabOrder TLabelL_ProxyLeftTopWidthQHeightCaptionAdresse du proxy  TLabelL_PortProxyLeftTop,Width>HeightCaptionPort du proxy  TEditE_ProxyLefthTopWidthYHeightTabOrder   TEditE_PortProxyLefthTop,WidthHeightTabOrder   	TCheckBoxCB_ProxyLeftTopHWidth� HeightCaptionUtilisation d'un proxyTabOrderOnClickCB_ProxyClick  TEditE_LoginLeftPTopWidthYHeightTabOrderTextseriemad  TEditE_PwdLeftPTop<WidthYHeightPasswordChar*TabOrder
OnKeyPressE_PwdKeyPress  TButtonBT_ConnectionLefthTopxWidthQHeightCaption
&ConnexionTabOrderOnClickBT_connectionClick   	TGroupBoxGB_TransfertLeftTop� Width�HeightaCaption Transfert du fichier TabOrder TLabelL_FichierPCLeftTop Width+HeightCaption
Le fichier  TLabelL_BiblioLeftTop8Width:HeightCaptionBiblioth�que  TLabelLabel1LeftTopPWidthHeightCaption2Biblioth�que *CURLIB de l'AS/400. (Exemple: FMPRO)Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditE_FichierPCLeftPTopWidth� HeightTabOrder   TButtonBT_TransfertLefthTop@WidthQHeightCaption
&TransfertTabOrderOnClickBT_transfertClick  TButtonBT_ChercherLeft(TopWidth� HeightCaptionC&hercher le fichier sur le PCTabOrderOnClickBT_ChercherClick  TEditE_BiblioLeftPTop8WidthyHeight	MaxLength
TabOrder   	TGroupBox	GB_StatusLeftTop(Width�HeightACaption Statut TabOrder TLabelL_StatusLeftTopWidth� HeightCaptionNon connect�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TButton	BT_FermerLefthToppWidthyHeightCaption&FermerTabOrderOnClickBT_FermerClick  TNMFTPNMFTP1PortReportLevel OnDisconnectNMFTP1Disconnect	OnConnectNMFTP1ConnectOnConnectionFailedNMFTP1ConnectionFailedOnErrorNMFTP1ErrorOnAuthenticationNeededNMFTP1AuthenticationNeededOnAuthenticationFailedNMFTP1AuthenticationFailed	OnFailureNMFTP1Failure	OnSuccessNMFTP1SuccessVendork		ParseList	ProxyPort PassiveFirewallTypeFTUserFWAuthenticateLeftTopx  TOpenDialogOD_ChercherFileName*.txtFilter*.txtOptions
ofReadOnlyofHideReadOnlyofAllowMultiSelectofEnableSizing Left8Top    