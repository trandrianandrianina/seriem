package test;



import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;

import transformation.securitydigest.SecurityDigesterProvider;



public class test01
{
	private String algo = "HMac-SHA256"; 
	private String key = null;
	private String filePath = null;
	
	public test01(String akey, String afilepath){
		key = akey;
		filePath = afilepath;
	}
	
	public String getComputeKey(){
		String digest = null;
		if( ( key != null ) && ( filePath != null) ){
			byte[] tableau = null;
		    try {
		    	FileInputStream inputStream = new FileInputStream(filePath);
		        tableau = IOUtils.toByteArray(inputStream);
			    inputStream.close();
				digest = SecurityDigesterProvider.getDigest(tableau, algo, key);
		    }
		    catch(Exception e){
		    	System.out.println("-->" + e);
		    }
		}
		return digest;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		String key = "0123456789ABCDEF0123456789abcdef";
		String filePath = "C:\\Documents and Settings\\Administrateur\\Mes documents\\workspace\\BouncyCastleCrytoTest\\src\\com\\thomsonreuters\\ktp\\paymentfactory\\scheduler\\services\\transmission\\emission\\authentication\\CEFB_B12165_140416182152.VIRD_SEPA.XML";
		test01 t = new test01(key, filePath);
		System.out.println("--> |" + t.getComputeKey()+ "|");
	}

}
