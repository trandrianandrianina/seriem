/**
 * 12/05/2015 - 23/06/2015
 *
 */
package transformation;

import java.io.File;
import java.util.HashMap;

public class DataFromEbicstrans
{
	// Constantes
	private static final String PATHLOGS = "/tmp";
	private static final int DLYMIN = 10; 					// en secondes
	private static final String LOGS = "logs";
	
	// Variables
	private File pathlogs=null;
	private int delay = DLYMIN * 1000; // Temps en sec du d�lai entre 2 balayages des dossiers
	private String key;		// Cl� pour le chiffrage (� voir si on le stocke dans le fichier ini)
	private HashMap<String, String> pathSource = new HashMap<String, String>();  
	private HashMap<String, String> pathDestination = new HashMap<String, String>();


	/**
	 * @return le pathlogs
	 */
	public File getPathlogs()
	{
		return pathlogs;
	}

	/**
	 * @param pathlogs le pathlogs � d�finir
	 */
	public void setPathlogs(String apathlogs)
	{
		if( (apathlogs == null) || (apathlogs.equals("")) )
			this.pathlogs = new File(PATHLOGS);
		else {
			this.pathlogs = new File(apathlogs).getParentFile();
			if (!this.pathlogs.getAbsolutePath().toLowerCase().endsWith(LOGS)) {
				this.pathlogs = new File(this.pathlogs.getAbsolutePath() + File.separator + LOGS);
				this.pathlogs.mkdirs();
			}
		}
	}

	/**
	 * @return le delay
	 */
	public int getDelay()
	{
		return delay;
	}

	/**
	 * @param delay le delay � d�finir
	 */
	public void setDelay(int delay)
	{
		if( delay >= DLYMIN ) // On ne tient pas compte d'un d�lai inf�rieur � 10 sec car c'est trop cours et cela ne rime � rien
			this.delay = delay * 1000;
	}

	/**
	 * @param delay le delay � d�finir
	 */
	public void setDelay(String delay)
	{
		if( delay == null ) return;
		
		try{
			setDelay( Integer.parseInt(delay) );
		}
		catch(NumberFormatException nfe){}
	}

	/**
	 * @return le key
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * @param key le key � d�finir
	 */
	public void setKey(String key)
	{
		this.key = key;
	}

	/**
	 * @return le pathSource
	 */
	public HashMap<String, String> getPathSource()
	{
		return pathSource;
	}

	/**
	 * @return le pathDestination
	 */
	public HashMap<String, String> getPathDestination()
	{
		return pathDestination;
	}

	/**
	 * Ajoute un chemin source
	 * @param akey
	 * @param value
	 */
	public void addPathSource(String akey, String value)
	{
		pathSource.put(akey, value);
	}

	/**
	 * Ajoute un chemin destination
	 * @param akey
	 * @param value
	 */
	public void addPathDestination(String akey, String value)
	{
		pathDestination.put(akey, value);
	}

	/**
	 * Retourne un chemin source
	 * @param akey
	 * @return
	 */
	public String getPathSource(String akey)
	{
		return pathSource.get(akey);
	}

	/**
	 * Retourne un chemin destination
	 * @param akey
	 * @return
	 */
	public String getPathDestination(String akey)
	{
		return pathDestination.get(akey);
	}

}
