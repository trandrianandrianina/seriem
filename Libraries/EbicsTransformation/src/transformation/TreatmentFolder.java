/**
 * 12/05/2015 - 13/05/2015
 *
 */
package transformation;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;

import ri.seriem.libcommun.outils.LogMessage;

public class TreatmentFolder
{
	// Constantes
	private static final String extension = ".VIR";
	private static final int CPTMAX = 2;
	
	// Variables
	private File pathSource = null;
	private String pathDestination = null;
	private HashMap<String, Integer>contentFolder = new HashMap<String, Integer>();
	private TransformFile transformFile = null;
	private LogMessage log = null; 

	/**
	 * Constructeur
	 * @param akey
	 * @param apathsource
	 * @param apathdestination
	 * @param alog
	 */
	public TreatmentFolder(String akey, String apathsource, String apathdestination, LogMessage alog)
	{
		pathSource = new File(apathsource);
		pathDestination = apathdestination;
		transformFile = new TransformFile(akey);
		log = alog;
	}
	
	/**
	 * Controle le contenu d'un dossier et effetcue les traitements si besoin 
	 */
	public void controlFolder()
	{
		//System.out.println("-TreatmentFolder (controlFolder)-> " + pathSource.getAbsolutePath());
		// Liste les fichiers du dossier avec un filtre sur l'extension
		String list[] = pathSource.list(new FilenameFilter() {
			public boolean accept(File folder, String file)
			{
				return file.toLowerCase().endsWith(extension.toLowerCase());
			}
		});
		// Si le dossier est vide, on sort
		if( list == null || list.length == 0 ){
			if (!contentFolder.isEmpty()) contentFolder.clear();
			return;
		}
		
		// On a trouv� des fichiers, on analyse les compteurs (permet d'�tre sur que le fichier trait� est bien totalement �crit sur disque)
		for(String file: list){
			Integer compteur = contentFolder.get(file);
			if( compteur != null ){
				if (compteur >= CPTMAX) // En fait cela repr�sente 2 * delay secondes d'attente 
					treatmentFile(pathSource.getAbsolutePath() + File.separatorChar + file);
				else
					contentFolder.put(file, ++compteur);
			} else{
				contentFolder.put(file, 1);
			}
		}
	}
	
	/**
	 * Traitement d'un fichier
	 * @param afile
	 */
	private void treatmentFile(String afile)
	{
		File file = new File(afile);
		log.ecritureMessage("Traitement du fichier " + afile);
		boolean ret = transformFile.addKeyIntoFile(afile, pathDestination);
		if( ret ){
			file.delete(); // On supprime le fichier dans le dossier source
			log.ecritureMessage("Transformation du fichier " + file.getName() + " termin� avec succ�s.\n");
		} else{
			log.ecritureMessage(transformFile.getMsgErreur());
			log.ecritureMessage("Echec de la transformation du fichier " + file.getName() + ".\n");
		}
	}
	
}
