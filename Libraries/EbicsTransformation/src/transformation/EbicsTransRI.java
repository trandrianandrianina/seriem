/**
 * 12/05/2015 - 13/05/2015 
 * Mise en place
 * 	Mettre le bcprov-jdk15-140.jar et le EbicsTransRI.jar dans le dossier jar de /ebanks/transformaton/jar 
 * 
 * Configurer le fichier /ebanks/transformaton/conf/ebicstrans.ini
 * 	- Dans la section [general]
 * 		log= mettre le chemin complet du fichier de log
 * 		key= la clef de scellement
 * 	- Pour chaque configuration
 * 		finaldestination=le dossier final qui va recevoir les fichiers scell�s
 */
package transformation;

import java.security.Security;
import java.util.Timer;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import ri.seriem.libcommun.outils.LogMessage;


public class EbicsTransRI
{
	// Constantes
	private static final String VERSION="1.0";
	private static final String SUFFIXE_LOG="_EbicsTransRI";
	
	// Variables
	private DataFromEbicstrans dataFromEbicstrans=null;
	private LogMessage log = null;

	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu
	
	
	/**
	 * Constructeur
	 * @param aparameter
	 */
	public EbicsTransRI(String aparameter)
	{
		treatment(aparameter);
	}

	/**
	 * Traitement principal 
	 * @param apathconf
	 */
	private void treatment(String apathconf)
	{
		// On charge les informations d'Ebicstrans
		if( !loadInformations(apathconf) ){
			System.out.println(getMsgErreur());
			System.out.println("Programme arr�t�.");
			log.ecritureMessage(getMsgErreur());
			return;
		}
		
		// Cr�ation du fichier de log
		log = new LogMessage(dataFromEbicstrans.getPathlogs().getAbsolutePath(), SUFFIXE_LOG);
		log.ecritureMessage("D�marrage du programme pour le scellement des fichiers [" + VERSION + "]");
		
		// On lance le timer qui va scanner les dossiers � surveiller
		Timer timer = new Timer();
		CheckFolders checkFolders = new CheckFolders(dataFromEbicstrans, log);
		timer.schedule(checkFolders, dataFromEbicstrans.getDelay(), dataFromEbicstrans.getDelay());
	}

	/**
	 * Chargement des informations provenant d'Ebicstrans
	 * @param apathconf
	 * @return
	 */
	private boolean loadInformations(String apathconf)
	{
		// Analyse du fichier ini
		AnalyseINI analyse = new AnalyseINI();
		analyse.chargement(apathconf);
		boolean ret = analyse.traitement();
		analyse.dispose();
		if( !ret){
			msgErreur += analyse.getMsgErreur();
			return false;
		}
		
		dataFromEbicstrans = analyse.getDataFromEbicstrans();
		return true;
	}
	
    /**
     * Retourne le message d'erreur
     * @return
     */
    public String getMsgErreur()
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }

	
	/**
	 * Programme principal
	 * @param args
	 */
	public static void main(String[] args)
	{
		// V�rification des param�tres
		if( args.length != 1 ){
			System.out.println("--> Attention, il vous manque un param�tre. Ce param�tre est le chemin qui m�ne au dossier configuration d'Ebicstran.\nPar exemple: /ebanks/transformation/conf ");
			System.exit(-1);
		}
		
		new EbicsTransRI(args[0]);
	}

}
