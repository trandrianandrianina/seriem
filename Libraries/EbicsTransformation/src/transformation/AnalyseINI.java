/**
 * 12/05/2015 - 13/05/2015
 *
 */
package transformation;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.seriem.libcommun.outils.GestionFichierINI;

public class AnalyseINI
{
	// Constantes
	public static final String NAME_INI = "ebicstrans.ini";
	private static final String GENERAL = "general";
	private static final String LOG = "log";
	private static final String DELAY = "tps";
	private static final String KEY = "key";
	private static final String LIST = "lsttrans";
	private static final String PATHSOURCE = "repsortie";
	private static final String PATHDESTINATION = "finaldestination";

	
	// Variables
	private GestionFichierINI gestionFichier = new GestionFichierINI();
 	private LinkedHashMap<String, HashMap<String, String>> sections = null;
    private DataFromEbicstrans dataFromEbicstrans = new DataFromEbicstrans();

 	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu
 	
 	
	/**
	 * Chargement du fichier ini de ebicstrans
	 * @param chemin
	 * @return
	 */
	public boolean chargement(String chemin)
	{
		if( (chemin == null) || (chemin.equals("")) ){
			msgErreur += "Chemin vers le fichier " + NAME_INI + " incorrect.";
			return false;
		}
		
		gestionFichier.setNomFichier(chemin + File.separatorChar + NAME_INI);
		sections = gestionFichier.getSections();

		return true;
	}
	
	/**
	 * Retourne les informations de Ebicstrans
	 * @return
	 */
	public DataFromEbicstrans getDataFromEbicstrans()
	{
		return dataFromEbicstrans;
	}
	
	/**
	 * Lib�re la m�moire 
	 */
	public void dispose()
	{
		gestionFichier.dispose();
	}
	
	/**
	 * Analyse des donn�es lues dans le fichier ini
	 * @return
	 */
	public boolean traitement()
	{
		if( (sections == null) || (sections.size() == 0) ){
			msgErreur += "Le fichier " + NAME_INI + " est vide.";
			return false;
		}
		
		sectionGeneral();
		sectionList();
		
		/*
		System.out.println("-key->" + dataFromEbicstrans.getKey());
		for(Entry<String, String> entry : dataFromEbicstrans.getPathSource().entrySet()) {
			System.out.println("-s-> " + entry.getKey() + "=" + entry.getValue());
		}
		for(Entry<String, String> entry : dataFromEbicstrans.getPathDestination().entrySet()) {
			System.out.println("-d-> " + entry.getKey() + "=" + entry.getValue());
		}*/
		

		return true;
	}
	
	/**
	 * On r�cup�re les informations dans la section GENERAL
	 * @return
	 */
	private boolean sectionGeneral()
	{
		HashMap<String, String> general = sections.get( GENERAL );
		if( (general == null) || (general.size() == 0) ){
			msgErreur += "La section " + GENERAL + " est vide ou inexistante.";
			return false;
		}
		dataFromEbicstrans.setPathlogs( general.get( LOG ) );
		dataFromEbicstrans.setDelay( general.get( DELAY ) );
		dataFromEbicstrans.setKey( general.get( KEY ) );
		
		general.clear();
		return true;
	}

	/**
	 * On r�cup�re les informations des sections contenu dans la section LIST
	 * @return
	 */
	private boolean sectionList()
	{
		HashMap<String, String> list = sections.get( LIST );
		if( (list == null) || (list.size() == 0) ){
			msgErreur += "La section " + LIST + " est vide ou inexistante.";
			return false;
		}

		// On parcourt la liste
		for(Entry<String, String> entry : sections.get( LIST ).entrySet()) {
		    String cle = entry.getKey();
		    String valeur = entry.getValue();
		    
		    // Pour chaque section trouv�e
		    if( cle.equals( valeur ) ){
		    	sectionDetail(cle);
		    }
		}
		
		list.clear();
		return true;
	}

	/**
	 * On r�cup�re les informations d'une session en particulier
	 * @param section
	 * @return
	 */
	private boolean sectionDetail(String section)
	{
		HashMap<String, String> detail = sections.get( section );
		if( (detail == null) || (detail.size() == 0) ){
			msgErreur += "La section " + section + " est vide ou inexistante.";
			return false;
		}
		dataFromEbicstrans.addPathSource( section, detail.get( PATHSOURCE ) );
		dataFromEbicstrans.addPathDestination( section, detail.get( PATHDESTINATION ) );
		
		detail.clear();
		return true;
	}
	
    /**
     * Retourne le message d'erreur
     * @return
     */
    public String getMsgErreur()
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }
}
