/**
 * 12/05/2015 - 13/05/2015
 *
 */
package transformation;


import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.GestionFichierTexte;
import transformation.securitydigest.SecurityDigesterProvider;



public class TransformFile
{
	private String algo = "HMac-SHA256"; 
	private String key = null;

	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

	/**
	 * Constructeur
	 * @param akey
	 * @param afilepath
	 */
	public TransformFile(String akey){
		key = akey;
	}
	
	/**
	 * Calcul de la cle pour le fichier donn�
	 * @param afilepath
	 * @return
	 */
	private String getComputeKey(String afilepath){
		String digest = null;
		if( ( key != null ) && ( afilepath != null) ){
			byte[] tableau = null;
		    try {
		    	FileInputStream inputStream = new FileInputStream(afilepath);
		        tableau = IOUtils.toByteArray(inputStream);
			    inputStream.close();
				digest = SecurityDigesterProvider.getDigest(tableau, algo, key);
		    }
		    catch(Exception e){
		    	msgErreur += "Erreur lors du calcul de la cl� : " + e;
		    }
		}
		return digest;
	}
	
	/**
	 * Ajoute le code en t�te du fichier 
	 * @param afilepath
	 * @param pathDestination
	 * @return
	 */
	public boolean addKeyIntoFile(String afilepath, String pathDestination){
		// Calcul de la cl�
		String digest = getComputeKey(afilepath);
		if( digest == null) return false;

		// Lecture du contenu du fichier source

		File ffilepath = new File(afilepath);
		GestionFichierTexte gft = new GestionFichierTexte(ffilepath);
		String contenu = gft.getContenuFichierString(false);
		if( contenu == null)
		{
			msgErreur += gft.getMsgErreur();
			return false;
		}
		
		// R��criture du fichier dans le dossier destination
		gft.setNomFichier(pathDestination + File.separatorChar + ffilepath.getName(), true);
		// Ajout de la cl�
		gft.setContenuFichier("Sealing value:" + digest + '\n' + contenu);
		if( gft.ecritureFichier() == Constantes.ERREUR )
		{
			msgErreur += gft.getMsgErreur();
			return false;
		}
		gft.dispose();
		
		return true;
	}
	
    /**
     * Retourne le message d'erreur
     * @return
     */
    public String getMsgErreur()
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }
	/**
	 * @param args
	 *
	public static void main(String[] args)
	{
		String key = "0123456789ABCDEF0123456789abcdef";
		String filePath = "C:\\Documents and Settings\\Administrateur\\Mes documents\\workspace\\BouncyCastleCrytoTest\\src\\com\\thomsonreuters\\ktp\\paymentfactory\\scheduler\\services\\transmission\\emission\\authentication\\CEFB_B12165_140416182152.VIRD_SEPA.XML";
		TransformFile t = new TransformFile(key, filePath);
		System.out.println("--> |" + t.getComputeKey()+ "|");
	}*/

}
