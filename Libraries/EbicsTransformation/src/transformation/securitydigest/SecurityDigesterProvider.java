package transformation.securitydigest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


/**
 * Encryption provider, used to compute the LAU token for Alliance Lite for example.
 * @author malik.chettih
 *
 */
public class SecurityDigesterProvider {
	
	/**
	 * Static constructor.
	 */
	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	/**
	 * Compute the digest (ex: LAU for alliance) for a messsage.
	 * @param message 
	 * @return
	 */
	public static String getDigest(byte[] message, String algo, String key){
		
		String digest = null;
		try{
			digest = computeHMAC(algo, key, message);
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return digest;
	}
	
	/**
	 * Compute HMAC ...
	 * @param hmacName
	 * @param key
	 * @param message
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws InvalidKeyException
	 */
	protected static String computeHMAC(final String hmacName, final String key, final byte[] message) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException{
		byte[] keyBytes = key.getBytes();
		byte[] result = null;
		byte[] result2 = new byte[16];
		
		SecretKey secretKey = new SecretKeySpec(keyBytes, hmacName);
		Mac mac = Mac.getInstance(hmacName, "BC");
        mac.init(secretKey);
        mac.reset();
        mac.update(message, 0, message.length);
        
        result = mac.doFinal();
        for(int i=0;i<result.length/2;i++){ 
        	result2[i] = result[i];
        }
        return Base64.encodeBase64String(result2).replaceAll("\\r\\n", "");
	}
	

}
