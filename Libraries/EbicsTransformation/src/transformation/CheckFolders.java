/**
 * 13/05/2015 - 13/05/2015
 *
 */
package transformation;

import java.util.Map.Entry;

import ri.seriem.libcommun.outils.LogMessage;

import java.util.TimerTask;

public class CheckFolders extends TimerTask
{
	// Variables
	private DataFromEbicstrans dataFromEbicstrans = null;
	private LogMessage log = null;
	private TreatmentFolder[] listFolders = null;
	
	/**
	 * Constructeur
	 * @param adataFromEbicstrans
	 */
	public CheckFolders(DataFromEbicstrans adataFromEbicstrans, LogMessage alog)
	{
		dataFromEbicstrans = adataFromEbicstrans;
		log = alog;
		preparationBeforeTreatment();
	}

	/**
	 * Traitement r�current toutes les X secondes
	 */
	@Override
	public void run()
	{
		//System.out.println("-CheckFolders (run)-> Traitement d�marr�.");
		for( TreatmentFolder folder: listFolders){
			folder.controlFolder();
		}
		
	}

	/**
	 * Pr�paration avant le traitement en boucle
	 */
	private void preparationBeforeTreatment()
	{
		listFolders = new TreatmentFolder[dataFromEbicstrans.getPathSource().size()];
		int i = 0;
		log.ecritureMessage("--> Dossier(s) sous surveillance:");
		for(Entry<String, String> entry: dataFromEbicstrans.getPathSource().entrySet())
		{
			log.ecritureMessage("--> [" + entry.getKey() + "]  src:" + entry.getValue() + " dst:" + dataFromEbicstrans.getPathDestination(entry.getKey()));
			listFolders[i] = new TreatmentFolder(dataFromEbicstrans.getKey(), entry.getValue(), dataFromEbicstrans.getPathDestination(entry.getKey()), log);
			i++;
		}
	}
}
