program presspap;
{$APPTYPE GUI}
uses
  Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs,
  Clipbrd;

{$R *.RES}
var
  i: Integer;
  chaine : ansistring;

begin
  // Lecture des param�tres entrants
  chaine := '';
  for i := 1 to ParamCount do
      if i = 1 then
          chaine := ParamStr(i)
      else
          chaine := chaine + #13 + #10 + ParamStr(i);

  // Mise en place des donn�es dans le presse papier
  if chaine <> '' then
      Clipboard.SetTextBuf(Pchar(chaine));
end. 