
package java;

import java.util.ArrayList;

import ri.seriem.libcommun.protocoleMsg.Db2Request;
import ri.seriem.libcommun.protocoleMsg.LineCells;
import ri.seriem.libcommun.protocoleMsg.MessageManager;

public class test15 {
  public static final ArrayList<Integer> ACTIONS = new ArrayList<Integer>() {
    {
      add(0);
    }
    {
      add(1);
    }
  };
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    // String message =
    // "{\"head\":{\"id\":0,\"hashcodeCaller\":28818547,\"version\":1,\"bcomp\":false,\"bcode\":3,\"blength\":218,\"bdesc\":\"protocoleMsg.Db2Request\"},\"body\":{\"request\":\"select
    // substring(PACCLE, 6, 2) as \\\"Journaux\\\", substring(PACFIL, 1, 30) as \\\"Libelle\\\" from FM201.pcgmpacm where
    // substring(PACCLE, 1, 5) \\u003d \\u0027RISJO\\u0027\",\"select\":true,\"actions\":1,\"success\":false}}"; //String message =
    // "{\"head\":{\"id\":0,\"hashcodeCaller\":22005471,\"version\":1,\"bcomp\":false,\"bcode\":3,\"blength\":218,\"bdesc\":\"protocoleMsg.Db2Request\"},\"body\":{\"request\":\"select
    // substring(PACCLE, 6, 2) as \\\"Journaux\\\", substring(PACFIL, 1, 30) as \\\"Libelle\\\" from FM001.pcgmpacm where
    // substring(PACCLE, 1, 5) \\u003d \\u002701 JO\\u0027\",\"select\":true,\"actions\":1,\"success\":false}}";
    // String message =
    // "{\"head\":{\"id\":0,\"hashcodeCaller\":22005471,\"version\":1,\"bcomp\":false,\"bcode\":3,\"blength\":218,\"bdesc\":\"protocoleMsg.toto\"},\"body\":{\"request\":\"test\",\"select\":true}}";
    // //, \"title\":null
    // System.out.println(message);
    // String message =
    // "{\"head\":{\"bdesc\":\"protocoleMsg.Db2Request\",\"id\":0,\"hashcodeCaller\":28476179,\"version\":1,\"bcomp\":true,\"bcode\":3,\"blength\":218},\"body\":\"eJxtjs0KwjAQhF9l2ZOFIEmLCr1JsWCp6AP0EuO2FtIfswkK4rtbtBfB0wzMx8w80dEtEHtMkcmS8cDhzN61fbM4bbOs3AlYC4gj0AwVFkNwvQ6PCsUvmO9LAUpAImeybM9kLVUItRs6yA9SquVomm7UpoP7lRz9mZoaVhFUQcrk8pF4IxUUx69FMZ/E1LtAArXx7dAzpmpKgjHEk6+1ZXq9AW4HREc=\"}";
    String message =
        "{\"head\":{\"bdesc\":\"protocoleMsg.Db2Request\",\"id\":0,\"hashcodeCaller\":271078,\"version\":1,\"bcomp\":true,\"bcode\":3,\"blength\":2957},\"body\":\"eJztlk1v1DAQhv/KKCeQIrQf/YDeHGeSujh2cD5UoBy2SyiVti0kWYFU8d8Zr5elu3avnDht4n0fe2b8euLHqO++r7thjM6ioVt1yxGG9fUw9rf3Ny9KxrnEGE5imL2ExQBX0cXDur9frH9eRfG+MBMyhmkM88lWKW+vu9Wqu4rgS/9wB1kxmUxffVve3H1bLO/gx9eu7wJL0QzHL+FqPZnMP29+ZqeTKVxo9xjF0Xg7rrro7OMuEhrbLhV9iqPPi3FB/z5G4+J61S3WVsk4aRg/ZzRPYxSTkGIFdqCuok+/4n2xsmKls52YgdJNi6yBzI6A8pjEMokqYcckoKD0daXVlXUJCVPvGoRSl41kwiDUvpozUnMGANxgKmpguRFcS/SVNkHOn6xvE7QDtW6kbqoAghZBOzkTVYWQugpjyUyuAvpzqz+nJfg5UugVIGWoTY0ZNkIGgkIbPrKDoDDLsKbiQ8ne+0xmiMlMjpAZCgtyVGhYc+kJC1cbcbKbfJtGJrExTHF/7iJxyOmTeP5QJWtFgOCOeB0gcpYwYwJMumGmE8s8iYujqg2T9KB9Bh0zPUxmB/mWKzLHzJ5lfEMVuWPmzzKtz2z2nc08E9KhQOnLhZN7qbSCQyJybYzPXDhmFiizoTJXdaDMbx3jpVIIlCxQLun0R1t9c7kD8IMI6AunPw7EVAgyWBqISTnDqx2zZ3ZDFfMZvWHMM4fEYMjItovolEvQKXCp64b6B16WaGpyV1HWLPGZdxtG6b11iGdJRYdY6EAJpq4RNn4jpOJJii/zmZlj2kDz5FKQzfyGW8zdKVO+68lhULDQ9h85pnrOyT5x7IhpYEO5VqkufOTEISFfKp0zE9jOU4fMQwh1Mu4Trx1xFDK/qHgolTcOOT5MnjXUpQ/Vmm12HhgJ39fCtm5fkzoNHLjDE5a2P5eGtuWp0A5gi0Vwe41tbgZLs//x/WulqYdUtk9VOYVTaS6wxu13IFCLtiZpS4q92VuKxE/z/+v/13/yStdQd5WOzsZ+3cXR3XCDff/Q0x2b7LpYjrcP90N0NiHdernshsEJf/0GfP5sqw==\"}";
    
    System.out.println("--> size " + test15.ACTIONS.size());
    
    MessageManager mc = new MessageManager(0);
    if (!mc.getMessageReceived(message, 0)) {
      System.out.println("Erreur\n");
    }
    else {
      System.out.println(mc.getHead().getHashcodeCaller());
      Db2Request db = (Db2Request) mc.getBody();
      ArrayList<LineCells> listcell = db.getData();
      System.out.println(listcell.size());
      for (LineCells cell : listcell) {
        System.out.println("-->" + cell.tableau.size());
      }
      // System.out.println(db.getActions());
      // System.out.println(db.getTTitle());
      // System.out.println(db.getTData());
      // for( int i=0; i<db.getTitle().size(); i++)
      // System.out.println(db.getTitle().get(i) );
      // System.out.println(db.isSuccess());
    }
  }
  
}
