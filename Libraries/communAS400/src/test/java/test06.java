
package java;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;

import ri.seriem.libas400.database.DatabaseManager;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.system.SystemManager;

public class test06 {
  protected Connection dataBase = null;
  private QueryManager qrm = null;
  private DatabaseManager databasemanager = null;
  private SystemManager sys = null;
  
  public test06(String[] args) {
    boolean driver = args[0].toLowerCase().equals("true");
    System.out.println("-driver-> " + driver);
    // sys = new SystemManager("*LOCAL", "SERIEMAD", "GAR1972", true, driver);
    sys = new SystemManager("172.31.1.249", "SERIEMAD", "GAR1972", true, false);
    // System.out.println("-msg-> " + sys.getMsgError() );
    dataBase = sys.getdatabase().getConnection();
    databasemanager = sys.getdatabase();
    qrm = new QueryManager(dataBase);
  }
  
  public QueryManager getQuery() {
    return qrm;
  }
  
  public DatabaseManager getDataBaseManager() {
    return databasemanager;
  }
  
  public void disconnect() {
    sys.disconnect();
  }
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    test06 t = new test06(args);
    // On affiche
    // String requete = "SELECT * FROM QIWS.QCUSTCDT";
    String requete = "WITH AFF AS (SELECT AAMAG, AAART, SUM(AAQTE) as QTEAFF FROM FM802.PGVMALA0"
        + " LEFT JOIN FM802.PGVMEBC ON AAETB=E1ETB AND AACOS=E1COD AND AANOS=E1NUM AND AASOS=E1SUF "
        + " WHERE E1ETA < 4 AND AATOE='S' GROUP BY AAMAG, AAART) "
        + " SELECT  S1ART, SUM((s1sTD+ S1QEE+ S1QSE+ S1QDE+ S1QEM+ S1QSM+S1QDM+ S1QES+S1QSS+ S1QDS)-QTEAFF) as STK "
        + " FROM FM802.PGVMSTK" + " LEFT JOIN AFF on S1MAG=AAMAG AND S1ARt=AAART "
        + " LEFT JOIN FM802.PGVMARTM ON S1ETB = A1ETB AND S1ART = A1ART " + " WHERE S1MAG in ('01' , '02')" // A1FAM = '601' AND
        + " GROUP BY S1ART";
    /*
    String requete = "WITH AFF AS (SELECT AAMAG, AAART, SUM(AAQTE) as QTEAFF FROM FM802.PGVMALA0" 
    		  + " LEFT JOIN FM802.PGVMEBC ON AAETB=E1ETB AND AACOS=E1COD AND AANOS=E1NUM AND AASOS=E1SUF "
    		  + " WHERE E1ETA < 4 AND AATOE='S' GROUP BY AAMAG, AAART) "
    		  + " SELECT S1MAG, S1ART, SUM((s1sTD+ S1QEE+ S1QSE+ S1QDE+ S1QEM+ S1QSM+S1QDM+ S1QES+S1QSS+ S1QDS)-QTEAFF) as STK "
    		  + " FROM FM802.PGVMSTK"
    		  + " LEFT JOIN AFF on S1MAG=AAMAG AND S1ARt=AAART "
    		  + " LEFT JOIN FM802.PGVMARTM ON S1ETB = A1ETB AND S1ART = A1ART " 
    		  + " WHERE A1FAM = '201' AND S1MAG in ('01' , '02')" // A1FAM = '601' AND
    		  + " GROUP BY S1MAG, S1ART";
    */
    /*
    String requete = "WITH AFF AS (SELECT AAMAG, AAART, SUM(AAQTE) as QTEAFF FROM FM802.PGVMALA0" 
    		  + " LEFT JOIN FM802.PGVMEBC ON AAETB=E1ETB AND AACOS=E1COD AND AANOS=E1NUM AND AASOS=E1SUF "
    		  + " WHERE E1ETA < 4 AND AATOE='S' GROUP BY AAMAG, AAART) "
    		  + " SELECT S1MAG, SUM((s1sTD+ S1QEE+ S1QSE+ S1QDE+ S1QEM+ S1QSM+S1QDM+ S1QES+S1QSS+ S1QDS)-QTEAFF) as STK "
    		  + " FROM FM802.PGVMSTK"
    		  + " LEFT JOIN AFF on S1MAG=AAMAG AND S1ARt=AAART "
    		  + " WHERE S1MAG in ('01' , '02')"
    		  + " GROUP BY S1MAG";
    */
    // String requete = "";
    
    /*
    ArrayList<GenericRecord>[] liste = new ArrayList[20];
    long total = 0;
    for ( int i=0; i < 20; i++ )
    {
    	//long t1 = System.nanoTime();
    	long t3 = System.currentTimeMillis();
    	liste[i] = t.getQuery().select(requete);
    	//ArrayList<GenericRecord> liste = t.getQuery().select(requete);
    	//long t2 = System.nanoTime() - t1;
    	long t4 = System.currentTimeMillis() - t3;
    	total += t4;
    //			System.out.println("--> La connexion est " + (t.getDataBaseManager().driverNative?"Toolbox":"Native"));
    	
    	//if (liste != null)
    	//	System.out.println("--> nbr enr:" + liste.size() + " en " + t4 + " ms "); // + t4 + " ms");
    	//else
    	//	System.out.println("--> pas d'enreg " + t.getQuery().getMsgError());
    }
    
    ArrayList<GenericRecord> liste2 = new ArrayList<GenericRecord>();
    long total2 = 0;
    String requetes = "";
    for ( int i=0; i < 20; i++ )
    	requetes += requete;
    long t3 = System.currentTimeMillis();
    liste2 = t.getQuery().select(requetes);
    long t4 = System.currentTimeMillis() - t3;
    total2 += t4;
    
    t.disconnect();
    System.out.println("--> 1 en " + total + " ms / 2 en " + total2 + " ms");
    */
    
    // System.out.println("--> nbr enr:" + liste.length + " en " + t2 + " ms");
    // for (GenericRecord record: liste)
    // System.out.println("--> " + record.getField("STK"));
    try {
      Statement statement = t.dataBase.createStatement();
      // statement.executeUpdate("DROP PROCEDURE IF EXISTS hello");
      statement.executeUpdate("CREATE PROCEDURE svas.myFirst()" + "LANGUAGE JAVA" + "END");
      // APPEL DE LA PROCEDURE
      String sql = "{call svas.myFirst()}";
      CallableStatement call = t.dataBase.prepareCall(sql);
      // passage de la valeur du param�tre
      // call.setString(1, " world ");
      // enregistrement du param�tre en tant que param�tre OUT
      // call.registerOutParameter(1, Types.VARCHAR);
      // ex�cution et r�cup�ration du r�sultat
      call.execute();
      System.out.println(call.getString(1));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    t.disconnect();
  }
  
  /*
  Il faut modifier DatabaseManager afin de supprimer le test initContext dans disconnect
  Il fut ajouter la gestion du pool (voir ci-dessous)
  Le test fait avec Tomcat avec le toolbox plante sur le factory, il faudrait tester avec le driver natif
  Tester si la gestion du pool avec Datamanager ne suffirait pas � la place du web.xml pour Tomcat
  
  les job QZADONIT ont disparu � 18h donc c'est li� au PC
  */
  /*
   * voir
   *  http://stackoverflow.com/questions/7899691/db2-trusted-connection-via-tomcat-datasource
   *  http://www.itjungle.com/fhg/fhg060204-story01.html
   * 
   DB2ConnectionPoolDataSource dbcpds = new DB2ConnectionPoolDataSource();  
   
        dbcpds.setUser(username);  
        dbcpds.setPassword(password);  
          
        PooledConnection pc = dbcpds.getPooledConnection();  
          
        // Get a Logical connection  
        Connection conn = pc.getConnection();  
          
        // Create a Statement  
        Statement stmt = conn.createStatement();  
          
        ResultSet rset = stmt.executeQuery("SELECT current date FROM sysibm.sysdummy1");  
          
        // Iterate through the result and print   
        while (rset.next())  
            out.println(rset.getString(1));  
          
        // Close the RseultSet  
        rset.close();  
        rset = null;  
          
        // Close the Statement  
        stmt.close();  
        stmt = null;  
          
        // Close the logical connection  
        conn.close();  
        conn = null;  
          
        // Close the pooled connection  
        pc.close();  
        pc = null;     
   */
}
