
package java;

import java.util.ArrayList;

public class test04 {
  
  private ArrayList<String> decoupeChaine(StringBuilder chaine, int longueur) {
    int deb = 0;
    int fin = longueur;
    ArrayList<String> liste = new ArrayList<String>();
    do {
      if (fin < chaine.length()) {
        liste.add(chaine.substring(deb, fin));
        deb = fin;
        fin += longueur;
      }
      else {
        liste.add(chaine.substring(deb));
        deb += longueur;
      }
    }
    while (deb < chaine.length());
    
    return liste;
  }
  
  /*
  private String[] getZones(String ligne, int longueurZone)
  {
  	float nbrzones = (float)(ligne.length() / longueurZone);
  	String[] zones = new String[Math.round(nbrzones)];
  	int deb = 0;
  	int fin = longueurZone;
  	for( int i=0; i < zones.length; i++ )
  	{
  		if( fin < ligne.length() )
  			zones[i] = ligne.substring(deb, fin);
  		else
  			zones[i] = ligne.substring(deb);
  		deb = fin;
  		fin += longueurZone;
  	}
  	return zones;
  }*/
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    test04 t = new test04();
    StringBuilder sb = new StringBuilder("123456");
    ArrayList<String> liste = t.decoupeChaine(sb, 5);
    // On ins�re
    for (int indice = liste.size(); --indice >= 0;)
      System.out.println("-" + indice + "->" + liste.get(indice));
    
  }
  
}
