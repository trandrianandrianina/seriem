
package java;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.ISeriesNetServer;

public class test09 {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    // Create a NetServer object for a specific system.
    AS400 system = new AS400("172.31.1.253", "SERIEMAD", "GAR1972");
    ISeriesNetServer ns = new ISeriesNetServer(system);
    
    try {
      
      // Get the name of the NetServer.
      System.out.println("Name: " + ns.getName());
      
      // Get the CCSID of the NetServer.
      System.out.println("CCSID: " + ns.getCCSID());
      
      // Get the "allow system name" value of the NetServer.
      System.out.println("'Allow system name': " + ns.isAllowSystemName());
      
      /*
      File fichier = new File("C:\\Documents and Settings\\Administrateur\\Bureau\\TestNativeJDBC.war");
      long date = fichier.lastModified();
      System.out.println("--> " + date);
      Date now = new Date(date);
      */
      /*
      ArrayList<Character> lettre = new ArrayList<Character>();
      lettre.add('J');
      lettre.add('F');
      lettre.add('M');
      lettre.add('A');
      lettre.add('S');
      lettre.add('O');
      lettre.add('N');
      lettre.add('D');
      
      FTPManager ftp = new FTPManager();
      System.out.println( ftp.connexion("31.193.129.158", "seriemclient", "seriemad") );
      String[] liste = ftp.listFolderWithAttribute("/httpdocs/WEB");
      for( String file: liste){
        int pos = file.lastIndexOf( ' ' );
        int posnom = file.indexOf(' ', pos);
        for (int i=pos-1; i!=0; i--){
         if( lettre.contains(file.charAt( i ) ) ){
      	   pos = i;
      	   break;
         }
        }
        System.out.println( file );
        System.out.println( file.substring( posnom + 1 ) );
        System.out.println( file.substring( pos, posnom ) );
      }
      ftp.disconnect();
      
      String test = "Encul�";
      char[] tab1 = new char[20];
      Arrays.fill(tab1, ' ');
      System.arraycopy(test.toCharArray(), 0, tab1, 0, test.length());
      System.out.println(new String(tab1) + "|");
      */
      // String toto = new String( + "|");
      // ns.createFileShare( "xweb", "/xweb");
      ns.removeShare("xweb");
      System.out.println(System.getProperty("java.version"));
      System.out.println(system.getVersion() + " " + system.getVRM() + " " + system.getRelease() + " " + system.getModification());
      
      // ISeriesNetServerFileShare[] liste = ns.listFileShares();
      // for( ISeriesNetServerFileShare elt : liste ){
      // System.out.println("--> " + elt.getName() + " " + elt.getPath());
      // }
      
      // Set the description of the NetServer.
      // Note: Changes will take effect after next start of NetServer.
      // ns.setDescription("The NetServer");
      // ns.commitChanges();
      
      // Set the CCSID of the NetServer to 13488.
      // ns.setCCSID(13488);
      
      // Set the "allow system name" value of the NetServer to true.
      // ns.setAllowSystemName(true);
      
      // Commit the attribute changes (send them to the system).
      // ns.commitChanges();
      
    } /*
      catch (AS400Exception e) {
       AS400Message[] messageList = e.getAS400MessageList();
       for (int i=0; i<messageList.length; i++) {
         System.out.println(messageList[i].getText());
       }
      }*/
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      if (system != null)
        system.disconnectAllServices();
    }
  }
}
