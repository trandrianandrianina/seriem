
package java;

import java.util.ArrayList;

import com.ibm.as400.access.AS400FileRecordDescription;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.RecordFormat;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.system.SystemManager;

// Ne marche pas sur les fichiers cr�� avec create table
// TODO Probl�me � corriger
public class convertDDS2DefinitionFileDB {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    String nomclasse = "psemrtym";
    SystemManager system = new SystemManager("172.31.1.249", "RIDEVSV", "GAR1972", true);
    AS400FileRecordDescription frd =
        new AS400FileRecordDescription(system.getSystem(), "/QSYS.LIB/FMDEV.LIB/" + nomclasse + ".FILE/" + nomclasse + ".MBR");
    ArrayList<String> source = new ArrayList<String>();
    ArrayList<String> initialisation = new ArrayList<String>();
    try {
      RecordFormat[] rf = frd.retrieveRecordFormat();
      System.out.println("-Nbr de membre-> " + rf.length);
      if (rf.length < 1)
        return;
      
      source.add("import database.BaseFileDB;\n");
      source.add("public abstract class FFD_" + frd.getFileName().toLowerCase() + " extends BaseFileDB\n{");
      source.add("\t// Constantes (valeurs r�cup�r�es via DSPFFD)");
      FieldDescription[] lfd = rf[0].getFieldDescriptions();
      for (FieldDescription fd : lfd) {
        String variable = fd.getDDSName();
        source.add("\tpublic static final int SIZE_" + variable + "\t\t\t= " + fd.getLength() + ";");
        if (fd instanceof PackedDecimalFieldDescription)
          source.add("\tpublic static final int DECIMAL_" + variable + "\t\t= "
              + ((PackedDecimalFieldDescription) fd).getDecimalPositions() + ";");
        else if (fd instanceof ZonedDecimalFieldDescription)
          source.add("\tpublic static final int DECIMAL_" + variable + "\t\t= "
              + ((ZonedDecimalFieldDescription) fd).getDecimalPositions() + ";");
      }
      source.add("\n\t// Variables fichiers");
      for (FieldDescription fd : lfd) {
        String type = "int\t\t";
        String init = "0";
        if (fd instanceof CharacterFieldDescription) {
          if (fd.getLength() == 1) {
            type = "char\t";
            init = "' '";
          }
          else {
            type = "String\t";
            init = null;
          }
        }
        source.add("\tprotected " + type + "" + fd.getDDSName() + "\t= " + init + ";\t\t// " + fd.getTEXT());
        initialisation.add("\t\t" + fd.getDDSName() + "\t= " + init + ";");
      }
      
      source.add("\n\t/**\n\t* Initialise les variables avec les valeurs par d�faut\n\t*/");
      source.add("\tpublic void initialization()\n\t{");
      source.addAll(initialisation);
      source.add("\t}");
      
      source.add("}");
      
      for (String line : source) {
        System.out.println(line);
      }
      
      initialisation.clear();
      source.clear();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
  }
  
}
