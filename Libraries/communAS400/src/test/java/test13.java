
package java;

import java.io.IOException;
import java.util.Enumeration;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Exception;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDescription;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ObjectList;
import com.ibm.as400.access.RequestNotSupportedException;

public class test13 {
  
  /**
   * @param args
   * @throws ObjectDoesNotExistException
   * @throws IOException
   * @throws InterruptedException
   * @throws ErrorCompletingRequestException
   * @throws AS400SecurityException
   * @throws AS400Exception
   */
  public static void main(String[] args) throws AS400Exception, AS400SecurityException, ErrorCompletingRequestException,
      InterruptedException, IOException, ObjectDoesNotExistException {
    AS400 sys = new AS400("172.31.1.249", "RIDEVSV", "gar1972");
    ObjectList ol = new ObjectList(sys, "QSYS", "FM*", "*LIB");
    ol.load();
    
    try {
      Enumeration enu = ol.getObjects();
      while (enu.hasMoreElements()) {
        ObjectDescription od = (ObjectDescription) enu.nextElement();
        System.out.println(od.getName() + " " + od.getValue(ObjectDescription.CREATION_DATE));
      }
    }
    catch (RequestNotSupportedException e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
    }
    
  }
  
}
