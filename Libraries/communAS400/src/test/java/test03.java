
package java;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.SystemManager;

public class test03 {
  protected Connection dataBase = null;
  private QueryManager qrm = null;
  
  public test03() {
    SystemManager sys = new SystemManager("172.31.1.249", "SERIEMAD", "GAR1972", true);
    dataBase = sys.getdatabase().getConnection();
    qrm = new QueryManager(dataBase);
  }
  
  public QueryManager getQuery() {
    return qrm;
  }
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    test03 t = new test03();
    
    /*		
     	Ordres via STRSQL pour cr�er la table de test + ins�rer un enregistrement
    	--> create table svas.sql001 (id char(5) not null, nom char(30) not null, bloc clob(10M) allocate(0))                                         
    	--> INSERT INTO SVAS.SQL001 (ID,NOM,BLOC) VALUES ('00001','TEST01', 'Ceci est un texte' )
    */
    /*
    		// Lecture d'un gros fichier texte (ici + de 5Mo)
    		GestionFichierTexte gft = new GestionFichierTexte("C:\\Documents and Settings\\Administrateur\\Bureau\\grosFichier.txt");
    
    		// Cr�ation d'un GenericRecord qui sera utilis� pour manipuler les donn�es 
    		GenericRecord rcd = new GenericRecord();
    		String[] requiredfield = {"ID", "NOM", "BLOC"}; // Les zones qui devront �tre obligatoirement dans la requ�te
    		rcd.setRequiredField(requiredfield);
    		
    		// On initialise les donn�es
    		rcd.setField("ID", "00004");
    		rcd.setField("NOM", "TEST04");
    		rcd.setField("BLOC", gft.getContenuFichierString(false), true);
    		
    		// On cr�� la requ�te d'insertion ou d'update puis affiche la requ�te g�n�r�e
    		//String ret = rcd.createSQLRequestInsert("SQL001", "SVAS");
    		String ret = rcd.createSQLRequestUpdate("SQL001", "SVAS", "ID = '00004'");
    		if (ret != null)
    			System.out.println(ret);
    		else
    			System.out.println(rcd.getMsgError());
    		
    		// On execute la requ�te, on peut utiliser la m�thode classique requete(String) mais si le contenu du clob est trop gros (mais je ne connais pas cette limite) une erreur SQL se d�clenchera
    		// C'est pour cela qu'il est fortement conseill� d'utiliser celle ci-dessous avec le formatage ad�quat de la requ�te
    		if (t.getQuery().requete(ret, rcd) == -1)
    			System.out.println(t.getQuery().getMsgError());
    		
    		// On affiche 
    		//t.getQuery().requete("delete from svas.sql001 where ID = '00004'");
    		 
    		ArrayList<GenericRecord> liste = t.getQuery().select("Select ID, NOM, BLOC from svas.sql001 where ID = '00004'");
    		for (GenericRecord record: liste)
    			System.out.println("--> " + record.getField("NOM") + " - " + /*record.getField("BLOC"));* ((String)record.getField("BLOC")).length());
    			 */
    ArrayList<GenericRecord> liste = t.getQuery().select("Select ID, NOM, BLOC from svas.sql001 where ID = '00004'");
    for (GenericRecord record : liste)
      System.out.println("--> " + ((String) record.getField("BLOC")).length());
  }
  
}
