
package java;

import ri.seriem.libas400.database.record.GenericRecord;

public class test01 {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    GenericRecord rcd = new GenericRecord();
    String[] requiredfield = { "BEETB", "BEMAG", "BEDAT", "BERBV" };
    rcd.setRequiredField(requiredfield);
    rcd.setField("BEETB", "DEM");
    rcd.setField("BEMAG", "MG");
    rcd.setField("BEDAT", new Integer(140624), 6, 0);
    rcd.setField("BERBV", "test 01");
    
    String ret = rcd.getInsertSQL("PINJBDVDSE", "QTEMP");
    if (ret != null)
      System.out.println(ret);
    else
      System.out.println(rcd.getMsgError());
    
  }
  
}
