
package java;

import com.ibm.as400.access.AS400;

import ri.seriem.libas400.system.AS400UserSpace;

public class test17 {
  // Programme associ� CL048 de SVAS pour la lecture du USERSPACE avec un CL
  /**
   * @param args
   */
  public static void main(String[] args) {
    AS400 system = new AS400("172.31.1.249", "RIDEVSV", "gar1972");
    AS400UserSpace upc = new AS400UserSpace(system, "M_GPL", "RIDEVSV");
    
    StringBuffer sb = new StringBuffer(String.format("%-4096s", "Ceci est un test concluant &����"));
    if (upc.create(4096, true)) {
      System.out.println("UserSpace cr��");
      if (upc.writeBlock(0, sb.toString())) {
        System.out.println("Bloc �crit");
      }
      else {
        System.out.println("Bloc non �crit");
      }
      upc.resize(8192);
      System.out.println(upc.readBlock(0));
    }
    else {
      System.out.println("UserSpace non cr�� " + upc.getMsgErreur());
    }
    
  }
  
}
