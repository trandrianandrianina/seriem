
package java;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;

public class test10 {
  public void disconnect() {
    
  }
  
  /**
   * @param args
   * @throws IOException
   * @throws AS400SecurityException
   */
  public static void main(String[] args) throws AS400SecurityException, IOException {
    long t0 = System.currentTimeMillis();
    AS400 sys = new AS400(args[0], args[1], args[2]);
    long t1 = System.currentTimeMillis();
    System.out.println("--> signon:" + (t1 - t0) + " ms");
    
    boolean ret = sys.validateSignon();
    long t2 = System.currentTimeMillis();
    
    System.out.println("--> validation:" + (t2 - t1) + " ms");
    sys.disconnectAllServices();
  }
  
}
