
package java;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.SystemManager;

public class test05 {
  protected Connection dataBase = null;
  private QueryManager qrm = null;
  
  public test05() {
    SystemManager sys = new SystemManager("172.31.1.249", "SERIEMAD", "GAR1972", true);
    dataBase = sys.getdatabase().getConnection();
    qrm = new QueryManager(dataBase);
  }
  
  public QueryManager getQuery() {
    return qrm;
  }
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    test05 t = new test05();
    
    // On affiche
    // ArrayList<GenericRecord> liste = t.getQuery().select("Select ID, NOM, BLOC from svas.sql001 where ID = '00004'");
    ArrayList<GenericRecord> liste = t.getQuery().select("Select * from FM930.PGVMPARM");
    System.out.println("-->" + t.getQuery().getMsgError());
    System.out.println("-->" + liste.size());
    /*
    for (GenericRecord record: liste)
    	System.out.println("-1-> " + /*record.getField("CGV_ID") + " | " + record.getField("BLOC"));* ((String)record.getField("CGV_NAME")));
    
    
    String value = (String) liste.get(0).getField("CGV_NAME");
    value = "tototutu" + value;
    liste.get(0).setField("CGV_NAME", value, true);
    //liste.get(0).removeField("CGV_ID");
    String requete = liste.get(0).createSQLRequestUpdate("CGV", "XWEBSHOP", "WHERE CGV_ID = 2");
    int r = t.getQuery().requete(requete, liste.get(0));
    System.out.println("-retour->" + r + "\n" + t.getQuery().getMsgError());
    
    liste = t.getQuery().select("Select * from XWEBSHOP.CGV");
    System.out.println("-->" + liste.size());		
    for (GenericRecord record: liste)
    	System.out.println("-2-> " + record.getField("CGV_ID") + " | " + /*record.getField("BLOC"));* ((String)record.getField("CGV_NAME")));
    	*/
  }
  
}
