package ri.seriem.libas400.parameters;


public class Societe
{
	// Variables
	private String raisonSociale="";				// Nom ou Raison sociale
	private String complementNom="";
	private String codeEtablissementPilote="";
	private Etablissement etablissementPilote=null;
	
	/**
	 * @return le raisonSociale
	 */
	public String getRaisonSociale()
	{
		return raisonSociale!=null?raisonSociale.trim():"";
	}
	/**
	 * @param raisonSociale le raisonSociale � d�finir
	 */
	public void setRaisonSociale(String raisonSociale)
	{
		this.raisonSociale = raisonSociale;
	}
	/**
	 * @return le complementNom
	 */
	public String getComplementNom()
	{
		return complementNom;
	}
	/**
	 * @param complementNom le complementNom � d�finir
	 */
	public void setComplementNom(String complementNom)
	{
		this.complementNom = complementNom;
	}
	/**
	 * @return le codeEtablissementPilote
	 */
	public String getCodeEtablissementPilote()
	{
		return codeEtablissementPilote;
	}
	/**
	 * @param codeEtablissementPilote le codeEtablissementPilote � d�finir
	 */
	public void setCodeEtablissementPilote(String codeEtablissementPilote)
	{
		if (this.codeEtablissementPilote != codeEtablissementPilote)
		{
			this.codeEtablissementPilote = codeEtablissementPilote;
			setEtablissementPilote(new Etablissement());
		}
	}
	/**
	 * @return le etablissementPilote
	 */
	public Etablissement getEtablissementPilote()
	{
		return etablissementPilote;
	}
	/**
	 * @param etablissementPilote le etablissementPilote � d�finir
	 */
	public void setEtablissementPilote(Etablissement etablissementPilote)
	{
		this.etablissementPilote = etablissementPilote;
		this.etablissementPilote.setCodeEtablissement(codeEtablissementPilote);
	}
	
	
	
}
