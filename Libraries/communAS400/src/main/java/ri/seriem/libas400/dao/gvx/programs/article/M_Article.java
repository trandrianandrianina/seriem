package ri.seriem.libas400.dao.gvx.programs.article;

import ri.seriem.libas400.dao.gvx.database.files.FFD_Pgvmartm;
import ri.seriem.libas400.database.QueryManager;

public class M_Article extends FFD_Pgvmartm
{
	// Constantes
	public		final static	String[][]					ETAT				= {{" ", "Article actif"},
	       		            	          					    				   {"1", "Article d\u00e9sactiv\u00e9"},
	       		            	          					    				   {"2", "Article \u00e9puis\u00e9"},
	       		            	          					    				   {"3", "Syst\u00e8me variable principal"},
	       		            	          					    				   {"4", "Syst\u00e8me variable secondaire"},
	       		            	          					    				   {"5", "Pr\u00e9-fin de s\u00e9rie"},
	       		            	          					    				   {"6", "Fin de s\u00e9rie"}
																					};
	public		final static	String[][]					ETAT_WEB			= {{" ", "En ligne"},
	       		            	          					    				   {"1", "Hors ligne"}
																					};
	// Variables de travail

	
	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_Article(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Ins�re l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean insertInDatabase()
	{
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PGVMARTM", querymg.getLibrary());
		return request(requete);
	}

	/**
	 * Modifie l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean updateInDatabase()
	{
		initGenericRecord(genericrecord, false);
		String requete = genericrecord.createSQLRequestUpdate("PGVMARTM", querymg.getLibrary(), "A1ETB=" + getA1ETB() + " and A1ART='" + getA1ART() + "'");
		return request(requete);
	}

	/**
	 * Suppression de l'enregistrement courant
	 * @return
	 */
	@Override
	public boolean deleteInDatabase()
	{
		String requete = "delete from " + querymg.getLibrary() + ".PGVMARTM where A1ETB=" + getA1ETB() + " and A1ART='" + getA1ART() + "'";
		return request(requete);
	}

	/**
	 * Retourne le nombre d'enregistrements
	 * @return
	 */
	public int getNumberOfRecords(String conditions)
	{
		String request = "select count(a1art) from " + querymg.getLibrary() + ".PGVMARTM";
		if( conditions != null )
			request += " where " + conditions;
		String ret = querymg.firstEnrgSelect(request, 1);
		if( ret == null ){
			msgErreur += '\n' + querymg.getMsgError();
			return -1;
		}
		return Integer.parseInt(ret);
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();
	}

	
	// -- M�thodes priv�es ----------------------------------------------------
	
	
	// -- Accesseurs ----------------------------------------------------------


}
