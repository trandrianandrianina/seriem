package ri.seriem.libas400.dao.exp.programs.contact;

import ri.seriem.libas400.dao.gvm.programs.client.M_Client;
import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.InfosMailServer;

import java.util.ArrayList;

public class GM_Contact extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_Contact(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Retourne un contact � partir de son id
	 * @param id
	 * @return
	 */
	public M_Contact readOneContact(int id){
		if( id < 1 ){
			msgErreur += "\nL'id (RENUM) est inf�rieur � 1.";
			return null;
		}
		if( querymg.getLibrary() == null ){
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PSEMRTEM where RENUM = " + id);
		if( listrcd == null ){
			return null;
		}
		
		return initContact(listrcd);
	}

	/**
	 * Retourne un contact � partir de son alias (reprf)
	 * @param id
	 * @return
	 */
	public M_Contact readOneContact(String alias){
		if( (alias == null) || (alias.trim().length() == 0) ){
			msgErreur += "\nL'alias (REPRF) est inf�rieur null ou blanc.";
			return null;
		}
		if( querymg.getLibrary() == null ){
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PSEMRTEM where REPRF = '" + alias + "'");
		if( listrcd == null ){
			return null;
		}
		
		return initContact(listrcd);
	}

	/**
	 * Retourne les informations du serveur de mail correspondant � l'alias
	 * @param alias
	 * @return
	 */
	public InfosMailServer getInfosMailServer(String alias)
	{
		// R�cup�ration de l'alias dans le fichier PSEMRTYM
		GM_Contact gc = new GM_Contact(querymg);
		M_Contact c = gc.readOneContact(alias);
		
		return c.getExtensionContact().getInfosMailServer();
//System.out.println("-->" + c.getExtensionContact().getRYSRVS());		
	}
	

	/**
	 * Insertion d'un contact li� � un client
	 * @param contact
	 * @param client
	 * @return
	 */
	public boolean insertContact4Client(M_Contact contact, M_Client client)
	{
		if( contact == null ){
			msgErreur += "\nLa classe M_Contact est nulle.";
			return false;
		}

		if( client == null ){
			msgErreur += "\nLa classe M_Client est nulle.";
			return false;
		}
		
		// Insertion dans la table contact
		boolean ret = contact.insertInDatabase();
		if( !ret )
			msgErreur += "\n" + contact.getMsgError();
		else { // Insertion dans la table des liens avec les tiers
			M_LienContactAvecTiers lat = new M_LienContactAvecTiers(querymg);
			lat.setRLCOD(M_LienContactAvecTiers.CLIENT);
			lat.setRLETB(client.getCLETB());
			lat.setRLIND(client.getCLIandLIV());
			lat.setRLETBT(contact.getREETB());
			lat.setRLNUMT(contact.getRENUM());
			ret = lat.insertInDatabase();
			if( !ret )
				msgErreur += "\n" + contact.getMsgError();
		}

		return ret;
	}

	/**
	 * Supprime le contact dans la table des liens et des contacts s'il n'est plus utilis�
	 * @param contact
	 * @param client
	 * @return
	 */
	public boolean deleteContact4Client(M_Contact contact, M_Client client)
	{
		if( contact == null ){
			msgErreur += "\nLa classe M_Contact est nulle.";
			return false;
		}

		if( client == null ){
			msgErreur += "\nLa classe M_Client est nulle.";
			return false;
		}

		// Suppression de la table des liens
		M_LienContactAvecTiers lcat = new M_LienContactAvecTiers(querymg);
		lcat.setRLCOD(M_LienContactAvecTiers.CLIENT);
		lcat.setRLETB(client.getCLETB());
		lcat.setRLIND(client.getCLIandLIV());
		lcat.setRLETBT(contact.getREETB());
		lcat.setRLNUMT(contact.getRENUM());
		boolean ret = lcat.deleteInDatabase();
		
		// On v�rifie que le contact n'ait plus d'�v�nements li�s 
		GM_LienEvenementContact glec = new GM_LienEvenementContact(querymg);
		boolean unengagedInEvenement = glec.checkContactWithoutLink( contact );

		// On v�rifie que le contact n'ait plus de liens avec des tiers  
		GM_LienContactAvecTiers glcat = new GM_LienContactAvecTiers(querymg);
		boolean unengagedInThirdParty = glcat.checkContactWithoutLink( contact );
		
		// Si le contact n'a aucun lien alors on le supprime
		if( unengagedInEvenement && unengagedInThirdParty ){
			ret = contact.deleteInDatabase();
		}
		
		return ret;
	}
	
	
	@Override
	public void dispose()
	{
		querymg = null;
	}

	
	/**
	 * Initialise les donn�es du contact
	 * @param listrcd
	 * @return
	 */
	private M_Contact initContact( ArrayList<GenericRecord> listrcd )
	{
		M_Contact c = new M_Contact(querymg);
		// zones venant du PSEMRTE
		c.initObject(listrcd.get(0), true);
		// zones venant du PSEMRTLM (je ne comprend pas ce que �a fait !! A voir plus tard)
		M_LienContactAvecTiers lcat = new M_LienContactAvecTiers(querymg);
		lcat.initObject(listrcd.get(0), true);
		c.getListLienContactTiers().add(lcat);
		// zones venant du PSEMRTYM
		GM_ContactExtension gec = new GM_ContactExtension(querymg);
		c.setExtensionContact(gec.readOneContact(c.getRENUM()));
		
		return c;
	}
}
