//=================================================================================================
//==>                                                                       27/01/2012 - 27/01/2012
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;


public class PsemenvmManager extends QueryManager
{

	/**
	 * Constructeur
	 * @param database
	 */
	public PsemenvmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Retourne l'enregistrement pour un environnement
	 * @param bibenv
	 * @param prf
	 * @return
	 */
	public GenericRecord getRecordForBibEnv(String bibenv)
	{
		GenericRecord psemenvm = null; 
		final ArrayList<GenericRecord> listeRecord = select("Select * from QGPL.psemenvm where ENVCLI = '" + bibenv +"'");
		
		if ((listeRecord != null) && (listeRecord.size() > 0))
			psemenvm = listeRecord.get(0); 
		else
			msgErreur += "\n[PsemenvmManagement] (getRecordForBibEnv) Environnement " + bibenv + " non trouv� dans le fichier QGPL.PSEMENVM";
		
		return psemenvm;
	}

    
}
