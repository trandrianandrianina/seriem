//=================================================================================================
//==>                                                                       26/06/2014 - 04/11/2014
//==> Gestion d'une biblioth�que
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

public class Library
{
	public static final String EXTENSION="LIB";
	
	private IFSFile library = null;
	protected AS400 system=null;
	protected String name=null;

	protected String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 

	/**
	 * Constructeur
	 * @param sys
	 */
	public Library(AS400 sys)
	{
		setSystem(sys);
	}

	/**
	 * Constructeur
	 * @param sys
	 * @param aname
	 */
	public Library(AS400 sys, String aname)
	{
		this(sys);
		setName(aname);
	}

	/**
	 * V�rifie l'existence de la biblioth�que
	 * @return
	 */
	public boolean isExists()
	{
		if (library == null) return false;
		
		try
	    {
			return library.exists();
	    }
	    catch (IOException ioe) 
	    {
	    	return false;
	    }
	}
	
	/**
	 * Cr�er la biblioth�que si elle n'existe pas
	 * @return
	 */
	public boolean create()
	{
		if (library == null) return false;
		
		try
	    {
			if (!library.exists()) 
				return library.mkdir();
	    }
	    catch (IOException ioe) 
	    {
	    	return false;
	    }
		return true;
	}

	/**
	 * Cr�er la biblioth�que sans tester son l'existence auparavent
	 * @return
	 */
	public boolean createWOCheckExists()
	{
		if (library == null) return false;
		
		try
	    {
			return library.mkdir();
	    }
	    catch (IOException ioe) 
	    {
	    	return false;
	    }
	}

	/**
	 * Supprime la biblioth�que
	 * @return
	 */
	public boolean delete()
	{
		if (library == null) return false;
		
		try
	    {
			if (library.exists())
			{
				// On commence par vider le contenu
				IFSFile[] contenu = library.listFiles();
				if (contenu.length > 0)
				{
					CallProgram cmd = new CallProgram(system);
					return cmd.execute("DLTLIB LIB("+name+")");
				}
				/* Ne fonctionne pas sur les objets
				for (IFSFile file: contenu)
				{
					boolean ret = file.delete();
				}*/
				
				// Enfin on supprime la biblioth�que
				return library.delete();
			}
	    }
	    catch (IOException ioe) 
	    {
	    	System.out.println("--> " + ioe);
	    	return false;
	    }
		return true;
	}
	

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le system
	 */
	public AS400 getSystem()
	{
		return system;
	}

	/**
	 * @param system le system � d�finir
	 */
	public void setSystem(AS400 system)
	{
		this.system = system;
	}

	/**
	 * @return le name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name le name � d�finir
	 */
	public void setName(String name)
	{
		this.name = name;
		if ((name == null) || (name.trim().equals("")) || (system == null))
		{
			library = null;
			return;
		}
		
		library = new IFSFile(system, QSYSObjectPathName.toPath("QSYS", name, EXTENSION));
	}
}
