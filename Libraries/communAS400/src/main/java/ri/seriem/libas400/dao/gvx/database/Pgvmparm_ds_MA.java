//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_MA pour les MA
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_MA extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "MALIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "MASAN"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "MADPR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "MAA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MARSA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAELV"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MADEP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAINV"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAREA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "MASMA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAFAC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAEAS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MADBC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "MANCG"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "MAJIT"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "MAJIR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MADAS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "MATYP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "MAAMA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "MAINT"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "MANBV"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "MANSA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAGES"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MAADS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MATPF"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "MAPCS"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "MALCH"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "MALCM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MANBP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MARES"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "MAASTK"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "MAMSTY"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "MATYGR"));

		length = 300;
	}
}
