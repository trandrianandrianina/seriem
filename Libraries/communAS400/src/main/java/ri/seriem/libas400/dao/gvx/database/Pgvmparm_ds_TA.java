//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_TA pour les TA
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_TA extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "TALIB"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "TADAP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(30, 0), "TACO")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(100), "TALB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN3"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TAART"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(13), "TATIR"));

		length = 300;
	}
}
