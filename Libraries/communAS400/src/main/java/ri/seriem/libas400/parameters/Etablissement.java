package ri.seriem.libas400.parameters;

public class Etablissement
{
	// Variables
	private String nom="";				// Nom ou Raison sociale
	private String complementNom="";
	private String codeEtablissement="";
	private String magasinGeneral="";
	private Personnalisations ps=null;
	

	/**
	 * @return le nom
	 */
	public String getNom()
	{
		return nom;
	}

	/**
	 * @param nom le nom � d�finir
	 */
	public void setNom(String nom)
	{
		this.nom = nom;
	}

	/**
	 * @return le complementNom
	 */
	public String getComplementNom()
	{
		return complementNom;
	}

	/**
	 * @param complementNom le complementNom � d�finir
	 */
	public void setComplementNom(String complementNom)
	{
		this.complementNom = complementNom;
	}

	/**
	 * @return le codeEtablissement
	 */
	public String getCodeEtablissement()
	{
		return codeEtablissement;
	}

	/**
	 * @param codeEtablissement le codeEtablissement � d�finir
	 */
	public void setCodeEtablissement(String codeEtablissement)
	{
		this.codeEtablissement = codeEtablissement;
	}

	/**
	 * @return le magasinGeneral
	 */
	public String getMagasinGeneral()
	{
		return magasinGeneral;
	}

	/**
	 * @param magasinGeneral le magasinGeneral � d�finir
	 */
	public void setMagasinGeneral(String magasinGeneral)
	{
		if (magasinGeneral == null)
			this.magasinGeneral = "";
		else
			this.magasinGeneral = magasinGeneral.substring(0, 2);
	}

	/**
	 * @return le ps
	 */
	public Personnalisations getPs()
	{
		if (ps == null)
			ps = new Personnalisations();
		return ps;
	}

	/**
	 * @param ps le ps � d�finir
	 */
	public void setPs(Personnalisations ps)
	{
		this.ps = ps;
	}
	
}
