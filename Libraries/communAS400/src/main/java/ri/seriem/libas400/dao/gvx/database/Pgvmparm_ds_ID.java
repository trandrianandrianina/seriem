//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_ID pour les ID
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_ID extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "IDRUE"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "IDLOC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "IDVIL"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "IDCGP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "IDNCG"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "IDCGP2"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "IDCGP3"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "IDCGP4"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(15), "IDNII"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "IDCNU"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "IDCG1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "IDCG2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "IDCG3"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "IDCG4"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "IDTEL"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "IDFAX"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "IDADH"));

		length = 300;
	}
}
