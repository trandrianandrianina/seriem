//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_CO pour les CO
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_CO extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "CONCG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "COLIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CONSA"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "CODGR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "COSAN"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "COLI1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "COLI2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "COCOT1"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "COCOS1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "COCOT2"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "COCOS2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "COCOL"));

		length = 300;
	}
}
