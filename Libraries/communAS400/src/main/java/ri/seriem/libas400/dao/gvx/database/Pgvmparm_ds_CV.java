//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_CV pour les CV
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_CV extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(50, 0), "CVCT")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "CVCOL"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(36), "CVS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "CVETC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVCJV"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVCJR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVCPC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVSEC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVSER"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVREF"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVATT"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CVDIF"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CVCOA1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVJOA1"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CVCOA2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVJOA2"));

		length = 300;
	}
}
