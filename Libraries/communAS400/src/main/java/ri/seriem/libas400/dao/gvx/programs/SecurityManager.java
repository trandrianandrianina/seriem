package ri.seriem.libas400.dao.gvx.programs;

import ri.seriem.libas400.dao.gvx.database.PgvmsecmManager;
import ri.seriem.libas400.database.DatabaseManager;
import ri.seriem.libas400.database.record.ExtendRecord;



public class SecurityManager
{
	// Variables
	private DatabaseManager database=null;
	private PgvmsecmManager pgvmsecm=null;

	/**
	 * Constructeur
	 * @param adatabase
	 * @param bibuser
	 */
	public SecurityManager(DatabaseManager adatabase, String abibuser)
	{
		database = adatabase;
		if (database == null) return;
		
		pgvmsecm = new PgvmsecmManager(database.getConnection());
		pgvmsecm.setLibrary(abibuser);
	}
	
	/**
	 * Retourne s'il y a une s�curit� globale
	 * @param listUser
	 * @return
	 */
	public boolean isGlobalSecurity(String[] listUser)
	{
		if (pgvmsecm == null) return false;
		
		// On recherche l'enregistrement pour la s�curit� globale 
		ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB("", "");
		if (record == null) return false;
		
		// On r�cup�re la liste des users qui ont tous les droits
		if (listUser != null)
			for (int i=0; i<listUser.length; i++)
				listUser[i] = ((String)record.getField(7+i)).trim();
		
		return ((String)record.getField("SEACT")).equals("OUI");
	}
	
	/**
	 * Retourne l'enregistrement de la s�curit� utilisateur
	 * @param dgetb
	 * @return
	 */
	public ExtendRecord getUserSecurity(String auser, String dgetb)
	{
		if (pgvmsecm == null) return null;
		if (auser == null) return null;
		
		// On recherche l'�tablissement de l'utilisateur s'il y en a un
		String etb = pgvmsecm.getEtablissementUser(auser);
		// Sinon on prend celui de la DG
		if (etb == null)
			etb = dgetb;
		
		// Lecture de l'enregistrement de la s�curit� utilisateur
		ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB(auser, etb);
		if (record == null) return null;
		
		// On contr�le s'il n'y a pas une s�curit� de groupe
		String usergrp = ((String)record.getField("SEGRP")).trim();
		// Si le profil de groupe est � blanc alors pas de s�curit� de groupe, on retourne directement l'enregitrement d�j� lu
		if (usergrp.equals(""))
			return record;
		// On retourne la s�curit� du profil de groupe
		return pgvmsecm.getRecordsbyUSERandETB(usergrp, etb);
	}
}
