package ri.seriem.libas400.dao.exp.database.files;


import ri.seriem.libas400.database.BaseFileDB;
import ri.seriem.libas400.database.QueryManager;

public abstract class FFD_Psemlecm extends BaseFileDB
{
	// Constantes (valeurs r�cup�r�es via DSPFFD)
	public static final int SIZE_ECID		= 9;
	public static final int DECIMAL_ECID	= 0;
	public static final int SIZE_ECIDC		= 9;
	public static final int DECIMAL_ECIDC	= 0;
	public static final int SIZE_ECETBC		= 3;
	public static final int SIZE_ECIDE		= 9;
	public static final int DECIMAL_ECIDE	= 0;
	public static final int SIZE_ECTYP		= 2;
	public static final int SIZE_ECHIE		= 9;
	public static final int DECIMAL_ECHIE	= 0;

	// Variables fichiers
	private int		ECID	= 0;			// L'ID
	private int		ECIDC	= 0;			// L'ID Contact
	private String	ECETBC	= null;			// Etablissement du contact
	private int		ECIDE	= 0; 			// L'ID Ev�nement
	private String	ECTYP	= null;			// Type du contact (Cr�ateur, Executant, Cible)
	private int		ECHIE	= 0; //NO_HIERARCHY;	// Code responsabilit� (hi�rarchie des intervenants qui doivent r�aliser la tache ds le cas d'une tache � plusieurs comme du phoning par ex ou d'un salon)
											// La valeur la plus faible correspond au niveau de hi�rarchie la plus haute
	
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public FFD_Psemlecm(QueryManager aquerymg)
	{
		super(aquerymg);
	}
	
	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Initialise les variables avec les valeurs par d�faut
	 */
	public void initialization()
	{
		ECID	= 0;
		ECIDC	= 0;
		ECETBC	= null;
		ECIDE	= 0;
		ECTYP	= null;
		ECHIE	= 0; //NO_HIERARCHY;
	}
	
	// -- Accesseurs ----------------------------------------------------------

	/**
	 * @return le eCID
	 */
	public int getECID()
	{
		return ECID;
	}

	/**
	 * @param eCID le eCID � d�finir
	 */
	public void setECID(int eCID)
	{
		ECID = eCID;
	}

	/**
	 * @return le eCIDC
	 */
	public int getECIDC()
	{
		return ECIDC;
	}


	/**
	 * @param eCIDC le eCIDC � d�finir
	 */
	public void setECIDC(int eCIDC)
	{
		ECIDC = eCIDC;
	}


	/**
	 * @return le eCETBC
	 */
	public String getECETBC()
	{
		return ECETBC;
	}


	/**
	 * @param eCETBC le eCETBC � d�finir
	 */
	public void setECETBC(String eCETBC)
	{
		ECETBC = eCETBC;
	}


	/**
	 * @return le eCIDE
	 */
	public int getECIDE()
	{
		return ECIDE;
	}


	/**
	 * @param eCIDE le eCIDE � d�finir
	 */
	public void setECIDE(int eCIDE)
	{
		ECIDE = eCIDE;
	}


	/**
	 * @return le eCTYP
	 */
	public String getECTYP()
	{
		return ECTYP;
	}


	/**
	 * @param eCTYP le eCTYP � d�finir
	 */
	public void setECTYP(String eCTYP)
	{
		ECTYP = eCTYP;
	}


	/**
	 * @return le eCHIE
	 */
	public int getECHIE()
	{
		return ECHIE;
	}


	/**
	 * @param eCHIE le eCHIE � d�finir
	 */
	public void setECHIE(int eCHIE)
	{
		ECHIE = eCHIE;
	}

}
