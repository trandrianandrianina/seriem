package ri.seriem.libas400.dao.exp.programs.contact;

import ri.seriem.libas400.dao.exp.database.files.FFD_Psemrtlm;
import ri.seriem.libas400.database.QueryManager;

public class M_LienContactAvecTiers extends FFD_Psemrtlm
{
	// Constantes
	public static final char CLIENT			= 'C';
	public static final char FOURNISSEUR	= 'F';
	public static final char PROSPECT		= 'P';
	
	
	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_LienContactAvecTiers(QueryManager aquerymg)
	{
		super(aquerymg);
	}
	
	/**
	 * Ins�re un lien contact dans le table
	 * @return
	 */
	@Override
	public boolean insertInDatabase()
	{
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PSEMRTLM", querymg.getLibrary());
		return request(requete);
	}

	/**
	 * Modifie un lien contact dans le table
	 * @return
	 */
	@Override
	public boolean updateInDatabase()
	{
		initGenericRecord(genericrecord, false);
		String requete = genericrecord.createSQLRequestUpdate("PSEMRTLM", querymg.getLibrary(), "RLCOD='" + getRLCOD() + "' and RLETB='" + getRLETB() + "' and RLIND='" + getRLIND() + "' and RLETBT='" + getRLETBT() + "' and RLNUMT=" + getRLNUMT());
		return request(requete);
	}

	/**
	 * Suppression de l'enregistrement courant ou de tous
	 * @return
	 */
	@Override
	public boolean deleteInDatabase()
	{
		return deleteInDatabase(false);
	}
	
	/**
	 * Suppression de l'enregistrement courant ou de tous
	 * @return
	 */
	public boolean deleteInDatabase(boolean deleteAll)
	{
		String requete = "delete from " + querymg.getLibrary() + ".PSEMRTLM where RLCOD='" + getRLCOD() + "' and RLETB='" + getRLETB() + "' and RLIND='" + getRLIND() + "'";
		if( !deleteAll )
			requete += " and RLETBT='" + getRLETBT() + "' and RLNUMT=" + getRLNUMT();
		return request(requete);
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();
	}
	

	/**
	 * @return le contactPrincipal
	 */
	public boolean isContactPrincipal()
	{
		return RLIN1 != ' '?true:false;
	}

	/**
	 * @param contactPrincipal le contactPrincipal � d�finir
	 */
	public void setContactPrincipal(boolean contactPrincipal)
	{
		if( contactPrincipal )
			setRLIN1('P');
		else
			setRLIN1(' ');
	}

	/**
	 * @param contactPrincipal le contactPrincipal � d�finir
	 */
	public void setContactPrincipal(String contactPrincipal)
	{
		if( (contactPrincipal != null) && ( contactPrincipal.length() > 0 )  && (contactPrincipal.charAt(0) != ' ') )
			setContactPrincipal(true);
		else
			setContactPrincipal(false);
	}

	/**
	 * @return le contactWebShop
	 */
	public boolean isContactWebShop()
	{
		return RLIN2 == 'W'?true:false;
	}

	/**
	 * @param contactWebShop le contactWebShop � d�finir
	 */
	public void setContactWebShop(boolean contactWebShop)
	{
		if( contactWebShop )
			setRLIN2('W');
		else
			setRLIN2(' ');
	}

	/**
	 * @param contactWebShop le contactWebShop � d�finir
	 */
	public void setContactWebShop(String contactWebShop)
	{
		if( (contactWebShop != null) && ( contactWebShop.length() > 0 )  && (contactWebShop.charAt(0) != ' ') )
			setContactWebShop(true);
		else
			setContactWebShop(false);
	}

	/**
	 * @return le typeWorkflow
	 */
	public char getTypeWorkflow()
	{
		return RLIN3;
	}

	/**
	 * @param typeWorkflow le typeWorkflow � d�finir
	 */
	public void setTypeWorkflow(char typeWorkflow)
	{
		setRLIN3( typeWorkflow );
	}

	/**
	 * @param typeWorkflow le typeWorkflow � d�finir
	 */
	public void setTypeWorkflow(String typeWorkflow)
	{
		if( (typeWorkflow != null) && (typeWorkflow.length() > 0) )
			setRLIN3( typeWorkflow.charAt(0) );
		else
			setRLIN3(' ');
	}
	
	// -- M�thodes priv�es ----------------------------------------------------
	


}
