//=================================================================================================
//==>                                                                       10/06/2013 - 10/06/2013
//==> Algo de d�chiffrage de la s�curit� dans S�rie M (algo dans QRPGSRC/SGVXSE)
//==> Fonctionne apr�s la conversion en ASCII des donn�es EBCDIC 
//=================================================================================================
package ri.seriem.libas400.security;

public class DeChiffreAscii
{
	private static final char[] wca={' ','<','>','1','A','Q','W','2','Z','S','X','3','E','D','C','4','R','F','V',
	                                 '5','T','G','B','6','Y','H','N','7','U','J','?','8','I','K','.','9','O','L',
	                                 '/','0','P','M','+','�','�','%','_','*','�','&','�','"','\'','(','�','�','!',
	                                 '�','�',')','-','^','$','�','�',',',';',':','=',' ',' ', ' '};
	

	/**
	 * Algo de d�chiffrage de la s�curit� S�rie M/N
	 * @param array1
	 * @param array2
	 * @return
	 *
	public char[] getDecryptSecurity(char[] array1, char[] array2)
	{
		char[] wc = new char[array1.length + array2.length];
		System.arraycopy(array1, 0, wc, 0, array1.length);
		System.arraycopy(array2, 0, wc, array1.length, array2.length);
		boolean l1, l2, in01;
		int xs;
		for (int xt=0; xt<wc.length; xt++)
		{
			in01 = false;
			l1 = (((xt+1) % 2) == 0);
			l2 = (((xt+1) % 3) == 0);
			// Recherche le caract�re dans la cl�
			for (xs=0; xs<wca.length; xs++)
				if (wc[xt] == wca[xs])
				{
					in01 = true;
					break;
				}
			if (in01)
			{
				xs++;
				xs = 72 - xs;
				if (l1) xs--;
				//if ((l2) || (xs < 0)) xs++;
				if (l2) xs++;
				wc[xt] = wca[xs-1];
			}
		}

		return wc;
	}*/

	/**
	 * Algo de d�chiffrage de la s�curit� S�rie M/N
	 * @param array1
	 * @param array2
	 * @return
	 */
	public char[] getDecryptSecurity(char[] array1)
	{
		char[] wc = new char[array1.length];
		System.arraycopy(array1, 0, wc, 0, array1.length);
		boolean l1, l2, in01;
		int xs;
		for (int xt=0; xt<wc.length; xt++)
		{
			in01 = false;
			l1 = (((xt+1) % 2) == 0);
			l2 = (((xt+1) % 3) == 0);
			// Recherche le caract�re dans la cl�
			for (xs=0; xs<wca.length; xs++)
				if (wc[xt] == wca[xs])
				{
					in01 = true;
					break;
				}
			if (in01)
			{
				xs++;
				xs = 72 - xs;
				if (l1) xs--;
				//if ((l2) || (xs < 0)) xs++;
				if (l2) xs++;
				wc[xt] = wca[xs-1];
			}
		}

		return wc;
	}

}
