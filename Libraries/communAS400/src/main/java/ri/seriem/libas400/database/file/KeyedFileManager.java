//=================================================================================================
//==>                                                                       26/03/2012 - 26/03/2012
//==> G�re les acc�s direct au fichier DB2 
//=================================================================================================
package ri.seriem.libas400.database.file;

import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400File;
import com.ibm.as400.access.AS400FileRecordDescription;
import com.ibm.as400.access.KeyedFile;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;

import ri.seriem.libas400.database.record.DataStructureRecord;

public class KeyedFileManager
{
	// Variables
	private AS400 systeme = null;
	private KeyedFile fichierDB = null;
	private RecordFormat recordFormat = null;
	private int nbrEnregistrement=100;
	//private Object[] theKey=null;
	
	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

	/**
	 * Constructeur
	 * @param asysteme
	 */
	public KeyedFileManager(AS400 asysteme)
	{
		systeme = asysteme;
	}
	
	/**
	 * Ouverture du fichier
	 * @param bibliotheque
	 * @param fichier
	 * @return
	 */
	public boolean openFile(String bibliotheque, String fichier)
	{
		final QSYSObjectPathName fileName = new QSYSObjectPathName(bibliotheque, fichier, "*FILE", "MBR");
		fichierDB = new KeyedFile(systeme, fileName.getPath());
		try
		{
			systeme.connectService(AS400.RECORDACCESS);
			AS400FileRecordDescription recordDescription = new AS400FileRecordDescription(systeme, fileName.getPath());
			recordFormat = recordDescription.retrieveRecordFormat()[0];
			fichierDB.setRecordFormat(recordFormat);
			fichierDB.open(AS400File.READ_WRITE, nbrEnregistrement, AS400File.COMMIT_LOCK_LEVEL_NONE);
		}
		catch (Exception e)
		{
			msgErreur += "\nEchec de l'ouverture du fichier : " + e;
			return false;
		}
		
		return true;
	}
	
	/**
	 * Fermeture du fichier
	 */
	public void closeFile()
	{
		try
		{
			if (fichierDB != null)
				fichierDB.close();
		}
		catch (Exception e)	{}
		//System.exit(0);
		//return;
	}


	public ArrayList<Record> getListRecord(Object[] theKey, DataStructureRecord dsrecord)
	{		
		if ((fichierDB == null) || (theKey == null)) return null;
		
		ArrayList<Record> liste = new ArrayList<Record>();
		try
		{
			Record data = fichierDB.read(theKey);
			while (data != null)
			{
				liste.add(dsrecord.setBytes(data.getContents()));
				data = fichierDB.readNextEqual(theKey);
			}
		}
		catch (Exception e)
		{
			msgErreur += "\nErreur durant la lecture du fichier " + e;
		}
		return liste;
	}
	

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
