package ri.seriem.libas400.system.systemrequest;

import java.io.IOException;
import java.util.Enumeration;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.ObjectDescription;
import com.ibm.as400.access.ObjectList;

import ri.seriem.libcommun.protocoleMsg.LibraryRequest;

public class LibraryRequestActions
{
	private AS400 sysAS400=null;
	private LibraryRequest rlib=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public LibraryRequestActions(AS400 system, LibraryRequest lr)
	{
		sysAS400 = system;
		rlib = lr;
	}
	
	/**
	 * Traitement des actions possibles 
	 */
	public void actions()
	{
		//int theactions = actions;
		for (int i=LibraryRequest.ACTIONS.size(); --i>0;)
		{
			if (rlib.getActions() >= LibraryRequest.ACTIONS.get(i))
			{
				switch(LibraryRequest.ACTIONS.get(i))
				{
					case LibraryRequest.ISEXISTS:
						libraryExists();
						break;
					case LibraryRequest.GETTEXT:
						libraryText();
						break;
					case LibraryRequest.CREATE:
						libraryCreate();
						break;
					case LibraryRequest.LIST:
						libraryList();
						break;
					//case LibraryRequest.LISTINTO:
					//	libraryListInto();
					//	break;
				}
				rlib.setActions(rlib.getActions()- LibraryRequest.ACTIONS.get(i));
			}
		}
	}

	/**
	 * Traite la demande de vérification d'une bibliothèque
	 * Attention si le nom de lib est par exemple FM450* il va retourner SUCCESS
	 */
	private void libraryExists()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setExist(false);
			rlib.setMsgError("Le nom de la bibliothèque est incorrect : " + library);
			return;
		}
		
		//QSYSObjectPathName lib = new QSYSObjectPathName(nomlib);
        final String racine = "/QSYS.LIB/" + library + ".LIB";
        final IFSFile bib = new IFSFile(sysAS400, racine);
        try
		{
        	rlib.setSuccess(true);
        	rlib.setMsgError("La bibliothèque " + library + " existe.");
			rlib.setExist(bib.exists());
		}
		catch (IOException e)
		{
			rlib.setSuccess(false);
			rlib.setExist(false);
			rlib.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la création d'une bibliothèque
	 */
	private void libraryCreate()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setMsgError("Le nom de la bibliothèque est incorrect : " + library);
			return;
		}
		
		// On vérifie que la library n'existe pas déjà
		libraryExists();
		if (!rlib.isExist())
		{
			rlib.setSuccess(false);
			rlib.setText(null);
			return;
		}

		// Sinon on l'a créé
		CommandCall cmd = new CommandCall(sysAS400, "QSYS/CRTLIB LIB("+library+")");
		try
		{
			// cmd.getMessageList());
			rlib.setMsgError(null);
			rlib.setSuccess(cmd.run());
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la demande de récupération du libellé d'une bibliothèque
	 */
	private void libraryText()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setMsgError("Le nom de la bibliothèque est incorrect : " + library);
			rlib.setText(null);
		}

		// On vérifie que la library n'existe pas déjà
		libraryExists();
		if (!rlib.isExist())
		{
			rlib.setSuccess(false);
			rlib.setText(null);
			return;
		}

		ObjectDescription od = new ObjectDescription(sysAS400, "/QSYS.LIB/" + library + ".LIB");
		try
		{
			rlib.setSuccess(true);
			rlib.setMsgError(null);
			rlib.setText(od.getValueAsString(ObjectDescription.TEXT_DESCRIPTION));
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
			rlib.setText(null);
		}
	}

	/**
	 * Traite la demande de listage d'une bibliothèque
	 */
	private void libraryList()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setMsgError("Le nom de la bibliothèque est incorrect : " + library);
			rlib.setText(null);
		}
		library = library.toUpperCase();
		rlib.getVariant().clear();
		rlib.setTypeVariant("LIST_NAMELIB");

		try
		{
			ObjectList ol = new ObjectList(sysAS400, "QSYS", library, "*LIB");
			ol.load();
		
			Enumeration enu = ol.getObjects();
			while(enu.hasMoreElements()){
				ObjectDescription od = (ObjectDescription) enu.nextElement();
				rlib.getVariant().add(od.getName());
			}
			
			rlib.setSuccess(true);
			rlib.setMsgError(null);
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la demande de listage d'une bibliothèque
	 *
	private void libraryListInto()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setMsgError("Le nom de la bibliothèque est incorrect : " + library);
			rlib.setText(null);
		}
		library = library.toUpperCase();
		rlib.getVariant().clear();
		rlib.setTypeVariant("NAMEOBJ");

		try
		{
			ObjectList ol = new ObjectList(sysAS400, "QSYS", library, "*LIB");
			ol.load();
		
			Enumeration enu = ol.getObjects();
			while(enu.hasMoreElements()){
				ObjectDescription od = (ObjectDescription) enu.nextElement();
				rlib.getVariant().put(od.getName(), null);
			}
			
			rlib.setSuccess(true);
			rlib.setMsgError(null);
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}*/

}
