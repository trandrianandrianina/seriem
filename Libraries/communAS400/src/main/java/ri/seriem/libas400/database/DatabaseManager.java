//=================================================================================================
//==>                                                                       23/12/2009 - 28/04/2015
//==> Permet de lancer des requ�tes sur un I5
//==> A faire:
//==> A Voir:
//==>	 https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/jdbc.htm
//==>	 https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/conjdbc.htm
//==>	 https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/conjdbc.htm
//
//==>    https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/jdbc.htm
//=================================================================================================
// Le jar db2_classes.jar est terouv� dans \\172.31.1.249\qibm\ProdData\OS400\Java400\ext


// A voir
// http://www.sqlthing.com/resources/UsingQAQQINI.htm
// http://www.ibm.com/developerworks/data/library/techarticle/dm-1001maxsqewithdds/
// http://www.itjungle.com/mpo/mpo052203-story02.html
// http://publib.boulder.ibm.com/iseries/v5r2/ic2924/index.htm?info/rzahh/javadoc/JDBCProperties.html


package ri.seriem.libas400.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class DatabaseManager
{
	// Constantes
	public final static int NODEBUG = 0;
	public final static int DEB_VERBOSE = 1;	// Pour ces valeurs n'utiliser que des puissances de 2
	public final static int DEB_NOCACHE = 2;	// c'est n�cessaire pour retrouver ensuite les options
	
	private final static String REQ_VERBOSE = "UPDATE QTEMP.QAQQINI SET QQVAL='*YES' WHERE QQPARM='MESSAGES_DEBUG'";
	private final static String REQ_NOCACHE = "UPDATE QTEMP.QAQQINI SET QQVAL='*NONE' WHERE QQPARM='CACHE_RESULTS'";
	
	// Variables
	private boolean driverNative 				= true;
    private boolean initWithContext 			= false;			// Indique si la connexion a �t� initialis� avec un context (Tomcat)  
    private Connection connection 				= null;
	private String msgErreur  					= "";				// Conserve le dernier message d'erreur �mit et non lu
	private Properties properties				= new Properties();	// Stocke les propri�t�s (options) que l'on souhaite activer
	private ArrayList<String>qaqqiniRequetes 	= new ArrayList<String>();

	/**
	 * Constructeur
	 */
	public DatabaseManager()
	{
	}

	/**
	 * Constructeur
	 * @param drivernative
	 */
	public DatabaseManager(boolean drivernative)
	{
		driverNative = drivernative;
	}
	
    /**
     * Enregistrement du driver natif
     * @return
     * @throws SQLException 
     */
    private boolean registerDriverNative()
	{
    	try{
            //Class.forName("com.ibm.db2.jdbc.app.DB2Driver");
    		DatabaseManager.class.getClassLoader().loadClass("com.ibm.db2.jdbc.app.DB2Driver");
    		//Class.forName("com.ibm.db2.jcc.DB2Driver");
    		//DriverManager.registerDriver(new com.ibm.db2.jcc.DB2Driver());
    		//DriverManager.registerDriver(new com.ibm.db2.jdbc.app.DB2Driver());
         } catch (Exception e){
            msgErreur += "Erreur: les drivers JDBC natif n'ont pas �t� charg�.\n" + e;
            driverNative = false;
            return false;
         }

    	return true;
    }

    /**
     * Enregistrement du driver toolbox
     * @return
     * @throws SQLException 
     */
    private boolean registerDriverToolbox()
	{
    	//DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
    	try{
            //Class.forName("com.ibm.as400.access.AS400JDBCDriver");
    		DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
         } catch (SQLException e) {
        	 msgErreur += "Erreur: les drivers JDBC toolbox n'ont pas �t� charg�.\n" + e;
        	 return false;
         }
    	return true;
    }

    /**
     * Connection au serveur
     * @param serveur
     * @param user
     * @param password
     * @return
     */
    public boolean connexion(String serveur, String user, String password)
	{
    	if( driverNative ){
    		try
    		{
    			if( registerDriverNative()){
    				if( serveur.toLowerCase().equals( "localhost" ) || serveur.toLowerCase().equals( "127.0.0.1" ) ){
    					properties.setProperty("user", user);
    					properties.setProperty("password", password);
    					properties.setProperty("prompt", "false");
    					//properties.setProperty("libraries", "QTEMP");
    					connection = DriverManager.getConnection("jdbc:db2://*LOCAL", properties);
    				} else{
    					properties.setProperty("user", user);
    					properties.setProperty("password", password);
    					connection = DriverManager.getConnection("jdbc:db2://" + serveur, properties);
    				}
    			} else{
    				driverNative = false;
    			}
    		}
    		catch (Exception exc)
    		{
    			msgErreur += "\nEchec de l'initialisation des drivers natifs (on tente avec la Toolbox)";
				driverNative = false;
    		}
    	}
    	
    	if( !driverNative ){
    		try
    		{
    			if( registerDriverToolbox()){
    				if (serveur.trim().equalsIgnoreCase( "*local" )){
    					serveur = "LOCALHOST";
    				}
					properties.setProperty("user", user);
					properties.setProperty("password", password);
					properties.setProperty("translate binary", "true"); // Permet de convertir en unicode si CCSID du fichier AS400 � 65535
					//properties.setProperty("ccsid", "297");
					
					//properties.setProperty("char.encoding", "ISO-8859-15");
					//properties.setProperty("char.encoding", "UTF-8");
					//properties.setProperty("characterEncoding", "UTF-8");
					//properties.setProperty("characterEncoding", "CP858");
					//properties.setProperty("characterEncoding", "WINDOWS-1252");
//System.out.println("-on passe ici->" + properties.getProperty("ccsid"));
					
					connection = DriverManager.getConnection("jdbc:as400://" + serveur, properties);
					//connection = DriverManager.getConnection("jdbc:as400://" + serveur + ";user="+user+";password="+password+";translate binary=true;ccsid=870;");
    			} else{
    				return false;
    			}
    		}
    		catch (Exception exc)
    		{
    			msgErreur += "\nEchec de la connexion: " + exc.getMessage();
    			return false;
    		}
    	}

    	msgErreur += "\nConnexion avec les drivers natifs: " + driverNative;
    	
    	// Initialise un Qaqqini si n�cessaire
    	prepareQaqqini();
    	
        return true;
    }

    /**
     * Connection au serveur
     * @return
     */
    public boolean connexion()
	{
    	if( driverNative ){
    		try{
    			if( registerDriverNative() ){
    				properties.setProperty("prompt", "false");
					//properties.setProperty("libraries", ",QTEMP,QGPL");
					connection = DriverManager.getConnection("jdbc:db2://LOCALHOST", properties); // <-- on peut utiliser *LOCAL si on veut
    			}
    		}
    		catch(Exception e){
    			msgErreur += "\nEchec de l'initialisation des drivers natifs (on tente avec la Toolbox)";
    			driverNative = false;
    		}
    	}

    	if( !driverNative ){
    		try
    		{
    			if( registerDriverToolbox() ){
    				properties.setProperty("prompt", "false");
					//properties.setProperty("libraries", "QTEMP");
    				connection = DriverManager.getConnection("jdbc:as400://LOCALHOST", properties); // <-- ne pas utiliser *LOCAL car �a crashe
    			} else{
    				return false;
    			}
    		}
    		catch (Exception exc)
    		{
    			msgErreur += "\nEchec de la connexion: " + exc.getMessage();
    			return false;
    		}
    	}
    	
    	msgErreur += "\nConnexion avec les drivers natifs: " + driverNative;
    	
    	// Initialise un Qaqqini si n�cessaire
    	prepareQaqqini();
    	
        return true;
    }

    /**
     * Connexion au serveur depuis un ficher context.xml (config Tomcat)
     * @param resourcename
     * @return
     * voir http://www.itjungle.com/mgo/mgo062602-story01.html
     */
    public boolean connexion (String resourcename)
    {
    	try {
    		Context ctx = (Context) new InitialContext().lookup("java:comp/env");
    		connection = ((DataSource) ctx.lookup(resourcename)).getConnection();
    		initWithContext = true;
    	}
    	catch (Exception e) {
    		msgErreur += "\nEchec de la connexion: " + e.getMessage();
    		return false;
    	}

    	// Initialise un Qaqqini si n�cessaire
    	prepareQaqqini();

    	return true;
    }

    /**
     * Ajoute une propri�t� pour la connexion 
     * @param key
     * @param value
     *
    public void addProperties(String key, String value)
    {
    	properties.setProperty(key, value);
    }*/
    
    /**
     * Retourne le pointeur sur la base de donn�es
     * @return
     */
    public Connection getConnection()
    {
    	return connection;
    }

    /**
     * Retourne le type de driver utilis�
     * @return
     */
    public boolean isDriverNative()
    {
    	return driverNative;
    }
    
    /**
	 * D�connexion du serveur base de donn�es
     * Force permet de d�connecter lorsque l'on a initialis� la connexion avec un context
     * @param force
     */
    public void disconnect(boolean force)
    {
//System.out.println("-disconnect-> force:" + force + " initWithContext:" + initWithContext);    	
    	if( !force && initWithContext ) return;
    	try
    	{
    		if (connection != null)	connection.close();
    		connection = null;
    	}
    	catch (Exception exc)
    	{
    		msgErreur += "\nEchec de la d�connexion: " + exc.getMessage();
    	}
//    	System.out.println("-disconnect-> ferm�");    	
    }

	/**
	 * Permet d'activer des options dans le QAQQINI 
	 * @param qaqqini le qaqqini � d�finir
	 */
	public void setQaqqini(int qaqqini)
	{
		qaqqiniRequetes.clear();
		if( qaqqini <= 0) return;
		
		properties.setProperty("extended dynamic", "true");
		// On parcourt les options possibles (de la valeur la plus grande � la plus petite - Important !!)
		while( qaqqini > 0){
			if( qaqqini >= DEB_NOCACHE ){
				qaqqini -= DEB_NOCACHE;
				qaqqiniRequetes.add(REQ_NOCACHE);
			} else if( qaqqini >= DEB_VERBOSE ){
				qaqqini -= DEB_VERBOSE;
				qaqqiniRequetes.add(REQ_VERBOSE);
			}
		}
	}

	/**
     * G�n�re un Qaqqini dans la QTEMP si n�cessaire
     */
    public void prepareQaqqini()
    {
    	if( (qaqqiniRequetes.size() == 0) || (connection == null) ){
    		return;
    	}
    	
    	try{
    		Statement stmt = connection.createStatement();
    		stmt.executeUpdate("CALL QSYS.QCMDEXC('CRTDUPOBJ OBJ(QAQQINI) FROMLIB(QSYS) OBJTYPE(*FILE) TOLIB(QTEMP) DATA(*YES)',0000000075.00000)");
    		for(String requete: qaqqiniRequetes ) {
    			stmt.executeUpdate(requete);
    		}
    		stmt.executeUpdate("CALL QSYS.QCMDEXC('CHGQRYA QRYOPTLIB(QTEMP)',0000000024.00000)");

    		stmt.executeUpdate("CALL QSYS.QCMDEXC('QSYS/DSPJOBLOG OUTPUT(*OUTFILE) OUTFILE(QTEMP/QAUGDBJOBN) OUTMBR(*FIRST *REPLACE)',0000000081.00000)");
    		System.out.println("Proc�dure Execut�e");
    		ResultSet rs = stmt.executeQuery ("SELECT SUBSTRING(QMHJOB,1,10) AS JOBNAME,SUBSTRING(QMHJOB,11,10) AS USERNAME,SUBSTRING(QMHJOB,21,6) AS JOBNUM FROM QTEMP.QAUGDBJOBN WHERE QMHMID = 'CPIAD02'");
    		rs.next();
    		System.out.println("JOBNAME  : " + rs.getString("JOBNAME")) ;
    		System.out.println("USERNAME : " + rs.getString("USERNAME")) ;
    		System.out.println("JOBNUM   : " + rs.getString("JOBNUM")) ;
    	} catch (SQLException e) {
    		System.out.println("SQL error : " + e);
    	}
    }
    
    /**
     * D�connexion du serveur base de donn�es
     */
    public void disconnect()
    {
    	disconnect(false);
    }

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
