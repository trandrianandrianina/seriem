//=================================================================================================
//==>                                                                       20/12/2011 - 22/01/2014
//==> Description du fichier enteteMsgGeneric
//=================================================================================================
package ri.seriem.libas400.messages;

import com.google.gson.JsonObject;

import ri.seriem.libas400.database.record.InterfaceRecord;

public class EnteteMsgRPG implements InterfaceRecord
{
	// Constantes
	public static final int OFFSET_MSGIDM=0;
	public static final int LONG_MSGIDM=5;
	public static final int OFFSET_MSGNJB=5;
	public static final int LONG_MSGNJB=10;
	public static final int OFFSET_MSGRCD=15;
	public static final int LONG_MSGRCD=10;
	public static final int OFFSET_MSGUSR=25;
	public static final int LONG_MSGUSR=10;
	public static final int OFFSET_MSGMOD=35;
	public static final int LONG_MSGMOD=3;
	public static final int OFFSET_MSGFMT=38;
	public static final int LONG_MSGFMT=10;
	public static final int OFFSET_MSGPRG=48;
	public static final int LONG_MSGPRG=10;
	public static final int OFFSET_MSGBLC=58;
	public static final int LONG_MSGBLC=1;
	public static final int OFFSET_MSGFIC=59;
	public static final int LONG_MSGFIC=20;
	public static final int OFFSET_MSGRES=79;
	public static final int LONG_MSGRES=10;
	public static final int OFFSET_MSGFK1=89;
	public static final int LONG_MSGFK1=1;
	public static final int OFFSET_MSGFK2=90;
	public static final int LONG_MSGFK2=1;
	public static final int OFFSET_MSGIND=91;
	public static final int LONG_MSGIND=99;
	public static final String[] VAR_NAME={"MSGIDM", "MSGNJB", "MSGRCD", "MSGUSR", "MSGMOD", "MSGFMT", "MSGPRG", "MSGBLC", "MSGFIC", "MSGRES", "MSGFK1", "MSGFK2", "MSGIND", };
	public static final int[] VAR_SIZE={5, 10, 10, 10, 3, 10, 10, 1, 20, 10, 1, 1, 99, };
	public static final boolean[] VAR_IS_ALPHA={true, true, true, true, true, true, true, true, true, true, true, true, true, };
	public static final int SIZE_MAX=190;
	public static final String spaces=String.format("%1$"+SIZE_MAX+"s", "");

	// Variables
	private String MSGIDM = null;		// 5 
	private String MSGNJB = null;		// 10 
	private String MSGRCD = null;		// 10 
	private String MSGUSR = null;		// 10 
	private String MSGMOD = null;		// 3 
	private String MSGFMT = null;		// 10 
	private String MSGPRG = null;		// 10 
	private String MSGBLC = null;		// 1 
	private String MSGFIC = null;		// 20 
	private String MSGRES = null;		// 10 
	private String MSGFK1 = null;		// 1 
	private String MSGFK2 = null;		// 1 
	private String MSGIND = null;		// 99 

	// Variables de travail
	private StringBuffer buffer = null;

	/**
	 * Constructeur
	 */
	public EnteteMsgRPG() {}
	
	/**
	 * Constructeur
	 * @param record
	 */
	public EnteteMsgRPG(String record)
	{
		setFlat(record);
	}
	
	/**
	 * @param MSGIDM
	 */
	public void setMSGIDM(String pMSGIDM)
	{
		if ((pMSGIDM == null) || (pMSGIDM.length() <= LONG_MSGIDM))
			MSGIDM = pMSGIDM;
		else
			MSGIDM = pMSGIDM.substring(0, LONG_MSGIDM);
	}

	/**
	 * @return la valeur de pMSGIDM
	 */
	public String getMSGIDM()
	{
		return MSGIDM;
	}

	/**
	 * @param MSGNJB
	 */
	public void setMSGNJB(String pMSGNJB)
	{
		if ((pMSGNJB == null) || (pMSGNJB.length() <= LONG_MSGNJB))
			MSGNJB = pMSGNJB;
		else
			MSGNJB = pMSGNJB.substring(0, LONG_MSGNJB);
	}

	/**
	 * @return la valeur de pMSGNJB
	 */
	public String getMSGNJB()
	{
		return MSGNJB;
	}

	/**
	 * @param MSGRCD
	 */
	public void setMSGRCD(String pMSGRCD)
	{
		if ((pMSGRCD == null) || (pMSGRCD.length() <= LONG_MSGRCD))
			MSGRCD = pMSGRCD;
		else
			MSGRCD = pMSGRCD.substring(0, LONG_MSGRCD);
	}

	/**
	 * @return la valeur de pMSGRCD
	 */
	public String getMSGRCD()
	{
		return MSGRCD;
	}

	/**
	 * @param MSGUSR
	 */
	public void setMSGUSR(String pMSGUSR)
	{
		if ((pMSGUSR == null) || (pMSGUSR.length() <= LONG_MSGUSR))
			MSGUSR = pMSGUSR;
		else
			MSGUSR = pMSGUSR.substring(0, LONG_MSGUSR);
	}

	/**
	 * @return la valeur de pMSGUSR
	 */
	public String getMSGUSR()
	{
		return MSGUSR;
	}

	/**
	 * @param MSGMOD
	 */
	public void setMSGMOD(String pMSGMOD)
	{
		if ((pMSGMOD == null) || (pMSGMOD.length() <= LONG_MSGMOD))
			MSGMOD = pMSGMOD;
		else
			MSGMOD = pMSGMOD.substring(0, LONG_MSGMOD);
	}

	/**
	 * @return la valeur de pMSGMOD
	 */
	public String getMSGMOD()
	{
		return MSGMOD;
	}

	/**
	 * @param MSGFMT
	 */
	public void setMSGFMT(String pMSGFMT)
	{
		if ((pMSGFMT == null) || (pMSGFMT.length() <= LONG_MSGFMT))
			MSGFMT = pMSGFMT;
		else
			MSGFMT = pMSGFMT.substring(0, LONG_MSGFMT);
	}

	/**
	 * @return la valeur de pMSGFMT
	 */
	public String getMSGFMT()
	{
		return MSGFMT;
	}

	/**
	 * @param MSGPRG
	 */
	public void setMSGPRG(String pMSGPRG)
	{
		if ((pMSGPRG == null) || (pMSGPRG.length() <= LONG_MSGPRG))
			MSGPRG = pMSGPRG;
		else
			MSGPRG = pMSGPRG.substring(0, LONG_MSGPRG);
	}

	/**
	 * @return la valeur de pMSGPRG
	 */
	public String getMSGPRG()
	{
		return MSGPRG;
	}

	/**
	 * @param MSGBLC
	 */
	public void setMSGBLC(String pMSGBLC)
	{
		if ((pMSGBLC == null) || (pMSGBLC.length() <= LONG_MSGBLC))
			MSGBLC = pMSGBLC;
		else
			MSGBLC = pMSGBLC.substring(0, LONG_MSGBLC);
	}

	/**
	 * @return la valeur de pMSGBLC
	 */
	public String getMSGBLC()
	{
		return MSGBLC;
	}

	/**
	 * @param MSGFIC
	 */
	public void setMSGFIC(String pMSGFIC)
	{
		if ((pMSGFIC == null) || (pMSGFIC.length() <= LONG_MSGFIC))
			MSGFIC = pMSGFIC;
		else
			MSGFIC = pMSGFIC.substring(0, LONG_MSGFIC);
	}

	/**
	 * @return la valeur de pMSGFIC
	 */
	public String getMSGFIC()
	{
		return MSGFIC;
	}

	/**
	 * @param MSGRES
	 */
	public void setMSGRES(String pMSGRES)
	{
		if ((pMSGRES == null) || (pMSGRES.length() <= LONG_MSGRES))
			MSGRES = pMSGRES;
		else
			MSGRES = pMSGRES.substring(0, LONG_MSGRES);
	}

	/**
	 * @return la valeur de pMSGRES
	 */
	public String getMSGRES()
	{
		return MSGRES;
	}

	/**
	 * @param MSGFK1
	 */
	public void setMSGFK1(String pMSGFK1)
	{
		if ((pMSGFK1 == null) || (pMSGFK1.length() <= LONG_MSGFK1))
			MSGFK1 = pMSGFK1;
		else
			MSGFK1 = pMSGFK1.substring(0, LONG_MSGFK1);
	}

	/**
	 * @return la valeur de pMSGFK1
	 */
	public String getMSGFK1()
	{
		return MSGFK1;
	}

	/**
	 * @param MSGFK2
	 */
	public void setMSGFK2(String pMSGFK2)
	{
		if ((pMSGFK2 == null) || (pMSGFK2.length() <= LONG_MSGFK2))
			MSGFK2 = pMSGFK2;
		else
			MSGFK2 = pMSGFK2.substring(0, LONG_MSGFK2);
	}

	/**
	 * @return la valeur de pMSGFK2
	 */
	public String getMSGFK2()
	{
		return MSGFK2;
	}

	/**
	 * @param MSGIND
	 */
	public void setMSGIND(String pMSGIND)
	{
		if ((pMSGIND == null) || (pMSGIND.length() <= LONG_MSGIND))
			MSGIND = pMSGIND;
		else
			MSGIND = pMSGIND.substring(0, LONG_MSGIND);
	}

	/**
	 * @return la valeur de pMSGIND
	 */
	public String getMSGIND()
	{
		return MSGIND;
	}

	/**
	 * @param variable
	 * @param valeur
	 */
	public void setField(String variable, Object valeur)
	{
		if (variable == null) return;

		if (variable.equals("MSGIDM")) setMSGIDM((String)valeur);
		else
		if (variable.equals("MSGNJB")) setMSGNJB((String)valeur);
		else
		if (variable.equals("MSGRCD")) setMSGRCD((String)valeur);
		else
		if (variable.equals("MSGUSR")) setMSGUSR((String)valeur);
		else
		if (variable.equals("MSGMOD")) setMSGMOD((String)valeur);
		else
		if (variable.equals("MSGFMT")) setMSGFMT((String)valeur);
		else
		if (variable.equals("MSGPRG")) setMSGPRG((String)valeur);
		else
		if (variable.equals("MSGBLC")) setMSGBLC((String)valeur);
		else
		if (variable.equals("MSGFIC")) setMSGFIC((String)valeur);
		else
		if (variable.equals("MSGRES")) setMSGRES((String)valeur);
		else
		if (variable.equals("MSGFK1")) setMSGFK1((String)valeur);
		else
		if (variable.equals("MSGFK2")) setMSGFK2((String)valeur);
		else
		if (variable.equals("MSGIND")) setMSGIND((String)valeur);
	}

	/**
	 * @param return
	 */
	public Object getField(int indice)
	{
		if (indice < 0) return null;
		
		if (indice == 0) return getMSGIDM();
		else
		if (indice == 1) return getMSGNJB();
		else
		if (indice == 3) return getMSGRCD();
		else
		if (indice == 4) return getMSGUSR();
		else
		if (indice == 5) return getMSGMOD();
		else
		if (indice == 6) return getMSGFMT();
		else
		if (indice == 7) return getMSGPRG();
		else
		if (indice == 8) return getMSGBLC();
		else
		if (indice == 9) return getMSGFIC();
		else
		if (indice == 10) return getMSGRES();
		else
		if (indice == 11) return getMSGFK1();
		else
		if (indice == 12) return getMSGFK2();
		else
		if (indice == 13) return getMSGIND();
		else
		return null;
	}
	
	/**
	 * @param return
	 */
	public Object getField(String variable)
	{
		if (variable == null) return null;

		if (variable.equals("MSGIDM")) return getMSGIDM();
		else
		if (variable.equals("MSGNJB")) return getMSGNJB();
		else
		if (variable.equals("MSGRCD")) return getMSGRCD();
		else
		if (variable.equals("MSGUSR")) return getMSGUSR();
		else
		if (variable.equals("MSGMOD")) return getMSGMOD();
		else
		if (variable.equals("MSGFMT")) return getMSGFMT();
		else
		if (variable.equals("MSGPRG")) return getMSGPRG();
		else
		if (variable.equals("MSGBLC")) return getMSGBLC();
		else
		if (variable.equals("MSGFIC")) return getMSGFIC();
		else
		if (variable.equals("MSGRES")) return getMSGRES();
		else
		if (variable.equals("MSGFK1")) return getMSGFK1();
		else
		if (variable.equals("MSGFK2")) return getMSGFK2();
		else
		if (variable.equals("MSGIND")) return getMSGIND();
		else
		return null;
	}

	/**
	 * Retourne 
	 * @return 
	 */
	public StringBuffer getFlat()
	{
		if (buffer == null) buffer = new StringBuffer(spaces);
		else
		{
			// On initialise � blanc
			buffer.setLength(0);
			buffer.append(spaces);
		}

		if (MSGIDM != null) buffer.replace(OFFSET_MSGIDM, OFFSET_MSGIDM+MSGIDM.length(), MSGIDM);
		if (MSGNJB != null) buffer.replace(OFFSET_MSGNJB, OFFSET_MSGNJB+MSGNJB.length(), MSGNJB);
		if (MSGRCD != null) buffer.replace(OFFSET_MSGRCD, OFFSET_MSGRCD+MSGRCD.length(), MSGRCD);
		if (MSGUSR != null) buffer.replace(OFFSET_MSGUSR, OFFSET_MSGUSR+MSGUSR.length(), MSGUSR);
		if (MSGMOD != null) buffer.replace(OFFSET_MSGMOD, OFFSET_MSGMOD+MSGMOD.length(), MSGMOD);
		if (MSGFMT != null) buffer.replace(OFFSET_MSGFMT, OFFSET_MSGFMT+MSGFMT.length(), MSGFMT);
		if (MSGPRG != null) buffer.replace(OFFSET_MSGPRG, OFFSET_MSGPRG+MSGPRG.length(), MSGPRG);
		if (MSGBLC != null) buffer.replace(OFFSET_MSGBLC, OFFSET_MSGBLC+MSGBLC.length(), MSGBLC);
		if (MSGFIC != null) buffer.replace(OFFSET_MSGFIC, OFFSET_MSGFIC+MSGFIC.length(), MSGFIC);
		if (MSGRES != null) buffer.replace(OFFSET_MSGRES, OFFSET_MSGRES+MSGRES.length(), MSGRES);
		if (MSGFK1 != null) buffer.replace(OFFSET_MSGFK1, OFFSET_MSGFK1+MSGFK1.length(), MSGFK1);
		if (MSGFK2 != null) buffer.replace(OFFSET_MSGFK2, OFFSET_MSGFK2+MSGFK2.length(), MSGFK2);
		if (MSGIND != null) buffer.replace(OFFSET_MSGIND, OFFSET_MSGIND+MSGIND.length(), MSGIND);
		return buffer;
	}
	
	/**
	 * Initialise l'ent�te avec un enregistrement plat
	 * @param record
	 * @return
	 */
	public boolean setFlat(String record)
	{
		if (record == null) return false;
		
		final int tailleRecord = record.length();
		int offset_deb=0;
		int offset_fin=0;
		for (int i=0; i<VAR_NAME.length; i++)
		{
			offset_deb = offset_fin;
			offset_fin += VAR_SIZE[i];
			if (offset_fin <= tailleRecord)
				setField(VAR_NAME[i], record.substring(offset_deb, offset_fin).trim());
			else
				setField(VAR_NAME[i], record.substring(offset_deb).trim());
		}
		return true;
	}

	/**
	 * API JsonSimple
	public JSONObject getJSON(boolean trim)
	{
		// TODO Auto-generated method stub
		return null;
	}*/

	public JsonObject getJSON(boolean trim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public boolean setJSON(String record)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
