//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_S4 pour les S4
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_S4 extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "S4LIB"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "S4STA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "S4ELG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "S4EDT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "S4FIC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "S4HAU"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(60), "S4TIT"));

		length = 300;
	}
}
