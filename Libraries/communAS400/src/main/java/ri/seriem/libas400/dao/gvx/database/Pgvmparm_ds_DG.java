//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_DG pour les DG
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_DG extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGNOM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGCPL"));
		//rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "DGT")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT01"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT02"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT03"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT04"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT05"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT06"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGTPF"));
		// A compl�ter car pb de d�coupage
		//rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "DGRBP"));
		//rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGIN2"));
		
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(11, 0), "DGDIVG"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX1G"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGDATG"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX0G"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGETP"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGCMAG"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 0), "DGNUM"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(3, 0), "DGNBS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "DGCNG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "DGTC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DGJRL"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DGRBP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGEUR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGDEV"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGDEP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "DGCNP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGIN1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGIN2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGCGE"));

		length = 300;
	}
	
}
