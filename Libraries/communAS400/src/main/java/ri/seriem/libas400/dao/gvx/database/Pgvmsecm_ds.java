//=================================================================================================
//==>                                                                       10/09/2013 - 10/09/2013
//==> Description de l'enregistrement du fichier Pgvmsecds_ds
//==> G�n�r� � la main
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

public class Pgvmsecm_ds extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "SETOP"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "SECRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "SEMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "SETRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "SEMDM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEUSR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "SEETB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(200), "SEZON1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(200), "SEZON2"));

		length = 429;
	}
}
