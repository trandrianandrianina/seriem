//=================================================================================================
//==>                                                                       25/04/2013 - 07/04/2016
//==> Regroupe toutes les requ�tes pour le syst�me  
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.AS400;

import ri.seriem.libas400.system.systemrequest.DataareaRequestActions;
import ri.seriem.libas400.system.systemrequest.Db2RequestActions;
import ri.seriem.libas400.system.systemrequest.LibraryRequestActions;
import ri.seriem.libas400.system.systemrequest.SavfRequestActions;
import ri.seriem.libas400.system.systemrequest.StreamRequestActions;
import ri.seriem.libas400.system.systemrequest.UserspaceRequestActions;
import ri.seriem.libcommun.protocoleMsg.DataareaRequest;
import ri.seriem.libcommun.protocoleMsg.Db2Request;
import ri.seriem.libcommun.protocoleMsg.LibraryRequest;
import ri.seriem.libcommun.protocoleMsg.MessageHead;
import ri.seriem.libcommun.protocoleMsg.MessageManager;
import ri.seriem.libcommun.protocoleMsg.SavfRequest;
import ri.seriem.libcommun.protocoleMsg.StreamRequest;
import ri.seriem.libcommun.protocoleMsg.UserspaceRequest;

public class TraitementRequeteSysteme
{
	// Constantes
	private static final String REQUETE_INCORRECTE = "Requ�te incorrecte.";
	private static final String REQUETE_NON_ANALYSABLE = "Requ�te non analysable.";
	
	// Variables
	private AS400 systeme;
	protected MessageManager mc = new MessageManager(0);
	protected String message;
	protected String reponse;
	//protected EnvUser infosUser=null;

	/**
	 * Constructeur
	 * @param chaine
	 */
	public TraitementRequeteSysteme(AS400 system/*, EnvUser infos*/, String amessage)
	{
		systeme = system;
		//infosUser = infos;
		if (systeme == null) systeme = new AS400();
		message = amessage;
	}
	
	/**
	 * Analyse le message re�u
	 */
	protected void analyseMessage()
	{
		reponse = null;
		if( (message == null) && (message.trim().equals("")) )
		{
			reponse = REQUETE_INCORRECTE;
			return;
		}

		if (!mc.getMessageReceived(message, 0))
		{
			reponse = REQUETE_NON_ANALYSABLE;
			return;
		}

		if (mc.getHead().getBodycode() == MessageHead.BODY_JSON)
		{
			if (mc.getBody() instanceof Db2Request)
				traitementDb2Request();
			else
				if (mc.getBody() instanceof LibraryRequest)
					traitementLibraryRequest();
				else
					if (mc.getBody() instanceof StreamRequest)
						traitementStreamRequest();
					else
						if (mc.getBody() instanceof SavfRequest)
							traitementSavfRequest();
						else
							if (mc.getBody() instanceof DataareaRequest)
								traitementDataareaRequest();
							else
								if (mc.getBody() instanceof UserspaceRequest)
									traitementUserspaceRequest();
		}
	}
	
	/**
	 * Execute une requ�te sur la base DB2
	 * @return
	 */
	private void traitementDb2Request()
	{
		Db2Request db2 = (Db2Request)mc.getBody();
		Db2RequestActions tdb2 = new Db2RequestActions(systeme, db2);
		tdb2.actions();
		//mc.createMessage(mc.getHead().getId(), true, db2);
		mc.createMessage(true, db2);
		reponse = mc.getMessageToSend();
	}
	
	/**
	 * Execute une requ�te sur la Library
	 * @return
	 */
	private void traitementLibraryRequest()
	{
		LibraryRequest rlibrary = (LibraryRequest)mc.getBody(); 
		LibraryRequestActions tlib = new LibraryRequestActions(systeme, rlibrary);
		tlib.actions();
		//mc.createMessage(mc.getHead().getId(), rlibrary);
		mc.createMessage(rlibrary);
		reponse = mc.getMessageToSend();
	}

	/**
	 * Execute une requ�te sur le stream
	 * @return
	 */
	private void traitementStreamRequest()
	{
		StreamRequest rstream = (StreamRequest)mc.getBody();
		StreamRequestActions tstream = new StreamRequestActions(systeme, rstream);
		tstream.actions();
		//mc.createMessage(mc.getHead().getId(), rstream);
		mc.createMessage(rstream);
		reponse = mc.getMessageToSend();
	}

	/**
	 * Execute une requ�te sur le savf
	 * @return
	 */
	private void traitementSavfRequest()
	{
		SavfRequest rsavf = (SavfRequest)mc.getBody();
		SavfRequestActions tsavf = new SavfRequestActions(systeme, rsavf);
		tsavf.actions();
		//mc.createMessage(mc.getHead().getId(), rsavf);
		mc.createMessage(rsavf);
		reponse = mc.getMessageToSend();
	}

	/**
	 * Execute une requ�te sur les dataarea
	 * @return
	 */
	private void traitementDataareaRequest()
	{
		DataareaRequest rdta = (DataareaRequest)mc.getBody();
		DataareaRequestActions tdta = new DataareaRequestActions(systeme, rdta);
		tdta.actions();
		//mc.createMessage(mc.getHead().getId(), rsavf);
		mc.createMessage(rdta);
		reponse = mc.getMessageToSend();
	}

	/**
	 * Execute une requ�te sur les userspace
	 * @return
	 */
	private void traitementUserspaceRequest()
	{
		UserspaceRequest rupc = (UserspaceRequest)mc.getBody();
		UserspaceRequestActions tupc = new UserspaceRequestActions(systeme, rupc);
		tupc.actions();
		//mc.createMessage(mc.getHead().getId(), rsavf);
		mc.createMessage(rupc);
		reponse = mc.getMessageToSend();
	}

	/**
	 * Retourne la r�ponse � la requ�te soumise
	 * @return
	 */
	public String getResponse()
	{
		analyseMessage();

		return reponse;
	}

	/**
	 * @return le infosUser
	 *
	public EnvUser getInfosUser()
	{
		return infosUser;
	}

	/**
	 * @param infosUser le infosUser � d�finir
	 *
	public void setInfosUser(EnvUser infosUser)
	{
		this.infosUser = infosUser;
	}*/
	
	/**
	 * Lance une commande CL
	 *
	private AS400Message[] executeCmd(String command)
	{
		CommandCall cmd = new CommandCall(systeme);
		try
		{
			cmd.run(command);
		}
		catch (Exception e)
		{
			return null;
		}

		return cmd.getMessageList();
	}*/

	/**
	 * Lib�re la m�moire
	 */
	public void dispose()
	{
		systeme = null;
		mc.dispose();
	}
}
