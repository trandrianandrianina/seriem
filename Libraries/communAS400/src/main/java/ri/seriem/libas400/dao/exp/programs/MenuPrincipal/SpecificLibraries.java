//=================================================================================================
//==>                                                                       03/07/2012 - 18/03/2016
//==> Recherche les biblioth�ques sp�cifiques
//==> Appel� par:
//==> Attention
//==>	Il faut modifier les valeurs des champs MNPP1 et MNEMOD (comme indiqu� dans la m�thode getListeBib)
//==>	afin que les menus se construisent correctement (� faire lors des migrations S�rie M -> S�rie N)
//=================================================================================================
package ri.seriem.libas400.dao.exp.programs.MenuPrincipal;

import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libas400.system.SystemManager;

public class SpecificLibraries
{
	private SystemManager systeme=null;
	private ArrayList<String>listeBibSpe = new ArrayList<String>();
	
	//private LogMessage log=new LogMessage("/tmp", "_spm");
	
	/**
	 * Constructeur
	 * @param sys
	 */
	public SpecificLibraries(SystemManager sys)
	{
		setSysteme(sys);
		//log.setRedirigeConsole(true);
	}
	
	/**
	 * Retourne la liste des bib sp�cifiques
	 * @param bibenv
	 * @param curlib
	 * @return
	 */
	public ArrayList<String> getListeBib(boolean update, String bibenv, String curlib, char lettreasp)
	{
		//System.out.println("-getListeBibSpe-> " + bibenv + " " + curlib);
		//if (!update)
		{
			String chaine;
			listeBibSpe.clear();
			
			// Les menus sp�cifiques activ�s avec l'option M
			String contenu = lectureDtaArea(systeme.getSystem(), "MWSPLIB", curlib);
			// MWSPLIB
			if (contenu != null) // La dataara existe
			{
				chaine = getBibSpe1(contenu, curlib);
				if (chaine != null) listeBibSpe.add(chaine.trim());
				//chaine = getBibSpe2(chaine, curlib); � l'origine mais source de bug avec le nom de la bib en double et est ce vraiment logique ? 
				chaine = getBibSpe2(contenu, curlib);
				if (chaine != null)	listeBibSpe.add(chaine.trim());
			}
			else // MWSPMAS
			{
				chaine = getBibSpe(bibenv);
				if (chaine != null) listeBibSpe.add(lettreasp + chaine.substring(1).trim());
			}
			
			// Les menus sp�cifiques activ�s avec l'option P
			chaine = getBibSpeP(bibenv, curlib);
			if (chaine != null) listeBibSpe.add(lettreasp + chaine.substring(1).trim());
//System.out.println("-SPE P->" + chaine);			
		}
//System.out.println("-getListeBibSpe-> taille liste " + listeBibSpe.size());
		
		return listeBibSpe;
	}

	/**
	 * Retourne la biblioth�que sp�cifique
	 * @param bibenv
	 * @return
	 */
	public String getBibSpe(String bibenv)
	{
		if ((bibenv == null) || (bibenv.trim().equals(""))) return null;
		
		// Lecture de la dataarea
		String bibspe = lectureDtaArea(systeme.getSystem(), "MWSPMAS", bibenv);
		if ((bibspe != null) && (bibspe.length() >= 10))
			return bibspe.substring(0, 10);
		return bibspe;
	}

	/**
	 * Retourne la biblioth�que sp�cifique des menus P
	 * @param bibenv
	 * @param curlib
	 * @return
	 */
	public String getBibSpeP(String bibenv, String curlib)
	{
		if ((bibenv == null) || (bibenv.trim().equals(""))) return null;
		
		// Lecture de la dataarea
		String bibspe = lectureDtaArea(systeme.getSystem(), "P"+curlib, bibenv);
		if ((bibspe != null) && (bibspe.length() >= 10))
			return bibspe.substring(0, 10);
		return bibspe;
	}

	/**
	 * Retourne la biblioth�que sp�cifique1
	 * @param contenu
	 * @param curlib
	 * @return
	 */
	public String getBibSpe1(String contenu, String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return null;
		
		if (contenu.length() >= 10)
			return contenu.substring(0, 10);
		return contenu;
	}

	/**
	 * Retourne la biblioth�que sp�cifique2
	 * @param contenu
	 * @param curlib
	 * @return
	 */
	public String getBibSpe2(String contenu, String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return null;
		
		if (contenu.length() >= 20)
			return contenu.substring(10, 20);
		return null;
	}

	/**
	 * Retourne la biblioth�que sp�cifique1
	 * @param curlib
	 * @return
	 *
	public String getBibSpe1(String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return null;
		
		// Lecture de la dataarea
		String bibspe = lectureDtaArea(systeme.getSystem(), "MWSPLIB", curlib);
		if ((bibspe != null) && (bibspe.length() >= 10))
			return bibspe.substring(0, 10);
		return bibspe;
	}*/

	/**
	 * Retourne la biblioth�que sp�cifique2
	 * @param curlib
	 * @return
	 *
	public String getBibSpe2(String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return null;
		
		// Lecture de la dataarea
		String bibspe = lectureDtaArea(systeme.getSystem(), "MWSPLIB", curlib);
		if ((bibspe != null) && (bibspe.length() >= 20))
			return bibspe.substring(10, 20);
		return bibspe;
	}*/

	// -- M�thodes priv�es --
	
	/**
	 * Lecture d'une data sp�cifique
	 * @param system
	 * @param dta
	 * @param bib
	 */
	private String lectureDtaArea(AS400 systeme, String dta, String bib)
	{
		// Lecture de la dataarea
		CharacterDataArea vgmdta = new CharacterDataArea(systeme, QSYSObjectPathName.toPath(bib, dta, "DTAARA"));
		try
		{
			return vgmdta.read();
		}
		catch (Exception e)
		{}
		return null;
	}

	// -- Accesseurs --
	
	/**
	 * @param systeme the systeme to set
	 */
	public void setSysteme(SystemManager systeme)
	{
		this.systeme = systeme;
	}

	/**
	 * @return the systeme
	 */
	public SystemManager getSysteme()
	{
		return systeme;
	}
	
}
