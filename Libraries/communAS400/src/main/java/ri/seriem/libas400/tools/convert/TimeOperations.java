package ri.seriem.libas400.tools.convert;


public class TimeOperations
{
	// Constantes
	public static final byte	ADD			= 1;
	public static final byte	SUB			= -1;
	public static final byte	INIT		= 0;

	public static final char	DAY			= 'j';
	public static final char	HOUR		= 'h';
	public static final char	MINUTE		= 'm';

	public static final String	LDAY		= "jour";
	public static final String	LHOUR		= "heure";
	public static final String	LMINUTE		= "minute";
	public static final String	LDAYS		= "jours";
	public static final String	LHOURS		= "heures";
	public static final String	LMINUTES	= "minutes";

	// Variables
	public int nbrhourbyday					= 8;
	
	/**
	 * Controle la validit� de la chaine pour l'op�ration
	 * @param operation
	 * @return
	 */
	public boolean validOperation(String operation)
	{
		if( operation == null ) return false;
		operation = operation.trim().toLowerCase();
		if( operation.length() == 0) return false;
		return isGood4Operation(operation);
	}
	
	/**
	 * Analyse une chaine afin de d�terminer l'op�ration � effectuer  	
	 * @param valeurori
	 * @param operation
	 * @return
	 */
	public int operationOnTime(int valeurori, String operation)
	{
		if( operation == null ) return valeurori;
		operation = operation.trim().toLowerCase();
		if( operation.length() == 0) return valeurori;

		// On d�termine le type d'op�ration
		byte typeoperation = INIT;
		if( operation.charAt(0) == '+' ){
			typeoperation = ADD;
			operation = operation.substring(1);
		} else if( operation.charAt(0) == '-' ){
			typeoperation = SUB;
			operation = operation.substring(1);
		} else {
			typeoperation = INIT;
		}

		// Controle de la validit� de la valeur
		if( !isNumeric(operation.substring(0, operation.length()-1)) ) return valeurori;
		
		// On d�termine quel type de valeur saisie (jour, heure, minute)
		int valeurminute = 0;
		switch( operation.charAt( operation.length()-1 ) ){
			case DAY	:	operation = operation.substring(0, operation.length()-1);
							valeurminute = Integer.parseInt( operation ) * nbrhourbyday * 60;
						break;
			case HOUR	:	operation = operation.substring(0, operation.length()-1);
							valeurminute = Integer.parseInt( operation ) * 60;
						break;
			case MINUTE	:	operation = operation.substring(0, operation.length()-1);
							valeurminute = Integer.parseInt( operation );
						break;
			default		:	if( !isNumeric(operation.substring(0, operation.length())) )
								return valeurori;
							valeurminute = Integer.parseInt( operation );
						break;
		}
 
		// On effectue l'op�ration finale
		switch( typeoperation ){
			case ADD	:	valeurori = valeurori + valeurminute;
						break;
			case SUB	:	valeurori = valeurori - valeurminute;
							if( valeurori < 0 )
								valeurori = 0;
						break;
			case INIT	:	valeurori = valeurminute;
						break;
		}
		
		return valeurori;
	}
	
	/**
	 * Converti une valeur en minutes en chaine indiquant jours, heures, minutes 
	 * @param minutes
	 * @return
	 */
	public String convertMinute2String(int minutes)
	{
		if( minutes <= 0 ) return "Aucun";
		
		int days = 0;
		int hours = 0;
		
		// On d�compose les jours, heures, minutes
		while( minutes >= 60 ){
			if( minutes >= (nbrhourbyday * 60) ){
				days++;
				minutes -= (nbrhourbyday * 60);
			} else if( minutes >= 60 ){
				hours++;
				minutes -= 60;
			}
		}
		
		// On construit la chaine
		String chaine = "";
		if( days > 0 ){
			chaine += days + " " + ((days==1)?LDAY:LDAYS) + " ";
		}
		if( hours > 0 ){
			chaine += hours + " " + ((hours==1)?LHOUR:LHOURS) + " ";
		}
		if( minutes > 0 ){
			chaine += minutes + " " + ((minutes==1)?LMINUTE:LMINUTES);
		}
		
		return chaine.trim();
	}
	
	/**
	 * Controle si une chaine ne contient que des valeurs num�riques 
	 * @param chaine
	 * @return
	 */
	private boolean isNumeric(String chaine)
	{
		if( chaine == null ) return false;
		
		StringBuilder sb = new StringBuilder(chaine.trim());
		for (int i=0; i<sb.length(); i++)
			if( (sb.charAt(i) != '0') && (sb.charAt(i) != '1') && (sb.charAt(i) != '2') && (sb.charAt(i) != '3') &&
				(sb.charAt(i) != '4') && (sb.charAt(i) != '5') && (sb.charAt(i) != '6') && (sb.charAt(i) != '7') &&
				(sb.charAt(i) != '8') && (sb.charAt(i) != '9') )
				return false;
		return true;
	}

	/**
	 * Controle si une chaine est correcte pour effectuer l'op�ration 
	 * @param chaine
	 * @return
	 */
	private boolean isGood4Operation(String chaine)
	{
		if( chaine == null ) return false;
		
		StringBuilder sb = new StringBuilder(chaine.trim());
		for (int i=0; i<sb.length(); i++)
			if( (sb.charAt(i) != '0') && (sb.charAt(i) != '1') && (sb.charAt(i) != '2') && (sb.charAt(i) != '3') &&
				(sb.charAt(i) != '4') && (sb.charAt(i) != '5') && (sb.charAt(i) != '6') && (sb.charAt(i) != '7') &&
				(sb.charAt(i) != '8') && (sb.charAt(i) != '9') && (sb.charAt(i) != '+') && (sb.charAt(i) != '-') &&
				(sb.charAt(i) != 'j') && (sb.charAt(i) != 'h') && (sb.charAt(i) != 'm'))
				return false;
		return true;
	}

	
	/**
	 * @param args
	 *
	public static void main(String[] args)
	{
		int valeurori = 562;
		String valeur = "4x";
		
		CalculTempsPasse t = new CalculTempsPasse();
		int minutes = t.operationOnTime(valeurori, valeur);
		System.out.println("--> " + minutes);
		System.out.println("--> " + t.convertMinute2String(minutes));
	}*/

}
