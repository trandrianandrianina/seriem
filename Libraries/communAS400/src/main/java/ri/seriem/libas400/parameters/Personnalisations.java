package ri.seriem.libas400.parameters;

public class Personnalisations
{
	
	// Variables
	public static int NBR_PS = 177;
	private char[] tableau = new char[NBR_PS];

	
	/**
	 * @return le tableau
	 */
	public char[] getTableau()
	{
		return tableau;
	}

	/**
	 * @param ps le ps � d�finir
	 */
	public void setTableau(char[] tableau)
	{
		this.tableau = tableau;
	}

	/**
	 * @param tableau le tableau � d�finir
	 */
	public void setTableau(String chaine)
	{
		if (chaine == null) return;
		if (chaine.length() > NBR_PS)
		{
			NBR_PS = chaine.length();
			tableau = new char[NBR_PS];
		}
		int delta = NBR_PS - chaine.length();
//System.out.println("nbr de PS : " + chaine.length() + " delta : " + delta );

		// On alimente le tableau des PS avec les valeurs r�cup�r�es
		int i=0;
		for (; i<chaine.length(); i++)
			tableau[i] = chaine.charAt(i);
		// On compl�te le tableau des PS avec du blancs si n�cessaire
		for (int j=0; j<delta; j++, i++)
			tableau[i] = ' ';
	}

	/**
	 * Retourne la valeur de la PS num�ro X
	 * @return
	 */
	public char getValue(int num)
	{
		if ((tableau == null) || (num > tableau.length)) return ' ';
		return tableau[num-1];
	}

}
