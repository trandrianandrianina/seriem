package ri.seriem.libas400.system.systemrequest;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libcommun.protocoleMsg.LibraryRequest;
import ri.seriem.libcommun.protocoleMsg.StreamRequest;

public class StreamRequestActions
{
	private AS400 sysAS400=null;
	private StreamRequest rstream=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public StreamRequestActions(AS400 system, StreamRequest arstream)
	{
		sysAS400 = system;
		rstream = arstream;
	}

	// --> M�thodes publiques <------------------------------------------------
	
	/**
	 * Traitement des actions possibles 
	 */
	public void actions()
	{
		//int theactions = actions;
		for (int i=StreamRequest.ACTIONS.size(); --i>0;)
		{
			if (rstream.getActions() >= StreamRequest.ACTIONS.get(i))
			{
				switch(StreamRequest.ACTIONS.get(i))
				{
					case StreamRequest.COPYTO5250:
						streamCopyTo5250(rstream.getName(), rstream.getLibrary_object());
						break;
				}
				rstream.setActions(rstream.getActions() - StreamRequest.ACTIONS.get(i));
			}
		}
	}

	// --> M�thodes priv�es <--------------------------------------------------

	/**
	 * Copie un fichier Stream de l'IFS vers le 5250
	 * @param stream
	 * @param dest
	 * @return
	 */
	private void streamCopyTo5250(String stream, String dest)
	{
		if ((stream == null) || (dest == null))
		{
			rstream.setSuccess(false);
			return;
		}
		stream = stream.replace('\\', '/');
		String[] bib_obj = getLibraryAndObject(dest, true);
		if (bib_obj[1] == null)
		{
			int posd = stream.lastIndexOf('/');
			int posf = stream.lastIndexOf('.');
			bib_obj[1] = stream.substring(posd+1, posf);
		}

		// On v�rifie l'existence de la biblioth�que
		LibraryRequest rlibrary = new LibraryRequest(bib_obj[0], LibraryRequest.CREATE);
		LibraryRequestActions tlib = new LibraryRequestActions(sysAS400, rlibrary);
		tlib.actions();
		if (!rlibrary.isExist())
		{
			rstream.setSuccess(false);
			return;
		}
		
		String targetPath = QSYSObjectPathName.toPath(bib_obj[0], bib_obj[1], "FILE");
		
		// De quel type est le fichier stream ?
		if (stream.toUpperCase().endsWith(".SAVF"))
		{
			StringBuilder sb = new StringBuilder();
			// On copie le SAVF 
			CommandCall cmd = new CommandCall(sysAS400,  "QSYS/CPYFRMSTMF FROMSTMF('"+ stream +"') TOMBR('" + targetPath + "') CVTDTA(*NONE) ENDLINFMT(*FIXED) TABEXPN(*NO)");
			try
			{
				rstream.setSuccess(cmd.run());
			}
			catch (Exception e)
			{
				rstream.setSuccess(false);
				rstream.setMsgError(e.getMessage());
			}
			AS400Message[] msg = cmd.getMessageList();
			if (msg != null)
			{
				for (int i=0; i<msg.length; i++)
					sb.append(msg[i].getText()).append('\n');
				rstream.setMsgError(sb.toString());
			}
		}
	}

	/**
	 * Teste l'existence d'un objet en 5250
	 * @param library
	 * @param objet
	 * @param type
	 */
	private void exist(String library, String objet, String type)
	{
		String targetPath = QSYSObjectPathName.toPath(library, objet, type);
		IFSFile file = new IFSFile(sysAS400, targetPath);
		try
		{
			if (file.exists())
			{
				rstream.setSuccess(true);
				rstream.setMsgError("L'objet "+ targetPath + " existe d�j�.");
				return;
			}
		}
		catch(IOException ioe)
		{
			rstream.setSuccess(false);
			rstream.setMsgError(ioe.getMessage());
		}
		
	}

	// --> M�thodes priv�es <--------------------------------------------------
	
	/**
	 * S�pare dans une chaine le couple BIB/OBJ et le retourne sous forme de tableau  
	 * @param valeur
	 * @return
	 */
	protected String[] getLibraryAndObject(String valeur, boolean forcebib)
	{
		String[] bib_obj = new String[2];
		
		// On r�cup�re les informations des arguments 
		int posd = valeur.indexOf('/');
		// On r�cup�re le nom du fichier SAVF car il n'est pas pr�cis� dans l'argument
		if (posd == -1)
		{
			if (forcebib)
				bib_obj[0] = valeur;
			else
				bib_obj[1] = valeur;
		}
		else // On a pass� le nom de la bib et le nom du SAVF (BIB/SAVF)
		{
			bib_obj[0] = valeur.substring(0, posd);
			bib_obj[1] = valeur.substring(posd+1);
		}
		return bib_obj;
	}

}
