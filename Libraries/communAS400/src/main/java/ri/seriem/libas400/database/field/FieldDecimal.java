//=================================================================================================
//==>                                                                       21/12/2011 - 26/05/2014
//==> Description de base d'un champ alphanumérique 
//=================================================================================================
package ri.seriem.libas400.database.field;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class FieldDecimal extends Field<BigDecimal>
{
	// Variable
	public BigDecimal valeur;

	/**
	 * Constructeur
	 */
	public FieldDecimal()
	{
		sqltype = "java.math.BigDecimal";
	}

	/**
	 * Constructeur
	 * @param nom
	 * @param valeur
	 * @param asize
	 * @param adecimal
	 */
	public FieldDecimal(String nom, BigDecimal valeur, int asize, int adecimal)
	{
		name = nom;
		sqltype = "java.math.BigDecimal";
		setValue(valeur);
		setSize(asize, adecimal);
	}

	/**
	 * Constructeur
	 * @param valeur
	 * @param asize
	 */
	public FieldDecimal(BigDecimal valeur, int asize, int adecimal)
	{
		sqltype = "java.math.BigDecimal";
		setValue(valeur);
		setSize(asize, adecimal);
	}

	/**
	 * Constructeur
	 * @param asize
	 */
	public FieldDecimal(int asize, int adecimal)
	{
		sqltype = "java.math.BigDecimal";
		setSize(asize, adecimal);
	}

	@Override
	public BigDecimal getValue()
	{
		return valeur;
	}

	@Override
	public void setValue(BigDecimal valeur)
	{
		this.valeur = valeur;
	}

	@Override
	public String toString()
	{
		return valeur!=null?valeur.toString():null;
	}

	@Override
	public String toFormattedString(int size, int adecimal)
	{
		if (valeur == null) return toString();
		//DecimalFormat df = new DecimalFormat(format);
		//return df.format(valeur.doubleValue()).replace(",", "");
		DecimalFormat df = new DecimalFormat("#");
		df.setMinimumIntegerDigits(size-adecimal);
		df.setMinimumFractionDigits(adecimal);
		return df.format(valeur.doubleValue()).replace(",", "");
	}

	@Override
	public String toFormattedString()
	{
		return toFormattedString(length, decimal);
	}

	@Override
	public BigDecimal getTrimValue()
	{
		return valeur;
	}
	
	@Override
	public void setSize(int asize)
	{
		length = asize;
		decimal = 0;
	}

	@Override
	public void setSize(int asize, int adecimal)
	{
		length = asize;
		if (adecimal < 0) decimal = 0;
		decimal = adecimal;
	}

	@Override
	public String getSQLType()
	{
		return sqltype;
	}

	@Override
	public void setSQLType(String sqltype)
	{
		this.sqltype = sqltype;
	}

}
