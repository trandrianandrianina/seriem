//=================================================================================================
//==>                                                                       11/09/2015 - 21/10/2015
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.gvm.programs.parametres;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.Record;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.ExtendRecord;


public class GM_PersonnalisationGVM extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param database
	 */
	public GM_PersonnalisationGVM(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Retourne la liste des objets possibles pour les actions
	 * @param curlib
	 * @param etb
	 * @return
	 **/
	public LinkedHashMap<String, String> getListObjectAction(String etb)
	{
//System.out.println("-loadListObjectAction-perso->etb:" + etb + " " + querymg);		
		LinkedHashMap<String, String>listObjectAction = new LinkedHashMap<String, String>();
		//String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_AA";
//System.out.println("-loadListObjectAction-perso->ds:" + ds);		
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + querymg.getLibrary() + ".PGVMPARM where PARTYP = 'AA' and PARETB = '" + etb + "'", "ri.seriem.libas400.dao.gvm.database.Pgvmparm_ds_AA");

		// On alimente la hashmap avec les enregistrements trouv�s
		if( listeRecord == null) return listObjectAction;
		
		ExtendRecord record = new ExtendRecord();
		for(Record rcd : listeRecord ) {
			record.setRecord(rcd);
			listObjectAction.put(((String)record.getField("INDIC")).substring(5).trim(), ((String)record.getField("AALIB")).trim());
		}

		return listObjectAction;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		
	}

}
