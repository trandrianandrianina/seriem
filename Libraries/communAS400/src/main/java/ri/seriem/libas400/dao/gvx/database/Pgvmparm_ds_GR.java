//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_GR pour les GR
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_GR extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "GRLIB"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "GRTVA"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "GRTPF"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "GRSPE"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "GRUNV"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "GRC")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "GRNSA"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "GRCMAG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "GRUNS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(15, 0), "GRKV1G"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "GRRTA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "GRSAN"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "GRRON"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "GRA")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "GRNAT"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 4), "GRCPR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "GRIMG"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "GRMAR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "GRWEB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "GRGP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(9, 0), "GRPQW"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "GRAQW"));

		length = 300;
	}
}
