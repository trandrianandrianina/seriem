//=================================================================================================
//==>                                                                       05/12/2011 - 18/03/2016
//==> Programme qui g�re les menus pour S�rie N
//==> Appel� par:
//==>	la m�thode SessionAS400 du serveur S�rie N
//==> Attention
//==> 	Pour les menus sp�cifiques penser � changer la valeur du champ MNEMOD en SPM, ensuite les valeurs
//==> 	seront modifi�es dynamiquement en S01, S02
//=================================================================================================
package ri.seriem.libas400.dao.exp.programs.MenuPrincipal;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import ri.seriem.libas400.dao.exp.database.PsemenvmManager;
import ri.seriem.libas400.dao.exp.database.PsemussmManager;
import ri.seriem.libas400.dao.gvx.programs.UtilisateurETB_MAG;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.database.record.GenericRecordManager;
import ri.seriem.libas400.messages.EnteteMsgRPG;
import ri.seriem.libas400.system.BaseProgram;
import ri.seriem.libas400.system.CallProgram;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.libas400.system.systemrequest.LibraryRequestActions;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.GestionFichierTexte;
import ri.seriem.libcommun.outils.menus.baseModel;
import ri.seriem.libcommun.outils.menus.structModel;
import ri.seriem.libcommun.protocoleMsg.LibraryRequest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

public class MenuPrincipal extends BaseProgram
{
	// Constantes
	private final static String NOM_CLASSE = "[MenuPrincipal]";
	private final static String[][] WGRP={{"CGM", "1"}, {"GEM", "1"}, {"GIM", "1"}, {"GLF", "1"}, {"TBM", "1"}, {"TIM", "1"}, {"GTM", "1"},
	                                      //{"GSM", "2"}, {"PAM", "2"}, {"TDS", "2"}, {"GNF", "2"}, {"GHM", "2"}, <-- La paye n'existe plus
	                                      {"GAM", "3"}, {"GMM", "3"}, {"GPM", "3"}, {"GVM", "3"}, {"GVX", "3"}, {"PRM", "3"}, {"TRM", "3"},
	                                      {"EXP", "5"},
	                                      {"SPM", "6"}, {"S01", "6"}, {"S02", "6"}, {"S03", "6"}, {"S04", "6"}
										 };
	
	// Variables
	private	structModel	sModel = new structModel();
	private final GenericRecordManager grm = new GenericRecordManager(systeme.getdatabase().getConnection());
	private GenericRecord psemenvm=null;
	private GenericRecord psemussm=null;
	private ArrayList<String> listebibspe=null;
	private Gson gson = new Gson();
	private String curlib = "";
	private String menuperso = "";
	private boolean menupersonotfound = false;
	
	//private String suftmnu = "sgm";

	
	/**
	 * Constructeur
	 * C'est celui-ci qui est utilis�, pour lancer le menu, dans la classe SessionAS400 
	 */
	public MenuPrincipal(HashMap<String, Object> parametres)
	{
		super(parametres);
		
		// Selection des fichiers menus en fonction du type de menu (rt.ini)
		/* Obsol�te avec les nouveaux menus - 05/01/2016
		String tmnu = getTypemenu();
		if ((tmnu != null) && tmnu.equalsIgnoreCase("negoce"))
			suftmnu = "neg";
			*/
	}

	/**
	 * Constructeur
	 */
	public MenuPrincipal(String prf, String device, String bibenv, char letasp)
	{
		super(null, prf, device, bibenv, letasp);
	}

	/**
	 * Constructeur
	 */
	public MenuPrincipal(SystemManager systeme, String prf, String device, String bibenv, char letasp)
	{
		super(systeme, prf, device, bibenv, letasp);
	}

	/**
	 * Hautement plus performant mais moins beau
	 */
	protected void receptionMessage(String message)
	{
		//log.ecritureMessage("-->" + message);
//System.out.println("[MenuPrincipal]-receptionMessage-> " + message);
		if (message == null) return;

		// R�cup�ration de l'ent�te du message
		final String idm = message.substring(0, 5);
//System.out.println("-message->" + idm);
		// Traitement en fonction de IDM re�u
		if (idm.equals("20001")) // Demande le d�tail d'un module
		{
//System.out.println("-receptionMessage-> " + message.trim());
			//int pos = message.indexOf('|');
			String[] param = Constantes.splitString(message, Constantes.SEPARATEUR_CHAINE_CHAR);
//System.out.println("-param->" + param.length);			
//System.out.println("-message 20001->" + message.substring(pos+1));
			if( sModel.isEmpty() )
				sendDetailModuleJSONwithCompression(param[0].substring(190), param[1], null, null);
			else{
				String lib = null;
				if( param[3] != null )
					lib = param[3].trim();
				sendDetailModuleJSONwithCompression(param[0].substring(190), param[1], param[2], lib);
			}
		}
		else if (idm.equals("20003")) // Demande un rafraichissement des infos suite au changement de curlib
		{
			sendRefreshDataChgCurlib(message.substring(190)); // TODO � am�liorer avec du json
		}
		else
			if (idm.equals("99999")) // On sort
				destructor();
				//waitMsg = false;
	}

	/**
	 * Traitement principal
	 */
	public boolean treatment()
	{
		super.treatment();

		// Test de la connection au serveur
		if (systeme == null) return false;
		
		// Contr�le du CCSID
		System.out.println("--> Connexion:" + systeme.getdatabase().getMsgError()+"\n--> Contr�le CCSID\nProfil : " +systeme.getSystem().getUserId() + "\nCCSID  : " + systeme.getSystem().getCcsid());
		System.out.println("-racine->" + getDossierRacine() + " Menus:" + getDossierMenus());
		
		// Mise en place de la gestion des DTAQs
		if (!manageDataQueue()) return false;

		// Lecture des donn�es pour le profil
		if (!readDataUser()) return false;

		// On contr�le l'existence d'un menus personnalis�
		menupersonotfound = loadCustomMenus(menuperso); 

		// Recherche d'�ventuelles biblioth�ques sp�cifiques
		listebibspe = getListBibSpe(curlib);

		// Envoi des infos sur l'utilisateur au client
		if (!sendDataUserJSON()) return false;

		// S'il y a un soucis avec les menus on ne va pas plus loin
		if( !menupersonotfound ) return false;

		// Envoi des modules
		if (!sendGroupModuleJSON()) return false;

		return true;
	}
	
	/**
	 * Converti le fichier JSON des menus en classe
	 * @param amodel
	 * @return
	 */
	private boolean convertJSON2Model(String amodel)
	{
		if( amodel == null ){
			msgErreur += "\nLa chaine JSON est nulle.";
			return false;
		}
		//Type typeMap = new TypeToken<LinkedHashMap<String, ArrayList<String>>>(){}.getType();
		//listMenu4Model = gson.fromJson(amodel, typeMap);
		sModel = gson.fromJson(amodel, sModel.getClass());
		
		return true;
	}

	/**
	 * Charge les menus persos
	 * @param menusname
	 * @return
	 */
	private boolean loadCustomMenus(String menusname)
	{
		if( (menuperso == null) || menuperso.trim().equals("") ) return true;
		menuperso = menuperso.trim() + ".mnu";

		// On charge le fichier
		GestionFichierTexte gft = new GestionFichierTexte(getDossierMenus() + File.separatorChar + menuperso);
		String model = gft.getContenuFichierString(false);
		gft.dispose();
		if( model == null ){
			System.out.println("Le menu perso " + menuperso + " n'a pas pu �tre charg�: " + gft.getMsgErreur() );
			return false;
		}
//System.out.println("-MenuPrincipal->model" + model);
		return convertJSON2Model(model);
	}
	
	/**
	 * R�cup�ration de l'enregistrement dans le psemussm pour le profil
	 * @return
	 */
	private boolean readDataUser()
	{
		// Lecture du fichier PSEMENVM pour l'environnement courant
		final PsemenvmManager psemenvmManager = new PsemenvmManager(systeme.getdatabase().getConnection());
		
		// On v�rifie les bibenv et bibjob d'abord
		if ((getBibjob() != null) || (getBibjob().trim().equals("")))
			psemenvm = psemenvmManager.getRecordForBibEnv(getBibjob());
		else
			psemenvm = psemenvmManager.getRecordForBibEnv(getBibenv());
		
		if (psemenvm == null)
		{
			msgErreur += psemenvmManager.getMsgError() + "\n\t" + NOM_CLASSE + " (readDataUser) L'enregistrement du psemenvm pour l'environnement " + getBibenv() + " est null.";
		}
		else
		{
			String envver = (String)psemenvm.getField("ENVVER"); 
			setLetasp(envver.trim().length()>0?envver.charAt(0):'W');
//System.out.println("--> PSEMENVM " + envver + " " + getLetasp());
		}
	
		// Lecture du fichier PSEMUSSM pour l'utilisateur courant
		final PsemussmManager psemussmManager = new PsemussmManager(systeme.getdatabase().getConnection());
		psemussm = psemussmManager.getRecordForPrf(getBibenv(), profil);
		if (psemussm == null)
		{
			msgErreur += psemussmManager.getMsgError() + "\n\t" + NOM_CLASSE + " (readDataUser) L'enregistrement du psemssum pour le profil " + profil + " est null.";
			return false;
		}
		curlib = (String)psemussm.getField("USSBIB");
		menuperso =  (String) psemussm.getField("USSMNP");
		
		return true;
	}

	/**
	 * Pr�pare et Envoi les donn�es du User vers le client
	 * @return
	 */
	private boolean sendDataUserJSON()
	{
		EnteteMsgRPG eMsg = getEnteteMsg();
		eMsg.setMSGIDM("20000");
		eMsg.setMSGBLC("0");

		JsonObject temp = psemussm.getJSON(false);
		temp.addProperty("USSBIB", curlib);	// A cause du changement possible de curlib
		
		// R�cup�ration du libell� de la curlib
		LibraryRequest rlibrary = new LibraryRequest(curlib, LibraryRequest.GETTEXT);
		LibraryRequestActions tlib = new LibraryRequestActions(systeme.getSystem(), rlibrary); 
		tlib.actions();
		if( rlibrary.isSuccess() )
			temp.add("TEXTBIB", new JsonPrimitive(rlibrary.getText()));
		else
			temp.add("TEXTBIB", new JsonPrimitive("Erreur: " + rlibrary.getMsgError()));
		
		// R�cup�ration des infos pour la barre de titre de la fen�tre (etb/mag du user)
		UtilisateurETB_MAG userInfosETB_MAG = new UtilisateurETB_MAG(systeme);
		temp.add("TEXTTITLEBARINFOS", new JsonPrimitive(userInfosETB_MAG.getTitleBarInfos(profil, curlib)));
		
		// Pr�paration s'il y a des biblioth�ques sp�cifiques
		if( (listebibspe != null) || (listebibspe.size() > 0) )
		{
			final JsonArray lstbibspe = new JsonArray();
			for (int i=0; i<listebibspe.size(); i++)
				lstbibspe.add(new JsonPrimitive(listebibspe.get(i)));
			// S'il y a des biblioth�ques sp�cifiques ont les ajoute
			if (lstbibspe.size() > 0)
				temp.add("LSTBIBSPE", lstbibspe);
		}

		// S'il y a un soucis avec les menus perso on n'envoi un message au client
		if( !menupersonotfound ){
			temp.addProperty("MSGMENUPERSO", "Le menu " + menuperso + " n'a pas �t� trouv� sur le serveur.");
		}
		
		final String chaine = getLetasp() + gson.toJson(temp);
		
		// Modif JSON
		return toOndeSrv.sendMessageDataQueue(eMsg.getFlat(), chaine, chaine.length());
	}

	/**
	 * Envoi les informations des groupes de modules au client
	 * @return
	 */
	private boolean sendGroupModuleJSON()
	{
		// Pr�paration de l'ent�te du message
		final String compression="1";
		final EnteteMsgRPG eMsg = getEnteteMsg();
		eMsg.setMSGIDM("20001");
		eMsg.setMSGBLC(compression); // <- Indique que le message est compress�
		
		// Requ�te pour r�cup�rer la liste des modules (GVM, CGM, ...) pour les menus
		final ArrayList<GenericRecord>liste = getListeModules();
		if ((liste == null) || (liste.size() == 0)) return false;
		
		final int nbr = liste.size();
		boolean retour=false;
		final JsonArray tab = new JsonArray();

		// On construit une chaine avec toutes les infos
		for (int i=0; i<nbr; i++)
		{
			//liste.get(i).setDescription(grdMnemMnpm);
			// On ajoute le WGRP
//System.out.println("[MenuPrincipal] (sendGroupModuleJSON)-> MNEMOD:" + (String)liste.get(i).getValueField("MNEMOD"));		
			String wgrp = getGroupNumberOfModule((String)liste.get(i).getField("MNEMOD"));
			JsonObject temp = liste.get(i).getJSON(false);
			temp.add("WGRP", new JsonPrimitive(wgrp));
			tab.add(temp);
		}
		StringBuffer sb = eMsg.getFlat();
//System.out.println("--> sendGroupModuleJSON entete : " +sb.length() + " " + sb);
		//System.out.println("--> sendGroupModuleJSON corps  : " +tab.size() + " " + Base64Coder.Compress(tab.toJSONString(), "UTF-8").length());
//System.out.println("--> sendGroupModuleJSON entete : " +toOndeSrv.getDtaq().getPath());

		if (compression.equals("1"))
			retour = toOndeSrv.sendMessageDataQueue(sb, Base64Coder.Compress(gson.toJson(tab), "UTF-8"), Constantes.DATAQLONG-sb.length());
		else
			retour = toOndeSrv.sendMessageDataQueue(sb, gson.toJson(tab), Constantes.DATAQLONG-sb.length());
		//tab.clear();
		liste.clear();
		
		// On envoi la balise de fin 
		if (retour)
		{
			// Pr�paration de l'ent�te du message
			eMsg.setMSGIDM("20011");
			eMsg.setMSGBLC(compression);
			retour = toOndeSrv.sendMessageDataQueue(eMsg.getFlat().toString());
		}
//System.out.println(tab.toJSONString());
//System.out.println("--> taille totale:" + tab.toJSONString().length() + " " + retour);
		return retour;
	}

	/**
	 * Envoi les informations du groupe sp�cifique au client
	 * @return
	 *
	private boolean sendSPEGroupModuleJSON()
	{
		// Pr�paration de l'ent�te du message
		final String compression="1";
		final EnteteMsgRPG eMsg = getEnteteMsg();
		eMsg.setMSGIDM("20001");
		eMsg.setMSGBLC(compression); // <- Indique que le message est compress�
		
		// Requ�te pour r�cup�rer la liste des modules (GVM, CGM, ...) pour les menus
		final ArrayList<GenericRecord>liste = getListeModules();
		if ((liste == null) || (liste.size() == 0)){ // Dans le cas o� on n'a pas de menu sp�cifique on envoi un code 20013
			eMsg.setMSGIDM("20013");
			return toOndeSrv.sendMessageDataQueue(eMsg.getFlat().toString());
		}
		
		final int nbr = liste.size();
		boolean retour=false;
		final JsonArray tab = new JsonArray();

		// On construit une chaine avec toutes les infos
		for (int i=0; i<nbr; i++)
		{
			//liste.get(i).setDescription(grdMnemMnpm);
			// On ajoute le WGRP
//System.out.println("[MenuPrincipal] (sendGroupModuleJSON)-> MNEMOD:" + (String)liste.get(i).getValueField("MNEMOD"));		
			String wgrp = getGroupNumberOfModule((String)liste.get(i).getField("MNEMOD"));
			if( !wgrp.trim().equals("6") ) continue;
			JsonObject temp = liste.get(i).getJSON(false);
			temp.add("WGRP", new JsonPrimitive(wgrp));
			tab.add(temp);
		}
		StringBuffer sb = eMsg.getFlat();
//System.out.println("--> sendGroupModuleJSON entete : " +sb.length() + " " + sb);
		//System.out.println("--> sendGroupModuleJSON corps  : " +tab.size() + " " + Base64Coder.Compress(tab.toJSONString(), "UTF-8").length());
//System.out.println("--> sendGroupModuleJSON entete : " +toOndeSrv.getDtaq().getPath());

//System.out.println("-tab->" + tab.size());		
		if( tab.size() == 0 ){ // Dans le cas o� on n'a pas de menu sp�cifique on envoi un code 20013
			eMsg.setMSGIDM("20013");
//System.out.println("-msg->" + eMsg.getFlat().toString());
			return toOndeSrv.sendMessageDataQueue(eMsg.getFlat().toString());
		}else{
		if (compression.equals("1"))
			retour = toOndeSrv.sendMessageDataQueue(sb, Base64Coder.Compress(gson.toJson(tab), "UTF-8"), Constantes.DATAQLONG-sb.length());
		else
			retour = toOndeSrv.sendMessageDataQueue(sb, gson.toJson(tab), Constantes.DATAQLONG-sb.length());
		}
		//tab.clear();
		liste.clear();
		
		// On envoi la balise de fin 
		if (retour)
		{
			// Pr�paration de l'ent�te du message
			eMsg.setMSGIDM("20011");
			eMsg.setMSGBLC(compression);
			retour = toOndeSrv.sendMessageDataQueue(eMsg.getFlat().toString());
		}
//System.out.println(tab.toJSONString());
//System.out.println("--> taille totale:" + tab.toJSONString().length() + " " + retour);
		return retour;
	}*/

	/**
	 * Retourne la liste des biblioth�ques sp�cifiques 
	 * @param acurlib
	 * @return
	 */
	private ArrayList<String> getListBibSpe(String acurlib)
	{
		SpecificLibraries spm = new SpecificLibraries(systeme);
		if ((getBibjob() != null) && (!getBibjob().trim().equals("")))
			return spm.getListeBib(false, getBibjob(), acurlib, getLetasp());
		return null;
	}

	/**
	 * Recherche le num�ro de groupe
	 * @param nameModule
	 * @return
	 */
	private String getGroupNumberOfModule(String nameModule)
	{
		if (nameModule == null) return "0";
		for (int i=0; i<WGRP.length; i++)
			if (WGRP[i][0].equals(nameModule)) return WGRP[i][1];
		return "0";
	}
	
	/**
	 * Retourne la liste les modules (sans le d�tail)
	 * @return
	 */
	private ArrayList<GenericRecord> getListeModules()
	{
		if( sModel.isEmpty() ){
			return getListeModulesAll();
		}
		
		return getListeModulesCustom();
	}
	
	/**
	 * Retourne la liste les modules (sans le d�tail) - menu entier
	 * @return
	 */
	private ArrayList<GenericRecord> getListeModulesAll()
	{
		final char lettreAsp = getLetasp();
		String request = "select * from "+lettreAsp+"expas.mnemsgm E, "+lettreAsp+"expas.mnpmsgm P where mnpp0 = '01' and mnpp1 <> '' and mnpp1 < '30' and mnpord <> 01268 and mnpp2 = '' and mnelan = 0  and mnpord = mneord and (mnemod <> 'PAM' and mnemod <> 'GSM' and mnemod <> 'TDS' and mnemod <> 'GNF' and mnemod <> 'GHM') order by mnpp0, mnpp1, mnpp2, mnpp3, mnpp4, mnpp5";

		// Lecture des fichiers MNPM et MNEM 
		ArrayList<GenericRecord>liste = grm.select( request );
		if( liste == null ){
			msgErreur += "\n" +NOM_CLASSE+ "(sendGroupModule) Aucune information sur les modules du menu.";
			liste = new ArrayList<GenericRecord>();
		}
		
		// Lecture des menus sp�cifiques et ajout dans la liste principale s'il y en a
		// Attention il est important qu'il y ait SPM dans la zone MNEMOD
		if( (listebibspe != null) && (listebibspe.size() > 0) )
		{
			int indice = 1;
			for( String spe: listebibspe ){
				ArrayList<GenericRecord>listespe = grm.select("select * from "+spe+".mnspmem E, "+spe+".mnspmpm P where mnpp0 = '01' and mnpp1 <> '' and mnpp1 < '30' and mnpp2 = '' and mnelan = 0  and mnpord = mneord and (mnemod <> 'PAM' and mnemod <> 'GSM' and mnemod <> 'TDS' and mnemod <> 'GNF' and mnemod <> 'GHM') order by mnpp0, mnpp1, mnpp2, mnpp3, mnpp4, mnpp5");
				if( listespe != null ){
					// On va changer dynamiquement les valeurs de MNEMOD & MNPP1 afin que l'affichage des menus soit correct (aussi bien en 5250 qu'en S�rie N)
					for( int i=0; i<listespe.size(); i++ ){
						listespe.get(i).setField("MNEMOD", String.format("S%02d", indice));
						listespe.get(i).setField("MNPP1", String.format("%02d", indice));
						liste.add(listespe.get(i));	
					}
					listespe.clear();
				}
				indice++;
			}
		}
		
		return liste;
	}

	/**
	 * Retourne la liste les modules (sans le d�tail) - menu perso
	 * @return
	 */
	private ArrayList<GenericRecord> getListeModulesCustom()
	{
		final char lettreAsp = getLetasp();
		final ArrayList<GenericRecord>liste = new ArrayList<GenericRecord>();

		// On construit les requ�tes
		StringBuilder cond = new StringBuilder(1024);
		for( baseModel bm : sModel.getBaseModel() ){
			// On cosntruit la requ�te
			bm.getConditions(cond);
			int size = cond.length();
			cond.delete(size-2, size);
			cond.insert(0, "mnpord in (").append(')');
			String fem = lettreAsp + bm.getLibrary() + "." +bm.getFileem(); 
			String fpm = lettreAsp + bm.getLibrary() + "." +bm.getFilepm(); 
			String request = "select * from "+ fem +" E, "+ fpm +" P where mnelan = 0 and mnpord = mneord and "+cond.toString()+" order by mnpp0, mnpp1, mnpp2, mnpp3, mnpp4, mnpp5";
			cond.setLength(0);
			
			// On lance la requ�te
			final ArrayList<GenericRecord>listetmp = grm.select( request );			
			if( listetmp != null ){
				for (int i=0; i<listetmp.size(); i++){
					listetmp.get(i).setField("library", bm.getLibrary());
					liste.add(listetmp.get(i));
				}
				listetmp.clear();
			}
		}

		return liste;
	}

	/**
	 * Envoi le d�tail sur un module (tous ses points de menu)
	 * @param mnpp1
	 * @return
	 */
	private boolean sendDetailModuleJSONwithCompression(String wgrp, String mnpp1, String mnpord, String lib)
	{
		// Pr�paration de l'ent�te du message
		final String compression="1";
		final EnteteMsgRPG eMsg = getEnteteMsg();
		eMsg.setMSGIDM("20002");
		eMsg.setMSGBLC(compression); // <- Indique que le message est compress�

		// Requ�te pour r�cup�rer les donn�es sur les menus
		ArrayList<GenericRecord>liste = getDetailModule(wgrp, mnpp1, mnpord, lib);
		if ((liste == null) || (liste.size() == 0)) return false;
		
		// On g�re les cas particuliers (par exemple car du MNPTYP = '*')
		traitementMenuParticuliers(liste);

		final int nbr = liste.size();
		boolean retour=false;
		final JsonArray tab = new JsonArray();
		
		// On construit une chaine avec toutes les infos
		for (int i=0; i<nbr; i++)
		{
			JsonObject temp = liste.get(i).getJSON(false);
//System.out.println("-*> " + wgrp + " " + getGroupNumberOfModule((String)liste.get(i).getValueField("MNEMOD")));			
			temp.add("WGRP", new JsonPrimitive(wgrp));
			tab.add(temp);
		}
//System.out.println("Longueur requete: " + requete.length());		

		final StringBuffer sb = eMsg.getFlat();
		//System.out.println("--> sendDetailModuleJSONwithCompression entete : " +sb.length() + " " + sb);
		//System.out.println("--> sendDetailModuleJSONwithCompression corps  : " +tab.size() + " " + Base64Coder.Compress(tab.toJSONString(), "UTF-8").length());
		// On envoi la balise de fin
		if (compression.equals("1"))
			retour = toOndeSrv.sendMessageDataQueue(sb, Base64Coder.Compress(gson.toJson(tab), "UTF-8"), Constantes.DATAQLONG-sb.length());
		else
			retour = toOndeSrv.sendMessageDataQueue(sb, gson.toJson(tab), Constantes.DATAQLONG-sb.length());
		//tab.clear();
		liste.clear();
		
		if (retour)
		{
			// Pr�paration de l'ent�te du message
			eMsg.setMSGIDM("20012");
			eMsg.setMSGBLC(compression);
			retour = toOndeSrv.sendMessageDataQueue(eMsg.getFlat().toString());
		}

		return retour;
	}

	/**
	 * Retourne la liste des points de menu du module demand�
	 * @param mnpp1
	 * @return
	 */
	private ArrayList<GenericRecord> getDetailModule(String wgrp, String mnpp1, String mnpord, String lib)
	{
		if (wgrp == null) return null;
		
		wgrp = wgrp.trim();
		if( mnpp1 != null ){
			mnpp1 = mnpp1.trim();
		}
		if( mnpord != null ){
			mnpord = mnpord.trim();
		}
//System.out.println("wgrp = " + wgrp + " mnpp1 = " + mnpp1 + " mnpord = " + mnpord);

		if( sModel.isEmpty() ){
			return getDetailModuleAll(wgrp, mnpp1, mnpord, null);
		} else{
			return getDetailModuleCustom(wgrp, mnpp1, mnpord, lib);
		}
	}
	
	/**
	 * Retourne la liste des points de menu du module demand� - menu entier
	 * @param mnpp1
	 * @return
	 */
	private ArrayList<GenericRecord> getDetailModuleAll(String wgrp, String mnpp1, String mnpord, String lib)
	{
		ArrayList<GenericRecord>liste = null;
		final char lettreAsp = getLetasp();
		// Lecture des menus "normaux"
		if( !wgrp.equals("6") ){
			if( mnpord == null ){
				String request = "select * from "+lettreAsp+"expas.mnemsgm E, "+lettreAsp+"expas.mnpmsgm P where mnpp0 = '01' and mnpp1 = '"+mnpp1+"' and mnelan = 0 and mnpord = mneord  order by mnpp0, mnpp1, mnpp2, mnpp3, mnpp4, mnpp5";
				// Lecture du fichier MNP1 et MNE0 (Attention le order by est super important sinon crash de oTree dans le client construction des menus)
				liste = grm.select(request);
				if (liste == null)
					msgErreur += "\n" +NOM_CLASSE+ "(getDetailModuleAll) Aucune information sur le module (MNPP1) : " + mnpp1;
			}
		} else{
			// Lecture des menus sp�cifiques et ajout dans la liste principale s'il y en a
			// Attention il est important qu'il y ait SPM dans la zone MNEMOD
			if (listebibspe != null){
				int indice = Integer.parseInt(mnpp1);
				liste = grm.select("select * from "+listebibspe.get(indice-1)+".mnspmem E, "+listebibspe.get(indice-1)+".mnspmpm P where mnpp0 = '01' and mnpp1 <> '' and mnelan = 0 and mnpord = mneord  order by mnpp0, mnpp1, mnpp2, mnpp3, mnpp4, mnpp5");
				if( liste == null )
					msgErreur += "\n" +NOM_CLASSE+ "(getDetailModuleAll) Aucune information sur le module (MNPP1) pour les sp�cifiques: " + mnpp1;
				else {
					// On va changer dynamiquement les valeurs de MNEMOD & MNPP1 afin que l'affichage des menus soit correct (aussi bien en 5250 qu'en S�rie N)
					for( int i=0; i<liste.size(); i++ ){
						liste.get(i).setField("MNEMOD", String.format("S%02d", indice));
						liste.get(i).setField("MNPP1", String.format("%02d", indice));
					}
				}
			}			
		}
		return liste;

	}
	
	/**
	 * Retourne la liste des points de menu du module demand� - menu perso
	 * @param mnpp1
	 * @return
	 */
	private ArrayList<GenericRecord> getDetailModuleCustom(String wgrp, String mnpp1, String mnpord, String lib)
	{
		final char lettreAsp = getLetasp();
		StringBuilder cond = new StringBuilder(1024);

		// On construit la requ�te
		ArrayList<String> lstdet =  sModel.getListOrd(lib, mnpord);
		for( String det : lstdet ){
			cond.append(det).append(", ");
		}
		int size = cond.length();
		cond.delete(size-2, size);
		cond.insert(0, "mnpord in (").append(')');

		String fem = lettreAsp + lib + "." + sModel.getFileEm(lib); 
		String fpm = lettreAsp + lib + "." + sModel.getFilePm(lib); 
		String request = "select * from "+ fem +" E, "+ fpm +" P where mnelan = 0 and mnpord = mneord and "+cond.toString()+" order by mnpp0, mnpp1, mnpp2, mnpp3, mnpp4, mnpp5";
		
		// On lance la requ�te
		return grm.select( request );	
	}
	
	/**
	 * Traitement des cas particuliers pour les menus
	 * @param liste
	 */
	private void traitementMenuParticuliers(ArrayList<GenericRecord> liste)
	{
		int i = 0;
		while( i < liste.size() ){
			if( ((String)liste.get(i).getField("MNPTYP")).equals("*") ){
				traitementTypeMenuEtoile(liste, liste.get(i));
			}
			i++;
		}
	}
	
	/**
	 * Traitement des cas o� le menu est de type '*'
	 * @param liste
	 * @param indice
	 */
	private void traitementTypeMenuEtoile(ArrayList<GenericRecord> liste, GenericRecord rcd)
	{
		char lettreAsp = getLetasp();
		
		// On lance le programme qui va nous indiquer quoi faire
		String mnptyp = (String)rcd.getField("MNPTYP");
		String mnpar5 = (String)rcd.getField("MNPAR5");
		if( mnpar5.charAt(0) == '*' ){
			// Lancement programme
			ProgramParameter[] parameterList = new ProgramParameter[1];
	        AS400Text as_mnptyp = new AS400Text(1);
	        parameterList[0] = new ProgramParameter(as_mnptyp.toBytes(mnptyp), 1);
	        CallProgram rpg = new CallProgram(systeme.getSystem(), "/QSYS.LIB/"+lettreAsp+"EXPAS.LIB/"+mnpar5.substring(1).trim()+".PGM", parameterList);
	        boolean ret = rpg.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('"+lettreAsp+"')");
	        if( !ret )
	        	System.out.println("-traitementTypeMenuEtoile->CHGDTAARA : " + rpg.getCodeErreur() + " " + rpg.getMsgError());
	        rpg.addLibrary(curlib, true);
	        //rpg.addLibrary("M_GPL", false);
	        rpg.addLibrary(getBibjob(), false);
	        rpg.addLibrary(lettreAsp + "EXPAS", false);
	        ret = rpg.execute();
	        if( !ret )
	        	System.out.println("-traitementTypeMenuEtoile->CHGDTAARA : " + rpg.getCodeErreur() + " " + rpg.getMsgError());
			mnptyp = (String) as_mnptyp.toObject(parameterList[0].getOutputData());
		} else{
			return;
		}
//System.out.println("-traitementTypeMenuEtoile->mnptyp:"+mnptyp);
		// Si la valeur du mnptyp vaut 1 alors on lance le programme directement et donc on supprime les sous-menus
		if( mnptyp.equals("1") ){
			int i = 0;
			while( i < liste.size() ){
				if( ((String)liste.get(i).getField("MNPP0")).equals((String)rcd.getField("MNPP0")) &&
					((String)liste.get(i).getField("MNPP1")).equals((String)rcd.getField("MNPP1")) &&
					((String)liste.get(i).getField("MNPP2")).equals((String)rcd.getField("MNPP2")) &&
					((String)liste.get(i).getField("MNPP3")).equals((String)rcd.getField("MNPP3")) &&
					!((String)liste.get(i).getField("MNPP4")).trim().equals("")
				){
//System.out.println("-traitementTypeMenuEtoile->suppression de "+liste.get(i).getField("MNPP0")+" "+liste.get(i).getField("MNPP1")+" "+liste.get(i).getField("MNPP2")+" "+liste.get(i).getField("MNPP3")+" "+liste.get(i).getField("MNPP4")+" "+liste.get(i).getField("MNPP5"));
					liste.remove(i);
				} else{
					i++;
				}
			}
			rcd.setField("MNPAR5", "");
		} else if( mnptyp.equals(" ") ){ // Nous sommes sur un noeud donc on supprime les r�f�rences au programme � lancer directement
			rcd.setField("MNPARG", "");
			rcd.setField("MNPAR1", "");
			rcd.setField("MNPAR2", "");
			rcd.setField("MNPAR3", "");
			rcd.setField("MNPAR4", "");
			rcd.setField("MNPAR5", "");
		}
		// On met � jour la valeur du MNPTYP gr�ce � la valeur de retour du programme 
		rcd.setField("MNPTYP", mnptyp);
	}
	
	/**
	 * Cr�ation de l'ent�te des messages (TODO � voir si plus intelligent d'une instanciation unique)
	 * @return
	 */
	private EnteteMsgRPG getEnteteMsg()
	{
		final EnteteMsgRPG eMsg = new EnteteMsgRPG();
		eMsg.setMSGNJB(getDevice());
		eMsg.setMSGRCD("");
		eMsg.setMSGUSR(profil);
		eMsg.setMSGMOD("");
		eMsg.setMSGFMT("");
		eMsg.setMSGPRG("MENUPRINCIPAL");

		return eMsg;
	}
	
	/**
	 * Renvoie les informations de l'utilisateur suite au changement de Curlib 
	 * @param acurlib
	 */
	private void sendRefreshDataChgCurlib(String acurlib)
	{
		curlib = acurlib.trim();
//System.out.println("-CURLIB->" + curlib);		
		listebibspe = getListBibSpe(curlib);
//System.out.println("-bibspe->" + (listebibspe!=null?listebibspe.size():"null"));		
		//sendDataUserJSON();
		//sendSPEGroupModuleJSON();
		
		// Envoi des infos sur l'utilisateur au client
		if (!sendDataUserJSON()) return;

		// Envoi des modules
		if (!sendGroupModuleJSON()) return;

	}
	
	/**
	 * Retourne les informations pour la barre de titre de la fen�tre de S�rie N
	 * @return
	 *
	private String getTitleBarInfos(String user, String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return "";
		
		PgvmsecmManager security = new PgvmsecmManager(systeme.getdatabase().getConnection());
		security.setLibrary(curlib);
		PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getdatabase().getConnection());
		pgvmparm.setLibrary(curlib);
	
		// Lecture de la DG blanche
		Societe infosSociete = new Societe();
		pgvmparm.getInfosSociete(infosSociete);
		
		// On recherche l'�tablissement de l'utilisateur
		Etablissement etbuser = new Etablissement();
		etbuser.setCodeEtablissement(security.getEtablissementUser(user));
		// Il en a pas, on prend celui de la DG blanche
		if (etbuser.getCodeEtablissement() == null)
		{
			//System.out.println("Pas de ETB USER, donc on prend celui de la DG: " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale() + " " + infosSociete.getEtablissementPilote().getMagasinGeneral());
			etbuser.setCodeEtablissement(infosSociete.getCodeEtablissementPilote());
		}
		// On va r�cup�rer les informations compl�mentaires de l'�tablissement
		pgvmparm.getInfosEtablissement(etbuser);

		// R�cup�ration des infos sur le magasin
		Magasin magasin = new Magasin(); 
		ExtendRecord record = security.getRecordsbyUSERandETB(user, etbuser.getCodeEtablissement());
		// De l'�tablissement si le user n'en a pas 
		if (record == null)
			magasin.setCodeMag(etbuser.getMagasinGeneral());
		else
			magasin.setCodeMag((String) record.getField("SEMAUS"));
		// R�cup�ration du libell� du code magasin
		pgvmparm.getInfosMagasin(etbuser.getCodeEtablissement(), magasin);

		String mag = magasin.getCodeMag() + " " + magasin.getMALIB();
		if (mag.trim().equals(""))
			return " - " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale();
		return " - " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale() + " - " + mag;
	}*/
	
	/**
	 * Ferme proprement le programme
	 */
	protected void destructor()
	{
		super.destructor();
		
		if( sModel != null ) sModel.dispose();

//System.out.println("--> destructor");		
		if (log != null) log.fermetureLog();
	}
	
	/**
	 * Progtramme principal
	 * @param args
	 *
	public static void main(String[] args)
	{
		//SystemManager systeme = new SystemManager("172.31.1.249", "RIDEVSV", "gar1972", true);
		//SystemManager systeme = new SystemManager(true);

		//MenuPrincipal test = new MenuPrincipal(systeme, "RIDEVSV", "SGMTEST", "M_GPL");
		MenuPrincipal test = new MenuPrincipal(args[0], args[1], args[2], args[3]==null?'W':args[3].charAt(0));
		test.treatment();
		//test.destructor();
		System.out.println("--> Programme termin�.");
		//System.exit(0); // TODO rechercher s'il y a des fuite m�moire
	}*/

}
