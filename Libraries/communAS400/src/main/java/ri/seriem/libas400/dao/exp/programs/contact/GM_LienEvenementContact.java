package ri.seriem.libas400.dao.exp.programs.contact;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;

public class GM_LienEvenementContact extends BaseGroupDB
{

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_LienEvenementContact(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Contr�le qu'un contact soit li� � des �v�nements
	 * @param contact
	 * @return
	 */
	public boolean checkContactWithoutLink(M_Contact contact)
	{
		if( contact == null){
			msgErreur += "\nLa classe M_Contact est nulle";
			return false;
		}
		
		String requete = "select count(*) from " + querymg.getLibrary() + ".PSEMLECM where ECIDC = "+contact.getRENUM()+" and ECETBC = '" + contact.getREETB()+"'";
		String resultat = querymg.firstEnrgSelect(requete, 1);
		if( resultat == null )
			return false;
		else
			if( resultat.equals("0") )
				return true;
		return false;
	}

	@Override
	public void dispose()
	{
		querymg = null;
	}

}
