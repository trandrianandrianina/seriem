//=================================================================================================
//==>                                                                       19/12/2011 - 19/12/2011
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.spm.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;


public class PsemdinmManager extends QueryManager
{
	/**
	 * Constructeur
	 * @param database
	 */
	public PsemdinmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Retourne la liste des modules (pour les menus)
	 * @param bibenv
	 * @param prf
	 * @return
	 */
	public ArrayList<GenericRecord> getTousEnregistrements()
	{
		// On s�lectionne tous les groupes sauf LTM (01268)
		return select("Select * from fm500.psemdinm ");
	}
    
}
