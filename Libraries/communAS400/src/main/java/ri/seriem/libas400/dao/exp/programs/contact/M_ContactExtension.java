package ri.seriem.libas400.dao.exp.programs.contact;

import ri.seriem.libas400.dao.exp.database.files.FFD_Psemrtym;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libcommun.outils.InfosMailServer;

/**
 * Classe contenant les zones du PSEMRTYM
 * @author Administrateur
 *
 */
public class M_ContactExtension extends FFD_Psemrtym
{
	// Variables de travail
	private						InfosMailServer				infosMailServer = null;
	
	
	
	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_ContactExtension(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Ins�re l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean insertInDatabase()
	{
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PSEMRTYM", querymg.getLibrary());
		return request(requete);
	}

	/**
	 * Modifie l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean updateInDatabase()
	{
		initGenericRecord(genericrecord, false);
		String requete = genericrecord.createSQLRequestUpdate("PSEMRTYM", querymg.getLibrary(), "RENUM=" + getRYNUM() + " and REETB='" + getRYETB() + "'");
		return request(requete);
	}

	/**
	 * Suppression de l'enregistrement courant
	 * @return
	 */
	@Override
	public boolean deleteInDatabase()
	{
		String requete = "delete from " + querymg.getLibrary() + ".PSEMRTYM where RENUM=" + getRYNUM() + " and REETB='" + getRYETB() + "'";
		return request(requete);
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();
	}

	/**
	 * @return le infosMailServer
	 */
	public InfosMailServer getInfosMailServer()
	{
		if( infosMailServer == null ){
			infosMailServer = new InfosMailServer();
			infosMailServer.setHosto(RYSRVS);
			infosMailServer.setPorto(RYPORS);
			infosMailServer.setHosti(RYSRVE);
			infosMailServer.setPorti(RYPORE);
			infosMailServer.setEmail(RYMAIL);
			infosMailServer.setLogin(RYCPTE);
			infosMailServer.setPassword(RYPASS);
		}
		return infosMailServer;
	}

	/**
	 * @param infosMailServer le infosMailServer � d�finir
	 */
	public void setInfosMailServer(InfosMailServer infosMailServer)
	{
		this.infosMailServer = infosMailServer;
	}

	
	// -- M�thodes priv�es ----------------------------------------------------
	
	
	// -- Accesseurs ----------------------------------------------------------

}
