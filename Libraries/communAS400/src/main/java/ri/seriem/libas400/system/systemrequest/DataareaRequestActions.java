package ri.seriem.libas400.system.systemrequest;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libcommun.protocoleMsg.DataareaRequest;
import ri.seriem.libcommun.protocoleMsg.LibraryRequest;

public class DataareaRequestActions
{
	private AS400 sysAS400=null;
	private DataareaRequest rdta=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public DataareaRequestActions(AS400 system, DataareaRequest ld)
	{
		sysAS400 = system;
		rdta = ld;
	}
	
	/**
	 * Traitement des actions possibles 
	 */
	public void actions()
	{
		//int theactions = actions;
		for (int i=LibraryRequest.ACTIONS.size(); --i>0;)
		{
			if (rdta.getActions() >= LibraryRequest.ACTIONS.get(i))
			{
				switch(LibraryRequest.ACTIONS.get(i))
				{
					case DataareaRequest.ISEXISTS:
						dataareaExists();
						break;
					case DataareaRequest.CREATE:
						dataareaCreate();
						break;
					case DataareaRequest.READ:
						dataareaRead();
						break;
					case DataareaRequest.WRITE:
						dataareaWrite();
						break;
					//case LibraryRequest.LIST:
					//	libraryList();
					//	break;
					//case LibraryRequest.LISTINTO:
					//	libraryListInto();
					//	break;
				}
				rdta.setActions(rdta.getActions()- LibraryRequest.ACTIONS.get(i));
			}
		}
	}

	/**
	 * Traite la demande de v�rification d'une dataarea
	 */
	private void dataareaExists()
	{
		// Contr�le des variables 
		String dataarea = rdta.getDataarea();
		if ((dataarea == null) || (dataarea.length() > 10))
		{
			rdta.setSuccess(false);
			rdta.setExist(false);
			rdta.setMsgError("Le nom de la dataarea est incorrect : " + dataarea);
			return;
		}
		String library = rdta.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rdta.setSuccess(false);
			rdta.setExist(false);
			rdta.setMsgError("Le nom de la biblioth�que est incorrect : " + library);
			return;
		}
		
        final String racine = "/QSYS.LIB/"+library+".LIB/" + dataarea + ".DTAARA";
        final IFSFile dta = new IFSFile(sysAS400, racine);
        try
		{
        	rdta.setSuccess(true);
        	rdta.setExist(dta.exists());
        	rdta.setMsgError("La dataarea " + dataarea + " existe.");
		}
		catch (IOException e)
		{
			rdta.setSuccess(false);
			rdta.setExist(false);
			rdta.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la cr�ation d'une biblioth�que
	 */
	private void dataareaCreate()
	{
		String dataarea = rdta.getDataarea();
		String library = rdta.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		dataareaExists();
		if (rdta.isExist())
		{
			rdta.setSuccess(true);
			rdta.setMsgError("La dataarea "+dataarea+" existe d�j� dans " + library);
			return;
		}

		// Sinon on l'a cr��
		CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
		try
		{
			dta.create();
			rdta.setMsgError(null);
			rdta.setSuccess(true);
		}
		catch (Exception e)
		{
			rdta.setSuccess(false);
			rdta.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la demande de r�cup�ration du texte de la dataarea
	 */
	private void dataareaRead()
	{
		String dataarea = rdta.getDataarea();
		String library = rdta.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		dataareaExists();
		if (!rdta.isExist())
		{
			rdta.setSuccess(true);
			rdta.setData(null);
			return;
		}

		CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
		try
		{
			rdta.setSuccess(true);
			rdta.setMsgError(null);
			rdta.setData(dta.read());
		}
		catch (Exception e)
		{
			rdta.setSuccess(false);
			rdta.setMsgError(e.getMessage());
			rdta.setData(null);
		}
	}

	/**
	 * Traite la demande de r�cup�ration du texte de la dataarea
	 */
	private void dataareaWrite()
	{
		String dataarea = rdta.getDataarea();
		String library = rdta.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		dataareaExists();
		if (!rdta.isExist())
		{
			rdta.setSuccess(true);
			return;
		}

		CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
		try
		{
			dta.write(rdta.getData());
			rdta.setSuccess(true);
			rdta.setMsgError(null);
		}
		catch (Exception e)
		{
			rdta.setSuccess(false);
			rdta.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la demande de listage d'une biblioth�que
	 *
	private void libraryList()
	{
		String dataarea = rdta.getDataarea();
		String library = rdta.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		dataareaExists();
		if (!rdta.isExist())
		{
			rdta.setSuccess(false);
			rdta.setText(null);
			return;
		}

		//CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
		dataarea = dataarea.toUpperCase();
		library = library.toUpperCase();
		rdta.getVariant().clear();
		rdta.setTypeVariant("LIST_NAMEDTA");

		try
		{
			ObjectList ol = new ObjectList(sysAS400, "QSYS", library, "*DTA");
			ol.load();
		
			Enumeration enu = ol.getObjects();
			while(enu.hasMoreElements()){
				ObjectDescription od = (ObjectDescription) enu.nextElement();
				rlib.getVariant().add(od.getName());
			}
			
			rlib.setSuccess(true);
			rlib.setMsgError(null);
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}*/

	/**
	 * Traite la demande de listage d'une biblioth�que
	 *
	private void libraryListInto()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setMsgError("Le nom de la biblioth�que est incorrect : " + library);
			rlib.setText(null);
		}
		library = library.toUpperCase();
		rlib.getVariant().clear();
		rlib.setTypeVariant("NAMEOBJ");

		try
		{
			ObjectList ol = new ObjectList(sysAS400, "QSYS", library, "*LIB");
			ol.load();
		
			Enumeration enu = ol.getObjects();
			while(enu.hasMoreElements()){
				ObjectDescription od = (ObjectDescription) enu.nextElement();
				rlib.getVariant().put(od.getName(), null);
			}
			
			rlib.setSuccess(true);
			rlib.setMsgError(null);
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}*/

}
