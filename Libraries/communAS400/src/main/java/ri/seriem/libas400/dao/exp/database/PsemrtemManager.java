//=================================================================================================
//==>                                                                       19/12/2011 - 23/07/2015
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.dao.exp.programs.contact.M_Contact;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;


public class PsemrtemManager extends QueryManager
{
	/**
	 * Constructeur
	 * @param database
	 */
	public PsemrtemManager(Connection database)
	{
		super(database);			
	}

	/**
	 * Retourne la liste des contacts
	 * @param curlib
	 * @return
	 */
	public ArrayList<GenericRecord> getTousEnregistrements(String curlib)
	{
		return select("Select * from " + curlib + ".psemrtem");
	}

	/**
	 * Retourne les informations primordiales des contacts
	 * @param curlib
	 * @return
	 */
	public ArrayList<GenericRecord> getCivNomPrenom(String curlib)
	{
		return select("Select RENUM, REETB, RECIV, REPAC, RENOM, REPRE from " + curlib + ".psemrtem");
	}

	/**
	 * Retourne une liste des civilit�s uniques 
	 * @param curlib
	 * @return
	 */
	public ArrayList<GenericRecord> getToutesLescivilites(String curlib)
	{
		return select("Select distinct reciv from " + curlib + ".psemrtem order by reciv asc");
	}

	/**
	 * Retourne la liste des �tablissements des contacts disponibles
	 * @param curlib
	 * @return
	 */
	public ArrayList<GenericRecord> getTousLesEtablissements(String curlib)
	{
		return select("Select distinct reetb from " + curlib + ".psemrtem");
	}
	
	/**
	 * Retourne le generic record du contact � partir de l'�tablissement et du profil
	 * @param curlib
	 * @param etb
	 * @return
	 */
	public GenericRecord getGRContact(String curlib, String etb, String profil){
		if( ( etb == null ) || (profil == null) )
			return null;
		
		ArrayList<GenericRecord>liste = select("Select * from " + curlib + ".psemrtem where REETB='"+etb+"' and REPRF='"+profil+"'");
		if( ( liste != null ) && !liste.isEmpty())
			return liste.get(0);
		else
			return null;
	}
	
	/**
	 * Retourne le contact � partir de l'�tablissement et du profil
	 * @param curlib
	 * @param etb
	 * @param profil
	 * @return
	 */
	public M_Contact getContact(String curlib, String etb, String profil){
		GenericRecord record = getGRContact(curlib, etb, profil);
		if( record == null) return null;

		M_Contact contact = new M_Contact(this);
		record.toObject(contact);

		return contact;
	}
	
	//--> M�thodes priv�es <---------------------------------------------------
	
	
}
