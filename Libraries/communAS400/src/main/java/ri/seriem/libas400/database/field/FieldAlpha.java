//=================================================================================================
//==>                                                                       21/12/2011 - 24/10/2014
//==> Description de base d'un champ alphanumérique 
//=================================================================================================

package ri.seriem.libas400.database.field;

public class FieldAlpha extends Field<String> {
  // Variables
  public String valeur;
  
  /**
   * Constructeur
   */
  public FieldAlpha() {
    sqltype = "java.lang.String";
  }
  
  /**
   * Constructeur
   * @param nom
   * @param valeur
   * @param asize
   */
  public FieldAlpha(String nom, String valeur, int asize) {
    name = nom;
    sqltype = "java.lang.String";
    setValue(valeur);
    setSize(asize);
  }
  
  /**
   * Constructeur
   * @param valeur
   * @param asize
   */
  public FieldAlpha(String valeur, int asize) {
    sqltype = "java.lang.String";
    setValue(valeur);
    setSize(asize);
  }
  
  /**
   * Constructeur
   * @param asize
   */
  public FieldAlpha(int asize) {
    setSize(asize);
  }
  
  // -- Méthodes statiques --------------------------------------------------
  
  /**
   * Formate une chaine afin qu'elle puisse êtreintégrée dans une requête SQL
   * @param afield
   * @return
   */
  public static String format4Request(String afield) {
    if (afield == null)
      return afield;
    return afield.replaceAll("'", "''");
  }
  
  @Override
  public String getValue() {
    return valeur;
  }
  
  @Override
  public void setValue(String valeur) {
    this.valeur = valeur;
  }
  
  @Override
  public String toString() {
    return valeur;
  }
  
  @Override
  public String toFormattedString(int size, int adecimal) {
    if (valeur == null)
      return valeur;
    return String.format("%1$-" + size + "s", valeur);
    // ou "%-3.3s"
    // return String.format("%-"+size+"."+size+"s", valeur);
  }
  
  @Override
  public String toFormattedString() {
    return toFormattedString(length, 0);
  }
  
  @Override
  public String getTrimValue() {
    if (valeur != null)
      return valeur.trim();
    else
      return valeur;
  }
  
  @Override
  public void setSize(int asize) {
    length = asize;
  }
  
  @Override
  public void setSize(int asize, int adecimal) {
    length = asize;
  }
  
  @Override
  public String getSQLType() {
    return sqltype;
  }
  
  @Override
  public void setSQLType(String sqltype) {
    this.sqltype = sqltype;
  }
  
}
