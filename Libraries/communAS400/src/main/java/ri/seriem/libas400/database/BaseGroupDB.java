//=================================================================================================
//==>                                                                       17/09/2015 - 18/04/2016
//=================================================================================================
package ri.seriem.libas400.database;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.EnvUser;
import ri.seriem.libcommun.outils.LaunchRequest;
import ri.seriem.libcommun.protocoleMsg.DataareaRequest;
import ri.seriem.libcommun.protocoleMsg.Db2Request;
import ri.seriem.libcommun.protocoleMsg.UserspaceRequest;



public abstract class BaseGroupDB
{
	// Variables
	protected					QueryManager 				querymg					= null;
	protected					String						curlib					= null;
	protected					EnvUser						infoUser				= null;
	protected					LaunchRequest				lr						= null;
	protected					GsonBuilder  				builderJSON				= null;
	protected					Gson						gson					= null;

	
	protected					String						msgErreur				= ""; // Conserve le dernier message d'erreur �mit et non lu 

	
	/**
	 * Constructeur � utiliser sur l'AS400
	 * @param database
	 */
	public BaseGroupDB(QueryManager aquerymg)
	{
		setQuerymg(aquerymg);
		if( aquerymg != null )
			setCurlib(aquerymg.getLibrary());
	}

	/**
	 * Constructeur � utiliser sur le poste client
	 * @param database
	 */
	public BaseGroupDB(String acurlib, EnvUser ainfoUser)
	{
		setCurlib(acurlib);
		setInfoUser(ainfoUser);
	}

	protected ArrayList<GenericRecord> getGenericRecord()
	{
		final ArrayList<GenericRecord> lstrcd = null;
		
		if( lr == null ){
			lr = new LaunchRequest(infoUser) {
				@Override
				protected boolean traitementUserspace(UserspaceRequest uspc){ return false; }
				@Override
				protected boolean traitementData(DataareaRequest dta){ return false; }
				@Override
				protected boolean traitementData(Db2Request db2)
				{
					if( db2.isSuccess() ){
						//lstrcd = db2.getGenericRecord();
						return true;
					}

					fintraitement();
					return true;
				}
				@Override
				protected void fintraitement()
				{
				}
			};
		}
		
		return lstrcd;
	}

	/**
	 * Initialisation JSON
	 * @return
	 */
	public boolean initJSON()
	{
		//debut de la construction du JSON
		if(builderJSON==null)
			builderJSON = new GsonBuilder();
		if(gson == null)
			gson = builderJSON.create();
		
		return (builderJSON!=null && gson!=null);
	}

	
	//-- M�thodes abstract ----------------------------------------------------

	public Gson getGson()
	{
		return gson;
	}

	public void setGson(Gson gson)
	{
		this.gson = gson;
	}

	//protected	abstract ArrayList<GenericRecord>	getGenericRecord();
	public		abstract void	dispose();

	//-- Accesseurs -----------------------------------------------------------

	/**
	 * @return le querymg
	 */
	public QueryManager getQuerymg()
	{
		return querymg;
	}

	/**
	 * @param querymg le querymg � d�finir
	 */
	public void setQuerymg(QueryManager querymg)
	{
		this.querymg = querymg;
	}

	/**
	 * @return le curlib
	 */
	public String getCurlib()
	{
		return curlib;
	}

	/**
	 * @param curlib le curlib � d�finir
	 */
	public void setCurlib(String curlib)
	{
		this.curlib = curlib;
	}

	/**
	 * @return le infoUser
	 */
	public EnvUser getInfoUser()
	{
		return infoUser;
	}

	/**
	 * @param infoUser le infoUser � d�finir
	 */
	public void setInfoUser(EnvUser infoUser)
	{
		this.infoUser = infoUser;
	}

	/**
	 * Construit le message d'erreur proprement */
	protected void majError(String nvMessage)
	{
		if(msgErreur==null)
			msgErreur = nvMessage;
		else msgErreur+= "\n" + nvMessage;
	}
	
	/**
	 * Retourne le message d'erreur  et nettoie la variable "msgErreur" en fonction du boolean
	 * @return
	 *//*
	public String getMsgError(boolean onDetruit)
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		if(onDetruit)
			msgErreur = null;

		return chaine;
	}*/
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = null;

		return chaine;
	}

}
