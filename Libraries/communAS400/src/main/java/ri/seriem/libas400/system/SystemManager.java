//=================================================================================================
//==>                                                                       05/12/2011 - 28/04/2015
//==> G�re les connection au serveur et � la base de donn�es
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.AS400;

import ri.seriem.libas400.database.DatabaseManager;

public class SystemManager
{
	// Variables
	private AS400 systeme=null;
	private DatabaseManager database=null;
	private boolean connexionDatabase=false;
	private int qaqqini = DatabaseManager.NODEBUG;
	
	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 

	/**
	 * Constructeur
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 * @param drivernative
	 */
	public SystemManager(boolean connexiondatabase, boolean drivernative)
	{
		connexionDatabase = connexiondatabase;
		setSystem();
		
		setDatabase(null, null, drivernative);
	}

	/**
	 * Constructeur
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 * @param drivernative
	 * @param qaqqini
	 */
	public SystemManager(boolean connexiondatabase, boolean drivernative, int qaqqini)
	{
		connexionDatabase = connexiondatabase;
		setSystem();
		setQaqqini(qaqqini);
		setDatabase(null, null, drivernative);
	}

	/**
	 * Constructeur
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 * @param drivernative
	 */
	public SystemManager(AS400 asystem, boolean connexiondatabase, boolean drivernative)
	{
		connexionDatabase = connexiondatabase;
		setSystem(asystem);
		setDatabase(null, null, drivernative); 
	}

	/**
	 * Constructeur
	 * @param asystem
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 * @param drivernative
	 * @param qaqqini
	 */
	public SystemManager(AS400 asystem, boolean connexiondatabase, boolean drivernative, int qaqqini)
	{
		connexionDatabase = connexiondatabase;
		setSystem(asystem);
		setQaqqini(qaqqini);
		setDatabase(null, null, drivernative); 
	}

	/**
	 * Constructeur
	 * @param adrip
	 * @param profil
	 * @param mdp
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 * @param drivernative
	 */
	public SystemManager(String adrip, String profil, String mdp, boolean connexiondatabase, boolean drivernative)
	{
		connexionDatabase = connexiondatabase;
		setSystem(adrip, profil, mdp);
		setDatabase(profil, mdp, drivernative);
	}

	/**
	 * Constructeur
	 * @param adrip
	 * @param profil
	 * @param mdp
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 * @param drivernative
	 * @param qaqqini
	 */
	public SystemManager(String adrip, String profil, String mdp, boolean connexiondatabase, boolean drivernative, int qaqqini)
	{
		connexionDatabase = connexiondatabase;
		setSystem(adrip, profil, mdp);
		setQaqqini(qaqqini);
		setDatabase(profil, mdp, drivernative);
	}

	/**
	 * Constructeur
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 */
	public SystemManager(boolean connexiondatabase)
	{
		this(connexiondatabase, true);
	}

	/**
	 * Constructeur
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 */
	public SystemManager(AS400 asystem, boolean connexiondatabase)
	{
		this( asystem, connexiondatabase, true);
	}

	/**
	 * Constructeur
	 * @param adrip
	 * @param profil
	 * @param mdp
	 * @param connexiondatabase, indique si l'on souhaite se connecter en m�me temps � la base
	 */
	public SystemManager(String adrip, String profil, String mdp, boolean connexiondatabase)
	{
		this(adrip, profil, mdp, connexiondatabase, true);
	}

	/**
	 * @param systeme the systeme to set
	 */
	public void setSystem()
	{
		this.systeme = new AS400();
	}
	
	/**
	 * @param systeme the systeme to set
	 */
	public void setSystem(AS400 systeme)
	{
		this.systeme = systeme;
	}

	/**
	 * @param systeme the systeme to set
	 */
	public void setSystem(String adrip, String profil, String mdp)
	{
		try
        {
            // On tente de se connecter � l'AS/400
			if ( adrip.trim().toUpperCase().equals( "*LOCAL" ) )
				adrip = "127.0.0.1";
            systeme = new AS400(adrip, profil, mdp);
            if (!systeme.validateSignon())
            {
            	systeme = null;
            	//System.out.println("authentification incorrecte");
            	msgErreur += "[SystemManager] (setSystem) Authentification incorrecte";
            }
            else
            {
            	msgErreur += "[SystemManager] (setSystem) Connexion au syst�me: " + systeme.toString();
            	//System.out.println("Connexion au systeme: " + systeme.toString());
            }
        }
        catch(Exception e)
        {
            systeme = null;
        }
		//System.out.println("Systeme cr��: " + systeme.isConnected() + systeme.toString());
	}
	
	/**
	 * Connexion � la base de donn�es
	 */
	public void setDatabase(String prf, String mdp, boolean drivernative)
	{
		if ((!connexionDatabase) || (systeme == null)) return;
		database = new DatabaseManager(drivernative);
		database.setQaqqini(qaqqini);
//System.out.println("-system-> " + systeme.getSystemName());
		if ((prf == null) || (prf.equals("")))
			database.connexion();
			//database.connexion(systeme.getSystemName());
		else
			database.connexion(systeme.getSystemName(), prf, mdp);
	}
	
	/**
	 * Initialise le Qaqqini (mode debug) � faire avant la connexion � la base 
	 * @param qaqqini
	 */
	public void setQaqqini(int qaqqini)
	{
		this.qaqqini = qaqqini;
	}
	
	/**
	 * Retourne si on est connect� au serveur ou pas
	 * @return
	 */
	public boolean isConnected()
	{
		return( systeme != null );
		/*if (systeme != null)
			return systeme.isConnected(); <- ne fonctionne pas
		else
			return false;
			*/
	}

	/**
	 * Retourne si on est connect� � la base de donn�es ou pas
	 * @return
	 */
	public boolean isDatabaseConnected()
	{
		if (database != null)
			return connexionDatabase;
		else
			return false;
	}

	/**
	 * @return the systeme
	 */
	public AS400 getSystem()
	{
		//if (systeme == null) setSystem();
		return systeme;
	}

	/**
	 * Retourne le pointeur vers la base de donn�es 
	 * @return the systeme
	 */
	public void setDatabaseManager(DatabaseManager database)
	{
		this.database = database;
	}

	/**
	 * Retourne le pointeur vers la base de donn�es 
	 * @return the systeme
	 */
	public DatabaseManager getdatabase()
	{
		return database;
	}

	/**
	 * D�connection du serveur
	 */
	public void disconnect()
	{
		// On d�connecte de la base de donn�es si besoin
		if ((database != null) && (connexionDatabase))
		{
			database.disconnect();
			connexionDatabase = false;
	   		//database = null; � cause du contexte il ne vaut mieux pas mettre � null	
		}
		
		// On se d�connecte du  serveur
   		if (systeme != null)
   			systeme.disconnectAllServices();
   		
		systeme = null;
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
