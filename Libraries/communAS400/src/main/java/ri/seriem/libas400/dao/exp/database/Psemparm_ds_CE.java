//=================================================================================================
//==>                                                                       04/02/2014 - 04/02/2014
//==> Description de l'enregistrement du fichier Psemparm_ds_CE pour les CE
//==> G�n�r� avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Psemparm_ds_CE extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "CELIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CECECH"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CEPDP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CENBJR"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "CELEJR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CEANC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "CENBMM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CELEFM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CELIMI"));

		length = 200;
	}
}
