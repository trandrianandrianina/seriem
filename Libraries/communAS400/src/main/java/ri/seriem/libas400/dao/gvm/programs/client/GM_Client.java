//=================================================================================================
//==>                                                                       09/09/2015 - 14/10/2015
//==> Gestion des actions commerciales
//=================================================================================================
package ri.seriem.libas400.dao.gvm.programs.client;

import java.util.ArrayList;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.SystemManager;

public class GM_Client extends BaseGroupDB
{
	// Variables
	private SystemManager			system					= null;
	private String					etablissement			= "";
	private M_Client				client					= new M_Client(querymg);


	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_Client(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	// -- M�thodes publiques --------------------------------------------------

	public void initialization(String etb)
	{
		// Initialisation des variables
		client.initialization();
		
		// Initialisation de l'�tablissement
		setEtablissement(etb);
	}
	

	/**
	 * Lecture d'un lot de client
	 * @param cond sans le "where"
	 * @return
	 */
	public M_Client[] readInDatabase(String cond)
	{
		String requete = "select * from "+ querymg.getLibrary()+".PGVMCLIM cli where TRIM(CLNOM) <> '' AND LENGTH(TRIM(CLNOM))>1"; 
		if( (cond != null) && (!cond.trim().equals("")  ) ){
			if( !cond.trim().toLowerCase().startsWith("and") )
				requete += " and " + cond;
			else
				requete += " " + cond;
		}
		return request4ReadClient(requete);
	}

	/**
	 * Supprime une liste de clients dans les tables
	 * @param lcl
	 * @return
	 *
	public boolean deleteInDatabase(ArrayList<M_Client> lcl)
	{
		if( lcl == null ){
			msgErreur += "\nLa liste de Client est nulle.";
			return false;
		}
		
		for( M_Client cl : lcl){
			if( !deleteInDatabase(cl) )
				return false;
		}

		return true;
	}*/

	/**
	 * Requ�te permettant la lecture d'un lot de clients
	 * @param requete
	 * @return
	 */
	public M_Client[] request4ReadClient(String requete)
	{
		if(( requete == null ) || (requete.trim().length() == 0)) {
			msgErreur += "\nLa requ�te n'est pas correcte.";
			return null;
		}
		if( querymg.getLibrary() == null ) {
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		// Lecture de la base afin de r�cup�rer les actions commerciales
		ArrayList<GenericRecord> listrcd = querymg.select(requete);
		if( listrcd == null ){
			return null;
		}
		// Chargement des classes avec les donn�es de la base
		int i = 0;
		M_Client[] listcl = new M_Client[listrcd.size()];
		for( GenericRecord rcd : listrcd ){
			M_Client cl = new M_Client(querymg);
			cl.initObject(rcd, true);
			cl.loadContacts();
			
			listcl[i++] = cl;
		}
		
		return listcl;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		system = null;
		
		if( client != null ){
			client.dispose();
		}
	}

	// -- M�thodes priv�es ----------------------------------------------------

	
	//-- Accesseurs -----------------------------------------------------------

	/**
	 * @return le etablissement
	 */
	public String getEtablissement()
	{
		return etablissement;
	}

	/**
	 * @param etablissement le etablissement � d�finir
	 */
	public void setEtablissement(String etablissement)
	{
		boolean etbIsLoaded = (etablissement == this.etablissement);
		if( etablissement == null )
			this.etablissement = "";
		else
			this.etablissement = etablissement;
		
		if( etbIsLoaded ){
			//loadListObjectAction();
			//actionCommerciale.setListObjectAction(listObjectAction);
		}
	}

	/**
	 * @return le system
	 */
	public SystemManager getSystem()
	{
		return system;
	}

	/**
	 * @param system le system � d�finir
	 */
	public void setSystem(SystemManager system)
	{
		this.system = system;
	}
	

	
}
