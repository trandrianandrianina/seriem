//=================================================================================================
//==>                                                                       10/06/2013 - 11/09/2013
//==> Algo de d�chiffrage de la s�curit� dans S�rie M (algo dans QRPGSRC/SGVXSE)
//==> Fonctionne sur les donn�es EBCDIC avant la conversion ASCII 
//=================================================================================================
package ri.seriem.libas400.security;

public class DeChiffreEbcdic
{
	// Cl� en d�cimal
	//private static final short[] wca={64,  76, 110, 241, 193, 216, 230, 242, 233, 226, 231, 243, 197, 196, 195, 244, 217, 198, 229, 245, 227, 199, 194, 246, 232, 200, 213, 247, 228, 209, 111, 248, 201, 210,  75, 248, 214, 211, 97, 240, 215, 212,  78,  74, 161, 108, 109,  92, 123,  80, 192, 127, 125,  77,  90, 208,  79, 224, 124,  93,  96,  95,  91, 106, 121, 107,  94, 122, 126,  64,  64,  64};
	// Cl� en h�xad�cimal
	private static final short[] wca={0x40, 0x4C, 0x6E, 0xF1, 0xC1, 0xD8, 0xE6, 0xF2, 0xE9, 0xE2, 0xE7, 0xF3, 0xC5, 0xC4, 0xC3, 0xF4, 0xD9, 0xC6, 0xE5, 0xF5, 0xE3, 0xC7, 0xC2, 0xF6, 0xE8, 0xC8, 0xD5, 0xF7, 0xE4, 0xD1, 0x6F, 0xF8, 0xC9, 0xD2, 0x4B, 0xF9, 0xD6, 0xD3, 0x61, 0xF0, 0xD7, 0xD4, 0x4E, 0x4A, 0xA1, 0x6C, 0x6D, 0x5C, 0x7B, 0x50, 0xC0, 0x7F, 0x7D, 0x4D, 0x5A, 0xD0, 0x4F, 0xE0, 0x7C, 0x5D, 0x60, 0x5F, 0x5B, 0x6A, 0x79, 0x6B, 0x5E, 0x7A, 0x7E, 0x40, 0x40, 0x40};
	

	/**
	 * Algo de d�chiffrage de la s�curit� S�rie M/N
	 * @param array1
	 * @param array2
	 * @return
	 *
	public byte[] getDecryptSecurity(byte[] array1, byte[] array2)
	{
		byte[] wc = new byte[array1.length + array2.length];
		System.arraycopy(array1, 0, wc, 0, array1.length);
		System.arraycopy(array2, 0, wc, array1.length, array2.length);
		boolean l1, l2, in01;
		int xs;
		for (int xt=0; xt<wc.length; xt++)
		{
			in01 = false;
			l1 = (((xt+1) % 2) == 0);
			l2 = (((xt+1) % 3) == 0);
			// Recherche le caract�re dans la cl�
			for (xs=0; xs<wca.length; xs++)
				if (wc[xt] == wca[xs])
				{
					in01 = true;
					break;
				}
			if (in01)
			{
				xs++;
				xs = 72 - xs;
				if (l1) xs--;
				//if ((l2) || (xs < 0)) xs++;
				if (l2) xs++;
				wc[xt] = (byte)wca[xs-1];
			}
		}

		return wc;
	}*/

	/**
	 * Algo de d�chiffrage de la s�curit� S�rie M/N
	 * @param array1
	 * @param array2
	 * @return
	 */
	public byte[] getDecryptSecurity(byte[] array1)
	{
		byte[] wc = new byte[array1.length];
		System.arraycopy(array1, 0, wc, 0, array1.length);
		boolean l1, l2, in01;
		int xs;
		for (int xt=0; xt<wc.length; xt++)
		{
			in01 = false;
			l1 = (((xt+1) % 2) == 0);
			l2 = (((xt+1) % 3) == 0);
			// Recherche le caract�re dans la cl�
			for (xs=0; xs<wca.length; xs++)
				if ((wc[xt] & 0xff) == (wca[xs] & 0xff))
				{
					in01 = true;
					break;
				}
			if (in01)
			{
				xs++;
				xs = 72 - xs;
				if (l1) xs--;
				//if ((l2) || (xs < 0)) xs++;
				if (l2) xs++;
				wc[xt] = (byte)(wca[xs-1] & 0xff);
			}
		}

		return wc;
	}

}
