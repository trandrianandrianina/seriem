//=================================================================================================
//==>                                                                       09/09/2015 - 14/10/2015
//==> Traitement pour UNE action commerciale 
//=================================================================================================

package ri.seriem.libas400.dao.gvm.programs.actioncommerciale;

import ri.seriem.libas400.dao.exp.programs.contact.M_Contact;
import ri.seriem.libas400.dao.exp.programs.contact.M_EvenementContact;
import ri.seriem.libas400.dao.exp.programs.contact.M_LienEvenementContact;
import ri.seriem.libas400.dao.gvm.database.files.FFD_Pgvmaccm;
import ri.seriem.libas400.dao.gvm.programs.client.M_Client;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * Classe contenant les zones du PGVMACCM
 * @author Administrateur
 *
 */
public class M_ActionCommerciale extends FFD_Pgvmaccm
{
	// Variables
	private	M_EvenementContact					evenementContact 		= new M_EvenementContact(querymg);
	private M_Client							client					= new M_Client(querymg);
	private ArrayList<M_LienEvenementContact>	lienEvenementContact	= new ArrayList<M_LienEvenementContact>();
	private LinkedHashMap<String, String>		listObjectAction 		= null;

	private ArrayList<M_Contact>				listContact				= new ArrayList<M_Contact>();
	private String[]							listLabelObjectAction	= null;
	private String[]							listCodeObjectAction	= null;


	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_ActionCommerciale(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Initialise les variables avec les valeurs par d�faut
	 */
	@Override
	public void initialization()
	{
		evenementContact.initialization();
		lienEvenementContact.clear();
		client.initialization();

		super.initialization();
	}

	/**
	 * Initialise les donn�es du record avec cet objet
	 * @param rcd
	 * @return
	 */
	public boolean initGenericRecord(GenericRecord rcd, boolean clearBefore)
	{
		evenementContact.initGenericRecord(rcd, false);
		return super.initGenericRecord(rcd, clearBefore);
	}

	/**
	 * Initialise l'objet avec les donn�es du record
	 * @param rcd
	 * @return
	 */
	public boolean initObject(GenericRecord rcd, boolean doinit)
	{
		evenementContact.initObject(rcd, true);
		//lienEvenementContact.initObject(rcd);
		return super.initObject(rcd, false);
	}

	/**
	 * Initialise les donn�es du record avec cet objet
	 * @param rcd
	 * @return
	 *
	public boolean initGenericRecord(GenericRecord rcd, boolean clearBefore)
	{
		if( rcd == null ) {
			return false;
		}
		
		evenementContact.initGenericRecord(rcd, false);
		//lienEvenementContact.initGenericRecord(rcd, false);

		rcd.setOmittedField(omittedField);
		return rcd.fromObject(this, clearBefore);
	}*/

	/**
	 * Initialise l'objet avec les donn�es du record
	 * @param rcd
	 * @return
	 *
	public boolean initObject(GenericRecord rcd)
	{
		if( rcd == null ) {
			return false;
		}
		initialization();
		evenementContact.initObject(rcd);
		//lienEvenementContact.initObject(rcd);

		return rcd.toObject(this);
	}*/


	/**
	 * Ins�re une action commerciale dans le table
	 * @return
	 */
	public boolean insertInDatabase()
	{
		int id = -1;
		
		// Insertion dans le PSEMEVTM
		if( evenementContact.insertInDatabase() ) {
			id = evenementContact.getETID();
		} else {
			msgErreur += '\n' + querymg.getMsgError();
			return false;
		}

		// Insertion dans le PGVMACCM
		setACID(id);
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PGVMACCM", querymg.getLibrary());
		int ret = querymg.requete(requete);
		if( ret == -1 ){
			msgErreur += '\n' + querymg.getMsgError();
			return false;
		}
		
		// Insertion dans le PSEMLECM
		if( (listContact != null) && (listContact.size() == 0) ) {
			msgErreur += "\nLa liste des contacts est vide.";
			return false;
		}
		lienEvenementContact.clear();
		for(M_Contact contact: listContact){
			M_LienEvenementContact lec = new M_LienEvenementContact(querymg);
			lienEvenementContact.add(lec);
			lec.setECIDC(contact.getRENUM());
			lec.setECETBC(contact.getREETB());
			lec.setECIDE(id);
			lec.setECTYP(contact.getCodeTypeContact());
			lec.setECHIE(contact.getCodeHierarchy());
			if( !lec.insertInDatabase() ) {
				msgErreur += '\n' + querymg.getMsgError();				
				return false; // A voir car pas vraiment judicieux
			}
		}
		
		return true;
	}

	/**
	 * Modifie une action commerciale dans le table
	 * @return
	 */
	public boolean updateInDatabase()
	{
		// Modification dans le PSEMEVTM
		if( !evenementContact.updateInDatabase() ) {
			msgErreur += '\n' + evenementContact.getMsgError();
			return false;
		}

		// Modification dans le PGVMACCM
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestUpdate("PGVMACCM", querymg.getLibrary(), "ACID = " + getACID());
		int ret = querymg.requete(requete);
		if( ret == -1 ){
			msgErreur += '\n' + querymg.getMsgError();
			return false;
		}
		
		// Modification dans le PSEMLECM
		analyzeListContacts();
		for( M_LienEvenementContact lec : lienEvenementContact ){
			switch( lec.getActionInDatabase() ){
				case M_LienEvenementContact.ACTION_INSERT:
System.out.println("-modifyInDatabase->Insert");					
					lec.insertInDatabase();
					break;
				case M_LienEvenementContact.ACTION_UPDATE:
System.out.println("-modifyInDatabase->Update");					
					lec.updateInDatabase();
					break;
				case M_LienEvenementContact.ACTION_DELETE:
System.out.println("-modifyInDatabase->Delete");					
					lec.deleteInDatabase(false);
					break;
			}
		}
		
		return true;
	}

	/**
	 * Suppression de l'enregistrement courant
	 * @return
	 */
	public boolean deleteInDatabase()
	{
		// Nettoyage dans la table des liens contacts
		if( lienEvenementContact.size() >= 1 )
			lienEvenementContact.get(0).deleteInDatabase(true);
		lienEvenementContact.clear();
		
		// Suppression dans le PSEMEVTM
		if( !evenementContact.deleteInDatabase() ) {
			return false;
		}

		// Suppression dans le PGVMACCM
		String requete = "delete from " + querymg.getLibrary() + ".PGVMACCM where ACID=" + getACID();
		boolean ret = request(requete);
		
		//listObjectAction.clear(); // Variable commune donc non
		listContact.clear();
		
		return ret;
		
	}
	
	/**
	 * R�cup�ration des contacts pour une action commerciale
	 * @param id
	 * @return
	 */
	public boolean loadContacts(QueryManager querymg)
	{
		// Lecture de la base afin de r�cup�rer les contacts li�es
		String requete = "select * from "+ querymg.getLibrary()+".psemrtem rte, "+ querymg.getLibrary()+".psemlecm lec where lec.ecide = "+ getACID() +" and rte.renum = lec.ecidc and rte.reetb = lec.ecetbc";
//System.out.println(requete);		
		ArrayList<GenericRecord> listrcd = querymg.select(requete);
		if( listrcd == null ){
			return false;
		}
		// Chargement des classes avec les donn�es de la base
		if( listContact != null)
			listContact.clear();
		else
			listContact = new ArrayList<M_Contact>(listrcd.size());
		lienEvenementContact.clear();
		for( GenericRecord rcd : listrcd ){
			M_Contact contact = new M_Contact(querymg);
			contact.initObject(rcd, true);
			M_LienEvenementContact lec = new M_LienEvenementContact(querymg);
			lec.initObject(rcd, true);
			contact.setCodeHierarchy(lec.getECHIE());
			contact.setCodeTypeContact(lec.getECTYP());
			listContact.add(contact);
			lienEvenementContact.add(lec);
		}
		
		return true;
	}

	/**
	 * Retourne la liste des Contacts d'un type donn�
	 * @param filtre
	 * @return
	 */
	public M_Contact[] getFiltreContacts(String filtre)
	{
		ArrayList<M_Contact> list = new ArrayList<M_Contact>();
		if( (listContact == null) || (filtre == null) ) return new M_Contact[0];
		
		for( M_Contact contact : listContact ){
			if( contact.getCodeTypeContact().equals( filtre ) ){
				list.add(contact);
			}
		}
		M_Contact[] tab = new M_Contact[list.size()];
		tab = list.toArray(tab);
		list.clear();
		return tab;
		//return (Contact[]) list.toArray(tab);
	}

	/**
	 * R�cup�ration des informations du client (Penser � voir comment g�rer la zone pour le suffixe ETSUFT)
	 * @param querymg
	 * @return
	 */
	public boolean loadClient(QueryManager querymg)
	{
		// Lecture de la base afin de r�cup�rer le client
		String requete = "select * from "+ querymg.getLibrary()+".pgvmclim where clcli = "+ evenementContact.getETIDT() +" and clliv = "+ evenementContact.getETSUFT() + " and cletb = '" + evenementContact.getETETBT() + "'";
//System.out.println(requete);
		ArrayList<GenericRecord> listrcd = querymg.select(requete);
		if( (listrcd == null) || (listrcd.size() < 1) ){
			return false;
		}
		// Chargement des classes avec les donn�es de la base
		boolean ret = client.initObject(listrcd.get(0), true);
		if( ret )
			ret = client.loadContacts();
		
		return ret;
	}


	// -- M�thodes priv�es ----------------------------------------------------
	
	private boolean analyzeListContacts()
	{
		if( (listContact == null) || (listContact.size() == 0) ){
			msgErreur += "\nLa liste des contacts est vide.";
			return false;
		}

//System.out.println("-analyzeListContacts->listContact			:" + listContact.size());
//System.out.println("-analyzeListContacts->lienEvenementContact	:" + lienEvenementContact.size());
		// Initialisation du tableau contenant les actions � faire sur la table des liens
		for( M_Contact contact : listContact) {
			boolean found = false;
			for( M_LienEvenementContact lec : lienEvenementContact ) {
//System.out.println("-analyzeListContacts->" + contact.getRENUM() + " == " + lec.getECIDC() + " && |" + contact.getREETB() + "| == |" + lec.getECETBC() + "| && |" + contact.getCodeTypeContact() + "| == |" + lec.getECTYP() + '|');
				if( (contact.getRENUM() == lec.getECIDC()) && contact.getREETB().equals(lec.getECETBC()) && contact.getCodeTypeContact().equals(lec.getECTYP())  ) {
					found = true;
					lec.setActionInDatabase(M_LienEvenementContact.ACTION_UPDATE);
				}
			}
//System.out.println("-analyzeListContacts->found:" + found);					
			// Il n'est pas dans la liste alors on l'ins�re
			if( !found ){
				M_LienEvenementContact nlec = new M_LienEvenementContact(querymg);
				nlec.setECIDC(contact.getRENUM());
				nlec.setECETBC(contact.getREETB());
				nlec.setECIDE(getACID());
				nlec.setECHIE(contact.getCodeHierarchy());
				nlec.setECTYP(contact.getCodeTypeContact());
				nlec.setActionInDatabase(M_LienEvenementContact.ACTION_INSERT);
				lienEvenementContact.add(nlec);
			}
		}
		// On marque les liens que l'on doit supprimer (ceux qui sont en NOTHING pour l'instant)
		for( M_LienEvenementContact lec : lienEvenementContact ) {
			if( lec.getActionInDatabase() == M_LienEvenementContact.ACTION_NOTHING ) {
				lec.setActionInDatabase(M_LienEvenementContact.ACTION_DELETE);
			}
		}

		return true;
	}
	
	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();
		
		if( evenementContact != null ){
			evenementContact.dispose();
		}
		if( client != null ){
			client.dispose();
		}
		if( lienEvenementContact != null ){
			for( M_LienEvenementContact lec : lienEvenementContact )
				lec.dispose();
			lienEvenementContact.clear();
		}
		//if( listObjectAction != null){
		//	listObjectAction.clear();
		//}
		listObjectAction = null;
		if( listContact != null ){
			for( M_Contact c : listContact )
				c.dispose();
			listContact.clear();
		}
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le evenementContact
	 */
	public M_EvenementContact getEvenementContact()
	{
		return evenementContact;
	}

	/**
	 * @param evenementContact le evenementContact � d�finir
	 */
	public void setEvenementContact(M_EvenementContact evenementContact)
	{
		this.evenementContact = evenementContact;
	}

	/**
	 * @return le lienEvenementContact
	 */
	public ArrayList<M_LienEvenementContact> getLienEvenementContact()
	{
		return lienEvenementContact;
	}

	/**
	 * @param lienEvenementContact le lienEvenementContact � d�finir
	 */
	public void setLienEvenementContact(ArrayList<M_LienEvenementContact> lienEvenementContact)
	{
		this.lienEvenementContact = lienEvenementContact;
	}


	/**
	 * @return le listObjectAction
	 */
	public HashMap<String, String> getListObjectAction()
	{
		return listObjectAction;
	}

	/**
	 * @param listObjectAction le listObjectAction � d�finir
	 */
	public void setListObjectAction(LinkedHashMap<String, String> listObjectAction)
	{
		this.listObjectAction = listObjectAction;
		
		// Chargement de la table des libell�s
		listLabelObjectAction = new String[listObjectAction.size()];
		listCodeObjectAction = new String[listObjectAction.size()];
		int i=0;
		for(Entry<String, String> entry : listObjectAction.entrySet()) {
			listCodeObjectAction[i] = entry.getKey().trim();
		    listLabelObjectAction[i++] = entry.getValue().trim();
		}
	}

	/**
	 * Retourne le libell� de l'objet
	 * @return
	 */
	public String getLabelACOBJ()
	{
		String code = getACOBJ();
//System.out.println("-M_ActionCommerciale->code:" + code);		
		if( code == null ) return "";
//System.out.println("-M_ActionCommerciale->listLabel:" + listLabelObjectAction.length);		
		for(int i=0; i< listLabelObjectAction.length; i++ ){
//System.out.println("-M_ActionCommerciale->" + listCodeObjectAction[i] + " " + code);
			if( listCodeObjectAction[i].equals( code ))
				return listLabelObjectAction[i];
		}
		return code;
	}

	/**
	 * @return le listContact
	 */
	public ArrayList<M_Contact> getListContact()
	{
		return listContact;
	}

	/**
	 * @param listContact le listContact � d�finir
	 */
	public void setListContact(ArrayList<M_Contact> listContact)
	{
		this.listContact = listContact;
	}

	/**
	 * @return le client
	 */
	public M_Client getClient()
	{
		return client;
	}

	/**
	 * @param client le client � d�finir
	 */
	public void setClient(M_Client client)
	{
		this.client = client;
	}

	/**
	 * @return le listLabelObjectAction
	 */
	public String[] getListLabelObjectAction()
	{
		return listLabelObjectAction;
	}

	/**
	 * @return le listCodeObjectAction
	 */
	public String[] getListCodeObjectAction()
	{
		return listCodeObjectAction;
	}

}
