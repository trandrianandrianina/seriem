//=================================================================================================
//==>                                                                       02/12/2011 - 05/12/2011
//==> Gestion des informations contenues dans la *LDA
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.LocalDataArea;

public class LdaManager
{
	// Variables
	private SystemManager systeme=null;
	private LocalDataArea lda=null;
	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 

	/**
	 * Constructeur
	 * @param systeme
	 */
	public LdaManager(SystemManager psysteme)
	{
		setSystem(psysteme);
		lda = new LocalDataArea(systeme.getSystem());
	}
	
	/**
	 * Lit et retourne le contenu de la LDA
	 * @return
	 */
	public String getData()
	{
		String data=null;
		
		try
		{
			data = lda.read();
		}
		catch (Exception e)
		{
			msgErreur = "[LDAOperation] (getData) Erreur : " + e;
		}
		
		return data;
	}

	/**
	 * Ecrit le contenu de data dans la LDA
	 * @param data
	 * @return
	 */
	public boolean setData(String data)
	{
		if (data == null) return false;
		try
		{
			if (data.length() > lda.getLength() ) return false;
			lda.write(data);
		}
		catch (Exception e)
		{
			msgErreur = "[LDAOperation] (setData) Erreur : " + e;
			return false;
		}
		
		return true;
	}
	
	/**
	 * Initialise la variable syst�me
	 * @param systeme the systeme to set
	 */
	public void setSystem(SystemManager psysteme)
	{
		if (psysteme == null)
			msgErreur += "\n[LDAOperation] (setSystem) Erreur : le param�tre psysteme est null";
		else
			this.systeme = psysteme;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
