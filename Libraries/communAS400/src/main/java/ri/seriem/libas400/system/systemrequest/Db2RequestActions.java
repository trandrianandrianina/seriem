package ri.seriem.libas400.system.systemrequest;

import java.util.ArrayList;

import com.ibm.as400.access.AS400;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.libcommun.protocoleMsg.Db2Request;

public class Db2RequestActions
{
	private AS400 sysAS400=null;
	private Db2Request rdb2=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public Db2RequestActions(AS400 system, Db2Request ldb)
	{
		sysAS400 = system;
		rdb2 = ldb;
	}

	// --> M�thodes publiques <------------------------------------------------
	
	/**
	 * Traitement des actions possibles 
	 */
	public void actions()
	{
		//int theactions = actions;
		for (int i=Db2Request.ACTIONS.size(); --i>0;)
		{
			if (rdb2.getActions() >= Db2Request.ACTIONS.get(i))
			{
				switch(Db2Request.ACTIONS.get(i))
				{
					case Db2Request.EXECUTE:
						executeRequest();
						break;
				}
				rdb2.setActions(rdb2.getActions() - Db2Request.ACTIONS.get(i));
			}
		}
	}

	// --> M�thodes priv�es <--------------------------------------------------

	/**
	 * Execute une requ�te sur la base DB2
	 * A affiner si select sans resultat mais pas en erreur alors faudrait au moins les titres de colonnes  
	 * @return
	 */
	private void executeRequest()
	{
		SystemManager manager = new SystemManager(sysAS400, true);
		QueryManager query = new QueryManager(manager.getdatabase().getConnection());

		if (rdb2.isSelect())
		{
			ArrayList<GenericRecord> liste = query.select(rdb2.getRequest());
//System.out.println("-Db2RequestActions-> Nbr d'enregistrement: " + liste==null?"null":liste.size());
			if (liste == null)
				rdb2.setSuccess(false);
			else
				if (liste.isEmpty())
				{
					rdb2.setSuccess(true);
					rdb2.setTData(null);
				}
			else
			{
				rdb2.setSuccess(true);
				rdb2.setTTitle(liste.get(0).getArrayTitle());
				Object[][] data = new Object[liste.size()][];
				/*
				for (int i=0; i<liste.size(); i++)
				{
					data[i] = liste.get(i).getArrayValue(true);
				}
				liste.clear();
				*/
				int i=0;
				while( liste.size() > 0 ){
					data[i++] = liste.get(0).getArrayValue(true);
					liste.remove(0);
				}
//System.out.println("--> Fin conv dans Data");				
				rdb2.setTData(data);
//System.out.println("--> Fin conv en msg");				
			}
		}
		else
			if (query.requete(rdb2.getRequest()) == -1)
				rdb2.setSuccess(false);
			else
				rdb2.setSuccess(true);
			
			rdb2.setMsgError(query.getMsgError());
			query.dispose();
	}

}
