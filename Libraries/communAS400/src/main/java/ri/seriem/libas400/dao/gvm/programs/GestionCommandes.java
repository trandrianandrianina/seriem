//=================================================================================================
//==>                                                                       26/05/2014 - 18/06/2014
//==> Permet la r�cup�ration d'un tarif pour un article  
//==> A faire:
//==> Exemple: voir test11
//=================================================================================================
package ri.seriem.libas400.dao.gvm.programs;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.seriem.libas400.database.field.Field;
import ri.seriem.libas400.database.field.FieldAlpha;
import ri.seriem.libas400.database.field.FieldDecimal;
import ri.seriem.libas400.system.CallProgram;
import ri.seriem.libas400.system.SystemManager;

public class GestionCommandes
{
	// Constantes
	private static final int LG_DATASTRUCT=64;
	
	private static final int LG_TYPEBON=1;
	private static final int LG_ETB=3;
	private static final int LG_NUMBON=6;
	private static final int LG_SUF=1;
	
	
	// Variables sp�cifiques du programme RPG
	private char[] indicateurs={'0', '0', '0', '0', '0', '0','0', '0', ' ', ' '};
	private FieldAlpha typeBon = new FieldAlpha(LG_TYPEBON);
	private FieldAlpha etablissement = new FieldAlpha(LG_ETB);
	private FieldDecimal numeroBon=new FieldDecimal(LG_NUMBON, 0);
	private FieldDecimal suffixeBon=new FieldDecimal(new BigDecimal(0), LG_SUF, 0);
	
	// Variables
	private boolean init=false;
	private SystemManager system = null;
	private ProgramParameter[] parameterList = new ProgramParameter[1];
	private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
	private char lettre = 'W';
	private String curlib = null;
	private CallProgram rpg = null;
	private StringBuilder sb = new StringBuilder();

	/**
	 * Constructeur
	 * @param sys
	 * @param let
	 * @param curlib
	 */
	public GestionCommandes(SystemManager sys, char let, String curlib)
	{
		system = sys;
		setLettre(let);
		setCurlib(curlib);
	}
	
	/**
	 * Initialise l'environnment d'execution du programme RPG
	 * @return
	 */
	public boolean init()
	{
		if (init) return true;
		
		parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
        //rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/"+lettre+"GVMAS.LIB/SGVMCCA1.PGM", null);
        rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/"+lettre+"GVMAS.LIB/BONDL0.PGM", null);
        // System.out.println("crea RPG 1" + rpg.getCodeErreur() + " -> " +  rpg.getMsgError());
        init = rpg.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('"+lettre+"')");
        // System.out.println("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('"+lettre+"') -> " + init );
        if (!init)
        {
        	System.out.println("Erreur CHGDTAARA: " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
        	return false;
        }
        
        rpg.addLibrary(curlib, true);
        rpg.addLibrary("M_GPL", false);
        rpg.addLibrary(lettre + "EXPAS", false);
        rpg.addLibrary(lettre + "GVMAS", false);
        init = rpg.addLibrary(lettre + "GVMX", false);
        
        return init;
	}

	/**
	 * Effectue la recherche du tarif 
	 * @param valEtb
	 * @param valNumCli
	 * @param valLivCli
	 * @param valCodeArt
	 * @param valQte
	 * @return
	 */
	public boolean execute(String valEtb, BigDecimal valNumBon, BigDecimal valSufBon)
	{
		setValueTypeBon("E");
		setValueEtablissement(valEtb);
		setValueNumeroBon(valNumBon);
		setValueSuffixeBon(valSufBon);
		
		return execute();
	}
	
	/**
	 * Effectue la recherche du tarif 
	 * @return
	 */
	private boolean execute()
	{
		if (rpg == null) return false;
		
		// Construction du param�tre AS400 PPAR1
		sb.setLength(0);
		sb.append(getIndicateurs())
		  .append(typeBon.toFormattedString())
		  .append(etablissement.toFormattedString())
		  .append(numeroBon.toFormattedString())
		  .append(suffixeBon.toFormattedString());
		try
		{
			System.out.println("CONCAT PARAMETRES: " + sb.toString());
			parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
			rpg.setParameterList(parameterList);
			// Appel du programme RPG
			if (rpg.execute())
			{
				//System.out.println("[GestionCommande] Ok" + rpg.getCodeErreur() + " " + rpg.getMsgError());
				return true;
			}
			else System.out.println("[GestionCommande]probleme de rpg.execute(): "+ rpg.getCodeErreur() + " -> " + rpg.getMsgError());
		}
		catch (PropertyVetoException e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	// -- M�thodes priv�es ----------------------------------------------------
	
	/**
	 * Formate une variable de type String (avec cadrage � droite, notamment pour l'�tablissement)
	 * @param valeur
	 * @param lg
	 * @return
	 */
	private String formateVar(String valeur, int lg)
	{
		if (valeur == null)
			return String.format("%"+lg+"."+lg+"s", "");
		else
			return String.format("%"+lg+"."+lg+"s", valeur);
	}

	
	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le lettre
	 */
	public char getLettre()
	{
		return lettre;
	}


	/**
	 * @param lettre le lettre � d�finir
	 */
	public void setLettre(char lettre)
	{
		if (lettre != ' ')
			this.lettre = lettre;
	}


	/**
	 * @return le curlib
	 */
	public String getCurlib()
	{
		return curlib;
	}


	/**
	 * @param curlib le curlib � d�finir
	 */
	public void setCurlib(String curlib)
	{
		this.curlib = curlib;
	}


	/**
	 * @return le indicateurs
	 */
	public char[] getIndicateurs()
	{
		return indicateurs;
	}
	
	/**
	 * @param indicateurs le indicateurs � d�finir
	 */
	public void setIndicateurs(char[] indicateurs)
	{
		this.indicateurs = indicateurs;
	}

	public FieldAlpha getTypeBon()
	{
		return typeBon;
	}

	public void setValueTypeBon(String typeBon)
	{
		this.typeBon.setValue(typeBon);
	}
	
	/**
	 * Retourne le champ Etablissement
	 * @return le etablissement
	 */
	public Field<String> getEtablissement()
	{
		return etablissement;
	}
	
	/**
	 * Initialise la valeur de l'�tablissement
	 * @param etablissement le etablissement � d�finir
	 */
	public void setValueEtablissement(String etablissement)
	{
		this.etablissement.setValue(formateVar(etablissement, LG_ETB));
	}


	public FieldDecimal getNumeroBon()
	{
		return numeroBon;
	}

	public void setValueNumeroBon(BigDecimal numeroBon)
	{
		this.numeroBon.setValue(numeroBon);
	}

	public FieldDecimal getSuffixeBon()
	{
		return suffixeBon;
	}

	public void setValueSuffixeBon(BigDecimal suffixeBon)
	{
		this.suffixeBon.setValue(suffixeBon);
	}
	
	
	
}
