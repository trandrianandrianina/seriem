//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_RG pour les RG
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_RG extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "RGLIB"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "RGAST"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "RGTYP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "RGCRB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "RGSNS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "RGACC"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "RGNBT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(60), "RGTXT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "RGCJO"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "RGTRG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "RGAFA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "RGEDT"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "RGMTM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "RGRGR"));

		length = 300;
	}
}
