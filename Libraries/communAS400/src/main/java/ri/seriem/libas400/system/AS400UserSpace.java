//=================================================================================================
//==>                                                                       17/03/2016 - 23/03/2016
//==> Gestion des UserSpace
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.UserSpace;

public class AS400UserSpace extends UserSpace
{
	// Constantes
	private	static final		long						serialVersionUID		= -8407482610975270107L;
	public	static final		int							BLOCK_SIZE				= 4096;
	
	// Variables
	private						AS400						system					= null;
    private						String						msgErreur				= null;
    private						String						library					= null;
    private						String						namespc					= null;
	

    /**
     * Constructeur
     * @param asystem
     * @param lib
     * @param aname
     */
	public AS400UserSpace(AS400 asystem, String lib, String aname)
	{
		super();
		system = asystem;
		setLibrary(lib);
		setNamespc(aname);
		QSYSObjectPathName sysobj = new QSYSObjectPathName(library, namespc, "USRSPC");
		try
		{
			setSystem(system);
			setPath(sysobj.getPath());
			//setMustUseNativeMethods(Constantes.isOs400()); // Comment� car plante si true (� voir plus tard)
		}
		catch (Exception e)
		{
			e.printStackTrace();
			msgErreur += "\n" + e.getMessage();
		}
	}


	/**
	 * Cr�ation d'un user space
	 * @param size
	 * @param replace
	 * @return
	 */
	public boolean create(int size, boolean replace)
	{
		if( library == null ){
			msgErreur += "\nLa biblioth�que est � null.";
			return false;
		}
		if( namespc == null ){
			msgErreur += "\nLe nom du userspace est � null.";
			return false;
		}
		if( system == null ){
			msgErreur += "\nLa variable system n'est pas initialis�e.";
			return false;
		}

		try
		{
			create(size, replace, " ", (byte)0x00, "UserSpace d'�change asynchrone", "*USE");
			return true;
		}
		catch (Exception e)
		{
			msgErreur += "\n" + e.getMessage();
		}
		
		return false;
	}

	/**
	 * Lecture d'un bloc
	 * @param offset
	 * @return
	 */
	public String readBlock(int offset)
	{
		try
		{
			return read(offset, BLOCK_SIZE);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			msgErreur += "\n" + e.getMessage();
		}

		return null;
	}
	
	/**
	 * Ecriture d'un bloc
	 * @param offset
	 * @param data
	 * @return
	 */
	public boolean writeBlock(int offset, String data)
	{
		try
		{
			write(data, offset);
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			msgErreur += "\n" + e.getMessage();
		}

		return false;
	}
	
	/**
	 * Redimensionne le userspace sans alt�rer le contenu
	 * @param newsize
	 * @return
	 */
	public boolean resize(int newsize)
	{
		try
		{
			setLength(newsize);
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			msgErreur += "\n" + e.getMessage();
		}

		return false;
	}
	
    /**
	 * @return le library
	 */
	public String getLibrary()
	{
		return library;
	}


	/**
	 * @param library le library � d�finir
	 */
	public void setLibrary(String library)
	{
		this.library = library;
	}


	/**
	 * @return le namespc
	 */
	public String getNamespc()
	{
		return namespc;
	}


	/**
	 * @param namespc le namespc � d�finir
	 */
	public void setNamespc(String namespc)
	{
		this.namespc = namespc;
	}


	/**
     * Retourne le message d'erreur
     * @return
     */
    public String getMsgErreur()
    {
        // La r�cup�ration du message est � usage unique
        String chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }

}
