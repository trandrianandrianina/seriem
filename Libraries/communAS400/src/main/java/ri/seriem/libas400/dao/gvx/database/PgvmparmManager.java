//=================================================================================================
//==>                                                                       21/05/2013 - 06/09/2013
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//==> ATTENTION de ne pas oublier d'initialiser la FM via le setLibrary
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.DataStructureRecord;
import ri.seriem.libas400.database.record.ExtendRecord;
import ri.seriem.libas400.parameters.Etablissement;
import ri.seriem.libas400.parameters.Magasin;
import ri.seriem.libas400.parameters.Societe;

import com.ibm.as400.access.Record;


public class PgvmparmManager extends QueryManager
{
	/**
	 * Constructeur
	 * @param database
	 */
	public PgvmparmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Lecture de la DG blanche
	 * @param infos
	 * @return
	 */
	public boolean getInfosSociete(Societe asociete)
	{
		if (asociete == null) return false;
		
		String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_DG";
		final ArrayList<Record> listeRecord = select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "+getLibrary()+".PGVMPARM where PARETB = '' and PARTYP = 'DG' and PARIND = ''", (DataStructureRecord) newObject(ds));

		// On alimente les infos si on trouve l'enregistrement
//System.out.println("Societe " + listeRecord.size());
		if (listeRecord.size() == 1)
		{
			ExtendRecord record = new ExtendRecord();
			record.setRecord(listeRecord.get(0));
			asociete.setRaisonSociale((String)record.getField("DGNOM"));
			asociete.setComplementNom((String)record.getField("DGCPL"));
			asociete.setCodeEtablissementPilote((String)record.getField("DGETP"));
//System.out.println("ETB 1 = " + record.getField("DGETP") + " " + asociete.getCodeEtablissementPilote());
			return true;
		}

		return false;
	}

	/**
	 * Lecture de la DG pour un etablissement
	 * @param aetablissement
	 * @return
	 */
	public boolean getInfosEtablissement(Etablissement aetablissement)
	{
		if (aetablissement == null) return false;
		
		String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_DG";
//System.out.println("ETB 2 = " + aetablissement.getCodeEtablissement());
		final ArrayList<Record> listeRecord = select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "+getLibrary()+".PGVMPARM where PARETB = '"+aetablissement.getCodeEtablissement()+"' and PARTYP = 'DG'", (DataStructureRecord) newObject(ds));

		// On alimente les infos si on trouve l'enregistrement
		if (listeRecord.size() == 1)
		{
			ExtendRecord record = new ExtendRecord();
			record.setRecord(listeRecord.get(0));
			aetablissement.setNom((String)record.getField("DGNOM"));
			aetablissement.setComplementNom((String)record.getField("DGCPL"));
//System.out.println("MAG = " + record.getField("DGETP"));
			aetablissement.setMagasinGeneral((String)record.getField("DGETP"));
			lectureDesPS(aetablissement);

			return true;
		}

		return false;
	}

	/**
	 * Lecture des PS pour un etablissement
	 * @param aetablissement
	 * @return
	 */
	public boolean lectureDesPS(Etablissement aetablissement)
	{
		if (aetablissement == null) return false;
		
		String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_PS";
		final ArrayList<Record> listeRecord = select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "+getLibrary()+".PGVMPARM where PARETB = '"+aetablissement.getCodeEtablissement()+"' and PARTYP = 'PS' and PARIND = ''", (DataStructureRecord) newObject(ds));

		// On alimente les infos si on trouve l'enregistrement
		if (listeRecord.size() == 1)
		{
			ExtendRecord record = new ExtendRecord();
			record.setRecord(listeRecord.get(0));
			aetablissement.getPs().setTableau((String)record.getField("PS"));
			
			return true;
		}

		return false;
	}

	/**
	 * Lecture des infos pour un magasin
	 * @param etb
	 * @param codemag
	 * @return
	 */
	public boolean getInfosMagasin(String etb, Magasin amagasin)
	{
		if (amagasin == null) return false;
		
		String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_MA";
		final ArrayList<Record> listeRecord = select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "+getLibrary()+".PGVMPARM where PARETB = '"+etb+"' and PARTYP = 'MA' and PARIND = '" + amagasin.getCodeMag()+"'", (DataStructureRecord) newObject(ds));

		// On alimente les infos si on trouve l'enregistrement
		if (listeRecord.size() == 1)
		{
			ExtendRecord record = new ExtendRecord();
			record.setRecord(listeRecord.get(0));
			amagasin.setMALIB((String)record.getField("MALIB"));
			
			return true;
		}

		return false;
	}
	
	/**
	 * Retourne la liste de tous les champs pour un �tablissement et un param�tre donn�
	 * @param etb
	 * @param parameter
	 * @return
	 */
	public ArrayList<Record> getRecordsByETB_PARAM(String etb, String parameter)
	{
		if (parameter == null) return null;
		
		etb = etb.toUpperCase();
		parameter = parameter.trim().toUpperCase();
		String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_" + parameter;
		final ArrayList<Record> listeRecord = select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "+getLibrary()+".PGVMPARM where PARETB = '"+ etb +"' and PARTYP = '"+parameter+"'", (DataStructureRecord) newObject(ds));
		
		return listeRecord;
	}
	
	/**
	 * Retourne la liste de tous les champs pour un �tablissement, un type donn� et un code. Normalement cela ne devrait retourner qu'un enregistrement
	 * @param etb
	 * @param parameter
	 * @return
	 */
	public ArrayList<Record> getRecordsByType_Code(String etb, String type, String code)
	{
		if (etb == null || type == null || code == null) return null;
		
		etb = etb.toUpperCase();
		type = type.trim().toUpperCase();
		code = code.trim().toUpperCase();
		String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_" + type;
		final ArrayList<Record> listeRecord = select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "+getLibrary()+".PGVMPARM where PARETB = '" + etb +"' and PARTYP = '"+ type + "' and PARIND = '" + code + "'", (DataStructureRecord) newObject(ds));
		
		return listeRecord;
	}
}
