//=================================================================================================
//==>                                                                       09/12/2011 - 12/12/2011
//==> Gestion des �v�nements sur les DTAQs (pas tr�s utile car d�clench� juste avant l'action)
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.DataQueueEvent;
import com.ibm.as400.access.DataQueueListener;

public class DataQListener implements DataQueueListener
{

	public void cleared(DataQueueEvent event)
	{
        System.out.println("Clear occurred");
        System.out.println("ID: " + event.getID());
	}

	public void peeked(DataQueueEvent event)
	{
        System.out.println("Peek occurred");
        System.out.println("ID: " + event.getID());
	}

	public void read(DataQueueEvent event)
	{
        System.out.println("Read occurred");
        //System.out.println("ID: " + event.getID()+ " " + event.getSource());
        //System.out.println("Yop => "  + ((DataQ)event.getSource()).getMessagelu());
        //System.out.println("Yop => "  + ((DataQ)event.getSource()).messagelu);
	}

	public void written(DataQueueEvent event)
	{
        System.out.println("Write occurred");
        //System.out.println("ID: " + event.getID() + " " + event.getSource());
	}

}
