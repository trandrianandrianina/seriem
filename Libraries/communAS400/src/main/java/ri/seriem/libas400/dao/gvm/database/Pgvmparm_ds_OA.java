//=================================================================================================
//==>                                                                       11/09/2015 - 11/09/2015
//==> Description de l'enregistrement du fichier Pgvmparm_ds_OA pour les OA
//==> G�n�r� avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
//=================================================================================================
package ri.seriem.libas400.dao.gvm.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_OA extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "OALIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "OAME1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "OAME2"));

		length = 200;
	}
}
