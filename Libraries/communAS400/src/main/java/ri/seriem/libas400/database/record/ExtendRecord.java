package ri.seriem.libas400.database.record;

import java.io.UnsupportedEncodingException;

import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.Record;

public class ExtendRecord extends Record
{
	private static final long serialVersionUID = 1L;
	
	private Record record;
	private FieldDescription[] descriptions=null;
	
	
	/**
	 * @param record the record to set
	 */
	public void setRecord(Record record)
	{
		this.record = record;
		if (descriptions == null)
			descriptions = record.getRecordFormat().getFieldDescriptions();
	}

	/**
	 * @return the record
	 */
	public Record getRecord()
	{
		return record;
	}

	/**
	 * Retourne le nombre de champs
	 */
	public int getNumberOfFields()
	{
		return record.getNumberOfFields(); 
	}
	
	/**
	 * Red�finition afin d'int�grer le try/catch
	 */
	public Object getField(int indice)
	{
		try
		{
			return record.getField(indice);
		}
		catch (UnsupportedEncodingException e)
		{
			return "";
			//e.printStackTrace();
		}
	}

	/**
	 * Red�finition afin d'int�grer le try/catch
	 */
	public Object getField(String field)
	{
		try
		{
			return record.getField(field);
		}
		catch (UnsupportedEncodingException e)
		{
			return "";
			//e.printStackTrace();
		}
	}
	
	/**
	 * Retourne le nom du champ � l'indice donn�
	 * @param indice
	 * @return
	 */
	public String getFieldName(int indice)
	{
		return descriptions[indice].getFieldName();
	}

}
