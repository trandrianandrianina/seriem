//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_UN pour les UN
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_UN extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(15), "UNLIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "UNU"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(40, 0), "UNK")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNNAT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNCND"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 3), "UNTAR"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "UNDEC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "UNART"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNCPDS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNCVOL"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNCCOL"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNQTD"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "UNCRDM"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "UNETQ"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "UNLIB2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "UNLONG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "UNLARG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "UNHAUT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "UNCMAX"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "UNREFU"));

		length = 300;
	}
}
