//=================================================================================================
//==>                                                                       11/05/2016 - 12/05/2016
//==> Gestion de l'envoi de mail via S�rie N
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.dao.exp.programs.mail;

import ri.seriem.libas400.dao.exp.programs.contact.GM_Contact;
import ri.seriem.libas400.dao.exp.programs.contact.M_Contact;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.InfosMailServer;
import ri.seriem.libcommun.outils.LogMessage;
import ri.seriem.mail.envoi.mailEnvoi;

public class MailManager
{
	// Constantes
    private		final static	String						NOM_CLASSE				= "[MailManager]";
	
	// Variables
	private						QueryManager				query					= null;
	private						String						mailsrvalias			= null;					// Alias du serveur de mail (PSEMRTEM - REPRF)
	private						String						mailtoalias				= null;					// Alias des destinataires (PSEMRTEM - REPRF)
	private 					boolean						isValide 				= false;
    private						LogMessage					log						= null;
    private						String						to						= null;
    private						String						cc						= "";
    private						InfosMailServer				infosMailServer			= null;
	private						String[]					params					= {	"TO",				// Destinataire
																		                 "FROM",			// Emetteur
																		                 "SUBJECT",
																		                 "",				// Message 1
																		                 "",				// Message 2
																		                 "",				// Message 3
																		                 "",				// Message 3
																		                 "",				// Priorit�
																		                 "",				// AR
																		                 "HOSTO",
																		                 "LOGIN",
																		                 "PWD",
																		                 "",				// Adresse IP
																		                 "",				// Dossier
																		                 "0",				// Nbr de fichiers
																		                 "",
																		                 "",
																		                 "",
																		                 "",
																		                 "",
																		                 "",
																		                 "",
																		                 "CC",				// CC
																		                 "",
																		                 "",				// Port
																		                 ""
																						};

	/**
	 * Constructeur
	 * @param aquery
	 * @param aliasmail
	 * @param aliascc
	 * @param alog
	 */
	public MailManager(QueryManager aquery, String aliasmail, String aliascc, LogMessage alog)
	{
		setQuery(aquery);
		setMailsrvalias(aliasmail);
		setMailtoalias(aliascc);
		setLog(alog);
	}

	
	// -- M�thodes publiques --------------------------------------------------

	
	/**
	 * Initialisation avant traitement
	 * @return
	 */
	public boolean init()
	{
		if( !searchInfosMailServer() )
		{ 
			isValide = false;
			return false;
		}
		if( !searchEmailRecipients() )
		{
			isValide = false;
			return false;
		}
		initParamsMailSrv();
		
		isValide = true;
		return true;
	}
	
	/**
	 * Envoi d'un mail
	 * @param subject
	 * @param msg1
	 * @param msg2
	 * @param msg3
	 * @param msg4
	 */
	public void send(String subject, String msg){
		params[2] = subject;
		params[3] = msg;
		params[4] = "";
		params[5] = "";
		params[6] = "";
		//On logge
		if(log!=null)
			log.ecritureMessage("mail envoye: sujet " + subject + " / message: " + msg);
		//on envoie
		new mailEnvoi(params);
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		log = null;
		query = null;
	}
	
	// -- M�thodes priv�es ----------------------------------------------------
	

	/**
	 * R�cup�ration des informations sur le serveur de mail
	 */
	private boolean searchInfosMailServer()
	{
		if( (query == null) || (mailsrvalias == null) || (mailsrvalias.trim().length() == 0) ){
	        log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Erreur lors de la r�cup�ration des infos sur le serveur de mail.");
			return false;
		}
		
		GM_Contact gc = new GM_Contact( query );
		infosMailServer = gc.getInfosMailServer(mailsrvalias);
		return true;
	}

	/**
	 * R�cup�ration des emails des destinataires
	 */
	private boolean searchEmailRecipients()
	{
		if( (query == null) || (mailtoalias == null) || (mailtoalias.trim().length() == 0)){
	        log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Erreur lors de la r�cup�ration des emails des destinataires.");
			return false;
		}
		
		String[] lstAlias = Constantes.splitString(mailtoalias, ';');
		if( lstAlias.length > 0){
			// Le premier est le destinataire principal
			to = getEmail4Alias(lstAlias[0]);
			if( to.length() == 0 ){
				return false;
			}

			// On traite les CC
			String chaine;
			StringBuffer sb = new StringBuffer();
			for( int i=1; i<lstAlias.length; i++ ){
				chaine = getEmail4Alias(lstAlias[i]);
				if( chaine.length() != 0 ){
					sb.append( chaine ).append(';');
				}
			}
			if( sb.length() > 0 ){
				sb.deleteCharAt(sb.length()-1);
				cc = sb.toString();
			}
		}
		return true;
	}

	/**
	 * Retourne l'email pour un alias
	 * @param alias
	 * @return
	 */
	private String getEmail4Alias(String alias)
	{
		GM_Contact gc = new GM_Contact( query );
		M_Contact c = gc.readOneContact( alias );
		if( c != null )
			return c.getRENET();
		else{
	        log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Erreur l'alias " + alias + " n'a pas d'email.");
		}
		return "";
	}
	
	/**
	 * Initialisation du tableau de param�tres
	 */
	private void initParamsMailSrv()
	{
		params[0] = to;
		params[1] = infosMailServer.getEmail();
		params[9] = infosMailServer.getHosto();
		params[10] = infosMailServer.getLogin();
		params[11] = infosMailServer.getPassword();
		params[22] = cc;
		params[24] = "" + infosMailServer.getPorto();
		System.out.println("INFOS ENVOI MAIL -->" +	infosMailServer.getEmail() + "\n"+ infosMailServer.getHosto() +"\n" + infosMailServer.getLogin() +"\n" + infosMailServer.getPassword() + "\n"+ cc +"\n" + "" + infosMailServer.getPorto());
		
		isValide = infosMailServer!=null && infosMailServer.getEmail()!=null && infosMailServer.getHosto()!=null && infosMailServer.getLogin()!=null && infosMailServer.getPassword()!=null && infosMailServer.getPorto()>-1;
		
	}


	// -- Accesseurs ----------------------------------------------------------


	/**
	 * @return le query
	 */
	public QueryManager getQuery()
	{
		return query;
	}


	/**
	 * @param query le query � d�finir
	 */
	public void setQuery(QueryManager query)
	{
		this.query = query;
	}


	/**
	 * @return le mailsrvalias
	 */
	public String getMailsrvalias()
	{
		return mailsrvalias;
	}

	/**
	 * @param mailsrvalias le mailsrvalias � d�finir
	 */
	public void setMailsrvalias(String mailsrvalias)
	{
		this.mailsrvalias = mailsrvalias;
	}


	/**
	 * @return le mailtoalias
	 */
	public String getMailtoalias()
	{
		return mailtoalias;
	}


	/**
	 * @param mailtoalias le mailtoalias � d�finir
	 */
	public void setMailtoalias(String mailtoalias)
	{
		this.mailtoalias = mailtoalias;
	}


	
	public boolean isValide()
	{
		return isValide;
	}


	public void setValide(boolean isValide)
	{
		this.isValide = isValide;
	}


	/**
	 * @return le log
	 */
	public LogMessage getLog()
	{
		return log;
	}


	/**
	 * @param log le log � d�finir
	 */
	public void setLog(LogMessage log)
	{
		this.log = log;
	}
}
