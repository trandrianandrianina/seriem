package ri.seriem.libas400.dao.gvx.database.files;

import ri.seriem.libas400.database.BaseFileDB;
import ri.seriem.libas400.database.QueryManager;

public abstract class FFD_Pgvmartm extends BaseFileDB
{
	// Constantes (valeurs r�cup�r�es via DSPFFD)
	public static final int SIZE_A1TOP			= 1;
	public static final int DECIMAL_A1TOP		= 0;
	public static final int SIZE_A1CRE			= 7;
	public static final int DECIMAL_A1CRE		= 0;
	public static final int SIZE_A1MOD			= 7;
	public static final int DECIMAL_A1MOD		= 0;
	public static final int SIZE_A1TRT			= 7;
	public static final int DECIMAL_A1TRT		= 0;
	public static final int SIZE_A1ETB			= 3;
	public static final int SIZE_A1ART			= 20;
	public static final int SIZE_A1NPU			= 1;
	public static final int SIZE_A1FAM			= 3;
	public static final int SIZE_A1LIB			= 30;
	public static final int SIZE_A1CL1			= 20;
	public static final int SIZE_A1CL2			= 20;
	public static final int SIZE_A1ASB			= 20;
	public static final int SIZE_A1GCD			= 13;
	public static final int DECIMAL_A1GCD		= 0;
	public static final int SIZE_A1NOP			= 9;
	public static final int SIZE_A1NMC			= 1;
	public static final int DECIMAL_A1NMC		= 0;
	public static final int SIZE_A1RST			= 20;
	public static final int SIZE_A1TOP1			= 2;
	public static final int SIZE_A1TOP2			= 2;
	public static final int SIZE_A1TOP3			= 2;
	public static final int SIZE_A1TOP4			= 2;
	public static final int SIZE_A1TOP5			= 2;
	public static final int SIZE_A1DSG1			= 1;
	public static final int SIZE_A1DSG2			= 1;
	public static final int SIZE_A1DSG3			= 1;
	public static final int SIZE_A1DSG4			= 1;
	public static final int SIZE_A1RTA			= 5;
	public static final int SIZE_A1CNV			= 5;
	public static final int SIZE_A1DDV			= 5;
	public static final int DECIMAL_A1DDV		= 0;
	public static final int SIZE_A1TSP			= 1;
	public static final int SIZE_A1TVA			= 1;
	public static final int DECIMAL_A1TVA		= 0;
	public static final int SIZE_A1TPF			= 1;
	public static final int DECIMAL_A1TPF		= 0;
	public static final int SIZE_A1SPE			= 1;
	public static final int DECIMAL_A1SPE		= 0;
	public static final int SIZE_A1STK			= 1;
	public static final int DECIMAL_A1STK		= 0;
	public static final int SIZE_A1DCS			= 1;
	public static final int DECIMAL_A1DCS		= 0;
	public static final int SIZE_A1DCV			= 1;
	public static final int DECIMAL_A1DCV		= 0;
	public static final int SIZE_A1SAI			= 1;
	public static final int DECIMAL_A1SAI		= 0;
	public static final int SIZE_A1TNC			= 1;
	public static final int DECIMAL_A1TNC		= 0;
	public static final int SIZE_A1RON			= 1;
	public static final int DECIMAL_A1RON		= 0;
	public static final int SIZE_A1UNV			= 2;
	public static final int SIZE_A1KSV			= 7;
	public static final int DECIMAL_A1KSV		= 3;
	public static final int SIZE_A1CMA			= 5;
	public static final int DECIMAL_A1CMA		= 4;
	public static final int SIZE_A1PMM			= 2;
	public static final int DECIMAL_A1PMM		= 0;
	public static final int SIZE_A1PMR			= 2;
	public static final int DECIMAL_A1PMR		= 0;
	public static final int SIZE_A1COF			= 1;
	public static final int DECIMAL_A1COF		= 0;
	public static final int SIZE_A1FRS			= 6;
	public static final int DECIMAL_A1FRS		= 0;
	public static final int SIZE_A1PDA			= 9;
	public static final int DECIMAL_A1PDA		= 2;
	public static final int SIZE_A1DDA			= 7;
	public static final int DECIMAL_A1DDA		= 0;
	public static final int SIZE_A1REA			= 1;
	public static final int DECIMAL_A1REA		= 0;
	public static final int SIZE_A1PERD			= 4;
	public static final int DECIMAL_A1PERD		= 0;
	public static final int SIZE_A1PERF			= 4;
	public static final int DECIMAL_A1PERF		= 0;
	public static final int SIZE_A1PERK			= 3;
	public static final int DECIMAL_A1PERK		= 0;
	public static final int SIZE_A1PE2D			= 4;
	public static final int DECIMAL_A1PE2D		= 0;
	public static final int SIZE_A1PE2F			= 4;
	public static final int DECIMAL_A1PE2F		= 0;
	public static final int SIZE_A1PE2K			= 3;
	public static final int DECIMAL_A1PE2K		= 0;
	public static final int SIZE_A1TPR			= 1;
	public static final int DECIMAL_A1TPR		= 0;
	public static final int SIZE_A1CPR			= 3;
	public static final int SIZE_A1PRV			= 9;
	public static final int DECIMAL_A1PRV		= 2;
	public static final int SIZE_A1UNS			= 2;
	public static final int SIZE_A1PDS			= 7;
	public static final int DECIMAL_A1PDS		= 3;
	public static final int SIZE_A1VOL			= 7;
	public static final int DECIMAL_A1VOL		= 3;
	public static final int SIZE_A1COL			= 2;
	public static final int DECIMAL_A1COL		= 0;
	public static final int SIZE_A1RGC			= 5;
	public static final int SIZE_A1ABC			= 1;
	public static final int SIZE_A1CND			= 9;
	public static final int DECIMAL_A1CND		= 3;
	public static final int SIZE_A1CND2			= 9;
	public static final int DECIMAL_A1CND2		= 3;
	public static final int SIZE_A1CND3			= 9;
	public static final int DECIMAL_A1CND3		= 3;
	public static final int SIZE_A1CND4			= 9;
	public static final int DECIMAL_A1CND4		= 3;
	public static final int SIZE_A1UNL			= 2;
	public static final int SIZE_A1UNL2			= 2;
	public static final int SIZE_A1UNL3			= 2;
	public static final int SIZE_A1UNL4			= 2;
	public static final int SIZE_A1TRU2			= 1;
	public static final int SIZE_A1TRU3			= 1;
	public static final int SIZE_A1TRU4			= 1;
	public static final int SIZE_A1PRI			= 9;
	public static final int DECIMAL_A1PRI		= 2;
	public static final int SIZE_A1PFB			= 9;
	public static final int DECIMAL_A1PFB		= 2;
	public static final int SIZE_A1MDP			= 7;
	public static final int DECIMAL_A1MDP		= 2;
	public static final int SIZE_A1PDP			= 5;
	public static final int DECIMAL_A1PDP		= 2;
	public static final int SIZE_A1CDP			= 1;
	public static final int SIZE_A1MAG			= 2;
	public static final int SIZE_A1MEX			= 2;
	public static final int SIZE_A1IN1			= 1;
	public static final int SIZE_A1IN2			= 1;
	public static final int SIZE_A1IN3			= 1;
	public static final int SIZE_A1IN4			= 1;
	public static final int SIZE_A1IN5			= 1;
	public static final int SIZE_A1IN6			= 1;
	public static final int SIZE_A1IN7			= 1;
	public static final int SIZE_A1IN8			= 1;
	public static final int SIZE_A1IN9			= 1;
	public static final int SIZE_A1N11			= 1;
	public static final int DECIMAL_A1N11		= 0;
	public static final int SIZE_A1N12			= 1;
	public static final int DECIMAL_A1N12		= 0;
	public static final int SIZE_A1N13			= 1;
	public static final int DECIMAL_A1N13		= 0;
	public static final int SIZE_A1N51			= 5;
	public static final int DECIMAL_A1N51		= 2;
	public static final int SIZE_A1N71			= 7;
	public static final int DECIMAL_A1N71		= 3;
	public static final int SIZE_A1DAT1			= 7;
	public static final int DECIMAL_A1DAT1		= 0;
	public static final int SIZE_A1DAT2			= 7;
	public static final int DECIMAL_A1DAT2		= 0;
	public static final int SIZE_A1DAT3			= 7;
	public static final int DECIMAL_A1DAT3		= 0;
	public static final int SIZE_A1EDT1			= 1;
	public static final int SIZE_A1EDT2			= 1;
	public static final int SIZE_A1EDT3			= 1;
	public static final int SIZE_A1EDT4			= 1;
	public static final int SIZE_A1EDT5			= 1;
	public static final int SIZE_A1EDT6			= 1;
	public static final int SIZE_A1EDT7			= 1;
	public static final int SIZE_A1EDT8			= 1;
	public static final int SIZE_A1EDT9			= 1;
	public static final int SIZE_A1UCS			= 2;
	public static final int SIZE_A1KCS			= 8;
	public static final int DECIMAL_A1KCS		= 3;
	public static final int SIZE_A1SFA			= 5;
	public static final int SIZE_A1IN10			= 1;
	public static final int SIZE_A1IN11			= 1;
	public static final int SIZE_A1IN12			= 1;
	public static final int SIZE_A1IN13			= 1;
	public static final int SIZE_A1IN14			= 1;
	public static final int SIZE_A1IN15			= 1;
	public static final int SIZE_A1REF1			= 15;
	public static final int SIZE_A1CCO			= 5;
	public static final int DECIMAL_A1CCO		= 3;
	public static final int SIZE_A1REC			= 1;
	public static final int SIZE_A1SV1			= 2;
	public static final int SIZE_A1SV2			= 2;
	public static final int SIZE_A1UNP			= 2;
	public static final int SIZE_A1IN16			= 1;
	public static final int SIZE_A1IN17			= 1;
	public static final int SIZE_A1UNT			= 2;
	public static final int SIZE_A1KTS			= 8;
	public static final int DECIMAL_A1KTS		= 3;
	public static final int SIZE_A1LNG			= 4;
	public static final int DECIMAL_A1LNG		= 2;
	public static final int SIZE_A1LRG			= 4;
	public static final int DECIMAL_A1LRG		= 2;
	public static final int SIZE_A1HTR			= 4;
	public static final int DECIMAL_A1HTR		= 2;
	public static final int SIZE_A1PDSL			= 7;
	public static final int DECIMAL_A1PDSL		= 3;
	public static final int SIZE_A1VOLL			= 7;
	public static final int DECIMAL_A1VOLL		= 3;
	public static final int SIZE_A1TDUO			= 1;
	public static final int SIZE_A1DUO1			= 2;
	public static final int DECIMAL_A1DUO1		= 0;
	public static final int SIZE_A1DUO2			= 2;
	public static final int DECIMAL_A1DUO2		= 0;
	public static final int SIZE_A1DUO3			= 2;
	public static final int DECIMAL_A1DUO3		= 0;
	public static final int SIZE_A1PDN			= 7;
	public static final int DECIMAL_A1PDN		= 3;
	public static final int SIZE_A1NDP			= 13;
	public static final int SIZE_A1OPA			= 2;
	public static final int SIZE_A1IN18			= 1;
	public static final int SIZE_A1IN19			= 1;
	public static final int SIZE_A1IN20			= 1;
	public static final int SIZE_A1IN21			= 1;
	public static final int SIZE_A1IN22			= 1;
	public static final int SIZE_A1IN23			= 1;
	public static final int SIZE_A1IN24			= 1;
	public static final int SIZE_A1IN25			= 1;
	public static final int SIZE_A1DAT4			= 7;
	public static final int DECIMAL_A1DAT4		= 0;
	public static final int SIZE_A1DAT5			= 7;
	public static final int DECIMAL_A1DAT5		= 0;
	public static final int SIZE_A1DAT6			= 7;
	public static final int DECIMAL_A1DAT6		= 0;
	public static final int SIZE_A1QT1			= 7;
	public static final int DECIMAL_A1QT1		= 0;
	public static final int SIZE_A1QT2			= 7;
	public static final int DECIMAL_A1QT2		= 0;
	public static final int SIZE_A1QT3			= 5;
	public static final int DECIMAL_A1QT3		= 0;
	public static final int SIZE_A1QT4			= 5;
	public static final int DECIMAL_A1QT4		= 2;
	public static final int SIZE_A1QT5			= 7;
	public static final int DECIMAL_A1QT5		= 5;
	public static final int SIZE_A1UNL5			= 2;
	public static final int SIZE_A1CND5			= 9;
	public static final int DECIMAL_A1CND5		= 3;
	public static final int SIZE_A1QT6			= 7;
	public static final int DECIMAL_A1QT6		= 0;
	public static final int SIZE_A1ZP20			= 40;
	public static final int SIZE_A1ZP21			= 20;
	public static final int SIZE_A1PRS			= 9;
	public static final int DECIMAL_A1PRS		= 2;
	public static final int SIZE_A1TNS			= 2;
	public static final int SIZE_A1IN26			= 1;
	public static final int SIZE_A1IN27			= 1;
	public static final int SIZE_A1IN28			= 1;
	public static final int SIZE_A1IN29			= 1;
	public static final int SIZE_A1IN30			= 1;
	public static final int SIZE_A1IN31			= 1;
	public static final int SIZE_A1IN32			= 1;
	public static final int SIZE_A1IN33			= 1;
	public static final int SIZE_A1IN34			= 1;
	public static final int SIZE_A1IN35			= 1;
	public static final int SIZE_A1MOT1			= 3;
	public static final int SIZE_A1MOT2			= 3;
	public static final int SIZE_A1MOT3			= 3;
	public static final int SIZE_A1OBS			= 10;
	public static final int SIZE_A1CNV1			= 3;
	public static final int SIZE_A1CNV2			= 5;
	public static final int SIZE_A1CNV3			= 6;
	public static final int SIZE_A1PRI1			= 9;
	public static final int DECIMAL_A1PRI1		= 2;
	public static final int SIZE_A1PRI2			= 9;
	public static final int DECIMAL_A1PRI2		= 2;
	public static final int SIZE_A1DAT7			= 7;
	public static final int DECIMAL_A1DAT7		= 0;
	public static final int SIZE_A1DAT8			= 7;
	public static final int DECIMAL_A1DAT8		= 0;
	public static final int SIZE_A1ZON1			= 10;
	public static final int SIZE_A1ZON2			= 10;

	// Variables fichiers
	protected int		A1TOP	= 0;		// Top syst�me
	protected int		A1CRE	= 0;		// Date de cr�ation
	protected int		A1MOD	= 0;		// Date de modification
	protected int		A1TRT	= 0;		// Date de traitement
	protected String	A1ETB	= null;		// Code �tablissement
	protected String	A1ART	= null;		// Code article
	protected char	A1NPU	= ' ';		// Top ne plus utiliser
	protected String	A1FAM	= null;		// Code famille
	protected String	A1LIB	= null;		// Libell� article
	protected String	A1CL1	= null;		// Cl� de classement 1
	protected String	A1CL2	= null;		// Cl� de classement 2
	protected String	A1ASB	= null;		// Article de remplacement Stk
	protected int		A1GCD	= 0;		// Code gencod
	protected String	A1NOP	= null;		// Nomenclature produit(CEE)
	protected int		A1NMC	= 0;		// Article nomenclature
	protected String	A1RST	= null;		// Regroupement Stat.
	protected String	A1TOP1	= null;		// Crit�res d'analyse 1
	protected String	A1TOP2	= null;		// Crit�res d'analyse 2
	protected String	A1TOP3	= null;		// Crit�res d'analyse 3
	protected String	A1TOP4	= null;		// Crit�res d'analyse 4
	protected String	A1TOP5	= null;		// Crit�res d'analyse 5
	protected char	A1DSG1	= ' ';		// Top de d�signation 1
	protected char	A1DSG2	= ' ';		// Top de d�signation 2
	protected char	A1DSG3	= ' ';		// Top de d�signation 3
	protected char	A1DSG4	= ' ';		// Top de d�signation 4
	protected String	A1RTA	= null;		// Code tarif
	protected String	A1CNV	= null;		// Rattachement CNV
	protected int		A1DDV	= 0;		// Date de derni�re vente
	protected char	A1TSP	= ' ';		// Top (2,3,V,S ou *)
	protected int		A1TVA	= 0;		// Code Tva
	protected int		A1TPF	= 0;		// Code Tpf
	protected int		A1SPE	= 0;		// Code sp�cial
	protected int		A1STK	= 0;		// Non stock�
	protected int		A1DCS	= 0;		// Top d�cimalisation pour US
	protected int		A1DCV	= 0;		// Top d�cimalisation pour UV
	protected int		A1SAI	= 0;		// article utilis� en saisie
	protected int		A1TNC	= 0;		// Top non commissionn�
	protected int		A1RON	= 0;		// Code d'arrondi
	protected String	A1UNV	= null;		// Code unit� de vente
	protected int		A1KSV	= 0;		// Coefficient US/UV
	protected int		A1CMA	= 0;		// Coefficient de marge
	protected int		A1PMM	= 0;		// Pourcentage de marge mini.
	protected int		A1PMR	= 0;		// Pourcentage maximum remise
	protected int		A1COF	= 0;		// Collectif fournisseur
	protected int		A1FRS	= 0;		// Num�ro fournisseur
	protected int		A1PDA	= 0;		// Dernier  prix d'achat
	protected int		A1DDA	= 0;		// Derniere date d'achat
	protected int		A1REA	= 0;		// Top r�appro
	protected int		A1PERD	= 0;		// Saison date d�but
	protected int		A1PERF	= 0;		// Saison date fin
	protected int		A1PERK	= 0;		// % de conso
	protected int		A1PE2D	= 0;		// Saison 2 date d�but
	protected int		A1PE2F	= 0;		// Saison 2 date fin
	protected int		A1PE2K	= 0;		// % de conso 2
	protected int		A1TPR	= 0;		// Protection prix de revient
	protected String	A1CPR	= null;		// Code prix de revient
	protected int		A1PRV	= 0;		// Prix de revient
	protected String	A1UNS	= null;		// Code unit� de stockage
	protected int		A1PDS	= 0;		// Poids en unit� de Stock
	protected int		A1VOL	= 0;		// Volume
	protected int		A1COL	= 0;		// Colisage
	protected String	A1RGC	= null;		// Regroupement Colisage
	protected char	A1ABC	= ' ';		// Zone ABC
	protected int		A1CND	= 0;		// Conditionnement
	protected int		A1CND2	= 0;		// Conditionnement 2
	protected int		A1CND3	= 0;		// Conditionnement 3
	protected int		A1CND4	= 0;		// Conditionnement 4
	protected String	A1UNL	= null;		// Unit� de conditionnement Vte
	protected String	A1UNL2	= null;		// Unit� conditionnement Vte 2
	protected String	A1UNL3	= null;		// Unit� conditionnement Vte 3
	protected String	A1UNL4	= null;		// Unit� conditionnement Vte 4
	protected char	A1TRU2	= ' ';		// Ind.rattachement unitaire 2
	protected char	A1TRU3	= ' ';		// Ind.rattachement unitaire 3
	protected char	A1TRU4	= ' ';		// Ind.rattachement unitaire 4
	protected int		A1PRI	= 0;		// Prix d'inventaire
	protected int		A1PFB	= 0;		// Prix de fab. OU mini vente
	protected int		A1MDP	= 0;		// Montant d�pr�ciation
	protected int		A1PDP	= 0;		// % d�pr�ciation
	protected char	A1CDP	= ' ';		// Code d�pr�ciation
	protected String	A1MAG	= null;		// Magasin  (Tourn�e)
	protected String	A1MEX	= null;		// Mode exp�dition (Tourn�e)
	protected char	A1IN1	= ' ';		// Ind. non soumis � escompte
	protected char	A1IN2	= ' ';		// Ind. gestion par lots
	protected char	A1IN3	= ' ';		// Type de fiche article
	protected char	A1IN4	= ' ';		// Type de substitution ASB
	protected char	A1IN5	= ' ';		// Ind. non g�r� en stats
	protected char	A1IN6	= ' ';		// Ind. non edt d�tail nmc
	protected char	A1IN7	= ' ';		// Type extension article/ligne
	protected char	A1IN8	= ' ';		// Ind. cnd non scindable
	protected char	A1IN9	= ' ';		// Exclusion remises globales
	protected int		A1N11	= 0;		// Zone utilis�e sp�cif1
	protected int		A1N12	= 0;		// Zone utilis�e/wspm001171
	protected int		A1N13	= 0;		// Zone non utilis�e
	protected int		A1N51	= 0;		// Zone utilis�e sp�cif1
	protected int		A1N71	= 0;		// Zone utilis�e sp�cif1
	protected int		A1DAT1	= 0;		// Date d�but commercialisation
	protected int		A1DAT2	= 0;		// Date fin de r�appro.
	protected int		A1DAT3	= 0;		// Date fin commercialisation
	protected char	A1EDT1	= ' ';		// Edition sur Bon de pr�pa
	protected char	A1EDT2	= ' ';		// Accus� de R�ception de cde
	protected char	A1EDT3	= ' ';		// Bon d"exp�dition
	protected char	A1EDT4	= ' ';		// Bon transporteur
	protected char	A1EDT5	= ' ';		// Facture
	protected char	A1EDT6	= ' ';		// Devis
	protected char	A1EDT7	= ' ';		// N.U
	protected char	A1EDT8	= ' ';		// N.U
	protected char	A1EDT9	= ' ';		// N.U
	protected String	A1UCS	= null;		// Unit� de conditionnement Stk
	protected int		A1KCS	= 0;		// Nombre d"UNS par UCS
	protected String	A1SFA	= null;		// Sous famille
	protected char	A1IN10	= ' ';		// Ind. exclusion ristournes
	protected char	A1IN11	= ' ';		// Sp�cificit� Extraction
	protected char	A1IN12	= ' ';		// Ind. Ensemble de Mat�riels
	protected char	A1IN13	= ' ';		// Taxe additionnelle
	protected char	A1IN14	= ' ';		// Taxe Tgap
	protected char	A1IN15	= ' ';		// Exclu. WEB 1=Saisie interdit
	protected String	A1REF1	= null;		// R�f�rence 1
	protected int		A1CCO	= 0;		// Coef. condition / Qt�
	protected char	A1REC	= ' ';		// R�gime douanier
	protected String	A1SV1	= null;		// Syst�me variable 1
	protected String	A1SV2	= null;		// Syst�me variable 2
	protected String	A1UNP	= null;		// Unit� de Pr�sentation
	protected char	A1IN16	= ' ';		// Type de choix FRS
	protected char	A1IN17	= ' ';		// Type extension mat�riel
	protected String	A1UNT	= null;		// Unit� de Transport
	protected int		A1KTS	= 0;		// Nombre d"UNS par UNT
	protected int		A1LNG	= 0;		// Longueur
	protected int		A1LRG	= 0;		// Largeur
	protected int		A1HTR	= 0;		// Hauteur
	protected int		A1PDSL	= 0;		// Poids en UNL
	protected int		A1VOLL	= 0;		// Volume  en UNL
	protected char	A1TDUO	= ' ';		// Type DLUO (Mois/Semaine/Jr)
	protected int		A1DUO1	= 0;		// D�lai d"Utilisat� Optimale 1
	protected int		A1DUO2	= 0;		// D�lai d"Utilisat� Optimale 2
	protected int		A1DUO3	= 0;		// D�lai d"Utilisat� Optimale 3
	protected int		A1PDN	= 0;		// Poids net en US
	protected String	A1NDP	= null;		// Nomenclature douani�re
	protected String	A1OPA	= null;		// Origine
	protected char	A1IN18	= ' ';		// El�ment de frais
	protected char	A1IN19	= ' ';		// Type r�partition
	protected char	A1IN20	= ' ';		// Type de produit (GP)
	protected char	A1IN21	= ' ';		// Code disponibilit�
	protected char	A1IN22	= ' ';		// Sp�cif.
	protected char	A1IN23	= ' ';		// D�positaire
	protected char	A1IN24	= ' ';		// Sp�cif.
	protected char	A1IN25	= ' ';		// Type pour A1QT1
	protected int		A1DAT4	= 0;		// Sp�cif.
	protected int		A1DAT5	= 0;		// SP�CIF. CGED/RUBIS
	protected int		A1DAT6	= 0;		// non utilis�
	protected int		A1QT1	= 0;		// Qte Maxi pour alerte en UV
	protected int		A1QT2	= 0;		// Sp�cif.
	protected int		A1QT3	= 0;		// Sp�cif.
	protected int		A1QT4	= 0;		// Sp�cif.
	protected int		A1QT5	= 0;		// Sp�cif.
	protected String	A1UNL5	= null;		// Unit� conditionnement Vte 5
	protected int		A1CND5	= 0;		// Conditionnement 5
	protected int		A1QT6	= 0;		// N.U
	protected String	A1ZP20	= null;		// ZP A20
	protected String	A1ZP21	= null;		// ZP A21
	protected int		A1PRS	= 0;		// Prix de revient standard
	protected String	A1TNS	= null;		// Type Non suivi Param NS
	protected char	A1IN26	= ' ';		// Autorisation vte non dispo
	protected char	A1IN27	= ' ';		// Non utilis�
	protected char	A1IN28	= ' ';		// Non utilis�
	protected char	A1IN29	= ' ';		// Non utilis�
	protected char	A1IN30	= ' ';		// Non utilis�
	protected char	A1IN31	= ' ';		// Non utilis�
	protected char	A1IN32	= ' ';		// Non utilis�
	protected char	A1IN33	= ' ';		// Non utilis�
	protected char	A1IN34	= ' ';		// Non utilis�
	protected char	A1IN35	= ' ';		// Non utilis�
	protected String	A1MOT1	= null;		// Mot directeur 1
	protected String	A1MOT2	= null;		// Mot directeur 2
	protected String	A1MOT3	= null;		// Mot directeur 3
	protected String	A1OBS	= null;		// Observations courte
	protected String	A1CNV1	= null;		// Rattachement CNV 1
	protected String	A1CNV2	= null;		// Rattachement CNV 2
	protected String	A1CNV3	= null;		// Rattachement CNV 3
	protected int		A1PRI1	= 0;		// Prix NU
	protected int		A1PRI2	= 0;		// Prix NU
	protected int		A1DAT7	= 0;		// non utilis�
	protected int		A1DAT8	= 0;		// non utilis�
	protected String	A1ZON1	= null;		// Non utilis�
	protected String	A1ZON2	= null;		// Non utilis�

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public FFD_Pgvmartm(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	// -- M�thodes publiques --------------------------------------------------
	
	/**
	* Initialise les variables avec les valeurs par d�faut
	*/
	public void initialization()
	{
		A1TOP	= 0;
		A1CRE	= 0;
		A1MOD	= 0;
		A1TRT	= 0;
		A1ETB	= null;
		A1ART	= null;
		A1NPU	= ' ';
		A1FAM	= null;
		A1LIB	= null;
		A1CL1	= null;
		A1CL2	= null;
		A1ASB	= null;
		A1GCD	= 0;
		A1NOP	= null;
		A1NMC	= 0;
		A1RST	= null;
		A1TOP1	= null;
		A1TOP2	= null;
		A1TOP3	= null;
		A1TOP4	= null;
		A1TOP5	= null;
		A1DSG1	= ' ';
		A1DSG2	= ' ';
		A1DSG3	= ' ';
		A1DSG4	= ' ';
		A1RTA	= null;
		A1CNV	= null;
		A1DDV	= 0;
		A1TSP	= ' ';
		A1TVA	= 0;
		A1TPF	= 0;
		A1SPE	= 0;
		A1STK	= 0;
		A1DCS	= 0;
		A1DCV	= 0;
		A1SAI	= 0;
		A1TNC	= 0;
		A1RON	= 0;
		A1UNV	= null;
		A1KSV	= 0;
		A1CMA	= 0;
		A1PMM	= 0;
		A1PMR	= 0;
		A1COF	= 0;
		A1FRS	= 0;
		A1PDA	= 0;
		A1DDA	= 0;
		A1REA	= 0;
		A1PERD	= 0;
		A1PERF	= 0;
		A1PERK	= 0;
		A1PE2D	= 0;
		A1PE2F	= 0;
		A1PE2K	= 0;
		A1TPR	= 0;
		A1CPR	= null;
		A1PRV	= 0;
		A1UNS	= null;
		A1PDS	= 0;
		A1VOL	= 0;
		A1COL	= 0;
		A1RGC	= null;
		A1ABC	= ' ';
		A1CND	= 0;
		A1CND2	= 0;
		A1CND3	= 0;
		A1CND4	= 0;
		A1UNL	= null;
		A1UNL2	= null;
		A1UNL3	= null;
		A1UNL4	= null;
		A1TRU2	= ' ';
		A1TRU3	= ' ';
		A1TRU4	= ' ';
		A1PRI	= 0;
		A1PFB	= 0;
		A1MDP	= 0;
		A1PDP	= 0;
		A1CDP	= ' ';
		A1MAG	= null;
		A1MEX	= null;
		A1IN1	= ' ';
		A1IN2	= ' ';
		A1IN3	= ' ';
		A1IN4	= ' ';
		A1IN5	= ' ';
		A1IN6	= ' ';
		A1IN7	= ' ';
		A1IN8	= ' ';
		A1IN9	= ' ';
		A1N11	= 0;
		A1N12	= 0;
		A1N13	= 0;
		A1N51	= 0;
		A1N71	= 0;
		A1DAT1	= 0;
		A1DAT2	= 0;
		A1DAT3	= 0;
		A1EDT1	= ' ';
		A1EDT2	= ' ';
		A1EDT3	= ' ';
		A1EDT4	= ' ';
		A1EDT5	= ' ';
		A1EDT6	= ' ';
		A1EDT7	= ' ';
		A1EDT8	= ' ';
		A1EDT9	= ' ';
		A1UCS	= null;
		A1KCS	= 0;
		A1SFA	= null;
		A1IN10	= ' ';
		A1IN11	= ' ';
		A1IN12	= ' ';
		A1IN13	= ' ';
		A1IN14	= ' ';
		A1IN15	= ' ';
		A1REF1	= null;
		A1CCO	= 0;
		A1REC	= ' ';
		A1SV1	= null;
		A1SV2	= null;
		A1UNP	= null;
		A1IN16	= ' ';
		A1IN17	= ' ';
		A1UNT	= null;
		A1KTS	= 0;
		A1LNG	= 0;
		A1LRG	= 0;
		A1HTR	= 0;
		A1PDSL	= 0;
		A1VOLL	= 0;
		A1TDUO	= ' ';
		A1DUO1	= 0;
		A1DUO2	= 0;
		A1DUO3	= 0;
		A1PDN	= 0;
		A1NDP	= null;
		A1OPA	= null;
		A1IN18	= ' ';
		A1IN19	= ' ';
		A1IN20	= ' ';
		A1IN21	= ' ';
		A1IN22	= ' ';
		A1IN23	= ' ';
		A1IN24	= ' ';
		A1IN25	= ' ';
		A1DAT4	= 0;
		A1DAT5	= 0;
		A1DAT6	= 0;
		A1QT1	= 0;
		A1QT2	= 0;
		A1QT3	= 0;
		A1QT4	= 0;
		A1QT5	= 0;
		A1UNL5	= null;
		A1CND5	= 0;
		A1QT6	= 0;
		A1ZP20	= null;
		A1ZP21	= null;
		A1PRS	= 0;
		A1TNS	= null;
		A1IN26	= ' ';
		A1IN27	= ' ';
		A1IN28	= ' ';
		A1IN29	= ' ';
		A1IN30	= ' ';
		A1IN31	= ' ';
		A1IN32	= ' ';
		A1IN33	= ' ';
		A1IN34	= ' ';
		A1IN35	= ' ';
		A1MOT1	= null;
		A1MOT2	= null;
		A1MOT3	= null;
		A1OBS	= null;
		A1CNV1	= null;
		A1CNV2	= null;
		A1CNV3	= null;
		A1PRI1	= 0;
		A1PRI2	= 0;
		A1DAT7	= 0;
		A1DAT8	= 0;
		A1ZON1	= null;
		A1ZON2	= null;
	}

	
	/**
	 * @return le a1TOP
	 */
	public int getA1TOP()
	{
		return A1TOP;
	}

	/**
	 * @param a1top le a1TOP � d�finir
	 */
	public void setA1TOP(int a1top)
	{
		A1TOP = a1top;
	}

	/**
	 * @return le a1CRE
	 */
	public int getA1CRE()
	{
		return A1CRE;
	}

	/**
	 * @param a1cre le a1CRE � d�finir
	 */
	public void setA1CRE(int a1cre)
	{
		A1CRE = a1cre;
	}

	/**
	 * @return le a1MOD
	 */
	public int getA1MOD()
	{
		return A1MOD;
	}

	/**
	 * @param a1mod le a1MOD � d�finir
	 */
	public void setA1MOD(int a1mod)
	{
		A1MOD = a1mod;
	}

	/**
	 * @return le a1TRT
	 */
	public int getA1TRT()
	{
		return A1TRT;
	}

	/**
	 * @param a1trt le a1TRT � d�finir
	 */
	public void setA1TRT(int a1trt)
	{
		A1TRT = a1trt;
	}

	/**
	 * @return le a1ETB
	 */
	public String getA1ETB()
	{
		return A1ETB;
	}

	/**
	 * @param a1etb le a1ETB � d�finir
	 */
	public void setA1ETB(String a1etb)
	{
		A1ETB = a1etb;
	}

	/**
	 * @return le a1ART
	 */
	public String getA1ART()
	{
		return A1ART;
	}

	/**
	 * @param a1art le a1ART � d�finir
	 */
	public void setA1ART(String a1art)
	{
		A1ART = a1art;
	}

	/**
	 * @return le a1NPU
	 */
	public char getA1NPU()
	{
		return A1NPU;
	}

	/**
	 * @param a1npu le a1NPU � d�finir
	 */
	public void setA1NPU(char a1npu)
	{
		A1NPU = a1npu;
	}

	/**
	 * @return le a1FAM
	 */
	public String getA1FAM()
	{
		return A1FAM;
	}

	/**
	 * @param a1fam le a1FAM � d�finir
	 */
	public void setA1FAM(String a1fam)
	{
		A1FAM = a1fam;
	}

	/**
	 * @return le a1LIB
	 */
	public String getA1LIB()
	{
		return A1LIB;
	}

	/**
	 * @param a1lib le a1LIB � d�finir
	 */
	public void setA1LIB(String a1lib)
	{
		A1LIB = a1lib;
	}

	/**
	 * @return le a1CL1
	 */
	public String getA1CL1()
	{
		return A1CL1;
	}

	/**
	 * @param a1cl1 le a1CL1 � d�finir
	 */
	public void setA1CL1(String a1cl1)
	{
		A1CL1 = a1cl1;
	}

	/**
	 * @return le a1CL2
	 */
	public String getA1CL2()
	{
		return A1CL2;
	}

	/**
	 * @param a1cl2 le a1CL2 � d�finir
	 */
	public void setA1CL2(String a1cl2)
	{
		A1CL2 = a1cl2;
	}

	/**
	 * @return le a1ASB
	 */
	public String getA1ASB()
	{
		return A1ASB;
	}

	/**
	 * @param a1asb le a1ASB � d�finir
	 */
	public void setA1ASB(String a1asb)
	{
		A1ASB = a1asb;
	}

	/**
	 * @return le a1GCD
	 */
	public int getA1GCD()
	{
		return A1GCD;
	}

	/**
	 * @param a1gcd le a1GCD � d�finir
	 */
	public void setA1GCD(int a1gcd)
	{
		A1GCD = a1gcd;
	}

	/**
	 * @return le a1NOP
	 */
	public String getA1NOP()
	{
		return A1NOP;
	}

	/**
	 * @param a1nop le a1NOP � d�finir
	 */
	public void setA1NOP(String a1nop)
	{
		A1NOP = a1nop;
	}

	/**
	 * @return le a1NMC
	 */
	public int getA1NMC()
	{
		return A1NMC;
	}

	/**
	 * @param a1nmc le a1NMC � d�finir
	 */
	public void setA1NMC(int a1nmc)
	{
		A1NMC = a1nmc;
	}

	/**
	 * @return le a1RST
	 */
	public String getA1RST()
	{
		return A1RST;
	}

	/**
	 * @param a1rst le a1RST � d�finir
	 */
	public void setA1RST(String a1rst)
	{
		A1RST = a1rst;
	}

	/**
	 * @return le a1TOP1
	 */
	public String getA1TOP1()
	{
		return A1TOP1;
	}

	/**
	 * @param a1top1 le a1TOP1 � d�finir
	 */
	public void setA1TOP1(String a1top1)
	{
		A1TOP1 = a1top1;
	}

	/**
	 * @return le a1TOP2
	 */
	public String getA1TOP2()
	{
		return A1TOP2;
	}

	/**
	 * @param a1top2 le a1TOP2 � d�finir
	 */
	public void setA1TOP2(String a1top2)
	{
		A1TOP2 = a1top2;
	}

	/**
	 * @return le a1TOP3
	 */
	public String getA1TOP3()
	{
		return A1TOP3;
	}

	/**
	 * @param a1top3 le a1TOP3 � d�finir
	 */
	public void setA1TOP3(String a1top3)
	{
		A1TOP3 = a1top3;
	}

	/**
	 * @return le a1TOP4
	 */
	public String getA1TOP4()
	{
		return A1TOP4;
	}

	/**
	 * @param a1top4 le a1TOP4 � d�finir
	 */
	public void setA1TOP4(String a1top4)
	{
		A1TOP4 = a1top4;
	}

	/**
	 * @return le a1TOP5
	 */
	public String getA1TOP5()
	{
		return A1TOP5;
	}

	/**
	 * @param a1top5 le a1TOP5 � d�finir
	 */
	public void setA1TOP5(String a1top5)
	{
		A1TOP5 = a1top5;
	}

	/**
	 * @return le a1DSG1
	 */
	public char getA1DSG1()
	{
		return A1DSG1;
	}

	/**
	 * @param a1dsg1 le a1DSG1 � d�finir
	 */
	public void setA1DSG1(char a1dsg1)
	{
		A1DSG1 = a1dsg1;
	}

	/**
	 * @return le a1DSG2
	 */
	public char getA1DSG2()
	{
		return A1DSG2;
	}

	/**
	 * @param a1dsg2 le a1DSG2 � d�finir
	 */
	public void setA1DSG2(char a1dsg2)
	{
		A1DSG2 = a1dsg2;
	}

	/**
	 * @return le a1DSG3
	 */
	public char getA1DSG3()
	{
		return A1DSG3;
	}

	/**
	 * @param a1dsg3 le a1DSG3 � d�finir
	 */
	public void setA1DSG3(char a1dsg3)
	{
		A1DSG3 = a1dsg3;
	}

	/**
	 * @return le a1DSG4
	 */
	public char getA1DSG4()
	{
		return A1DSG4;
	}

	/**
	 * @param a1dsg4 le a1DSG4 � d�finir
	 */
	public void setA1DSG4(char a1dsg4)
	{
		A1DSG4 = a1dsg4;
	}

	/**
	 * @return le a1RTA
	 */
	public String getA1RTA()
	{
		return A1RTA;
	}

	/**
	 * @param a1rta le a1RTA � d�finir
	 */
	public void setA1RTA(String a1rta)
	{
		A1RTA = a1rta;
	}

	/**
	 * @return le a1CNV
	 */
	public String getA1CNV()
	{
		return A1CNV;
	}

	/**
	 * @param a1cnv le a1CNV � d�finir
	 */
	public void setA1CNV(String a1cnv)
	{
		A1CNV = a1cnv;
	}

	/**
	 * @return le a1DDV
	 */
	public int getA1DDV()
	{
		return A1DDV;
	}

	/**
	 * @param a1ddv le a1DDV � d�finir
	 */
	public void setA1DDV(int a1ddv)
	{
		A1DDV = a1ddv;
	}

	/**
	 * @return le a1TSP
	 */
	public char getA1TSP()
	{
		return A1TSP;
	}

	/**
	 * @param a1tsp le a1TSP � d�finir
	 */
	public void setA1TSP(char a1tsp)
	{
		A1TSP = a1tsp;
	}

	/**
	 * @return le a1TVA
	 */
	public int getA1TVA()
	{
		return A1TVA;
	}

	/**
	 * @param a1tva le a1TVA � d�finir
	 */
	public void setA1TVA(int a1tva)
	{
		A1TVA = a1tva;
	}

	/**
	 * @return le a1TPF
	 */
	public int getA1TPF()
	{
		return A1TPF;
	}

	/**
	 * @param a1tpf le a1TPF � d�finir
	 */
	public void setA1TPF(int a1tpf)
	{
		A1TPF = a1tpf;
	}

	/**
	 * @return le a1SPE
	 */
	public int getA1SPE()
	{
		return A1SPE;
	}

	/**
	 * @param a1spe le a1SPE � d�finir
	 */
	public void setA1SPE(int a1spe)
	{
		A1SPE = a1spe;
	}

	/**
	 * @return le a1STK
	 */
	public int getA1STK()
	{
		return A1STK;
	}

	/**
	 * @param a1stk le a1STK � d�finir
	 */
	public void setA1STK(int a1stk)
	{
		A1STK = a1stk;
	}

	/**
	 * @return le a1DCS
	 */
	public int getA1DCS()
	{
		return A1DCS;
	}

	/**
	 * @param a1dcs le a1DCS � d�finir
	 */
	public void setA1DCS(int a1dcs)
	{
		A1DCS = a1dcs;
	}

	/**
	 * @return le a1DCV
	 */
	public int getA1DCV()
	{
		return A1DCV;
	}

	/**
	 * @param a1dcv le a1DCV � d�finir
	 */
	public void setA1DCV(int a1dcv)
	{
		A1DCV = a1dcv;
	}

	/**
	 * @return le a1SAI
	 */
	public int getA1SAI()
	{
		return A1SAI;
	}

	/**
	 * @param a1sai le a1SAI � d�finir
	 */
	public void setA1SAI(int a1sai)
	{
		A1SAI = a1sai;
	}

	/**
	 * @return le a1TNC
	 */
	public int getA1TNC()
	{
		return A1TNC;
	}

	/**
	 * @param a1tnc le a1TNC � d�finir
	 */
	public void setA1TNC(int a1tnc)
	{
		A1TNC = a1tnc;
	}

	/**
	 * @return le a1RON
	 */
	public int getA1RON()
	{
		return A1RON;
	}

	/**
	 * @param a1ron le a1RON � d�finir
	 */
	public void setA1RON(int a1ron)
	{
		A1RON = a1ron;
	}

	/**
	 * @return le a1UNV
	 */
	public String getA1UNV()
	{
		return A1UNV;
	}

	/**
	 * @param a1unv le a1UNV � d�finir
	 */
	public void setA1UNV(String a1unv)
	{
		A1UNV = a1unv;
	}

	/**
	 * @return le a1KSV
	 */
	public int getA1KSV()
	{
		return A1KSV;
	}

	/**
	 * @param a1ksv le a1KSV � d�finir
	 */
	public void setA1KSV(int a1ksv)
	{
		A1KSV = a1ksv;
	}

	/**
	 * @return le a1CMA
	 */
	public int getA1CMA()
	{
		return A1CMA;
	}

	/**
	 * @param a1cma le a1CMA � d�finir
	 */
	public void setA1CMA(int a1cma)
	{
		A1CMA = a1cma;
	}

	/**
	 * @return le a1PMM
	 */
	public int getA1PMM()
	{
		return A1PMM;
	}

	/**
	 * @param a1pmm le a1PMM � d�finir
	 */
	public void setA1PMM(int a1pmm)
	{
		A1PMM = a1pmm;
	}

	/**
	 * @return le a1PMR
	 */
	public int getA1PMR()
	{
		return A1PMR;
	}

	/**
	 * @param a1pmr le a1PMR � d�finir
	 */
	public void setA1PMR(int a1pmr)
	{
		A1PMR = a1pmr;
	}

	/**
	 * @return le a1COF
	 */
	public int getA1COF()
	{
		return A1COF;
	}

	/**
	 * @param a1cof le a1COF � d�finir
	 */
	public void setA1COF(int a1cof)
	{
		A1COF = a1cof;
	}

	/**
	 * @return le a1FRS
	 */
	public int getA1FRS()
	{
		return A1FRS;
	}

	/**
	 * @param a1frs le a1FRS � d�finir
	 */
	public void setA1FRS(int a1frs)
	{
		A1FRS = a1frs;
	}

	/**
	 * @return le a1PDA
	 */
	public int getA1PDA()
	{
		return A1PDA;
	}

	/**
	 * @param a1pda le a1PDA � d�finir
	 */
	public void setA1PDA(int a1pda)
	{
		A1PDA = a1pda;
	}

	/**
	 * @return le a1DDA
	 */
	public int getA1DDA()
	{
		return A1DDA;
	}

	/**
	 * @param a1dda le a1DDA � d�finir
	 */
	public void setA1DDA(int a1dda)
	{
		A1DDA = a1dda;
	}

	/**
	 * @return le a1REA
	 */
	public int getA1REA()
	{
		return A1REA;
	}

	/**
	 * @param a1rea le a1REA � d�finir
	 */
	public void setA1REA(int a1rea)
	{
		A1REA = a1rea;
	}

	/**
	 * @return le a1PERD
	 */
	public int getA1PERD()
	{
		return A1PERD;
	}

	/**
	 * @param a1perd le a1PERD � d�finir
	 */
	public void setA1PERD(int a1perd)
	{
		A1PERD = a1perd;
	}

	/**
	 * @return le a1PERF
	 */
	public int getA1PERF()
	{
		return A1PERF;
	}

	/**
	 * @param a1perf le a1PERF � d�finir
	 */
	public void setA1PERF(int a1perf)
	{
		A1PERF = a1perf;
	}

	/**
	 * @return le a1PERK
	 */
	public int getA1PERK()
	{
		return A1PERK;
	}

	/**
	 * @param a1perk le a1PERK � d�finir
	 */
	public void setA1PERK(int a1perk)
	{
		A1PERK = a1perk;
	}

	/**
	 * @return le a1PE2D
	 */
	public int getA1PE2D()
	{
		return A1PE2D;
	}

	/**
	 * @param a1pe2d le a1PE2D � d�finir
	 */
	public void setA1PE2D(int a1pe2d)
	{
		A1PE2D = a1pe2d;
	}

	/**
	 * @return le a1PE2F
	 */
	public int getA1PE2F()
	{
		return A1PE2F;
	}

	/**
	 * @param a1pe2f le a1PE2F � d�finir
	 */
	public void setA1PE2F(int a1pe2f)
	{
		A1PE2F = a1pe2f;
	}

	/**
	 * @return le a1PE2K
	 */
	public int getA1PE2K()
	{
		return A1PE2K;
	}

	/**
	 * @param a1pe2k le a1PE2K � d�finir
	 */
	public void setA1PE2K(int a1pe2k)
	{
		A1PE2K = a1pe2k;
	}

	/**
	 * @return le a1TPR
	 */
	public int getA1TPR()
	{
		return A1TPR;
	}

	/**
	 * @param a1tpr le a1TPR � d�finir
	 */
	public void setA1TPR(int a1tpr)
	{
		A1TPR = a1tpr;
	}

	/**
	 * @return le a1CPR
	 */
	public String getA1CPR()
	{
		return A1CPR;
	}

	/**
	 * @param a1cpr le a1CPR � d�finir
	 */
	public void setA1CPR(String a1cpr)
	{
		A1CPR = a1cpr;
	}

	/**
	 * @return le a1PRV
	 */
	public int getA1PRV()
	{
		return A1PRV;
	}

	/**
	 * @param a1prv le a1PRV � d�finir
	 */
	public void setA1PRV(int a1prv)
	{
		A1PRV = a1prv;
	}

	/**
	 * @return le a1UNS
	 */
	public String getA1UNS()
	{
		return A1UNS;
	}

	/**
	 * @param a1uns le a1UNS � d�finir
	 */
	public void setA1UNS(String a1uns)
	{
		A1UNS = a1uns;
	}

	/**
	 * @return le a1PDS
	 */
	public int getA1PDS()
	{
		return A1PDS;
	}

	/**
	 * @param a1pds le a1PDS � d�finir
	 */
	public void setA1PDS(int a1pds)
	{
		A1PDS = a1pds;
	}

	/**
	 * @return le a1VOL
	 */
	public int getA1VOL()
	{
		return A1VOL;
	}

	/**
	 * @param a1vol le a1VOL � d�finir
	 */
	public void setA1VOL(int a1vol)
	{
		A1VOL = a1vol;
	}

	/**
	 * @return le a1COL
	 */
	public int getA1COL()
	{
		return A1COL;
	}

	/**
	 * @param a1col le a1COL � d�finir
	 */
	public void setA1COL(int a1col)
	{
		A1COL = a1col;
	}

	/**
	 * @return le a1RGC
	 */
	public String getA1RGC()
	{
		return A1RGC;
	}

	/**
	 * @param a1rgc le a1RGC � d�finir
	 */
	public void setA1RGC(String a1rgc)
	{
		A1RGC = a1rgc;
	}

	/**
	 * @return le a1ABC
	 */
	public char getA1ABC()
	{
		return A1ABC;
	}

	/**
	 * @param a1abc le a1ABC � d�finir
	 */
	public void setA1ABC(char a1abc)
	{
		A1ABC = a1abc;
	}

	/**
	 * @return le a1CND
	 */
	public int getA1CND()
	{
		return A1CND;
	}

	/**
	 * @param a1cnd le a1CND � d�finir
	 */
	public void setA1CND(int a1cnd)
	{
		A1CND = a1cnd;
	}

	/**
	 * @return le a1CND2
	 */
	public int getA1CND2()
	{
		return A1CND2;
	}

	/**
	 * @param a1cnd2 le a1CND2 � d�finir
	 */
	public void setA1CND2(int a1cnd2)
	{
		A1CND2 = a1cnd2;
	}

	/**
	 * @return le a1CND3
	 */
	public int getA1CND3()
	{
		return A1CND3;
	}

	/**
	 * @param a1cnd3 le a1CND3 � d�finir
	 */
	public void setA1CND3(int a1cnd3)
	{
		A1CND3 = a1cnd3;
	}

	/**
	 * @return le a1CND4
	 */
	public int getA1CND4()
	{
		return A1CND4;
	}

	/**
	 * @param a1cnd4 le a1CND4 � d�finir
	 */
	public void setA1CND4(int a1cnd4)
	{
		A1CND4 = a1cnd4;
	}

	/**
	 * @return le a1UNL
	 */
	public String getA1UNL()
	{
		return A1UNL;
	}

	/**
	 * @param a1unl le a1UNL � d�finir
	 */
	public void setA1UNL(String a1unl)
	{
		A1UNL = a1unl;
	}

	/**
	 * @return le a1UNL2
	 */
	public String getA1UNL2()
	{
		return A1UNL2;
	}

	/**
	 * @param a1unl2 le a1UNL2 � d�finir
	 */
	public void setA1UNL2(String a1unl2)
	{
		A1UNL2 = a1unl2;
	}

	/**
	 * @return le a1UNL3
	 */
	public String getA1UNL3()
	{
		return A1UNL3;
	}

	/**
	 * @param a1unl3 le a1UNL3 � d�finir
	 */
	public void setA1UNL3(String a1unl3)
	{
		A1UNL3 = a1unl3;
	}

	/**
	 * @return le a1UNL4
	 */
	public String getA1UNL4()
	{
		return A1UNL4;
	}

	/**
	 * @param a1unl4 le a1UNL4 � d�finir
	 */
	public void setA1UNL4(String a1unl4)
	{
		A1UNL4 = a1unl4;
	}

	/**
	 * @return le a1TRU2
	 */
	public char getA1TRU2()
	{
		return A1TRU2;
	}

	/**
	 * @param a1tru2 le a1TRU2 � d�finir
	 */
	public void setA1TRU2(char a1tru2)
	{
		A1TRU2 = a1tru2;
	}

	/**
	 * @return le a1TRU3
	 */
	public char getA1TRU3()
	{
		return A1TRU3;
	}

	/**
	 * @param a1tru3 le a1TRU3 � d�finir
	 */
	public void setA1TRU3(char a1tru3)
	{
		A1TRU3 = a1tru3;
	}

	/**
	 * @return le a1TRU4
	 */
	public char getA1TRU4()
	{
		return A1TRU4;
	}

	/**
	 * @param a1tru4 le a1TRU4 � d�finir
	 */
	public void setA1TRU4(char a1tru4)
	{
		A1TRU4 = a1tru4;
	}

	/**
	 * @return le a1PRI
	 */
	public int getA1PRI()
	{
		return A1PRI;
	}

	/**
	 * @param a1pri le a1PRI � d�finir
	 */
	public void setA1PRI(int a1pri)
	{
		A1PRI = a1pri;
	}

	/**
	 * @return le a1PFB
	 */
	public int getA1PFB()
	{
		return A1PFB;
	}

	/**
	 * @param a1pfb le a1PFB � d�finir
	 */
	public void setA1PFB(int a1pfb)
	{
		A1PFB = a1pfb;
	}

	/**
	 * @return le a1MDP
	 */
	public int getA1MDP()
	{
		return A1MDP;
	}

	/**
	 * @param a1mdp le a1MDP � d�finir
	 */
	public void setA1MDP(int a1mdp)
	{
		A1MDP = a1mdp;
	}

	/**
	 * @return le a1PDP
	 */
	public int getA1PDP()
	{
		return A1PDP;
	}

	/**
	 * @param a1pdp le a1PDP � d�finir
	 */
	public void setA1PDP(int a1pdp)
	{
		A1PDP = a1pdp;
	}

	/**
	 * @return le a1CDP
	 */
	public char getA1CDP()
	{
		return A1CDP;
	}

	/**
	 * @param a1cdp le a1CDP � d�finir
	 */
	public void setA1CDP(char a1cdp)
	{
		A1CDP = a1cdp;
	}

	/**
	 * @return le a1MAG
	 */
	public String getA1MAG()
	{
		return A1MAG;
	}

	/**
	 * @param a1mag le a1MAG � d�finir
	 */
	public void setA1MAG(String a1mag)
	{
		A1MAG = a1mag;
	}

	/**
	 * @return le a1MEX
	 */
	public String getA1MEX()
	{
		return A1MEX;
	}

	/**
	 * @param a1mex le a1MEX � d�finir
	 */
	public void setA1MEX(String a1mex)
	{
		A1MEX = a1mex;
	}

	/**
	 * @return le a1IN1
	 */
	public char getA1IN1()
	{
		return A1IN1;
	}

	/**
	 * @param a1in1 le a1IN1 � d�finir
	 */
	public void setA1IN1(char a1in1)
	{
		A1IN1 = a1in1;
	}

	/**
	 * @return le a1IN2
	 */
	public char getA1IN2()
	{
		return A1IN2;
	}

	/**
	 * @param a1in2 le a1IN2 � d�finir
	 */
	public void setA1IN2(char a1in2)
	{
		A1IN2 = a1in2;
	}

	/**
	 * @return le a1IN3
	 */
	public char getA1IN3()
	{
		return A1IN3;
	}

	/**
	 * @param a1in3 le a1IN3 � d�finir
	 */
	public void setA1IN3(char a1in3)
	{
		A1IN3 = a1in3;
	}

	/**
	 * @return le a1IN4
	 */
	public char getA1IN4()
	{
		return A1IN4;
	}

	/**
	 * @param a1in4 le a1IN4 � d�finir
	 */
	public void setA1IN4(char a1in4)
	{
		A1IN4 = a1in4;
	}

	/**
	 * @return le a1IN5
	 */
	public char getA1IN5()
	{
		return A1IN5;
	}

	/**
	 * @param a1in5 le a1IN5 � d�finir
	 */
	public void setA1IN5(char a1in5)
	{
		A1IN5 = a1in5;
	}

	/**
	 * @return le a1IN6
	 */
	public char getA1IN6()
	{
		return A1IN6;
	}

	/**
	 * @param a1in6 le a1IN6 � d�finir
	 */
	public void setA1IN6(char a1in6)
	{
		A1IN6 = a1in6;
	}

	/**
	 * @return le a1IN7
	 */
	public char getA1IN7()
	{
		return A1IN7;
	}

	/**
	 * @param a1in7 le a1IN7 � d�finir
	 */
	public void setA1IN7(char a1in7)
	{
		A1IN7 = a1in7;
	}

	/**
	 * @return le a1IN8
	 */
	public char getA1IN8()
	{
		return A1IN8;
	}

	/**
	 * @param a1in8 le a1IN8 � d�finir
	 */
	public void setA1IN8(char a1in8)
	{
		A1IN8 = a1in8;
	}

	/**
	 * @return le a1IN9
	 */
	public char getA1IN9()
	{
		return A1IN9;
	}

	/**
	 * @param a1in9 le a1IN9 � d�finir
	 */
	public void setA1IN9(char a1in9)
	{
		A1IN9 = a1in9;
	}

	/**
	 * @return le a1N11
	 */
	public int getA1N11()
	{
		return A1N11;
	}

	/**
	 * @param a1n11 le a1N11 � d�finir
	 */
	public void setA1N11(int a1n11)
	{
		A1N11 = a1n11;
	}

	/**
	 * @return le a1N12
	 */
	public int getA1N12()
	{
		return A1N12;
	}

	/**
	 * @param a1n12 le a1N12 � d�finir
	 */
	public void setA1N12(int a1n12)
	{
		A1N12 = a1n12;
	}

	/**
	 * @return le a1N13
	 */
	public int getA1N13()
	{
		return A1N13;
	}

	/**
	 * @param a1n13 le a1N13 � d�finir
	 */
	public void setA1N13(int a1n13)
	{
		A1N13 = a1n13;
	}

	/**
	 * @return le a1N51
	 */
	public int getA1N51()
	{
		return A1N51;
	}

	/**
	 * @param a1n51 le a1N51 � d�finir
	 */
	public void setA1N51(int a1n51)
	{
		A1N51 = a1n51;
	}

	/**
	 * @return le a1N71
	 */
	public int getA1N71()
	{
		return A1N71;
	}

	/**
	 * @param a1n71 le a1N71 � d�finir
	 */
	public void setA1N71(int a1n71)
	{
		A1N71 = a1n71;
	}

	/**
	 * @return le a1DAT1
	 */
	public int getA1DAT1()
	{
		return A1DAT1;
	}

	/**
	 * @param a1dat1 le a1DAT1 � d�finir
	 */
	public void setA1DAT1(int a1dat1)
	{
		A1DAT1 = a1dat1;
	}

	/**
	 * @return le a1DAT2
	 */
	public int getA1DAT2()
	{
		return A1DAT2;
	}

	/**
	 * @param a1dat2 le a1DAT2 � d�finir
	 */
	public void setA1DAT2(int a1dat2)
	{
		A1DAT2 = a1dat2;
	}

	/**
	 * @return le a1DAT3
	 */
	public int getA1DAT3()
	{
		return A1DAT3;
	}

	/**
	 * @param a1dat3 le a1DAT3 � d�finir
	 */
	public void setA1DAT3(int a1dat3)
	{
		A1DAT3 = a1dat3;
	}

	/**
	 * @return le a1EDT1
	 */
	public char getA1EDT1()
	{
		return A1EDT1;
	}

	/**
	 * @param a1edt1 le a1EDT1 � d�finir
	 */
	public void setA1EDT1(char a1edt1)
	{
		A1EDT1 = a1edt1;
	}

	/**
	 * @return le a1EDT2
	 */
	public char getA1EDT2()
	{
		return A1EDT2;
	}

	/**
	 * @param a1edt2 le a1EDT2 � d�finir
	 */
	public void setA1EDT2(char a1edt2)
	{
		A1EDT2 = a1edt2;
	}

	/**
	 * @return le a1EDT3
	 */
	public char getA1EDT3()
	{
		return A1EDT3;
	}

	/**
	 * @param a1edt3 le a1EDT3 � d�finir
	 */
	public void setA1EDT3(char a1edt3)
	{
		A1EDT3 = a1edt3;
	}

	/**
	 * @return le a1EDT4
	 */
	public char getA1EDT4()
	{
		return A1EDT4;
	}

	/**
	 * @param a1edt4 le a1EDT4 � d�finir
	 */
	public void setA1EDT4(char a1edt4)
	{
		A1EDT4 = a1edt4;
	}

	/**
	 * @return le a1EDT5
	 */
	public char getA1EDT5()
	{
		return A1EDT5;
	}

	/**
	 * @param a1edt5 le a1EDT5 � d�finir
	 */
	public void setA1EDT5(char a1edt5)
	{
		A1EDT5 = a1edt5;
	}

	/**
	 * @return le a1EDT6
	 */
	public char getA1EDT6()
	{
		return A1EDT6;
	}

	/**
	 * @param a1edt6 le a1EDT6 � d�finir
	 */
	public void setA1EDT6(char a1edt6)
	{
		A1EDT6 = a1edt6;
	}

	/**
	 * @return le a1EDT7
	 */
	public char getA1EDT7()
	{
		return A1EDT7;
	}

	/**
	 * @param a1edt7 le a1EDT7 � d�finir
	 */
	public void setA1EDT7(char a1edt7)
	{
		A1EDT7 = a1edt7;
	}

	/**
	 * @return le a1EDT8
	 */
	public char getA1EDT8()
	{
		return A1EDT8;
	}

	/**
	 * @param a1edt8 le a1EDT8 � d�finir
	 */
	public void setA1EDT8(char a1edt8)
	{
		A1EDT8 = a1edt8;
	}

	/**
	 * @return le a1EDT9
	 */
	public char getA1EDT9()
	{
		return A1EDT9;
	}

	/**
	 * @param a1edt9 le a1EDT9 � d�finir
	 */
	public void setA1EDT9(char a1edt9)
	{
		A1EDT9 = a1edt9;
	}

	/**
	 * @return le a1UCS
	 */
	public String getA1UCS()
	{
		return A1UCS;
	}

	/**
	 * @param a1ucs le a1UCS � d�finir
	 */
	public void setA1UCS(String a1ucs)
	{
		A1UCS = a1ucs;
	}

	/**
	 * @return le a1KCS
	 */
	public int getA1KCS()
	{
		return A1KCS;
	}

	/**
	 * @param a1kcs le a1KCS � d�finir
	 */
	public void setA1KCS(int a1kcs)
	{
		A1KCS = a1kcs;
	}

	/**
	 * @return le a1SFA
	 */
	public String getA1SFA()
	{
		return A1SFA;
	}

	/**
	 * @param a1sfa le a1SFA � d�finir
	 */
	public void setA1SFA(String a1sfa)
	{
		A1SFA = a1sfa;
	}

	/**
	 * @return le a1IN10
	 */
	public char getA1IN10()
	{
		return A1IN10;
	}

	/**
	 * @param a1in10 le a1IN10 � d�finir
	 */
	public void setA1IN10(char a1in10)
	{
		A1IN10 = a1in10;
	}

	/**
	 * @return le a1IN11
	 */
	public char getA1IN11()
	{
		return A1IN11;
	}

	/**
	 * @param a1in11 le a1IN11 � d�finir
	 */
	public void setA1IN11(char a1in11)
	{
		A1IN11 = a1in11;
	}

	/**
	 * @return le a1IN12
	 */
	public char getA1IN12()
	{
		return A1IN12;
	}

	/**
	 * @param a1in12 le a1IN12 � d�finir
	 */
	public void setA1IN12(char a1in12)
	{
		A1IN12 = a1in12;
	}

	/**
	 * @return le a1IN13
	 */
	public char getA1IN13()
	{
		return A1IN13;
	}

	/**
	 * @param a1in13 le a1IN13 � d�finir
	 */
	public void setA1IN13(char a1in13)
	{
		A1IN13 = a1in13;
	}

	/**
	 * @return le a1IN14
	 */
	public char getA1IN14()
	{
		return A1IN14;
	}

	/**
	 * @param a1in14 le a1IN14 � d�finir
	 */
	public void setA1IN14(char a1in14)
	{
		A1IN14 = a1in14;
	}

	/**
	 * @return le a1IN15
	 */
	public char getA1IN15()
	{
		return A1IN15;
	}

	/**
	 * @param a1in15 le a1IN15 � d�finir
	 */
	public void setA1IN15(char a1in15)
	{
		A1IN15 = a1in15;
	}

	/**
	 * @return le a1REF1
	 */
	public String getA1REF1()
	{
		return A1REF1;
	}

	/**
	 * @param a1ref1 le a1REF1 � d�finir
	 */
	public void setA1REF1(String a1ref1)
	{
		A1REF1 = a1ref1;
	}

	/**
	 * @return le a1CCO
	 */
	public int getA1CCO()
	{
		return A1CCO;
	}

	/**
	 * @param a1cco le a1CCO � d�finir
	 */
	public void setA1CCO(int a1cco)
	{
		A1CCO = a1cco;
	}

	/**
	 * @return le a1REC
	 */
	public char getA1REC()
	{
		return A1REC;
	}

	/**
	 * @param a1rec le a1REC � d�finir
	 */
	public void setA1REC(char a1rec)
	{
		A1REC = a1rec;
	}

	/**
	 * @return le a1SV1
	 */
	public String getA1SV1()
	{
		return A1SV1;
	}

	/**
	 * @param a1sv1 le a1SV1 � d�finir
	 */
	public void setA1SV1(String a1sv1)
	{
		A1SV1 = a1sv1;
	}

	/**
	 * @return le a1SV2
	 */
	public String getA1SV2()
	{
		return A1SV2;
	}

	/**
	 * @param a1sv2 le a1SV2 � d�finir
	 */
	public void setA1SV2(String a1sv2)
	{
		A1SV2 = a1sv2;
	}

	/**
	 * @return le a1UNP
	 */
	public String getA1UNP()
	{
		return A1UNP;
	}

	/**
	 * @param a1unp le a1UNP � d�finir
	 */
	public void setA1UNP(String a1unp)
	{
		A1UNP = a1unp;
	}

	/**
	 * @return le a1IN16
	 */
	public char getA1IN16()
	{
		return A1IN16;
	}

	/**
	 * @param a1in16 le a1IN16 � d�finir
	 */
	public void setA1IN16(char a1in16)
	{
		A1IN16 = a1in16;
	}

	/**
	 * @return le a1IN17
	 */
	public char getA1IN17()
	{
		return A1IN17;
	}

	/**
	 * @param a1in17 le a1IN17 � d�finir
	 */
	public void setA1IN17(char a1in17)
	{
		A1IN17 = a1in17;
	}

	/**
	 * @return le a1UNT
	 */
	public String getA1UNT()
	{
		return A1UNT;
	}

	/**
	 * @param a1unt le a1UNT � d�finir
	 */
	public void setA1UNT(String a1unt)
	{
		A1UNT = a1unt;
	}

	/**
	 * @return le a1KTS
	 */
	public int getA1KTS()
	{
		return A1KTS;
	}

	/**
	 * @param a1kts le a1KTS � d�finir
	 */
	public void setA1KTS(int a1kts)
	{
		A1KTS = a1kts;
	}

	/**
	 * @return le a1LNG
	 */
	public int getA1LNG()
	{
		return A1LNG;
	}

	/**
	 * @param a1lng le a1LNG � d�finir
	 */
	public void setA1LNG(int a1lng)
	{
		A1LNG = a1lng;
	}

	/**
	 * @return le a1LRG
	 */
	public int getA1LRG()
	{
		return A1LRG;
	}

	/**
	 * @param a1lrg le a1LRG � d�finir
	 */
	public void setA1LRG(int a1lrg)
	{
		A1LRG = a1lrg;
	}

	/**
	 * @return le a1HTR
	 */
	public int getA1HTR()
	{
		return A1HTR;
	}

	/**
	 * @param a1htr le a1HTR � d�finir
	 */
	public void setA1HTR(int a1htr)
	{
		A1HTR = a1htr;
	}

	/**
	 * @return le a1PDSL
	 */
	public int getA1PDSL()
	{
		return A1PDSL;
	}

	/**
	 * @param a1pdsl le a1PDSL � d�finir
	 */
	public void setA1PDSL(int a1pdsl)
	{
		A1PDSL = a1pdsl;
	}

	/**
	 * @return le a1VOLL
	 */
	public int getA1VOLL()
	{
		return A1VOLL;
	}

	/**
	 * @param a1voll le a1VOLL � d�finir
	 */
	public void setA1VOLL(int a1voll)
	{
		A1VOLL = a1voll;
	}

	/**
	 * @return le a1TDUO
	 */
	public char getA1TDUO()
	{
		return A1TDUO;
	}

	/**
	 * @param a1tduo le a1TDUO � d�finir
	 */
	public void setA1TDUO(char a1tduo)
	{
		A1TDUO = a1tduo;
	}

	/**
	 * @return le a1DUO1
	 */
	public int getA1DUO1()
	{
		return A1DUO1;
	}

	/**
	 * @param a1duo1 le a1DUO1 � d�finir
	 */
	public void setA1DUO1(int a1duo1)
	{
		A1DUO1 = a1duo1;
	}

	/**
	 * @return le a1DUO2
	 */
	public int getA1DUO2()
	{
		return A1DUO2;
	}

	/**
	 * @param a1duo2 le a1DUO2 � d�finir
	 */
	public void setA1DUO2(int a1duo2)
	{
		A1DUO2 = a1duo2;
	}

	/**
	 * @return le a1DUO3
	 */
	public int getA1DUO3()
	{
		return A1DUO3;
	}

	/**
	 * @param a1duo3 le a1DUO3 � d�finir
	 */
	public void setA1DUO3(int a1duo3)
	{
		A1DUO3 = a1duo3;
	}

	/**
	 * @return le a1PDN
	 */
	public int getA1PDN()
	{
		return A1PDN;
	}

	/**
	 * @param a1pdn le a1PDN � d�finir
	 */
	public void setA1PDN(int a1pdn)
	{
		A1PDN = a1pdn;
	}

	/**
	 * @return le a1NDP
	 */
	public String getA1NDP()
	{
		return A1NDP;
	}

	/**
	 * @param a1ndp le a1NDP � d�finir
	 */
	public void setA1NDP(String a1ndp)
	{
		A1NDP = a1ndp;
	}

	/**
	 * @return le a1OPA
	 */
	public String getA1OPA()
	{
		return A1OPA;
	}

	/**
	 * @param a1opa le a1OPA � d�finir
	 */
	public void setA1OPA(String a1opa)
	{
		A1OPA = a1opa;
	}

	/**
	 * @return le a1IN18
	 */
	public char getA1IN18()
	{
		return A1IN18;
	}

	/**
	 * @param a1in18 le a1IN18 � d�finir
	 */
	public void setA1IN18(char a1in18)
	{
		A1IN18 = a1in18;
	}

	/**
	 * @return le a1IN19
	 */
	public char getA1IN19()
	{
		return A1IN19;
	}

	/**
	 * @param a1in19 le a1IN19 � d�finir
	 */
	public void setA1IN19(char a1in19)
	{
		A1IN19 = a1in19;
	}

	/**
	 * @return le a1IN20
	 */
	public char getA1IN20()
	{
		return A1IN20;
	}

	/**
	 * @param a1in20 le a1IN20 � d�finir
	 */
	public void setA1IN20(char a1in20)
	{
		A1IN20 = a1in20;
	}

	/**
	 * @return le a1IN21
	 */
	public char getA1IN21()
	{
		return A1IN21;
	}

	/**
	 * @param a1in21 le a1IN21 � d�finir
	 */
	public void setA1IN21(char a1in21)
	{
		A1IN21 = a1in21;
	}

	/**
	 * @return le a1IN22
	 */
	public char getA1IN22()
	{
		return A1IN22;
	}

	/**
	 * @param a1in22 le a1IN22 � d�finir
	 */
	public void setA1IN22(char a1in22)
	{
		A1IN22 = a1in22;
	}

	/**
	 * @return le a1IN23
	 */
	public char getA1IN23()
	{
		return A1IN23;
	}

	/**
	 * @param a1in23 le a1IN23 � d�finir
	 */
	public void setA1IN23(char a1in23)
	{
		A1IN23 = a1in23;
	}

	/**
	 * @return le a1IN24
	 */
	public char getA1IN24()
	{
		return A1IN24;
	}

	/**
	 * @param a1in24 le a1IN24 � d�finir
	 */
	public void setA1IN24(char a1in24)
	{
		A1IN24 = a1in24;
	}

	/**
	 * @return le a1IN25
	 */
	public char getA1IN25()
	{
		return A1IN25;
	}

	/**
	 * @param a1in25 le a1IN25 � d�finir
	 */
	public void setA1IN25(char a1in25)
	{
		A1IN25 = a1in25;
	}

	/**
	 * @return le a1DAT4
	 */
	public int getA1DAT4()
	{
		return A1DAT4;
	}

	/**
	 * @param a1dat4 le a1DAT4 � d�finir
	 */
	public void setA1DAT4(int a1dat4)
	{
		A1DAT4 = a1dat4;
	}

	/**
	 * @return le a1DAT5
	 */
	public int getA1DAT5()
	{
		return A1DAT5;
	}

	/**
	 * @param a1dat5 le a1DAT5 � d�finir
	 */
	public void setA1DAT5(int a1dat5)
	{
		A1DAT5 = a1dat5;
	}

	/**
	 * @return le a1DAT6
	 */
	public int getA1DAT6()
	{
		return A1DAT6;
	}

	/**
	 * @param a1dat6 le a1DAT6 � d�finir
	 */
	public void setA1DAT6(int a1dat6)
	{
		A1DAT6 = a1dat6;
	}

	/**
	 * @return le a1QT1
	 */
	public int getA1QT1()
	{
		return A1QT1;
	}

	/**
	 * @param a1qt1 le a1QT1 � d�finir
	 */
	public void setA1QT1(int a1qt1)
	{
		A1QT1 = a1qt1;
	}

	/**
	 * @return le a1QT2
	 */
	public int getA1QT2()
	{
		return A1QT2;
	}

	/**
	 * @param a1qt2 le a1QT2 � d�finir
	 */
	public void setA1QT2(int a1qt2)
	{
		A1QT2 = a1qt2;
	}

	/**
	 * @return le a1QT3
	 */
	public int getA1QT3()
	{
		return A1QT3;
	}

	/**
	 * @param a1qt3 le a1QT3 � d�finir
	 */
	public void setA1QT3(int a1qt3)
	{
		A1QT3 = a1qt3;
	}

	/**
	 * @return le a1QT4
	 */
	public int getA1QT4()
	{
		return A1QT4;
	}

	/**
	 * @param a1qt4 le a1QT4 � d�finir
	 */
	public void setA1QT4(int a1qt4)
	{
		A1QT4 = a1qt4;
	}

	/**
	 * @return le a1QT5
	 */
	public int getA1QT5()
	{
		return A1QT5;
	}

	/**
	 * @param a1qt5 le a1QT5 � d�finir
	 */
	public void setA1QT5(int a1qt5)
	{
		A1QT5 = a1qt5;
	}

	/**
	 * @return le a1UNL5
	 */
	public String getA1UNL5()
	{
		return A1UNL5;
	}

	/**
	 * @param a1unl5 le a1UNL5 � d�finir
	 */
	public void setA1UNL5(String a1unl5)
	{
		A1UNL5 = a1unl5;
	}

	/**
	 * @return le a1CND5
	 */
	public int getA1CND5()
	{
		return A1CND5;
	}

	/**
	 * @param a1cnd5 le a1CND5 � d�finir
	 */
	public void setA1CND5(int a1cnd5)
	{
		A1CND5 = a1cnd5;
	}

	/**
	 * @return le a1QT6
	 */
	public int getA1QT6()
	{
		return A1QT6;
	}

	/**
	 * @param a1qt6 le a1QT6 � d�finir
	 */
	public void setA1QT6(int a1qt6)
	{
		A1QT6 = a1qt6;
	}

	/**
	 * @return le a1ZP20
	 */
	public String getA1ZP20()
	{
		return A1ZP20;
	}

	/**
	 * @param a1zp20 le a1ZP20 � d�finir
	 */
	public void setA1ZP20(String a1zp20)
	{
		A1ZP20 = a1zp20;
	}

	/**
	 * @return le a1ZP21
	 */
	public String getA1ZP21()
	{
		return A1ZP21;
	}

	/**
	 * @param a1zp21 le a1ZP21 � d�finir
	 */
	public void setA1ZP21(String a1zp21)
	{
		A1ZP21 = a1zp21;
	}

	/**
	 * @return le a1PRS
	 */
	public int getA1PRS()
	{
		return A1PRS;
	}

	/**
	 * @param a1prs le a1PRS � d�finir
	 */
	public void setA1PRS(int a1prs)
	{
		A1PRS = a1prs;
	}

	/**
	 * @return le a1TNS
	 */
	public String getA1TNS()
	{
		return A1TNS;
	}

	/**
	 * @param a1tns le a1TNS � d�finir
	 */
	public void setA1TNS(String a1tns)
	{
		A1TNS = a1tns;
	}

	/**
	 * @return le a1IN26
	 */
	public char getA1IN26()
	{
		return A1IN26;
	}

	/**
	 * @param a1in26 le a1IN26 � d�finir
	 */
	public void setA1IN26(char a1in26)
	{
		A1IN26 = a1in26;
	}

	/**
	 * @return le a1IN27
	 */
	public char getA1IN27()
	{
		return A1IN27;
	}

	/**
	 * @param a1in27 le a1IN27 � d�finir
	 */
	public void setA1IN27(char a1in27)
	{
		A1IN27 = a1in27;
	}

	/**
	 * @return le a1IN28
	 */
	public char getA1IN28()
	{
		return A1IN28;
	}

	/**
	 * @param a1in28 le a1IN28 � d�finir
	 */
	public void setA1IN28(char a1in28)
	{
		A1IN28 = a1in28;
	}

	/**
	 * @return le a1IN29
	 */
	public char getA1IN29()
	{
		return A1IN29;
	}

	/**
	 * @param a1in29 le a1IN29 � d�finir
	 */
	public void setA1IN29(char a1in29)
	{
		A1IN29 = a1in29;
	}

	/**
	 * @return le a1IN30
	 */
	public char getA1IN30()
	{
		return A1IN30;
	}

	/**
	 * @param a1in30 le a1IN30 � d�finir
	 */
	public void setA1IN30(char a1in30)
	{
		A1IN30 = a1in30;
	}

	/**
	 * @return le a1IN31
	 */
	public char getA1IN31()
	{
		return A1IN31;
	}

	/**
	 * @param a1in31 le a1IN31 � d�finir
	 */
	public void setA1IN31(char a1in31)
	{
		A1IN31 = a1in31;
	}

	/**
	 * @return le a1IN32
	 */
	public char getA1IN32()
	{
		return A1IN32;
	}

	/**
	 * @param a1in32 le a1IN32 � d�finir
	 */
	public void setA1IN32(char a1in32)
	{
		A1IN32 = a1in32;
	}

	/**
	 * @return le a1IN33
	 */
	public char getA1IN33()
	{
		return A1IN33;
	}

	/**
	 * @param a1in33 le a1IN33 � d�finir
	 */
	public void setA1IN33(char a1in33)
	{
		A1IN33 = a1in33;
	}

	/**
	 * @return le a1IN34
	 */
	public char getA1IN34()
	{
		return A1IN34;
	}

	/**
	 * @param a1in34 le a1IN34 � d�finir
	 */
	public void setA1IN34(char a1in34)
	{
		A1IN34 = a1in34;
	}

	/**
	 * @return le a1IN35
	 */
	public char getA1IN35()
	{
		return A1IN35;
	}

	/**
	 * @param a1in35 le a1IN35 � d�finir
	 */
	public void setA1IN35(char a1in35)
	{
		A1IN35 = a1in35;
	}

	/**
	 * @return le a1MOT1
	 */
	public String getA1MOT1()
	{
		return A1MOT1;
	}

	/**
	 * @param a1mot1 le a1MOT1 � d�finir
	 */
	public void setA1MOT1(String a1mot1)
	{
		A1MOT1 = a1mot1;
	}

	/**
	 * @return le a1MOT2
	 */
	public String getA1MOT2()
	{
		return A1MOT2;
	}

	/**
	 * @param a1mot2 le a1MOT2 � d�finir
	 */
	public void setA1MOT2(String a1mot2)
	{
		A1MOT2 = a1mot2;
	}

	/**
	 * @return le a1MOT3
	 */
	public String getA1MOT3()
	{
		return A1MOT3;
	}

	/**
	 * @param a1mot3 le a1MOT3 � d�finir
	 */
	public void setA1MOT3(String a1mot3)
	{
		A1MOT3 = a1mot3;
	}

	/**
	 * @return le a1OBS
	 */
	public String getA1OBS()
	{
		return A1OBS;
	}

	/**
	 * @param a1obs le a1OBS � d�finir
	 */
	public void setA1OBS(String a1obs)
	{
		A1OBS = a1obs;
	}

	/**
	 * @return le a1CNV1
	 */
	public String getA1CNV1()
	{
		return A1CNV1;
	}

	/**
	 * @param a1cnv1 le a1CNV1 � d�finir
	 */
	public void setA1CNV1(String a1cnv1)
	{
		A1CNV1 = a1cnv1;
	}

	/**
	 * @return le a1CNV2
	 */
	public String getA1CNV2()
	{
		return A1CNV2;
	}

	/**
	 * @param a1cnv2 le a1CNV2 � d�finir
	 */
	public void setA1CNV2(String a1cnv2)
	{
		A1CNV2 = a1cnv2;
	}

	/**
	 * @return le a1CNV3
	 */
	public String getA1CNV3()
	{
		return A1CNV3;
	}

	/**
	 * @param a1cnv3 le a1CNV3 � d�finir
	 */
	public void setA1CNV3(String a1cnv3)
	{
		A1CNV3 = a1cnv3;
	}

	/**
	 * @return le a1PRI1
	 */
	public int getA1PRI1()
	{
		return A1PRI1;
	}

	/**
	 * @param a1pri1 le a1PRI1 � d�finir
	 */
	public void setA1PRI1(int a1pri1)
	{
		A1PRI1 = a1pri1;
	}

	/**
	 * @return le a1PRI2
	 */
	public int getA1PRI2()
	{
		return A1PRI2;
	}

	/**
	 * @param a1pri2 le a1PRI2 � d�finir
	 */
	public void setA1PRI2(int a1pri2)
	{
		A1PRI2 = a1pri2;
	}

	/**
	 * @return le a1DAT7
	 */
	public int getA1DAT7()
	{
		return A1DAT7;
	}

	/**
	 * @param a1dat7 le a1DAT7 � d�finir
	 */
	public void setA1DAT7(int a1dat7)
	{
		A1DAT7 = a1dat7;
	}

	/**
	 * @return le a1DAT8
	 */
	public int getA1DAT8()
	{
		return A1DAT8;
	}

	/**
	 * @param a1dat8 le a1DAT8 � d�finir
	 */
	public void setA1DAT8(int a1dat8)
	{
		A1DAT8 = a1dat8;
	}

	/**
	 * @return le a1ZON1
	 */
	public String getA1ZON1()
	{
		return A1ZON1;
	}

	/**
	 * @param a1zon1 le a1ZON1 � d�finir
	 */
	public void setA1ZON1(String a1zon1)
	{
		A1ZON1 = a1zon1;
	}

	/**
	 * @return le a1ZON2
	 */
	public String getA1ZON2()
	{
		return A1ZON2;
	}

	/**
	 * @param a1zon2 le a1ZON2 � d�finir
	 */
	public void setA1ZON2(String a1zon2)
	{
		A1ZON2 = a1zon2;
	}
}
