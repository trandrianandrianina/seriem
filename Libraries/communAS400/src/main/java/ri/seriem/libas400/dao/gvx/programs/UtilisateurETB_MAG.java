package ri.seriem.libas400.dao.gvx.programs;

import java.io.IOException;

import ri.seriem.libas400.dao.gvx.database.PgvmparmManager;
import ri.seriem.libas400.dao.gvx.database.PgvmsecmManager;
import ri.seriem.libas400.database.record.ExtendRecord;
import ri.seriem.libas400.parameters.Etablissement;
import ri.seriem.libas400.parameters.Magasin;
import ri.seriem.libas400.parameters.Societe;
import ri.seriem.libas400.system.SystemManager;

import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

public class UtilisateurETB_MAG
{
	private SystemManager systeme=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public UtilisateurETB_MAG(SystemManager system)
	{
		systeme = system;
	}
	
	/**
	 * Contr�le la pr�sence du module GVM via l'existence du fichier PGVMPARM (m�me si c'est pas la meilleure m�thode)
	 * @param curlib
	 * @return
	 */
	private boolean isGVMisPresent(String curlib)
	{
		IFSFile pgvmparm = new IFSFile(systeme.getSystem(), QSYSObjectPathName.toPath(curlib, "PGVMPARM", "FILE"));
		try
		{
			return pgvmparm.exists();
		}
		catch (IOException e)
		{
			return false;
		}
	}
	
	/**
	 * Retourne l'etablissement d'un utilisateur
	 * @param user
	 * @param curlib
	 * @return
	 */
	public String getEtablissement(String user, String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return "";
		
		if (!isGVMisPresent(curlib)) return "";
		
		PgvmsecmManager security = new PgvmsecmManager(systeme.getdatabase().getConnection());
		security.setLibrary(curlib);
		PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getdatabase().getConnection());
		pgvmparm.setLibrary(curlib);
	
		// Lecture de la DG blanche
		Societe infosSociete = new Societe();
		pgvmparm.getInfosSociete(infosSociete);
		
		// On recherche l'�tablissement de l'utilisateur
		Etablissement etbuser = new Etablissement();
		etbuser.setCodeEtablissement(security.getEtablissementUser(user));
		// Il en a pas, on prend celui de la DG blanche
		if (etbuser.getCodeEtablissement() == null)
		{
			//System.out.println("Pas de ETB USER, donc on prend celui de la DG: " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale() + " " + infosSociete.getEtablissementPilote().getMagasinGeneral());
			etbuser.setCodeEtablissement(infosSociete.getCodeEtablissementPilote());
		}
		return etbuser.getCodeEtablissement();
	}

	
	/**
	 * Retourne les informations pour la barre de titre de la fen�tre de S�rie N
	 * @return
	 */
	public String getTitleBarInfos(String user, String curlib)
	{
		if ((curlib == null) || (curlib.trim().equals(""))) return "";
		
		if (!isGVMisPresent(curlib)) return "";
		
		PgvmsecmManager security = new PgvmsecmManager(systeme.getdatabase().getConnection());
		security.setLibrary(curlib);
		PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getdatabase().getConnection());
		pgvmparm.setLibrary(curlib);
	
		// Lecture de la DG blanche
		Societe infosSociete = new Societe();
		pgvmparm.getInfosSociete(infosSociete);
		
		// On recherche l'�tablissement de l'utilisateur
		Etablissement etbuser = new Etablissement();
		etbuser.setCodeEtablissement(security.getEtablissementUser(user));
		// Il en a pas, on prend celui de la DG blanche
		if (etbuser.getCodeEtablissement() == null)
		{
			//System.out.println("Pas de ETB USER, donc on prend celui de la DG: " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale() + " " + infosSociete.getEtablissementPilote().getMagasinGeneral());
			etbuser.setCodeEtablissement(infosSociete.getCodeEtablissementPilote());
		}
		// On va r�cup�rer les informations compl�mentaires de l'�tablissement
		pgvmparm.getInfosEtablissement(etbuser);

		// R�cup�ration des infos sur le magasin
		Magasin magasin = new Magasin(); 
		ExtendRecord record = security.getRecordsbyUSERandETB(user, etbuser.getCodeEtablissement());
		// De l'�tablissement si le user n'en a pas 
		if (record == null)
			magasin.setCodeMag(etbuser.getMagasinGeneral());
		else
			magasin.setCodeMag((String) record.getField("SEMAUS"));
		// R�cup�ration du libell� du code magasin
		pgvmparm.getInfosMagasin(etbuser.getCodeEtablissement(), magasin);

		String mag = magasin.getCodeMag() + " " + magasin.getMALIB();
		if (mag.trim().equals(""))
			return " - " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale();
		return " - " + infosSociete.getCodeEtablissementPilote() + " " + infosSociete.getRaisonSociale() + " - " + mag;
	}
}
