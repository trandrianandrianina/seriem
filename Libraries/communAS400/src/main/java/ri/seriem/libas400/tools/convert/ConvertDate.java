//=================================================================================================
//==>                                                                       04/06/2013 - 20/10/2015
//==> Permet de convertir une date au format JJ.MM.AA en 0/1AAMMJJ et inversement   
//=================================================================================================
package ri.seriem.libas400.tools.convert;

import ri.seriem.libcommun.outils.Constantes;

public class ConvertDate
{
	// Constantes
	public final static char SEPARATOR='.';
	

	/**
	 * Converti une date de type JJ.MM.AA ou JJ.MM.AAAA en 0/1AAMMJJ 
	 * @param date
	 * @return
	 */
	public static String toDb2(String date)
	{
		return toDb2(date, "\\"+SEPARATOR);
	}

	/**
	 * Converti une date de type JJ.MM.AA ou JJ.MM.AAAA en 0/1AAMMJJ 
	 * @param date
	 * @return
	 */
	public static String toDb2(String date, String separator)
	{
		if (date == null) return "";
		
		String[] decoupage = date.split(separator);
		if (decoupage.length != 3) return "";
		
		StringBuilder newdate = new StringBuilder(7);
		
		// Si�cle
		if (decoupage[2].length() < 4) // Dans le cas o� c'est cod� en AA
		{
			int siecle = Integer.parseInt(decoupage[2]);
			if (siecle < 30)
				newdate.append('1');
			else
				newdate.append('0');
			newdate.append(decoupage[2]);
		}
		else  // Dans le cas o� c'est cod� en AAAA
		{
			if (decoupage[2].charAt(0) == '2')
				newdate.append('1');
			else
				newdate.append('0');
			newdate.append(decoupage[2].substring(2));
		}
		
		// Mois (TODO ajouter un contr�le sur le nombre <= 12)
		newdate.append(decoupage[1]);
				
		// Jour (TODO ajouter un contr�le sur le nombre <= 31)
		newdate.append(decoupage[0]);
			
		return newdate.toString();
	}
	

	/**
	 * Converti une date de type 0/1AAMMJJ en JJ.MM.AA ou JJ.MM.AAAA 
	 * @param date
	 * @param complete
	 * @return
	 */
	public static String toNormal(String date, boolean complete)
	{
		return toNormal(date, SEPARATOR, complete);
	}

	/**
	 * Converti une date de type 0/1AAMMJJ en JJ.MM.AA ou JJ.MM.AAAA
	 * @param date
	 * @param separator
	 * @param complete
	 * @return
	 */
	public static String toNormal(String date, char separator, boolean complete)
	{
		if ((date == null) || (date.length() < 6)) return "";

		StringBuilder newdate = new StringBuilder(9);

		if (date.length() == 6)
		{
			// Jour
			newdate.append(date.substring(4)).append(separator);
			// Mois
			newdate.append(date.substring(2, 4)).append(separator);
			// Si�cle
			newdate.append(date.substring(0, 2));
		}
		else
			if (date.length() == 7)
			{
				// Jour
				newdate.append(date.substring(5)).append(separator);
				// Mois
				newdate.append(date.substring(3, 5)).append(separator);
				// Si�cle
				newdate.append(date.substring(1, 3));
			}

		// Si besoin on met le si�cle sur 4 chiffres
		if (complete)
		{
			if (date.length() == 6)
				newdate.insert(6, "19");
			else
				if (date.length() == 7)
					newdate.insert(6, date.charAt(0)=='1'?"20":"19");
		}
		
		return newdate.toString();
	}
	
	/**
	 * Contr�le que la chaine est un num�rique
	 * @param chaine
	 * @return
	 */
	public static boolean isNumeric(String chaine)
	{
		if( chaine == null ) return false;
		
		StringBuilder sb = new StringBuilder(chaine.trim());
		for (int i=0; i<sb.length(); i++)
			if( (sb.charAt(i) != '0') && (sb.charAt(i) != '1') && (sb.charAt(i) != '2') && (sb.charAt(i) != '3') &&
				(sb.charAt(i) != '4') && (sb.charAt(i) != '5') && (sb.charAt(i) != '6') && (sb.charAt(i) != '7') &&
				(sb.charAt(i) != '8') && (sb.charAt(i) != '9') && (sb.charAt(i) != Constantes.SEPARATEUR_DECIMAL_CHAR) &&
				(sb.charAt(i) != Constantes.SEPARATEUR_MILLIER_CHAR))
				return false;
		return true;
	}

	/**
	 * Converti une date pour db2 (S�rie N)
	 * @param date
	 * @return
	 */
	public static int convertDate4DataBase(String date)
	{
		if( date.isEmpty() || ( (date.length() == 1) && (date.charAt(0)) == '0') ) return 0;
		if( date.length() < 8 ) return -1;
		
		StringBuilder sb = new StringBuilder(7);
		if( date.length() == 10 ){
			if( date.charAt(6) == '2')
				sb.append('1');
			sb.append( date.substring(8) );
		} else if( date.length() == 8 ){ 
			sb.append('1');	// Si date sur 8 alors on part du principe que l'on est au dela de l'an 2000
			sb.append( date.substring(6) );
		}
		sb.append( date.substring(3, 5) );
		sb.append( date.substring(0, 2) );

		try
		{
			return Integer.parseInt( sb.toString() );
		}
		catch (Exception e)
		{
			return -1;
		}
	}

	/**
	 * Converti une heure pour db2 (S�rie N)
	 * @param heure
	 * @return
	 */
	public static int convertHeure4DataBase(String heure)
	{
//System.out.println("-Heure->" + heure);		
		if( heure.isEmpty() || ( (heure.length() == 1) && (heure.charAt(0)) == '0') ) return 0;
		StringBuilder sb = new StringBuilder(heure);
		if( heure.length() == 3 ){
			sb.insert(0, '0');
		} else if( heure.length() == 2 ){
			sb.append("00");
		} else if( heure.length() == 1 ){
			sb.insert(0, '0').append("00");
		} else if( heure.length() >= 5 ){
			sb.deleteCharAt(2);
		}
//System.out.println("-Heure->" + sb.toString());		

		try
		{
			return Integer.parseInt( sb.toString() );
		}
		catch (Exception e)
		{
			return -1;
		}
	}

}
