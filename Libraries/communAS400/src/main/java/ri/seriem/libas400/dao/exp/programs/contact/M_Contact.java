package ri.seriem.libas400.dao.exp.programs.contact;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.seriem.libas400.dao.exp.database.files.FFD_Psemrtem;
import ri.seriem.libas400.dao.exp.programs.parametres.GM_PersonnalisationEXP;
import ri.seriem.libas400.database.QueryManager;

/**
 * Classe contenant les zones du PSEMRTEM
 * @author Administrateur
 *
 */
public class M_Contact extends FFD_Psemrtem
{
	// Variables de travail
	private int			codeHierarchy	= M_LienEvenementContact.NO_HIERARCHY;
	private String		codeTypeContact	= null;	// TODO � voir si pas mieux de mettre createur par defaut
	private String		etablissement	= "";
	
	private LinkedHashMap<String, String>		listCivility			= null;
	private String[]							listLabelCivility		= null;
	private String[]							listCodeCivility		= null;

	private LinkedHashMap<String, String>		listClass				= null;
	private String[]							listLabelClass			= null;
	private String[]							listCodeClass			= null;

	private ArrayList<M_LienContactAvecTiers>	listLienContactTiers	= new ArrayList<M_LienContactAvecTiers>();
	private	M_ContactExtension					extensionContact		= null;

	
	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_Contact(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Ins�re l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean insertInDatabase()
	{
		setRENUM(computeRENUM4Insert());
		if( (REPAC == null) || (REPAC.trim().length() == 0) ){
			setREPAC( getRENOM() + " " + getREPRE() );
		}

		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PSEMRTEM", querymg.getLibrary());
		return request(requete);
	}

	/**
	 * Modifie l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean updateInDatabase()
	{
		if( (REPAC == null) || (REPAC.trim().length() == 0) ){
			setREPAC( getRENOM() + " " + getREPRE() );
		}

		initGenericRecord(genericrecord, false);
		String requete = genericrecord.createSQLRequestUpdate("PSEMRTEM", querymg.getLibrary(), "RENUM=" + getRENUM() + " and REETB='" + getREETB() + "'");
		return request(requete);
	}

	/**
	 * Suppression de l'enregistrement courant
	 * @return
	 */
	@Override
	public boolean deleteInDatabase()
	{
		String requete = "delete from " + querymg.getLibrary() + ".PSEMRTEM where RENUM=" + getRENUM() + " and REETB='" + getREETB() + "'";
		return request(requete);
	}

	/**
	 * Retourne la zone Nom "am�lior�" 
	 */
	@Override
	public String getRENOM()
	{
		if( (super.getRENOM() == null) || super.getRENOM().equals("") ){
			return getREPAC();
		}
		return super.getRENOM(); 
	}

	/**
	 * Retourne la zone Pr�nom "am�lior�" 
	 */
	@Override
	public String getREPRE()
	{
		if( (super.getREPRE() == null) || super.getREPRE().equals("") ){
			return getREPAC();
		}
		return super.getREPRE(); 
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();

		listCivility.clear();
		listClass.clear();
		listLienContactTiers.clear();
	}

	
	// -- M�thodes priv�es ----------------------------------------------------
	
	/**
	 * Charge les civilit�s possibles
	 */
	private void loadCivility()
	{
		GM_PersonnalisationEXP psemparm = new GM_PersonnalisationEXP( querymg );
		setListCivility(psemparm.getListCivility( etablissement ));
	}

	/**
	 * Charge les fonctions possibles
	 */
	private void loadClass()
	{
		GM_PersonnalisationEXP psemparm = new GM_PersonnalisationEXP( querymg );
		setListClass(psemparm.getListClass( etablissement ));
	}

	/**
	 * Calcule le num�ro (id) avant l'insertion d'un enregistrement  
	 * @return
	 */
	private int computeRENUM4Insert()
	{
		String resultat = querymg.firstEnrgSelect("select max(RENM) from " + querymg.getLibrary()+ ".psemrtem", 1);
		int number = 1;
		if( resultat != null ){
			number = Integer.parseInt( resultat ) + 1;
		}
		return number;
	}
	
	// -- Accesseurs ----------------------------------------------------------

	/**
	 * @return le codeHierarchy
	 */
	public int getCodeHierarchy()
	{
		return codeHierarchy;
	}

	/**
	 * @param codeHierarchy le codeHierarchy � d�finir
	 */
	public void setCodeHierarchy(int codeHierarchy)
	{
		this.codeHierarchy = codeHierarchy;
	}

	/**
	 * @return le codeTypeContact
	 */
	public String getCodeTypeContact()
	{
		return codeTypeContact;
	}

	/**
	 * @param codeTypeContact le codeTypeContact � d�finir
	 */
	public void setCodeTypeContact(String codeTypeContact)
	{
		this.codeTypeContact = codeTypeContact;
	}

	/**
	 * @return le listLienContactTiers
	 */
	public ArrayList<M_LienContactAvecTiers> getListLienContactTiers()
	{
		return listLienContactTiers;
	}

	/**
	 * @param listLienContactTiers le listLienContactTiers � d�finir
	 */
	public void setListLienContactTiers(ArrayList<M_LienContactAvecTiers> listLienContactTiers)
	{
		this.listLienContactTiers = listLienContactTiers;
	}

	/**
	 * @return le listCivility
	 */
	public LinkedHashMap<String, String> getListCivility()
	{
		if( listCivility == null )
			loadCivility();
		return listCivility;
	}

	/**
	 * @param listCivility le listCivility � d�finir
	 */
	public void setListCivility(LinkedHashMap<String, String> listCivility)
	{
		this.listCivility = listCivility;
		
		// Chargement de la table des libell�s
		listLabelCivility = new String[listCivility.size()];
		listCodeCivility = new String[listCivility.size()];
		int i=0;
		for(Entry<String, String> entry : listCivility.entrySet()) {
			listCodeCivility[i] = entry.getKey().trim();
		    listLabelCivility[i++] = entry.getValue().trim();
		}
	}

	/**
	 * @return le listLabelCivility
	 */
	public String[] getListLabelCivility()
	{
		if( listCivility == null )
			loadCivility();
		return listLabelCivility;
	}

	/**
	 * @return le listCodeCivility
	 */
	public String[] getListCodeCivility()
	{
		if( listCivility == null )
			loadCivility();
		return listCodeCivility;
	}

	/**
	 * @return le listClass
	 */
	public LinkedHashMap<String, String> getListClass()
	{
		if( listClass == null )
			loadClass();
		return listClass;
	}

	/**
	 * @param listClass le listClass � d�finir
	 */
	public void setListClass(LinkedHashMap<String, String> listClass)
	{
		this.listClass = listClass;
		
		// Chargement de la table des libell�s
		listLabelClass = new String[listClass.size()];
		listCodeClass = new String[listClass.size()];
		int i=0;
		for(Entry<String, String> entry : listClass.entrySet()) {
			listCodeClass[i] = entry.getKey().trim();
		    listLabelClass[i++] = entry.getValue().trim();
		}
	}

	/**
	 * @return le listLabelClass
	 */
	public String[] getListLabelClass()
	{
		if( listClass == null )
			loadClass();
		return listLabelClass;
	}

	/**
	 * @return le listCodeClass
	 */
	public String[] getListCodeClass()
	{
		if( listClass == null )
			loadClass();
		return listCodeClass;
	}

	/**
	 * @return le extensionContact
	 */
	public M_ContactExtension getExtensionContact()
	{
		return extensionContact;
	}

	/**
	 * @param extensionContact le extensionContact � d�finir
	 */
	public void setExtensionContact(M_ContactExtension extensionContact)
	{
		this.extensionContact = extensionContact;
	}

}
