package ri.seriem.libas400.dao.exp.database.files;


import java.sql.Timestamp;

import ri.seriem.libas400.database.BaseFileDB;
import ri.seriem.libas400.database.QueryManager;

public abstract class FFD_Psemhflxm extends BaseFileDB
{
	// Constantes (valeurs r�cup�r�es via DSPFFD)
	public static final int SIZE_FLID		= 9;
	public static final int DECIMAL_FLID	= 0;
	public static final int SIZE_FLIDF		= 6;
	public static final int SIZE_FLETB		= 3;
	public static final int SIZE_FLVER		= 7;
	public static final int SIZE_FLCRE		= 26;
	public static final int DECIMAL_FLCRE	= 0;
	public static final int SIZE_FLCOD		= 50;
	public static final int SIZE_FLSNS		= 9;
	public static final int DECIMAL_FLSNS	= 0;
	public static final int SIZE_FLSTT		= 9;
	public static final int DECIMAL_FLSTT	= 0;
	public static final int SIZE_FLTRT		= 26;
	public static final int DECIMAL_FLTRT	= 0;
	public static final int SIZE_FLCPT		= 9;
	public static final int DECIMAL_FLCPT	= 0;
	public static final int SIZE_FLJSN		= 1048576;	// A corriger (c'est uyne valeur par d�faut du create table) 1048576
	public static final int SIZE_FLERR		= 9;
	public static final int DECIMAL_FLERR	= 0;
	public static final int SIZE_FLMSGERR	= 44;

	// Variables fichiers
	private int			FLID	= 0;			// L'ID record
	private String		FLIDF	= null;			// L'ID du flux
	private String		FLETB	= null;			// Etablissement concern� par le flux
	private String		FLVER	= null;			// Version du flux
	private Timestamp	FLCRE	= null;			// Date de cr�ation du flux
	private String		FLCOD	= null;			// Code objet � traiter, l'indicatif venant du RPG (en gros la cle)
	private int			FLSNS	= 0;			// Sens du flux (Envoi, R�ception)
	private int			FLSTT	= 0;			// Status du flux (A traiter, Succ�s, Echec, ...))
	private Timestamp	FLTRT	= null;			// Date de traitement
	private int			FLCPT	= 0;			// Nombre de tentatives 
	private String		FLJSN	= null;			// Message JSON 
	private int			FLERR	= 0;			// Code erreur
	private String		FLMSGERR = null;		// Message d'erreur

	

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public FFD_Psemhflxm(QueryManager aquerymg)
	{
		super(aquerymg);
	}
	
	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Initialise les variables avec les valeurs par d�faut
	 */
	public void initialization()
	{
		FLID	= 0;
		FLIDF	= null;
		FLETB	= null;
		setFLVER(null);
		FLCRE	= null;
		FLCOD	= null;
		FLSNS	= 0;
		FLSTT	= 0;
		FLTRT	= null;
		FLCPT	= 0; 
		FLJSN	= null; 
		FLERR	= 0;
		FLMSGERR= null;
	}

	/**
	 * @return le fLID
	 */
	public int getFLID()
	{
		return FLID;
	}

	/**
	 * @param fLID le fLID � d�finir
	 */
	public void setFLID(int fLID)
	{
		FLID = fLID;
	}

	/**
	 * @return le fLIDF
	 */
	public String getFLIDF()
	{
		return FLIDF;
	}

	/**
	 * @param fLIDF le fLIDF � d�finir
	 */
	public void setFLIDF(String fLIDF)
	{
		FLIDF = fLIDF;
	}

	/**
	 * @return le fLETB
	 */
	public String getFLETB()
	{
		return FLETB;
	}

	/**
	 * @param fLETB le fLETB � d�finir
	 */
	public void setFLETB(String fLETB)
	{
		FLETB = fLETB;
	}

	/**
	 * @return le fLVER
	 */
	public String getFLVER()
	{
		return FLVER;
	}

	/**
	 * @param fLVER le fLVER � d�finir
	 */
	public void setFLVER(String fLVER)
	{
		FLVER = fLVER;
	}

	/**
	 * @return le fLCRE
	 */
	public Timestamp getFLCRE()
	{
		return FLCRE;
	}

	/**
	 * @param fLCRE le fLCRE � d�finir
	 */
	public void setFLCRE(Timestamp fLCRE)
	{
		FLCRE = fLCRE;
	}

	/**
	 * @return le fLCOD
	 */
	public String getFLCOD()
	{
		return FLCOD;
	}

	/**
	 * @param fLCOD le fLCOD � d�finir
	 */
	public void setFLCOD(String fLCOD)
	{
		FLCOD = fLCOD;
	}

	/**
	 * @return le fLSNS
	 */
	public int getFLSNS()
	{
		return FLSNS;
	}

	/**
	 * @param fLSNS le fLSNS � d�finir
	 */
	public void setFLSNS(int fLSNS)
	{
		FLSNS = fLSNS;
	}

	/**
	 * @return le fLSTT
	 */
	public int getFLSTT()
	{
		return FLSTT;
	}

	/**
	 * @param fLSTT le fLSTT � d�finir
	 */
	public void setFLSTT(int fLSTT)
	{
		FLSTT = fLSTT;
	}

	/**
	 * @return le fLTRT
	 */
	public Timestamp getFLTRT()
	{
		return FLTRT;
	}

	/**
	 * @param fLTRT le fLTRT � d�finir
	 */
	public void setFLTRT(Timestamp fLTRT)
	{
		FLTRT = fLTRT;
	}

	/**
	 * @return le fLCPT
	 */
	public int getFLCPT()
	{
		return FLCPT;
	}

	/**
	 * @param fLCPT le fLCPT � d�finir
	 */
	public void setFLCPT(int fLCPT)
	{
		FLCPT = fLCPT;
	}

	/**
	 * @return le fLJSN
	 */
	public String getFLJSN()
	{
		return FLJSN;
	}

	/**
	 * @param fLJSN le fLJSN � d�finir
	 */
	public void setFLJSN(String fLJSN)
	{
		FLJSN = fLJSN;
	}

	/**
	 * @return le fLERR
	 */
	public int getFLERR()
	{
		return FLERR;
	}

	/**
	 * @param fLERR le fLERR � d�finir
	 */
	public void setFLERR(int fLERR)
	{
		FLERR = fLERR;
	}
	
	/**
	 * @return le fLMSGERR
	 */
	public String getFLMSGERR() 
	{
		return FLMSGERR;
	}

	/**
	 * @param fLMSGERR le fLMSGERR � d�finir
	 */
	public void setFLMSGERR(String fLMSGERR) 
	{
		FLMSGERR = fLMSGERR;
	}
	
	// -- Accesseurs ----------------------------------------------------------

	}
