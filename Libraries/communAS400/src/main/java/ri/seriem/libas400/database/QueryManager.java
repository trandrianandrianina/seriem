//=================================================================================================
//==>                                                                       07/12/2011 - 13/05/2016
//==> G�re les op�rations de base sur les fichiers (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.database;

import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ibm.as400.access.Record;

import ri.seriem.libas400.database.field.Field;
import ri.seriem.libas400.database.record.DataStructureRecord;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.security.DeChiffreEbcdic;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.GestionFichierSQL;

public class QueryManager
{
	// Constantes
	public		final static	int							ERREUR					= -1;
	
	private		final static	String						MARKER_CURLIB			= "##CURLIB##.";
	
	// Variables
	protected					Connection					dataBase				= null;
	protected					String						library					= "";
	protected					String						msgErreur 				= ""; // Conserve le dernier message d'erreur �mit et non lu 


	/**
	 * Constructeur
	 * @param database
	 */
	public QueryManager(Connection database)
	{
		setDatabaseOperation(database);
	}

	/**
	 * Constructeur
	 * @param database
	 * @param drivenative
	 *
	public QueryManager(Connection database, boolean drivenative)
	{
		setDatabaseOperation(database);
		driverNative = drivenative; 
	}*/

	/**
	 * Initialise le pointeur vers la base de donn�es
	 * @param database
	 */
	public void setDatabaseOperation(Connection database)
	{
		dataBase = database;
	}
	
    /**
     * Retourne la library (mais ce n'est pas forc�ment la curlib de la session)
	 * @return the library
	 */
	public String getLibrary()
	{
		return library;
	}


	/**
	 * Initialise la library (mais ne change pas la curlib de la session)
	 * @param bibliotheque the library to set
	 */
	public void setLibrary(String lib)
	{
		library = lib;
	}

	/**
	 *  Retourne la curlib
	 * @return
	 */
	public String getCurlib()
	{
		return firstEnrgSelect("select  current_schema from sysibm.sysdummy1", 1);
	}

	/**
	 * Initialise la curlib pour la session
	 * @return
	 */
	public boolean setCurlib(String curlib)
	{
		if( curlib != null ){
			//library = curlib.trim().toUpperCase();
			curlib = curlib.trim().toUpperCase();
			int ret = requete("set schema '"+curlib+"'");
			if( ret == Constantes.ERREUR ){
				return false;
			}
			return true;
		}
		msgErreur += "\nThe library is null.";
		return false;
	}

	/**
	 * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist
	 * @param requete
	 * @param lock
	 * @return
	 */
    public ArrayList<GenericRecord> select(String requete)
    {
    	if (dataBase == null) return null;

    	final ArrayList<GenericRecord> listeRecord = new ArrayList<GenericRecord>();
    	int i=0;
    	GenericRecord record = null;
    	PreparedStatement select = null;
    	ResultSet rs = null;
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			while (rs.next())
        		{
//System.out.println("-record-> " + rs.getMetaData().getColumnCount());
					record = new GenericRecord();
					for (i=1; i <= rs.getMetaData().getColumnCount(); i++)
					{
//System.out.println("[QueryManager] (select) " + rs.getMetaData().getColumnClassName(i) + "|" + rs.getObject(i)/*rs.getString(i)*/ + "|" + rs.getMetaData().getColumnName(i) + "|" + rs.getMetaData().getPrecision(i)+ "|" + rs.getMetaData().getScale(i));
						record.setField(rs.getMetaData().getColumnName(i), rs.getObject(i), rs.getMetaData().getPrecision(i), rs.getMetaData().getScale(i)); //, rs.getMetaData().getColumnClassName(i));
					}
           			listeRecord.add(record);
        		}
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{    	
    		try { if (rs != null) rs.close(); if (select != null) select.close(); } catch(Exception e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		System.out.println(msgErreur);
    	}
//System.out.println("--> " + resultat.size());
    	return listeRecord;
    }

    /**
     * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist
     * @param requete
     * @param datastructure
     * @return
     */
    public ArrayList<Record> select(String requete, String datastructure)
    {
    	return select(requete, (DataStructureRecord) newObject(datastructure) );
    }
    
    /**
     * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist
     * @param requete
     * @param DataStructureRecord
     * @return
     */
    public ArrayList<Record> select(String requete, DataStructureRecord datastructure)
    {
    	if (dataBase == null) return null;
//System.out.println("-Requete-> " + requete);

    	final ArrayList<Record> listeRecord = new ArrayList<Record>();
    	int i=0;
    	PreparedStatement select = null;
    	ResultSet rs = null;
    	byte[] content = null;
    	int offset;
    	
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			// On parcourt les records trouv�s
//System.out.println("-record-> " + datastructure.length);
    			content = new byte[datastructure.length];
    			while (rs.next())
        		{
        	    	offset = 0;
//System.out.println("-record-> " + rs.getMetaData().getColumnCount());
					for (i=1; i <= rs.getMetaData().getColumnCount(); i++)
					{
//System.out.println("[QueryManager] (select) " + rs.getMetaData().getColumnClassName(i) + "|" + rs.getString(i) + "|" + rs.getMetaData().getColumnName(i));
    					System.arraycopy(rs.getBytes(i), 0, content, offset, rs.getBytes(i).length);
						offset += rs.getBytes(i).length;
					}
           			listeRecord.add(datastructure.setBytes(content));
        		}
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{    	
    		try { if (rs != null) rs.close(); if (select != null) select.close(); } catch(Exception e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		System.out.println(msgErreur);
    	}
    	return listeRecord;
    }

    /**
     * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist et des champs � d�chiffrer
     * @param requete
     * @param DataStructureRecord
     * @param idxdeb, indice de d�but du tableau (tableau java donc indice_RPG-1)
     * @param length, longueur du buffer
     * @param offsetrecopy, position de recopie du buffer d�chiffr�
     * @return
     */
    public ArrayList<Record> selectWithDecryption(String requete, DataStructureRecord datastructure, int idxdeb, int length)
    {
    	if (dataBase == null) return null;
//System.out.println("-Requete-> " + requete);

    	final ArrayList<Record> listeRecord = new ArrayList<Record>();
    	int i=0;
    	PreparedStatement select = null;
    	ResultSet rs = null;
    	byte[] content = null;
    	int offset;
    	final DeChiffreEbcdic dechiffrage = new DeChiffreEbcdic();
    	
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			// On parcourt les records trouv�s
//System.out.println("-record-> " + datastructure.length);
    			content = new byte[datastructure.length];
    			while (rs.next())
        		{
        	    	offset = 0;
//System.out.println("-record-> " + rs.getMetaData().getColumnCount());
					for (i=1; i <= rs.getMetaData().getColumnCount(); i++)
					{
//System.out.println("[QueryManager] (select) " + rs.getMetaData().getColumnClassName(i) + "|" + rs.getString(i) + "|" + rs.getMetaData().getColumnName(i));
						System.arraycopy(rs.getBytes(i), 0, content, offset, rs.getBytes(i).length);
						offset += rs.getBytes(i).length;
					}
					// On d�chiffre la partion des octets si besoin (on ne peut pas champ par champ car la cle fonctionne pour un ensemble)
					if (idxdeb != -1)
					{	
						byte[] tabconv = new byte[length];
						System.arraycopy(content, idxdeb, tabconv, 0, length);
						System.arraycopy(dechiffrage.getDecryptSecurity(tabconv), 0, content, idxdeb, length);
						tabconv = null;
					}
           			listeRecord.add(datastructure.setBytes(content));
        		}
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{    	
    		try { if (rs != null) rs.close(); if (select != null) select.close(); } catch(Exception e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		System.out.println(msgErreur);
    	}
    	return listeRecord;
    }

    
    /**
     * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist
     * @param requete
     * @return
     *
    public ArrayList<?> select(String requete, String typeClass)
    {
    	if (dataBase == null) return null;
//System.out.println("-Requete-> " + requete);
//System.out.println("-TypeClass-> " + typeClass);

    	final ArrayList<Object> listeRecord = new ArrayList<Object>();
    	int i=0;
    	Object record = null;
    	PreparedStatement select = null;
    	ResultSet rs = null;
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			while (rs.next())
        		{
    		    	record = newObject(typeClass);
//System.out.println("-record-> " + record + "\n" + getMsgError());    		    	
       				for (i=1; i <= rs.getMetaData().getColumnCount(); i++)
       				{
       					//A ou A (Varlen)  java.lang.String - S java.math.BigDecimal (P est consid�r� comme un BigDecimal)
//System.out.println("[QueryManager] (select) " + rs.getMetaData().getColumnClassName(i) + "|" + rs.getString(i) + "|" + rs.getMetaData().getColumnName(i));
       					if (rs.getMetaData().getColumnClassName(i).equals("java.lang.String"))
       						((InterfaceRecord)record).setField(rs.getMetaData().getColumnName(i), rs.getString(i));
   						else
           					if (rs.getMetaData().getColumnClassName(i).equals("java.math.BigDecimal"))
           					{
//System.out.println(record + " " + rs.getMetaData().getColumnName(i) + " " + rs.getBigDecimal(i));           						
           						((InterfaceRecord)record).setField(rs.getMetaData().getColumnName(i), rs.getBigDecimal(i));
           					}
       				}
           			listeRecord.add(record);
        		}
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{    	
    		try { if (rs != null) rs.close(); if (select != null) select.close(); } catch(Exception e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		System.out.println(msgErreur);
    	}
//System.out.println("--> " + resultat.size());
    	return listeRecord;
    }*/

    /**
     * Instancie une classe 
     * @param typeClass
     * @return
     */
    protected Object newObject(String typeClass)
    {
    	try
		{
			return Class.forName(typeClass, true, this.getClass().getClassLoader()).getConstructor ().newInstance();
    		//return QueryManager.class.getClassLoader().loadClass(typeClass).getConstructor ().newInstance();
		}
		catch (Exception e)
		{
			msgErreur += "\nObjet non instanci� avec le type : " + typeClass;	
		}
		return null;
    }
    
    /**
     * Lancement d'une requ�te Select - Pb avec le select non clos�
     * @param requete
     * @return
     */
    public ResultSet requeteSelect(String requete)
    {
    	//PreparedStatement select=null;
    	Statement statement = null;
    	try
    	{
    		// Pr�pare le statement
    		//select = dataBase.prepareStatement(requete);
    		statement = dataBase.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY); 
    		//return select.executeQuery();
    		//ResultSet rs = statement.executeQuery();
    		ResultSet rs = statement.executeQuery(requete);
    		//select.close();
        	return rs;
    	}
    	catch (SQLException exc)
    	{
    		//if (select != null) try {select.close();} catch (SQLException e) {}
    		if (statement != null) try {statement.close();} catch (SQLException e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return null;
    	}
    }

    /**
     * Retourne le nombre d'enregistrement suite � un Select
     * @param requete
     * @return
     */
    public int nbrEnrgSelect(String requete)
    {
    	int count=0;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			while (rs.next())
    				count++;
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return -1;
    	}
    	
    	return count;
    }
    
    /**
     * Retourne le premier enregistrement de la colonne voulu suite � un Select
     * @param requete
     * @return
     * @throws SQLException 
     */
    public String firstEnrgSelect(String requete, String labelCol)
    {
    	String valeur=null;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			rs.next();
    			valeur = rs.getString(labelCol);
    			rs.close();
    		}
			select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return null;
    	}
    	
    	return valeur;
    }
    
    /**
     * Retourne le premier enregistrement de la colonne voulu suite � un Select (col doit �tre sup�rieur � 0)
     * @param requete
     * @return
     * @throws SQLException 
     */
    public String firstEnrgSelect(String requete, int col)
    {
    	String valeur=null;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
    			rs.next();
    			valeur = rs.getString(col);
    			rs.close();
    		}
			select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return null;
    	}
    	
    	return valeur;
    }

    /**
     * Lancement d'une requ�te Select avec retour des donn�es dans une arraylist
     * @param requete
     * @return
     */
    public ArrayList<String> traitementSelect(String requete)
    {
    	StringBuffer chaine = new StringBuffer(0);
    	ArrayList<String> resultat = new ArrayList<String>();
    	int i=0;
    	PreparedStatement select=null;
    	ResultSet rs=null;
    	try
    	{
    		// Pr�pare le statement
//System.out.println(requete);    			
    		select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
    		rs = select.executeQuery();
    		if (rs != null)
    		{
//System.out.println(rs.getMetaData().getColumnCount());    			
    			while (rs.next())
        		{
        			chaine.setLength(0);
        			// Si plusieurs colonnes
        			if (rs.getMetaData().getColumnCount() > 1)
        			{
        				for (i=1; i <= rs.getMetaData().getColumnCount()-1; i++)
        					chaine.append(rs.getString(i)).append("|");
            			chaine.append(rs.getString(i));
            			resultat.add(chaine.toString());
        			}
        			else // Une seule colonne
       					resultat.add(rs.getString(1));
        		}
    			rs.close();
    		}
    		select.close();
    	}
    	catch (SQLException exc)
    	{
    		try {if (rs != null) rs.close(); if (select != null) select.close();} catch (SQLException e) {}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		System.out.println(msgErreur);
    		return null;
    	}
//System.out.println("--> " + resultat.size());
    	return resultat;
    }

    /**
     * Lancement d'une requ�te insert, delete, update
     * @param requete
     * @return
     */
    public int requete(String requete)
    {
    	Statement stmt=null;
    	try
    	{
    		// Prepare le statement
    		stmt = dataBase.createStatement();
            int resultat = stmt.executeUpdate(requete);
            stmt.close();
            return resultat;
    	}
    	catch (SQLException exc)
    	{
    		if (stmt != null) try{stmt.close();}catch (SQLException e){}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return ERREUR;
    	}
    }

    /**
     * Lancement d'une requ�te insert, delete, update
     * Permet de r�cup�rer la valeur de l'autoincr�m�nt (ID) 
     * @param requete
     * @return
     */
    public int insertWhoReturnId(String requete)
    {
		int id = Constantes.ERREUR;
    	Statement stmt=null;
    	try
    	{
    		// Prepare le statement
    		stmt = dataBase.createStatement();
            stmt.execute(requete);
           	ResultSet rs = stmt.executeQuery("SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1"); // Attention si il y a un commit juste avant cette requ�te c'est mort 
           	if(rs.next()) {
           		id = Integer.parseInt(rs.getString(1)); // ID auto g�n�r�
           	}
            stmt.close();
            return id;
    	}
    	catch (SQLException exc)
    	{
    		if (stmt != null) try{stmt.close();}catch (SQLException e){}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return ERREUR;
    	}
    }

    /**
     * Lance des requ�tes contenues dans un fichier SQL 
     * @param requete
     * @return
     */
    public boolean executeSQLFile(String sqlfile, String curlib)
    {
    	if( curlib == null ){
    		curlib = "";
    	} else if( !curlib.trim().endsWith(".")){
    		curlib += '.';
    	}
    	
    	// On charge les requ�tes depuis le fichier
		GestionFichierSQL sql = new GestionFichierSQL(sqlfile);
		if( !sql.treatmentFile() ){
			msgErreur += sql.getMsgErreur();
			sql.dispose();
			return false;
		}
		ArrayList<String> requetes = sql.getListQueries();
    	Statement stmt=null;
    	String requete = null;
    	try
    	{
    		// Pr�pare le statement
    		stmt = dataBase.createStatement();
    		for( int i = 0; i < requetes.size(); i++ ){
    			requete = requetes.get( i ).replaceAll(MARKER_CURLIB, curlib);
//System.out.println("-->"+ requete);    			
    			stmt.executeUpdate(requete);
    			msgErreur += "\n[ OK ] : " + requete;
    		}
    		stmt.close();
    		sql.dispose();
    		return true;
    	}
    	catch (SQLException exc)
    	{
    		if (stmt != null) try{stmt.close();}catch (SQLException e){}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		sql.dispose();
    		return false;
    	}
    }

    /**
     * Lancement d'une requ�te du type insert, update // TODO � am�liorer afin qu'elle soit plus g�n�rique
     * @param requete
     * @param rcd
     * @return
     */
    public int requete(String requete, GenericRecord rcd)
    {
    	PreparedStatement stmt=null;
    	
    	try
    	{
    		// Pr�pare le statement
    		stmt = dataBase.prepareStatement(requete);
    		for (int i=1; i<=stmt.getParameterMetaData().getParameterCount(); i++)
    		{
    			if( stmt.getParameterMetaData().getParameterClassName(i).equals("java.sql.Timestamp") ){
        			Field<?> champ = rcd.getFieldWithTypeAndNumber(stmt.getParameterMetaData().getParameterClassName(i), i);
        			stmt.setTimestamp(i, (Timestamp) champ.getValue());
    				
    			} else {
    		    	// Pr�paration des types en fonction des drivers
    		    	//if( driverNative ){
    		    	//	clobLocator = "com.ibm.db2.jdbc.app.DB2ClobLocator";
    		    	//}
    		    	String clobLocator = "com.ibm.as400.access.AS400JDBCClobLocator";
	    			Field<?> champ = rcd.getFieldWithTypeAndNumber(clobLocator, i);
	    			if (champ != null)
	    			{
	    				if (champ.getSQLType().equals(clobLocator))
	    				{
	    					Reader reader = new StringReader((String) champ.getValue());
	    					stmt.setCharacterStream(i, reader, champ.length);
	    				}
	    			}
    			}
    		}
            int resultat = stmt.executeUpdate();
            stmt.close();
            return resultat;
    	}
    	catch (SQLException exc)
    	{
    		if (stmt != null) try{stmt.close();}catch (SQLException e){}
    		msgErreur += "\nEchec de la requ�te: " + requete + "\n" + exc.getMessage();
    		return ERREUR;
    	}
    }

    /**
     * Retourne le r�sultat de la requ�te sous forme de Document XML
     * @param requete
     * @param tag
     * @param doc
     * @return
     *
    public Document select2XML(String requete, String root, String tag, Document doc) // Tag sans les < > 
    {
    	if (dataBase == null) return null;
    	
		try
		{
			Statement stmt = dataBase.createStatement();
			ResultSet rs = stmt.executeQuery(requete);

			doc = toDocument(rs, root, tag, doc);

			rs.close();
			stmt.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

    	return doc;
    }*/
    
    /**
     * Retourne le r�sultat de la requ�te sous forme de Document XML
     * @param requete
     * @param tag
     * @param doc
     * @return
     */
    public Document select2XML(String requete, String[] path, String tag, Document doc) // Tag sans les < > 
    {
    	if (dataBase == null) return null;
    	
		try
		{
			Statement stmt = dataBase.createStatement();
			ResultSet rs = stmt.executeQuery(requete);

			doc = toDocument(rs, path, tag, doc);

			rs.close();
			stmt.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

    	return doc;
    }

    
    /*
	private static Document toDocument(ResultSet rs, String root, String tag, Document doc) throws ParserConfigurationException, SQLException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Element results = null;
		if( doc == null){
			doc = builder.newDocument();
			results = doc.createElement(root);
			doc.appendChild(results);
		} else{
			results = doc.getDocumentElement();
		}

		ResultSetMetaData rsmd = rs.getMetaData();
		int colCount = rsmd.getColumnCount();

		while (rs.next())
		{
			Element row = doc.createElement(tag);
			results.appendChild(row);

			for (int i = 1; i <= colCount; i++)
			{
				String columnName = rsmd.getColumnName(i);
				Object value = rs.getObject(i);

				Element node = doc.createElement(columnName);
				if( value instanceof String ){
					String str = value.toString().trim();
					if( str.length() == 0){
						node.appendChild(doc.createTextNode(str));
					} else{
						node.appendChild(doc.createCDATASection(str));
					}
					
				} else{
					node.appendChild(doc.createTextNode(value.toString().trim()));
				}

				row.appendChild(node);
			}
		}
		return doc;
	}*/

	private static Document toDocument(ResultSet rs, String[] path, String tag, Document doc) throws ParserConfigurationException, SQLException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Element[] noeud = new Element[path.length+1];
		int j=0;
		if( doc == null ){
			doc = builder.newDocument();
			noeud[0] = doc.createElement(path[0]);
			doc.appendChild(noeud[0]);
		} else{
			noeud[0] = doc.getDocumentElement();
		}

		// On construit l'arborescence
		for(j=1; j < path.length; j++ ){
			noeud[j] = doc.createElement(path[j]);
			noeud[j-1].appendChild(noeud[j]);
		}
		j--; // On se positionne sur le dernier noeud

		ResultSetMetaData rsmd = rs.getMetaData();
		int colCount = rsmd.getColumnCount();

		while (rs.next())
		{
			Element row = doc.createElement(tag);
			noeud[j].appendChild(row);

			for (int i = 1; i <= colCount; i++)
			{
				String columnName = rsmd.getColumnName(i);
				Object value = rs.getObject(i);

				Element node = doc.createElement(columnName);
				if( value instanceof String ){
					String str = value.toString().trim();
					if( str.length() == 0){
						node.appendChild(doc.createTextNode(str));
					} else{
						node.appendChild(doc.createCDATASection(str));
					}
					
				} else{
					node.appendChild(doc.createTextNode(value.toString().trim()));
				}

				row.appendChild(node);
			}
		}
		return doc;
	}

	/**
	 * Verrouille les enregistrments
	 */
	public boolean lockRecord()
	{
		setLock(true);
		int ret = requete("SET TRANSACTION ISOLATION LEVEL REPEATABLE READ");
		if( ret == Constantes.ERREUR )
			return false;
		return true;
	}

	/**
	 * D�verrouille les enregistrments et commite les modifications
	 */
	public void unlockRecord()
	{
		commit();
		setLock(false);
	}
	
	/**
	 * @return le lock
	 */
	public boolean isLocked()
	{
		try{
			return dataBase.getAutoCommit();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		if (dataBase != null){
			try
			{
				dataBase.close();
			}
			catch (SQLException e)
			{
				// TODO Bloc catch g�n�r� automatiquement
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Active ou d�sactive le commit de la base
	 * @param lock le lock � d�finir
	 */
	private void setLock(boolean lock)
	{
		try{
			if( lock )
				dataBase.setAutoCommit(false);
			else
				dataBase.setAutoCommit(true);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Commit de la base de donn�es 
	 */
	private void commit()
	{
		try{
			dataBase.commit();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Rollback de la base de donn�es 
	 *
	private void rollback()
	{
		try{
			dataBase.rollback();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}*/


}
