package ri.seriem.libas400.system.systemrequest;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.IFSFile;

import ri.seriem.libas400.system.AS400UserSpace;
import ri.seriem.libcommun.protocoleMsg.LibraryRequest;
import ri.seriem.libcommun.protocoleMsg.UserspaceRequest;

public class UserspaceRequestActions
{
	private AS400 sysAS400=null;
	private UserspaceRequest rupc=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public UserspaceRequestActions(AS400 system, UserspaceRequest ld)
	{
		sysAS400 = system;
		rupc = ld;
	}
	
	/**
	 * Traitement des actions possibles 
	 */
	public void actions()
	{
		//int theactions = actions;
		for (int i=LibraryRequest.ACTIONS.size(); --i>0;)
		{
			if (rupc.getActions() >= LibraryRequest.ACTIONS.get(i))
			{
				switch(LibraryRequest.ACTIONS.get(i))
				{
					case UserspaceRequest.ISEXISTS:
						userspaceExists();
						break;
					case UserspaceRequest.CREATE:
						userspaceCreate();
						break;
					case UserspaceRequest.READ:
						userspaceRead();
						break;
					case UserspaceRequest.WRITE:
						userspaceWrite();
						break;
					//case LibraryRequest.LIST:
					//	libraryList();
					//	break;
					//case LibraryRequest.LISTINTO:
					//	libraryListInto();
					//	break;
				}
				rupc.setActions(rupc.getActions()- LibraryRequest.ACTIONS.get(i));
			}
		}
	}

	/**
	 * Traite la demande de v�rification d'une userspace
	 */
	private void userspaceExists()
	{
		// Contr�le des variables 
		String userspace = rupc.getUserspace();
		if ((userspace == null) || (userspace.length() > 10))
		{
			rupc.setSuccess(false);
			rupc.setExist(false);
			rupc.setMsgError("Le nom du userspace est incorrect : " + userspace);
			return;
		}
		String library = rupc.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rupc.setSuccess(false);
			rupc.setExist(false);
			rupc.setMsgError("Le nom de la biblioth�que est incorrect : " + library);
			return;
		}
		
        final String racine = "/QSYS.LIB/"+library+".LIB/" + userspace + ".USRSPC";
        final IFSFile dta = new IFSFile(sysAS400, racine);
        try
		{
        	rupc.setSuccess(true);
        	rupc.setExist(dta.exists());
        	rupc.setMsgError("Le userspace " + userspace + " existe.");
		}
		catch (IOException e)
		{
			rupc.setSuccess(false);
			rupc.setExist(false);
			rupc.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la cr�ation d'une biblioth�que
	 */
	private void userspaceCreate()
	{
		String userspace = rupc.getUserspace();
		String library = rupc.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		userspaceExists();
		if (rupc.isExist())
		{
			rupc.setSuccess(true);
			rupc.setMsgError("Le userspace "+userspace+" existe d�j� dans " + library);
			return;
		}

		// Sinon on l'a cr��
		AS400UserSpace uspc = new AS400UserSpace(sysAS400, library, userspace);
		try
		{
			uspc.create(AS400UserSpace.BLOCK_SIZE, true); // TODO Variabliliser la taille et le replace
			rupc.setMsgError(null);
			rupc.setSuccess(true);
		}
		catch (Exception e)
		{
			rupc.setSuccess(false);
			rupc.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la demande de r�cup�ration du texte de la userspace
	 */
	private void userspaceRead()
	{
		String userspace = rupc.getUserspace();
		String library = rupc.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		userspaceExists();
		if (!rupc.isExist())
		{
			rupc.setSuccess(true);
			rupc.setData(null);
			return;
		}

		AS400UserSpace uspc = new AS400UserSpace(sysAS400, library, userspace);
		try
		{
			rupc.setSuccess(true);
			rupc.setMsgError(null);
			rupc.setData(uspc.readBlock(0)); // Variabiliser l'offset 
		}
		catch (Exception e)
		{
			rupc.setSuccess(false);
			rupc.setMsgError(e.getMessage());
			rupc.setData(null);
		}
	}

	/**
	 * Traite la demande de r�cup�ration du texte de la userspace
	 */
	private void userspaceWrite()
	{
		String userspace = rupc.getUserspace();
		String library = rupc.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		userspaceExists();
		if (!rupc.isExist())
		{
			rupc.setSuccess(true);
			return;
		}

		AS400UserSpace uspc = new AS400UserSpace(sysAS400, library, userspace);
		try
		{
			uspc.writeBlock(0, rupc.getData()); // TODO Variabiliser l'offset
			rupc.setSuccess(true);
			rupc.setMsgError(null);
		}
		catch (Exception e)
		{
			rupc.setSuccess(false);
			rupc.setMsgError(e.getMessage());
		}
	}

	/**
	 * Traite la demande de listage d'une biblioth�que
	 *
	private void libraryList()
	{
		String dataarea = rdta.getDataarea();
		String library = rdta.getLibrary();
		
		// On v�rifie que la library n'existe pas d�j�
		dataareaExists();
		if (!rdta.isExist())
		{
			rdta.setSuccess(false);
			rdta.setText(null);
			return;
		}

		//CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
		dataarea = dataarea.toUpperCase();
		library = library.toUpperCase();
		rdta.getVariant().clear();
		rdta.setTypeVariant("LIST_NAMEDTA");

		try
		{
			ObjectList ol = new ObjectList(sysAS400, "QSYS", library, "*DTA");
			ol.load();
		
			Enumeration enu = ol.getObjects();
			while(enu.hasMoreElements()){
				ObjectDescription od = (ObjectDescription) enu.nextElement();
				rlib.getVariant().add(od.getName());
			}
			
			rlib.setSuccess(true);
			rlib.setMsgError(null);
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}*/

	/**
	 * Traite la demande de listage d'une biblioth�que
	 *
	private void libraryListInto()
	{
		String library = rlib.getLibrary();
		if ((library == null) || (library.length() > 10))
		{
			rlib.setSuccess(false);
			rlib.setMsgError("Le nom de la biblioth�que est incorrect : " + library);
			rlib.setText(null);
		}
		library = library.toUpperCase();
		rlib.getVariant().clear();
		rlib.setTypeVariant("NAMEOBJ");

		try
		{
			ObjectList ol = new ObjectList(sysAS400, "QSYS", library, "*LIB");
			ol.load();
		
			Enumeration enu = ol.getObjects();
			while(enu.hasMoreElements()){
				ObjectDescription od = (ObjectDescription) enu.nextElement();
				rlib.getVariant().put(od.getName(), null);
			}
			
			rlib.setSuccess(true);
			rlib.setMsgError(null);
		}
		catch (Exception e)
		{
			rlib.setSuccess(false);
			rlib.setMsgError(e.getMessage());
		}
	}*/

}
