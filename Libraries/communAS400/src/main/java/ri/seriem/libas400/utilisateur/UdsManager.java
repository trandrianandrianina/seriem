//=================================================================================================
//==>                                                                       17/01/2012 - 17/01/2012
//==> Gestion des informations contenues dans la *LDA
//=================================================================================================
package ri.seriem.libas400.utilisateur;

import ri.seriem.libas400.system.LdaManager;
import ri.seriem.libas400.system.SystemManager;

public class UdsManager extends LdaManager
{
	// Constantes
	public static final int OFFSET_LOCASP=1024;
	public static final int LONG_LOCASP=1;
	
	// Variables
	private char locasp=' ';

	/**
	 * Constructeur
	 * @param psysteme
	 */
	public UdsManager(SystemManager psysteme)
	{
		super(psysteme);
	}

	/**
	 * D�coupe la dataara pour alimenter les zones
	 * @param uds
	 * @return
	 */
	public boolean parseUds()
	{
		// Lecture de la *LDA
		String uds = getData();
		if (uds == null) return false;
		
		// D�coupage
		if (uds.length() >= OFFSET_LOCASP)
			setLocasp(uds.charAt(OFFSET_LOCASP-1));
			
		return true;
	}
	
	/**
	 * @param locasp the locasp to set
	 */
	public void setLocasp(char locasp)
	{
		this.locasp = locasp;
	}

	/**
	 * @return the locasp
	 */
	public char getLocasp()
	{
		return locasp;
	}
	
}
