//=================================================================================================
//==>                                                                       21/05/2014 - 09/02/2015
//==> Permet d'executer un programme RPG ou de lancer une commande 
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.Job;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;

public class CallProgram
{
	// Variables
	private AS400 systeme=null;
	private String nomPrg=null;						// Nom du programme AS400 sous la forme "/QSYS.LIB/SVAS.LIB/BASE64.PGM";
	private ProgramParameter[] parameterList=null;	// ProgramParameter[] parameterList = new ProgramParameter[1];
    												// AS400Text nametext = new AS400Text(668);
    												// parameterList[0] = new ProgramParameter(nametext.toBytes("Coucou"), 668);
	private ProgramCall program=null;
	private CommandCall command=null;
	
	private String codeErreur= "";					// Contient l'ID de l'erreur lors d'une execution
	private String msgErreur = "";					// Conserve le dernier message d'erreur �mit et non lu

	
	/**
	 * Constructeur
	 * @param system
	 */
	public CallProgram(AS400 system)
	{
		setSysteme(system);
	}

	/**
	 * Constructeur
	 * @param system
	 * @param prg
	 * @param aparameterList
	 */
	public CallProgram(AS400 system, String prg, ProgramParameter[] aparameterList)
	{
		this(system);
		setNomPrg(prg);
		setParameterList(aparameterList);
	}
	
	/**
	 * Execute le programme sur l'AS400
	 * @return
	 */
	public boolean execute()
	{
		try
		{
			if (parameterList == null)
				program.setProgram(nomPrg);
			else
				program.setProgram(nomPrg, parameterList);

	        // Run the program.
			boolean retour = program.run(); 
            AS400Message[] messagelist = program.getMessageList();
            if (messagelist.length > 0)
            	codeErreur = messagelist[0].getID();
	        if (!retour)
	        {
	            // Report failure.
	        	msgErreur += "\nProgramme en echec !";
	        	
	            // Show the messages.
	            for (int i = 0; i < messagelist.length; ++i)
	            	msgErreur += "\n" + messagelist[i];
	        }
	        else
	        	return true;
		}
		catch(Exception e)
		{
			msgErreur += "\n" + e;
		}
		
		return false;
	}

	/**
	 * Execute le programme sur l'AS400
	 * @param ordre
	 * @return
	 */
	public boolean execute(String ordre)
	{
		if ((ordre == null) || ordre.trim().equals("")) return false;
				
		if (command == null)
			command = new CommandCall(systeme);
		try
		{
			codeErreur = null;
			command.setCommand(ordre);
	        // Run the program
			boolean retour = command.run();
            AS400Message[] messagelist = command.getMessageList();
            if (messagelist.length > 0)
            	codeErreur = messagelist[0].getID();
            	
	        if (!retour)
	        {
	            // Report failure.
	        	msgErreur += "\nProgramme en echec !";
	        	
	            // Show the messages.
	            for (int i = 0; i < messagelist.length; ++i)
	            	msgErreur += "\n" + messagelist[i];
	        }
	        else
	        	return true;
		}
		catch(Exception e)
		{
			msgErreur += "\n" + e;
		}
		
		return false;
	}

	
	/**
	 * Ajout d'une biblioth�que
	 * @param library
	 * @param curlib
	 */
	public boolean addLibrary(String library, boolean curlib)
	{
		if (library == null) return false;

		String chaine;
		if (curlib)
			chaine = "CHGCURLIB CURLIB("+library.trim()+")";
		else
			chaine = "ADDLIBLE LIB("+library.trim()+")";
		
		return execute(chaine);
	}
	
	public Job getJob()
	{
		try
		{
			if (program != null)
				return program.getServerJob();
			if (command != null)
				return command.getServerJob();
		}
		catch (Exception e)
		{}
		return null;
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	
	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le systeme
	 */
	public AS400 getSysteme()
	{
		return systeme;
	}


	/**
	 * @param systeme le systeme � d�finir
	 */
	public void setSysteme(AS400 systeme)
	{
		this.systeme = systeme;
		if (systeme != null)
		{
			program = new ProgramCall(systeme);
			execute("CHGJOB JOBPTY(0)");
			//program.setThreadSafe(true); comment� car en ex�cution directe sur l'AS400 il cr�e un autre job pour lancer le program
		}
	}


	/**
	 * @return le nomPrg
	 */
	public String getNomPrg()
	{
		return nomPrg;
	}


	/**
	 * @param nomPrg le nomPrg � d�finir
	 */
	public void setNomPrg(String nomPrg)
	{
		this.nomPrg = nomPrg;
	}



	/**
	 * @return le parameterList
	 */
	public ProgramParameter[] getParameterList()
	{
		return parameterList;
	}



	/**
	 * @param parameterList le parameterList � d�finir
	 */
	public void setParameterList(ProgramParameter[] parameterList)
	{
		this.parameterList = parameterList;
	}
	
	/**
	 * 	
	 */
	public String getCodeErreur()
	{
		return codeErreur;
	}
}
