//=================================================================================================
//==>                                                                       19/12/2011 - 19/12/2011
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;


public class PsemdimmManager extends QueryManager
{
	/**
	 * Constructeur
	 * @param database
	 */
	public PsemdimmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Retourne la liste des modules (pour les menus)
	 * @param debut
	 * @param fin
	 * @return
	 */
	public ArrayList<GenericRecord> getTousEnregistrements(int debut, int fin)
	{
		String requete = "Select * from m_gpl.psemdimm WHERE DMNIN >= '" + debut + "' AND DMNIN < '"  + fin + "'";
		return select(requete);
	}
    
}
