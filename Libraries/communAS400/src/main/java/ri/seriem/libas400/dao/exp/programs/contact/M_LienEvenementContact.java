package ri.seriem.libas400.dao.exp.programs.contact;

import ri.seriem.libas400.dao.exp.database.files.FFD_Psemlecm;
import ri.seriem.libas400.database.QueryManager;

public class M_LienEvenementContact extends FFD_Psemlecm
{
	// Constantes
	public static final int ACTION_NOTHING	= 0;
	public static final int ACTION_INSERT	= 1;
	public static final int ACTION_UPDATE	= 2;
	public static final int ACTION_DELETE	= 3;
	/*
	public static final HashMap<String, String> TYPE_CONTACT = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{put("Cr\u00e9ateur", "CR");}
				{put("Ex\u00e9cutant", "EX");}
				{put("Cible", "CI");}
			};
			*/
	public static final String[]	TYPE_CONTACT		= {"CR", "EX", "CI"};
	public static final String[]	TYPE_CONTACT_LIB_FR	= {"Cr\u00e9ateur", "Ex\u00e9cutant", "Cible"};
	public static final byte 		NO_HIERARCHY		= 0;
			
	// Variables
	private int					actionInDatabase		= ACTION_NOTHING;

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_LienEvenementContact(QueryManager aquerymg)
	{
		super(aquerymg);
		omittedField = new String[]{ "ECID" };
	}

	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Ins�re un lien �v�nement dans le table
	 * @return
	 */
	public boolean insertInDatabase()
	{
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PSEMLECM", querymg.getLibrary());
		return request(requete);
	}

	/**
	 * Modifie un lien �v�nement dans le table
	 * @return
	 */
	public boolean updateInDatabase()
	{
		initGenericRecord(genericrecord, false);
		String requete = genericrecord.createSQLRequestUpdate("PSEMLECM", querymg.getLibrary(), "ECID = " + getECID());
		return request(requete);
	}

	/**
	 * Suppression de l'enregistrement courant ou de tous
	 * @return
	 */
	public boolean deleteInDatabase()
	{
		return deleteInDatabase(false);
	}
	
	/**
	 * Suppression de l'enregistrement courant ou de tous
	 * @param querymg
	 * @return
	 */
	public boolean deleteInDatabase(boolean deleteAll)
	{
		// Suppression dans le PSEMLECM
		String requete = "delete from " + querymg.getLibrary() + ".PSEMLECM where ECIDE=" + getECIDE();
		if( !deleteAll )
			requete += " and ECIDC=" + getECIDC() + " and ECETBC='" + getECETBC() + "'";
		return request(requete);
	}
	
	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();
	}

	// -- M�thodes priv�es ----------------------------------------------------

	// -- Accesseurs ----------------------------------------------------------

	/**
	 * @return le actionInDatabase
	 */
	public int getActionInDatabase()
	{
		return actionInDatabase;
	}

	/**
	 * @param actionInDatabase le actionInDatabase � d�finir
	 */
	public void setActionInDatabase(int actionInDatabase)
	{
		this.actionInDatabase = actionInDatabase;
	}

}
