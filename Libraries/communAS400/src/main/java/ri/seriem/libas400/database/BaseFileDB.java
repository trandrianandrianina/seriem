//=================================================================================================
//==>                                                                       17/09/2015 - 18/04/2016
//==> Lorsque l'on cr�� une classe bas�e sur la d�finition d'un fichier DB2
//==> il faut d�river depuis cette classe de base (voir S�rieNMetier dans exp.program.contact)
//=================================================================================================

package ri.seriem.libas400.database;
import ri.seriem.libas400.database.record.GenericRecord;


public abstract class BaseFileDB
{
	// Variables de travail
	protected QueryManager 			querymg					= null;
	protected String[]				omittedField 			= null;
	protected GenericRecord 		genericrecord			= new GenericRecord();
	
	protected String				msgErreur				= ""; // Conserve le dernier message d'erreur �mit et non lu 


	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public BaseFileDB(QueryManager aquerymg)
	{
		setQuerymg(aquerymg);
	}
	
	//-- M�thodes abstract ----------------------------------------------------
	
	public abstract void 	initialization();
	public abstract boolean insertInDatabase();
	public abstract boolean updateInDatabase();
	public abstract boolean deleteInDatabase(); //, boolean deleteAll);
	public abstract void	dispose();
	
	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * @return le querymg
	 */
	public QueryManager getQuerymg()
	{
		return querymg;
	}

	/**
	 * @param querymg le querymg � d�finir
	 */
	public void setQuerymg(QueryManager querymg)
	{
		this.querymg = querymg;
	}

	/**
	 * Initialise les donn�es du record avec cet objet
	 * @param rcd
	 * @return
	 */
	public boolean initGenericRecord(GenericRecord rcd, boolean clearBefore)
	{
		if( rcd == null ) {
			return false;
		}
		
		rcd.setOmittedField(omittedField);
		return rcd.fromObject(this, clearBefore);
	}

	/**
	 * Initialise l'objet avec les donn�es du record
	 * @param rcd
	 * @return
	 */
	public boolean initObject(GenericRecord rcd, boolean doinit)
	{
		if( rcd == null ) {
			return false;
		}
		
		if( doinit ) initialization();
		return rcd.toObject(this);
	}

	/**
	 * @return le omittedField
	 */
	public String[] getOmittedField()
	{
		return omittedField;
	}

	// -- M�thodes prot�g�es --------------------------------------------------

	/**
	 * Requ�te permettant de lancer une requ�te
	 * @param querymg
	 * @param requete
	 * @return
	 */
	protected boolean request(String requete)
	{
		int ret = querymg.requete(requete);
		if( ret == -1 ){
			msgErreur += '\n' + querymg.getMsgError();
			return false;
		}
		return true;
	}

	/**
	 * Requ�te permettant de lancer une requ�te
	 * @param querymg
	 * @param requete
	 * @return
	 */
	protected boolean securerequest(String requete)
	{
		int ret = querymg.requete(requete, genericrecord);
		if( ret == -1 ){
			msgErreur += '\n' + querymg.getMsgError();
			return false;
		}
		return true;
	}

	// -- Accesseurs ----------------------------------------------------------

	/**
	 * @param omittedField le omittedField � d�finir
	 */
	public void setOmittedField(String[] omittedField)
	{
		this.omittedField = omittedField;
	}


	/**
	 * @return le genericrecord
	 */
	public GenericRecord getGenericrecord()
	{
		return genericrecord;
	}


	/**
	 * @param genericrecord le genericrecord � d�finir
	 */
	public void setGenericrecord(GenericRecord genericrecord)
	{
		this.genericrecord = genericrecord;
	}


	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
