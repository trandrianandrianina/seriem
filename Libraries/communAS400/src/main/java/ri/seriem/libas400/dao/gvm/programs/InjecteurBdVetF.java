//=================================================================================================
//==>                                                                       11/06/2014 - 03/11/2014
//==> Permet l'injection des commandes  
//==> On utilise le programme CL SGVMIVWSCL de SGVMAS car il effectue un controle des donn�es avant l'injection
//==> A faire:
//==> Exemple:  voir test12 ou test13 (surtout) pour une injection de base (fait main)
//==> 			voir test14 pour l'injection � partir d'un panier
//==> Attention, ne pas travailler dans la QTEMP car SQL et CallPrograme n'ont pas la m�me QTEMP
//==> Notes:
//==> 	Si le java ne vous rend pas la main c'est que le CL est plant� chercher un job QPWFSERVSO
//==> dans le sous-syst�me QSERVER (avec votre profil) en MSGW
//==> 	Soit vous tenez le fichier FCHLOG de la bib TMP???????
//=================================================================================================
package ri.seriem.libas400.dao.gvm.programs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.CallProgram;
import ri.seriem.libas400.system.SystemManager;

public class InjecteurBdVetF
{
	// Variables
	private SystemManager system = null;
	private CallProgram programmeAS = null;
	private QueryManager queryManager = null;
	
	private char letter = 'W';
	private String curlib = null;
	private String wrklib = "QTEMP";
	private boolean gestionComplementEntete = false;

	// Liste des champs obligatoires
	private String[] requiredFieldDBE={"BEETB", "BEMAG", "BEDAT", "BERBV", "BEERL", "BENLI"};
	private String[] requiredFieldDBF={"BFETB", "BFMAG", "BFDAT", "BFRBV", "BFERL", "BFNLI"};
	private String[] requiredFieldDBL={"BLETB", "BLMAGE", "BLDAT", "BLRBV", "BLERL", "BLNLI", "BLQTE"};

	private StringBuilder sbk = new StringBuilder(1024);
	private StringBuilder sbv = new StringBuilder(1024);
	//private LinkedHashMap<String, String> detailErreurs = new LinkedHashMap<String, String>();  
	private ArrayList<String> log = new ArrayList<String>();

	protected String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 


	/**
	 * Constructeur
	 * Si la biblioth�que de travail est la curlib renseigner qd m�me wrklib
	 * @param sys
	 * @param wlib
	 * @param clib
	 * @param let
	 */
	public InjecteurBdVetF(SystemManager sys, String wlib, String clib, char let)
	{
		this(sys, wlib, clib);
		letter = let;
	}

	/**
	 * Constructeur
	 * Si la biblioth�que de travail est la curlib renseigner qd m�me wrklib
	 * @param sys
	 * @param wlib
	 * @param clib
	 */
	public InjecteurBdVetF(SystemManager sys, String wlib, String clib)
	{
		system = sys;
		setWrklib(wlib);
		setCurlib(clib);
	}

	/**
	 * Initialise le d�but des op�rations
	 * @return
	 */
	public boolean init()
	{
		if (curlib == null) return false;
		
		programmeAS = new CallProgram(system.getSystem());
		queryManager = new QueryManager(system.getdatabase().getConnection());
		boolean retour = programmeAS.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('"+letter+"')");
        if (!retour)
        	return false;
		retour = programmeAS.addLibrary("QGPL", false);
		retour = programmeAS.addLibrary("M_GPL", false);
		retour = programmeAS.addLibrary(letter + "EXMAS", false);
		retour = programmeAS.addLibrary(letter + "EXPAS", false);
		retour = programmeAS.addLibrary(letter + "GVMAS", false);
		retour = programmeAS.addLibrary(letter + "GVMX", false); // <- commenter cette ligne pour planter le programme volontairement
		retour = programmeAS.addLibrary(curlib, true);
		return retour;
	}

	/**
	 * Nettoie et cr�er les fichiers d'injection PINJBDV* dans la biblioth�que de travail
	 * @return
	 */
	public boolean clearAndCreateAllFilesInWrkLib()
	{
		return clearAndCreateAllFiles(wrklib);
	}

	/**
	 * Nettoie et cr�er les fichiers d'injection PINJBDV* dans la curlib
	 * @return
	 */
	public boolean clearAndCreateAllFilesInCurLib()
	{
		return clearAndCreateAllFiles(wrklib);
	}

	/**
	 * Nettoie et cr�er les fichiers d'injection PINJBDV* dans la biblioth�que voulue
	 * @param bib
	 * @return
	 */
	public boolean clearAndCreateAllFiles(String bib)
	{
		boolean retour = clearAndCreateFile(bib, "PINJBDVDSE");
		if (retour)
		{
			if(gestionComplementEntete)
				retour = clearAndCreateFile(bib, "PINJBDVDSF"); 
			if(retour)
			{
				retour = clearAndCreateFile(bib, "PINJBDVDSL");
				if (retour)
					retour = clearAndCreateFile(bib, "PINJBDV");
			}
		}
		return retour;
	}
	
	/**
	 * Enregistre une liste d'enregistrements de type E, F ou L dans le pinjbdvdse, pinjbdvsf ou pinjbdvdsl
	 * @param data
	 * @return
	 */
	public boolean insertRecord(ArrayList<GenericRecord> data)
	{
		if ((data == null) || (data.size() == 0)) return false;

		String table=null;
		boolean erreur=false;
		int i = 0;
		for (GenericRecord rcd: data)
		{
			i++;
			// On d�termine le fichier concern�
			//ENTETE DE BON
			if (rcd.isPresentField("BEERL"))
			{
				table = "PINJBDVDSE";
				rcd.setRequiredField(requiredFieldDBE);
			}
			//ENTETE COMPLEMENTAIRE
			else if(rcd.isPresentField("BFERL"))
			{
				table = "PINJBDVDSF";
				rcd.setRequiredField(requiredFieldDBF);
			}
			else
				if (rcd.isPresentField("BLERL"))
				{
					table = "PINJBDVDSL";
					if ( rcd.isPresentField("BLART") )  // Si ce n'est pas un article commentaire 
						rcd.setRequiredField(requiredFieldDBL);
				}
			if (table == null)
			{
				msgErreur += "Le record n�" + i + " n'appartient ni au PINJBDVSE, PINJBDVSF ou PINJBDVSL.\n"; 
				continue;
			}
			
			// Insertion de l'enregistrement
			String requete = rcd.getInsertSQL(table, wrklib);
			if (requete != null)
			{
				int retour = queryManager.requete(requete);
				if (retour == QueryManager.ERREUR)
				{
					msgErreur += "La requ�te (" + requete + ") n'a pas pu �tre ex�cut�.\n" + queryManager.getMsgError() + '\n'; 
					erreur = true;
				}
			}
			else
			{
				msgErreur += "La requ�te pour le record n�" + i + " n'a pas pu �tre g�n�r�e.\n" + rcd.getMsgError() + '\n'; 
				erreur = true;
			}
		}
		return !erreur;
	}
	
	/**
	 * Enregistre un seul enregistrement de type E ou L dans le pinjbdvdse ou pinjbdvdsl
	 * @param data
	 * @return
	 */
	public boolean insertRecord(HashMap <String, Object> data)
	{
		if ((data == null) || (data.size() == 0)) return false;

		// On concat�ne les donn�es r�colt�es
		sbk.setLength(0);
		sbv.setLength(0);
		Object valeur;
		for(Entry<String, Object> entry : data.entrySet())
		{
		    sbk.append(entry.getKey()).append(',');
		    valeur = entry.getValue();
		    if (valeur instanceof String)
		    	sbv.append('\'').append(valeur).append("', ");
		    else
		    	if (valeur instanceof BigDecimal)
		    		sbv.append(((BigDecimal)valeur).doubleValue()).append(", ");
		    	else
		    		sbv.append(valeur).append(", ");
		}
		sbk.deleteCharAt(sbk.length()-1);
		sbv.deleteCharAt(sbv.length()-2);

		// On d�termine le fichier concern�
		String table;
		if (data.get("BEERL") != null)
			table = "PINJBDVDSE";
		else 
			if (data.get("BFERL") != null)
				table = "PINJBDVDSF";
		else
			if (data.get("BLERL") != null)
				table = "PINJBDVDSL";
			else
				return false;
		
		// Construction de la requ�te
		sbk.insert(0, " (").insert(0, table).insert(0, '.').insert(0, wrklib).insert(0, "INSERT INTO ").append(") VALUES (").append(sbv).append(')');
System.out.println("-insertRecord-> sbk.toString(): " + sbk.toString() + " -> sbv.toString(): " + sbv.toString());

		// Insertion de l'enregistrement
		int retour = queryManager.requete(sbk.toString());
		return (retour != QueryManager.ERREUR);
	}
	
	/**
	 * Lancement du programme d'injection (SGVMIVWSCL qui effectue un contr�le des donn�es � injecter)
	 * @return
	 */
	public boolean injectFile(LinkedHashMap<String, String> rapport)
	{
        // On cr�e le param�tre pour l'appel au RPG
		ProgramParameter[] parameterList = new ProgramParameter[2];
        AS400Text XNBON = new AS400Text(11);
        AS400Text BIBTMP = new AS400Text(10);
        parameterList[0] = new ProgramParameter(XNBON.toBytes(String.format("%11s", " ")), 11);
        parameterList[1] = new ProgramParameter(BIBTMP.toBytes(wrklib), 10);
		programmeAS.setParameterList(parameterList);
		programmeAS.setNomPrg("/QSYS.LIB/"+letter+"GVMAS.LIB/SGVMIVWSCL.PGM");
		boolean retour = programmeAS.execute();
		if (!retour)
		{
			msgErreur += "Erreur lors du lancement du programme d'injection du PINJBDV.\n" + programmeAS.getMsgError();
			return retour;
		}
		
		// Analyse des erreurs possibles
		retour = analyseErreurInjection(rapport);
		if (!retour)
		{
			msgErreur += log.size() + " erreurs(s) d�tect�e(s), consulter la hashmap qui contient le d�tail des erreurs.\n"; 
		}
		else // On r�cup�re les informations du bon g�n�r� (num�ro, etb)
		{
			recupereInfosBon(rapport, (String)XNBON.toObject(parameterList[0].getOutputData()));
		}
		
		return retour;
	}

	// --> M�thodes priv�es <--------------------------------------------------

	/**
	 * Analyse le param�tre XNBON du CL SGVMIVWSCL afin de r�cup�rer les informations sur le bon cr��
	 * @param unbon
	 * @param xnbon
	 */
	private void recupereInfosBon(LinkedHashMap<String, String> rapport, String xnbon)
	{
		//rapport.clear();
		if ((xnbon == null) || (xnbon.trim().equals(""))) return;
		
		rapport.put("TYPE", ""+xnbon.charAt(0));			// D pour Devis, ...
		rapport.put("ETB", xnbon.substring(1, 4));
		rapport.put("NUMERO", xnbon.substring(4, 10));
		rapport.put("SUFFIXE", xnbon.substring(10));
	}
	
	/**
	 * Lecture du fichier fchlog suite � l'injection
	 * @param alog
	 */
	private void lectureFchlog()
	{
		log.clear();
		ArrayList<GenericRecord> fchlog = queryManager.select("select * from " + wrklib + ".FCHLOG");
		if (fchlog == null) return;
		
		for (GenericRecord rcd: fchlog)
			log.add((String)rcd.getField(0));
	}

	/**
	 * Analyse des erreurs possibles lors de l'injection (enregistrements du FCHLOG)
	 * D�tail des champs (CSV) du FCHLOG
	 * 1: PINJBDV
	 * 2: Date d'injecttion
	 * 3: Heure d'injection
	 * 4: Code erreur
	 * 5: Cl� de l'enregistrement en erreur
	 * 6: D�tail de l'erreur
	 * @param detailErreurs
	 * @return
	 */
	private boolean analyseErreurInjection(LinkedHashMap<String, String> detailErreurs)
	{
		boolean retour = true;
		
		// Lecture du fichier de log g�n�r� par le RPG 
		lectureFchlog();
		
		// Analyse des donn�es
		int indice=0;
		detailErreurs.clear();
		for (String chaine: log)
		{
			String[] champs = chaine.split(";");
			if (!champs[3].trim().equals(""))
			{
				retour = false;
				String ch_indice = String.format("%02d", ++indice);
				detailErreurs.put(ch_indice + "_CLEF", champs[4]);
				detailErreurs.put(ch_indice + "_CODE_ERREUR", champs[3]);
				detailErreurs.put(ch_indice + "_LIBELLE_ERREUR", champs[5]);
			}
		}
		
		return retour;
	}
	
	/**
	 * Copie les fichiers PINJBDVDSE et PINJBDVDSL dans le PINJBDV
	 * @return
	 */
	private boolean copy2Pinjbdv()
	{
		boolean retour = programmeAS.execute("CPYF FROMFILE("+wrklib+"/PINJBDVDSE) TOFILE("+wrklib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
		String codeErreur = programmeAS.getCodeErreur();
		if (retour && (codeErreur.equals("CPC2955") || codeErreur.equals("CPF4011")))
		{
			retour = programmeAS.execute("CPYF FROMFILE("+wrklib+"/PINJBDVDSL) TOFILE("+wrklib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
			codeErreur = programmeAS.getCodeErreur();
			if (!(retour && (codeErreur.equals("CPC2955") || codeErreur.equals("CPF4011"))))
			{
				msgErreur += "Code erreur:" + codeErreur + " pour "+"CPYF FROMFILE("+wrklib+"/PINJBDVDSL) TOFILE("+wrklib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)";
				return false;
			}
		}
		else
		{
			msgErreur += "Code erreur:" + codeErreur + " pour "+"CPYF FROMFILE("+wrklib+"/PINJBDVDSE) TOFILE("+wrklib+"/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)";
			return false;
		}
		
		return true;
	}
	
	/**
	 * Injecte le fichier PINJBDV
	 * @return
	 */
	private boolean injectePinjbdv()
	{
		boolean retour=true;
		String codeErreur = "";
		
		if (wrklib.equals(curlib)) // On travaille dans la CURLIB
		{
			retour = programmeAS.execute("CALL PGM(SGVMIVCL) PARM('X')");	// <- Ca plante chez nous (msgo CPF4103) ca cache un truc !! (10/01/2013) // Attention peut planter sur le SNDRCVFM (*REQUESTER) car le fichier PSEMSEC n'est pas l� o� il faut... Et M_GPL doit �tre en ligne (sinon �a marche moins bien)
			codeErreur = programmeAS.getCodeErreur();
			if (!retour)
				msgErreur += "Code erreur:" + codeErreur + " pour "+"CALL PGM(SGVMIVCL) PARM('X')";
		}
		else
		{
			retour = programmeAS.execute("CALL PGM(SGVMIVCM) PARM('X' '"+wrklib+"')");
			codeErreur = programmeAS.getCodeErreur();
			if (!retour)
				msgErreur += "Code erreur:" + codeErreur + " pour "+"CALL PGM(SGVMIVCM) PARM('X' '"+wrklib+"')";
		}
		
		return retour;
	}
	
	/**
	 * Nettoie et cr�er le fichier d'injection PINJBDV
	 * @param bib
	 * @param pinjbdv
	 * @return
	 */
	private boolean clearAndCreateFile(String bib, String pinjbdv)
	{
		if (programmeAS == null) return false;
		
		// On cleare le fichier
		boolean retour = programmeAS.execute("CLRPFM FILE("+bib +"/"+pinjbdv+")");
		if (!retour)
		{
			// Il y a eu une erreur, il est possible que le fichier n'existe pas
			if (!programmeAS.getCodeErreur().equals("CPF3101"))
			{
				retour = programmeAS.execute("CRTPF FILE("+bib +"/"+pinjbdv+") SRCFILE("+letter+"GVMAS/QDDSFCH) SRCMBR(*FILE) OPTION(*NOSRC *NOLIST) SIZE(*NOMAX) WAITFILE(*CLS) WAITRCD(*NOMAX)");
			
			}
		}
		
		
		System.out.println(retour + " CRTPF FILE("+bib +"/"+pinjbdv+") ");	
		
		return retour;
	}

	// --> Accesseurs <--------------------------------------------------------
	
	/**
	 * @return le wrklib
	 */
	public String getWrklib()
	{
		return wrklib;
	}

	/**
	 * @param wlib la library de travail
	 */
	public void setWrklib(String wlib)
	{
		this.wrklib = wlib;
	}

	/**
	 * @return le curlib
	 */
	public String getCurlib()
	{
		return curlib;
	}

	/**
	 * @param curlib le curlib � d�finir
	 */
	public void setCurlib(String curlib)
	{
		this.curlib = curlib;
	}

	
	
	/**
	 * @return le detailErreurs
	 *
	public LinkedHashMap<String, String> getDetailErreurs()
	{
		return detailErreurs;
	}*/

	

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	public boolean isGestionComplementEntete()
	{
		return gestionComplementEntete;
	}

	public void setGestionComplementEntete(boolean gestionComplementEntete)
	{
		this.gestionComplementEntete = gestionComplementEntete;
	}

}
