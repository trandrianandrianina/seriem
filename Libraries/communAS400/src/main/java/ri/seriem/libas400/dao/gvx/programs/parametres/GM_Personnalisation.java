//=================================================================================================
//==>                                                                       30/05/2016 - 30/05/2016
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.gvx.programs.parametres;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.Record;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.ExtendRecord;
import ri.seriem.libas400.database.record.GenericRecord;


public class GM_Personnalisation extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param database
	 */
	public GM_Personnalisation(QueryManager aquerymg)
	{
		super(aquerymg);
	}
	
	/**
	 * Retourne la liste des �tablissements
	 * @return
	 */
	public String[] getListeEtablissements() {
		String request = "select paretb  from " + querymg.getLibrary() + ".pgvmparm where partyp = 'DG' and paretb <> ''";
		ArrayList<GenericRecord> listrcd = querymg.select(request);
		if( (listrcd == null) || (listrcd.size() == 0) ){
			return null;
		}
		
		int i = 0;
		String[] liste = new String[listrcd.size()];
		for(GenericRecord rcd : listrcd ){
			liste[i++] = (String) rcd.getField(0);
		}
		
		return liste;
	}

	/**
	 * Retourne la liste des groupes
	 * @param etb
	 * @return
	 **/
	public boolean getListGroupes(String etb, LinkedHashMap<String, String> liste)
	{
		if( liste == null ) return false;
		
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + querymg.getLibrary() + ".PGVMPARM where PARTYP = 'GR' and PARETB = '" + etb + "'", "ri.seriem.libas400.dao.gvx.database.Pgvmparm_ds_GR");
		if( listeRecord == null) return false;
		
		// On alimente la hashmap avec les enregistrements trouv�s
		ExtendRecord record = new ExtendRecord();
		for(Record rcd : listeRecord ) {
			record.setRecord(rcd);
			liste.put(((String)record.getField("INDIC")).substring(5).trim(), ((String)record.getField("GRLIB")).trim());
		}

		return true;
	}

	/**
	 * Retourne la liste des familles
	 * @param etb
	 * @return
	 **/
	public boolean getListFamilles(String etb, LinkedHashMap<String, String> liste)
	{
		if( liste == null ) return false;
		
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + querymg.getLibrary() + ".PGVMPARM where PARTYP = 'FA' and PARETB = '" + etb + "'", "ri.seriem.libas400.dao.gvx.database.Pgvmparm_ds_FA");
		if( listeRecord == null) return false;
		
		// On alimente la hashmap avec les enregistrements trouv�s
		ExtendRecord record = new ExtendRecord();
		for(Record rcd : listeRecord ) {
			record.setRecord(rcd);
			liste.put(((String)record.getField("INDIC")).substring(5).trim(), ((String)record.getField("FALIB")).trim());
		}

		return true;
	}

	/**
	 * Retourne la liste des sous familles
	 * @param etb
	 * @return
	 **/
	public boolean getListSousFamilles(String etb, LinkedHashMap<String, String> liste)
	{
		if( liste == null ) return false;
		
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + querymg.getLibrary() + ".PGVMPARM where PARTYP = 'SF' and PARETB = '" + etb + "'", "ri.seriem.libas400.dao.gvx.database.Pgvmparm_ds_SF");
		if( listeRecord == null) return false;
		
		// On alimente la hashmap avec les enregistrements trouv�s
		ExtendRecord record = new ExtendRecord();
		for(Record rcd : listeRecord ) {
			record.setRecord(rcd);
			liste.put(((String)record.getField("INDIC")).substring(5).trim(), ((String)record.getField("SFLIB")).trim());
		}

		return true;
	}

	/**
	 * Retourne la TVA de l'�tablissement pilote
	 * @return
	 */
	public BigDecimal getTVA1EtablissementPilote()
	{
		BigDecimal tva = null; 
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + querymg.getLibrary() + ".PGVMPARM where PARETB = '' and PARTYP = 'DG' and PARIND = ''", "ri.seriem.libas400.dao.gvx.database.Pgvmparm_ds_DG");
		if( listeRecord == null) return tva;

		// On alimente la hashmap avec les enregistrements trouv�s
		if( listeRecord.size() > 0 ){
			ExtendRecord record = new ExtendRecord();
			record.setRecord(listeRecord.get(0));
			tva  = (BigDecimal) record.getField("DGT01");
		}

		return tva;
	}
	
	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		
	}

}
