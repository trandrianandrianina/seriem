//=================================================================================================
//==>                                                                       26/06/2014 - 04/11/2014
//==> Gestion de la biblioth�que temporaire (�quivalent de la QTEMP)
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.AS400;

public class AlmostQtemp extends Library
{
	// Constantes
	private static final String PREFIX = "TMP";
	
	// Variable
	private int compteur = 0;	// S'auto incr�mente si le nom est d�j� utilis�
	private int id		 = 0;

	/**
	 * Constructeur
	 * @param sys
	 * @param id
	 */
	public AlmostQtemp(AS400 sys, int _id)
	{
		super(sys);
		id = _id;
		createName();
	}

	/**
	 * Cr�er le nom de la biblioth�que
	 * @param id, est une valeur comprise entre 0 et 99999
	 */
	private void createName()
	{
		if (id < 0)
			id = Math.abs(id);
		//String valeur = String.format("%05d%02d", id);
		setName(String.format( "%s%05d%02d", PREFIX, id, compteur ));
	}
	
	/**
	 * Cr�er la biblioth�que si elle n'existe pas
	 * @return
	 */
	@Override
	public boolean create()
	{
		boolean stop = false;
		boolean ret = false;
		do{
			if( !isExists() ){
				ret = createWOCheckExists();
				stop = ret;
			} else{
				compteur++;
				if( compteur < 100 ){
					createName();
				} else{
					stop = true;
				}
			}
		} while( !stop );
		
		return ret;
	}

	
}
