//=================================================================================================
//==>                                                                       21/12/2011 - 10/09/2015
//==> Description de base d'un champ 
//=================================================================================================
package ri.seriem.libas400.database.field;


public abstract class Field<Type>
{
	public String name=null;
	public int length=0;
	public int decimal=0; 
	public String sqltype=null;
	public boolean omitRequest=false;

	public abstract void setValue(Type valeur);
	public abstract Type getValue();
	public abstract Type getTrimValue();
	public abstract String toString();
	public abstract String toFormattedString(int asize, int adecimal);
	public abstract String toFormattedString();
	public abstract void setSize(int asize);
	public abstract void setSize(int asize, int adecimal);
	public abstract void setSQLType(String sqltype);
	public abstract String getSQLType();

	public void setOmitRequest(boolean omit)
	{
		this.omitRequest = omit;
	}

	public boolean isOmitRequest()
	{
		return omitRequest;
	}

}
