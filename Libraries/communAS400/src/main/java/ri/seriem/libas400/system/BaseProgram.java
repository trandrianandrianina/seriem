//=================================================================================================
//==>                                                                       07/12/2011 - 05/01/2016
//==> Programme contenant les m�thodes de base
//=================================================================================================
package ri.seriem.libas400.system;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.HashMap;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.LogMessage;


public abstract class BaseProgram implements PropertyChangeListener
{
	// Constantes
    //private static final String NOM_CLASSE = "[BaseProgram]";
    

	// Variables
	protected SystemManager systeme=null;
	protected String profil=null;
	private String device=null;				// Nom commun des DTAQs
	private String bibenv=null;				// Biblioth�que d'environnement
	private String bibjob=null;				// Biblioth�que o� s'execute les travaux
	//private String typemenu=null;			// D�fini le type de menu (null par d�faut pour les fichiers MNPMSGM)
	private char letasp='W';
	private String dossierRacine=null;		// Dossier racine du serveur de S�rie N
	private String dossierMenus=null;		// Dossier contenant les menus de S�rie N
	
	private String nomDtaqSender=null;		// Nom de la DTAQ �mettrice (toOndeSrv)
	private String nomDtaqRecepter=null;	// Nom de la DTAQ r�ceptrice(fromOndeSrv)
	
    // Infos sur DataQ
	protected DataQManager fromOndeSrv=null;
	protected DataQManager toOndeSrv=null;
	protected boolean waitMsg=true;

	protected String msgErreur = "";		// Conserve le dernier message d'erreur �mit et non lu
	protected LogMessage log=null;

	/**
	 * Constructeur
	 * @param systeme
	 */
	protected BaseProgram(SystemManager psysteme, String profil, String device, String bibenv, char letasp)
	{
		setSystemManagement(psysteme);
		setProfil(profil);
		setDevice(device);
		setBibenv(bibenv);
		setLetasp(letasp);
	}

	/**
	 * Constructeur
	 * @param systeme
	 */
	protected BaseProgram(HashMap<String, Object> parametres)
	{
		if (parametres == null) return;
		setSystemManagement((SystemManager) parametres.get("SYSTEM"));
		setProfil((String) parametres.get("PROFIL"));
		setDevice((String)parametres.get("DEVICE"));
		setBibenv((String)parametres.get("BIBENV"));
		setLetasp(((String)parametres.get("LETASP")).charAt(0));
		setBibjob((String)parametres.get("BIBJOB"));
		//setTypemenu((String)parametres.get("TYPEMENU"));
		setDossierRacine((String)parametres.get("DRACINE"));
	}

	//------- Gestion DTAQs ------------------------------------------------------

	/**
	 * Met en place la gestion des DATQs
	 * @return
	 */
	protected boolean manageDataQueue()
	{
		String bib = getBibjob();
		if ((bib == null) || (bib.trim().equals("")))
			bib = getBibenv();
		fromOndeSrv = new DataQManager(systeme, bib, getNameDtaqRecepter(), Constantes.DATAQLONG);
		toOndeSrv = new DataQManager(systeme, bib, getNameDtaqSender(), Constantes.DATAQLONG);
		if ((fromOndeSrv == null) || (fromOndeSrv.getDtaq() == null)) return false;
		if ((toOndeSrv == null) || (toOndeSrv.getDtaq() == null)) return false;
		
		fromOndeSrv.superviseDataQueue();
		fromOndeSrv.getDtaq().addPropertyChangeListener(this);
		
		return true;
	}

    /**
     * D�connecte les DTAQs si besoin 
     */
    private void disconnectDataQueue()
    {
		if (fromOndeSrv != null) fromOndeSrv.disconnectDataQueue();
		if (toOndeSrv != null) toOndeSrv.disconnectDataQueue();
    }
    
	//------- Accesseurs ------------------------------------------------------

	/**
	 * Connection au serveur
	 * @param psysteme
	 * @return
	 */
	protected void setSystemManagement(SystemManager psysteme)
	{
		if (psysteme == null)
			systeme = new SystemManager(true);
		else
			systeme = psysteme;
	}

	/**
	 * @param profil the profil to set
	 */
	public void setProfil(String profil)
	{
		if (profil != null)
			this.profil = profil.toUpperCase();
		else
			this.profil = profil;
	}

	/**
	 * @return the profil
	 */
	public String getProfil()
	{
		return profil;
	}

	/**
	 * @return the device
	 */
	public String getDevice()
	{
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(String device)
	{
		this.device = device;
		setNameDtaqSender(this.device);
		setNameDtaqRecepter(this.device);
	}

	/**
	 * @return the bibenv
	 */
	public String getBibenv()
	{
		return bibenv;
	}

	/**
	 * @param bibenv the bibenv to set
	 */
	public void setBibenv(String bibenv)
	{
		this.bibenv = bibenv;
	}

	/**
	 * @param letasp the letasp to set
	 */
	public void setLetasp(char letasp)
	{
		this.letasp = letasp;
	}

	/**
	 * @return the letasp
	 */
	public char getLetasp()
	{
		return letasp;
	}

	/**
	 * @return le bibjob
	 */
	public String getBibjob()
	{
		return bibjob;
	}

	/**
	 * @param bibjob le bibjob � d�finir
	 */
	public void setBibjob(String bibjob)
	{
		this.bibjob = bibjob;
	}

	/**
	 * @return le typemenu
	 *
	public String getTypemenu()
	{
		return typemenu;
	}

	/**
	 * @param typemenu le typemenu � d�finir
	 *
	public void setTypemenu(String typemenu)
	{
		this.typemenu = typemenu;
	}*/

	/**
	 * @return le dossierRacine
	 */
	public String getDossierRacine()
	{
		return dossierRacine;
	}

	/**
	 * @param dossierRacine le dossierRacine � d�finir
	 */
	public void setDossierRacine(String dossierRacine)
	{
		this.dossierRacine = dossierRacine;
		setDossierMenus(this.dossierRacine + File.separatorChar + Constantes.DOSSIER_CONFIG + File.separatorChar + Constantes.DOSSIER_MENUS);
	}

	/**
	 * @return le dossierMenus
	 */
	public String getDossierMenus()
	{
		return dossierMenus;
	}

	/**
	 * @param dossierMenus le dossierMenus � d�finir
	 */
	public void setDossierMenus(String dossierMenus)
	{
		this.dossierMenus = dossierMenus;
	}

	/**
	 * @return the nomDtaqSender
	 */
	public String getNameDtaqSender()
	{
		return nomDtaqSender;
	}

	/**
	 * @param nomDtaqSender the nomDtaqSender to set
	 */
	public void setNameDtaqSender(String prefixedtaq)
	{
		if (prefixedtaq == null) 
			nomDtaqSender = null;
		else
		{
			prefixedtaq = prefixedtaq.trim();
			if (prefixedtaq.length() < 10)
				nomDtaqSender = prefixedtaq.toUpperCase() + "S";
			else
				nomDtaqSender = prefixedtaq.substring(0, 10).toUpperCase() + "S";
		}
	}

	/**
	 * @return the nomDtaqRecepter
	 */
	public String getNameDtaqRecepter()
	{
		return nomDtaqRecepter;
	}

	/**
	 * @param nomDtaqRecepter the nomDtaqRecepter to set
	 */
	public void setNameDtaqRecepter(String prefixedtaq)
	{
		if (prefixedtaq == null) 
			nomDtaqRecepter = null;
		else
		{
			prefixedtaq = prefixedtaq.trim();
			if (prefixedtaq.length() < 10)
				nomDtaqRecepter = prefixedtaq.toUpperCase() + "R";
			else
				nomDtaqRecepter = prefixedtaq.substring(0, 10).toUpperCase() + "R";
		}
	}

	/**
	 * Traitement lors de la r�ception d'un message dans la Dataqueue  
	 */
	protected abstract void receptionMessage(String message);
	
	//------- Autres ----------------------------------------------------------

	/**
	 * Traitement principal
	 */
	public boolean treatment()
	{
		return true;
	}
	
	/**
	 * Traite l'�v�nement r�ception d'un message dans la DATQ
	 */
	public void propertyChange(PropertyChangeEvent evt)
	{
		if (evt.getPropertyName().equals("dqRead"))
			receptionMessage((String)evt.getNewValue());
	}

	/**
	 * Ferme proprement le programme
	 */
	protected void destructor()
	{
		disconnectDataQueue();	
		if (systeme != null) systeme.disconnect();
		systeme = null;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
