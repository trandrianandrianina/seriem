package ri.seriem.libas400.dao.exp.programs.contact;

import ri.seriem.libas400.dao.gvm.programs.client.M_Client;
import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;

import java.util.ArrayList;

public class GM_LienContactAvecTiers extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_LienContactAvecTiers(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Controle qu'un contact soit li� � des tiers
	 * @param contact
	 * @return
	 */
	public boolean checkContactWithoutLink(M_Contact contact)
	{
		if( contact == null){
			msgErreur += "\nLa classe M_Contact est nulle";
			return false;
		}
		
		String requete = "select count(*) from " + querymg.getLibrary() + ".PSEMRTLM where RLNUMT = "+contact.getRENUM()+" and RLETBT = '" + contact.getREETB()+"'";
		String resultat = querymg.firstEnrgSelect(requete, 1);
		if( resultat == null )
			return false;
		else
			if( resultat.equals("0") )
				return true;
		return false;
	}
	
	/**
	 * Lecture d'un lot de contacts pour un client
	 * @param client
	 * @return
	 */
	//public M_Contact[] readContact4Client(M_Client client)
	public ArrayList<M_Contact> readContact4Client(M_Client client)
	{
		if( client == null){
			msgErreur += "\nLa classe M_Client est nulle";
			return null;
		}
		return readContacts(M_LienContactAvecTiers.CLIENT, client.getCLETB(), client.getCLIandLIV() );
	}		

	/**
	 * Lecture d'un lot de contacts pour un tiers
	 * @param type
	 * @param etb
	 * @param ind
	 * @return
	 */
	//public M_Contact[] readContacts(char type, String etb, String ind)
	public ArrayList<M_Contact> readContacts(char type, String etb, String ind)
	{
		if( etb == null){
			msgErreur += "\nL'�tablissement est nul";
			return null;
		}
		if( ind == null){
			msgErreur += "\nL'indicatif du tiers est nul";
			return null;
		}
		
		String cond = "RLCOD = '"+type+"' and RLETB='" + etb + "' and RLIND='"+ ind +"'";
		return readInDatabase(cond);
	}		

	/**
	 * Lecture d'un lot de contacts
	 * @param cond sans le "where"
	 * @return
	 */
	//public M_Contact[] readInDatabase(String cond)
	public ArrayList<M_Contact> readInDatabase(String cond)
	{
		String requete = "select rte.*, rtl.*  from " + querymg.getLibrary() + ".PSEMRTLM rtl, "+ querymg.getLibrary()+".PSEMRTEM rte where RENUM = RLNUMT and REETB = RLETBT "; 
		if( !cond.trim().toLowerCase().startsWith("and") )
			requete += " and " + cond;
		else
			requete += " " + cond;
		return request4ReadContacts(requete);
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Requ�te permettant la lecture d'un lot de contacts
	 * @param requete
	 * @return
	 */
	private ArrayList<M_Contact> request4ReadContacts(String requete)
	{
//System.out.println("-GM_LienContactAvecTiers-request4ReadContacts->"+ requete);		

		if(( requete == null ) || (requete.trim().length() == 0)) {
			msgErreur += "\nLa requ�te n'est pas correcte.";
			return null;
		}
		if( querymg.getLibrary() == null ) {
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		// Lecture de la base
		ArrayList<GenericRecord> listrcd = querymg.select(requete);
		if( listrcd == null ){
			return null;
		}
//System.out.println("-GM_LienContactAvecTiers-request4ReadContacts->"+ listrcd.size());		
		// Chargement des classes avec les donn�es de la base
		ArrayList<M_Contact> listc = new ArrayList<M_Contact>(listrcd.size());
		for( GenericRecord rcd : listrcd ){
			M_Contact c = new M_Contact(querymg);
			// zones venant du PSEMTRLE
			c.initObject(rcd, true);
			// zones venant du PSEMTRLM
			M_LienContactAvecTiers lcat = new M_LienContactAvecTiers(querymg);
			lcat.initObject(rcd, true);
			c.getListLienContactTiers().add(lcat);
	
			listc.add(c);
		}
		listrcd.clear();
		
		return listc;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
	}

	// -- Accesseurs ----------------------------------------------------------


}
