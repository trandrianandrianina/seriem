//=================================================================================================
//==>                                                                       ??/??/20?? - 15/01/2015
//==> Gestion des Dataqueue (ATTENTION nouvelle version en cours d'am�lioration)
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.DataQueue;

/**
*La classe DataQ, qui h�rite de la classe DataQueue du JT400, permet d'�tablir une connexion
*entre un AS400 et un PC par le biais des objets AS400 "DataQ". 
*@author Procureur Vincent
*@version 1.0
*
*/
public class DataQ extends DataQueue
{
	// Constantes
    private static final long serialVersionUID = 1L;
    private static final String NOM_CLASSE = "[DataQ]";
    
    public static final String EXTENSION="DTAQ";
    
    // Variables
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private AS400 sysAS400;
    private String messagelu=null;

    /**         
    * DataQ permet de construire l'objet DataQueue qui sera utilis� lors de la communication.       
    * Si la dataqueue sp�cifi�e n'existe pas elle est cr�e avec une taille maximale d�fini par longueur
    * @param <B>sysAS400</B> Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilis�e (l'appelant doit �tre cpnnect�).
    * @param <B>chemin</B> Chemin complet dans l'IFS de la DataQueue � creer (ou � utiliser). Le chemin doit donc imperativement se terminer par un objet de type DTAQ : mon_obj.DTAQ
    * <BLOCKQUOTE><I>Note :</I> Si la Dataqueue sp�cifi�e dans chemin n'existe pas elle sera cr�e.</BLOCKQUOTE>
    */       
    public DataQ(AS400 systeme, String chemin)
    {
        super();        
        try
        {   
            sysAS400 = systeme;
            this.setSystem(sysAS400);
            this.setPath(chemin);        				
        }
        catch (Exception e)
        {
            System.out.println("Probl�me dans le constructeur DataQ : " + e); 
        }
    }

    /**
     * Constructeur
     * @param systeme
     * @param chemin
     */
    public DataQ(SystemManager systememg, String chemin)
    {
       this(systememg.getSystem(), chemin);
    }

    /**         
    * DataQ permet de construire l'objet DataQueue qui sera utilis� lors de la communication.       
    * Si la dataqueue sp�cifi�e n'existe pas elle est cr�e avec une taille maximale d�fini par longueur
    * @param <B>sysAS400</B> Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilis�e (l'appelant doit �tre cpnnect�).
    * @param <B>chemin</B> Chemin complet dans l'IFS de la DataQueue � creer (ou � utiliser). Le chemin doit donc imperativement se terminer par un objet de type DTAQ : mon_obj.DTAQ
    * @param <B>longueur</B> Longueur de l'enregistrement de la DataQueue compris entre 1 et 64512
    * <BLOCKQUOTE><I>Note :</I> Si la Dataqueue sp�cifi�e dans chemin existe elle sera vid�e.</BLOCKQUOTE>
    *       
    public DataQ(AS400 systeme, String chemin, int longueur)
    {
        try
        {   
            sysAS400 = systeme;
            this.setSystem(sysAS400);
            this.setPath(chemin);        				
      	    if (this.exists())
      	    	this.clear();
      	    else
      	    	this.create(longueur, "*ALL", false, true, true, "DataQueue pour SGM");
        }
        catch (Exception e)
        {
            System.out.println("Probl�me dans le constructeur DataQ : " + e); 
        }
    }*/ //Fin du constructeur

    
    /**         
    * DataQ permet de construire l'objet DataQueue qui sera utilis� lors de la communication.       
    * Si la dataqueue sp�cifi�e n'existe pas elle est cr�e avec une taille maximale d�fini par longueur
    * @param <B>sysAS400</B> Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilis�e (l'appelant doit �tre cpnnect�).
    * @param <B>chemin</B> Chemin complet dans l'IFS de la DataQueue � creer (ou � utiliser). Le chemin doit donc imperativement se terminer par un objet de type DTAQ : mon_obj.DTAQ
    * @param <B>longueur</B> Longueur de l'enregistrement de la DataQueue compris entre 1 et 64512
    * <BLOCKQUOTE><I>Note :</I> Si la Dataqueue sp�cifi�e dans chemin existe elle sera vid�e.</BLOCKQUOTE>
    */       
    public DataQ(AS400 systeme, String chemin, int longueur, String commentaire)
    {
        try
        {   
            sysAS400 = systeme;
            this.setSystem(sysAS400);
            this.setPath(chemin);        				
      	    if (this.exists())
      	    	this.clear();
      	    else
      	    	this.create(longueur, "*ALL", false, true, true, commentaire);
        }
        catch (Exception e)
        {
            System.out.println("Probl�me dans le constructeur DataQ : " + e); 
        }
    }

    /**
     * Constructeur
     * @param systememg
     * @param chemin
     * @param longueur
     */
    public DataQ(SystemManager systememg, String chemin, int longueur, String commentaire)
    {
       this(systememg.getSystem(), chemin, longueur, commentaire);
    }
    
    /**         
    * DataQ permet de construire l'objet DataQueue qui sera utilis� lors de la communication.
    * Si la dataqueue sp�cifi�e n'existe pas elle est cr�e avec une taille d�fini par longueur. 
    * @param <B>nom_machine</B> Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilis�e.
    * @param <B>user</B> Nom de l'utilisateur sur l'AS400
    * @param <B>pwd</B> Mot de passe de l'utilisateur sur l'AS400
    * @param <B>chemin</B> Chemin complet dans l'IFS de la DataQueue � creer (ou � utiliser). Le chemin doit donc imperativement se terminer par un objet de type DTAQ : mon_obj.DTAQ
    * @param <B>longueur</B> Longueur de l'enregistrement de la DataQueue compris entre 1 et 64512
    * <BLOCKQUOTE><I>Note :</I> Si la Dataqueue sp�cifi�e dans chemin n'existe pas elle sera cr�e.</BLOCKQUOTE>
    */       
    public DataQ(String nom_machine, String user, String pwd, String chemin, int longueur)
    {
        super();       
        try
        {       
            sysAS400 = new AS400(nom_machine, user, pwd);
            this.setSystem(sysAS400);
//            this.sysAS400 = sysAS400;
            this.setPath(chemin);        				
      	    if (this.exists())
      	    	this.clear();
      	    else
                this.create(longueur);
        }
        catch (Exception e)
        {
            System.out.println("Probl�me dans le constructeur DataQ : " + e); 
        }
    }//Fin du constructeur
       
    /**         
     * writePackedDec permet d'�crire un nombre d�cimal dans la Dataqueue courante.
     * @param <B>taille</B>  contient le nombre de position de la valeur � �crire.<br>Par exemple 1956 => 4
     * @param <B>nbdec</B>  contient le nombre de d�cimale de la valeur � �crire.<br>Par exemple 145.65 => 2
     * @param <B>val</B>  contien la valeur � �crire dans la Dataqueue.
     */        
      public boolean writePackedDec( int taille , int nbdec , double val )  
      {
          boolean res;
          
          try
          {
              AS400PackedDecimal converter = new AS400PackedDecimal(taille,nbdec);
              byte[] tab_bytes = converter.toBytes(val);
              this.write(tab_bytes);
              res = true;
          }
          catch (Exception e)
          {                       
              res = false;
          }
          return res; 
       }


       /**         
       * readPackedDec permet de lire un nombre d�cimal dans la Dataqueue courante.
       * @param <B>taille</B>  contient le nombre de position de la valeur � lire.<br>Par exemple 1956 => 4
       * @param <B>nbdec</B>  contient le nombre de d�cimale de la valeur � lire.<br>Par exemple 145.65 => 2
       * <BLOCKQUOTE><I>Important :</I> les parma�tres 'taille' et 'nbdec' doivent correspondrent exactement � la structure de la zone AS400.</BLOCKQUOTE>
       */        
       public double readPackedDec( int taille , int nbdec )
       {
           double ret;
           
           try
           {
               byte[] val = this.read().getData();
               AS400PackedDecimal converter = new AS400PackedDecimal(taille,nbdec);
               ret = converter.toDouble(val);
           }
           catch (Exception e)
           {                                              
               ret = -1;
           }
           return ret; 
       }


       /**         
       * readString permet de lire une chaine de caract�res dans la Dataqueue courante.
       *
       */        
       public String readString(int attente)
       {
           try
           {
               return(this.read(attente).getString());
           }
           catch (Exception e)
           {
               return null;                                           
           }
       }

       /**         
        * readString permet de lire une chaine de caract�res dans la Dataqueue courante.
        *
        */        
        public void readMessage(int attente)
        {
            try
            {
				setMessageLu(this.read(attente).getString());
            }
            catch (Exception e)
            {
            	//setMessageLu(null);
            	messagelu = null;
            }
        }

       
       /**         
       *  writeString permet d'�crire une chaine de caract�res dans la Dataqueue courante.
       * @param <B>ch</B> chaine � �crire dans la Dataqueue.
       * <BLOCKQUOTE><I>Note :</I> La chaine ne pourra pas d�passer 256 caracteres.</BLOCKQUOTE>
       */        
       public boolean writeString( String ch )  
       {
//           String pb="0";
           boolean res;
           
           try 
           {
//               System.out.println("Chaine : "+ch);
               AS400Text converter = new AS400Text(ch.length(),sysAS400);
//               pb="1";
               byte[]tab = converter.toBytes(ch.trim());
//	             pb="2";
               this.write(tab);
//               pb="3";
               res = true;
           }
           catch (Exception e)
           {
//               System.out.println("Erreur d'ecriture : " +e+"\nprobleme:"+pb);
               res = false;
           }
           return res ;
       }
      
       
       /**         
       *  writeString permet d'�crire une chaine de caract�res dans la Dataqueue courante en sp�cifiant la taille de la chaine.
       * @param <B>ch</B> chaine � �crire dans la Dataqueue.
       * @param <B>taille</B> longueur de la chaine.
       */        
       public boolean writeString(String chaine , int taille)  
       {
           try 
           {
               AS400Text converter = new AS400Text(taille, sysAS400);
               byte[] tableau = converter.toBytes(chaine);
               this.write(tableau);
               return true;
           }
           catch (Exception e)
           {
               return false;
           }
       }       

       
       /**
       * write_fromfile permet d'�crire dans la dataqueue courante le contenu du fichier pass� en paramam�tre.
       * Chaque ligne du fichier constitue une entr�e dans la dataqueue.
       * @param <B>chemin</B> chemin du fichier sur votre disque dur.
       * @param <B>nomfichier</B> nom du fichier qui doit etre copi� dans la dataqueue.
       * @return si 0 tout c'est bien pass�, erreur sinon
       *  <BLOCKQUOTE><I>Note :</I> le chemin doit comporter le s�parateur final.
       */
       public int write_fromfile(String chemin,String nomfichier)
       {
           int ret;
           BufferedReader in=null;
           try
           {
               String s;
               in = new BufferedReader(new FileReader(chemin+nomfichier));
                       
               while ((s=in.readLine())!=null)                       
               {						
                   /*Ecriture d'une chaine*/							  
            	   AS400Text converter = new AS400Text(256, sysAS400);
            	   byte[] tab = converter.toBytes(s.trim());
                   this.write(tab);                                                        
                   //byte[]tab = s.getBytes();
                   //this.write(s.trim());
                   //System.out.println (s + "a ete �cris.");               
               }
               ret = 0;
           }
           catch (FileNotFoundException fnfe)
           {
                ret=1;
           }
           catch(Exception e)
           {
               //System.out.println("Erreur : " + e);
               ret = 2;
           }
           finally{
        	   try{
        		   in.close();
        	   }
        	   catch (IOException e){
        	   }
           }
           return ret;
       }


       public int  read_tofile(String chemin, String nomfichier)
       {
           FileWriter sortie = null ;	
           int ret = 0;
           
           //Methode qui ecrit toutes les entr�es de la dataQ dans un fichier (une ligne par entr�e)    
           try
	   {
               File dest_file = new File(new File(chemin).getPath());
               if (!dest_file.exists())
                   dest_file.mkdirs();
                                                                                                            
               while (true)
               {
                   sortie = new FileWriter(chemin+File.separator+nomfichier,true);
                   String s = this.read().getString();                                                      
                   sortie.write( s +"\n");                               
                   sortie.flush();                                                            
                   sortie.close();
                }			
           }
           catch (NullPointerException npe)
           {  
               System.out.println("La DataQ est vide.");
               try
               {
                   sortie.close();
               } 
               catch(IOException ioe){}	
               ret = 0;
	   }
	   catch (Exception e)
           {
               System.out.println("Erreur de lecture de la DataQ : " + e);
               try
               {
                   sortie.close();
                }
                catch(IOException ioe){}
                ret = 2;
	   }
           return ret ;
	}//Fin read_tofile()
        

	/**
	 * @param messagelu the messagelu to set
	 */
	public void setMessageLu(String msglu)
	{
		String oldMsgLu = messagelu; 
		messagelu = msglu;
		pcs.firePropertyChange("dqRead", oldMsgLu, messagelu);
	}

	/**
	 * @return the messagelu
	 */
	public String getMessageLu()
	{
		return messagelu;
	}

	public void close()
    {
		PropertyChangeListener[] liste = pcs.getPropertyChangeListeners();
		if( liste != null )
			for( PropertyChangeListener l: liste )
				pcs.removePropertyChangeListener( l );
    }        

	/*
    public void Close()
    {
        sysAS400.disconnectAllServices() ;                
    }*/        

	/**
     * Add a PropertyChangeListener for bound property using the services of the
     * PropertyChangeSupport class.
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
          this.pcs.addPropertyChangeListener(listener);
    }

     /**
     * Remove a PropertyChangeListener using the services of the
     * PropertyChangeSupport class.
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
          this.pcs.removePropertyChangeListener(listener);
    }

}        
