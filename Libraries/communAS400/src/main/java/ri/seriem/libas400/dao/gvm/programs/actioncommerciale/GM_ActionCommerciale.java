//=================================================================================================
//==>                                                                       09/09/2015 - 21/10/2015
//==> Gestion des actions commerciales
//=================================================================================================
package ri.seriem.libas400.dao.gvm.programs.actioncommerciale;

import ri.seriem.libas400.dao.gvm.programs.parametres.GM_PersonnalisationGVM;
import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class GM_ActionCommerciale extends BaseGroupDB
{
	// Variables
	private LinkedHashMap<String, String>	listObjectAction 		= new LinkedHashMap<String, String>();
	private String							etablissement			= ""; // ETB � blanc car pour l'instant son int�r�t n'est pas d�montr� et EXP ne g�re pas vraiment l'ETB
	
	
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_ActionCommerciale(QueryManager aquerymg)
	{
		super(aquerymg);
		loadListObjectAction();
	}

	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Initialise l'�tablissemment
	 * @param etb
	 *
	public void initialization(String etb)
	{
		// Initialisation des variables
		//actionCommerciale.initialization();
		
		// Initialisation de l'�tablissement
		setEtablissement(etb);
	}*/
	
	/**
	 * Ins�re une action commerciale dans les tables
	 * @param ac
	 * @return
	 */
	public boolean insertInDatabase(M_ActionCommerciale ac)
	{
		if( ac == null ){
			msgErreur += "\nLa classe ActionCommerciale est nulle.";
			return false;
		}

		if( !ac.insertInDatabase() ){
			return false;
		}

		return true;
	}

	/**
	 * Ins�re une liste d'action commerciale dans les tables
	 * @param lac
	 * @return
	 */
	public boolean insertInDatabase(ArrayList<M_ActionCommerciale> lac)
	{
		if( lac == null ){
			msgErreur += "\nLa liste d'ActionCommerciale est nulle.";
			return false;
		}
		
		for( M_ActionCommerciale ac : lac){
			if( !insertInDatabase(ac) )
				return false;
		}

		return true;
	}

	/**
	 * Lecture d'un lot d'actions commeciales
	 * @param cond sans le "where"
	 * @return
	 */
	public M_ActionCommerciale[] readInDatabase(String cond)
	{
		String requete = "select * from "+ querymg.getLibrary()+".PSEMEVTM evt, "+ querymg.getLibrary()+".PGVMACCM acc where ETID = ACID"; 
		if( (cond != null) && (!cond.trim().equals("")  ) ){
			if( !cond.trim().toLowerCase().startsWith("and") )
				requete += " and " + cond;
			else
				requete += " " + cond;
		}
		return request4ReadActionCommercial(requete);
	}

	/**
	 * Modifie une action commerciale dans les tables
	 * @param ac
	 * @return
	 */
	public boolean modifyInDatabase(M_ActionCommerciale ac)
	{
		if( ac == null ){
			msgErreur += "\nLa classe ActionCommerciale est nulle.";
			return false;
		}

		if( !ac.updateInDatabase() ){
			return false;
		}

		return true;
	}

	/**
	 * Supprime une action commerciale dans les tables
	 * @param ac
	 * @return
	 */
	public boolean deleteInDatabase(M_ActionCommerciale ac)
	{
		if( ac == null ){
			msgErreur += "\nLa classe ActionCommerciale est nulle.";
			return false;
		}

		if( !ac.deleteInDatabase() ){
			return false;
		}

		return true;
	}
	
	/**
	 * Supprime une liste d'action commerciale dans les tables
	 * @param lac
	 * @return
	 */
	public boolean deleteInDatabase(ArrayList<M_ActionCommerciale> lac)
	{
		if( lac == null ){
			msgErreur += "\nLa liste d'ActionCommerciale est nulle.";
			return false;
		}
		
		for( M_ActionCommerciale ac : lac){
			if( !deleteInDatabase(ac) )
				return false;
		}

		return true;
	}

	/**
	 * Charge la liste des objets pour les actions commerciales depuis le fichier param�tre de GVM
	 */
	public void loadListObjectAction()
	{
//System.out.println("-loadListObjectAction->etb:" + etablissement + " " + querymg);		
		GM_PersonnalisationGVM pgvmparm = new GM_PersonnalisationGVM( querymg );
		listObjectAction = pgvmparm.getListObjectAction( etablissement );
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		
		if( listObjectAction != null ){
			listObjectAction.clear();
		}
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Requ�te permettant la lecture d'un lot d'actions commeciales
	 * @param requete
	 * @return
	 */
	private M_ActionCommerciale[] request4ReadActionCommercial(String requete)
	{
		if(( requete == null ) || (requete.trim().length() == 0)) {
			msgErreur += "\nLa requ�te n'est pas correcte.";
			return null;
		}
		if( querymg.getLibrary() == null ) {
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		// Lecture de la base
		ArrayList<GenericRecord> listrcd = querymg.select(requete);
		if( listrcd == null ){
			return null;
		}
		// Chargement des classes avec les donn�es de la base
		int i = 0;
		M_ActionCommerciale[] listac = new M_ActionCommerciale[listrcd.size()];
		for( GenericRecord rcd : listrcd ){
			M_ActionCommerciale ac = new M_ActionCommerciale(querymg);
			ac.initObject(rcd, true);
			// Recup�ration des contacts li�s
			ac.loadContacts( querymg );
			// R�cup�ration des infos du client/prospect
			ac.loadClient( querymg );
			// Chargement des objets et de l'�tablissement
			ac.setListObjectAction(listObjectAction);
			ac.getEvenementContact().setETETB(this.etablissement);
	
			listac[i++] = ac;
		}
		
		return listac;
	}

	//-- Accesseurs -----------------------------------------------------------

	/**
	 * @return le etablissement
	 */
	public String getEtablissement()
	{
		return etablissement;
	}

	/**
	 * @param etablissement le etablissement � d�finir
	 *
	public void setEtablissement(String etablissement)
	{
		this.etablissement = etablissement;
		loadListObjectAction();
	}*/

	/**
	 * @return le listObjectAction
	 */
	public LinkedHashMap<String, String> getListObjectAction()
	{
		return listObjectAction;
	}

	/**
	 * @param listObjectAction le listObjectAction � d�finir
	 */
	public void setListObjectAction(LinkedHashMap<String, String> listObjectAction)
	{
		this.listObjectAction = listObjectAction;
	}


	
}
