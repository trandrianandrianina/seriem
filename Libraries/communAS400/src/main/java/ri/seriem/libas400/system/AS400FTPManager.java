//=================================================================================================
//==>                                                                       25/11/2014 - 28/01/2016
//==> Gestion du FTP (sp�cial AS400)
//==> A faire:
//=================================================================================================
package ri.seriem.libas400.system;

import java.io.IOException;

import com.ibm.as400.access.FTP;


public class AS400FTPManager 
{
	// Variables
	private 	FTP	 		connexion = null;
	protected	String		msgErreur = "";		// Conserve le dernier message d'erreur �mit et non lu 
	
	
	/**
	 * Constructeur
	 */
	public AS400FTPManager()
	{
	}
	
	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Connexion au serveur
	 * @param serveur
	 * @param user
	 * @param password
	 * @return
	 */
	public boolean connexion(String serveur, String user, String password)
	{
		if( !testStringVariable( serveur ) || !testStringVariable( user ) || !testStringVariable( password ) ){
			return false;
		}
		try{
			connexion = new FTP( serveur, user, password ); 
			
			if( (connexion != null) && connexion.noop() ){
				msgErreur += "\nConnexion FTP �tablie";
				return true;
			} else {
				msgErreur += "\nConnexion FTP non �tablie";
				return false;
			}
		}
		catch(Exception e){
			msgErreur += "\nErreur lors de la connexion : " + e;
			return false;
		}
	}

	/**
	 * Teste l'existance d'un fichier
	 * @param remoteFile
	 * @return
	 *
	public boolean exists(String remoteFile)
	{
		try
		{
			FTPFile file = connexion.mlistFile(remoteFile);
			return file != null;
		}
		catch (IOException e)
		{
			msgErreur += "\n" + e;
		}
		return false;
	}*/
	/**
	 * Lancement de commande distante
	 * @param cmd
	 * @return
	 */
	public String remotecmd(String cmd)
	{
		try
		{
			return connexion.issueCommand(cmd);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			msgErreur += "\nErreur lors du lancement de la commande " + cmd + " : " + e;
			return null;
		}
	}

	/**
	 * Liste le contenu d'un dossier
	 * @param folder
	 * @return
	 */
	public String[] listFolder(String folder)
	{
		try
		{
			return connexion.ls(folder);
		}
		catch (IOException e)
		{
			msgErreur += "\n" + e;
		}
		return null;
	}

	/**
	 * Liste le contenu d'un dossier
	 * @param folder
	 * @return
	 */
	public String[] listFolderWithAttribute(String folder)
	{
		try
		{
			return connexion.dir(folder);
		}
		catch (IOException e)
		{
			msgErreur += "\n" + e;
		}
		return null;
	}

	/**
	 * T�l�charge un fichier 
	 * @param remoteFile
	 * @param localFile
	 * @return
	 */
	public boolean downloadFile(String remoteFile, String localFile, boolean isbinary)
	{
		boolean ret=false;
		try
		{
			if( isbinary ){
				connexion.setDataTransferType( com.ibm.as400.access.FTP.BINARY );
			}
			ret = connexion.get(remoteFile, localFile);
			//OutputStream output = new FileOutputStream(localFile);
	        //ret = connexion.retrieveFile(remoteFile, output);
		}
		catch (Exception e)
		{
			msgErreur += "\n" + e;
		}
		return ret;
	}

	/**
	 * T�l�verse un fichier
	 * @param localFile
	 * @param remoteFile
	 * @return
	 */
	public boolean uploadFile(String localFile, String remoteFile, boolean isbinary)
	{
		boolean ret=false;
		try
		{
			if( isbinary ){
				connexion.setDataTransferType( com.ibm.as400.access.FTP.BINARY );
			}
			ret = connexion.put(localFile, remoteFile);
			//InputStream input = new FileInputStream(localFile);
			//ret = connexion.storeFile(remoteFile, input);
	        //input.close();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + e;
		}
		return ret;
	}
	
	/*
	public void as400specific()
	{
		try
		{
			connexion.enterLocalPassiveMode();
			System.out.println("-bin-" + connexion.sendSiteCommand("bin") );
			System.out.println("-bin-" + connexion.sendCommand("bin") );
			System.out.println("-nam-" + connexion.sendSiteCommand("nam 1") );
			System.out.println("-nam-" + connexion.sendCommand("nam 1") );
		}
		catch (IOException e)
		{
			// TODO Bloc catch g�n�r� automatiquement
			e.printStackTrace();		}
	}*/

	/**
	 * D�connexion
	 */
	public void disconnect()
	{
		try
		{
			connexion.disconnect();
		}
		catch(Exception e)
		{
			msgErreur += "\nErreur d�connexion: " + e;
		}
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Teste la validit� des variables de type String
	 * @param varname
	 * @param varvalue
	 * @return
	 */
	private boolean testStringVariable( String varvalue )
	{
		if( ( varvalue == null ) || ( varvalue.trim().equals( "" ) ) ){
			msgErreur += "\nUne ou plusieurs variables incorrectes.";
			return false;
		}
		return true;	
	}

	// -- Accesseurs ----------------------------------------------------------
	
	public FTP getConnexion()
	{
		return connexion;
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
