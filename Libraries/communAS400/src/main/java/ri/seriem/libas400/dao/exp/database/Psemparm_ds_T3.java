//=================================================================================================
//==>                                                                       04/02/2014 - 04/02/2014
//==> Description de l'enregistrement du fichier Psemparm_ds_T3 pour les T3
//==> G�n�r� avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Psemparm_ds_T3 extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD7"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF7"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN7"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD8"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF8"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN8"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD9"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF9"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN9"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD10"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF10"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN10"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD11"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF11"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN11"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD12"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF12"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN12"));

		length = 200;
	}
}
