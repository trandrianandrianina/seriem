//=================================================================================================
//==>                                                                       09/09/2015 - 16/09/2015
//==> Traitement pour UNE action commerciale 
//=================================================================================================
package ri.seriem.libas400.dao.gvm.database.files;


import ri.seriem.libas400.database.BaseFileDB;
import ri.seriem.libas400.database.QueryManager;

/**
* Classe contenant les zones du PGVMACCM
* @author Administrateur
*
*/
public abstract class FFD_Pgvmaccm extends BaseFileDB
{
	// Constantes (valeurs r�cup�r�es via DSPFFD)
	public static final int	SIZE_ACID		= 9;
	public static final int	DECIMAL_ACID	= 0;
	public static final int	SIZE_ACOBJ		= 5;
	public static final int	SIZE_ACEBCN		= 6;
	public static final int	SIZE_ACEBCC		= 1;
	public static final int	DECIMAL_ACEBCN	= 0;
	public static final int	SIZE_ACEBCS		= 1;
	public static final int	DECIMAL_ACEBCS	= 0;
	public static final int	SIZE_ACAFN		= 6;
	public static final int	DECIMAL_ACAFN	= 0;
	
	// Variables fichiers
	protected int			ACID	= 0;		// ID, le m�me que ETID puisque c'est du compl�ment d'information
	protected String		ACOBJ	= null;		// Objet (Visite de courtoisie/devis/commande/r�cup�ration de r�glement/...) - stock� dans le PGVMPARM param�tre AA
	protected char			ACEBCC	= ' ';		// Code ERL (devis/commande)
	protected int			ACEBCN	= 0;		// Num�ro de bon (devis/commande)
	protected byte			ACEBCS	= 0;		// Suffixe (devis/commande)
	protected int			ACAFN	= 0;		// Num�ro d'ordre affaire
	

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public FFD_Pgvmaccm(QueryManager aquerymg)
	{
		super(aquerymg);
	}


	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Initialise les variables avec les valeurs par d�faut
	 */
	public void initialization()
	{
		ACID	= 0;
		ACOBJ	= null;		// Objet (Visite de courtoisie/devis/commande/r�cup�ration de r�glement/...) - stock� dans le PGVMPARM param�tre AA
		ACEBCC	= ' ';		// Code ERL (devis/commande)
		ACEBCN	= 0;
		ACEBCS	= 0;
		ACAFN	= 0;
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le aCID
	 */
	public int getACID()
	{
		return ACID;
	}
	/**
	 * @param aCID le aCID � d�finir
	 */
	public void setACID(int aCID)
	{
		ACID = aCID;
	}
	/**
	 * @return le aCOBJ
	 */
	public String getACOBJ()
	{
		return ACOBJ;
	}
	/**
	 * @param aCOBJ le aCOBJ � d�finir
	 */
	public void setACOBJ(String aCOBJ)
	{
		ACOBJ = aCOBJ;
	}
	/**
	 * @return le aCEBCC
	 */
	public char getACEBCC()
	{
		return ACEBCC;
	}
	/**
	 * @param aCEBCC le aCEBCC � d�finir
	 */
	public void setACEBCC(char aCEBCC)
	{
		ACEBCC = aCEBCC;
	}
	/**
	 * @return le aCEBCS
	 */
	public byte getACEBCS()
	{
		return ACEBCS;
	}
	/**
	 * @param aCEBCS le aCEBCS � d�finir
	 */
	public void setACEBCS(byte aCEBCS)
	{
		ACEBCS = aCEBCS;
	}

	/**
	 * @return le aCEBCN
	 */
	public int getACEBCN()
	{
		return ACEBCN;
	}

	/**
	 * @param aCEBCN le aCEBCN � d�finir
	 */
	public void setACEBCN(int aCEBCN)
	{
		ACEBCN = aCEBCN;
	}

	/**
	 * @return le aCAFN
	 */
	public int getACAFN()
	{
		return ACAFN;
	}

	/**
	 * @param aCAFN le aCAFN � d�finir
	 */
	public void setACAFN(int aCAFN)
	{
		ACAFN = aCAFN;
	}

}
