package ri.seriem.libas400.dao.gvm.programs.client;

import ri.seriem.libas400.dao.exp.programs.contact.GM_LienContactAvecTiers;
import ri.seriem.libas400.dao.exp.programs.contact.M_Contact;
import ri.seriem.libas400.dao.gvm.database.files.FFD_Pgvmclim;
import ri.seriem.libas400.database.QueryManager;

import java.util.ArrayList;

public class M_Client extends FFD_Pgvmclim
{
	// Variables
	private ArrayList<M_Contact>			listContact			= null;


	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public M_Client(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Ins�re l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean insertInDatabase()
	{
		initGenericRecord(genericrecord, true);
		String requete = genericrecord.createSQLRequestInsert("PGVMCLIM", querymg.getLibrary());
		return request(requete);
	}


	/**
	 * Modifie l'enregistrement dans le table
	 * @return
	 */
	@Override
	public boolean updateInDatabase()
	{
		initGenericRecord(genericrecord, false);
		String requete = genericrecord.createSQLRequestUpdate("PGVMCLIM", querymg.getLibrary(), "CLNUM=" + getCLCLI() + " and CLLIV=" + getCLLIV() + " and CLETB='" + getCLETB() + "'");
		return request(requete);
	}


	/**
	 * Suppression de l'enregistrement courant
	 * @return
	 */
	@Override
	public boolean deleteInDatabase()
	{
		return deleteInDatabase(false);
	}

	/**
	 * Suppression de l'enregistrement courant ou de tous
	 * @return
	 */
	public boolean deleteInDatabase(boolean deleteAll)
	{
		String requete = "delete from " + querymg.getLibrary() + ".PGVMCLIM where CLNUM=" + getCLCLI() + " and CLETB='" + getCLETB() + "'";
		if( !deleteAll )
			requete += " and CLNUM=" + getCLCLI();
		return request(requete);
	}

	/**
	 * Initialise l'objet avec les donn�es du record
	 * @param rcd
	 * @param doinit
	 * @return
	 *
	public boolean initObject(GenericRecord rcd, boolean doinit)
	{
		boolean ret = super.initObject(rcd, doinit);
		if( ret )
			ret = loadContacts();
		return ret;
	}*/
	
	/**
	 * R�cup�ration des contacts pour le client (Attention il est possible que le client n'est pas de contact)
	 * @param id
	 * @return
	 */
	public boolean loadContacts()
	{
		GM_LienContactAvecTiers lct = new GM_LienContactAvecTiers( querymg );
		
		// Chargement des classes avec les donn�es de la base
		if( listContact != null){
			listContact.clear();
		}
		listContact = lct.readContact4Client( this );
		return true;
	}
	
	/**
	 * Retourne le num�ro client et livr� concat�n�s
	 * @return
	 */
	public String getCLIandLIV()
	{
		return String.format("%0"+M_Client.SIZE_CLCLI+"d%0"+M_Client.SIZE_CLLIV+"d", getCLCLI(), getCLLIV());
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		genericrecord.dispose();
		
		if( listContact != null ){
			for( M_Contact c : listContact )
				c.dispose();
			listContact.clear();
		}
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le listContact
	 */
	public ArrayList<M_Contact> getListContact(boolean refresh)
	{
		if( refresh )
			loadContacts();
		return listContact;
	}

	/**
	 * @param listContact le listContact � d�finir
	 *
	public void setListContact(ArrayList<M_Contact> listContact)
	{
		this.listContact = listContact;
	}*/


}
