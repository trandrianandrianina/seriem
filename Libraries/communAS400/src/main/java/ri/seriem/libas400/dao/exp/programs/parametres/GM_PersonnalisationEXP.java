//=================================================================================================
//==>                                                                       21/10/2015 - 21/10/2015
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.exp.programs.parametres;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.Record;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.ExtendRecord;


public class GM_PersonnalisationEXP extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param database
	 */
	public GM_PersonnalisationEXP(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Retourne la liste des civilit�es possibles
	 * @param etb
	 * @return
	 **/
	public LinkedHashMap<String, String> getListCivility(String etb)
	{
		LinkedHashMap<String, String>listCivility = new LinkedHashMap<String, String>();
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARFIL1, PARCLE, PARFIL from " + querymg.getLibrary() + ".PSEMPARM where substring(PARCLE, 4, 2) = 'CI'", "ri.seriem.libas400.dao.exp.database.Psemparm_ds_CI");

		// On alimente la hashmap avec les enregistrements trouv�s
		if( listeRecord == null) return listCivility;
		
		ExtendRecord record = new ExtendRecord();
		for(Record rcd : listeRecord ) {
			record.setRecord(rcd);
			listCivility.put(((String)record.getField("INDIC")).substring(5, 8).trim(), ((String)record.getField("CILIB")).trim());
		}

		return listCivility;
	}

	/**
	 * Retourne la liste des fonctions possibles
	 * @param etb
	 * @return
	 **/
	public LinkedHashMap<String, String> getListClass(String etb)
	{
		LinkedHashMap<String, String>listClass = new LinkedHashMap<String, String>();
		final ArrayList<Record> listeRecord = querymg.select("Select PARTOP, PARFIL1, PARCLE, PARFIL from " + querymg.getLibrary() + ".PSEMPARM where substring(PARCLE, 4, 2) = 'CC'", "ri.seriem.libas400.dao.exp.database.Psemparm_ds_CC");

		// On alimente la hashmap avec les enregistrements trouv�s
		if( listeRecord == null) return listClass;
		
		ExtendRecord record = new ExtendRecord();
		for(Record rcd : listeRecord ) {
			record.setRecord(rcd);
			listClass.put(((String)record.getField("INDIC")).substring(5, 8).trim(), ((String)record.getField("CCLIB")).trim());
		}

		return listClass;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		querymg = null;
		
	}

}
