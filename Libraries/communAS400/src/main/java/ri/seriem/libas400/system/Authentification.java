//=================================================================================================
//==>                                                                       ??/??/2005 - 14/11/2014
//==> Description de l'authentification
//==> A faire:
//==>    - Si le profil & le mot de passe incorrect alors la mire d'IBM s'affiche (g�nant si serveur Tiers)
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.AS400;

import ri.seriem.libcommun.outils.DateHeure;

public class Authentification
{
    // Variables
    private String profil=null;
    private String motdepasse=null;
    private String machine="localhost";
    private AS400 systeme=null;

    /**
     * Constructeur de la classe
     * @param prf
     * @param mdp
     */
    public Authentification(String prf, String mdp)
    {
        profil = prf;
        motdepasse = mdp;
    }

    /**
     * Constructeur de la classe
     * @param sys
     * @param prf
     * @param mdp
     */
    public Authentification(String sys, String prf, String mdp)
    {
        this(prf, mdp);
        machine = sys;
    }

    /**
     * Connexion � l'AS/400
     * @return
     */
    public AS400 Connexion()
    {
        try
        {
            // On tente de se connecter � l'AS/400
            systeme = new AS400(machine, profil, motdepasse);
            if (systeme.validateSignon())
            	return systeme;
            else
            	return null;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    /**
     * D�connexion de l'AS/400
     */
    public void Deconnexion()
    {
        systeme.disconnectAllServices();
        systeme = null;
    }

    /**
     * Retourne la date de la derni�re connexion
     * @return
     */
    public String lastConnexion()
    {
    	try
    	{
    		// On a pu se connecter, on retourne la date de la derni�re connexion r�ussie
    		if (systeme != null)
    			return DateHeure.getJourHeure(0, systeme.getPreviousSignonDate().getTime());
    		else
    			return null;
    	}
    	catch(Exception e)
    	{
    		return null;
    	}
    }

}