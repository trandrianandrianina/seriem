//=================================================================================================
//==>                                                                       11/09/2015 - 11/09/2015
//==> Description de l'enregistrement du fichier Pgvmparm_ds_AA pour les AA
//==> G�n�r� avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
//=================================================================================================
package ri.seriem.libas400.dao.gvm.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_AA extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "AALIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(100), "AATIT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "AATYP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "AALET"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "AAHTM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "AAFRM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "AAOFV"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "AAEVE"));

		length = 300;
	}
}
