//=================================================================================================
//==>                                                                       13/11/2008 - 14/11/2014
//==> Gestion des fichiers spools pour une outq sur un AS/400 
//==>
//==> A faire:
//==>  Ajouter une m�thode qui construit le message d'erreur avec gestion d'un mode debug
//=================================================================================================

package ri.seriem.libas400.system;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Enumeration;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Exception;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ConnectionDroppedException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.OutputQueue;
import com.ibm.as400.access.PrintObject;
import com.ibm.as400.access.PrintObjectTransformedInputStream;
import com.ibm.as400.access.PrintParameterList;
import com.ibm.as400.access.RequestNotSupportedException;
import com.ibm.as400.access.SpooledFile;
import com.ibm.as400.access.SpooledFileList;
import com.ibm.as400.access.SpooledFileOutputStream;

import ri.seriem.libcommun.edition.ListContains;
import ri.seriem.libcommun.edition.Spool;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.DateHeure;

public class GestionSpoolAS400
{
	// Constantes
	private final static String NOM_CLASSE = "[GestionSpoolAS400]";
	public final static String PLAIN_TEXT = "/QSYS.LIB/QWPDEFAULT.WSCST";
	public final static String PLAIN_GIF = "/QSYS.LIB/QWPGIF.WSCST";
	public final static String PLAIN_EURO = "/QSYS.LIB/QWPHPEURO.WSCST";
	
	private final static char CR=0x0d;
	private final static char LF=0x0a;
	private final static char FF=0x0c;
	
	private final static String[] titres = new String[] {"Fichier", "Utilisateur", "R\u00e9f\u00e9rence", "Etat", "Pages", "Travail nom", "Travail num", "Spool num", "Date", "Heure"};
	private final static String[] types = new String[] {"String", "String", "String", "String", "Integer", "String", "String", "Integer", "Date", "String"};

	//Variables
	private AS400 systeme=null;
	private OutputQueue outq =null;
	private String nomOutq=null;
	private String codepage="CP858"; // ou CP858
	private SpooledFileList splfList=null;
	private Gson gson = new Gson();

	
	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu

	/**
	 * Constructeur
	 */
	public GestionSpoolAS400()
	{
		systeme = new AS400();
	}

	/**
	 * Constructeur
	 * @param systeme
	 */
	public GestionSpoolAS400(AS400 systeme)
	{
		if (systeme == null)
			this.systeme = new AS400();
		else
			this.systeme = systeme;
	}

	/**
	 * Constructeur
	 * @param host
	 * @param user
	 * @param mdp
	 */
	public GestionSpoolAS400(String host, String user, String mdp)
	{
		systeme = new AS400(host, user, mdp);
	}

	/**
	 * D�connecte la session
	 */
	public void deconnecter()
	{
		if (systeme != null)
			systeme.disconnectAllServices();
		systeme = null;
	}
	
	/**
	 * Retourne la valeur de syst�me 
	 * @return
	 */
	public AS400 getSystem()
	{
		if (systeme == null) systeme = new AS400();
		return systeme;
	}
	
	/**
	 * Retourne le nom du spool � partir de son indice (dans la outq)
	 * @param indice
	 * @return
	 */
	public SpooledFile getSpooledFile(int indice)
	{
		if ((splfList == null) || (indice < 0) || (splfList.size() <= indice)) return null;
		return ((SpooledFile)splfList.getObject(indice));
	}

	/**
	 *  Met le spool en premier dans sa outq
	 * @param splf
	 * @return
	 */
	public boolean moveTop(SpooledFile splf)
	{
		if (splf == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (moveTop) Spool null.";
			return false;
		}
		try
		{
			splf.moveToTop();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (moveTop) " + e;
			return false;
		}
		return true;
	}

	/**
	 *  Met le spool en premier dans sa outq
	 * @param splf
	 * @return
	 */
	public boolean move(SpooledFile splf, OutputQueue targetOutputQueue, int exemplaire)
	{
		// Contr�le des param�tres
		if (splf == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (move) Le spool est � null.";
			return false;
		}
		if (targetOutputQueue == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (move) La outqueue est � null.";
			return false;
		}
		if (exemplaire <= 0) exemplaire = 1;
		IFSFile outq = new IFSFile(systeme, targetOutputQueue.getPath());
		try
		{
			if (!outq.exists())
			{
				msgErreur += "\n" + NOM_CLASSE + " (move) La outqueue " + outq.getAbsolutePath() + " n'existe pas.";
				return false;
			}
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (move) Probl�me lors de la v�rification de la outqueue " + outq.getAbsolutePath() + ".";
			return false;
		}
		
		// Envoi le spool dans une outq avec le nombre d'exemplaire voulus 
		try
		{
			splf.move(targetOutputQueue);
			if (exemplaire > 1)
			{
				PrintParameterList parms = new PrintParameterList();
				parms.setParameter(PrintObject.ATTR_COPIES, exemplaire);
				splf.setAttributes(parms);
			}
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (move) " + e;
			return false;
		}
		return true;
	}

	/**
	 * G�n�re un spooledfile pour le premier spool de la OUTQ
	 */
	public SpooledFile creationSpoolF() throws AS400Exception,
			ConnectionDroppedException, AS400SecurityException,
			ErrorCompletingRequestException, InterruptedException, IOException,
			RequestNotSupportedException
	{
		SpooledFileList splfList = new SpooledFileList(systeme);
		splfList.openSynchronously();
		Enumeration<?> iter = splfList.getObjects();
		SpooledFile splf = (SpooledFile) iter.nextElement();
		splf = new SpooledFile(systeme, splf.getName(), splf.getNumber(), splf.getJobName(), splf.getJobUser(), splf.getJobNumber());

		System.out.println(splf.getJobUser());
		System.out.println(splf.getName());
		System.out.println(splf.getNumber());
		System.out.println(splf.getJobName());
		System.out.println(splf.getJobNumber());

		return (splf);

	}

	/**
	 * Suspend le spool
	 *     typhold peut avoir 2 valeurs:  * '*IMMED' ou '*PAGEEND'
	 *     Le fichier doit �tre suspendu au plus t�t 			-> *IMMED
	 *     Le fichier doit �tre suspendu � une limite de page	-> *PAGEEND
	 */
	public boolean suspendSpool(SpooledFile splf, String typhold)
	{
		if (splf == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (suspendSpool) Spool null.";
			return false;
		}
		try
		{
			splf.hold(typhold);
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (suspendSpool) " + e;
			return false;
		}
		return true;
	}

	/**
	 * Lib�re le spool
	 * @param splf
	 * @return
	 */
	public boolean libereSpool(SpooledFile splf)
	{
		if (splf == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (libereSpool) Spool null.";
			return false;
		}
		try
		{
			splf.release();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (libereSpool) " + e;
			return false;
		}
		return true;
	}

	/**
	 * Supprime le spool
	 * @param splf
	 * @param typhold
	 * @return
	 */
	public boolean supprimeSpool(SpooledFile splf, String typhold)
	{
		if (splf == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (supprimeSpool) Spool null.";
			return false;
		}
		try
		{
			splf.delete();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (supprimeSpool) " + e;
			return false;
		}
		return true;
	}
	
	/**
	 * Lecture Message d'un spool
	 */
	public String messageSpool(SpooledFile splf, boolean ireply, String reply)
	{
		String message=null;
		try
		{
			if (ireply)
			{
				splf.answerMessage(reply);
				message = "";
			}
			else
			{
				AS400Message msg = splf.getMessage();
				message = splf.getJobNumber()+"/"+splf.getJobUser()+"/"+splf.getJobName() + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getID() + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getSeverity() + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getText() + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getHelp() + Constantes.SEPARATEUR_CHAINE_CHAR + DateHeure.getJourHeure(0, msg.getDate().getTime());
			}
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (messageSpool) " + e;
		}
		return message;
	}

/***************************************************************************
****************************************************************************
******************************* chgsplfa spool******************************
ATTR_ALIGN - Align Page 
ATTR_BACK_OVERLAY - Back Overlay Integrated File System Name 
ATTR_BKOVL_DWN - Back Overlay Offset Down 
ATTR_BKOVL_ACR - Back Overlay offset across 
ATTR_COPIES - Copies 
ATTR_ENDPAGE - Ending Page 
ATTR_FILESEP - File Separators 
ATTR_FORM_DEFINITION - Form definition Integrated File System Name 
ATTR_FORMFEED - Form Feed 
ATTR_FORMTYPE - Form Type 
ATTR_FRONTSIDE_OVERLAY - Front overlay Integrated File System Name 
ATTR_FTOVL_ACR - Front Overlay Offset Across 
ATTR_FTOVL_DWN - Front Overlay Offset Down 
ATTR_OUTPTY - Output Priority 
ATTR_OUTPUT_QUEUE - Output Queue Integrated File System Name 
ATTR_MULTIUP - Pages Per Side 
ATTR_FIDELITY - Print Fidelity 
ATTR_DUPLEX - Print on Both Sides 
ATTR_PRTQUALITY - Print Quality 
ATTR_PRTSEQUENCE - Print Sequence 
ATTR_PRINTER - Printer 
ATTR_RESTART - Restart Printing 
ATTR_SAVE - Save Spooled File 
ATTR_SCHEDULE - Spooled Output Schedule 
ATTR_STARTPAGE - Starting Page 
ATTR_USERDATA - User Data 
ATTR_USRDEFOPT - User defined option(s) 
ATTR_USER_DEFINED_OBJECT - User defined object Integrated File System Name 
***************************************************************************
**************************************************************************/
	public void chgattSpool(SpooledFile splf)
	{
		try {

			String mess;
			String rep;
			AS400Message msg = splf.getMessage();
			mess = msg.getText();
			System.out.println(mess);
			System.out.println("Repondre au message svp ?");
			Reader reader = new InputStreamReader(System.in); 
			BufferedReader input = new BufferedReader(reader); 
			rep = input.readLine(); 
			splf.answerMessage(rep);
		} catch (Exception e) {
			System.out.println("[GestionSpoolAS400] (chgattSpool) Spool non trouv�.");

		}
	}

	/**
	 * Copie un fichier dans un spool
	 * @param fichier
	 * @param nomspool
	 * @param nbrcopy
	 * @param suspendre
	 * @param qualite
	 * @param priorite
	 * @param scriptavant
	 * @param scriptapres
	 * @param tiroirEntree - Non utilis� pour l'instant car non pris en compte si pdf ou ps
	 * @param rectoverso   - Non utilis� pour l'instant car non pris en compte si pdf ou ps
	 * @return
	 *	//Attributs ici : http://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzahh/javadoc/PrintAttributes.html
	 */
	public SpooledFile cpyFichier2Spool(String fichier, String nomspool, int nbrcopy, boolean suspendre, String qualite, String priorite, String scriptavant, String scriptapres/*int tiroirEntree, boolean rectoverso*/)
	{
		byte[] buf = new byte[65536];
		int bytesRead;
		SpooledFileOutputStream fileout =null;
		FileInputStream in=null;

		// Contr�le variables
		if (outq == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (cpyFichier2Spool) La outq n'est pas initialis�e.";
			return null;
		}
		if (fichier == null) return null;

		// Initialisation des param�tres du spool
		PrintParameterList parms = new PrintParameterList();
		if (nomspool != null)
			parms.setParameter(PrintObject.ATTR_SPOOLFILE , nomspool);
		parms.setParameter(PrintObject.ATTR_COPIES, nbrcopy<=0?1:nbrcopy);
		parms.setParameter(PrintObject.ATTR_HOLD, suspendre?"*YES":"*NO");
		parms.setParameter(PrintObject.ATTR_OUTPUT_QUEUE, outq.getPath());
		//parms.setParameter(PrintObject.ATTR_DUPLEX, rectoverso?"*YES":"*NO");		// Recto Verso
		//parms.setParameter(PrintObject.ATTR_SRCDRWR, tiroirEntree);				// Tiroir entr�e
		if (qualite != null)
			parms.setParameter(PrintObject.ATTR_PRTQUALITY, qualite);				// Qualit� d'impression : *STD, *DRAFT, *NLQ, *FASTDRAFT
		if (priorite != null)
			parms.setParameter(PrintObject.ATTR_OUTPTY, priorite);					// Priorit� : 1 (haute) � 9 (basse)
		
		// Cr�ation du fichier spool
		try
		{
			//PrinterFile pf = new PrinterFile(systeme, "/QSYS.LIB/SVAS.LIB/BONA4.FILE");
			fileout = new SpooledFileOutputStream(systeme, parms, null, outq);
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (cpyFichier2Spool) Erreur lors de l'ouverture du fichier spool : " + e.getMessage();
			return null;
		}

		// Copie du contenu du fichier dans le spool
		try
		{
			in = new FileInputStream(fichier);
			if (scriptavant != null) fileout.write(scriptavant.getBytes()); 
			do
			{
				bytesRead = in.read(buf);
				if (bytesRead != -1)
					fileout.write(buf, 0, bytesRead);
			}
			while (bytesRead != -1);
			if (scriptapres != null) fileout.write(scriptapres.getBytes());
			fileout.close();
			in.close();
			return fileout.getSpooledFile();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (cpyFichier2Spool) Erreur lors de la copie du fichier spool : " + e.getMessage();
			try
			{
				fileout.close();
				in.close();
			}
			catch(Exception ee) {}
			return null;
		}
	}

	/**
	 * Copie une liste d'objet dans un spool
	 * @param lbuffer
	 * @param outq
	 * @param nomspool
	 * @param nbrcopy
	 * @param suspendre
	 * @param qualite
	 * @param priorite
	 * @param tiroirEntree - Non utilis� pour l'instant car non pris en compte si pdf ou ps
	 * @param rectoverso   - Non utilis� pour l'instant car non pris en compte si pdf ou ps
	 * @return
	 */
	public SpooledFile cpyFichier2Spool(ArrayList<?> lbuffer, String nomspool, int nbrcopy, boolean suspendre, String qualite, String priorite, int tiroirEntree, boolean rectoverso)
	{
		int i=0;
		SpooledFileOutputStream fileout =null;
		
		// Contr�le variables
		if (outq == null) return null;
		if ((lbuffer == null) || (lbuffer.size() == 0)) return null;

		// Initialisation des param�tres du spool
		PrintParameterList parms = new PrintParameterList();
		if (nomspool != null)
			parms.setParameter(PrintObject.ATTR_SPOOLFILE , nomspool);
		parms.setParameter(PrintObject.ATTR_COPIES, nbrcopy<=0?1:nbrcopy);
		parms.setParameter(PrintObject.ATTR_HOLD, suspendre?"*YES":"*NO");
		parms.setParameter(PrintObject.ATTR_OUTPUT_QUEUE, outq.getPath());
		//parms.setParameter(PrintObject.ATTR_DUPLEX, rectoverso?"*YES":"*NO");		// Recto Verso
		//parms.setParameter(PrintObject.ATTR_SRCDRWR, tiroirEntree);				// Tiroir entr�e
		if (qualite != null)
			parms.setParameter(PrintObject.ATTR_PRTQUALITY, qualite);				// Qualit� d'impression : *STD, *DRAFT, *NLQ, *FASTDRAFT
		if (priorite != null)
			parms.setParameter(PrintObject.ATTR_OUTPTY, priorite);					// Priorit� : 1 (haute) � 9 (basse)

		// Cr�ation du fichier spool
		try
		{
			fileout = new SpooledFileOutputStream(systeme, parms, null, outq);
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (cpyFichier2Spool) Erreur lors de l'ouverture du fichier spool : " + e.getMessage();
			return null;
		}

		// Copie du contenu du fichier dans le spool
		try
		{
			for(i=0; i<lbuffer.size(); i++)
				fileout.write((byte[])lbuffer.get(i));
			fileout.close();
			return fileout.getSpooledFile();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (cpyFichier2Spool) Erreur lors de la copie du fichier spool : " + e.getMessage();
			return null;
		}
	}

	/**
	 * Copie un spool dans un fichier texte (� tester sous LINUX & Mac OS X)
	 * @param splf
	 * @param out
	 *
	public ArrayList cpySpool2FichierTexte(SpooledFile splf)
	{
		ArrayList document = new ArrayList();
		StringBuffer aspool=null;
		int i=0, k=0;

		if (splf == null) return null;
		
		aspool = recupSpool(splf);
		
		// Traitement du spool
		for (i=0; i<aspool.length(); i++)
		{
			// Test saut de ligne
			if( (i<(aspool.length()-2)) && (aspool.charAt(i) == 0x0d) && (aspool.charAt(i+1) == 0x0d) && (aspool.charAt(i+2) == 0x0a))
			{
				document.add(aspool.substring(k, i));
				i+=2;
				k=i;
			}
			else // Test saut de page
				if( (i<(aspool.length()-1)) && (aspool.charAt(i) == 0x0d) && (aspool.charAt(i+1) == 0x0c))
				{
					if (aspool.charAt(i-1) == 0x0d)
						document.add(aspool.substring(k, i-1));
					else
						document.add(aspool.substring(k, i));
					document.add(Constantes.sautpage);
					i+=1;
					k=i;
				}
			}
		
		// On supprime le dernier marquer de saut de page
		if( (document.size() !=0) && (document.get(document.size()-1).equals(Constantes.sautpage) ))
			document.remove(document.size()-1);
		
		return document;
	}*/

	/**
	 * Retourne un spool dans un StringBuffer
	 * @param splf
	 * A mettre � jour si r�utiliser avec la m�thode qui suit
	 *
	public StringBuffer cpySpool2StringBuffer(SpooledFile splf)
	{
		int i=0;

		// Contr�le variables
		if (splf == null) return null;

		StringBuffer aspool = recupSpool(splf);
		
		// Suppression syst�matique du premier caract�re
		if (aspool.length() > 0) aspool.deleteCharAt(0);
		
		// Traitement du spool on analyse chaque caract�re
		i=0;
		while (i<aspool.length())
		{
			// Test saut de ligne
			if( (i<(aspool.length()-2)) && (aspool.charAt(i) == 0x0d) && (aspool.charAt(i+1) == 0x0d) && (aspool.charAt(i+2) == 0x0a))
			{
				aspool.deleteCharAt(i);
				aspool.deleteCharAt(i);
				aspool.deleteCharAt(i);
				aspool.insert(i, Constantes.SAUTLIGNE_CHAR);
				i+=Constantes.SAUTLIGNE_CHAR.length;
			}
			else // Test saut de page
				if( (i<(aspool.length()-1)) && (aspool.charAt(i) == 0x0d) && (aspool.charAt(i+1) == 0x0c))
				{
					if (aspool.charAt(i-1) == 0x0d)	aspool.deleteCharAt(--i);
					aspool.deleteCharAt(i);
					aspool.deleteCharAt(i);
					aspool.insert(i, Constantes.crlf);
					i+=Constantes.SAUTLIGNE_CHAR.length;

					// On ins�re un saut de page si une page suis...
					if (i < aspool.length())
					{
						aspool.insert(i, Constantes.SAUTPAGE+Constantes.crlf);
						i+=Constantes.SAUTPAGE_CHAR.length + Constantes.SAUTLIGNE_CHAR.length;
					}
				}
				else
					i++;
		}
		
		return aspool;
	}*/

	/**
	 * Retourne le spool dans un tableau de StringBuffer
	 * @param splf
	 */
	public String[] cpySpool2TabStringBuffer(SpooledFile splf)
	{
		int i=0;
		StringBuffer sb=new StringBuffer();
		StringBuffer page=new StringBuffer();
		int dim=0;
		String[] pages=null;
		String[] ligne=null;
		String[] tabsurimpression=null;

		// Contr�le variables
		if (splf == null) return null;

		sb = recupSpool(splf);
		if (sb != null)
		{
			// Suppression syst�matique du premier caract�re NULL
			if (sb.length() > 0) sb.deleteCharAt(0);
			pages = sb.toString().split(""+FF);
		}
		if (pages == null) return null;
//System.out.println("-1-> pages: " + pages.length);

		/*
		char[] tab = new char[aspool.length()];
		for (int y=0; y<aspool.length(); y++)
			tab[y] = aspool.charAt(y);
		try {
			PrintWriter pw = new PrintWriter("/home/ritousv/Bureau/bulla4.txt");
			pw.write(tab);
			pw.flush();
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		// On parcourt la liste afin de corriger les surimpressions et les sauts de page
		for (int p=0; p<pages.length; p++)
		{
			// Suppression des caract�res de contr�le de SIM : �G
			pages[p] = pages[p].replaceAll("�G", "  ");

			page.setLength(0);
			ligne = pages[p].split(""+LF);
			for (i=0; i<ligne.length; i++)
			{
				dim = ligne[i].length(); 
				if (dim == 0)
				{
					page.append(Constantes.crlf);
					continue;
				}

				// On supprime le dernier caract�re si c'est CR
				if (ligne[i].charAt(dim-1) == CR)
					if (dim > 1)
						ligne[i] = ligne[i].substring(0, dim-1);
					else
						ligne[i] = "";

				// On recherche des caract�res CR dans la chaine, cas de la surimpression
				tabsurimpression = ligne[i].split(""+CR);
				if (tabsurimpression.length > 1)
				{
					sb.setLength(0);
					sb.append(tabsurimpression[0]);
					for (int j=1; j<tabsurimpression.length; j++)
						for (int k=0; k<tabsurimpression[j].length(); k++)
							if (sb.length() <= k)
								sb.append(tabsurimpression[j].charAt(k));
							else
								if (tabsurimpression[j].charAt(k) != ' ') sb.setCharAt(k, tabsurimpression[j].charAt(k));
					ligne[i] = sb.toString();
				}
				page.append(ligne[i]).append(Constantes.crlf);
			}
			pages[p] = page.toString();
		}
		
		return pages;
	}

	/**
	 * Retourne le spool avec les informations principales
	 * @param splf
	 */
	public Spool getSpool(SpooledFile splf)
	{
		String[] pages = cpySpool2TabStringBuffer(splf);
		if (pages == null)
		{
			msgErreur += "\n" + NOM_CLASSE + " (getSpool) Pas de donn�es sur le spool.";
			return null;
		}
		
		Spool spool=new Spool();
		try
		{
		//System.out.println(pol.getFloatAttribute(PrintObject.ATTR_LPI) + " " + pol.getFloatAttribute(PrintObject.ATTR_PAGELEN) + " "+  pol.getFloatAttribute(PrintObject.ATTR_PAGEWIDTH) + " " + pol.getIntegerAttribute(PrintObject.ATTR_PAGRTT));
			spool.setNom(splf.getStringAttribute(PrintObject.ATTR_SPOOLFILE));
			spool.setNbrColonnes(splf.getFloatAttribute(PrintObject.ATTR_PAGEWIDTH).intValue());
			spool.setNbrLignes(splf.getFloatAttribute(PrintObject.ATTR_PAGELEN).intValue());
			spool.setPortrait(!(splf.getIntegerAttribute(PrintObject.ATTR_PAGRTT).intValue()==90));
			spool.setNbrLigneInch(splf.getFloatAttribute(PrintObject.ATTR_LPI).intValue());
			spool.setNbrPages(splf.getIntegerAttribute(PrintObject.ATTR_PAGES).intValue());
			spool.setPages(pages);
			return spool;
		}
		catch(Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (getSpool) Erreur : " + e.getMessage();
			return null;
		}
	}
	
	/**
	 * Copie un spool dans un fichier texte (� tester sous LINUX & Mac OS X)
	 * @param splf
	 * @param out
	 *
	public ArrayList cpySpool2FichierTexte(SpooledFile splf)
	{
		InputStreamReader in=null;
		char[] buffer = new char[32767];
		char[] buffertraite = new char[32767];
		ArrayList document = new ArrayList();
		int i=0, k=0;
		int bytesRead = 0;

		// Contr�le variables
		if (splf == null) return null;

		PrintParameterList printParms = new PrintParameterList();
		printParms.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, PLAIN_TEXT);
		printParms.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");

		try
		{
			in = new InputStreamReader(splf.getTransformedInputStream(printParms), "CP858");
			if (in.ready())
			{
				in.skip(1);
				bytesRead = in.read(buffer, 0, buffer.length);
				while (bytesRead > 0)
				{
					// On supprime les caract�res parasites 0D0D0A (saut de ligne) et 0D0D0C (saut de page)
					k=0;
					for (i=0; i<bytesRead; i++)
					{
						// Test saut de ligne
						if( (i<(bytesRead-2)) && (buffer[i] == 0x0d) && (buffer[i+1] == 0x0d) && (buffer[i+2] == 0x0a))
						{
System.out.println("1-> " +bytesRead + " " + buffertraite.length + " " + k);								
							document.add(new String (buffertraite, 0, k));
							k=0;
							i+=2;
						}
						else // Test saut de page
							if( (i<(bytesRead-1)) && (buffer[i] == 0x0d) && (buffer[i+1] == 0x0c))
							{
								if (buffertraite[k-1] == 0x0d) k--;
System.out.println("2-> " +bytesRead + " " + buffertraite.length + " " + k);								
								document.add(new String (buffertraite, 0, k));
								document.add(Constantes.sautpage);
								i+=1; // A voir
							}
							else
								//if (i < bytesRead)
									buffertraite[k++] = buffer[i];
					}

					bytesRead = in.read(buffer, 0, buffer.length);
				}
			}
			in.close();
		}
		catch(Exception e)
		{
			System.out.println("[GestionSpoolAS400] 1 (cpySpool2FichierTexte) Erreur " + e);
		}
		
		// On supprime le dernier marquer de saut de page
		if( (document.size() !=0) && (document.get(document.size()-1).equals(Constantes.sautpage) ))
			document.remove(document.size()-1);
		
		return document;
	}
	*/
	
	/**
	 * Copie un spool dans un fichier texte (� tester sous LINUX & Mac OS X)
	 * @param splf
	 * @param out
	 *
	public void cpySpool2FichierTexte(SpooledFile splf, String fichier)
	{
		InputStreamReader in=null;
		char[] buffer = new char[32767];
		char[] buffertraite = new char[32767];
		FileWriter out=null;
		int i=0, j=0, k=0;
		int bytesRead = 0;

		// Contr�le variables
		if (splf == null) return;
		if (fichier == null) return;

		PrintParameterList printParms = new PrintParameterList();
		printParms.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, PLAIN_TEXT);
		printParms.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");

		try
		{
			in = new InputStreamReader(splf.getTransformedInputStream(printParms), "CP858");
			out = new FileWriter(fichier);
			if (in.ready())
			{
				bytesRead = in.read(buffer, 0, buffer.length);
				while (bytesRead > 0)
				{
					// On supprime les caract�res parasites 0D0D0A (saut de ligne) et 0D0D0C (saut de page)
					k=0;
					for (i=0; i<bytesRead; i++)
					{
						// Test saut de ligne
						if( (i<(bytesRead-2)) && (buffer[i] == 0x0d) && (buffer[i+1] == 0x0d) && (buffer[i+2] == 0x0a))
						{
							for (j=0; j<Constantes.c_sautligne.length; j++)
								buffertraite[k++] = Constantes.c_sautligne[j];
							i+=2;
						}
						else // Test saut de page
							if( (i<(bytesRead-1)) && (buffer[i] == 0x0d) && (buffer[i+1] == 0x0c))
							{
								if (buffertraite[k-1] == 0x0d) k--;
								for (j=0; j<Constantes.c_sautligne.length; j++)
									buffertraite[k++] = Constantes.c_sautligne[j];
								for (j=0; j<Constantes.c_sautpage.length; j++)
									buffertraite[k++] = Constantes.c_sautpage[j];
								for (j=0; j<Constantes.c_sautligne.length; j++)
									buffertraite[k++] = Constantes.c_sautligne[j];
								i+=1; // A voir
							}
							else
								buffertraite[k++] = buffer[i];
					}
					out.write(buffertraite, 1, k-1);

					bytesRead = in.read(buffer, 0, buffer.length);
				}
			}
			in.close();
			out.close();
		}
		catch(Exception e)
		{
			System.out.println("[GestionSpoolAS400] 2 (cpySpool2FichierTexte) Erreur " + e);
		}
	}
	*/

	/**
	 * Copie un spool dans un fichier (ici gif)
	 * @param splf
	 * @param out
	 */
	public boolean cpySpool2Fichier(SpooledFile splf, String fichier)
	{
		PrintObjectTransformedInputStream in = null;
		int count = 0;
		byte[] buffer = new byte[32767];
		FileOutputStream out=null;

		// Contr�le variableswrk
		if (splf == null) return false;
		if (fichier == null) return false;

		PrintParameterList printParms = new PrintParameterList();
		printParms.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, PLAIN_GIF);
		printParms.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");

		try
		{
			in = splf.getTransformedInputStream(printParms);
			out = new FileOutputStream(fichier);
			count = 0;
			do
			{
				out.write(buffer, 0, count);
				count = in.read(buffer, 0, buffer.length); //IBM00858
			}
			while (count != -1);
			in.close();
			out.close();
		}
		catch(Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (cpySpool2Fichier) Erreur : " + e.getMessage();
			return false;
		}
		return true;
	}

	/**
	 * Retourne la liste des spools d'une outq
	 * @param filtre
	 * @return
	 */
	public SpooledFileList listeSpool(String filtre, String nomoutq, boolean synchrone)
	{
		// Contr�le variables
		try
		{
			//if (splfList == null) --> Erreur : Object cannot be in an open state.
			splfList = new SpooledFileList(systeme);

			// Filtre sur les users
			splfList.setUserFilter(filtre.toUpperCase());
			//splfList.setFormTypeFilter(arg0)
			if ((nomoutq != null) && (nomoutq.toUpperCase().startsWith("/QSYS.LIB"))) splfList.setQueueFilter(nomoutq);
			if (synchrone)
				splfList.openSynchronously();
			else
			{
				splfList.openAsynchronously();
			    splfList.waitForListToComplete(); // Risque de blocage si coupure de l'as400, il faut tester le service FILE
			}
		}
		catch (Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (listeSpool) Erreur : " + e.getMessage();
			e.printStackTrace();
			return null;
		}
		
		return splfList;
	}

	/**
	 * Retourne un model de table avec les donn�es de la liste des spools d'une outq
	 * @param splfList
	 * @return
	 */
	public ListContains getContainslListeSpool(SpooledFileList splfList)
	{
		final int NBRCOL=10;
		//final String[] titres = new String[] {"Fichier", "Utilisateur", "R\u00e9f\u00e9rence", "Etat", "Pages", "Travail nom", "Travail num", "Spool num", "Date", "Heure"};
		if (splfList == null) return new ListContains(new String[0][NBRCOL], titres);

		String[][] data = new String[splfList.size()][NBRCOL];
		for (int i=0; i<splfList.size(); i++)
		{
			PrintObject pol = splfList.getObject(i);
			try
			{
//System.out.println(pol.getFloatAttribute(PrintObject.ATTR_LPI) + " " + pol.getFloatAttribute(PrintObject.ATTR_PAGELEN) + " "+  pol.getFloatAttribute(PrintObject.ATTR_PAGEWIDTH) + " " + pol.getIntegerAttribute(PrintObject.ATTR_PAGRTT));
//System.out.println("-GestionSpoolAS400-> " + pol.getStringAttribute(PrintObject.ATTR_DATE) + " " + pol.getStringAttribute(PrintObject.ATTR_TIME));				
				data[i][0] = pol.getStringAttribute(PrintObject.ATTR_SPOOLFILE);
				data[i][1] = pol.getStringAttribute(PrintObject.ATTR_JOBUSER);
				data[i][2] = pol.getStringAttribute(PrintObject.ATTR_USERDATA);
				data[i][3] = pol.getStringAttribute(PrintObject.ATTR_SPLFSTATUS);
				data[i][4] = String.valueOf(pol.getIntegerAttribute(PrintObject.ATTR_PAGES));
				
				data[i][5] = pol.getStringAttribute(PrintObject.ATTR_JOBNAME);
				data[i][6] = pol.getStringAttribute(PrintObject.ATTR_JOBNUMBER);
				data[i][7] = String.valueOf(pol.getIntegerAttribute(PrintObject.ATTR_SPLFNUM));
				
				data[i][8] = getFormateDate(pol.getStringAttribute(PrintObject.ATTR_DATE));
				data[i][9] = getFormateHeure(pol.getStringAttribute(PrintObject.ATTR_TIME));
			}
			catch (Exception e)
			{
				msgErreur += "\n" + NOM_CLASSE + " (getContainslListeSpool) Erreur : " + e.getMessage();
				e.printStackTrace();
				return null;
			}
		}
		return new ListContains(data, titres);
	}

	/**
	 * Retourne les donn�es de la liste des spools d'une outq sous forme JSon
	 * @param splfList
	 * @return
	 * API JsonSimple
	@SuppressWarnings("unchecked")
	public String getJSONListeSpool(SpooledFileList splfList)
	{
		// Cr�ation de l'objet JSON
		// Le type
		JSONArray objtypes = new JSONArray();
		for (int i=0; i<types.length; i++)
			objtypes.add(types[i]);
		JSONObject objet = new JSONObject();
		objet.put("TYPE", objtypes);
		
		// Le titre
		JSONArray objtitres = new JSONArray();
		for (int i=0; i<titres.length; i++)
			objtitres.add(Constantes.lettreori2code(titres[i]));
		//JSONObject objet = new JSONObject();
		objet.put("TITLE", objtitres);

		if (splfList == null)
		{
			objet.put("DATA", null);
			return objet.toJSONString();
		}

		// Les donn�es
		JSONArray objdata = new JSONArray();
		for (int i=0; i<splfList.size(); i++)
		{
			JSONArray objligne = new JSONArray();
			PrintObject pol = splfList.getObject(i);
			try
			{
				objligne.add(pol.getStringAttribute(PrintObject.ATTR_SPOOLFILE));
				objligne.add(pol.getStringAttribute(PrintObject.ATTR_JOBUSER));
				objligne.add(pol.getStringAttribute(PrintObject.ATTR_USERDATA));
				objligne.add(pol.getStringAttribute(PrintObject.ATTR_SPLFSTATUS));
				objligne.add(pol.getIntegerAttribute(PrintObject.ATTR_PAGES));
				
				objligne.add(pol.getStringAttribute(PrintObject.ATTR_JOBNAME));
				objligne.add(pol.getStringAttribute(PrintObject.ATTR_JOBNUMBER));
				objligne.add(pol.getIntegerAttribute(PrintObject.ATTR_SPLFNUM));
				
				objligne.add(getFormateDate(pol.getStringAttribute(PrintObject.ATTR_DATE)));
				objligne.add(getFormateHeure(pol.getStringAttribute(PrintObject.ATTR_TIME)));
				
			}
			catch (Exception e)
			{
				msgErreur += "\n" + NOM_CLASSE + " (getContainslListeSpool) Erreur : " + e.getMessage();
				e.printStackTrace();
				return null;
			}
			objdata.add(objligne);
		}
		objet.put("DATA", objdata);
		return objet.toJSONString();
	}*/

	/**
	 * Retourne les donn�es de la liste des spools d'une outq sous forme JSon
	 * @param splfList
	 * @return
	 */
	public String getJSONListeSpool(SpooledFileList splfList)
	{
		// Cr�ation de l'objet JSON
		// Le type
		JsonArray objtypes = new JsonArray();
		for (int i=0; i<types.length; i++)
			objtypes.add(new JsonPrimitive(types[i]));
		JsonObject objet = new JsonObject();
		objet.add("TYPE", objtypes);
		
		// Le titre
		JsonArray objtitres = new JsonArray();
		for (int i=0; i<titres.length; i++)
			objtitres.add(new JsonPrimitive(Constantes.lettreori2code(titres[i])));
		//JSONObject objet = new JSONObject();
		objet.add("TITLE", objtitres);

		if (splfList == null)
		{
			objet.add("DATA", null);
			return gson.toJson(objet);
		}

		// Les donn�es
		JsonArray objdata = new JsonArray();
		for (int i=0; i<splfList.size(); i++)
		{
			JsonArray objligne = new JsonArray();
			PrintObject pol = splfList.getObject(i);
			try
			{
				objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_SPOOLFILE)));
				objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_JOBUSER)));
				objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_USERDATA)));
				objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_SPLFSTATUS)));
				objligne.add(new JsonPrimitive(pol.getIntegerAttribute(PrintObject.ATTR_PAGES)));
				
				objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_JOBNAME)));
				objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_JOBNUMBER)));
				objligne.add(new JsonPrimitive(pol.getIntegerAttribute(PrintObject.ATTR_SPLFNUM)));
				
				objligne.add(new JsonPrimitive(getFormateDate(pol.getStringAttribute(PrintObject.ATTR_DATE))));
				objligne.add(new JsonPrimitive(getFormateHeure(pol.getStringAttribute(PrintObject.ATTR_TIME))));
				
			}
			catch (Exception e)
			{
				msgErreur += "\n" + NOM_CLASSE + " (getContainslListeSpool) Erreur : " + e.getMessage();
				e.printStackTrace();
				return null;
			}
			objdata.add(objligne);
		}
		objet.add("DATA", objdata);
		return gson.toJson(objet);
	}

	/**
	 * R�cup�re le spool dans un StringBuffer
	 * @param splf
	 */
	private StringBuffer recupSpool(SpooledFile splf)
	{
		InputStreamReader in=null;
		StringBuffer spool=new StringBuffer(); 
		char[] buffer = new char[32767];
		int bytesRead = 0;

		// Contr�le variables
		if (splf == null) return spool;

		PrintParameterList printParms = new PrintParameterList();
		printParms.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, PLAIN_TEXT);
		printParms.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");

		// Chargement du spool 
		try
		{
//System.out.println("--> cp=" + splf.getStringAttribute(PrintObject.ATTR_CODEPAGE_NAME));			
//System.out.println("--> cp=" + splf.getStringAttribute(PrintObject.ATTR_CODEPAGE));
			if (codepage == null)
				in = new InputStreamReader(splf.getTransformedInputStream(printParms));
			else
				in = new InputStreamReader(splf.getTransformedInputStream(printParms), codepage);
			if (in.ready())
			{
				bytesRead = in.read(buffer, 0, buffer.length);
				while (bytesRead > 0)
				{
					spool.append(buffer, 0, bytesRead);
					bytesRead = in.read(buffer, 0, buffer.length);
				}
			}
			in.close();
		}
		catch(Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (recupSpool) Erreur : " + e.getMessage();
			return null;
		}
		
		return spool;
	}

	/**
	 * Enregistre le spool dans le StringBuffer dans un fichier
	 */
	public boolean enregistreSpool2Fichier(StringBuffer aspool, String fichier)
	{
		FileWriter out=null;

		// On �crit le fichier
		try
		{
			out = new FileWriter(fichier);
			out.write(aspool.toString());
			out.close();
		}
		catch(Exception e)
		{
			msgErreur += "\n" + NOM_CLASSE + " (enregistreSpool2Fichier) Erreur : " + e.getMessage();
			return false;
		}
		return true;
	}

	/**
	 * @return the codepage
	 */
	public String getCodepage()
	{
		return codepage;
	}

	/**
	 * @param codepage the codepage to set
	 */
	public void setCodepage(String codepage)
	{
		this.codepage = codepage;
	}

	/**
	 * @return outq
	 */
	public OutputQueue getOutq()
	{
		return outq;
	}

	/**
	 * @param outq outq � d�finir
	 */
	public void setOutq(OutputQueue outq)
	{
		this.outq = outq;
		nomOutq = outq.getPath();
	}

	/**
	 * @param String outq � d�finir
	 */
	public void setOutq(String outq)
	{
		nomOutq = outq.toUpperCase();
		if (systeme != null)
			this.outq = new OutputQueue(systeme, outq);
		else
			this.outq = new OutputQueue(new AS400(), outq);
	}

	/**
	 * @return nomoutq
	 */
	public String getNomOutq()
	{
		return nomOutq;
	}
	
	/**
	 * Retourne le message d'erreur 
	 * @return
	 */
	public String getMsgErreur()
	{
		// La r�cup�ration du message est � usage unique
		String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	// Formatte une date SAAMMJJ en JJ/MM/AA
	private String getFormateDate(String date)
	{
		if ((date == null) || (date.length() != 7)) return date;
		
		return  date.substring(5) + '/' + date.substring(3, 5) + '/' + (date.charAt(0)=='1'?"20":"19") + date.substring(1, 3);
	}
	
	// Formatte une heure HHMMSS en HH:MM:SS
	private String getFormateHeure(String heure)
	{
		if ((heure == null) || (heure.length() != 6)) return heure;
		
		return  heure.substring(0, 2) + ':' + heure.substring(2, 4) + ':' + heure.substring(4);
	}

}