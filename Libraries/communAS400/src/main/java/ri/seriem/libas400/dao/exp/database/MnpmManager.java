//=================================================================================================
//==>                                                                       19/12/2011 - 23/05/2012
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;


public class MnpmManager extends QueryManager
{
	/**
	 * Constructeur
	 * @param database
	 */
	public MnpmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Retourne la liste des modules (pour les menus)
	 * @param bibenv
	 * @param prf
	 * @return
	 */
	public ArrayList<GenericRecord> getRecordForMenu(boolean isSerieN)
	{
		if (isSerieN) // Menus sp�cifiques pour S�rie N (S�rie M �pur�s)
			return select("Select * from mnp1sgm where mnpp0 = '01' and mnpp1 <> '' and mnpp1 < '30' and mnpord <> 01268 and mnpp2 = ''");
		// On s�lectionne tous les groupes sauf LTM (01268)
		return select("Select * from mnp1 where mnpp0 = '01' and mnpp1 <> '' and mnpp1 < '30' and mnpord <> 01268 and mnpp2 = ''");
	}

    
}
