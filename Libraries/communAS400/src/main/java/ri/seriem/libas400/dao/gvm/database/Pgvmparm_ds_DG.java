//=================================================================================================
//==>                                                                       11/09/2015 - 11/09/2015
//==> Description de l'enregistrement du fichier Pgvmparm_ds_DG pour les DG
//==> G�n�r� avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
//=================================================================================================
package ri.seriem.libas400.dao.gvm.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_DG extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGNOM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGCPL"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "DGT")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGTPF"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(11, 0), "DGDIVG"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX1G"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGDATG"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX0G"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGETP"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGCMAG"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 0), "DGNUM"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(3, 0), "DGNBS"));

		length = 200;
	}
}
