package ri.seriem.libas400.system.systemrequest;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.SaveFile;
import com.ibm.as400.access.SaveFileEntry;

import ri.seriem.libcommun.protocoleMsg.SavfRequest;

public class SavfRequestActions
{
	private AS400 sysAS400=null;
	private SavfRequest rsavf=null;
	
	/**
	 * Constructeur
	 * @param system
	 */
	public SavfRequestActions(AS400 system, SavfRequest arsavf)
	{
		sysAS400 = system;
		rsavf = arsavf;
	}

	// --> M�thodes publiques <------------------------------------------------
	
	/**
	 * Traitement des actions possibles 
	 */
	public void actions()
	{
		//int theactions = actions;
		for (int i=SavfRequest.ACTIONS.size(); --i>0;)
		{
			if (rsavf.getActions() >= SavfRequest.ACTIONS.get(i))
			{
				switch(SavfRequest.ACTIONS.get(i))
				{
					case SavfRequest.RESTORE:
						savfRestore(getLibraryAndObject(rsavf.getLibrary_savf(), false), null);
						break;
				}
				rsavf.setActions(rsavf.getActions() - SavfRequest.ACTIONS.get(i));
			}
		}
	}

	// --> M�thodes priv�es <--------------------------------------------------

	/**
	 * Restaure un fichier SAVF
	 * @param bib_savf
	 * @param arg
	 * @return
	 */
	private void savfRestore(String[] bib_savf, String arg)
	{
		if (bib_savf[0] == null)
			bib_savf[0] = "";

		try
		{
			SaveFile savf = new SaveFile(sysAS400, bib_savf[0], bib_savf[1]);
			if (!savf.exists())
			{
				rsavf.setSuccess(false);
				return;
			}
			// Analyse du SAVF et restauration du savf
			//ObjectDescription od = savf.getObjectDescription();
			//if (od.getValue(ObjectDescription.SAVE_COMMAND).equals("SAVOBJ"))
			//{
			SaveFileEntry[] liste = savf.listEntries();
			String[] listeFile = new String[liste.length];
			int i=0;
			for (SaveFileEntry fichier : liste)
				listeFile[i++] = fichier.getName();
			savf.restore(bib_savf[1], listeFile, bib_savf[1]);
			rsavf.setSuccess(true);

			//}
			//else
			//	savf.restore(bib_savf[1]);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			rsavf.setSuccess(false);
		}
	}

	/**
	 * S�pare dans une chaine le couple BIB/OBJ et le retourne sous forme de tableau  
	 * @param valeur
	 * @return
	 */
	protected String[] getLibraryAndObject(String valeur, boolean forcebib)
	{
		String[] bib_obj = new String[2];
		
		// On r�cup�re les informations des arguments 
		int posd = valeur.indexOf('/');
		// On r�cup�re le nom du fichier SAVF car il n'est pas pr�cis� dans l'argument
		if (posd == -1)
		{
			if (forcebib)
				bib_obj[0] = valeur;
			else
				bib_obj[1] = valeur;
		}
		else // On a pass� le nom de la bib et le nom du SAVF (BIB/SAVF)
		{
			bib_obj[0] = valeur.substring(0, posd);
			bib_obj[1] = valeur.substring(posd+1);
		}
		return bib_obj;
	}

}
