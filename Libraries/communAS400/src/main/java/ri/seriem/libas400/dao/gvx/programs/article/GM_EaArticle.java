package ri.seriem.libas400.dao.gvx.programs.article;

import java.util.ArrayList;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;

public class GM_EaArticle extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_EaArticle(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Retourne un contact � partir de son id
	 * @param id
	 * @return
	 */
	public M_EaArticle readOneArticle(String etb, String code){
		if( (etb == null) || (etb.trim().length() == 0) ){
			msgErreur += "\nL'�tablissement est � null ou vide.";
			return null;
		}
		if( (code == null) || (code.trim().length() == 0) ){
			msgErreur += "\nLe code article est � null ou vide.";
			return null;
		}
		if( querymg.getLibrary() == null ){
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PGVMEAAM where A1ETB = '" + etb +"' and A1ART = '"+code+"'");
		if( (listrcd == null) || (listrcd.size() == 0) ){
			return null;
		}
		M_EaArticle a = new M_EaArticle(querymg);
		a.initObject(listrcd.get(0), true);
		
		return a;
	}
	
	
	@Override
	public void dispose()
	{
		querymg = null;
	}

}
