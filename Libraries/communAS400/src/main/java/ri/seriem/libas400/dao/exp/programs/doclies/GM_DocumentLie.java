package ri.seriem.libas400.dao.exp.programs.doclies;

import ri.seriem.libas400.dao.gvx.programs.article.GM_Article;
import ri.seriem.libas400.dao.gvx.programs.article.GM_EaArticle;
import ri.seriem.libas400.dao.gvx.programs.article.GM_EcArticle;
import ri.seriem.libas400.dao.gvx.programs.article.M_Article;
import ri.seriem.libas400.dao.gvx.programs.article.M_EaArticle;
import ri.seriem.libas400.dao.gvx.programs.article.M_EcArticle;
import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.Constantes;

import java.util.ArrayList;

public class GM_DocumentLie extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_DocumentLie(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Retourne un enregistrement des documents li�s
	 * @param doc
	 * @param fic
	 * @return
	 */
	public M_DocumentLie readOneLink(String doc, String fic){
		if( doc == null ){
			msgErreur += "\nLe champ PDDOC est � null.";
			return null;
		}
		if( fic == null ){
			msgErreur += "\nLe champ PDFIC est � null.";
			return null;
		}
		if( querymg.getLibrary() == null ){
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PSEMPDMM where PDDOC = '" + doc +"' and PDFIC = '" + fic +"'");
		if( listrcd == null ){
			return null;
		}
		M_DocumentLie dl = new M_DocumentLie(querymg);
		dl.initObject(listrcd.get(0), true);
		
		return dl;
	}

	/**
	 * Retourne le chemin partie commune (partie sp�cifique VEXPD2) TODO A am�liorer
	 * @param etb
	 * @param doc
	 * @param fic
	 * @param path
	 * @return
	 */
	public M_DocumentLie getPathCommon(String etb, String doc, String fic, StringBuffer path)
	{
		if( (etb == null) || (etb.trim().length() == 0) ){
			msgErreur += "\nL'�tablissement est � null ou vide.";
			return null;
		}
		if( doc == null ){
			msgErreur += "\nLe champ PDDOC est � null.";
			return null;
		}
		if( fic == null ){
			msgErreur += "\nLe champ PDFIC est � null.";
			return null;
		}
		
		M_DocumentLie dl = readOneLink(doc, fic);
		if( dl == null ) return null;

		// Traite la racine
		String chaine = getRoot( dl.getPDCHM0() );
		if( chaine == null ) return null;
		path.append( chaine );

		// Traite la curlib
		if( dl.getPDIN1() == '1' ){
			if( (path.charAt(path.length()-1) != '\\') && (path.charAt(path.length()-1) != '/') )
				path.append('/');
			path.append( querymg.getLibrary() );
		}

		// Traite l'�tablissement
		if( dl.getPDIN2() == '1' ){
			if( (path.charAt(path.length()-1) != '\\') && (path.charAt(path.length()-1) != '/') )
				path.append('/');
			path.append( cleanString(etb) );
		}
		
		// Traite le type de la fiche
		if( dl.getPDIN3() == '1' ){
			if( (path.charAt(path.length()-1) != '\\') && (path.charAt(path.length()-1) != '/') )
				path.append('/');
			path.append( cleanString(fic) );
		}

		// Traite le type du document
		if( (dl.getPDIN4() == '1') || (doc.equalsIgnoreCase("PHO")) ){
			if( (path.charAt(path.length()-1) != '\\') && (path.charAt(path.length()-1) != '/') )
				path.append('/');
			path.append( cleanString(doc) );
		}
		
		return dl;
	}

	/**
	 * Retourne le chemin pour les articles TODO A am�liorer
	 * @param etb
	 * @param a1art
	 * @param doc
	 * @param fic
	 * @return
	 */
	public String getPathArticles(String etb, String a1art, String doc, String fic)
	{
		if( (a1art == null) || (a1art.trim().length() == 0) ){
			msgErreur += "\nLe code article est � null ou vide.";
			return null;
		}
		
		StringBuffer path = new StringBuffer(); 
		M_DocumentLie dl = getPathCommon(etb, doc, fic, path);
		if( dl == null ) return null;
		
		// Traite les zones variables
		String chaine = getValueArticle(etb, a1art, dl.getPDCHM1());
		if( chaine.trim().length() > 0 )
			path.append('/').append( cleanString(chaine) );
		chaine = getValueArticle(etb, a1art, dl.getPDCHM2());
		if( chaine.trim().length() > 0 )
			path.append('/').append( cleanString(chaine) );
		chaine = getValueArticle(etb, a1art, dl.getPDCHM3());
		if( chaine.trim().length() > 0 )
			path.append('/').append( cleanString(chaine) );
		chaine = getValueArticle(etb, a1art, dl.getPDCHM4());
		if( chaine.trim().length() > 0 )
			path.append('/').append( cleanString(chaine) );
		chaine = getValueArticle(etb, a1art, dl.getPDCHM5());
		if( chaine.trim().length() > 0 )
			path.append('/').append( cleanString(chaine) );
		
		if( Constantes.isWindows() ){
			chaine = path.toString().replace('/', '\\');
		} else
			chaine = path.toString();
			
		return chaine;
	}
	
	@Override
	public void dispose()
	{
		querymg = null;
	}

	
	// -- M�thodes priv�es ----------------------------------------------------
	
	/**
	 * Retourne la racine du chemin (en fonction du type d'OS) TODO � am�liorer
	 * @param value
	 * @return
	 */
	private String getRoot(String value)
	{
		String root = "";
		if( value == null ) return root;
		
		String tab[] = value.split(";");
		if( tab.length == 2 ){
			if( Constantes.isOs400() ){
				root = tab[1]; 
			}
		}
		if( !Constantes.isOs400() ){
			root = tab[0]; 
		}
		
		return root;
	}
	
	/**
	 * Retourne pour un article la valeur du param�tre (partie sp�cifique VEXPDART)
	 * @param etb
	 * @param code
	 * @param key
	 * @return
	 */
	private String getValueArticle(String etb, String code, String key)
	{
		if( (key == null) || (key.trim().length() == 0) ) return "";

		int indice = Integer.parseInt(key.substring(1).trim());

		// Pour le fichier PGVMEEAM
		if( (indice == 2) || (indice == 3) || (indice == 4) ){
			GM_EaArticle geaart = new GM_EaArticle(querymg);
			M_EaArticle eaart = geaart.readOneArticle(etb, code);
			if( eaart == null ) return "";
			switch( indice ){
				case 2:	return eaart.getA1LB1();
				case 3:	return eaart.getA1LB2();
				case 4:	return eaart.getA1LB3();
			}
		}

		// Pour le fichier PGVMECAM
		if( (indice > 70) && (indice <= 88) ){
			GM_EcArticle gecart = new GM_EcArticle(querymg);
			M_EcArticle ecart = gecart.readOneArticle(etb, code);
			if( ecart == null ) return "";
			int index = indice - 70;
			return ecart.getEBZPX(index);
		}
		
		// Pour le fichier PGVMARTM
		GM_Article gart = new GM_Article(querymg);
		M_Article art = gart.readOneArticle(etb, code);
		if( art == null ) return "";
		switch( indice ){
			case 1:	return art.getA1LIB();
			case 5: return art.getA1CL1();
			case 6: return art.getA1CL2();
			case 7: return art.getA1FAM();
			case 10: return art.getA1ART();
			case 11: return art.getA1UNS();
			case 48: if( art.getA1GCD() > 0 ) return ""+art.getA1GCD();
			case 70: return ""+art.getA1IN7();
		}
		return "";
	}
	
	/**
	 * Remplace les caract�res inapropri�s par un underscore 
	 * @param str
	 * @return
	 */
	private String cleanString(String str)
	{
		char[] listchar = {' ', '/', '\\', ':', '*', '?', '"', '<', '>', '|'};
		boolean trouve = false;
			
		StringBuffer sb = new StringBuffer();
		for( int i=0; i<str.length(); i++){
			trouve = false;
			for( int c=0; c<listchar.length; c++){
				if( str.charAt(i) == listchar[c] ){
					sb.append('_');
					trouve = true;
					break;
				}
			}
			if( !trouve) sb.append(str.charAt(i));
		}
		
		return sb.toString();
	}

}
