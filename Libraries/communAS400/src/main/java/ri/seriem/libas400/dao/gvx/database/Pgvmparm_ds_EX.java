//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_EX pour les EX
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_EX extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(16), "EXLIB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXCPR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXPRF"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "EXFRP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXMCP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXFGC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "EXNCO"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "EXIHH"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "EXIHM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXENL"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXETQ"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXRTR"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "EXFRM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EXURG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "EXOPT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "EXCOM"));

		length = 300;
	}
}
