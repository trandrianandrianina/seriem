package ri.seriem.libas400.database.record;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;

public abstract class DataStructureRecord
{
	// Variables
	public int length=0;							// Longueur du record (et non de la datastructure)
	protected RecordFormat rf = new RecordFormat();
	protected int[] offsetField;
	protected ArrayList<byte[]> listFields;
	
	/**
	 * Constructeur
	 */
	public DataStructureRecord()
	{
		initRecord();
		prepareAnalyze();
	}
	
	/**
	 * Initialise le record avec la data structure
	 */
	protected abstract void initRecord();

	/**
	 * Initialise l'enregistrement avec les donn�es
	 * @param data
	 * @return
	 */
	public Record setBytes(byte[] data)
	{
		try
		{
			return rf.getNewRecord(analyzeNumeric(data));
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Retourne le format du record
	 * @return
	 */
	public RecordFormat getRecordFormat()
	{
		return rf;
	}

	/**
	 * Pr�pare les variables pour l'analyse des champs lors des requ�tes 
	 */
	private void prepareAnalyze()
	{
		// Pr�paration des masques pour d�tecter si les zones num�riques ne contiennent pas de l'alpha 
		FieldDescription[] descriptions = rf.getFieldDescriptions();
    	offsetField = new int[descriptions.length+1]; // On triche plus simple pour la boucle for ensuite
		offsetField[0] = 0;
    	listFields = new ArrayList<byte[]>();
		
		for (int i=0; i <descriptions.length; i++)
		{
			if (descriptions[i].getDataType().getInstanceType() == AS400DataType.TYPE_ZONED)
			{
				listFields.add(new byte[descriptions[i].getLength()]);
				offsetField[i+1] = offsetField[i] + descriptions[i].getLength();
			}
			else
				if (descriptions[i].getDataType().getInstanceType() == AS400DataType.TYPE_PACKED)
				{
					listFields.add(null);
					offsetField[i+1] = offsetField[i] + (descriptions[i].getLength()+1)/2;
				}
				else
				{
					listFields.add(null);
					offsetField[i+1] = offsetField[i] + descriptions[i].getLength();
				}
		}
	}
	
	/**
	 * Analyse les donn�es num�riques � appliquer sur la datastructure afin d'�viter les crashs
	 * @param contents
	 * @return
	 */
	private byte[] analyzeNumeric(byte[] contents)
	{
		byte[] field;
		
		// On contr�le la validit� des champs d�cimaux (parfois on trouve de l'alpha dedans !!)
		for (int i=0; i <rf.getNumberOfFields(); i++)
		{
			// Si le champs est num�rique alors on lui fait subir le traitement
			if (listFields.get(i) != null)
			{
				field = listFields.get(i);
				System.arraycopy(contents, offsetField[i], field, 0, field.length);
				if (!testNumericContent(field))
					System.arraycopy(field, 0, contents, offsetField[i], field.length);
			}
		}
		return contents;
	}
	
    /**
     * Test le contenu du tableau de byte s'il ne contient pas de valeur num�rique on le force � z�ro 
     * @param field
     * @return
     */
    private boolean testNumericContent(byte[] field)
    {
    	for (int i=0; i<field.length; i++)
    	{
//if (go) System.out.println(String.format("%02X", contents[i]) + " ^ 0xFF = " + String.format("%02X ", (byte)(contents[i] ^ 0xff)) + " " + (byte)(contents[i] ^ 0xff));
    		// Si le r�sultat du masque binaire est sup�rieur � 15 (0x0f) alors ce n'est pas un num�rique
    		if ((field[i] ^ 0xff) > 0x0f)
    		{
    			Arrays.fill(field, (byte)0xf0);
    			return false;
    		}
    	}	
    	return true;
    }

}
