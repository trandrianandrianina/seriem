//=================================================================================================
//==>                                                                       07/06/2013 - 12/09/2013
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import java.sql.Connection;
import java.util.ArrayList;

import com.ibm.as400.access.Record;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.DataStructureRecord;
import ri.seriem.libas400.database.record.ExtendRecord;
import ri.seriem.libas400.database.record.GenericRecord;


public class PgvmsecmManager extends QueryManager
{
	
	/**
	 * Constructeur
	 * @param database
	 */
	public PgvmsecmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Retourne l'�tablissement de l'utilisateur s'il existe, null sinon
	 * @param user
	 * @return
	 */
	public String getEtablissementUser(String user)
	{
		ArrayList<GenericRecord> records = select("Select SEZON1 from "+getLibrary()+".pgvmsec where SEMDM='GVM' and SEUSR='"+ user + "' and SEETB=''");
		
		// A priori, il n'y a qu'un seul enregistrement par profil/etb (� v�rifier)
		if (records.size() == 0) return null;
		
		// On initiaise les donn�es
		GenericRecord record = records.get(0);
		return record.getString("SEZON1").substring(0, 3);
	}

	/**
	 * Retourne les enregistrements pour un utilisateur 
	 * @param user
	 * @return
	 */
	public ExtendRecord getRecordsbyUSERandETB(String user, String etb)
	{
		String ds;
		// Si l'utilisateur est � blanc alors on utilise la description pour la s�curit� globale  
		if (user.trim().equals(""))
			 ds = getClass().getPackage().getName() + ".Pgvmsecds_GLOBAL";
		else // sinon on utilise la description pour la s�curit� user
			 ds = getClass().getPackage().getName() + ".Pgvmsecds_USER";
		final ArrayList<Record> listeRecord = selectWithDecryption("Select SETOP, SECRE, SEMOD, SETRT, SEMDM, SEUSR, SEETB, SEZON1, SEZON2 from "+getLibrary()+".PGVMSEC where SEUSR = '"+user+"' and SEETB='"+etb+"' and SEMDM = 'GVM'", (DataStructureRecord) newObject(ds), 29, 400);
		
		// A priori, il n'y a qu'un seul enregistrement par profil/etb (� v�rifier)
		if (listeRecord.size() == 0) return null;
		
		// Initialisation du record  
		ExtendRecord record = new ExtendRecord();
		record.setRecord(listeRecord.get(0));

		return record;
	}

	
	/**
	 * Retourne le tableau de donn�es de la s�curit�
	 * @param user
	 * @param etb
	 * @return
	 *
	public char[] getSecurityByUSER_ETB(String user, String etb)
	{
		ArrayList<GenericRecord> records = select("Select SEZON1, SEZON2 from "+getLibrary()+".pgvmsec where SEMDM='GVM' and SEUSR='"+ user + "' and SEETB='"+etb+"'");
		
		// A priori, il n'y a qu'un seul enregistrement par profil/etb (� v�rifier)
		if (records.size() == 0) return null;
		
		// On initiaise les donn�es
		GenericRecord record = records.get(0);
		return security.getDecryptSecurity(record.getString("SEZON1").toCharArray(), record.getString("SEZON2").toCharArray());
	}*/

	/**
	 * Retourne l'�tablissement de l'utilisateur s'il existe, null sinon
	 * @param user
	 * @return
	 *
	public ExtendRecord getSecurityRecord(char[] zon1, char[] zon2, boolean secuser)
	{
		if ((zon1 == null) || (zon1.length == 0) || (zon2 == null) || (zon2.length == 0))
		{
			msgErreur += "\n[PgvmsecmManagement] (getSecurityRecord) Les donn�es en entr�e sont � null ou vides.";
			return null;
		}
		
		return getSecurityRecord(security.getDecryptSecurity(zon1, zon2), secuser);
	}*/

	/**
	 * Retourne l'�tablissement de l'utilisateur s'il existe, null sinon
	 * @param user
	 * @return
	 *
	public ExtendRecord getSecurityRecord(char[] data, boolean secuser)
	{
		if ((data == null) || (data.length == 0))
		{
			msgErreur += "\n[PgvmsecmManagement] (getSecurityRecord) Les donn�es en entr�e sont � null ou vides.";
			return null;
		}

		// Conversion du tableau de char en tableau de byte
		byte[] datab = new byte[data.length];
		for (int i=0; i<datab.length; i++)
			datab[i] = (byte)data[i];

		DataStructureRecord datastructure = new Pgvmsecds_ds();
		// On double la taille du buffer car la DS est unique et avec d�calage (1..400-401..800) suivant le cas
		byte[] content = new byte[datastructure.length];
		Arrays.fill(content, (byte)32); // On remplit de blanc le tableau
			
		if (secuser)
			System.arraycopy(datab, 0, content, 0, datab.length);
		else
			System.arraycopy(datab, 400, content, 0, datab.length);
		
		ExtendRecord record = new ExtendRecord();
		record.setRecord(datastructure.setBytes(content));
		return record;
	}*/

}
