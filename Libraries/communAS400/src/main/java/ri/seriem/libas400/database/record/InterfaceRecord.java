package ri.seriem.libas400.database.record;

import com.google.gson.JsonObject;


public interface InterfaceRecord
{
	/**
	 * @param namefield
	 * @param value
	 */
	public void setField(String namefield, Object value);
	
	/**
	 * @param return
	 */
	public Object getField(int indice);

	/**
	 * @param return
	 */
	public Object getField(String namefield);
	
	/**
	 * Retourne le record sous forme d'une ligne (string) avec les donn�es � la bonne place (offset) 
	 * @return 
	 */
	public StringBuffer getFlat();
	
	/**
	 * Initialise l'ent�te avec un enregistrement plat
	 * @param record
	 * @return
	 */
	public boolean setFlat(String record);
	
	/**
	 * Retourne un Objet de type JSON 
	 * API JsonSimple
	public JSONObject getJSON(boolean trim);*/

	/**
	 * Retourne un Objet de type JSON 
	 */
	public JsonObject getJSON(boolean trim);

	/**
	 * Initialise l'ent�te avec un enregistrement au format JSON 
	 * @param record
	 */
	public boolean setJSON(String record);

}
