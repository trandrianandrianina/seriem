package ri.seriem.libas400.dao.exp.database.files;

import ri.seriem.libas400.database.BaseFileDB;
import ri.seriem.libas400.database.QueryManager;

public abstract class FFD_Psemrtlm extends BaseFileDB
{
	// Constantes (valeurs r�cup�r�es via DSPFFD)
	public static final int SIZE_RLCOD			= 1;
	public static final int SIZE_RLETB			= 3;
	public static final int SIZE_RLIND			= 15;
	public static final int SIZE_RLETBT			= 3;
	public static final int SIZE_RLNUMT			= 6;
	public static final int DECIMAL_RLNUMT		= 0;
	public static final int SIZE_RLIN1			= 1;
	public static final int SIZE_RLIN2			= 1;
	public static final int SIZE_RLIN3			= 1;
	public static final int SIZE_RLIN4			= 1;
	public static final int SIZE_RLIN5			= 1;

	// Variables fichiers
	protected char		RLCOD	= ' ';		// Type fiche GVM
	protected String	RLETB	= null;		// Code Etablissement GVM
	protected String	RLIND	= null;		// Indicatif GVM
	protected String	RLETBT	= null;		// Code Etablissement T�l�phone
	protected int		RLNUMT	= 0;		// Num�ro d'ordre T�l�phone
	protected char		RLIN1	= ' ';		// Contact principal
	protected char		RLIN2	= ' ';		// Contact WorkShop
	protected char		RLIN3	= ' ';		// Type de Workflow
	protected char		RLIN4	= ' ';		// Non utilis�
	protected char		RLIN5	= ' ';		// Non utilis�


	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public FFD_Psemrtlm(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Initialise les variables avec les valeurs par d�faut
	 */
	public void initialization()
	{
		RLCOD	= ' ';
		RLETB	= null;
		RLIND	= null;
		RLETBT	= null;
		RLNUMT	= 0;
		RLIN1	= ' ';
		RLIN2	= ' ';
		RLIN3	= ' ';
		RLIN4	= ' ';
		RLIN5	= ' ';
	}

	/**
	 * @return le rLCOD
	 */
	public char getRLCOD()
	{
		return RLCOD;
	}
	/**
	 * @param rLCOD le rLCOD � d�finir
	 */
	public void setRLCOD(char rLCOD)
	{
		RLCOD = rLCOD;
	}
	/**
	 * @return le rLETB
	 */
	public String getRLETB()
	{
		return RLETB;
	}
	/**
	 * @param rLETB le rLETB � d�finir
	 */
	public void setRLETB(String rLETB)
	{
		RLETB = rLETB;
	}
	/**
	 * @return le rLIND
	 */
	public String getRLIND()
	{
		return RLIND;
	}
	/**
	 * @param rLIND le rLIND � d�finir
	 */
	public void setRLIND(String rLIND)
	{
		RLIND = rLIND;
	}
	/**
	 * @return le rLETBT
	 */
	public String getRLETBT()
	{
		return RLETBT;
	}
	/**
	 * @param rLETBT le rLETBT � d�finir
	 */
	public void setRLETBT(String rLETBT)
	{
		RLETBT = rLETBT;
	}
	/**
	 * @return le rLNUMT
	 */
	public int getRLNUMT()
	{
		return RLNUMT;
	}
	/**
	 * @param rLNUMT le rLNUMT � d�finir
	 */
	public void setRLNUMT(int rLNUMT)
	{
		RLNUMT = rLNUMT;
	}
	/**
	 * @return le rLIN1
	 */
	public char getRLIN1()
	{
		return RLIN1;
	}
	/**
	 * @param rLIN1 le rLIN1 � d�finir
	 */
	public void setRLIN1(char rLIN1)
	{
		RLIN1 = rLIN1;
	}
	/**
	 * @return le rLIN2
	 */
	public char getRLIN2()
	{
		return RLIN2;
	}
	/**
	 * @param rLIN2 le rLIN2 � d�finir
	 */
	public void setRLIN2(char rLIN2)
	{
		RLIN2 = rLIN2;
	}
	/**
	 * @return le rLIN3
	 */
	public char getRLIN3()
	{
		return RLIN3;
	}
	/**
	 * @param rLIN3 le rLIN3 � d�finir
	 */
	public void setRLIN3(char rLIN3)
	{
		RLIN3 = rLIN3;
	}
	/**
	 * @return le rLIN4
	 */
	public char getRLIN4()
	{
		return RLIN4;
	}
	/**
	 * @param rLIN4 le rLIN4 � d�finir
	 */
	public void setRLIN4(char rLIN4)
	{
		RLIN4 = rLIN4;
	}
	/**
	 * @return le rLIN5
	 */
	public char getRLIN5()
	{
		return RLIN5;
	}
	/**
	 * @param rLIN5 le rLIN5 � d�finir
	 */
	public void setRLIN5(char rLIN5)
	{
		RLIN5 = rLIN5;
	}
}
