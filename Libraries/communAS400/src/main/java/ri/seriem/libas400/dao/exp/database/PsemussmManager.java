//=================================================================================================
//==>                                                                       05/12/2011 - 14/11/2013
//==> G�re les op�rations sur le fichier (cr�ation, suppression, lecture, ...) 
//=================================================================================================
package ri.seriem.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;


public class PsemussmManager extends QueryManager
{

	/**
	 * Constructeur
	 * @param database
	 */
	public PsemussmManager(Connection database)
	{
		super(database);
	}

	/**
	 * Retourne l'enregistrement pour un profil
	 * Attention la requ�te est sensible � la casse
	 * @param bibenv
	 * @param prf
	 * @return
	 */
	public GenericRecord getRecordForPrf(String bibenv, String prf)
	{
		GenericRecord psemussm = null;
		final ArrayList<GenericRecord> listeRecord = select("Select * from "+bibenv+".psemussm where ussprf = '" + prf +"'");

		if ((listeRecord != null) && (listeRecord.size() > 0))
			psemussm = listeRecord.get(0); 
		else
			msgErreur += "\n[PsemussmManagement] (getRecordForPrf) Utilisateur " + prf + " non trouv� dans le fichier " + bibenv+".PSEMUSSM";
		
		return psemussm;
	}

	/**
	 * R�cup�ration de la biblioth�que utilisateur
	 * @return
	 */
	public String getBibliotheque(String bibenv, String prf)
	{
		GenericRecord psemussm = getRecordForPrf(bibenv, prf);
		if (psemussm == null)
			return null;
		
		return psemussm.getString("USSBIB");
	}
}
