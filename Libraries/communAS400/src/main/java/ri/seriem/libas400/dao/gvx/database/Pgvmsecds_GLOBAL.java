//=================================================================================================
//==>                                                                       10/09/2013 - 10/09/2013
//==> Description de l'enregistrement du fichier Pgvmsecds_ds
//==> G�n�r� avec la classe Qdds3dsToJava du projet AnalyseSourceRPG
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

public class Pgvmsecds_GLOBAL extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "SETOP"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "SECRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "SEMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "SETRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "SEMDM"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEUSR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "SEETB"));

		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU001"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU002"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU003"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU004"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU005"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU006"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU007"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU008"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU009"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU010"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU011"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU012"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU013"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU014"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU015"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU016"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU017"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU018"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU019"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "SEU020"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "SEACT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(8), "SENS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(189), "SEFIL2"));

		//length = 400;
		length = 429;
	}
}
