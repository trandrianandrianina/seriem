package ri.seriem.libas400.dao.exp.programs.contact;

import java.util.ArrayList;

import ri.seriem.libas400.database.BaseGroupDB;
import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.database.record.GenericRecord;

public class GM_ContactExtension extends BaseGroupDB
{
	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public GM_ContactExtension(QueryManager aquerymg)
	{
		super(aquerymg);
	}

	/**
	 * Retourne l'extension d'un contact � partir de son id
	 * @param id
	 * @return
	 */
	public M_ContactExtension readOneContact(int id){
		if( id < 1 ){
			msgErreur += "\nL'id (RYNUM) est inf�rieur � 1.";
			return null;
		}
		if( querymg.getLibrary() == null ){
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PSEMRTYM where RYNUM = " + id);
		if( listrcd == null ){
			return null;
		}
		// zones venant du PSEMRTYM
		M_ContactExtension ec = new M_ContactExtension(querymg);
		ec.initObject(listrcd.get(0), true);
		
		return ec;
	}

	/**
	 * Retourne l'extension d'un contact � partir de son alias (RYSRVM)
	 * @param id
	 * @return
	 *
	public M_ContactExtension readOneContact(String alias){
		if( (alias == null) || (alias.trim().length() == 0) ){
			msgErreur += "\nL'alias (REPRF) est inf�rieur null ou blanc.";
			return null;
		}
		if( querymg.getLibrary() == null ){
			msgErreur += "\nLa CURLIB n'est pas initialis�e.";
			return null;
		}

		ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PSEMRTYM where RYSRVM = '" + alias + "'");
		if( listrcd == null ){
			return null;
		}
		// zones venant du PSEMRTYM
		M_ContactExtension ec = new M_ContactExtension(querymg);
		ec.initObject(listrcd.get(0), true);
		
		return ec;
	}*/

	/*
	public InfosMailServer getInfosMailServer()
	{
		// R�cup�ration de l'alias dans le fichier PSEMRTYM
		M_ContactExtension extcontact = new M_ContactExtension(querymg);
		String alias = extcontact.getRYMAIL();
	}*/
	
	
	@Override
	public void dispose()
	{
		querymg = null;
	}

}
