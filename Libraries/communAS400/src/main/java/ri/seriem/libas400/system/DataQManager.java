//=================================================================================================
//==>                                                                       12/12/2011 - 21/10/2013
//==> Gestion des connexions aux DTAQs
//=================================================================================================
package ri.seriem.libas400.system;

import com.ibm.as400.access.QSYSObjectPathName;

public class DataQManager
{
	// Constantes
	//private static final long serialVersionUID = -799008393011017147L;
    private static final String NOM_CLASSE = "[DataQManager]";

    // Variables
	private SystemManager systeme=null;
    private DataQ dtaq=null;
    private int longueurdtaq=1024;
	private String commentaire="";
	private Thread threadDataQueue=null; 
	
	protected String msgErreur = "";		// Conserve le dernier message d'erreur �mit et non lu

	/**
	 * Constructeur
	 * @param system
	 * @param bib
	 * @param nomdtaq
	 * @param longueur
	 */
    public DataQManager(SystemManager system, String bib, String nomdtaq, int longueur)
    {
    	setSysteme(system);
    	setLongueurdtaq(longueur);
    	connexionDataQueue(bib, nomdtaq);
    }
    
    /**
     * Connexion aux DataQueues
     */
    private void connexionDataQueue(String bib, String nomdtaq)
    {
//System.out.println("--> connexionDataQueue: " + system + " " + bib + " " + nomdtaq);    	
    	final String DataQRJ = QSYSObjectPathName.toPath(bib, nomdtaq, DataQ.EXTENSION);
//System.out.println("--> connexionDataQueue: " + DataQRJ + " " + systeme.getSystem()+ " " + getLongueurdtaq() + " " + getCommentaire());    	
        try
        {
        	dtaq = new DataQ(systeme.getSystem(), DataQRJ, getLongueurdtaq(), getCommentaire());
        }
        catch (Exception e)
        {
        	msgErreur += "\n" + NOM_CLASSE + " (connexionDataQueue) Erreur:" + e.getMessage();
        }
    }        

    /**
     * R�ceptionne un message du programme RPG
     * On teste l'existance de la DTAQ afin d'�viter un bouclage et un remplissage de la file d'attente avec le m�me message  
     */
    public void superviseDataQueue()
    {
    	threadDataQueue = new Thread()
    	{
			public void run()
			{
		        try
		        {
		        	while (isAlive() && (dtaq.exists()))
		        	{
		        		dtaq.readMessage(-1); //DELAI_ATTENTE
		        	}
	            }
		        catch (Exception e) 
		        { 
		        	msgErreur += "\nException sur \u00e9coute de la DTAQ : " + e;
		        }
			}
    	};
    	threadDataQueue.start();
    }

    /**
     * Envoi un message au programme RPG
     *
    public synchronized boolean SendMessageDataQueue(String head, String message)
    {
    	final StringBuffer sb = new StringBuffer(longueurdtaq);
    	
    	// On v�rifie la longeur du message � envoyer
    	sb.append(head);
    	final int taillehead=head.length();
    	int offset=0;
    	int taille_msg=longueurdtaq-taillehead;
    	while ((message.length()-offset) > taille_msg)
    	{
    		sb.append(message.substring(offset, offset+taille_msg));
    		if (!dtaq.writeString(sb.toString(), longueurdtaq))
    		{
    			msgErreur += "\nProbl\u00e8me lors de l'envoi du message dans la dataqueue.";
    			return false;
    		}
    		offset += taille_msg;
    		sb.setLength(taillehead);
    	}
		sb.append(message.substring(offset));
		if (!dtaq.writeString(sb.toString(), longueurdtaq))
		{
			msgErreur += "\nProbl\u00e8me lors de l'envoi du message dans la dataqueue.";
			return false;
		}
        return true;
    }*/

    /**
     * Envoi un message avec ent�te dissoci�e au programme RPG
     * Remplit avec des messages entiers
     */
    public boolean sendMessageDataQueue(StringBuffer head, StringBuffer message, final int sizemsg)
    {
    	// On v�rifie la longeur du message � envoyer
    	final int taillehead=head.length();
    	int offset=0;
    	int taillerest = message.length(); 
    	while (taillerest > sizemsg)
    	{
    		head.append(message.substring(offset, offset+sizemsg));
    		sendMessageDataQueue(head.toString());
    		offset += sizemsg;
    		taillerest -= sizemsg;
    		head.setLength(taillehead);
    	}
    	if (taillerest > 0)
    	{
    		head.append(message.substring(offset));
    		sendMessageDataQueue(head.toString());
    	}
    	return true;
    }

    /**
     * Envoi un message avec ent�te dissoci�e au programme RPG
     * Remplit avec des messages entiers
     */
    public boolean sendMessageDataQueue(StringBuffer head, String message, final int sizemsg)
    {
    	// On v�rifie la longeur du message � envoyer
    	final int taillehead=head.length();
    	int offset=0;
    	int taillerest = message.length(); 
    	while (taillerest > sizemsg)
    	{
    		head.append(message.substring(offset, offset+sizemsg));
    		sendMessageDataQueue(head.toString());
    		offset += sizemsg;
    		taillerest -= sizemsg;
    		head.setLength(taillehead);
    	}
    	if (taillerest > 0)
    	{
    		head.append(message.substring(offset));
    		sendMessageDataQueue(head.toString());
    	}
    	return true;
    }

    /**
     * Fonctionne mais pas avec le client actuel (pas pr�vu pour recuperer un msg coup� au milieu )
     * Envoi un message avec ent�te dissoci�e au programme RPG
     *
    public boolean sendMessageDataQueue(StringBuffer head, StringBuffer message)
    {
    	// On v�rifie la longeur du message � envoyer
    	final int taillehead=head.length();
    	final int taillemsg=longueurdtaq-taillehead;
    	int offset=0;
    	int taillerest = message.length(); 
    	while (taillerest > taillemsg)
    	{
//System.out.println("-sdmsg 1->" + offset + " " + taillemsg + " " + taillerest);
    		head.append(message.substring(offset, offset+taillemsg));
//System.out.println("1->"+ head.toString()+"|");   		
    		sendMessageDataQueue(head.toString());
    		offset += taillemsg;
    		taillerest -= taillemsg;
    		head.setLength(taillehead);
    	}
    	if (taillerest > 0)
    	{
//System.out.println("-sdmsg 2->" + offset + " " + taillemsg + " " + taillerest);
    		head.append(message.substring(offset));
//System.out.println("2->"+ head.toString()+"|");    		
    		sendMessageDataQueue(head.toString());
    	}
    	return true;
    }
    */

    /**
     * Envoi un message au programme RPG
     * @param message
     * @return
     */
    public synchronized boolean sendMessageDataQueue(String message)
    {
		if (!dtaq.writeString(message, longueurdtaq))
		{
			msgErreur += "\nProbl\u00e8me lors de l'envoi du message dans la dataqueue.";
			return false;
		}
		return true;
    }
    
    /**
     * D�connecte les DTAQs si besoin 
     */
    public void disconnectDataQueue()
    {
		if (dtaq != null)
		{
			//if ((threadDataQueue != null) && threadDataQueue.isAlive()) threadDataQueue.interrupt();
			//threadDataQueue = null;
			dtaq.close();
			dtaq = null;
		}
    }

	//-------------------------------------------------------------------------
	//------- Accesseurs ------------------------------------------------------
	//-------------------------------------------------------------------------

	/**
	 * @return the systeme
	 */
	public SystemManager getSysteme()
	{
		return systeme;
	}

	/**
	 * @param systeme the systeme to set
	 */
	public void setSysteme(SystemManager systeme)
	{
		this.systeme = systeme;
	}

	/**
	 * @return the dtaq
	 */
	public DataQ getDtaq()
	{
		return dtaq;
	}

	/**
	 * @param dtaq the dtaq to set
	 */
	public void setDtaq(DataQ dtaq)
	{
		this.dtaq = dtaq;
	}

	/**
	 * @param longueurdtaq the longueurdtaq to set
	 */
	public void setLongueurdtaq(int longueurdtaq)
	{
		this.longueurdtaq = longueurdtaq;
	}

	/**
	 * @return the longueurdtaq
	 */
	public int getLongueurdtaq()
	{
		return longueurdtaq;
	}
	
	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire)
	{
		this.commentaire = commentaire;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire()
	{
		return commentaire;
	}

	//-------------------------------------------------------------------------
	//------- Autres ----------------------------------------------------------
	//-------------------------------------------------------------------------

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}
}
