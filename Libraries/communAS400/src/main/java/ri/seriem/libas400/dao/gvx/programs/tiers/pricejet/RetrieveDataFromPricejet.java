package ri.seriem.libas400.dao.gvx.programs.tiers.pricejet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import ri.seriem.libcommun.outils.Chiffrage;
import ri.seriem.libcommun.outils.DateHeure;

public class RetrieveDataFromPricejet
{
	// Constantes
	private		final static	String							NOM_CLASSE			= "[RetrieveDataFromPricejet]";
	private		final static	String							ERROR_CODE_WAIT		= "?WAIT";	
	private		final static	String							ERROR_CODE_ACCOUNT	= "?ACCOUNT";	
	private		final static	String							ERROR_CODE_AUTH		= "?AUTH";
	private		final static	String							ERROR_MSG_WAIT		= "Il n'y a pas encore d'analyse pour la demande. Il faut attendre que nos robots scannent l'article.";	
	private		final static	String							ERROR_MSG_ACCOUNT	= "Le compte (login) n'existe pas.";	
	private		final static	String							ERROR_MSG_AUTH		= "Votre authentification n'est pas correcte. Il y a un probl�me dans la signature.";
	private		final static	String							URL					= "http://api.pricejet.fr?";
	private		final static	String							SIGNATURE			= "signature=";
	private		final static	String							LOGIN				= "login=";
	private		final static	String							LISTE				= "liste=";
	private		final static	String							EXTENSION			= ".json";

	// TODO � mettre dans le param�trage lorsqu'il existera
	private		final static	String							ACCOUNT				= "PhaseNeutre";
	private		final static	String							KEY					= "a79bd5f48594499a4d89786ed4c0960a";

	// Variables
	private						String							msgErreur			= "";          // Conserve le dernier message d'erreur �mit et non lu
	private						String							tmpfolder			= null;
	private						String							namefile			= null;

	
	/**
	 * Constructeur
	 * @param atmpfolder, dossier temporaire de stockage du fichier r�cup�r�
	 */
	public RetrieveDataFromPricejet(String atmpfolder)
	{
		tmpfolder = atmpfolder;
	}
	
	// -- M�thodes priv�es ----------------------------------------------------
	

	/**
	 * Retourne la signature chiffr�e en MD5
	 * @param account
	 * @param date, au format AAAAMMJJ ou null pour la date du jour
	 * @return
	 */
	private String getSignature(String key, String date)
	{
		// V�rification des param�tres
		if( (key == null) || (key.trim().length() == 0) ){
			addMsgError("getSignature", "La cl� associ�e au compte utilisateur n'est pas renseign�.");
			return null;
		}
		if( date == null)
			date = DateHeure.getJourHeure(4);
		
		return Chiffrage.md5(date + key); // AAAAMMJJ + compte pricejet
	}
	
	/**
	 * Retourne l'url qui permet de r�cup�rer une liste d'article chez Pricejet
	 * @param signature
	 * @param account
	 * @param namelist
	 * @return
	 */
	private URL getURL4List(String signature, String account, String namelist)
	{
		if( (signature == null) || (signature.trim().length() == 0) ){
			addMsgError("getURL4List", "La signature est incorrecte.");
			return null;
		}
		if( (account == null) || (account.trim().length() == 0) ){
			addMsgError("getURL4List", "Le compte utilisateur est incorrect.");
			return null;
		}
		if( (namelist == null) || (namelist.trim().length() == 0) ){
			addMsgError("getURL4List", "Le nom de la liste est incorrecte.");
			return null;
		}
		
		StringBuilder url = new StringBuilder(URL);
		// Ajout de la signature
		url.append(SIGNATURE).append(signature).append('&');
		// Ajout du login
		url.append(LOGIN).append(account).append('&');
		// Ajout de nom de la liste
		url.append(LISTE).append(namelist);
		
		try{
			return new URL(url.toString());
		} catch( MalformedURLException e ){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Cr�er un fichier
	 * @param pathname
	 * @throws IOException 
	 */
	private File createFile(String pathname) throws IOException
	{
		File file = new File(pathname);
		if( !file.exists() ){
			file.createNewFile();
		}
		return file;
	}
	
	/**
	 * Connexion au site de Pricejet
	 * @param url
	 * @param namefile
	 * @return
	 */
	private boolean connexion2Pricejet(URL url, String namefile)
	{
		String text = null;
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8")); 
			File file = createFile(namefile);
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
		    for(String line; (line = reader.readLine()) != null; ){
		    	text = line;
		    	System.out.println("line " + line);
		        bw.write(line);
		    }
		    reader.close();
		    bw.close();
		}catch(Exception e){
			e.printStackTrace();
			addMsgError("retrieveTheList", e.getMessage());
			return false;
		}
		return treatmentError(text); 
	}
	
	/**
	 * Traitement permettant de r�cup�rer le code erreur
	 * @param line
	 * @return
	 */
	private boolean treatmentError(String line)
	{
		if( line == null ) return true;
		
	    if( line.startsWith(ERROR_CODE_WAIT) )
	    	addMsgError(null, ERROR_MSG_WAIT);
	    else if( line.startsWith(ERROR_CODE_ACCOUNT) )
	    	addMsgError(null, ERROR_MSG_ACCOUNT);
	    else if( line.startsWith(ERROR_CODE_AUTH) )
	    	addMsgError(null, ERROR_MSG_AUTH);
	    return false;
	}

	/**
	 * Ajoute un message d'erreur de traitement
	 * @param methodname
	 * @param text
	 */
	private void addMsgError(String methodname, String text)
	{
		if( (methodname == null) || (methodname.trim().length() == 0) ){
			msgErreur += "\n" + NOM_CLASSE + '\t' + text;
		} else{
			msgErreur += "\n" + NOM_CLASSE + "\t(" + methodname + ")\t" + text;
		}
	}
	
	// -- M�thodes publiques --------------------------------------------------
	
	
	/**
	 * R�cup�re la liste souhait�e 
	 * @param namelist
	 * @param date
	 * @return
	 */
	public boolean retrieveTheList(String namelist, String date)
	{
		namefile = tmpfolder + File.separator + "pricejet_" + namelist + EXTENSION;
		String signature = getSignature(KEY, date);
		URL url = getURL4List(signature, ACCOUNT, namelist);
		System.out.println(url.toString());
		return connexion2Pricejet(url, namefile);
	}
	
	/**
	 * Retourne le chemin complet du fichier r�cup�r�
	 * @return
	 */
	public String getTempFile()
	{
		return namefile;
	}
	
	/**
	 * Retourne le message d'erreur
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
