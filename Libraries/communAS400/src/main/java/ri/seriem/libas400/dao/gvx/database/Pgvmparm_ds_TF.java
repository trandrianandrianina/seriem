//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_TF pour les TF
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_TF extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(96), "TFT"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 1), "TFFF1"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 1), "TFFF2"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFNCO"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFPMP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "TFC")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "TFTXP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "TFRBP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TFCP"));

		length = 300;
	}
}
