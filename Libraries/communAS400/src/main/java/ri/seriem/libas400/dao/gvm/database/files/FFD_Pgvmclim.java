package ri.seriem.libas400.dao.gvm.database.files;



import ri.seriem.libas400.database.BaseFileDB;
import ri.seriem.libas400.database.QueryManager;

public abstract class FFD_Pgvmclim extends BaseFileDB
{
	// Constantes (valeurs r�cup�r�es via DSPFFD)
	public static final int SIZE_CLTOP			= 1;
	public static final int DECIMAL_CLTOP		= 0;
	public static final int SIZE_CLCRE			= 7;
	public static final int DECIMAL_CLCRE		= 0;
	public static final int SIZE_CLMOD			= 7;
	public static final int DECIMAL_CLMOD		= 0;
	public static final int SIZE_CLTRT			= 7;
	public static final int DECIMAL_CLTRT		= 0;
	public static final int SIZE_CLETB			= 3;
	public static final int SIZE_CLCLI			= 6;
	public static final int DECIMAL_CLCLI		= 0;
	public static final int SIZE_CLLIV			= 3;
	public static final int DECIMAL_CLLIV		= 0;
	public static final int SIZE_CLNOM			= 30;
	public static final int SIZE_CLCPL			= 30;
	public static final int SIZE_CLRUE			= 30;
	public static final int SIZE_CLLOC			= 30;
	public static final int SIZE_CLVIL			= 30;
	public static final int SIZE_CLPAY			= 26;
	public static final int SIZE_CLFIL			= 1;
	public static final int SIZE_CLCOP			= 3;
	public static final int SIZE_CLCDP1			= 5;
	public static final int DECIMAL_CLCDP1		= 0;
	public static final int SIZE_CLTOP1			= 2;
	public static final int SIZE_CLTOP2			= 2;
	public static final int SIZE_CLTOP3			= 2;
	public static final int SIZE_CLTOP4			= 2;
	public static final int SIZE_CLTOP5			= 2;
	public static final int SIZE_CLCAT			= 3;
	public static final int SIZE_CLCLK			= 15;
	public static final int SIZE_CLAPE			= 4;
	public static final int SIZE_CLTEL			= 20;
	public static final int SIZE_CLFAX			= 20;
	public static final int SIZE_CLTLX			= 7;
	public static final int SIZE_CLLAN			= 6;
	public static final int SIZE_CLOBS			= 18;
	public static final int SIZE_CLREP			= 2;
	public static final int SIZE_CLREP2			= 2;
	public static final int SIZE_CLTRP			= 1;
	public static final int SIZE_CLGEO			= 5;
	public static final int SIZE_CLMEX			= 2;
	public static final int SIZE_CLCTR			= 2;
	public static final int SIZE_CLMAG			= 2;
	public static final int SIZE_CLFRP			= 7;
	public static final int DECIMAL_CLFRP		= 0;
	public static final int SIZE_CLTFA			= 1;
	public static final int SIZE_CLTTC			= 1;
	public static final int SIZE_CLDEV			= 3;
	public static final int SIZE_CLCLFP			= 6;
	public static final int DECIMAL_CLCLFP		= 0;
	public static final int SIZE_CLCLFS			= 3;
	public static final int DECIMAL_CLCLFS		= 0;
	public static final int SIZE_CLCLP			= 6;
	public static final int DECIMAL_CLCLP		= 0;
	public static final int SIZE_CLAFA			= 7;
	public static final int SIZE_CLESC			= 3;
	public static final int DECIMAL_CLESC		= 2;
	public static final int SIZE_CLTAR			= 1;
	public static final int DECIMAL_CLTAR		= 0;
	public static final int SIZE_CLTA1			= 1;
	public static final int DECIMAL_CLTA1		= 0;
	public static final int SIZE_CLREM1			= 4;
	public static final int DECIMAL_CLREM1		= 2;
	public static final int SIZE_CLREM2			= 4;
	public static final int DECIMAL_CLREM2		= 2;
	public static final int SIZE_CLREM3			= 4;
	public static final int DECIMAL_CLREM3		= 2;
	public static final int SIZE_CLRP1			= 4;
	public static final int DECIMAL_CLRP1		= 2;
	public static final int SIZE_CLRP2			= 4;
	public static final int DECIMAL_CLRP2		= 2;
	public static final int SIZE_CLRP3			= 4;
	public static final int DECIMAL_CLRP3		= 2;
	public static final int SIZE_CLCNV			= 6;
	public static final int SIZE_CLPFA			= 1;
	public static final int SIZE_CLPRL			= 1;
	public static final int SIZE_CLRLV			= 1;
	public static final int SIZE_CLRBF			= 1;
	public static final int DECIMAL_CLRBF		= 0;
	public static final int SIZE_CLRGL			= 2;
	public static final int SIZE_CLECH			= 2;
	public static final int SIZE_CLNCG			= 6;
	public static final int DECIMAL_CLNCG		= 0;
	public static final int SIZE_CLNCA			= 6;
	public static final int DECIMAL_CLNCA		= 0;
	public static final int SIZE_CLACT			= 4;
	public static final int SIZE_CLPLF			= 7;
	public static final int DECIMAL_CLPLF		= 0;
	public static final int SIZE_CLPLF2			= 7;
	public static final int DECIMAL_CLPLF2		= 0;
	public static final int SIZE_CLTNS			= 1;
	public static final int DECIMAL_CLTNS		= 0;
	public static final int SIZE_CLATT			= 18;
	public static final int SIZE_CLNOT			= 2;
	public static final int SIZE_CLDDV			= 7;
	public static final int DECIMAL_CLDDV		= 0;
	public static final int SIZE_CLDVE			= 7;
	public static final int DECIMAL_CLDVE		= 0;
	public static final int SIZE_CLSRN			= 9;
	public static final int SIZE_CLSRT			= 5;
	public static final int SIZE_CLFIL1			= 1;
	public static final int SIZE_CLCEE			= 3;
	public static final int SIZE_CLVAE			= 7;
	public static final int DECIMAL_CLVAE		= 0;
	public static final int SIZE_CLCDE			= 9;
	public static final int DECIMAL_CLCDE		= 2;
	public static final int SIZE_CLEXP			= 9;
	public static final int DECIMAL_CLEXP		= 2;
	public static final int SIZE_CLFAC			= 9;
	public static final int DECIMAL_CLFAC		= 2;
	public static final int SIZE_CLPCO			= 7;
	public static final int DECIMAL_CLPCO		= 0;
	public static final int SIZE_CLCGM			= 1;
	public static final int DECIMAL_CLCGM		= 0;
	public static final int SIZE_CLMAJ			= 1;
	public static final int DECIMAL_CLMAJ		= 0;
	public static final int SIZE_CLNEX1			= 1;
	public static final int DECIMAL_CLNEX1		= 0;
	public static final int SIZE_CLNEX2			= 1;
	public static final int DECIMAL_CLNEX2		= 0;
	public static final int SIZE_CLNEX3			= 1;
	public static final int DECIMAL_CLNEX3		= 0;
	public static final int SIZE_CLNLA			= 4;
	public static final int SIZE_CLRST			= 6;
	public static final int SIZE_CLIN1			= 1;
	public static final int SIZE_CLIN2			= 1;
	public static final int SIZE_CLIN3			= 1;
	public static final int SIZE_CLIN4			= 1;
	public static final int SIZE_CLIN5			= 1;
	public static final int SIZE_CLIN6			= 1;
	public static final int SIZE_CLIN7			= 1;
	public static final int SIZE_CLIN8			= 1;
	public static final int SIZE_CLIN9			= 1;
	public static final int SIZE_CLIN10			= 1;
	public static final int SIZE_CLIN11			= 1;
	public static final int SIZE_CLIN12			= 1;
	public static final int SIZE_CLIN13			= 1;
	public static final int SIZE_CLIN14			= 1;
	public static final int SIZE_CLIN15			= 1;
	public static final int SIZE_CLADH			= 15;
	public static final int SIZE_CLCNC			= 4;
	public static final int SIZE_CLCC1			= 6;
	public static final int DECIMAL_CLCC1		= 0;
	public static final int SIZE_CLCC2			= 6;
	public static final int DECIMAL_CLCC2		= 0;
	public static final int SIZE_CLCC3			= 6;
	public static final int DECIMAL_CLCC3		= 0;
	public static final int SIZE_CLFIR			= 5;
	public static final int SIZE_CLTRA			= 6;
	public static final int DECIMAL_CLTRA		= 0;
	public static final int SIZE_CLCNR			= 5;
	public static final int SIZE_CLCNP			= 5;
	public static final int SIZE_CLCNB			= 5;
	public static final int SIZE_CLDPL			= 7;
	public static final int DECIMAL_CLDPL		= 0;
	public static final int SIZE_CLIN16			= 1;
	public static final int SIZE_CLIN17			= 1;
	public static final int SIZE_CLCRT			= 5;
	public static final int SIZE_CLOTO			= 5;
	public static final int DECIMAL_CLOTO		= 0;
	public static final int SIZE_CLCAN			= 3;
	public static final int SIZE_CLPRN			= 30;
	public static final int SIZE_CLREC			= 1;
	public static final int SIZE_CLFRC			= 1;
	public static final int SIZE_CLMTC			= 1;
	public static final int SIZE_CLNIP			= 2;
	public static final int SIZE_CLNIK			= 2;
	public static final int SIZE_CLCL1			= 1;
	public static final int SIZE_CLCL2			= 2;
	public static final int SIZE_CLCL3			= 2;
	public static final int SIZE_CLCLA			= 15;
	public static final int SIZE_CLIN18			= 1;
	public static final int SIZE_CLIN19			= 1;
	public static final int SIZE_CLIN20			= 1;
	public static final int SIZE_CLIN21			= 1;
	public static final int SIZE_CLIN22			= 1;
	public static final int SIZE_CLIN23			= 1;
	public static final int SIZE_CLIN24			= 1;
	public static final int SIZE_CLIN25			= 1;
	public static final int SIZE_CLIN26			= 1;
	public static final int SIZE_CLIN27			= 1;
	public static final int SIZE_CLAPEN			= 5;
	public static final int SIZE_CLPLF3			= 7;
	public static final int DECIMAL_CLPLF3		= 0;
	public static final int SIZE_CLDPL3			= 7;
	public static final int DECIMAL_CLDPL3		= 0;
	public static final int SIZE_CLIDAS			= 15;
	public static final int SIZE_CLCAS			= 5;
	public static final int SIZE_CLDVP3			= 7;
	public static final int DECIMAL_CLDVP3		= 0;
	public static final int SIZE_CLDAT1			= 7;
	public static final int DECIMAL_CLDAT1		= 0;
	public static final int SIZE_CLDAT2			= 7;
	public static final int DECIMAL_CLDAT2		= 0;
	public static final int SIZE_CLDAT3			= 7;
	public static final int DECIMAL_CLDAT3		= 0;
	public static final int SIZE_CLFFP1			= 4;
	public static final int DECIMAL_CLFFP1		= 2;
	public static final int SIZE_CLFFP2			= 4;
	public static final int DECIMAL_CLFFP2		= 2;
	public static final int SIZE_CLFFP3			= 4;
	public static final int DECIMAL_CLFFP3		= 2;
	public static final int SIZE_CLIN28			= 1;
	public static final int SIZE_CLIN29			= 1;
	public static final int SIZE_CLIN30			= 1;

	// Variables fichiers
	protected int		CLTOP	= 0;		// Top syst�me
	protected int		CLCRE	= 0;		// Date de cr�ation
	protected int		CLMOD	= 0;		// Date de modification
	protected int		CLTRT	= 0;		// Date de traitement
	protected String	CLETB	= null;		// Code �tablissement
	protected int		CLCLI	= 0;		// Code Client
	protected int		CLLIV	= 0;		// Suffixe de livraison
	protected String	CLNOM	= null;		// Nom ou raison sociale
	protected String	CLCPL	= null;		// Compl�ment de nom
	protected String	CLRUE	= null;		// Adresse - rue
	protected String	CLLOC	= null;		// Adresse - localit�
	protected String	CLVIL	= null;		// Adresse - ville
	protected String	CLPAY	= null;		// Adresse - pays
	protected char		CLFIL	= ' ';		// 
	protected String	CLCOP	= null;		// Code pays
	protected int		CLCDP1	= 0;		// Code Postal
	protected String	CLTOP1	= null;		// Crit�res d'analyse 1
	protected String	CLTOP2	= null;		// Crit�res d'analyse 2
	protected String	CLTOP3	= null;		// Crit�res d'analyse 3
	protected String	CLTOP4	= null;		// Crit�res d'analyse 4
	protected String	CLTOP5	= null;		// Crit�res d'analyse 5
	protected String	CLCAT	= null;		// Code cat�gorie
	protected String	CLCLK	= null;		// Cl� de classement alpha
	protected String	CLAPE	= null;		// Cod  APE
	protected String	CLTEL	= null;		// T�l�phone
	protected String	CLFAX	= null;		// T�l�copie
	protected String	CLTLX	= null;		// T�lex
	protected String	CLLAN	= null;		// Code Langue
	protected String	CLOBS	= null;		// Observations
	protected String	CLREP	= null;		// Code repr�sentant
	protected String	CLREP2	= null;		// Code repr�sentant 2
	protected char		CLTRP	= ' ';		// Type commissionnement rep.
	protected String	CLGEO	= null;		// Zone g�ographique
	protected String	CLMEX	= null;		// Mode d'exp�dition
	protected String	CLCTR	= null;		// Code transporteur habituel
	protected String	CLMAG	= null;		// Code magasin de rattachement
	protected int		CLFRP	= 0;		// Montant franco de port
	protected char		CLTFA	= ' ';		// Type de facturation
	protected char		CLTTC	= ' ';		// Facture ttc oui/non
	protected String	CLDEV	= null;		// Code devise
	protected int		CLCLFP	= 0;		// Code client factur�
	protected int		CLCLFS	= 0;		// Code client factur� suffixe
	protected int		CLCLP	= 0;		// Code Client Payeur
	protected String	CLAFA	= null;		// Code Affacturage
	protected int		CLESC	= 0;		// Taux d'escompte
	protected int		CLTAR	= 0;		// Num�ro Tarif
	protected int		CLTA1	= 0;		// 2�me num�ro de tarif
	protected int		CLREM1	= 0;		// Remise 1
	protected int		CLREM2	= 0;		// Remise 2
	protected int		CLREM3	= 0;		// Remise 3
	protected int		CLRP1	= 0;		// Remise Pied 1
	protected int		CLRP2	= 0;		// Remise Pied 2
	protected int		CLRP3	= 0;		// Remise Pied 3
	protected String	CLCNV	= null;		// Code condition de vente
	protected char		CLPFA	= ' ';		// P�riodicit� de facturation
	protected char		CLPRL	= ' ';		// P�riodicit� de relev�
	protected char		CLRLV	= ' ';		// Code relev�
	protected int		CLRBF	= 0;		// Regroupement Bon sur Facture
	protected String	CLRGL	= null;		// Code r�glement
	protected String	CLECH	= null;		// Code �ch�ance
	protected int		CLNCG	= 0;		// Collectif comptable
	protected int		CLNCA	= 0;		// Compte auxiliaire
	protected String	CLACT	= null;		// Code affaire en cours
	protected int		CLPLF	= 0;		// Plafond encours autoris�
	protected int		CLPLF2	= 0;		// Sur-Plafond encours autoris�
	protected int		CLTNS	= 0;		// Top Attn
	protected String	CLATT	= null;		// Texte d'attention pour cde
	protected String	CLNOT	= null;		// Note CLient
	protected int		CLDDV	= 0;		// Date de derni�re visite
	protected int		CLDVE	= 0;		// Date de derni�re vente
	protected String	CLSRN	= null;		// Num�ro SIREN
	protected String	CLSRT	= null;		// Compl�ment SIRET
	protected char		CLFIL1	= ' ';		// Filler (ex NII)
	protected String	CLCEE	= null;		// Code pays CEE
	protected int		CLVAE	= 0;		// Vente assimil� export
	protected int		CLCDE	= 0;		// Encours / Commande
	protected int		CLEXP	= 0;		// Encours / Exp�di�
	protected int		CLFAC	= 0;		// Encours / Factur�
	protected int		CLPCO	= 0;		// Position comptable
	protected int		CLCGM	= 0;		// Zone syst�me pour maj CGM
	protected int		CLMAJ	= 0;		// Top MAJ
	protected int		CLNEX1	= 0;		// Nombre exemplaires Bon Hom.
	protected int		CLNEX2	= 0;		// Nombre exemplaires Bon Exp.
	protected int		CLNEX3	= 0;		// Nombre exemplaires facture
	protected String	CLNLA	= null;		// Num�ro libell� article/Edt
	protected String	CLRST	= null;		// Zone regroupement stats
	protected char		CLIN1	= ' ';		// Livraison partielle oui/non
	protected char		CLIN2	= ' ';		// Reliquats accept�s  oui/non
	protected char		CLIN3	= ' ';		// BL chiffr�  oui/non
	protected char		CLIN4	= ' ';		// Non utilisation DGCNV
	protected char		CLIN5	= ' ';		// Code GBA � r�percuter / bon
	protected char		CLIN6	= ' ';		// "C" rfa sur centrale
	protected char		CLIN7	= ' ';		// Non �dition etq colis / BL
	protected char		CLIN8	= ' ';		// Edit. Fact,Bon Exp enchain�
	protected char		CLIN9	= ' ';		// Type Image Client
	protected char		CLIN10	= ' ';		// Exclusion CNV Quantitative
	protected char		CLIN11	= ' ';		// Pas ETQ colis / Exp�dit�
	protected char		CLIN12	= ' ';		// Fac.Dif./ sur Cde sold�e
	protected char		CLIN13	= ' ';		// Non Soumis � taxe addit.
	protected char		CLIN14	= ' ';		// Code PE particip. frais exp.
	protected char		CLIN15	= ' ';		// G�n.CNV si FAC avec prix net
	protected String	CLADH	= null;		// Code adh�rent
	protected String	CLCNC	= null;		// Code Cond. commissionnement
	protected int		CLCC1	= 0;		// Centrale 1
	protected int		CLCC2	= 0;		// Centrale 2
	protected int		CLCC3	= 0;		// Centrale 3
	protected String	CLFIR	= null;		// Code Firme
	protected int		CLTRA	= 0;		// Client Transitaire
	protected String	CLCNR	= null;		// Code condition ristourne
	protected String	CLCNP	= null;		// Code condition Promo
	protected String	CLCNB	= null;		// Code remise pied de bon
	protected int		CLDPL	= 0;		// Date Limite pour Sur-plafond
	protected char		CLIN16	= ' ';		// Application frais fixes (CC)
	protected char		CLIN17	= ' ';		// Application remise pied fact
	protected String	CLCRT	= null;		// Code Regroupt.Transport
	protected int		CLOTO	= 0;		// Ordre dans la Tourn�e
	protected String	CLCAN	= null;		// Canal de vente
	protected String	CLPRN	= null;		// Pr�noms
	protected char		CLREC	= ' ';		// (R)R�cence commande
	protected char		CLFRC	= ' ';		// (F)Fr�quence commande
	protected char		CLMTC	= ' ';		// (M)Montant commande
	protected String	CLNIP	= null;		// Code pays TVA Intracommun.
	protected String	CLNIK	= null;		// Cl� infor.TVA Intracommun.
	protected char		CLCL1	= ' ';		// Classe de client 1
	protected String	CLCL2	= null;		// Classe de client 2
	protected String	CLCL3	= null;		// Classe de client 3
	protected String	CLCLA	= null;		// Cl� de classement 2 alpha
	protected char		CLIN18	= ' ';		// Client soumis � droitde pr�t
	protected char		CLIN19	= ' ';		// NU
	protected char		CLIN20	= ' ';		// Pr�commandes accept�es O/N
	protected char		CLIN21	= ' ';		// Nombre exemplaires avoirs
	protected char		CLIN22	= ' ';		// Type de vente param�tre VT
	protected char		CLIN23	= ' ';		// Tranche Effectif
	protected char		CLIN24	= ' ';		// Code PE particip. frais exp2
	protected char		CLIN25	= ' ';		// Code PE particip. frais exp3
	protected char		CLIN26	= ' ';		// Non utilis�
	protected char		CLIN27	= ' ';		// Non utilis�
	protected String	CLAPEN	= null;		// Code APE v.2
	protected int		CLPLF3	= 0;		// Plafond demand�
	protected int		CLDPL3	= 0;		// Date demande du plafond
	protected String	CLIDAS	= null;		// Identifiant assurance
	protected String	CLCAS	= null;		// Code assurance
	protected int		CLDVP3	= 0;		// Date validit� du plafond
	protected int		CLDAT1	= 0;		// CA Previsionnel
	protected int		CLDAT2	= 0;		// non utilis�e
	protected int		CLDAT3	= 0;		// non utilis�e
	protected int		CLFFP1	= 0;		// frais fact. pied 1
	protected int		CLFFP2	= 0;		// frais fact. pied 2
	protected int		CLFFP3	= 0;		// frais fact. pied 3
	protected char		CLIN28	= ' ';		// Non utilis�
	protected char		CLIN29	= ' ';		// Non utilis�
	protected char		CLIN30	= ' ';		// Non utilis�

	/**
	 * Constructeur
	 * @param aquerymg
	 */
	public FFD_Pgvmclim(QueryManager aquerymg)
	{
		super(aquerymg);
	}


	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Initialise les variables avec les valeurs par d�faut
	 */
	public void initialization()
	{
		CLTOP	= 0;
		CLCRE	= 0;
		CLMOD	= 0;
		CLTRT	= 0;
		CLETB	= null;
		CLCLI	= 0;
		CLLIV	= 0;
		CLNOM	= null;
		CLCPL	= null;
		CLRUE	= null;
		CLLOC	= null;
		CLVIL	= null;
		CLPAY	= null;
		CLFIL	= ' '; 
		CLCOP	= null;
		CLCDP1	= 0;
		CLTOP1	= null;
		CLTOP2	= null;
		CLTOP3	= null;
		CLTOP4	= null;
		CLTOP5	= null;
		CLCAT	= null;
		CLCLK	= null;
		CLAPE	= null;
		CLTEL	= null;
		CLFAX	= null;
		CLTLX	= null;
		CLLAN	= null;
		CLOBS	= null;
		CLREP	= null;
		CLREP2	= null;
		CLTRP	= ' ';
		CLGEO	= null;
		CLMEX	= null;
		CLCTR	= null;
		CLMAG	= null;
		CLFRP	= 0;
		CLTFA	= ' ';
		CLTTC	= ' ';
		CLDEV	= null;
		CLCLFP	= 0;
		CLCLFS	= 0;
		CLCLP	= 0;
		CLAFA	= null;
		CLESC	= 0;
		CLTAR	= 0;
		CLTA1	= 0;
		CLREM1	= 0;
		CLREM2	= 0;
		CLREM3	= 0;
		CLRP1	= 0;
		CLRP2	= 0;
		CLRP3	= 0;
		CLCNV	= null;
		CLPFA	= ' ';
		CLPRL	= ' ';
		CLRLV	= ' ';
		CLRBF	= 0;
		CLRGL	= null;
		CLECH	= null;
		CLNCG	= 0;
		CLNCA	= 0;
		CLACT	= null;
		CLPLF	= 0;
		CLPLF2	= 0;
		CLTNS	= 0;
		CLATT	= null;
		CLNOT	= null;
		CLDDV	= 0;
		CLDVE	= 0;
		CLSRN	= null;
		CLSRT	= null;
		CLFIL1	= ' ';
		CLCEE	= null;
		CLVAE	= 0;
		CLCDE	= 0;
		CLEXP	= 0;
		CLFAC	= 0;
		CLPCO	= 0;
		CLCGM	= 0;
		CLMAJ	= 0;
		CLNEX1	= 0;
		CLNEX2	= 0;
		CLNEX3	= 0;
		CLNLA	= null;
		CLRST	= null;
		CLIN1	= ' ';
		CLIN2	= ' ';
		CLIN3	= ' ';
		CLIN4	= ' ';
		CLIN5	= ' ';
		CLIN6	= ' ';
		CLIN7	= ' ';
		CLIN8	= ' ';
		CLIN9	= ' ';
		CLIN10	= ' ';
		CLIN11	= ' ';
		CLIN12	= ' ';
		CLIN13	= ' ';
		CLIN14	= ' ';
		CLIN15	= ' ';
		CLADH	= null;
		CLCNC	= null;
		CLCC1	= 0;
		CLCC2	= 0;
		CLCC3	= 0;
		CLFIR	= null;
		CLTRA	= 0;
		CLCNR	= null;
		CLCNP	= null;
		CLCNB	= null;
		CLDPL	= 0;
		CLIN16	= ' ';
		CLIN17	= ' ';
		CLCRT	= null;
		CLOTO	= 0;
		CLCAN	= null;
		CLPRN	= null;
		CLREC	= ' ';
		CLFRC	= ' ';
		CLMTC	= ' ';
		CLNIP	= null;
		CLNIK	= null;
		CLCL1	= ' ';
		CLCL2	= null;
		CLCL3	= null;
		CLCLA	= null;
		CLIN18	= ' ';
		CLIN19	= ' ';
		CLIN20	= ' ';
		CLIN21	= ' ';
		CLIN22	= ' ';
		CLIN23	= ' ';
		CLIN24	= ' ';
		CLIN25	= ' ';
		CLIN26	= ' ';
		CLIN27	= ' ';
		CLAPEN	= null;
		CLPLF3	= 0;
		CLDPL3	= 0;
		CLIDAS	= null;
		CLCAS	= null;
		CLDVP3	= 0;
		CLDAT1	= 0;
		CLDAT2	= 0;
		CLDAT3	= 0;
		CLFFP1	= 0;
		CLFFP2	= 0;
		CLFFP3	= 0;
		CLIN28	= ' ';
		CLIN29	= ' ';
		CLIN30	= ' ';
	}


	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le cLTOP
	 */
	public int getCLTOP()
	{
		return CLTOP;
	}

	/**
	 * @param cLTOP le cLTOP � d�finir
	 */
	public void setCLTOP(int cLTOP)
	{
		CLTOP = cLTOP;
	}

	/**
	 * @return le cLCRE
	 */
	public int getCLCRE()
	{
		return CLCRE;
	}

	/**
	 * @param cLCRE le cLCRE � d�finir
	 */
	public void setCLCRE(int cLCRE)
	{
		CLCRE = cLCRE;
	}

	/**
	 * @return le cLMOD
	 */
	public int getCLMOD()
	{
		return CLMOD;
	}

	/**
	 * @param cLMOD le cLMOD � d�finir
	 */
	public void setCLMOD(int cLMOD)
	{
		CLMOD = cLMOD;
	}

	/**
	 * @return le cLTRT
	 */
	public int getCLTRT()
	{
		return CLTRT;
	}

	/**
	 * @param cLTRT le cLTRT � d�finir
	 */
	public void setCLTRT(int cLTRT)
	{
		CLTRT = cLTRT;
	}

	/**
	 * @return le cLETB
	 */
	public String getCLETB()
	{
		return CLETB;
	}

	/**
	 * @param cLETB le cLETB � d�finir
	 */
	public void setCLETB(String cLETB)
	{
		CLETB = cLETB;
	}

	/**
	 * @return le cLCLI
	 */
	public int getCLCLI()
	{
		return CLCLI;
	}

	/**
	 * @param cLCLI le cLCLI � d�finir
	 */
	public void setCLCLI(int cLCLI)
	{
		CLCLI = cLCLI;
	}

	/**
	 * @return le cLLIV
	 */
	public int getCLLIV()
	{
		return CLLIV;
	}

	/**
	 * @param cLLIV le cLLIV � d�finir
	 */
	public void setCLLIV(int cLLIV)
	{
		CLLIV = cLLIV;
	}

	/**
	 * @return le cLNOM
	 */
	public String getCLNOM()
	{
		return CLNOM;
	}

	/**
	 * @param cLNOM le cLNOM � d�finir
	 */
	public void setCLNOM(String cLNOM)
	{
		CLNOM = cLNOM;
	}

	/**
	 * @return le cLCPL
	 */
	public String getCLCPL()
	{
		return CLCPL;
	}

	/**
	 * @param cLCPL le cLCPL � d�finir
	 */
	public void setCLCPL(String cLCPL)
	{
		CLCPL = cLCPL;
	}

	/**
	 * @return le cLRUE
	 */
	public String getCLRUE()
	{
		return CLRUE;
	}

	/**
	 * @param cLRUE le cLRUE � d�finir
	 */
	public void setCLRUE(String cLRUE)
	{
		CLRUE = cLRUE;
	}

	/**
	 * @return le cLLOC
	 */
	public String getCLLOC()
	{
		return CLLOC;
	}

	/**
	 * @param cLLOC le cLLOC � d�finir
	 */
	public void setCLLOC(String cLLOC)
	{
		CLLOC = cLLOC;
	}

	/**
	 * @return le cLVIL
	 */
	public String getCLVIL()
	{
		return CLVIL;
	}

	/**
	 * @param cLVIL le cLVIL � d�finir
	 */
	public void setCLVIL(String cLVIL)
	{
		CLVIL = cLVIL;
	}

	/**
	 * @return le cLPAY
	 */
	public String getCLPAY()
	{
		return CLPAY;
	}

	/**
	 * @param cLPAY le cLPAY � d�finir
	 */
	public void setCLPAY(String cLPAY)
	{
		CLPAY = cLPAY;
	}

	/**
	 * @return le cLFIL
	 */
	public char getCLFIL()
	{
		return CLFIL;
	}

	/**
	 * @param cLFIL le cLFIL � d�finir
	 */
	public void setCLFIL(char cLFIL)
	{
		CLFIL = cLFIL;
	}

	/**
	 * @return le cLCOP
	 */
	public String getCLCOP()
	{
		return CLCOP;
	}

	/**
	 * @param cLCOP le cLCOP � d�finir
	 */
	public void setCLCOP(String cLCOP)
	{
		CLCOP = cLCOP;
	}

	/**
	 * @return le cLCDP1
	 */
	public int getCLCDP1()
	{
		return CLCDP1;
	}

	/**
	 * @param cLCDP1 le cLCDP1 � d�finir
	 */
	public void setCLCDP1(int cLCDP1)
	{
		CLCDP1 = cLCDP1;
	}

	/**
	 * @return le cLTOP1
	 */
	public String getCLTOP1()
	{
		return CLTOP1;
	}

	/**
	 * @param cLTOP1 le cLTOP1 � d�finir
	 */
	public void setCLTOP1(String cLTOP1)
	{
		CLTOP1 = cLTOP1;
	}

	/**
	 * @return le cLTOP2
	 */
	public String getCLTOP2()
	{
		return CLTOP2;
	}

	/**
	 * @param cLTOP2 le cLTOP2 � d�finir
	 */
	public void setCLTOP2(String cLTOP2)
	{
		CLTOP2 = cLTOP2;
	}

	/**
	 * @return le cLTOP3
	 */
	public String getCLTOP3()
	{
		return CLTOP3;
	}

	/**
	 * @param cLTOP3 le cLTOP3 � d�finir
	 */
	public void setCLTOP3(String cLTOP3)
	{
		CLTOP3 = cLTOP3;
	}

	/**
	 * @return le cLTOP4
	 */
	public String getCLTOP4()
	{
		return CLTOP4;
	}

	/**
	 * @param cLTOP4 le cLTOP4 � d�finir
	 */
	public void setCLTOP4(String cLTOP4)
	{
		CLTOP4 = cLTOP4;
	}

	/**
	 * @return le cLTOP5
	 */
	public String getCLTOP5()
	{
		return CLTOP5;
	}

	/**
	 * @param cLTOP5 le cLTOP5 � d�finir
	 */
	public void setCLTOP5(String cLTOP5)
	{
		CLTOP5 = cLTOP5;
	}

	/**
	 * @return le cLCAT
	 */
	public String getCLCAT()
	{
		return CLCAT;
	}

	/**
	 * @param cLCAT le cLCAT � d�finir
	 */
	public void setCLCAT(String cLCAT)
	{
		CLCAT = cLCAT;
	}

	/**
	 * @return le cLCLK
	 */
	public String getCLCLK()
	{
		return CLCLK;
	}

	/**
	 * @param cLCLK le cLCLK � d�finir
	 */
	public void setCLCLK(String cLCLK)
	{
		CLCLK = cLCLK;
	}

	/**
	 * @return le cLAPE
	 */
	public String getCLAPE()
	{
		return CLAPE;
	}

	/**
	 * @param cLAPE le cLAPE � d�finir
	 */
	public void setCLAPE(String cLAPE)
	{
		CLAPE = cLAPE;
	}

	/**
	 * @return le cLTEL
	 */
	public String getCLTEL()
	{
		return CLTEL;
	}

	/**
	 * @param cLTEL le cLTEL � d�finir
	 */
	public void setCLTEL(String cLTEL)
	{
		CLTEL = cLTEL;
	}

	/**
	 * @return le cLFAX
	 */
	public String getCLFAX()
	{
		return CLFAX;
	}

	/**
	 * @param cLFAX le cLFAX � d�finir
	 */
	public void setCLFAX(String cLFAX)
	{
		CLFAX = cLFAX;
	}

	/**
	 * @return le cLTLX
	 */
	public String getCLTLX()
	{
		return CLTLX;
	}

	/**
	 * @param cLTLX le cLTLX � d�finir
	 */
	public void setCLTLX(String cLTLX)
	{
		CLTLX = cLTLX;
	}

	/**
	 * @return le cLLAN
	 */
	public String getCLLAN()
	{
		return CLLAN;
	}

	/**
	 * @param cLLAN le cLLAN � d�finir
	 */
	public void setCLLAN(String cLLAN)
	{
		CLLAN = cLLAN;
	}

	/**
	 * @return le cLOBS
	 */
	public String getCLOBS()
	{
		return CLOBS;
	}

	/**
	 * @param cLOBS le cLOBS � d�finir
	 */
	public void setCLOBS(String cLOBS)
	{
		CLOBS = cLOBS;
	}

	/**
	 * @return le cLREP
	 */
	public String getCLREP()
	{
		return CLREP;
	}

	/**
	 * @param cLREP le cLREP � d�finir
	 */
	public void setCLREP(String cLREP)
	{
		CLREP = cLREP;
	}

	/**
	 * @return le cLREP2
	 */
	public String getCLREP2()
	{
		return CLREP2;
	}

	/**
	 * @param cLREP2 le cLREP2 � d�finir
	 */
	public void setCLREP2(String cLREP2)
	{
		CLREP2 = cLREP2;
	}

	/**
	 * @return le cLTRP
	 */
	public char getCLTRP()
	{
		return CLTRP;
	}

	/**
	 * @param cLTRP le cLTRP � d�finir
	 */
	public void setCLTRP(char cLTRP)
	{
		CLTRP = cLTRP;
	}

	/**
	 * @return le cLGEO
	 */
	public String getCLGEO()
	{
		return CLGEO;
	}

	/**
	 * @param cLGEO le cLGEO � d�finir
	 */
	public void setCLGEO(String cLGEO)
	{
		CLGEO = cLGEO;
	}

	/**
	 * @return le cLMEX
	 */
	public String getCLMEX()
	{
		return CLMEX;
	}

	/**
	 * @param cLMEX le cLMEX � d�finir
	 */
	public void setCLMEX(String cLMEX)
	{
		CLMEX = cLMEX;
	}

	/**
	 * @return le cLCTR
	 */
	public String getCLCTR()
	{
		return CLCTR;
	}

	/**
	 * @param cLCTR le cLCTR � d�finir
	 */
	public void setCLCTR(String cLCTR)
	{
		CLCTR = cLCTR;
	}

	/**
	 * @return le cLMAG
	 */
	public String getCLMAG()
	{
		return CLMAG;
	}

	/**
	 * @param cLMAG le cLMAG � d�finir
	 */
	public void setCLMAG(String cLMAG)
	{
		CLMAG = cLMAG;
	}

	/**
	 * @return le cLFRP
	 */
	public int getCLFRP()
	{
		return CLFRP;
	}

	/**
	 * @param cLFRP le cLFRP � d�finir
	 */
	public void setCLFRP(int cLFRP)
	{
		CLFRP = cLFRP;
	}

	/**
	 * @return le cLTFA
	 */
	public char getCLTFA()
	{
		return CLTFA;
	}

	/**
	 * @param cLTFA le cLTFA � d�finir
	 */
	public void setCLTFA(char cLTFA)
	{
		CLTFA = cLTFA;
	}

	/**
	 * @return le cLTTC
	 */
	public char getCLTTC()
	{
		return CLTTC;
	}

	/**
	 * @param cLTTC le cLTTC � d�finir
	 */
	public void setCLTTC(char cLTTC)
	{
		CLTTC = cLTTC;
	}

	/**
	 * @return le cLDEV
	 */
	public String getCLDEV()
	{
		return CLDEV;
	}

	/**
	 * @param cLDEV le cLDEV � d�finir
	 */
	public void setCLDEV(String cLDEV)
	{
		CLDEV = cLDEV;
	}

	/**
	 * @return le cLCLFP
	 */
	public int getCLCLFP()
	{
		return CLCLFP;
	}

	/**
	 * @param cLCLFP le cLCLFP � d�finir
	 */
	public void setCLCLFP(int cLCLFP)
	{
		CLCLFP = cLCLFP;
	}

	/**
	 * @return le cLCLFS
	 */
	public int getCLCLFS()
	{
		return CLCLFS;
	}

	/**
	 * @param cLCLFS le cLCLFS � d�finir
	 */
	public void setCLCLFS(int cLCLFS)
	{
		CLCLFS = cLCLFS;
	}

	/**
	 * @return le cLCLP
	 */
	public int getCLCLP()
	{
		return CLCLP;
	}

	/**
	 * @param cLCLP le cLCLP � d�finir
	 */
	public void setCLCLP(int cLCLP)
	{
		CLCLP = cLCLP;
	}

	/**
	 * @return le cLAFA
	 */
	public String getCLAFA()
	{
		return CLAFA;
	}

	/**
	 * @param cLAFA le cLAFA � d�finir
	 */
	public void setCLAFA(String cLAFA)
	{
		CLAFA = cLAFA;
	}

	/**
	 * @return le cLESC
	 */
	public int getCLESC()
	{
		return CLESC;
	}

	/**
	 * @param cLESC le cLESC � d�finir
	 */
	public void setCLESC(int cLESC)
	{
		CLESC = cLESC;
	}

	/**
	 * @return le cLTAR
	 */
	public int getCLTAR()
	{
		return CLTAR;
	}

	/**
	 * @param cLTAR le cLTAR � d�finir
	 */
	public void setCLTAR(int cLTAR)
	{
		CLTAR = cLTAR;
	}

	/**
	 * @return le cLTA1
	 */
	public int getCLTA1()
	{
		return CLTA1;
	}

	/**
	 * @param cLTA1 le cLTA1 � d�finir
	 */
	public void setCLTA1(int cLTA1)
	{
		CLTA1 = cLTA1;
	}

	/**
	 * @return le cLREM1
	 */
	public int getCLREM1()
	{
		return CLREM1;
	}

	/**
	 * @param cLREM1 le cLREM1 � d�finir
	 */
	public void setCLREM1(int cLREM1)
	{
		CLREM1 = cLREM1;
	}

	/**
	 * @return le cLREM2
	 */
	public int getCLREM2()
	{
		return CLREM2;
	}

	/**
	 * @param cLREM2 le cLREM2 � d�finir
	 */
	public void setCLREM2(int cLREM2)
	{
		CLREM2 = cLREM2;
	}

	/**
	 * @return le cLREM3
	 */
	public int getCLREM3()
	{
		return CLREM3;
	}

	/**
	 * @param cLREM3 le cLREM3 � d�finir
	 */
	public void setCLREM3(int cLREM3)
	{
		CLREM3 = cLREM3;
	}

	/**
	 * @return le cLRP1
	 */
	public int getCLRP1()
	{
		return CLRP1;
	}

	/**
	 * @param cLRP1 le cLRP1 � d�finir
	 */
	public void setCLRP1(int cLRP1)
	{
		CLRP1 = cLRP1;
	}

	/**
	 * @return le cLRP2
	 */
	public int getCLRP2()
	{
		return CLRP2;
	}

	/**
	 * @param cLRP2 le cLRP2 � d�finir
	 */
	public void setCLRP2(int cLRP2)
	{
		CLRP2 = cLRP2;
	}

	/**
	 * @return le cLRP3
	 */
	public int getCLRP3()
	{
		return CLRP3;
	}

	/**
	 * @param cLRP3 le cLRP3 � d�finir
	 */
	public void setCLRP3(int cLRP3)
	{
		CLRP3 = cLRP3;
	}

	/**
	 * @return le cLCNV
	 */
	public String getCLCNV()
	{
		return CLCNV;
	}

	/**
	 * @param cLCNV le cLCNV � d�finir
	 */
	public void setCLCNV(String cLCNV)
	{
		CLCNV = cLCNV;
	}

	/**
	 * @return le cLPFA
	 */
	public char getCLPFA()
	{
		return CLPFA;
	}

	/**
	 * @param cLPFA le cLPFA � d�finir
	 */
	public void setCLPFA(char cLPFA)
	{
		CLPFA = cLPFA;
	}

	/**
	 * @return le cLPRL
	 */
	public char getCLPRL()
	{
		return CLPRL;
	}

	/**
	 * @param cLPRL le cLPRL � d�finir
	 */
	public void setCLPRL(char cLPRL)
	{
		CLPRL = cLPRL;
	}

	/**
	 * @return le cLRLV
	 */
	public char getCLRLV()
	{
		return CLRLV;
	}

	/**
	 * @param cLRLV le cLRLV � d�finir
	 */
	public void setCLRLV(char cLRLV)
	{
		CLRLV = cLRLV;
	}

	/**
	 * @return le cLRBF
	 */
	public int getCLRBF()
	{
		return CLRBF;
	}

	/**
	 * @param cLRBF le cLRBF � d�finir
	 */
	public void setCLRBF(int cLRBF)
	{
		CLRBF = cLRBF;
	}

	/**
	 * @return le cLRGL
	 */
	public String getCLRGL()
	{
		return CLRGL;
	}

	/**
	 * @param cLRGL le cLRGL � d�finir
	 */
	public void setCLRGL(String cLRGL)
	{
		CLRGL = cLRGL;
	}

	/**
	 * @return le cLECH
	 */
	public String getCLECH()
	{
		return CLECH;
	}

	/**
	 * @param cLECH le cLECH � d�finir
	 */
	public void setCLECH(String cLECH)
	{
		CLECH = cLECH;
	}

	/**
	 * @return le cLNCG
	 */
	public int getCLNCG()
	{
		return CLNCG;
	}

	/**
	 * @param cLNCG le cLNCG � d�finir
	 */
	public void setCLNCG(int cLNCG)
	{
		CLNCG = cLNCG;
	}

	/**
	 * @return le cLNCA
	 */
	public int getCLNCA()
	{
		return CLNCA;
	}

	/**
	 * @param cLNCA le cLNCA � d�finir
	 */
	public void setCLNCA(int cLNCA)
	{
		CLNCA = cLNCA;
	}

	/**
	 * @return le cLACT
	 */
	public String getCLACT()
	{
		return CLACT;
	}

	/**
	 * @param cLACT le cLACT � d�finir
	 */
	public void setCLACT(String cLACT)
	{
		CLACT = cLACT;
	}

	/**
	 * @return le cLPLF
	 */
	public int getCLPLF()
	{
		return CLPLF;
	}

	/**
	 * @param cLPLF le cLPLF � d�finir
	 */
	public void setCLPLF(int cLPLF)
	{
		CLPLF = cLPLF;
	}

	/**
	 * @return le cLPLF2
	 */
	public int getCLPLF2()
	{
		return CLPLF2;
	}

	/**
	 * @param cLPLF2 le cLPLF2 � d�finir
	 */
	public void setCLPLF2(int cLPLF2)
	{
		CLPLF2 = cLPLF2;
	}

	/**
	 * @return le cLTNS
	 */
	public int getCLTNS()
	{
		return CLTNS;
	}

	/**
	 * @param cLTNS le cLTNS � d�finir
	 */
	public void setCLTNS(int cLTNS)
	{
		CLTNS = cLTNS;
	}

	/**
	 * @return le cLATT
	 */
	public String getCLATT()
	{
		return CLATT;
	}

	/**
	 * @param cLATT le cLATT � d�finir
	 */
	public void setCLATT(String cLATT)
	{
		CLATT = cLATT;
	}

	/**
	 * @return le cLNOT
	 */
	public String getCLNOT()
	{
		return CLNOT;
	}

	/**
	 * @param cLNOT le cLNOT � d�finir
	 */
	public void setCLNOT(String cLNOT)
	{
		CLNOT = cLNOT;
	}

	/**
	 * @return le cLDDV
	 */
	public int getCLDDV()
	{
		return CLDDV;
	}

	/**
	 * @param cLDDV le cLDDV � d�finir
	 */
	public void setCLDDV(int cLDDV)
	{
		CLDDV = cLDDV;
	}

	/**
	 * @return le cLDVE
	 */
	public int getCLDVE()
	{
		return CLDVE;
	}

	/**
	 * @param cLDVE le cLDVE � d�finir
	 */
	public void setCLDVE(int cLDVE)
	{
		CLDVE = cLDVE;
	}

	/**
	 * @return le cLSRN
	 */
	public String getCLSRN()
	{
		return CLSRN;
	}

	/**
	 * @param cLSRN le cLSRN � d�finir
	 */
	public void setCLSRN(String cLSRN)
	{
		CLSRN = cLSRN;
	}

	/**
	 * @return le cLSRT
	 */
	public String getCLSRT()
	{
		return CLSRT;
	}

	/**
	 * @param cLSRT le cLSRT � d�finir
	 */
	public void setCLSRT(String cLSRT)
	{
		CLSRT = cLSRT;
	}

	/**
	 * @return le cLFIL1
	 */
	public char getCLFIL1()
	{
		return CLFIL1;
	}

	/**
	 * @param cLFIL1 le cLFIL1 � d�finir
	 */
	public void setCLFIL1(char cLFIL1)
	{
		CLFIL1 = cLFIL1;
	}

	/**
	 * @return le cLCEE
	 */
	public String getCLCEE()
	{
		return CLCEE;
	}

	/**
	 * @param cLCEE le cLCEE � d�finir
	 */
	public void setCLCEE(String cLCEE)
	{
		CLCEE = cLCEE;
	}

	/**
	 * @return le cLVAE
	 */
	public int getCLVAE()
	{
		return CLVAE;
	}

	/**
	 * @param cLVAE le cLVAE � d�finir
	 */
	public void setCLVAE(int cLVAE)
	{
		CLVAE = cLVAE;
	}

	/**
	 * @return le cLCDE
	 */
	public int getCLCDE()
	{
		return CLCDE;
	}

	/**
	 * @param cLCDE le cLCDE � d�finir
	 */
	public void setCLCDE(int cLCDE)
	{
		CLCDE = cLCDE;
	}

	/**
	 * @return le cLEXP
	 */
	public int getCLEXP()
	{
		return CLEXP;
	}

	/**
	 * @param cLEXP le cLEXP � d�finir
	 */
	public void setCLEXP(int cLEXP)
	{
		CLEXP = cLEXP;
	}

	/**
	 * @return le cLFAC
	 */
	public int getCLFAC()
	{
		return CLFAC;
	}

	/**
	 * @param cLFAC le cLFAC � d�finir
	 */
	public void setCLFAC(int cLFAC)
	{
		CLFAC = cLFAC;
	}

	/**
	 * @return le cLPCO
	 */
	public int getCLPCO()
	{
		return CLPCO;
	}

	/**
	 * @param cLPCO le cLPCO � d�finir
	 */
	public void setCLPCO(int cLPCO)
	{
		CLPCO = cLPCO;
	}

	/**
	 * @return le cLCGM
	 */
	public int getCLCGM()
	{
		return CLCGM;
	}

	/**
	 * @param cLCGM le cLCGM � d�finir
	 */
	public void setCLCGM(int cLCGM)
	{
		CLCGM = cLCGM;
	}

	/**
	 * @return le cLMAJ
	 */
	public int getCLMAJ()
	{
		return CLMAJ;
	}

	/**
	 * @param cLMAJ le cLMAJ � d�finir
	 */
	public void setCLMAJ(int cLMAJ)
	{
		CLMAJ = cLMAJ;
	}

	/**
	 * @return le cLNEX1
	 */
	public int getCLNEX1()
	{
		return CLNEX1;
	}

	/**
	 * @param cLNEX1 le cLNEX1 � d�finir
	 */
	public void setCLNEX1(int cLNEX1)
	{
		CLNEX1 = cLNEX1;
	}

	/**
	 * @return le cLNEX2
	 */
	public int getCLNEX2()
	{
		return CLNEX2;
	}

	/**
	 * @param cLNEX2 le cLNEX2 � d�finir
	 */
	public void setCLNEX2(int cLNEX2)
	{
		CLNEX2 = cLNEX2;
	}

	/**
	 * @return le cLNEX3
	 */
	public int getCLNEX3()
	{
		return CLNEX3;
	}

	/**
	 * @param cLNEX3 le cLNEX3 � d�finir
	 */
	public void setCLNEX3(int cLNEX3)
	{
		CLNEX3 = cLNEX3;
	}

	/**
	 * @return le cLNLA
	 */
	public String getCLNLA()
	{
		return CLNLA;
	}

	/**
	 * @param cLNLA le cLNLA � d�finir
	 */
	public void setCLNLA(String cLNLA)
	{
		CLNLA = cLNLA;
	}

	/**
	 * @return le cLRST
	 */
	public String getCLRST()
	{
		return CLRST;
	}

	/**
	 * @param cLRST le cLRST � d�finir
	 */
	public void setCLRST(String cLRST)
	{
		CLRST = cLRST;
	}

	/**
	 * @return le cLIN1
	 */
	public char getCLIN1()
	{
		return CLIN1;
	}

	/**
	 * @param cLIN1 le cLIN1 � d�finir
	 */
	public void setCLIN1(char cLIN1)
	{
		CLIN1 = cLIN1;
	}

	/**
	 * @return le cLIN2
	 */
	public char getCLIN2()
	{
		return CLIN2;
	}

	/**
	 * @param cLIN2 le cLIN2 � d�finir
	 */
	public void setCLIN2(char cLIN2)
	{
		CLIN2 = cLIN2;
	}

	/**
	 * @return le cLIN3
	 */
	public char getCLIN3()
	{
		return CLIN3;
	}

	/**
	 * @param cLIN3 le cLIN3 � d�finir
	 */
	public void setCLIN3(char cLIN3)
	{
		CLIN3 = cLIN3;
	}

	/**
	 * @return le cLIN4
	 */
	public char getCLIN4()
	{
		return CLIN4;
	}

	/**
	 * @param cLIN4 le cLIN4 � d�finir
	 */
	public void setCLIN4(char cLIN4)
	{
		CLIN4 = cLIN4;
	}

	/**
	 * @return le cLIN5
	 */
	public char getCLIN5()
	{
		return CLIN5;
	}

	/**
	 * @param cLIN5 le cLIN5 � d�finir
	 */
	public void setCLIN5(char cLIN5)
	{
		CLIN5 = cLIN5;
	}

	/**
	 * @return le cLIN6
	 */
	public char getCLIN6()
	{
		return CLIN6;
	}

	/**
	 * @param cLIN6 le cLIN6 � d�finir
	 */
	public void setCLIN6(char cLIN6)
	{
		CLIN6 = cLIN6;
	}

	/**
	 * @return le cLIN7
	 */
	public char getCLIN7()
	{
		return CLIN7;
	}

	/**
	 * @param cLIN7 le cLIN7 � d�finir
	 */
	public void setCLIN7(char cLIN7)
	{
		CLIN7 = cLIN7;
	}

	/**
	 * @return le cLIN8
	 */
	public char getCLIN8()
	{
		return CLIN8;
	}

	/**
	 * @param cLIN8 le cLIN8 � d�finir
	 */
	public void setCLIN8(char cLIN8)
	{
		CLIN8 = cLIN8;
	}

	/**
	 * @return le cLIN9
	 */
	public char getCLIN9()
	{
		return CLIN9;
	}

	/**
	 * @param cLIN9 le cLIN9 � d�finir
	 */
	public void setCLIN9(char cLIN9)
	{
		CLIN9 = cLIN9;
	}

	/**
	 * @return le cLIN10
	 */
	public char getCLIN10()
	{
		return CLIN10;
	}

	/**
	 * @param cLIN10 le cLIN10 � d�finir
	 */
	public void setCLIN10(char cLIN10)
	{
		CLIN10 = cLIN10;
	}

	/**
	 * @return le cLIN11
	 */
	public char getCLIN11()
	{
		return CLIN11;
	}

	/**
	 * @param cLIN11 le cLIN11 � d�finir
	 */
	public void setCLIN11(char cLIN11)
	{
		CLIN11 = cLIN11;
	}

	/**
	 * @return le cLIN12
	 */
	public char getCLIN12()
	{
		return CLIN12;
	}

	/**
	 * @param cLIN12 le cLIN12 � d�finir
	 */
	public void setCLIN12(char cLIN12)
	{
		CLIN12 = cLIN12;
	}

	/**
	 * @return le cLIN13
	 */
	public char getCLIN13()
	{
		return CLIN13;
	}

	/**
	 * @param cLIN13 le cLIN13 � d�finir
	 */
	public void setCLIN13(char cLIN13)
	{
		CLIN13 = cLIN13;
	}

	/**
	 * @return le cLIN14
	 */
	public char getCLIN14()
	{
		return CLIN14;
	}

	/**
	 * @param cLIN14 le cLIN14 � d�finir
	 */
	public void setCLIN14(char cLIN14)
	{
		CLIN14 = cLIN14;
	}

	/**
	 * @return le cLIN15
	 */
	public char getCLIN15()
	{
		return CLIN15;
	}

	/**
	 * @param cLIN15 le cLIN15 � d�finir
	 */
	public void setCLIN15(char cLIN15)
	{
		CLIN15 = cLIN15;
	}

	/**
	 * @return le cLADH
	 */
	public String getCLADH()
	{
		return CLADH;
	}

	/**
	 * @param cLADH le cLADH � d�finir
	 */
	public void setCLADH(String cLADH)
	{
		CLADH = cLADH;
	}

	/**
	 * @return le cLCNC
	 */
	public String getCLCNC()
	{
		return CLCNC;
	}

	/**
	 * @param cLCNC le cLCNC � d�finir
	 */
	public void setCLCNC(String cLCNC)
	{
		CLCNC = cLCNC;
	}

	/**
	 * @return le cLCC1
	 */
	public int getCLCC1()
	{
		return CLCC1;
	}

	/**
	 * @param cLCC1 le cLCC1 � d�finir
	 */
	public void setCLCC1(int cLCC1)
	{
		CLCC1 = cLCC1;
	}

	/**
	 * @return le cLCC2
	 */
	public int getCLCC2()
	{
		return CLCC2;
	}

	/**
	 * @param cLCC2 le cLCC2 � d�finir
	 */
	public void setCLCC2(int cLCC2)
	{
		CLCC2 = cLCC2;
	}

	/**
	 * @return le cLCC3
	 */
	public int getCLCC3()
	{
		return CLCC3;
	}

	/**
	 * @param cLCC3 le cLCC3 � d�finir
	 */
	public void setCLCC3(int cLCC3)
	{
		CLCC3 = cLCC3;
	}

	/**
	 * @return le cLFIR
	 */
	public String getCLFIR()
	{
		return CLFIR;
	}

	/**
	 * @param cLFIR le cLFIR � d�finir
	 */
	public void setCLFIR(String cLFIR)
	{
		CLFIR = cLFIR;
	}

	/**
	 * @return le cLTRA
	 */
	public int getCLTRA()
	{
		return CLTRA;
	}

	/**
	 * @param cLTRA le cLTRA � d�finir
	 */
	public void setCLTRA(int cLTRA)
	{
		CLTRA = cLTRA;
	}

	/**
	 * @return le cLCNR
	 */
	public String getCLCNR()
	{
		return CLCNR;
	}

	/**
	 * @param cLCNR le cLCNR � d�finir
	 */
	public void setCLCNR(String cLCNR)
	{
		CLCNR = cLCNR;
	}

	/**
	 * @return le cLCNP
	 */
	public String getCLCNP()
	{
		return CLCNP;
	}

	/**
	 * @param cLCNP le cLCNP � d�finir
	 */
	public void setCLCNP(String cLCNP)
	{
		CLCNP = cLCNP;
	}

	/**
	 * @return le cLCNB
	 */
	public String getCLCNB()
	{
		return CLCNB;
	}

	/**
	 * @param cLCNB le cLCNB � d�finir
	 */
	public void setCLCNB(String cLCNB)
	{
		CLCNB = cLCNB;
	}

	/**
	 * @return le cLDPL
	 */
	public int getCLDPL()
	{
		return CLDPL;
	}

	/**
	 * @param cLDPL le cLDPL � d�finir
	 */
	public void setCLDPL(int cLDPL)
	{
		CLDPL = cLDPL;
	}

	/**
	 * @return le cLIN16
	 */
	public char getCLIN16()
	{
		return CLIN16;
	}

	/**
	 * @param cLIN16 le cLIN16 � d�finir
	 */
	public void setCLIN16(char cLIN16)
	{
		CLIN16 = cLIN16;
	}

	/**
	 * @return le cLIN17
	 */
	public char getCLIN17()
	{
		return CLIN17;
	}

	/**
	 * @param cLIN17 le cLIN17 � d�finir
	 */
	public void setCLIN17(char cLIN17)
	{
		CLIN17 = cLIN17;
	}

	/**
	 * @return le cLCRT
	 */
	public String getCLCRT()
	{
		return CLCRT;
	}

	/**
	 * @param cLCRT le cLCRT � d�finir
	 */
	public void setCLCRT(String cLCRT)
	{
		CLCRT = cLCRT;
	}

	/**
	 * @return le cLOTO
	 */
	public int getCLOTO()
	{
		return CLOTO;
	}

	/**
	 * @param cLOTO le cLOTO � d�finir
	 */
	public void setCLOTO(int cLOTO)
	{
		CLOTO = cLOTO;
	}

	/**
	 * @return le cLCAN
	 */
	public String getCLCAN()
	{
		return CLCAN;
	}

	/**
	 * @param cLCAN le cLCAN � d�finir
	 */
	public void setCLCAN(String cLCAN)
	{
		CLCAN = cLCAN;
	}

	/**
	 * @return le cLPRN
	 */
	public String getCLPRN()
	{
		return CLPRN;
	}

	/**
	 * @param cLPRN le cLPRN � d�finir
	 */
	public void setCLPRN(String cLPRN)
	{
		CLPRN = cLPRN;
	}

	/**
	 * @return le cLREC
	 */
	public char getCLREC()
	{
		return CLREC;
	}

	/**
	 * @param cLREC le cLREC � d�finir
	 */
	public void setCLREC(char cLREC)
	{
		CLREC = cLREC;
	}

	/**
	 * @return le cLFRC
	 */
	public char getCLFRC()
	{
		return CLFRC;
	}

	/**
	 * @param cLFRC le cLFRC � d�finir
	 */
	public void setCLFRC(char cLFRC)
	{
		CLFRC = cLFRC;
	}

	/**
	 * @return le cLMTC
	 */
	public char getCLMTC()
	{
		return CLMTC;
	}

	/**
	 * @param cLMTC le cLMTC � d�finir
	 */
	public void setCLMTC(char cLMTC)
	{
		CLMTC = cLMTC;
	}

	/**
	 * @return le cLNIP
	 */
	public String getCLNIP()
	{
		return CLNIP;
	}

	/**
	 * @param cLNIP le cLNIP � d�finir
	 */
	public void setCLNIP(String cLNIP)
	{
		CLNIP = cLNIP;
	}

	/**
	 * @return le cLNIK
	 */
	public String getCLNIK()
	{
		return CLNIK;
	}

	/**
	 * @param cLNIK le cLNIK � d�finir
	 */
	public void setCLNIK(String cLNIK)
	{
		CLNIK = cLNIK;
	}

	/**
	 * @return le cLCL1
	 */
	public char getCLCL1()
	{
		return CLCL1;
	}

	/**
	 * @param cLCL1 le cLCL1 � d�finir
	 */
	public void setCLCL1(char cLCL1)
	{
		CLCL1 = cLCL1;
	}

	/**
	 * @return le cLCL2
	 */
	public String getCLCL2()
	{
		return CLCL2;
	}

	/**
	 * @param cLCL2 le cLCL2 � d�finir
	 */
	public void setCLCL2(String cLCL2)
	{
		CLCL2 = cLCL2;
	}

	/**
	 * @return le cLCL3
	 */
	public String getCLCL3()
	{
		return CLCL3;
	}

	/**
	 * @param cLCL3 le cLCL3 � d�finir
	 */
	public void setCLCL3(String cLCL3)
	{
		CLCL3 = cLCL3;
	}

	/**
	 * @return le cLCLA
	 */
	public String getCLCLA()
	{
		return CLCLA;
	}

	/**
	 * @param cLCLA le cLCLA � d�finir
	 */
	public void setCLCLA(String cLCLA)
	{
		CLCLA = cLCLA;
	}

	/**
	 * @return le cLIN18
	 */
	public char getCLIN18()
	{
		return CLIN18;
	}

	/**
	 * @param cLIN18 le cLIN18 � d�finir
	 */
	public void setCLIN18(char cLIN18)
	{
		CLIN18 = cLIN18;
	}

	/**
	 * @return le cLIN19
	 */
	public char getCLIN19()
	{
		return CLIN19;
	}

	/**
	 * @param cLIN19 le cLIN19 � d�finir
	 */
	public void setCLIN19(char cLIN19)
	{
		CLIN19 = cLIN19;
	}

	/**
	 * @return le cLIN20
	 */
	public char getCLIN20()
	{
		return CLIN20;
	}

	/**
	 * @param cLIN20 le cLIN20 � d�finir
	 */
	public void setCLIN20(char cLIN20)
	{
		CLIN20 = cLIN20;
	}

	/**
	 * @return le cLIN21
	 */
	public char getCLIN21()
	{
		return CLIN21;
	}

	/**
	 * @param cLIN21 le cLIN21 � d�finir
	 */
	public void setCLIN21(char cLIN21)
	{
		CLIN21 = cLIN21;
	}

	/**
	 * @return le cLIN22
	 */
	public char getCLIN22()
	{
		return CLIN22;
	}

	/**
	 * @param cLIN22 le cLIN22 � d�finir
	 */
	public void setCLIN22(char cLIN22)
	{
		CLIN22 = cLIN22;
	}

	/**
	 * @return le cLIN23
	 */
	public char getCLIN23()
	{
		return CLIN23;
	}

	/**
	 * @param cLIN23 le cLIN23 � d�finir
	 */
	public void setCLIN23(char cLIN23)
	{
		CLIN23 = cLIN23;
	}

	/**
	 * @return le cLIN24
	 */
	public char getCLIN24()
	{
		return CLIN24;
	}

	/**
	 * @param cLIN24 le cLIN24 � d�finir
	 */
	public void setCLIN24(char cLIN24)
	{
		CLIN24 = cLIN24;
	}

	/**
	 * @return le cLIN25
	 */
	public char getCLIN25()
	{
		return CLIN25;
	}

	/**
	 * @param cLIN25 le cLIN25 � d�finir
	 */
	public void setCLIN25(char cLIN25)
	{
		CLIN25 = cLIN25;
	}

	/**
	 * @return le cLIN26
	 */
	public char getCLIN26()
	{
		return CLIN26;
	}

	/**
	 * @param cLIN26 le cLIN26 � d�finir
	 */
	public void setCLIN26(char cLIN26)
	{
		CLIN26 = cLIN26;
	}

	/**
	 * @return le cLIN27
	 */
	public char getCLIN27()
	{
		return CLIN27;
	}

	/**
	 * @param cLIN27 le cLIN27 � d�finir
	 */
	public void setCLIN27(char cLIN27)
	{
		CLIN27 = cLIN27;
	}

	/**
	 * @return le cLAPEN
	 */
	public String getCLAPEN()
	{
		return CLAPEN;
	}

	/**
	 * @param cLAPEN le cLAPEN � d�finir
	 */
	public void setCLAPEN(String cLAPEN)
	{
		CLAPEN = cLAPEN;
	}

	/**
	 * @return le cLPLF3
	 */
	public int getCLPLF3()
	{
		return CLPLF3;
	}

	/**
	 * @param cLPLF3 le cLPLF3 � d�finir
	 */
	public void setCLPLF3(int cLPLF3)
	{
		CLPLF3 = cLPLF3;
	}

	/**
	 * @return le cLDPL3
	 */
	public int getCLDPL3()
	{
		return CLDPL3;
	}

	/**
	 * @param cLDPL3 le cLDPL3 � d�finir
	 */
	public void setCLDPL3(int cLDPL3)
	{
		CLDPL3 = cLDPL3;
	}

	/**
	 * @return le cLIDAS
	 */
	public String getCLIDAS()
	{
		return CLIDAS;
	}

	/**
	 * @param cLIDAS le cLIDAS � d�finir
	 */
	public void setCLIDAS(String cLIDAS)
	{
		CLIDAS = cLIDAS;
	}

	/**
	 * @return le cLCAS
	 */
	public String getCLCAS()
	{
		return CLCAS;
	}

	/**
	 * @param cLCAS le cLCAS � d�finir
	 */
	public void setCLCAS(String cLCAS)
	{
		CLCAS = cLCAS;
	}

	/**
	 * @return le cLDVP3
	 */
	public int getCLDVP3()
	{
		return CLDVP3;
	}

	/**
	 * @param cLDVP3 le cLDVP3 � d�finir
	 */
	public void setCLDVP3(int cLDVP3)
	{
		CLDVP3 = cLDVP3;
	}

	/**
	 * @return le cLDAT1
	 */
	public int getCLDAT1()
	{
		return CLDAT1;
	}

	/**
	 * @param cLDAT1 le cLDAT1 � d�finir
	 */
	public void setCLDAT1(int cLDAT1)
	{
		CLDAT1 = cLDAT1;
	}

	/**
	 * @return le cLDAT2
	 */
	public int getCLDAT2()
	{
		return CLDAT2;
	}

	/**
	 * @param cLDAT2 le cLDAT2 � d�finir
	 */
	public void setCLDAT2(int cLDAT2)
	{
		CLDAT2 = cLDAT2;
	}

	/**
	 * @return le cLDAT3
	 */
	public int getCLDAT3()
	{
		return CLDAT3;
	}

	/**
	 * @param cLDAT3 le cLDAT3 � d�finir
	 */
	public void setCLDAT3(int cLDAT3)
	{
		CLDAT3 = cLDAT3;
	}

	/**
	 * @return le cLFFP1
	 */
	public int getCLFFP1()
	{
		return CLFFP1;
	}

	/**
	 * @param cLFFP1 le cLFFP1 � d�finir
	 */
	public void setCLFFP1(int cLFFP1)
	{
		CLFFP1 = cLFFP1;
	}

	/**
	 * @return le cLFFP2
	 */
	public int getCLFFP2()
	{
		return CLFFP2;
	}

	/**
	 * @param cLFFP2 le cLFFP2 � d�finir
	 */
	public void setCLFFP2(int cLFFP2)
	{
		CLFFP2 = cLFFP2;
	}

	/**
	 * @return le cLFFP3
	 */
	public int getCLFFP3()
	{
		return CLFFP3;
	}

	/**
	 * @param cLFFP3 le cLFFP3 � d�finir
	 */
	public void setCLFFP3(int cLFFP3)
	{
		CLFFP3 = cLFFP3;
	}

	/**
	 * @return le cLIN28
	 */
	public char getCLIN28()
	{
		return CLIN28;
	}

	/**
	 * @param cLIN28 le cLIN28 � d�finir
	 */
	public void setCLIN28(char cLIN28)
	{
		CLIN28 = cLIN28;
	}

	/**
	 * @return le cLIN29
	 */
	public char getCLIN29()
	{
		return CLIN29;
	}

	/**
	 * @param cLIN29 le cLIN29 � d�finir
	 */
	public void setCLIN29(char cLIN29)
	{
		CLIN29 = cLIN29;
	}

	/**
	 * @return le cLIN30
	 */
	public char getCLIN30()
	{
		return CLIN30;
	}

	/**
	 * @param cLIN30 le cLIN30 � d�finir
	 */
	public void setCLIN30(char cLIN30)
	{
		CLIN30 = cLIN30;
	}

}
