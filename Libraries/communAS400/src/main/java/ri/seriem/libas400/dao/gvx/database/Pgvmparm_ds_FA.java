//=================================================================================================
//==>                                                                       06/06/2013 - 06/06/2013
//==> Description de l'enregistrement du fichier Pgvmparm_ds_FA pour les FA
//=================================================================================================
package ri.seriem.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.seriem.libas400.database.record.DataStructureRecord;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.PackedDecimalFieldDescription;

public class Pgvmparm_ds_FA extends DataStructureRecord
{

	/**
	 * Cr�ation de la data structure pour l'enregistrement du fichier
	 */
	public void initRecord()
	{
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "FALIB"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "FATVA"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "FATPF"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "FASPE"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FAUNV"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "FAC")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "FANSA"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "FACMAG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FAUNS"));
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(15, 0), "FAKV1G"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "FARTA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "FASAN"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "FARON"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "FANAT"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "FAA")); // A contr�ler car � l'origine c'est une zone packed
		rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 4), "FACPR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAIMG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAEFR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAGSP"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "FACTA"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "FAFAR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAPDG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "FADPS"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FAMAR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FACIP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAVOD"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FASV1"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FASV2"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FASVL"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "FAPR"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FACQQ"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FACQN"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "FACG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FADEB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FATEX"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAWEB"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAPHO"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "FAJIT"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "FAPIC"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FATDUO"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FADUO1"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FADUO2"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FADUO3"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAETQ"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FATEM"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FANV1"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FANV2"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FANV3"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FANV4"));
		rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "FAPER"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAGP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FAMAG"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FADPOS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAEQI"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FALOTP"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FALOTH"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FALOTE"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FALOTF"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FAADS"));
		rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FATYGR"));

		length = 300;
	}
}
