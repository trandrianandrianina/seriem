//=================================================================================================
//==>                                                                       21/12/2011 - 17/05/2016
//==> Description d'un enregistrement g�n�rique (utile pour les requ�tes avec jointure) 
//=================================================================================================
package ri.seriem.libas400.database.record;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.ibm.as400.access.AS400JDBCClob;
import com.ibm.as400.access.AS400JDBCClobLocator;
import com.ibm.as400.access.AS400Text;
import com.ibm.db2.jdbc.app.DB2Clob;
import com.ibm.db2.jdbc.app.DB2ClobLocator;

import ri.seriem.libas400.database.field.Field;
import ri.seriem.libas400.database.field.FieldAlpha;
import ri.seriem.libas400.database.field.FieldDecimal;
import ri.seriem.libas400.database.field.FieldTimestamp;

public class GenericRecord implements InterfaceRecord
{
	// Constantes
	private static final String TYPE_PARTICULIER="##??##";	// Pour marquer les valeurs des types du genre BLOB, CLOB
	// Variables
	private final LinkedHashMap<String, Field<?>> listFields=new LinkedHashMap<String, Field<?>>();
	private StringBuffer buffer = null;
	private int length=0;
	private String[] requiredField=null;
	private String[] omittedField=null;
	private int nbrOmitField = 0;
	private JsonParser parser = new JsonParser();
	
	protected String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu 


	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 */
	public void setField(String namefield, Object value, int size, int decimal)
	{
//System.out.println(value.getClass());		
		if(value instanceof BigDecimal)
			setField(namefield, (BigDecimal)value, size, decimal);
		else
			if(value instanceof Integer)
				setField(namefield, String.valueOf(value), size);
			else
				if(value instanceof byte[]) // Cas de lecture des fichiers plats
				{
					AS400Text champs = new AS400Text(size);
					setField(namefield, (String)champs.toObject((byte[])value), size);
				}
				else
					if( value instanceof Timestamp ){
						setField(namefield, (Timestamp)value);
					}
					else
					if(value instanceof AS400JDBCClobLocator) // C'est un CLOB
					{
						try
						{
							char[] contenu = new char[(int) ((AS400JDBCClobLocator)value).length()];
							((AS400JDBCClobLocator)value).getCharacterStream().read(contenu);
							//byte[] tab = new byte[t];
							//((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
							setField(namefield, new String(contenu), true);
						}
						catch (Exception e)
						{
							msgErreur += "Erreur sur AS400JDBCClobLocator: " + e;
						}
					}
					else
						if(value instanceof AS400JDBCClob) // C'est un CLOB
						{
							try
							{
								char[] contenu = new char[(int) ((AS400JDBCClob)value).length()];
								((AS400JDBCClob)value).getCharacterStream().read(contenu);
								setField(namefield, new String(contenu), true);
							}
							catch (Exception e)
							{
								e.printStackTrace();
								msgErreur += "Erreur sur AS400JDBCClob: " + e;
							}
						}
						else
							if(value instanceof DB2ClobLocator) // C'est un CLOB natif
							{
								try
								{
									char[] contenu = new char[(int) ((DB2ClobLocator)value).length()];
									((DB2ClobLocator)value).getCharacterStream().read(contenu);
									//byte[] tab = new byte[t];
									//((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
									setField(namefield, new String(contenu), true);
								}
								catch (Exception e)
								{
									msgErreur += "Erreur sur DB2ClobLocator: " + e;
								}
							}
							else
								if(value instanceof DB2Clob) // C'est un CLOB natif
								{
									try
									{
										char[] contenu = new char[(int) ((DB2Clob)value).length()];
										((DB2Clob)value).getCharacterStream().read(contenu);
										setField(namefield, new String(contenu), true);
									}
									catch (Exception e)
									{
										e.printStackTrace();
										msgErreur += "Erreur sur DB2Clob: " + e;
									}
								}
								else
									if( value instanceof Long){
										setField(namefield, value);
									} else
										{
											setField(namefield, (String)value, size);
										}
	}

	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 */
	public void setField(String namefield, Object value)
	{
		if (value instanceof BigDecimal)
			setField(namefield, (BigDecimal)value);
		else
			if(value instanceof Integer)
				setField(namefield, String.valueOf(value));
			else
				if(value instanceof Long)
					setField(namefield, String.valueOf(value));
				else
				if(value instanceof byte[])
				{
					int size = ((byte[]) value).length;
					AS400Text champs = new AS400Text(size);
					setField(namefield, (String)champs.toObject((byte[])value), size);
					//setField(namefield, new String((byte[])value));
				}
				else
					if( value instanceof Timestamp ){
						setField(namefield, (Timestamp)value);
					}
					else
					if(value instanceof AS400JDBCClobLocator) // C'est un CLOB
					{
						try
						{
							char[] contenu = new char[(int) ((AS400JDBCClobLocator)value).length()];
							((AS400JDBCClobLocator)value).getCharacterStream().read(contenu);
							//byte[] tab = new byte[t];
							//((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
							setField(namefield, new String(contenu), true);
						}
						catch (Exception e)
						{
							msgErreur += "Erreur sur AS400JDBCClobLocator: " + e;
						}						
					}
					else
						if(value instanceof AS400JDBCClob) // C'est un CLOB
						{
							try
							{
								char[] contenu = new char[(int) ((AS400JDBCClob)value).length()];
								((AS400JDBCClob)value).getCharacterStream().read(contenu);
								setField(namefield, new String(contenu), true);
							}
							catch (Exception e)
							{
								msgErreur += "Erreur sur AS400JDBCClob: " + e;
							}						
						}
						else
							if(value instanceof DB2ClobLocator ) // C'est un CLOB natif
							{
								try
								{
									char[] contenu = new char[(int) ((DB2ClobLocator )value).length()];
									((DB2ClobLocator )value).getCharacterStream().read(contenu);
									//byte[] tab = new byte[t];
									//((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
									setField(namefield, new String(contenu), true);
								}
								catch (Exception e)
								{
									msgErreur += "Erreur sur DB2ClobLocator : " + e;
								}						
							}
							else
								if(value instanceof DB2Clob) // C'est un CLOB natif
								{
									try
									{
										char[] contenu = new char[(int) ((DB2Clob)value).length()];
										((DB2Clob)value).getCharacterStream().read(contenu);
										setField(namefield, new String(contenu), true);
									}
									catch (Exception e)
									{
										msgErreur += "Erreur sur DB2Clob: " + e;
									}						
								}
								else
									setField(namefield, (String)value);
	}

	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 * @param size
	 */
	@SuppressWarnings("unchecked")
	public void setField(String namefield, String valeur, int size)
	{
		if (namefield == null) return;
		Field<String> field = (Field<String>) listFields.get(namefield);
		if (field == null)
		{
			field = new FieldAlpha();
			listFields.put(namefield, field);
		}
		field.setValue(valeur);
		field.name = namefield;
		field.length = size;
		length += size;
	}

	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 */
	@SuppressWarnings("unchecked")
	public void setField(String namefield, String valeur)
	{
		if (namefield == null) return;
		Field<String> field = (Field<String>) listFields.get(namefield);
		if (field == null)
		{
			field = new FieldAlpha();
			listFields.put(namefield, field);
		}
		field.setValue(valeur);
		field.name = namefield;
	}

	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 * @param size
	 * @param decimal
	 */
	@SuppressWarnings("unchecked")
	public void setField(String namefield, BigDecimal valeur, int size, int decimal)
	{
		if (namefield == null) return;
		Field<BigDecimal> field = (Field<BigDecimal>) listFields.get(namefield);
		if (field == null)
		{
			field = new FieldDecimal();
			listFields.put(namefield, field);
		}
		field.setValue(valeur);
		field.name = namefield;
		field.length = size;
		field.decimal = decimal;
		length += size;		
	}

	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 */
	@SuppressWarnings("unchecked")
	public void setField(String namefield, BigDecimal valeur)
	{
		if (namefield == null) return;
		Field<BigDecimal> field = (Field<BigDecimal>) listFields.get(namefield);
		if (field == null)
		{
			field = new FieldDecimal();
			listFields.put(namefield, field);

		}
		field.setValue(valeur);
		field.name = namefield;
	}

	/**
	 * Met � jour la valeur d'un champ (Sp�cial pour les clob)
	 * @param namefield
	 * @param valeur
	 * @param isClob
	 */
	@SuppressWarnings("unchecked")
	public void setField(String namefield, String valeur, boolean isClob)
	{
		if (namefield == null) return;
		Field<String> field = (Field<String>) listFields.get(namefield);
		if (field == null)
		{
			field = new FieldAlpha();
			listFields.put(namefield, field);
		}
		field.setValue(valeur);
		field.name = namefield;
		field.length = valeur!=null?valeur.length():0;
		if (isClob)
			field.setSQLType("com.ibm.as400.access.AS400JDBCClobLocator");
			//field.setSQLType("com.ibm.as400.access.AS400JDBCClob");
	}

	/**
	 * Met � jour la valeur d'un champ 
	 * @param namefield
	 * @param valeur
	 */
	@SuppressWarnings("unchecked")
	public void setField(String namefield, Timestamp valeur)
	{
		if (namefield == null) return;
		Field<Timestamp> field = (Field<Timestamp>) listFields.get(namefield);
		if (field == null)
		{
			field = new FieldTimestamp();
			listFields.put(namefield, field);

		}
		field.setValue(valeur);
		field.name = namefield;
	}

	/**
	 * Retourne si le champ est pr�sent dans l'enregistrement
	 * @param namefield
	 * @return
	 */
	public boolean isPresentField(String namefield)
	{
		if (namefield == null) return false;

		namefield = namefield.toUpperCase();
		// On teste d'abord la pr�sence du Field
		if(listFields.containsKey(namefield))
		{
			//Puis sa valeur si ce n'est pas un champ Clob
			if (!(listFields.get(namefield).getSQLType().equals("com.ibm.as400.access.AS400JDBCClobLocator")))
			//if (!(listFields.get(namefield).getSQLType().equals("com.ibm.as400.access.AS400JDBCClob")))
				return listFields.get(namefield).getValue()!= null;
			else
				return true;
		}
		else
			return false;
	}
	
	/**
	 * On supprime un champ du record
	 * @param namefield
	 * @return
	 */
	public void removeField(String namefield)
	{
		if (namefield == null) return;

		//on teste d'abord la pr�sence du Field
		if(listFields.containsKey(namefield.toUpperCase()))
			listFields.remove(namefield.toUpperCase());
	}

	/**
	 * Retourne la valeur g�n�rique d'un champ
	 * @param indice
	 * @return
	 */
	public Object getField(int indice)
	{
		if (indice < 0) return null;
		return listFields.get(getCle(indice)).getValue();
	}

	/**
	 * Retourne la valeur g�n�rique d'un champ (attention le nom doit �tre en majuscule)
	 * @param namefield
	 * @return
	 */
	public Object getField(String namefield)
	{
		if (namefield == null) return null;
		return listFields.get(namefield).getValue();
	}

	/**
	 * Retourne la valeur g�n�rique d'un champ pour l'affichage avec possibilit� de d�finir une valeur par d�faut
	 * @param indice
	 * @param defaultvalue
	 * @return
	 */
	public Object getDspValueField(int indice, Object defaultvalue)
	{
		Object o = getField(indice);
		if( o == null ){
			if( defaultvalue == null ){
				return "";
			}
			return defaultvalue;
		}
		return o;
	}

	/**
	 * Retourne la valeur g�n�rique d'un champ (attention le nom doit �tre en majuscule) pour l'affichage avec possibilit� de d�finir une valeur par d�faut
	 * @param namefield
	 * @param defaultvalue
	 * @return
	 */
	public Object getDspValueField(String namefield, Object defaultvalue)
	{
		Object o = getField(namefield);
		if( o == null ){
			if( defaultvalue == null ){
				return "";
			}
			return defaultvalue;
		}
		return o;
	}

	/**
	 * Retourne la valeur d'un champ (attention le nom doit �tre en majuscule)
	 * @param namefield
	 * @return
	 */
	public BigDecimal getDecimal(String namefield)
	{
		if (namefield == null) return null;
		return ((FieldDecimal) listFields.get(namefield)).getValue(); 
	}

	/**
	 * Retourne la valeur d'un champ (attention le nom doit �tre en majuscule)
	 * @param namefield
	 * @return
	 */
	public String getString(String namefield)
	{
		if (namefield == null) return null;
		return ((FieldAlpha)listFields.get(namefield)).getValue(); 
	}

	/**
	 * Retourne la valeur d'un champ (attention le nom doit �tre en majuscule)
	 * @param namefield
	 * @return
	 */
	public Timestamp getTimestamp(String namefield)
	{
		if (namefield == null) return null;
		return ((FieldTimestamp) listFields.get(namefield)).getValue(); 
	}


	/**
	 * Retourne le champ (attention le nom doit �tre en majuscule)
	 * @param namefield
	 * @return
	 */
	public Field<?> getInfosField(String namefield)
	{
		if (namefield == null) return null;
		return listFields.get(namefield);
	}

	/**
	 * Retourne le champ
	 * @param indice
	 * @return
	 */
	public Field<?> getInfosField(int indice)
	{
		if (indice < 0) return null;
		return listFields.get(getCle(indice));
	}

	/**
	 * Retourne le nombre de champs 
	 * @return
	 */
	public int getNumberOfFields()
	{
		return listFields.size();
	}

	/**
	 * Retourne la longueur du record 
	 * @return
	 */
	public int getRecordLength()
	{
		return length;
	}
	
	/**
	 * Retourne une chaine avec les champs au bon offset
	 * TODO Vouez � disparaitre
	 */
	public StringBuffer getFlat()
	{
		if (buffer == null)
			buffer = new StringBuffer(length);
		else
			// On initialise � blanc
			buffer.setLength(0);

		String chaine=null;
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext())
		{
		   String cle = (String)it.next();
		   Field<?> valeur = listFields.get(cle);

		   chaine = (String)valeur.toFormattedString(valeur.length, valeur.decimal);
			if (chaine != null) buffer.append(chaine);
		}
		return buffer;
	}

	/**
	 * Retourne une chaine avec les champs s�par�s avec un caract�re au choix
	 */
	public StringBuffer getFlatWithSeparator(String separator)
	{
		if (buffer == null)
			buffer = new StringBuffer(length);
		else
			// On initialise � blanc
			buffer.setLength(0);

		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext())
		{
		   String cle = (String)it.next();
		   Field<?> valeur = listFields.get(cle);
			if (valeur != null)
				buffer.append(valeur.getTrimValue()).append(separator);
			else
				buffer.append(separator);
		}
		buffer.deleteCharAt(buffer.lastIndexOf(separator));
		
		return buffer;
	}

	/**
	 * Initialise l'ent�te avec un enregistrement plat
	 * @param record
	 * @return
	 */
	public boolean setFlat(String record)
	{
		if (record == null) return false;
		
		final int tailleRecord = record.length();
		int offset_deb=0;
		int offset_fin=0;

		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext())
		{
			String cle = (String)it.next();
			Field<?> valeur = listFields.get(cle);

			offset_deb = offset_fin;
			offset_fin += valeur.length;

			if (offset_fin <= tailleRecord)
				setField(cle, record.substring(offset_deb, offset_fin).trim());
			else
				setField(cle, record.substring(offset_deb).trim());
		}
		return true;
	}

	/**
	 * Retourne une chaine du type Objet JSON (il y a � la fois les clefs et les valeurs)
	 * @param trim
	 * @return
	 * API JsonSimple
	public JSONObject getJSON(boolean trim)
	{
		JSONObject objjson = new JSONObject();
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		if (trim)
			while (it.hasNext())
			{
				String cle = (String)it.next();
				//Field valeur = listFields.get(cle);
				objjson.put(cle, listFields.get(cle).getTrimValue());
			}
		else
			while (it.hasNext())
			{
				String cle = (String)it.next();
				objjson.put(cle, listFields.get(cle).getValue());
			}
		
		return objjson;
	}*/
	
	/**
	 * Retourne une chaine du type Objet JSON (il y a � la fois les clefs et les valeurs)
	 * @param trim
	 * @return
	 */
	public JsonObject getJSON(boolean trim)
	{
		JsonObject objjson = new JsonObject();
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		if (trim)
			while (it.hasNext())
			{
				String cle = (String)it.next();
				Object value = listFields.get(cle).getTrimValue();
				if (value instanceof String)
					objjson.add(cle, new JsonPrimitive((String)value));
				else
					objjson.add(cle, new JsonPrimitive((BigDecimal)value));
			}
		else
			while (it.hasNext())
			{
				String cle = (String)it.next();
				Object value = listFields.get(cle).getValue();
				if (value instanceof String)
					objjson.add(cle, new JsonPrimitive((String)value));
				else
					objjson.add(cle, new JsonPrimitive((BigDecimal)value));
			}
		
		return objjson;
	}

	/**
	 * Retourne une chaine du type Array JSON (il y n'y a que les clefs)
	 * @param trim
	 * @return
	 * API JsonSimple
	public JSONArray getJSONKey()
	{
		JSONArray arrayjson = new JSONArray();
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext())
		{
			arrayjson.add((String)it.next());
		}
		
		return arrayjson;
	}*/

	/**
	 * Retourne une chaine du type Array JSON (il y n'y a que les clefs)
	 * @param trim
	 * @return
	 */
	public JsonArray getJSONKey()
	{
		JsonArray arrayjson = new JsonArray();
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext())
		{
			arrayjson.add(new JsonPrimitive((String)it.next()));
		}
		
		return arrayjson;
	}

	/**
	 * Retourne une chaine du type Array JSON (il y n'y a que les valeurs)
	 * @param trim
	 * @return
	 * API JsonSimple
	public JSONArray getJSONValue(boolean trim)
	{
		JSONArray arrayjson = new JSONArray();
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		if (trim)
			while (it.hasNext())
			{
				String cle = (String)it.next();
				arrayjson.add(listFields.get(cle).getTrimValue());
			}
		else
			while (it.hasNext())
			{
				String cle = (String)it.next();
				arrayjson.add(listFields.get(cle).getValue());
			}
		
		return arrayjson;
	}*/

	/**
	 * Retourne une chaine du type Array JSON (il n'y a que les valeurs)
	 * @param trim
	 * @return
	 */
	public JsonArray getJSONValue(boolean trim)
	{
		JsonArray arrayjson = new JsonArray();
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		if (trim)
			while (it.hasNext())
			{
				String cle = (String)it.next();
				Object value = listFields.get(cle).getTrimValue();
				if (value instanceof String)
					arrayjson.add(new JsonPrimitive((String)value));
				else
					arrayjson.add(new JsonPrimitive((BigDecimal)value));

			}
		else
			while (it.hasNext())
			{
				String cle = (String)it.next();
				Object value = listFields.get(cle).getValue();
				if (value instanceof String)
					arrayjson.add(new JsonPrimitive((String)value));
				else
					arrayjson.add(new JsonPrimitive((BigDecimal)value));
			}
		
		return arrayjson;
	}

	/**
	 * Initialise l'ent�te avec un enregistrement au format JSON
	 * @param record
	 * @return
	 * API JsonSimple
	public boolean setJSON(String record)
	{
		if (record == null) return false;
		
		JSONParser parser = new JSONParser();
		JSONObject objet;

		try
		{
			objet = (JSONObject) parser.parse(record);
			
			Set<String> cles = listFields.keySet();
			Iterator<String> it = cles.iterator();
			while (it.hasNext())
			{
			   String cle = (String)it.next();
			   //Object valeur = listFields.get(cle);
			   setField(cle, objet.get(cle));
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			return false;
		}
		return true;
	}*/

	/**
	 * Initialise l'ent�te avec un enregistrement au format JSON
	 * @param record
	 * @return
	 */
	public boolean setJSON(String record)
	{
		if (record == null) return false;
		
		JsonObject objet;

		try
		{
			objet = (JsonObject) parser.parse(record);
			
			Set<String> cles = listFields.keySet();
			Iterator<String> it = cles.iterator();
			while (it.hasNext())
			{
			   String cle = (String)it.next();
			   //Object valeur = listFields.get(cle);
			   setField(cle, objet.get(cle));
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Recherche le nom de la cle avec l'indice
	 * @param indice
	 * @return
	 */
	private String getCle(int indice)
	{
		int i=0;
		
		Set<String> cles = listFields.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext())
		{
		   String cle = (String)it.next();
		   if (i == indice) return cle;
		   i++;
		}
		return null;
	}

	/**
	 * Retourne un tableau avec les titres des records
	 * @param trim
	 * @return
	 */
	public String[] getArrayTitle()
	{
		return getArrayTitle(false);
	}

	/**
	 * Retourne un tableau avec les titres des records
	 * @param trim
	 * @return
	 */
	public String[] getArrayTitle(boolean omitfields)
	{
		int i=0;

		String[] title = null;
		if( omitfields )
			title = new String[listFields.size() - nbrOmitField];
		else
			title = new String[listFields.size()];
		
		for (Map.Entry<String, Field<?>> entry : listFields.entrySet()) {
		    Field<?> value = entry.getValue();
		    if( omitfields && value.isOmitRequest() ) continue;
		    title[i++] = (String)entry.getKey();
		}
		
		return title;
	}

	/**
	 * Retourne un tableau avec les donn�es 
	 * @param trim
	 * @return
	 */
	public Object[] getArrayValue(boolean trim)
	{
		return getArrayValue(false, trim, true);
	}
	
	/**
	 * Retourne un tableau avec les donn�es 
	 * @param trim
	 * @return
	 */
	public Object[] getArrayValue(boolean omitfields, boolean trim, boolean justvalue) // Evite le remplacement de la valeur par ##??##
	{
		int i=0;
		Object[] data = null;
		
		if( omitfields )
			data = new Object[listFields.size() - nbrOmitField];
		else
			data = new Object[listFields.size()];

		if (trim)
			for (Map.Entry<String, Field<?>> entry : listFields.entrySet())
			{
			    Field<?> value = entry.getValue();
//System.out.println("-getArrayValue->" + value + " " + entry.getValue());			    
			    if( omitfields && value.isOmitRequest() ) continue;
				if( !justvalue && value.getSQLType().equals("com.ibm.as400.access.AS400JDBCClobLocator") ) // TODO � corriger via un Fieldclob
				//if (listFields.get(cle).getSQLType().equals("com.ibm.as400.access.AS400JDBCClob"))
					data[i++] = TYPE_PARTICULIER;
				else
					data[i++] = value.getTrimValue();
			}
		else
			for (Map.Entry<String, Field<?>> entry : listFields.entrySet())
			{
			    Field<?> value = entry.getValue();
			    if( omitfields && value.isOmitRequest() ) continue;
				if( !justvalue && value.getSQLType().equals("com.ibm.as400.access.AS400JDBCClobLocator") )  // TODO � corriger via un Fieldclob
				//if (listFields.get(cle).getSQLType().equals("com.ibm.as400.access.AS400JDBCClob"))
					data[i++] = "?";
				else
					data[i++] = value.getValue();
			}
		
		return data;
	}

	/**
	 * V�rifie que tous les champs obligatoires soient pr�sents dans le record et que leur valeur ne soit pas nulle 
	 * @return
	 */
	public boolean verifRequiredField()
	{
		if (requiredField == null) return true;
		
		for (String chaine: requiredField)
			if (!isPresentField(chaine))
			{
				msgErreur += "Le champ " + chaine + " n'est pas pr�sent ou sa valeur n'est pas correcte.\n";
				return false;
			}
		return true;
	}

	/**
	 * Retourne une requ�te SQL (UPDATE) � partir des donn�es du record
	 * @param table
	 * @param lib
	 * @param condition
	 * @return
	 */
	public String createSQLRequestUpdate(String table, String lib, String condition)
	{
		StringBuilder sb = new StringBuilder(512); 

		// On v�rifie d'abord les champs obligatoires
		if (!verifRequiredField()) return null;
		
		String[] title = getArrayTitle(true);
		Object[] value = getArrayValue(true, true, false);
		
		// On charge les cles et les valeurs
		sb.append("UPDATE ").append(lib).append('.').append(table).append(" SET ");
		for (int i=0; i<title.length; i++)
		{
			if (value[i] == null) continue;

			sb.append(title[i]).append('=');
			Object valeur = value[i];
			if (valeur instanceof Timestamp){
				sb.append("?, ");
			}
			else
				if (valeur instanceof String)
					if (valeur.equals(TYPE_PARTICULIER))
						sb.append("?, ");
					else
						sb.append('\'').append(FieldAlpha.format4Request((String)valeur)).append("', ");
				else
					if (valeur instanceof BigDecimal)
						sb.append(((BigDecimal)valeur).doubleValue()).append(", ");
					else
						sb.append(valeur).append(", ");
		}
		sb.deleteCharAt(sb.length()-2);

		// Construction de la requ�te
		if ((condition != null) && (!condition.trim().equals("")))
			if (condition.toUpperCase().contains("WHERE"))
				sb.append(' ').append(condition);
			else
				sb.append(" WHERE ").append(condition);
		
		return sb.toString();
	}

	/**
	 * Retourne une requ�te SQL (INSERT) � partir des donn�es du record
	 * @param table
	 * @param lib
	 * @return
	 */
	public String createSQLRequestInsert(String table, String lib)
	{
		StringBuilder sbk = new StringBuilder(512); 
		StringBuilder sbv = new StringBuilder(512); 

		// On v�rifie d'abord les champs obligatoires
		if (!verifRequiredField()) return null;
		
		String[] title = getArrayTitle(true);
		Object[] value = getArrayValue(true, true, false);
		
		// On charge les cles et les valeurs
		for (int i=0; i<title.length; i++)
		{
//System.out.println("-createSQLRequestInsert->" + title[i] + " " + value[i]);				
			if (value[i] == null) continue;

			sbk.append(title[i]).append(',');
			Object valeur = value[i];
			if (valeur instanceof Timestamp){
				sbv.append("?, ");
			}
			else
				if (valeur instanceof String)
					if (valeur.equals(TYPE_PARTICULIER))
						sbv.append("?, ");
					else
						sbv.append('\'').append(FieldAlpha.format4Request((String)valeur)).append("', ");
				else
					if (valeur instanceof BigDecimal)
						sbv.append(((BigDecimal) valeur).doubleValue()).append(", ");
					else
						sbv.append(valeur).append(", ");
		}
		sbk.deleteCharAt(sbk.length()-1);
		sbv.deleteCharAt(sbv.length()-2);

		// Construction de la requ�te
		sbk.insert(0, " (").insert(0, table).insert(0, '.').insert(0, lib).insert(0, "INSERT INTO ").append(") VALUES (").append(sbv).append(')');

		return sbk.toString();
	}

	/**
	 * Pour la compatibilit� mais ne pas utiliser dans le futur, pr�f�rer createSQLRequestInsert 
	 * @param table
	 * @param lib
	 * @return
	 */
	public String getInsertSQL(String table, String lib)
	{
		return createSQLRequestInsert(table, lib);
	}
	
	/**
	 * Retourne le champ avec le type voulu et son rang (qui commence � 1) 
	 * @return
	 */
	public Field<?> getFieldWithTypeAndNumber(String type, int num)
	{
		int rang = 1;
		for(Entry<String, Field<?>> entry : listFields.entrySet())
		{
			// On recherche le bon type
			if (!entry.getValue().getSQLType().equals(type)) continue;
			// On v�rifie que ce soit le bon rang
			if (rang == num)
				return entry.getValue();
			rang++;
		}
		return null;
	}
	
	/**
	 * Permet d'initialiser un objet � partir des donn�es du record
	 * @param object
	 * 
	 * Les types g�r�s:
	 * Pour l'objet: char, String, int, long, short, byte, BigDecimal
	 * Pour le record: String, BigDecimal
	 * 
	 * Note: il faudrait tenter d'utiliser les annotations � la place du nom des m�thodes (solution plus souple)
	 */
	public boolean toObject(Object object){
		if( object == null ) return false;
		
		Class<?> classe = object.getClass();
		// R�cup�ration des m�thodes 
		Method[] methods =  classe.getMethods();
		
		// On va initialiser les variables de la classe object avec les accesseurs de cette m�me classe
		for( Method method: methods){
			// On ne s�lectionne que les accesseurs d'init
			if( !method.getName().startsWith("set") ) continue;
			String variable = method.getName().substring(3).trim();
//System.out.println("-gr-> variable " + variable);			
			// On recherche la variable dans le record
			if( !isPresentField( variable )) continue;
			ri.seriem.libas400.database.field.Field<?> champ = getInfosField( variable );
			if( champ.getValue() == null ) continue;
			
			// On alimente la variable de la classe avec la valeur du record
			try
			{
				Type[] tv = method.getGenericParameterTypes();
				// Le type du champ est un String
				String typeChamp = champ.getSQLType();  
//System.out.println("-gr-> methode " + method.getName()+ " typechamp " + typeChamp + " tv:" + tv[0]);			
				if( typeChamp.equals("java.lang.String")){
					String value = (String)champ.getValue();
					//if( tv[0].toString().lastIndexOf("char") != -1 )
					if( tv[0] == char.class )
						method.invoke(object, value.charAt(0));
					else
						//if( tv[0].toString().lastIndexOf("String") != -1 )
						if( tv[0] == String.class )
							method.invoke(object, value.trim());
						else
							if( tv[0] == Integer.class )
								method.invoke(object, Integer.parseInt(value.trim()));
							else
								if( tv[0] == int.class )
									method.invoke(object, Integer.parseInt(value.trim()));
								else
									if( tv[0] == byte.class )
										method.invoke(object, Byte.parseByte(value.trim()));
									else
										if( tv[0] == short.class )
											method.invoke(object, Short.parseShort(value.trim()));

							
				} else
					// Le type du champ est un BigDecimal 
					if( typeChamp.equals("java.math.BigDecimal") ){
						BigDecimal value = (BigDecimal)champ.getValue();
						//if( tv[0].toString().lastIndexOf("BigDecimal") != -1 )
						if( tv[0] == java.math.BigDecimal.class )
							method.invoke(object, value);
						else
							//if( tv[0].toString().lastIndexOf("int") != -1 )
							if( tv[0] == int.class ){
								method.invoke(object, value.intValue());
							}
							else
								//if( tv[0].toString().lastIndexOf("long") != -1 )
								if( tv[0] == long.class )
									method.invoke(object, value.longValue());
								else
									//if( tv[0].toString().lastIndexOf("byte") != -1 )
									if( tv[0] == byte.class )
										method.invoke(object, value.byteValue());
									else
										if( tv[0] == short.class )
											method.invoke(object, value.shortValue());
					}
					else
						if( typeChamp.equals("java.sql.Timestamp") ){
							Timestamp value = (Timestamp)champ.getValue();
							method.invoke(object, value);
						}
						else
							if( typeChamp.equals("com.ibm.as400.access.AS400JDBCClobLocator") ){
								method.invoke(object, champ.getValue());
							}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Permet d'initialiser les donn�es du record � partir d'un objet (Attention les variables de doivent �tre obligatoirement en majuscules)
	 * @param object
	 * @param clearBefore
	 * 
	 * Les types g�r�s:
	 * Pour l'objet: char, String, int, long, short, byte, BigDecimal
	 * Pour le record: String, BigDecimal
	 * 
	 * Note: il faudrait tenter d'utiliser les annotations � la place du nom des m�thodes (solution plus souple)
	 */
	public boolean fromObject(Object object, boolean clearBefore){
		if( object == null ) return false;
		if( clearBefore )
			listFields.clear();
		
		String variable = null;
		Class<?> classe = object.getClass();
		// R�cup�ration des m�thodes 
		Method[] methods =  classe.getMethods();
				
		// On va initialiser les variables de la classe object avec les accesseurs de cette m�me classe
		for( Method method: methods){
//System.out.println("-methode name-> " +  method.getName());			
			// On ne s�lectionne que les accesseurs de retour
			if( method.getName().startsWith("get") ) {
				variable = method.getName().substring(3).trim();
			} else if( method.getName().startsWith("is") ) {
				variable = method.getName().substring(2).trim();
			} else
				continue;
//System.out.println("-variable-> " +  variable);			
			// On v�rifie qu'il s'agit bien d'un champ fichier (car obligatoirement en majuscule)
			if ( !variable.toUpperCase().equals( variable )) continue;
			
			// On alimente les donn�es du record avec la variable de la classe
			try
			{
				Type type = method.getGenericReturnType();
				Object o = method.invoke(object);
				if ( o == null ) continue;
				
//System.out.println("-GenericRecord-> type " + type + " variable " + variable);
						
				// Le type du champ est un char
				if( type == char.class ) {
					listFields.put(variable, new FieldAlpha(o.toString(), 1));
				} else if( type == String.class ) { // Le type du champ est un String
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
					listFields.put(variable, new FieldAlpha(o.toString(), size.getInt(object)));
				} else if( type == java.math.BigDecimal.class ) { // Le type du champ est un BigDecimal
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
					java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
					listFields.put(variable, new FieldDecimal((BigDecimal) o, size.getInt(object), decimal.getInt(object)) );
				} else if( type == int.class ) { // Le type du champ est un int
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
					java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
					listFields.put(variable, new FieldDecimal(new BigDecimal((Integer)o), size.getInt(object), decimal.getInt(object)) );
				} else if( type == long.class ) { // Le type du champ est un long
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
					java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
					listFields.put(variable, new FieldDecimal(new BigDecimal((Long)o), size.getInt(object), decimal.getInt(object)) );
				} else if( type == byte.class ) { // Le type du champ est un byte
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
					java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
					listFields.put(variable, new FieldDecimal(new BigDecimal((Byte)o), size.getInt(object), decimal.getInt(object)) );
				} else if( type == short.class ) { // Le type du champ est un short
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
					java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
					listFields.put(variable, new FieldDecimal(new BigDecimal((Short)o), size.getInt(object), decimal.getInt(object)) );
				} else if( type == Timestamp.class ) { // Timestamp
					java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
//System.out.println("-->variable " + variable + "  " + o);					
					listFields.put(variable, new FieldTimestamp( (Timestamp) o) );
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}

		}
		
		// Gestion de l'attribut Omit dans les champs
		if( omittedField != null ) {
			for( String field: omittedField){
//System.out.println("-GenericRecord->field " + field + " " + listFields.get(field));						
				listFields.get(field).omitRequest = true;
			}
		}

		return true;
	}


	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le requiredField
	 */
	public String[] getRequiredField()
	{
		return requiredField;
	}

	/**
	 * @param requiredField le requiredField � d�finir
	 */
	public void setRequiredField(String[] requiredField)
	{
		this.requiredField = requiredField;
	}

	/**
	 * @return le omittedField
	 */
	public String[] getOmittedField()
	{
		return omittedField;
	}

	/**
	 * @param omittedField le omittedField � d�finir
	 */
	public void setOmittedField(String[] omittedField)
	{
		this.omittedField = omittedField;
		nbrOmitField = omittedField!=null?omittedField.length:0;
	}
	
	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		listFields.clear();
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
