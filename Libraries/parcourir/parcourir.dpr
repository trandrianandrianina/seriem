// Ouvre la popup Windows afin de rechercher un fichier dans l'arborescence du disque
// est retourne son chemin � JWalk
// Param�tre:
//    entree :
//             la session
//             le nom de la variable receptrice
//             la taille en octet de cette variable

program parcourir;
{$APPTYPE GUI}
uses
  Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs, DdeMan;

Function GfApiMainSessionConnect(SessionId: LongInt; AppName: String; CapBits: LongInt; ConMode: LongInt; WinHand: LongInt; Anchor: LongInt; var ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiMainSessionDisconnect(ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiHostFieldPutData(ApiHand: LongInt; VarName: String; line: LongInt; VarValue: String; VarLen: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';

{$R *.RES}

const
  GF_OK : longint = 0;    // No error / OK
  GF_SCRIPT_VAR_STRING : longint = 0;
  GF_SCRIPT_VAR_NUMERIC : longint = 1;
  GF_EOF : longint = -1;  // End of file
  GF_FAIL : longint = 1;  // Failure
  GF_FALSE : longint = 0; // False
  GF_TRUE : longint = 1; // True

  GF_CONNECT_LOCKED : longint = 1; //One application to session.
  GF_CONNECT_API : longint = 2;    //Connect to API only. No session given
  GF_CONNECT_DLL : longint = 3;    //Connection is with DLL. Need not be done
  GF_CONENCT_NEW : longint = 4;    //Start a blind session when it doesn't exist

var
  api_hand : longint;
  ois_error : longint;
  nomvar : array[0..14] of char;
  buffer : array[0..255] of char;
  handlexplore : hwnd;

  OD_Parcourir: TOpenDialog;
  i, limite : Integer;
  chaine : string;
  repertoire : string;
  retour : boolean;
  sessionid : char;

begin
    repertoire := GetCurrentDir;
    chaine := '';
    retour := false;

    // Lecture des param�tres
    if ParamCount = 0 then
        Application.Terminate;

    if Paramcount > 2 then
        limite := StrToInt(ParamStr(3))
    else
        limite := 255;

    // Connexion � la session JWalk en cours
    sessionid := char(ParamStr(1)[1]);

    // Connexion � la session JWalk en cours
    handlexplore := FindWindow(nil, 'parcourir');
    api_hand := 0;
    ois_error := GfApiMainSessionConnect(ord(sessionid), 'RI', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
    if ois_error <> GF_OK then
        Application.Terminate;

    // Ouvre la popup pour rechercher le fichier
    OD_Parcourir := TOpenDialog.Create(nil);
    OD_Parcourir.Title := 'Chercher le fichier';
    OD_Parcourir.Filter := 'Tous les fichiers|*.*';
    OD_Parcourir.Execute;
    if Trim(OD_Parcourir.FileName) <> '' then
        if Length(OD_Parcourir.FileName) > limite then begin
            ShowMessage('Le chemin d�passe la capacit� de la variable r�ceptrice (Chemin trop long).');
            retour := false; end
        else begin
            chaine := OD_Parcourir.FileName;
            retour := true;
        end;

    if retour then begin
        // Envoi du chemin dans la variable globale JWALK
        StrPCopy(nomvar, ParamStr(2));
        StrPCopy(buffer, chaine);
        ois_error := GfApiHostFieldPutData(api_hand, nomvar, 0, buffer, 255);
        if ois_error <> GF_OK then
            Showmessage('Erreur lors de l''init de la variable JWALK');
    end;

    // Repositionne dans le r�pertoire avant la recherche
    SetCurrentDir(repertoire);

    // D�connection de la session JWalk en cours
    ois_error := GfApiMainSessionDisconnect(api_hand);
end.
