// Lance le crunch lors de la sauvegarde
// Param�tre:
//    entree : le numero de t�l�phone
//             le nom du client
//             1 pour num�roter
//             le prefixe
//

program numeroteur;
{$APPTYPE GUI}
uses
  Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs;

function tapiRequestMakeCall(lpszDestAddress, lpszAppName, lpszCalledParty, lpszComment: LPCSTR) : DWORD; stdcall; far; external 'TAPI32.DLL';

const
    TAPIERR_NOREQUESTRECIPIENT : Integer = -2;
    TAPIERR_REQUESTQUEUEFULL : Integer = -3;
    TAPIERR_INVALDESTADDRESS : Integer = -4;
    LINECALLPRIVILEGE_NONE : Integer = 1;
    LINEMEDIAMODE_INTERACTIVEVOICE : Integer = 4;

var
    NomClient : string;
    Prefixe : string;
    NumTelephone : string;
    Numerote : integer;


{$R *.RES}

//------------------------------------------------------------------------------
//--> Appel TAPI
//------------------------------------------------------------------------------
function Appel : Integer;
var
    i, j, retour : Integer;
    buff : String;
    numero : array[0..255] of char;
    client : array[0..255] of char;
begin
    //strPCopy(numero, Prefixe + NumTelephone);
    NumTelephone := Trim(Prefixe) + Trim(NumTelephone);
    j := 0;
    for i := 1 to Length(NumTelephone) do
        if (NumTelephone[i] >= '0') and (NumTelephone[i] <= '9') then begin
            numero[j] := NumTelephone[i];
            Inc(j);
        end;
    strPCopy(client, NomClient);
    retour := tapiRequestMakeCall(numero, 'Num�roteur S�rie M', client, '');
    // Affiche un message si erreur
    if retour <> 0 then begin
        buff := 'Erreur lors de la num�rotation : ';
       case retour of
            -2 : buff := buff + 'No Windows Telephony dialing application is running and none could be started.';
            -3 : buff := buff + 'The queue of pending Windows Telephony dialing requests is full.';
            -4 : buff := buff + 'The phone number is not valid.';
            else
                buff := buff + 'Erreur inconnue.';
         end;
        MessageDlg(buff, mtError, [mbOk], 0);
    end;
    Appel := retour;
end;

//------------------------------------------------------------------------------
//--> Programme principal
//------------------------------------------------------------------------------
begin
    // Lecture des param�tres
    if ParamCount < 3 then begin
        ShowMessage('Il manque des param�tres.');
        Application.Terminate;
    end;

    NumTelephone := Trim(ParamStr(1));
    NomClient := Trim(ParamStr(2));
    Numerote := StrToInt(ParamStr(3));

    if ParamCount = 4 then
        Prefixe := Trim(ParamStr(4));

    Appel;
end.
