program explore;
{$APPTYPE GUI}

uses Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj;


{$R *.RES}

//------------------------------------------------------------------------------
//--> Retourne s'il s'agit d'un NT ou pas
//------------------------------------------------------------------------------
function isNT : boolean;
var
    retour : boolean;
begin
    if SysUtils.Win32Platform = VER_PLATFORM_WIN32_NT then
        retour := true
    else
        retour := false;
    isNT := retour;
end;


var
    Handle : HWND;

begin
//showmessage(getcurrentdir);
    if isNT = false then
        ShellExecute(Handle,'open', PChar(ParamStr(1)), nil, nil, SW_SHOWNORMAL)
    else
        WinExec(PChar('explorer.exe '+ParamStr(1)), SW_SHOWNORMAL);

end.
