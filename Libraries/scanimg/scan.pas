unit scan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, FileCtrl, Registry, ExtCtrls;

type
  TForm1 = class(TForm)
    L_Dossier: TLabel;
    BT_Fermer: TButton;
    procedure FormActivate(Sender: TObject);
    procedure BT_FermerClick(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    function RegRead (Mykey,MyField : String) : string;
    function ScanDossier(Filespec:string):boolean;
  end;

var
  Form1: TForm1;
  Dossier : string;
  F: TextFile;

implementation

{$R *.DFM}

//------------------------------------------------------------------------------
//--> Lit une chaine dans la base de registre
//------------------------------------------------------------------------------
function TForm1.RegRead (Mykey,MyField : String) : string;
begin
  //Create the Object
  with (TRegistry.Create) do begin
    //Sets the destination for our requests
    RootKey:=HKEY_LOCAL_MACHINE;
    //Check if whe can open our key, if the key dosn't exist, we create it
    if OpenKey(MyKey, false) then begin
      //Is our field availbe
      if ValueExists(MyField) then
          //Read the value from the field
          result := ReadString(MyField)
      else
          //ShowMessage(MyField+' does not exists under '+MyKey);
          result := ''; end
    else
        //There is a big error if we get an errormessage by
        //opening/creating the key
        ShowMessage('Error opening/creating key : '+MyKey);
  end;
end;

function TForm1.ScanDossier(Filespec:string) : boolean;
var
  validres : integer;
  SearchRec : TSearchRec;
  DirPath, FullName, Flname : string;

  chaine : String;
begin
     DirPath := ExtractFilePath(FileSpec);
     Result := DirectoryExists(DirPath);
     if not Result then exit;
     Flname := ExtractFileName(FileSpec);
     validres := FindFirst(FileSpec, faAnyFile, SearchRec);
     while validres=0 do begin
          if (SearchRec.Name[1] <> '.') then begin {not a directory}
	      FullName:=DirPath + LowerCase(SearchRec.Name);
              if ((SearchRec.Attr and faDirectory) = 0) then begin
                  chaine := UpperCase(FullName);
                  chaine := Copy(chaine, Length(Dossier)+2, Length(chaine)-Length(Dossier)-1);
                  if (Pos('\PHO\', chaine) <> 0) then
                      Writeln(F, chaine);
              end;

              if ((SearchRec.Attr and faDirectory) > 0) then
                 ScanDossier(FullName+'\'+Flname);
          end;
          validres:=FindNext(SearchRec);
     end;
end;

procedure TForm1.FormActivate(Sender: TObject);
var
   position : integer;
   DossierVGM : String;
begin
    repaint;
    if ParamCount = 1 then
        Dossier := ParamStr(1)
    else begin
        Dossier := Trim(RegRead('SOFTWARE\RI\vgm', 'installpath'));
        if Dossier = '' then begin
           ShowMessage('Il manque des param�tres:'+#13#10+'Pr�ciser le chemin que vous souhaitez balayer.');
           Form1.Caption := 'En erreur.';
           BT_Fermer.Enabled := True;
           Exit; end
        else
            Dossier := Dossier + '\Documents';
    end;
    L_Dossier.Caption := Dossier;
    position := LastDelimiter('\', Dossier);
    DossierVGM := Copy(Dossier, 1, position-1);

    // On ouvre le fichier en �criture
    AssignFile(F, DossierVGM+'\listephoto.txt');
    Rewrite(F);
    ScanDossier(Dossier+'\*.*');

    // On ferme de fichier
    CloseFile(F);
    Form1.Caption := 'Balayage du dossier termin�.';
    BT_Fermer.Enabled := True;
end;

procedure TForm1.BT_FermerClick(Sender: TObject);
begin
    Application.Terminate;
end;

end.
