object Form1: TForm1
  Left = 192
  Top = 107
  BorderStyle = bsSingle
  Caption = 'Balayage du dossier en cours ...'
  ClientHeight = 75
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object L_Dossier: TLabel
    Left = 16
    Top = 16
    Width = 257
    Height = 17
    Alignment = taCenter
    AutoSize = False
  end
  object BT_Fermer: TButton
    Left = 104
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Fermer'
    Enabled = False
    TabOrder = 0
    OnClick = BT_FermerClick
  end
end
