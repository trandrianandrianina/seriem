unit exportation;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    CB_separateur: TComboBox;
    BT_Valider: TButton;
    BT_Annuler: TButton;
    SD_Parcourir: TSaveDialog;
    Label2: TLabel;
    CB_Version: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure BT_AnnulerClick(Sender: TObject);
    procedure BT_ValiderClick(Sender: TObject);
  private
    { D�clarations priv�es}
  public
    { D�clarations publiques}
  end;

var
  Form1: TForm1;
  xtabbasic : array[1..2] of string=('"Nom"','"Adresse d''email"');
  xtab2000: array [1..12] of string=('"Titre"','"Nom"','"Titre"','"Soci�t�"','"Rue (bureau)"','"Rue (bureau) 2"','"Ville (bureau)"','"Code postal (bureau)"','"T�l�phone (bureau)"','"T�l. mobile"','"T�l�copie (bureau)"','"Adresse de messagerie"');

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
    // R�cup�ration des param�tres
    if ParamCount = 0 then begin
        MessageDlg('Param�tre manquant.', mtWarning, [mbOk], 0);
        Halt(1);
    end;
end;

procedure TForm1.BT_AnnulerClick(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TForm1.BT_ValiderClick(Sender: TObject);
var
   F : TextFile;
   tableau : array[1..100] of string;
   i : integer;

begin
    for i := 1 to ParamCount do begin
        tableau[i] := ParamStr(i);
//         showMessage(IntTOStr(i) + ':' + ParamStr(i));
    end;
    for i := (ParamCount+1) to 100 do
        tableau[i] := '';

     {$I-}
    SD_Parcourir.Title := 'Exportation fichier';
    if SD_Parcourir.Execute then begin
      // Cr�ation du fichier d'importation
      AssignFile(F, SD_Parcourir.FileName);
      ReWrite(F);
      if CB_Version.Text = 'Basic' then begin
          Write(F, xtabbasic[1]+CB_separateur.Text);
          WriteLn(F, xtabbasic[2]);
          Write(F, '"'+tableau[2]+'"' + CB_separateur.Text);
          WriteLn(F, '"'+tableau[11]+'"'); end
      else
        if CB_Version.Text = 'Office 2000' then begin
          for i:=1 to 11 do
              Write(F, xtab2000[i]+CB_separateur.Text);
          WriteLn(F, xtab2000[12]);
          Write(F, '"'+tableau[1]+'"' + CB_separateur.Text);
          Write(F, '"'+tableau[2]+'"' + CB_separateur.Text);
          Write(F, '"'+tableau[1]+'"' + CB_separateur.Text);
          for i:= 3 to 10 do
              Write(F, '"'+tableau[i]+'"' + CB_separateur.Text);
          WriteLn(F, '"'+tableau[11]+'"');
        end;
      CloseFile(F);
    end;
    {$I+}

    MessageDlg('Exportation termin�e.'+#10#13+'(Fichier texte avec valeurs s�par�es par ' + CB_separateur.Text + ')', mtInformation, [mbOk], 0);
    Application.Terminate;
end;

end.