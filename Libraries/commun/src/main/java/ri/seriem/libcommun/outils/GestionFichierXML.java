//=================================================================================================
//==>                                                                       02/01/2008 - 22/12/2009
//==> Gestion des fichiers XML 
//==> A faire:
//==>	A modifier comme GestionFichierTexte
//=================================================================================================

package ri.seriem.libcommun.outils;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;

import javax.swing.JFileChooser;


public class GestionFichierXML
{
	// Constantes erreurs de chargement
	private final static String ERREUR_FICHIER_INTROUVABLE="Le fichier est introuvable.";
	private final static String ERREUR_LECTURE_FICHIER="Erreur lors de la lecture du fichier.";
	private final static String ERREUR_ENREGISTREMENT_FICHIER="Erreur lors de l'enregistrement du fichier.";
	private final static String ERREUR_BUFFER_VIDE="Le buffer est vide.";

	// Constantes
	private final static String FILTER_EXT_XML="xml";
	private final static String FILTER_DESC_XML="Fichier XML (*.xml)";

	// Variables
	private File nomFichier=null;
	private URL urlFichier=null;
	private Object contenuFichier=null;

	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu


	/**
	 * Constructeur
	 */
	public GestionFichierXML()
	{
	}

	/**
	 * Constructeur
	 * @param nomFichier
	 */
	public GestionFichierXML(String nomfichier)
	{
		setNomFichier(nomfichier);
	}

	/**
	 * Constructeur
	 * @param nomFichier
	 */
	public GestionFichierXML(File fichier)
	{
		setNomFichier(fichier.getAbsolutePath());
	}

	/**
	 * Constructeur de la classe
	 * @param urlFichier
	 */
	public GestionFichierXML(URL urlFichier)
	{
		nomFichier = null;
		this.urlFichier = urlFichier;
	}

	/**
	 * Initialise le nom du fichier
	 * @param nomFichier
	 */
	public void setNomFichier(File nomFichier)
	{
		this.nomFichier = nomFichier;
	}

	/**
	 * Initialise le nom du fichier
	 * @param nomFichier
	 */
	public void setNomFichier(String nomFichier)
	{
		if (nomFichier == null) return;
		this.nomFichier = new File(nomFichier);
	}

	/**
	 * Retourne le nom du fichier
	 * @return
	 */
	public String getNomFichier()
	{
		return nomFichier.getAbsolutePath();
	}

	/**
	 * V�rifie l'existence d'un fichier
	 * @return
	 */
	private boolean isPresent()
	{
		if (nomFichier == null) return false;

		return nomFichier.exists();
	}

	/**
	 * Initialise le buffer 
	 * @param contenuFichier
	 */
	public void setContenuFichier(Object contenuFichier)
	{
		this.contenuFichier = contenuFichier; 
	}

	/**
	 * Retourne le buffer
	 * @param contenuFichier
	 */
	public Object getContenuFichier()
	{
		if (contenuFichier == null)
			if (lectureFichier() == Constantes.ERREUR) 
				return null;
		
		return this.contenuFichier; 
	}

	/**
	 * Lecture du fichier XML
	 * @return
	 */
	public int lectureFichier()
	{
		XMLDecoder decoder=null;

		// On v�rifie que le fichier existe
		if ((urlFichier == null) &&  !isPresent())
		{
			msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE;
			return Constantes.FALSE;
		}

		// Lecture du fichier XML
		try
		{
			if (nomFichier != null)
				decoder = new XMLDecoder(new FileInputStream(nomFichier));
			else
				decoder = new XMLDecoder(urlFichier.openStream());
			contenuFichier = decoder.readObject();
			decoder.close();
		}
		catch (Exception e)
		{
			if (decoder != null) decoder.close();
			msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + e;
			return Constantes.FALSE;
		}

		return Constantes.OK;
	}

	/**
	 * Ecrit le fichier texte
	 * @return
	 */
	public int ecritureFichier()
	{
		XMLEncoder encoder = null;

		if (contenuFichier == null)
		{
			msgErreur = ERREUR_BUFFER_VIDE;
			return Constantes.ERREUR;
		}

		// On cr�� le fichier
		try
		{
			encoder = new XMLEncoder(new FileOutputStream(nomFichier));
			// serialisation de l'objet
			encoder.writeObject(contenuFichier);
			encoder.flush();
			encoder.close();
		}
		catch (Exception e) 
		{
			if (encoder != null) encoder.close();
			msgErreur = ERREUR_ENREGISTREMENT_FICHIER + Constantes.crlf + e;
			return Constantes.ERREUR;
		}

		return Constantes.OK;
	}
	
	/**
	 * Retourne les filtres possible pour les boites de dialogue
	 * @param jfc
	 */
	public void initFiltre(JFileChooser jfc)
	{
		jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_XML}, FILTER_DESC_XML));
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
