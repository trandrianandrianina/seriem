/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.util.HashMap;

/**
 * Description des informations sur le serveur NewSim pour le serveur S�rie M
 */
public class ServerEditionInfos {
  // Variables
  private String os; // Nom de l'os sur lequel s'�x�cute le serveur NewSim
  // D�but du chemin pour acc�der aux documents li�s (z:\, \\192.168.1.1\, ...) depuis le serveur NewSim
  private HashMap<String, String> racineIFSfromEditionSrv = new HashMap<String, String>();
  // D�but du chemin pour acc�der aux documents li�s (/documents/, ...) depuis le serveur S�rie M
  private HashMap<String, String> racineIFSfromSerieNSrv = new HashMap<String, String>();
  private char separateurPath = '/'; // S�parateur de dossier de l'os o� est install� le serveur NewSim
  private boolean onWindows = false; // Indique si le serveur s'execute sur un poste Windows
  
  /**
   * Retourne le chemin complet pour acc�der au dossier temporaire dans l'IFS (documents li�s) quelque soit l'endroit o� s'ex�cute le
   * serveur NewSim
   * @return
   */
  public String getPathDocTempoOnServerEdition(String bibfm) {
    if (racineIFSfromEditionSrv.size() == 0) {
      return "/sgm/tmp";
    }
    
    if (onWindows) {
      return racineIFSfromEditionSrv.get(bibfm) + separateurPath + ConstantesNewSim.DOSSIER_TMP;
    }
    return racineIFSfromSerieNSrv.get(bibfm) + separateurPath + ConstantesNewSim.DOSSIER_TMP;
  }
  
  /**
   * Converti un chemin Windows en chemin interne IFS (pour qu'il soit accessible au serveur S�rie N)
   * @param chemin
   * @return
   */
  public String convertPathForSerieNSrv(String bibfm, String chemin, boolean convertSeparateur) {
    // Tests de validit�
    if (chemin == null) {
      return null;
    }
    chemin = chemin.trim();
    if (chemin.length() <= 2) {
      return chemin;
    }
    
    // Transformation
    if (chemin.startsWith(racineIFSfromEditionSrv.get(bibfm))) {
      chemin = racineIFSfromSerieNSrv.get(bibfm) + separateurPath + chemin.substring(3);
    }
    
    if (convertSeparateur) {
      chemin = chemin.replace('/', '&').replace('\\', '&');
    }
    
    return chemin;
  }
  
  /**
   * Retourne si le serveur s'ex�cute sous Windows ou non
   * @return
   */
  public boolean isOnWindows() {
    return onWindows;
  }
  
  /**
   * @return the os
   */
  public String getOs() {
    return os;
  }
  
  /**
   * @param os the os to set
   */
  public void setOs(String os) {
    this.os = os;
    
    onWindows = this.os.toLowerCase().startsWith("windows");
    
    if (onWindows) {
      separateurPath = '\\';
    }
    else {
      separateurPath = '/';
    }
  }
  
  /**
   * @return the racineIFSfromEditionSrv
   */
  public String getRacineIFSfromEditionSrv(String bibfm) {
    return racineIFSfromEditionSrv.get(bibfm);
  }
  
  /**
   * Initialise la racine vers les documents li�s depuis serveur Newsim (sans s�parateur de fin) en fonction de l'OS
   * @param racineIFSfromEditionSrv the racineIFSfromEditionSrv to set
   */
  public void setRacineIFSfromEditionSrv(String bibfm, String aracineIFSfromEditionSrv) {
    if (aracineIFSfromEditionSrv == null) {
      return;
    }
    aracineIFSfromEditionSrv = aracineIFSfromEditionSrv.trim();
    
    // On supprime le s�parateur � la fin
    if ((aracineIFSfromEditionSrv.charAt(aracineIFSfromEditionSrv.length() - 1) == '/')
        || (aracineIFSfromEditionSrv.charAt(aracineIFSfromEditionSrv.length() - 1) == '\\')) {
      aracineIFSfromEditionSrv = aracineIFSfromEditionSrv.substring(0, aracineIFSfromEditionSrv.length() - 1);
    }
    racineIFSfromEditionSrv.put(bibfm, aracineIFSfromEditionSrv);
  }
  
  /**
   * @return the racineIFSfromSerieNSrv
   */
  public String getRacineIFSfromSerieNSrv(String bibfm) {
    return racineIFSfromSerieNSrv.get(bibfm);
  }
  
  /**
   * Initialise la racine vers les documents li�s depuis serveur S�rie N (sans s�parateur de fin)
   * @param racineIFSfromSerieNSrv the racineIFSfromSerieNSrv to set
   */
  public void setRacineIFSfromSerieNSrv(String bibfm, String aracineIFSfromSerieNSrv) {
    if (aracineIFSfromSerieNSrv == null) {
      return;
    }
    aracineIFSfromSerieNSrv = aracineIFSfromSerieNSrv.trim();
    if (aracineIFSfromSerieNSrv.length() == 0) {
      return;
    }
    
    // On supprime le s�parateur � la fin
    if ((aracineIFSfromSerieNSrv.charAt(aracineIFSfromSerieNSrv.length() - 1) == '/')
        || (aracineIFSfromSerieNSrv.charAt(aracineIFSfromSerieNSrv.length() - 1) == '\\')) {
      aracineIFSfromSerieNSrv = aracineIFSfromSerieNSrv.substring(0, aracineIFSfromSerieNSrv.length() - 1);
    }
    racineIFSfromSerieNSrv.put(bibfm, aracineIFSfromSerieNSrv);
  }
  
  /**
   * Lib�re les ressources
   */
  public void dispose() {
    racineIFSfromEditionSrv.clear();
    racineIFSfromEditionSrv = null;
    racineIFSfromSerieNSrv.clear();
    racineIFSfromSerieNSrv = null;
  }
}
