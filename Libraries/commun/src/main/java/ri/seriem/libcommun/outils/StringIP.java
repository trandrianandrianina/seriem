//=================================================================================================
//==>                                                                       08/09/2009 - 08/09/2009
//==> Permet de mettre en forme une adresse IP
//=================================================================================================
package ri.seriem.libcommun.outils;

public class StringIP
{
	// Variables
	private						String						ip						= null;

	
	/**
	 * Constructeur
	 * @param aip
	 */
	public StringIP(String aip)
	{
		setIP(aip);
	}


	// -- M�thodes priv�es ----------------------------------------------------

	


	// -- M�thodes prot�g�es --------------------------------------------------

	


	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Retourne l'adresse IP sans les points sous forme: 192168001001
	 * @return
	 */
	public String getIPwoPoint()
	{
		String[] tab=null;
		StringBuffer ipwopoint=new StringBuffer();
		int lg=0;
		
		if (ip == null) return null;
		
		tab = Constantes.splitString(ip, '.');
		for (int i=0; i<tab.length; i++)
			if (tab[i].length() >= 3) ipwopoint.append(tab[i]);
			else
			{
				lg = 3 - tab[i].length();
				for (int j=0; j<lg; j++) ipwopoint.append('0');
				ipwopoint.append(tab[i]);
			}
		return ipwopoint.toString();
	}

	/**
	 * Lib�re les resources
	 */
	public void dispose(){}
	
	
 	// -- Accesseurs ----------------------------------------------------------


	/**
	 * Initialise l'adresse IP
	 * @param aip
	 */
	public void setIP(String aip)
	{
		ip = aip.substring(aip.indexOf("/") + 1);
	}
	
	/**
	 * Retourne l'adresse IP
	 * @return
	 */
	public String getIP()
	{
		return ip;
	}

}
