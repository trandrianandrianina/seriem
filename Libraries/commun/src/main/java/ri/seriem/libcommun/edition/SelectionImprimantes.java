/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.jdesktop.swingx.VerticalLayout;

/**
 * Affiche la liste des imprimantes connect�es au System I
 */
public class SelectionImprimantes extends JDialog {
  // Variables
  private GfxImprimantePanel[] listePanelImp = null;
  private ActionListener action = null;
  private boolean modeSelection = false;
  private ArrayList<Imprimante> listeImprimantes = null;
  
  /**
   * Constructeur
   * @param owner
   * @param alisteImprimantes
   * @param modeselection
   */
  public SelectionImprimantes(Frame owner, ArrayList<Imprimante> alisteImprimantes, boolean modeselection) {
    super(owner);
    modeSelection = modeselection;
    initComponents();
    listeImprimantes = alisteImprimantes;
    generepanel();
  }
  
  /**
   * Constructeur
   * @param owner
   * @param alisteImprimantes
   * @param modeselection
   */
  public SelectionImprimantes(Dialog owner, ArrayList<Imprimante> alisteImprimantes, boolean modeselection) {
    super(owner);
    modeSelection = modeselection;
    initComponents();
    listeImprimantes = alisteImprimantes;
    generepanel();
  }
  
  /**
   * Initialise le panel principal avec la liste des imprimantes et en fonction du mode
   */
  private void generepanel() {
    if (modeSelection) {
      setTitle("S�lection des imprimantes li�es au serveur");
    }
    else {
      setTitle("S�lection de l'imprimante li�e au serveur");
    }
    
    if (listeImprimantes == null) {
      return;
    }
    
    // On g�n�re l'action � effectuer lors de la s�lection d'une imprimante
    action = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    };
    
    listePanelImp = new GfxImprimantePanel[listeImprimantes.size()];
    for (int i = 0; i < listeImprimantes.size(); i++) {
      listePanelImp[i] = new GfxImprimantePanel(listeImprimantes.get(i), modeSelection);
      listePanelImp[i].setAction(action);
      p_ListeImprimantes.add(listePanelImp[i]);
    }
    
    validate();
    repaint();
  }
  
  public void setAlreadySelectionned(ArrayList<String> listeimprimantesactuelles) {
    if (listeimprimantesactuelles == null) {
      return;
    }
    
    for (int i = 0; i < listeImprimantes.size(); i++) {
      for (int j = 0; j < listeimprimantesactuelles.size(); j++) {
        if (listeImprimantes.get(i).getNom().equals(listeimprimantesactuelles.get(j))) {
          listePanelImp[i].setSelectionned(true);
          listeImprimantes.get(i).setSelection(true);
        }
      }
    }
  }
  
  /**
   * Gestion du filtre pour la recherche d'une imprimante
   */
  private void filtrepanel(String filtre) {
    if ((filtre == null) || (listePanelImp == null)) {
      return;
    }
    
    // Pas de filtre, on affiche tout
    if (filtre.length() == 0) {
      for (int i = 0; i < listePanelImp.length; i++) {
        listePanelImp[i].setVisible(true);
      }
    }
    // Sinon on affiche les imprimantes qui commence par le filtre
    else {
      for (int i = 0; i < listePanelImp.length; i++) {
        listePanelImp[i].setVisible(listePanelImp[i].getImprimante().getNom().startsWith(filtre));
      }
    }
    
    validate();
    repaint();
  }
  
  private void valideSelection() {
    for (int i = 0; i < listeImprimantes.size(); i++) {
      listeImprimantes.get(i).setSelection(listePanelImp[i].isSelectionned());
    }
  }
  
  private void cancelButtonActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void tf_NomImprimanteCaretUpdate(CaretEvent e) {
    filtrepanel(tf_NomImprimante.getText().toUpperCase());
  }
  
  private void bt_EnregistrerActionPerformed(ActionEvent e) {
    valideSelection();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    l_NomImprimante = new JLabel();
    tf_NomImprimante = new JTextField();
    scrp_ListeImprimantes = new JScrollPane();
    p_ListeImprimantes = new JPanel();
    buttonBar = new JPanel();
    bt_Enregistrer = new JButton();
    cancelButton = new JButton();
    
    // ======== this ========
    setTitle("S\u00e9lection d'une imprimante li\u00e9e au serveur");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setMinimumSize(new Dimension(380, 400));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setName("dialogPane");
      dialogPane.setLayout(new BorderLayout());
      
      // ======== contentPanel ========
      {
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(new BorderLayout());
        
        // ======== panel1 ========
        {
          panel1.setName("panel1");
          panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
          
          // ---- l_NomImprimante ----
          l_NomImprimante.setText("Nom de l'imprimante");
          l_NomImprimante.setName("l_NomImprimante");
          panel1.add(l_NomImprimante);
          
          // ---- tf_NomImprimante ----
          tf_NomImprimante.setPreferredSize(new Dimension(115, 28));
          tf_NomImprimante.setName("tf_NomImprimante");
          tf_NomImprimante.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent e) {
              tf_NomImprimanteCaretUpdate(e);
            }
          });
          panel1.add(tf_NomImprimante);
        }
        contentPanel.add(panel1, BorderLayout.NORTH);
        
        // ======== scrp_ListeImprimantes ========
        {
          scrp_ListeImprimantes.setName("scrp_ListeImprimantes");
          
          // ======== p_ListeImprimantes ========
          {
            p_ListeImprimantes.setName("p_ListeImprimantes");
            p_ListeImprimantes.setLayout(new VerticalLayout());
          }
          scrp_ListeImprimantes.setViewportView(p_ListeImprimantes);
        }
        contentPanel.add(scrp_ListeImprimantes, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      
      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setName("buttonBar");
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 85, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0 };
        
        // ---- bt_Enregistrer ----
        bt_Enregistrer.setVisible(modeSelection);
        bt_Enregistrer.setText("Enregistrer");
        bt_Enregistrer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Enregistrer.setName("bt_Enregistrer");
        bt_Enregistrer.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_EnregistrerActionPerformed(e);
          }
        });
        buttonBar.add(bt_Enregistrer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cancelButton ----
        cancelButton.setText("Fermer");
        cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cancelButton.setName("cancelButton");
        cancelButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            cancelButtonActionPerformed(e);
          }
        });
        buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel l_NomImprimante;
  private JTextField tf_NomImprimante;
  private JScrollPane scrp_ListeImprimantes;
  private JPanel p_ListeImprimantes;
  private JPanel buttonBar;
  private JButton bt_Enregistrer;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
