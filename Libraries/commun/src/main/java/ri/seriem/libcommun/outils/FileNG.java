//=================================================================================================
//==>                                                                       24/06/2001 - 09/06/2015
//==> Routines concernant les fichiers & dossiers
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class FileNG extends File
{
	private static final long serialVersionUID = 1L;
	
	// Variable
	private String extension=null;	// ne contient pas le point
	private String namewoextension=null;	// ne contient pas le point
	

	/**
	 * Constructeur
	 * @param fichier
	 */
	public FileNG(String fichier)
    {
        super(fichier);
    }

	/**
	 * Constructeur
	 * @param fichier
	 */
	public FileNG(URI fichier)
    {
        super(fichier);
    }

	/**
	 * Copie le fichier vers le chemin sp�cifi�
	 * @param chemincomplet avec le nom du fichier destination
	 * @return
	 * @throws IOException
	 */
    public boolean copyTo(String chemincomplet)
    {
    	if (chemincomplet == null) return false;
    	
        byte[] bytes=null;
        RandomAccessFile source=null, destination=null;
        
        try        
        {
            // On teste si le fichier existe et si on a les droits de lecture
            source = new RandomAccessFile(this, "r");

            File dest_file = new File(chemincomplet);
            if (!dest_file.exists())
                dest_file.mkdirs();
            //dest_file = new File(chemin+File.separator+this.getName());
            dest_file = new File(chemincomplet);
            if (dest_file.exists())
                dest_file.delete();
          
             // On effectue la copie      
             bytes = new byte[(int)source.length()];
             destination = new RandomAccessFile(dest_file, "rw");
             source.readFully(bytes);
             source.close();
             destination.write(bytes);
             destination.close();
         }
         catch (Exception e) 
         {
        	 try
        	 {
        		 if (source != null) source.close();
        		 if (destination != null) destination.close();
        	 }
        	 catch(IOException ioe) { }
             return false;
         }
         
        return true;
    }
    
	/**
	 * Retourne l'extension du fichier avec ou sans point, blanc sinon
	 * @return
	 */
	public String getExtension(boolean point)
	{
		if (isDirectory()) return "";
		
		if (extension == null)
		{
			// On recherche le point
			final String nom = getName();
			int pos = nom.lastIndexOf('.');
			if (pos == -1)
				extension = "";
			else
				extension = nom.substring(pos+1);
		}
		
		// On retourne l'extension
		if (point)
			return '.' + extension;
		
		return extension;
	}

	/**
	 * Retourne le nom sans l'extension
	 * @return le namewoextension
	 */
	public String getNamewoExtension(boolean point)
	{
		if ( namewoextension == null ){
			// On recherche le point
			final String nom = getName();
			int pos = nom.lastIndexOf('.');
			if( pos == -1 )
				namewoextension = nom;
			else
				namewoextension = nom.substring(0, pos);
		}
		// On retourne le nom sans extension
		if( point && !isDirectory() )
			return namewoextension + '.';

		return namewoextension;
	}

	/**
	 * Supprime le fichier, le dossier et son contenu
	 *
	 */
	public void remove()
	{
		remove(this.getAbsoluteFile());
	}

	/**
	 * Suppression r�cursive d'un dossier
	 * @param file
	 *
	public void removeFolder(File file)
	{
		if (file.isFile())
			file.delete();
		else
			if (file.isDirectory())
			{
				File[] listing = file.listFiles();
				for (int i=0; i<listing.length; i++)
					if (listing[i].isDirectory())
						removeFolder(listing[i]);
					else
						listing[i].delete();
				file.delete();
			}
	}*/

	/**
	 * Suppression r�cursive d'un dossier ou un fichier
	 * @param file
	 */
	public static void remove(File file)
	{
		
		if (file.isFile())
			file.delete();
		else
			if (file.isDirectory())
			{
				File[] listing = file.listFiles();
				for (int i=0; i<listing.length; i++)
					if (listing[i].isDirectory())
						remove(listing[i]);
					else
						listing[i].delete();
				file.delete();
			}
	}

	/**
	 * Suppression r�cursive du contenu d'un dossier
	 * @param file
	 */
	public static void removeContents(File file)
	{
		
		if (file.isFile())
			file.delete();
		else
			if (file.isDirectory())
			{
				File[] listing = file.listFiles();
				for (int i=0; i<listing.length; i++)
					if (listing[i].isDirectory())
						remove(listing[i]);
					else
						listing[i].delete();
			}
	}

	/**
	 * Liste tous les fichiers d'un r�pertoire de mani�re r�cursive
	 * @param repertoire
	 * @param tousLesFichiers
	 * @return
	 */
	public List<File> listeFichier(ArrayList<File> tousLesFichiers)
	{
		if (tousLesFichiers == null) tousLesFichiers = new ArrayList<File>();
		if (exists())
			if (isDirectory())
			{
				final File[] list = listFiles();
				if (list != null)
				{
					for (int i=0; i < list.length; i++)
					{
						if(list[i].isDirectory())
							listeFichier(list[i], tousLesFichiers);
						else
							tousLesFichiers.add(list[i]);
					}
				}
			}
		return tousLesFichiers;
	}

	/**
	 * Liste tous les fichiers d'un r�pertoire de mani�re r�cursive
	 * @param repertoire
	 * @param tousLesFichiers
	 * @return
	 */
	public static List<File> listeFichier(File repertoire, ArrayList<File> tousLesFichiers)
	{
		if (tousLesFichiers == null) tousLesFichiers = new ArrayList<File>();
		if (repertoire.exists())
			if (repertoire.isDirectory())
			{
				final File[] list = repertoire.listFiles();
				if (list != null)
				{
					for (int i=0; i < list.length; i++)
					{
						if(list[i].isDirectory())
							listeFichier(list[i], tousLesFichiers);
						else
							tousLesFichiers.add(list[i]);
					}
				}
			}
		return tousLesFichiers;
	}

	/**
	 * <p>Cr�er un <tt>FilenameFilter</tt> filtrant uniquement les fichiers dont
	 * le nom se termine par <tt>extension</tt>.
	 */
	public static FilenameFilter avecExtension(final String extension)
	{
		if (extension == null) return null;
		else
			return new FilenameFilter()
			{
				public boolean accept(File repertoire, String nom)
				{
					return nom.endsWith(extension);
				}
			};
	}

	/**
	 * Filtre tous les fichiers d'un r�pertoire de mani�re r�cursive
	 * @param tousLesFichiers
	 * @param filtre
	 * @return
	 */
	public List<File> filtreFichier(ArrayList<File> tousLesFichiers, FilenameFilter filtre)
	{
		if (tousLesFichiers == null) tousLesFichiers = new ArrayList<File>();
		if (exists())
			if (isDirectory())
			{
				final File[] list = listFiles();
				if (list != null)
				{
					for (int i=0; i < list.length; i++)
					{
						if(list[i].isDirectory())
							filtreFichier(list[i], tousLesFichiers, filtre);
						else
							if (filtre.accept(list[i], list[i].getName()))
								tousLesFichiers.add(list[i]);
					}
				}
			}
		return tousLesFichiers;
	}

	/**
	 * Filtre tous les fichiers d'un r�pertoire de mani�re r�cursive
	 * @param repertoire
	 * @param tousLesFichiers
	 * @param filtre
	 * @return
	 */
	public static List<File> filtreFichier(File repertoire, ArrayList<File> tousLesFichiers, FilenameFilter filtre)
	{
		if (tousLesFichiers == null) tousLesFichiers = new ArrayList<File>();
		if (repertoire.exists())
			if (repertoire.isDirectory())
			{
				final File[] list = repertoire.listFiles();
				if (list != null)
				{
					for (int i=0; i < list.length; i++)
					{
						if(list[i].isDirectory())
							filtreFichier(list[i], tousLesFichiers, filtre);
						else
							if (filtre.accept(list[i], list[i].getName()))
								tousLesFichiers.add(list[i]);
					}
				}
			}
		return tousLesFichiers;
	}

	/**
	 * Nettoie le nom du dossier (suppression du s�parateur � la fin, ...)
	 * @param folder
	 * @return
	 */
	public static String cleanNameFolder(String folder)
	{
		if( ( folder == null ) || ( folder.trim().equals("") ) ){
			return folder;
		}
		
		folder = folder.trim();
		int pos = folder.lastIndexOf( File.separatorChar );
		if( pos == folder.length()-1 ){
			folder = folder.substring(0, pos);
		}
		
		return folder;
	}

	// Permet de copier un repertoire en passant en param�tre le repertoire source vers celui de destination
	
	public static void copyDirectory(final File from, final File to) throws IOException {
		if (! to.exists()) {
			to.mkdir();
		}
		final File[] inDir = from.listFiles();
		for (int i = 0; i < inDir.length; i++) {
			final File file = inDir[i];
			copy(file, new File(to, file.getName()));
			
		}
	}
	
	/*//Copie de fichiers en pr�cisant la taille
	public static void copy(final InputStream inStream, final OutputStream outStream, final int bufferSize) throws IOException
	{
		final byte[] buffer = new byte[bufferSize];
		int nbRead;
		while ((nbRead = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, nbRead);
			
		}
	}*/

	//Copie de fichiers 
		public static void copy(final FileInputStream inStream, final FileOutputStream outStream) throws IOException
		{
			//final byte[] buffer = new byte[bufferSize];
			int nbRead;
			while ((nbRead = inStream.read()) != -1) {
				outStream.write(nbRead);
				
			}
		}
	
	public static void copyFile(final File from, final File to) throws IOException {
		final FileInputStream inStream = new FileInputStream(from);
		final FileOutputStream outStream = new FileOutputStream(to);
		copy(inStream, outStream); //(int) Math.min(from.length(), 4*1024));
		
		inStream.close();
		outStream.close();
	}
	
	
	public static void copy(final File from, final File to) throws IOException {
		if (from.isFile()) {
			copyFile(from, to);
		} else if (from.isDirectory()){
			copyDirectory(from, to);
		} else {
			throw new FileNotFoundException(from.toString() + " does not exist" );
		}
	} 
}

