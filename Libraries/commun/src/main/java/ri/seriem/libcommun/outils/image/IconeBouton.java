/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils.image;

import javax.swing.ImageIcon;

import ri.seriem.libcommun.outils.Trace;

/**
 * Ic�ne destin�e aux boutons, disponible dans quatres variantes.
 * 
 * Cette classe permet de charger et m�moriser les ic�nes affich�es dans les boutons. Les ic�nes sont charg�es uniquement la premi�re fois
 * qu'elles sont utilis�s. Ensuite, c'est la version en m�moire qui est utilis�e.
 * 
 * Chaque ic�ne existe en 4 versions :
 * - la version standard utilis�e lorsque l'ic�ne est active.
 * - la version gris�e lorsque l'ic�ne est inactive.
 * - la version sombre utilis�e lorsque le pointeur de la souris passe au dessus d'une ic�ne active.
 * - la version claire utilis�e pour mettre l'ic�ne en surbrillance lorsqu'on clique dessus.
 * 
 * Seules la version standard est r�cup�r�e � partir des ressources. Les autres versions sont d�termin�es automatiquement � partir de
 * la version standard.
 */
public class IconeBouton {
  private static float POURCENTAGE_ASSOMBRISSEMENT = 0.15f;
  private static float POURCENTAGE_ECLAIRCISSEMENT = 0.25f;
  
  private String ressource = null;
  private ImageIcon iconeStandard = null;
  private ImageIcon iconeGrise = null;
  private ImageIcon iconeSombre = null;
  private ImageIcon iconeClaire = null;
  
  /**
   * Constructeur.
   * @param pResource Nom de la resource contentant l'ic�ne standard.
   */
  public IconeBouton(String pResource) {
    ressource = pResource;
  }
  
  /**
   * Ic�ne standard utilis�e lorsque l'ic�ne est active.
   * @return Icone.
   */
  public ImageIcon getIconeStandard() {
    // Tester si l'ic�ne existe d�j�
    if (iconeStandard == null && ressource != null) {
      try {
        // Charger l'ic�ne standard � partir des ressources
        iconeStandard = new ImageIcon(IconeBouton.class.getClassLoader().getResource(ressource));
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors du chargement de l'ic�ne : " + ressource);
      }
    }
    
    return iconeStandard;
  }
  
  /**
   * Ic�ne gris�e lorsque l'ic�ne est inactive.
   * @return Icone.
   */
  public ImageIcon getIconeGrisee() {
    // Tester si l'ic�ne existe d�j�
    if (iconeGrise == null) {
      // Cr�er l'icone gris�e
      iconeGrise = OutilImage.griserIcone(getIconeStandard());
    }
    
    return iconeGrise;
  }
  
  /**
   * Ic�ne sombre utilis�e lorsque le pointeur de la souris passe au dessus d'une ic�ne active.
   * @return Icone.
   */
  public ImageIcon getIconeSombre() {
    // Tester si l'ic�ne existe d�j�
    if (iconeSombre == null) {
      // Cr�er l'icone sombre
      iconeSombre = OutilImage.assombrirIcone(getIconeStandard(), POURCENTAGE_ASSOMBRISSEMENT);
    }
    
    return iconeSombre;
  }
  
  /**
   * Ic�ne en surbrillance utilis�e pour mettre l'ic�ne en surbrillance lorsqu'on clique dessus.
   * @return Icone.
   */
  public ImageIcon getIconeClaire() {
    // Tester si l'ic�ne existe d�j�
    if (iconeClaire == null) {
      // Cr�er l'icone claire
      iconeClaire = OutilImage.eclaircirIcone(getIconeStandard(), POURCENTAGE_ECLAIRCISSEMENT);
    }
    
    return iconeClaire;
  }
}
