//=================================================================================================
//==>                                                                       16/11/2015 - 01/02/2016
//==> Gestion pour le client d'une connexion socket
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils.communication;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.StringIP;

public class LineClient extends LineBase
{
	// Constantes
	private		final static	String						NOM_CLASSE				= "[LineClient]";

	// Variables
	private						LineClient					thisLine				= this;

	
	/**
	 * Constructeur
	 * @param alistEvents
	 */
	public LineClient(LinkedHashMapManager alistEvents)
	{
		super(alistEvents);
	}


	// -- M�thodes priv�es ----------------------------------------------------


	/**
	 * D�marre l'�coute de la socket
	 */
	private boolean startListen()
	{
		if( !socket.isConnected() ){
			msgError += "\nL'�coute de la socket n'est pas possible car il n'y a pas de connexion.";
			return false;
		}
		
		new Thread(){
			public void run()
			{
				int idgroup = -1;
				long idsession = -1;
				
				while( state > STATE_CLOSED ){
					try{
						ByteBuffer bbuffer = receiveBuffer();
						if( bbuffer == null ) continue;
//System.out.println("-------------------------- RECEPTION BUFFER ----------------------------------");						
						
						idgroup = bbuffer.getInt();
						idsession = bbuffer.getLong();
//System.out.println("--> idgroup:"+idgroup+" idsession:" + idsession + " (" + bbuffer.capacity()+")");						
						if( idsession == IDSESSION_CTRL ){
							controlLine(bbuffer, true);
						} else{
							listSessions.get(idsession).addBufferReceived(bbuffer);
						}
					}
					catch(Exception e){
						e.printStackTrace();
						state = STATE_OUTOFORDER;
					}
				}
//System.out.println("On sort de startListen");
				// On ferme la connexion
				close();
//System.out.println("state == " + state);
				// On teste si c'est une fermeture propre ou non (dans le cas contraire une action sera men�e)
				if( state == STATE_OUTOFORDER){
					if( listEvents != null )
						listEvents.addObject(thisLine, DO_RECONNEXION);
				}
			}

			/**
			 * Re�oit des donn�es et les stocke dans un buffer  
			 * @param in
			 * @return
			 */
			private ByteBuffer receiveBuffer()
			{
				if( from == null ){
					return null;
				}
				
				int octet_alire = 0, octet_lus = 0, compteur = 0;
				byte buffercomplet[] = null;

				// On r�cup�re tous les octets envoy�s par le client
				try
				{
		//System.out.println("-receiveBuffer->Attente de donn�es");	
					int type = from.readInt();
					state = STATE_BUSY; //<-- Attention � son positionnement car �a peut bloquer la ligne
					switch(type){
						case TYPE_SESSION:
							octet_alire = from.readInt();
							// System.out.println("-octet:" + octet_alire);
							buffercomplet = new byte[octet_alire];
		//System.out.println("-octet:" + octet_alire);
							if (octet_alire > 0)
							{
								do
								{
									octet_lus = from.read(buffercomplet, compteur, octet_alire);
									compteur += octet_lus;
									octet_alire -= octet_lus;
								}
								while (octet_alire != 0);
							}
							break;
					}
				}
				catch (Exception e)
				{
					e.getStackTrace();
					msgError += "\nNOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e";
					state = STATE_OUTOFORDER;
					return null;
				}
				state = STATE_READY;
				return ByteBuffer.wrap(buffercomplet);
			}

		}.start();
		
		return true;
	}


	// -- M�thodes prot�g�es --------------------------------------------------

	
	

	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Connexion de la ligne au serveur
	 * @return
	 */
	public boolean open()
	{
		if( state >= STATE_READY ){
			msgError += "\nConnexion d�j� ouverte.";
			return true;
		}
		// Connexion au serveur
		try
		{
			socket = new Socket(host, port);
			socket.setSoTimeout(Constantes.SOCKET_TIMEOUT);
			socket.setTcpNoDelay(true);	// Pour �viter le d�lai lors des d�connexions
			socket.setKeepAlive(true);
			
			setIpsocket( new StringIP(socket.getLocalAddress().toString()) );
			
			// Cr�ation des �couteurs
			from = new DataInputStream(new BufferedInputStream(socket.getInputStream(), Constantes.TAILLE_BUFFER));
			to = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream(), Constantes.TAILLE_BUFFER));
			
			if( startListen() )
				state = STATE_READY;
			else{
				state = STATE_OUTOFORDER;
				return false;
			}
		}
		catch (IOException ioe)
		{
			state = STATE_OUTOFORDER;
			msgError += '\n' + NOM_CLASSE + " Erreur lors de la connexion au serveur:\n" + ioe;
			return false;
		}
		return true;
	}

	/**
	 * D�connexion de la ligne
	 * @return
	 */
	public boolean disconnect()
	{
		if( !super.disconnect() )
			return false;
		
		BufferManager buffer = new BufferManager();
		//buffer.setIdsession(IDGROUP_CTRL);
		buffer.setIdsession(IDSESSION_CTRL);
		buffer.setIdmessage(MSG_DISCONNECT);
		send(buffer, false);
		
		return true;
	}

	
	/**
	 * Lib�re les ressources
	 */
 	public void dispose()
	{
 		super.dispose();
 		thisLine = null;
	}


 	// -- Accesseurs ----------------------------------------------------------


}
