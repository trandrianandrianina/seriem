//=================================================================================================
//==> Gestion des fichiers de description CSV                               27/10/2006 - 04/03/2014
//==> G�re les fichiers de type CSV
//==> A faire:
//==>    Am�liorer la sortie HTML avec gestion des caract�res accentu�s (UTF-8)
//==>    Variabiliser les couleurs de l'ent�te des colonnes
//==>    Rajouter le type TXT (champs s�par�s par des tabulations)
//=================================================================================================
package ri.seriem.libcommun.outils;

//                                  ATTENTION LES CLES SONT SENSIBLES A LA CASSE
import java.util.ArrayList;

/*import javax.swing.JFileChooser;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
*/

public class GestionFichierCSV extends GestionFichierTexte 
{
    //private final static String NOM_CLASSE="[GestionFichierCSV]";
    
    // Constantes
    public final static String FILTER_EXT_CSV="csv";
    public final static String FILTER_DESC_CSV="CSV (s�parateur: point-virgule) (*.csv)";
    public final static String FILTER_EXT_XLS="xls";
    public final static String FILTER_DESC_XLS="Fichier Microsoft Excel (*.xls)";
    public final static String FILTER_EXT_HTM="htm";
    public final static String FILTER_DESC_HTM="Page web (*.htm)";

    /**
     * Constructeur de la classe
     * @param fchcsv
     */
    public GestionFichierCSV(String fchcsv)
    {
        setNomFichier(fchcsv);
    }

    /**
     * Ajoute une ligne du fichier
     * @param ligne
     */
    public void add(String ligne)
    {
    	if (contenuFichier == null)
    		contenuFichier = new ArrayList<String>(Constantes.MAXVAR);
    	
    	contenuFichier.add(ligne); 
    }

    /**
     * Convertit le fichier CSV en HTM (sans enregistrement du fichier � la fin)
     * @return
     */
    public int CSVtoHTM()
    {
    	if (contenuFichier == null) 
    		if (lectureFichier() == Constantes.ERREUR) return Constantes.ERREUR;
    	
    	// R�cup�ration juste du nom du fichier
    	String nomfichier = getNomFichier().substring(getNomFichier().lastIndexOf('_') + 1, getNomFichier().lastIndexOf('.'));

    	boolean isGuillemet=true;
    	String chaine=null;
    	String separateur="\";\"";
    	
    	ArrayList<String> listeLigneHTM=new ArrayList<String>(Constantes.MAXVAR);
    	// Ecriture de l'ent�te du fichier System.getProperty("file.encoding")
    	//listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset="+Constantes.CODEPAGE+"\">");
    	listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset="+System.getProperty("file.encoding")+"\">");
//        listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">");
//        listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html;CHARSET=iso-8859-1\">");
        listeLigneHTM.add("<TITLE><B>" + nomfichier + "</B></TITLE>");
        listeLigneHTM.add("<BODY LANG=\"fr-FR\" DIR=\"LTR\"><TABLE BORDER=1 CELLPADDING=2 CELLSPACING=0>");

        // Avant de commencer le traitement des lignes
        if (contenuFichier.size() != 0)
        {
        	// Test pour savoir si le s�parateur est la virgule ou le point-virgule (avec ou sans guillemet)
        	isGuillemet = (((String)contenuFichier.get(0)).indexOf("\"") != -1);
        	if (!isGuillemet)
        	{
        		separateur = ";";
        		if (((String)contenuFichier.get(0)).indexOf(",") != -1)
        			separateur = ",";
        	}
        	else
        		if (((String)contenuFichier.get(0)).indexOf("\",\"") != -1)
        			separateur = "\",\"";
        	
        	// On arrange l'ent�te des colonnes
        	listeLigneHTM.add("<TR>");
        	chaine = ((String)contenuFichier.get(0)).replaceAll(separateur, "</FONT></TD><TD BGCOLOR=\"#0066FF\"><FONT COLOR=\"#FFFF99\">").trim();
        	if ((chaine.charAt(0) == '"') && (chaine.charAt(chaine.length()-1) == '"'))
        		chaine = chaine.substring(1, chaine.length()-1);
        	listeLigneHTM.add("<TD BGCOLOR=\"#0066FF\"><FONT COLOR=\"#FFFF99\">" + chaine + "</FONT></TD>");
            listeLigneHTM.add("</TR>");
        }
        
        // D�tail des lignes
        for (int i=1; i<contenuFichier.size(); i++)
        {
        	listeLigneHTM.add("<TR>");
        	chaine = ((String)contenuFichier.get(i)).replaceAll(separateur, "</TD><TD>").trim(); 
        	if ((chaine.charAt(0) == '"') && (chaine.charAt(chaine.length()-1) == '"'))
        		chaine = chaine.substring(1, chaine.length()-1);
        	listeLigneHTM.add("<TD>" + chaine + "</TD>");
        	listeLigneHTM.add("</TR>");
        }
        
        // Fin de fichier
        listeLigneHTM.add("</TABLE><P><BR><BR></P></BODY></HTML>");
        contenuFichier = listeLigneHTM;
        
        return Constantes.OK;
    }

    /**
     * Convertit le fichier CSV en XLS (avec enregistrement du fichier � la fin)
     * TODO A revoir car l'algo est pourrave
     * @return
     */
    public int CSVtoXLS()
    {
    	
    	/*if (contenuFichier == null) 
    		if (lectureFichier() == Constantes.ERREUR) return Constantes.ERREUR;
    	
    	// R�cup�ration juste du nom du fichier
    	//String nomfichier = getNomFichier().substring(getNomFichier().lastIndexOf('_') + 1, getNomFichier().lastIndexOf('.'));
    	//System.out.println("--> " + nomfichier);

    	boolean isGuillemet=true;
    	String chaine=null;
    	String separateur="\";\"";
    	
        // Avant de commencer le traitement des lignes
        if (contenuFichier.size() != 0)
        {
        	// Test pour savoir si le s�parateur est la virgule ou le point-virgule (avec ou sans guillemet)
        	isGuillemet = (((String)contenuFichier.get(0)).indexOf("\"") != -1);
        	if (!isGuillemet)
        	{
        		separateur = ";";
        		if (((String)contenuFichier.get(0)).indexOf(",") != -1)
        			separateur = ",";
        	}
        	else
        		if (((String)contenuFichier.get(0)).indexOf("\",\"") != -1)
        			separateur = "\",\"";
        }*/
        
    /*    // D�tail des lignes
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("new sheet");
		for(int k=0; k<contenuFichier.size(); k++)
		{
			String ligne = contenuFichier.get(k);
			// On supprime les guillemets de d�but et fin de chaine qui restent apr�s le split
			if (ligne.length() > 2)
				if ((ligne.charAt(0) == '"') && (ligne.charAt(ligne.length()-1) == '"'))
					ligne = ligne.substring(1, ligne.length()-1);
				
			String[] dataligne = ligne.split(separateur);
			HSSFRow row = sheet.createRow((short) 0+k);
			for(int p=0; p<dataligne.length; p++)
			{
				HSSFCell cell = row.createCell(p);
				chaine = dataligne[p].trim();
				// On teste si c'est une valeur num�rique
				if (chaine.matches("(\\+|-)?(\\d*|0)")) // Entier
				{
					cell.setCellType(Cell.CELL_TYPE_NUMERIC);
					if (chaine.equals(""))
						cell.setCellValue("");
					else
						try
						{
							cell.setCellValue(Integer.parseInt(chaine));
						}
						catch(Exception e)
						{
							cell.setCellValue(chaine);
						}
				}
				else // D�cimal
					if (chaine.matches("(\\+|-)?(\\d*\\,\\d+|\\d+\\,\\d*)((e|E)(\\+|-){0,1}\\d+){0,1}"))
					{
						cell.setCellType(Cell.CELL_TYPE_NUMERIC);
						if (chaine.equals(""))
							cell.setCellValue("");
						else
							try
							{
								cell.setCellValue(Double.parseDouble(chaine.replace(',','.')));
							}
							catch(Exception e)
							{
								cell.setCellValue(chaine);
							}
					}
					else // String
					{
						cell.setCellType(Cell.CELL_TYPE_STRING);
						cell.setCellValue(chaine);
					}
			}
		}
        
		// Construction du nom du fichier sortant
		String nomfichier = getNomFichier();
		int pos = nomfichier.lastIndexOf('.');
		if (pos != -1)
			nomfichier = nomfichier.substring(0, pos) + ".xls";
		else
			nomfichier += ".xls";
		setNomFichier(nomfichier);

		// Enregistrement du fichier xls
		try
		{
			FileOutputStream fileOut = new FileOutputStream(nomfichier);
			hwb.write(fileOut);
			fileOut.close();
		}
		catch(Exception e)
		{
			return Constantes.ERREUR;
		}*/
        
        return Constantes.OK;
    }

   /* *//**
     * Retourne les filtres possible pour les boites de dialogue
     * @param jfc
     *//*
    public void initFiltre(JFileChooser jfc)
    {
        jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_CSV}, FILTER_DESC_CSV));
        jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_HTM}, FILTER_DESC_HTM));
        jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_XLS}, FILTER_DESC_XLS));
    }*/
    
}
    