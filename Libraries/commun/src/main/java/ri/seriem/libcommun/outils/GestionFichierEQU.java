//=================================================================================================
//==> Gestion des fichiers de description EQU                               27/10/2006 - 23/11/2009
//==> G�re les fichiers de type EQU (Clef<tabulation>Valeur)
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

//                                  ATTENTION LES CLES SONT SENSIBLES A LA CASSE
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.swing.JFileChooser;


public class GestionFichierEQU extends GestionFichierTexte
{
    //private final static String NOM_CLASSE="[GestionFichierEQU]";
    
    // Constantes
	private final static String FILTER_EXT_EQU="equ";
	private final static String FILTER_DESC_EQU="Fichier EQU (*.equ)";


    /**
     * Constructeur de la classe
     * @param fchequ
     */
    public GestionFichierEQU(String fchequ)
    {
        super(fchequ);
    }

    /**
     * Initialise la liste des �quivalences
     * @param liste
     */
    public void setListe(HashMap<String, String> lstequ)
    {
        if (lstequ == null) return;
        
        ArrayList<String> liste = new ArrayList<String>(lstequ.size());
        Set<Entry<String,String>> donnee = lstequ.entrySet();
        Iterator<Entry<String, String>> it = donnee.iterator();
        while (it.hasNext()) 
        {
            Map.Entry<String, String> e = it.next();
            liste.add(e.getKey() + Constantes.SEPARATEUR + ((String)e.getValue()).replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR));
        }
        super.setContenuFichier(liste);
    }

    /**
     * Retourne la liste des equivalences
     * @return
     */
    public HashMap<String, String> getListe(boolean trfpath)
    {
    	String chaine=null;
    	String[] tabchaine=null;
        HashMap<String, String> listeEqu=new HashMap<String, String>();
    	ArrayList<String> liste = super.getContenuFichier();
    	
    	if (trfpath)
    		for (int i=0; i<liste.size(); i++)
    		{
    			chaine = Constantes.code2unicode((String) liste.get(i));
    			tabchaine = chaine.split(Constantes.SEPARATEUR);
    			listeEqu.put(tabchaine[0], tabchaine[1].replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    		}
    	else
    		for (int i=0; i<liste.size(); i++)
    		{
    			chaine = Constantes.code2unicode((String) liste.get(i));
    			tabchaine = chaine.split(Constantes.SEPARATEUR);
    			listeEqu.put(tabchaine[0], tabchaine[1]);
    		}

        return listeEqu; 
    }

	/**
	 * Retourne les filtres possible pour les boites de dialogue
	 * @param jfc
	 */
	public void initFiltre(JFileChooser jfc)
	{
		super.initFiltre(jfc);
		jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_EQU}, FILTER_DESC_EQU));
	}


}
    