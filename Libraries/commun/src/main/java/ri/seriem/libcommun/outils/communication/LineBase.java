//=================================================================================================
//==>                                                                       30/10/2015 - 04/02/2016
//==> Gestion de base d'une connexion socket
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils.communication;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.Session;
import ri.seriem.libcommun.outils.StringIP;

public abstract class LineBase
{
	// Constantes
	private		final static	String						NOM_CLASSE				= "[LineBase]";

	protected	final static	int							DELAY_PING				= 10 * 60 * 1000; // D�lai d'attente pour le d�clenchement d'un ping qui va g�n�rer du trafic sur le r�seau
	
	public		final static	int							IDSESSION_CTRL			= 0;
	public		final static	int							TYPE_SESSION			= 0;
	public		final static	int							DO_RECONNEXION			= 1;
	public		final static	int							MSG_DISCONNECT			= -9;
	public		final static	int							POS_FIRST				= 0;
	
	public		final static	int							STATE_TOCLOSE			= -2;
	public		final static	int							STATE_OUTOFORDER		= -1;
	public		final static	int							STATE_CLOSED			= 0;
	public		final static	int							STATE_READY				= 1;
	public		final static	int							STATE_BUSY				= 2;


	// Variables
	public		static		ConcurrentHashMap<Long, Session>listSessions			= null; 

	protected					String						host					= "127.0.0.1";
	protected					int							port					= Constantes.SERVER_PORT;
	protected					int							state 					= STATE_CLOSED;
	protected					Socket						socket					= null;
	protected					DataInputStream				from					= null;
	protected					DataOutputStream			to						= null;
	protected					LinkedHashMapManager		listEvents				= null;
	protected					boolean						ack						= true;
	protected					StringIP					ipsocket				= new StringIP("");
	protected					String						msgError				= "";

	
	// -- M�thodes abstraites -------------------------------------------------
	
	public		abstract		boolean						open();


	/**
	 * Constructeur
	 */
	public LineBase()
	{}
	
	/**
	 * Constructeur
	 * @param alistEvents
	 */
	public LineBase(LinkedHashMapManager alistEvents)
	{
		listEvents = alistEvents ;
	}


	// -- M�thodes priv�es ----------------------------------------------------

	
	/**
	 * Envoi une r�ponse � un message PING
	 */
	private void sendPong()
	{
		StringIP client = new StringIP("");
		client.setIP(socket.getInetAddress().toString());
		BufferManager bufferToSend = new BufferManager();
		
		//bufferToSend.setIdgroup(IDGROUP_CTRL);
		bufferToSend.setIdsession(IDSESSION_CTRL);
		bufferToSend.setIdmessage(Constantes.PONG);
		bufferToSend.addBuffer(NOM_CLASSE + " " + socket.getLocalAddress().getCanonicalHostName() + "(" + client +")");
		send(bufferToSend, false);
	}


	// -- M�thodes prot�g�es --------------------------------------------------

	
	/**
	 * Envoi les donn�es contenus dans le buffer
	 * @param ou
	 * @throws IOException
	 */
	protected boolean sendBuffer(byte[] header, int size, ByteArrayOutputStream abbuffer)
	{
		if( to == null ){
			return false;
		}

		ack = false;
		try{
			to.write(header);
//System.out.println("-sendBuffer->taille buffer:" + ataille);			
			if( size > BufferHeader.SIZE_HEADER ){
				abbuffer.writeTo(to);
			}
			to.flush();
			ack = true;
//System.out.println("-sendBuffer-> fin send");
		}
		catch (SocketException e) // Erreur socket on sort
		{
			System.out.println("-sendBuffer->" + e);
			e.printStackTrace();
			//log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "setSession->run : Socket");
			//log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Device:" + infoUser.getDevice());
			//log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
			//isConnect = Constantes.FALSE;
//			log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Connect� : " + soccli.isConnected() + " toServer:" + toServer + " fromServer:" + fromServer);
		}
		catch (SocketTimeoutException ioe) // Voir � supprimer bient�t
		{
			System.out.println("-sendBuffer->" + ioe);
			ioe.printStackTrace();
			//log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "setSession->run : TimeOut");
			//log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Device:" + infoUser.getDevice());
			//log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + ioe);
			//if( !soccli.isConnected() )
			//	isConnect = Constantes.FALSE;
//			log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Connect� : " + soccli.isConnected() + " " + isConnect);
		}
		catch(Exception e){
			System.out.println("-sendBuffer->" + e);
			e.printStackTrace();
			msgError += "\nLe buffer n'a pas pu �tre envoy� : " + e;
			return !ack;
		}
		return ack;
	}

	/**
	 * Gestion des messages qui ne concernent que cette ligne
	 * @param abuffer
	 */
	protected void controlLine(ByteBuffer abuffer, boolean isclient)
	{
//System.out.println("-LineBase-> isclient:" + isclient);
		int idmessage = abuffer.getInt();
		switch( idmessage ){
			case Constantes.PING :
					//System.out.println("-LineBase-> Ping recu ");
					if( isclient ){
						//System.out.println("-LineBase-> Nous sommes client, on r�pond pong ");
						sendPong();
					} else{
						//System.out.println("-LineBase-> Nous sommes serveur, on clear le buffer ");
						abuffer.clear();
					}
					break;
		}
	}
	
	/**
	 * D�connexion de la ligne
	 * @return
	 */
	protected boolean disconnect()
	{
		if( (state > STATE_TOCLOSE) && (state <= STATE_CLOSED) ){
			msgError += "\nConnexion d�j� ferm�e.";
			return true;
		}
		return true;
	}
	

	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Envoi d'un message au serveur
	 * @param aobject
	 * @return
	 */
	public boolean send(BufferManager abufferm, boolean clearheader)
	{
		setState(STATE_BUSY);
		boolean ret = sendBuffer(abufferm.getBHeader(), abufferm.getTotalSize(), abufferm.getBbufferToSend());
		if( ret ){
//System.out.println("-LineDetail-> On clear le buffer");			
			abufferm.clearBuffer(clearheader);
		}
		setState(STATE_READY);
		
		return ret;
	}
	
	/**
	 * Fermeture de la connexion
	 * @return
	 */
	public boolean close()
	{
		try
		{
			if( to != null ) to.close();
			if( from != null ) from.close();
			if( socket != null ) socket.close();
			
			if( state != STATE_OUTOFORDER ){
				state = STATE_CLOSED;
			}

			if( ipsocket  != null ){
				ipsocket.dispose();
				ipsocket = null;
			}
		}
		catch (IOException ioe){
			msgError += '\n' + NOM_CLASSE + Constantes.SEPARATEUR + ioe;
			return false;
		}
		return true;
	}

	/**
	 * Retourne si l'�tat de la connexion est pr�te
	 * @return
	 */
	public boolean isReady()
	{
//System.out.println("-LineBase-> connected:" + !socket.isOutputShutdown() );		
		return state == STATE_READY;
	}

	/**
	 * Retourne si l'�tat de la connexion est occup�
	 * @return
	 */
	public boolean isBusy()
	{
		return state == STATE_BUSY;
	}

	/**
	 * Retourne si l'�tat de la connexion a un probl�me
	 * @return
	 */
	public boolean isClosed()
	{
		return state <= STATE_CLOSED;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgError;
		msgError = "";

		return chaine;
	}
	
	/**
	 * Lib�re les ressources
	 */
 	public void dispose()
	{
 		socket = null;
 		from = null;
 		to = null;
 		if( listEvents != null ){
 			listEvents.clearObject();
 			listEvents = null;
 		}
 		
//System.out.println("-[LineBase]->dispose()");	
	}


 	// -- Accesseurs ----------------------------------------------------------


	/**
	 * @return le host
	 */
	public String getHost()
	{
		return host;
	}

	/**
	 * @param host le host � d�finir
	 */
	public void setHost(String host)
	{
		this.host = host;
	}

	/**
	 * @return le port
	 */
	public int getPort()
	{
		return port;
	}

	/**
	 * @param port le port � d�finir
	 */
	public void setPort(int port)
	{
		this.port = port;
	}

	/**
	 * @return le state
	 */
	public int getState()
	{
		return state;
	}

	/**
	 * @param state le state � d�finir
	 */
	public void setState(int state)
	{
		this.state = state;
	}


	/**
	 * @return le ipsocket
	 */
	public StringIP getIpsocket()
	{
		return ipsocket;
	}


	/**
	 * @param ipsocket le ipsocket � d�finir
	 */
	public void setIpsocket(StringIP ipsocket)
	{
		this.ipsocket = ipsocket;
	}
 	
 	
}
