package ri.seriem.libcommun.protocoleMsg;

import java.util.ArrayList;


public class StreamRequest extends BaseRequest
{
	// Constantes
	public static final int NOACTION=0;
	public static final int COPYTO5250=1;
	// Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
	//public static final int[] ACTIONS={NOACTION, COPYTO5250};
	public static final ArrayList<Integer> ACTIONS=new ArrayList<Integer>(){
		private static final long serialVersionUID = -5223936552676156713L;
		{ add(NOACTION); }
		{ add(COPYTO5250);  }
	};
	
	// Variables
	private String name=null;
	private String library_object=null; 
	//private boolean exist=false;

	
	public StreamRequest()
	{}
	
	public StreamRequest(String aname, String alibrary_object, int anaction)
	{
		setName(aname);
		setLibrary_object(alibrary_object);
		setActions(anaction);
	}

	
	// --> Accesseurs <--------------------------------------------------------
	
	/**
	 * @return le name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name le name � d�finir
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return le library_object
	 */
	public String getLibrary_object()
	{
		return library_object;
	}

	/**
	 * @param library_object le library_object � d�finir
	 */
	public void setLibrary_object(String library_object)
	{
		this.library_object = library_object;
	}

}
