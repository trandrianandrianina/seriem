package ri.seriem.libcommun.protocoleMsg;

import java.util.ArrayList;


public class LibraryRequest extends BaseRequest
{
	// Constantes
	public static final int NOACTION=0;
	public static final int GETTEXT=1;
	public static final int CREATE=2;
	public static final int ISEXISTS=4;
	public static final int LIST=8;
	public static final int LISTINTO=16;
	// Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
	//public static final int[] ACTIONS={NOACTION, GETTEXT, CREATE, ISEXISTS, LIST, LISTINTO};
	public static final ArrayList<Integer> ACTIONS=new ArrayList<Integer>(){
		private static final long serialVersionUID = -5223936552676156713L;
		{ add(NOACTION); }
		{ add(GETTEXT);  }
		{ add(CREATE);  }
		{ add(ISEXISTS);  }
		{ add(LIST);  }
		{ add(LISTINTO);  }
	};

	// Variables
	private String library=null;
	private String text=null;
	private boolean exist=false;
	private String typeVariant=null;
	private ArrayList<Object> variant = new ArrayList<Object>(); // Permet de stocker des donn�es diff�rentes d�fini par le typeVariant

	
	public LibraryRequest()
	{
	}
	
	public LibraryRequest(String alibrary, int anaction)
	{
		setLibrary(alibrary);
		setActions(anaction);
	}

	// --> M�thodes publiques <------------------------------------------------
	

	// --> M�thodes priv�es <--------------------------------------------------


	// --> Accesseurs <--------------------------------------------------------
	
	/**
	 * @return le library
	 */
	public String getLibrary()
	{
		return library;
	}
	/**
	 * @param library le library � d�finir
	 */
	public void setLibrary(String library)
	{
		if( library != null ){
			this.library = library.trim();
		} else{
			this.library = library;
		}
	}
	/**
	 * @return le text
	 */
	public String getText()
	{
		return text;
	}
	/**
	 * @param text le text � d�finir
	 */
	public void setText(String text)
	{
		this.text = text;
	}
	/**
	 * @return le exist
	 */
	public boolean isExist()
	{
		return exist;
	}
	/**
	 * @param exist le exist � d�finir
	 */
	public void setExist(boolean exist)
	{
		this.exist = exist;
	}

	/**
	 * @return le typeVariant
	 */
	public String getTypeVariant()
	{
		return typeVariant;
	}

	/**
	 * @param typeVariant le typeVariant � d�finir
	 */
	public void setTypeVariant(String typeVariant)
	{
		this.typeVariant = typeVariant;
	}

	/**
	 * @return le variant
	 */
	public ArrayList<Object> getVariant()
	{
		return variant;
	}

	/**
	 * @param variant le variant � d�finir
	 */
	public void setVariant(ArrayList<Object> variant)
	{
		this.variant = variant;
	}


}
