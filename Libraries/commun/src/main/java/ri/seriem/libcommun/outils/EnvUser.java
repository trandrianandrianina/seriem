/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ri.seriem.libcommun.edition.ServerEditionInfos;
import ri.seriem.libcommun.outils.communication.LinesManager;

/**
 * Gestion de l'environnement de l'utilisateur sur l'I5
 */
public class EnvUser extends User {
  // Constantes
  public final static int DELAY_WAIT = 30; // D�lai d'attente pour la reconnexion avant d'envoyer un message � l'utilisateur
  
  // S�curit�s profil
  public final static char TOUS_LES_DROITS = ' '; // Administrateur (droit � tout)
  public final static char DROITS_RESTREINTS = '1';
  public final static char DROITS_RESTREINTS_SAUF_CHGBIB = '2';
  public final static char TOUS_LES_DROITS_ASP = '3'; // Administrateur ASP droit � tout sauf le changement de biblioth�ques
  
  // Variables
  private String nomConfig = ""; // Nom de l'environnement s�l�ctionn�
  private String prefixdevice = ""; // Pr�fixe pour le nom des DTAQs & des JOBs
  private String device = ""; // Nom de la device (DataQueue) sur 9 carcat�res max (donc sans le 'S' ou le 'R')
  private String host = null; // Adresse (IP) de l'I5 sur lequel l'utilisateur va travailler
  private String jobq = ""; // Nom de la jobq dans laquelle l'utilisateur va travailler
  private String program = ""; // Programme de d�marrage pour la session Menu
  private String library = ""; // Biblioth�que des jobs utilisteurs
  private String libraryEnv = ""; // Biblioth�que de l'environnement de d�marrage
  private String folder = ""; // Dossier contenant les runtimes sur le serveur
  protected String curlib = ""; // Biblioth�que courante pour l'utilisateur
  protected String textcurlib = ""; // Libell� de la biblioth�que courante
  protected String texttitlebarinformations = ""; // Texte contenant l'�tablissement et le magasin
  protected char letter = ' '; // Lettre pour l'ASP (biblioth�ques)
  private boolean titleBarInformations = false;
  private String typemenu = null; // Le type de menu utilis� par l'environnement d�finit dans le rt.ini
  
  // Pour l'instant comment� en vue d'�tre supprim� et de cr�er soit une classe soit d'ajouter les variables juste n�cessaire
  private String USSMNP = ""; // Menu personnalis�
  private char USSPOR = ' '; // Possibilit�s restreintes
  private char USSDSP = ' '; // Type �mulation
  protected ArrayList<String> listeBibSpe = new ArrayList<String>();
  
  private String serveurSGM = null; // Adresse IP du serveur SGM
  private int portSGM = -1; // Port du serveur SGM
  private InetAddress ia_serveurSGM = null; // InetAdresse du serveur SGM
  private SessionTransfert trfSession = null;
  private String fichierTraduction = Constantes.INIT_TRADUCTION;
  private String langue = "fr";
  private HashMap<String, String> translationtable = null;
  private HashMap<String, String> libimg = null;
  private int deltaPersoTailleFont = 0;
  private HashMap<String, String> config = null;
  private String versionProtocoleServeur = null;
  private String versionServeur = null;
  private String availFonts[] = null;
  private ServerEditionInfos infosServerEdition = null; // Contient les infos sur le serveur Newsim
  private String dossierRacineServeur = null;
  private boolean stopReconnexion = false; // Indique si l'utilisateur souhaite continuer � attendre que la reconnexion se fasse
  private byte compteurReconnexion = 0; // Comptabilise le nombre de sessions � reconnecter (si =0 alors les connexions sont ok)
  private String deviceImpression = null;
  
  private LinesManager linesManager = null;
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  /**
   * Initialise le nom de la Config
   */
  public void setNomConfig(String valeur) {
    if (valeur != null) {
      nomConfig = valeur.trim();
    }
    if ((folder == null) || (folder.trim().equals(""))) {
      setFolder(valeur);
    }
  }
  
  /**
   * Initialise la device
   */
  public void setDevice(String valeur) {
    device = valeur.trim();
  }
  
  /**
   * Initialise le pr�fixe de la device
   */
  public void setPrefixDevice(String valeur) {
    if (valeur != null) {
      prefixdevice = valeur.trim();
    }
  }
  
  /**
   * Initialise le nom du Host
   */
  public void setHost(String valeur) {
    if (valeur != null) {
      host = valeur.trim();
    }
  }
  
  /**
   * Initialise la jobq
   */
  public void setJobQ(String valeur) {
    if (valeur != null) {
      jobq = valeur.trim();
    }
  }
  
  /**
   * Initialise le programme initial
   */
  public void setProgram(String valeur) {
    if (valeur != null) {
      program = valeur.trim();
    }
  }
  
  /**
   * Initialise la biblioth�que du programme
   */
  public void setLibrary(String valeur) {
    if (valeur != null) {
      library = valeur.trim();
    }
  }
  
  /**
   * Initialise la biblioth�que du programme
   */
  public void setLibraryEnv(String valeur) {
    if (valeur != null) {
      libraryEnv = valeur.trim();
    }
  }
  
  /**
   * Initialise la biblioth�que courante
   */
  public void setCurlib(String valeur) {
    if (valeur != null) {
      curlib = valeur.trim();
    }
  }
  
  /**
   * Initialise le libell� de la biblioth�que courante
   */
  public void setTextCurlib(String valeur) {
    if (valeur != null) {
      textcurlib = valeur.trim();
    }
    else {
      textcurlib = "";
    }
  }
  
  /**
   * Initialise l'adresse IP ou host du serveur SGM
   */
  public void setServeurSGM(String valeur) {
    if (valeur != null) {
      serveurSGM = valeur.trim();
      try {
        setIa_serveurSGM(InetAddress.getByName(serveurSGM));
      }
      catch (UnknownHostException e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Initialise le port du serveur SGM
   */
  public void setPortSGM(int valeur) {
    portSGM = valeur;
  }
  
  /**
   * Initialise les valeurs � partir du PSEMUSSM
   */
  public void set2Psemussm(String valeur) {
    char[] blancs = new char[157];
    
    // On s'assure que la chaine soit assez longue
    StringBuffer sb = new StringBuffer(valeur);
    while (sb.length() < 157) {
      Arrays.fill(blancs, ' ');
      sb.append(blancs);
    }
    
    // Extraction des donn�es (a am�liorer = via une desciption fichier pour plus de souplesse)
    letter = sb.charAt(0); // Lettre pour l'ASP (biblioth�ques)
    if (letter == ' ') {
      letter = 'W';
    }
    curlib = sb.substring(10, 15); // Biblioth�que courante pour l'utilisateur
    USSPOR = sb.charAt(41); // Possibilit� restreint
    USSDSP = sb.charAt(52); // Type �mulation
  }
  
  /**
   * Initialisation la session transfert
   */
  public void initTransfertSession(GestionRT serveurRT, GestionRT clientRT) {
    trfSession = new SessionTransfert(this);
    dossierTravail = dossierRacine + File.separator + Constantes.DOSSIER_RT + File.separator + folder + File.separator;
    trfSession.setSession(this);
    trfSession.setGestionRT(serveurRT, clientRT);
  }
  
  /**
   * Initialise le fichier de traduction
   */
  public void setTranslationTable() {
    // Lecture du fichier des traductions
    trfSession.FichierArecupRT(fichierTraduction);
    trfSession.FichierRecupRT(fichierTraduction);
    GestionFichierEQU gfe = new GestionFichierEQU(dossierTravail + fichierTraduction);
    if (gfe.lectureFichier() == Constantes.OK) {
      translationtable = gfe.getListe(true);
    }
    else {
      log.ecritureMessage(profil, gfe.getMsgErreur());
    }
    // Dans tous les cas on part pas avec la liste vide (sert plus tard, en vue d'�viter un test sur null dans oData notamment)
    if (translationtable == null) {
      translationtable = new HashMap<String, String>();
    }
  }
  
  /**
   * Initialise le fichier de traduction des images (�quivalence V07F)
   */
  public void setTranslationImage() {
    // Lecture du fichier des �quivalences
    trfSession.FichierArecupRT(Constantes.INIT_LIBIMG);
    trfSession.FichierRecupRT(Constantes.INIT_LIBIMG);
    GestionFichierEQU gfe = new GestionFichierEQU(dossierTravail + Constantes.INIT_LIBIMG);
    if (gfe.lectureFichier() == Constantes.OK) {
      libimg = gfe.getListe(true);
    }
    else {
      log.ecritureMessage(profil, gfe.getMsgErreur());
    }
  }
  
  /**
   * Initialise la langue
   * @param lg
   */
  public void setLangue(String lg) {
    int pos = fichierTraduction.indexOf('_');
    
    langue = lg.trim();
    
    // On modifie le nom du fichier en cons�quence
    if (pos == -1) {
      return;
    }
    fichierTraduction = fichierTraduction.substring(0, pos + 1) + langue + fichierTraduction.substring(pos + 1 + langue.length());
    if (trfSession != null) {
      setTranslationTable();
    }
  }
  
  /**
   * Initialise le delta pour la personnalisation de la taille de la police
   * @param delta
   */
  public void setDeltaPersoTailleFont(int delta) {
    deltaPersoTailleFont = delta;
  }
  
  /**
   * Retourne le nom de la Config
   */
  public String getNomConfig() {
    return nomConfig;
  }
  
  /**
   * Retourne la device
   */
  public String getDevice() {
    return device;
  }
  
  /**
   * Retourne le pr�fixe de la device
   */
  public String getPrefixDevice() {
    if ((prefixdevice == null) || (prefixdevice.equals(""))) {
      return "QPADEV";
    }
    return prefixdevice;
  }
  
  /**
   * Retourne le nom du Host
   */
  public String getHost() {
    return host;
  }
  
  /**
   * Retourne la jobq
   */
  public String getJobQ() {
    return jobq;
  }
  
  /**
   * Retourne le programme initial
   */
  public String getProgram() {
    return program;
  }
  
  /**
   * Retourne la biblioth�que du programme
   */
  public String getLibrary() {
    return library;
  }
  
  /**
   * Retourne la biblioth�que du programme
   */
  public String getLibraryEnv() {
    return libraryEnv;
  }
  
  /**
   * @return le folder
   */
  public String getFolder() {
    return folder;
  }
  
  /**
   * @param folder le folder � d�finir
   */
  public void setFolder(String folder) {
    this.folder = folder;
  }
  
  /**
   * Retourne la biblioth�que courante
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * Retourne le libell� de la biblioth�que courante
   */
  public String getTextCurlib() {
    return textcurlib;
  }
  
  /**
   * Retourne la lettre d'environnement S�rie N
   */
  public char getLetter() {
    return letter;
  }
  
  /**
   * @param listeBibSpe the listeBibSpe to set
   */
  public void setListeBibSpe(ArrayList<String> listeBibSpe) {
    this.listeBibSpe = listeBibSpe;
  }
  
  /**
   * @return the listeBibSpe
   */
  public ArrayList<String> getListeBibSpe() {
    return listeBibSpe;
  }
  
  /**
   * Retourne l'adresse IP ou host du serveur S�rie N
   */
  public String getServeurSGM() {
    return serveurSGM;
  }
  
  /**
   * @return le ia_serveurSGM
   */
  public InetAddress getIa_serveurSGM() {
    return ia_serveurSGM;
  }
  
  /**
   * @param ia_serveurSGM le ia_serveurSGM � d�finir
   */
  public void setIa_serveurSGM(InetAddress ia_serveurSGM) {
    this.ia_serveurSGM = ia_serveurSGM;
  }
  
  /**
   * Retourne le port du serveur SGM
   */
  public int getPortSGM() {
    return portSGM;
  }
  
  /**
   * Retourne la session transfert
   * @return
   */
  public SessionTransfert getTransfertSession() {
    return trfSession;
  }
  
  /**
   * Retourne la table de traduction
   */
  public HashMap<String, String> getTranslationTable() {
    return translationtable;
  }
  
  /**
   * Retourne la table de traduction des �quivalences
   */
  public HashMap<String, String> getTranslationImage() {
    return libimg;
  }
  
  /**
   * Retourne la langue utilis�
   */
  public String getLangue() {
    return langue;
  }
  
  /**
   * Retourne le delta pour la personnalisation de la taille de la police
   */
  public int getDeltaPersoTailleFont() {
    return deltaPersoTailleFont;
  }
  
  /**
   * Initialise les variables de la config (logo, couleur, etc...)
   * PAS UTILISE voir +tard
   */
  public void setConfig(HashMap<String, String> config) {
    this.config = config;
  }
  
  /**
   * Retourne les variables de la config (logo, couleur, etc...)
   * PAS UTILISE voir +tard
   */
  public HashMap<String, String> getConfig() {
    return config;
  }
  
  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * @return uSSDSP
   */
  public char getUSSDSP() {
    return USSDSP;
  }
  
  /**
   * @param ussdsp uSSDSP � d�finir
   */
  public void setUSSDSP(char ussdsp) {
    USSDSP = ussdsp;
  }
  
  /**
   * @return uSSPOR
   */
  public char getUSSPOR() {
    return USSPOR;
  }
  
  /**
   * @param usspor uSSPOR � d�finir
   */
  public void setUSSPOR(char usspor) {
    USSPOR = usspor;
  }
  
  /**
   * @param usspor uSSPOR � d�finir
   */
  public void setUSSPOR(String usspor) {
    if (usspor.trim().equals("")) {
      USSPOR = ' ';
    }
    else {
      USSPOR = usspor.charAt(0);
    }
  }
  
  /**
   * @return le uSSMNP
   */
  public String getUSSMNP() {
    return USSMNP;
  }
  
  /**
   * @param uSSMNP le uSSMNP � d�finir
   */
  public void setUSSMNP(String uSSMNP) {
    if (uSSMNP == null) {
      USSMNP = uSSMNP;
    }
    else {
      USSMNP = uSSMNP.trim();
    }
  }
  
  /**
   * @return versionServeur
   */
  public String getVersionProtocoleServeur() {
    return versionProtocoleServeur;
  }
  
  /**
   * @param versionServeur versionServeur � d�finir
   */
  public void setVersionProtocoleServeur(String versionProtocoleServeur) {
    this.versionProtocoleServeur = versionProtocoleServeur;
  }
  
  /**
   * @return le versionServeur
   */
  public String getVersionServeur() {
    return versionServeur;
  }
  
  /**
   * @param versionServeur le versionServeur � d�finir
   */
  public void setVersionServeur(String versionServeur) {
    this.versionServeur = versionServeur;
  }
  
  /**
   * Retourne une chaine avec toutes les infos n�cessaire pour l'ouverture d'une session
   */
  public String getDemandeSession(String demandesession) {
    StringBuffer sb = new StringBuffer(1024);
    
    // EnvUser
    sb.append(getNomConfig()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getProfil()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        .append(getMotDePasse()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getPrefixDevice())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getJobQ()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getProgram())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getLibraryEnv()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getLibrary())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getCurlib()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getFolder())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getDeviceImpression()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        // EnvProgram
        .append(demandesession);
    
    // Chiffrage de la chaine
    Chiffrage bf = new Chiffrage(getCodepageServeur());
    return new String(bf.cryptInString(sb.toString()));
  }
  
  /**
   * Liste les polices disponibles sur le syst�me
   */
  private void listFontSystem() {
    GraphicsEnvironment graphicsEvn = GraphicsEnvironment.getLocalGraphicsEnvironment();
    availFonts = graphicsEvn.getAvailableFontFamilyNames();
  }
  
  /**
   * Recherche si un police donn�e est bien presente sur le syst�me
   * @param font
   * @return
   */
  public boolean isFontPresent(String font) {
    if (font == null) {
      return false;
    }
    font = font.trim();
    if (availFonts == null) {
      listFontSystem();
    }
    for (int i = 0; i < availFonts.length; i++) {
      if (font.equalsIgnoreCase(availFonts[i])) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * V�rifie que le profil soit bien valide, notamment que ce soit un profil S�rie N
   * @return
   */
  public boolean isValidProfil() {
    return (getUSSDSP() == 'N');
  }
  
  /**
   * @param infosServerEdition the infosServerEdition to set
   */
  public void setInfosServerEdition(ServerEditionInfos infosServerEdition) {
    this.infosServerEdition = infosServerEdition;
  }
  
  /**
   * @return the infosServerEdition
   */
  public ServerEditionInfos getInfosServerEdition() {
    return infosServerEdition;
  }
  
  /**
   * @return le dossierRacineServeur
   */
  public String getDossierRacineServeur() {
    if (dossierRacineServeur == null) {
      dossierRacineServeur = '/' + getLetter() + Constantes.DOSSIER_RACINE;
      getLog().ecritureMessage(getProfil(),
          "Le dossier racine du serveur S�rie N  est � null, on l'initialise avec " + dossierRacineServeur);
    }
    return dossierRacineServeur;
  }
  
  /**
   * @param dossierRacineServeur le dossierRacineServeur � d�finir
   */
  public void setDossierRacineServeur(String dossierRacineServeur) {
    this.dossierRacineServeur = dossierRacineServeur;
  }
  
  /**
   * @return le titleBarInformations
   */
  public boolean isTitleBarInformations() {
    return titleBarInformations;
  }
  
  /**
   * @param titleBarInformations le titleBarInformations � d�finir
   */
  public void setTitleBarInformations(boolean titleBarInformations) {
    this.titleBarInformations = titleBarInformations;
  }
  
  /**
   * @return le texttitlebarinformations
   */
  public String getTexttitlebarinformations() {
    return texttitlebarinformations;
  }
  
  /**
   * @param texttitlebarinformations le texttitlebarinformations � d�finir
   */
  public void setTexttitlebarinformations(String valeur) {
    if (valeur != null) {
      texttitlebarinformations = valeur.trim();
    }
    else {
      texttitlebarinformations = "";
    }
  }
  
  /**
   * @return le typemenu
   */
  public String getTypemenu() {
    return typemenu;
  }
  
  /**
   * @param typemenu le typemenu � d�finir
   */
  public void setTypemenu(String typemenu) {
    this.typemenu = typemenu;
  }
  
  /**
   * @return le stopReconnexion
   */
  public boolean isStopReconnexion() {
    return stopReconnexion;
  }
  
  /**
   * @param stopReconnexion le stopReconnexion � d�finir
   */
  public void setStopReconnexion(boolean stopReconnexion) {
    this.stopReconnexion = stopReconnexion;
  }
  
  /**
   * @return le compteurReconnexion
   */
  public byte getCompteurReconnexion() {
    return compteurReconnexion;
  }
  
  /**
   * Incr�mente le compteur des reconnexions
   * @param compteurReconnexion le compteurReconnexion � d�finir
   */
  public void incCompteurReconnexion() {
    this.compteurReconnexion++;
    if (compteurReconnexion > Constantes.MAXRECONNEXION) {
      setStopReconnexion(true);
    }
  }
  
  /**
   * Remet � 0 le compteur des reconnexions
   * @param compteurReconnexion le compteurReconnexion � d�finir
   */
  public void resetCompteurReconnexion() {
    this.compteurReconnexion = 0;
  }
  
  /**
   * @return le deviceImpression
   */
  public String getDeviceImpression() {
    return deviceImpression;
  }
  
  /**
   * @param deviceImpression le deviceImpression � d�finir
   */
  public void setDeviceImpression(String deviceImpression) {
    this.deviceImpression = deviceImpression;
  }
  
  /**
   * Initialise le pool des connexions
   * @return
   */
  public boolean initPool() {
    linesManager = new LinesManager(serveurSGM, portSGM);
    boolean ret = linesManager.initPool();
    if (!ret) {
      msgErreur = linesManager.getMsgErreur();
    }
    else {
      setAdresseIP(linesManager.getClientIP());
    }
    return ret;
  }
  
  /**
   * @return le linesManager
   */
  public LinesManager getLinesManager() {
    return linesManager;
  }
  
  /**
   * Lib�re les ressources
   */
  public void dispose() {
    super.dispose();
    
    if (listeBibSpe != null) {
      listeBibSpe.clear();
      listeBibSpe = null;
    }
    if (translationtable != null) {
      translationtable.clear();
      translationtable = null;
    }
    if (libimg != null) {
      libimg.clear();
      libimg = null;
    }
    if (config != null) {
      config.clear();
      config = null;
    }
    if (infosServerEdition != null) {
      infosServerEdition.dispose();
      infosServerEdition = null;
    }
    ia_serveurSGM = null;
    trfSession = null;
    linesManager = null;
  }
}
