package ri.seriem.libcommun.protocoleMsg;

import java.util.ArrayList;
import java.util.Arrays;


public class Db2Request extends BaseRequest
{
	// Constantes
	public static final int NOACTION=0;
	public static final int EXECUTE=1;
	// Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
	//public static final int[] ACTIONS={NOACTION, EXECUTE};
	public static final ArrayList<Integer> ACTIONS=new ArrayList<Integer>(){
		private static final long serialVersionUID = -5223936552676156713L;
		{ add(NOACTION); }
		{ add(EXECUTE);  }
	};

	// Variables
	private String request=null;
	private boolean select=false; 
	// Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
	//private String[] title=null;
	//private Object[][] data=null;
	// Nouvelle m�thode
	private ArrayList<String> title=null;
	private ArrayList<LineCells> data=null;
	
	/**
	 * Constructeur
	 */
	public Db2Request()
	{
	}

	/**
	 * Constructeur
	 * @param arequest
	 */
	public Db2Request(String arequest, int anaction)
	{
		setRequest(arequest);
		setActions(anaction);
	}


	// --> Accesseurs <--------------------------------------------------------

	/**
	 * @return le request
	 */
	public String getRequest()
	{
		return request;
	}
	/**
	 * @param request le request � d�finir
	 */
	public void setRequest(String request)
	{
		this.request = request;
		setSelect(request.toLowerCase().startsWith("select"));
	}
	/**
	 * @return le select
	 */
	public boolean isSelect()
	{
		return select;
	}

	/**
	 * @param select le select � d�finir
	 */
	public void setSelect(boolean select)
	{
		this.select = select;
	}

	/**
	 * @return le title
	 * 
	 * Fait pour garder la compatibilit� avec les autres classes  
	 */
	public String[] getTTitle()
	{
		if( title == null ) return null;
		
		String[] tab = new String[title.size()]; 
		return (String[]) title.toArray(tab); 
	}
	/**
	 * @param title le title � d�finir
	 * Fait pour garder la compatibilit� avec les autres classes
	 */
	public void setTTitle(String[] atitle)
	{
		if( atitle == null ) return;
		if( title != null ) title.clear();  
		title = new ArrayList<String>(Arrays.asList(atitle));
	}
	
	/**
	 * @param title le title � d�finir
	 */
	public void setTitle(ArrayList<String> title)
	{
		this.title = title;
	}


	/**
	 * @return le title
	 */
	public ArrayList<String> getTitle()
	{
		return title;
	}

	/**
	 * @return le data
	 * Fait pour garder la compatibilit� avec les autres classes
	 */
	public Object[][] getTData()
	{
		if( data == null ) return null;
//System.out.println("--> " + data.size());
///System.out.println("--> " + data.get(0).tableau.size());
		Object[][] tab = new Object[ data.size() ][ data.get(0).tableau.size() ];
		for( int i=0; i< tab.length; i++ )
			for( int j=0; j< tab[i].length; j++ ){
				tab[i][j] = data.get(i).tableau.get(j);
			}
		
		return tab;
	}
	
	/**
	 * @param data le data � d�finir
	 * Fait pour garder la compatibilit� avec les autres classes
	 */
	public void setTData(Object[][] adata)
	{
		if( adata == null ) return;
		if( data != null ){
			for( int i=0; i < data.size(); i++ ){
				data.get(i).tableau.clear();
				//data.get(i).tableau.trimToSize();
			}
			data.clear();
			//data.trimToSize();
		} else {
			data = new ArrayList<LineCells>();
		}
		
//System.out.println("-i-> " + adata.length);
		for( int i=0; i < adata.length; i++ ){
//			System.out.println("-j-> " + adata[i].length);
			if( data.size() < adata.length )
				data.add(new LineCells() );
			for( int j=0; j < adata[i].length; j++ ){
				//if( data.get(i).tableau == null )
				//	data.get(i).tableau = new ArrayList<Object>();  
				data.get(i).tableau.add( adata[i][j] );
			}
		}
	}

	/**
	 * @param data le data � d�finir
	 */
	public void setData(ArrayList<LineCells> data)
	{
		this.data = data;
	}

	/**
	 * @return le data
	 */
	public ArrayList<LineCells> getData()
	{
		return data;
	}


}
