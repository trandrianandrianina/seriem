/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.io.PrintStream;

import org.apache.log4j.Logger;

/**
 * Cette classe permet de rediriger les flux de la console vers un logger nomm� (en dur) "consoleFichier" d�finit dans la classe Trace.
 */
public class StdOutErrTrace {
  // Variables
  private static Logger loggerConsole = null;
  
  public static void redirigerSystemOutAndErr() {
    loggerConsole = Logger.getLogger("consoleFichier");
    System.setOut(createLoggingProxy(System.out));
    System.setErr(createLoggingProxy(System.err));
  }
  
  public static PrintStream createLoggingProxy(final PrintStream realPrintStream) {
    return new PrintStream(realPrintStream) {
      @Override
      public void print(final String string) {
        loggerConsole.info(string);
      }
      
      @Override
      public void println(final String string) {
        loggerConsole.info(string);
      }
    };
  }
}
