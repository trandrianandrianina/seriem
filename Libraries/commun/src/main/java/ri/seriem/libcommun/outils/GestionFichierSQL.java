//=================================================================================================
//==> Gestion des fichiers SQL                                              19/11/2014 - 19/11/2014
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.util.ArrayList;

import javax.swing.JFileChooser;


public class GestionFichierSQL extends GestionFichierTexte
{
    // Constantes
	private final static String FILTER_EXT_SQL	= "sql";
	private final static String FILTER_DESC_SQL	= "Fichier SQL (*.sql)";
	private final static String[] KEYWORDS		= {"INSERT", "UPDATE", "DELETE", "SELECT", "CREATE", "ALTER", "DROP"}; // A mettre � jour si besoin
	
	// Variables
	private ArrayList<String>listQueries = new ArrayList<String>(); 

	
	/**
     * Constructeur de la classe
     * @param fch
     */
    public GestionFichierSQL(String fch)
    {
    	super(fch);
    }

	/**
	 * Retourne les filtres possible pour les boites de dialogue
	 * @param jfc
	 */
	public void initFiltre(JFileChooser jfc)
	{
		super.initFiltre(jfc);
		jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_SQL}, FILTER_DESC_SQL));
	}

	// -- M�thodes publiques  -------------------------------------------------

	/**
	 * Analyse et traite le fichier texte afion d'en extraire les requ�tes
	 * @return
	 */
	public boolean treatmentFile()
	{
		// Chargement du fichier
		ArrayList<String> lines = getContenuFichier();
		if( lines == null ){
			return false;
		}

		// On �limine les lignes qui ne servent � rien
		removeBlanksLine( lines );
		removeComments( lines );
		removeSpecificString(";;;");
		
		//for( String line : lines )
		//	System.out.println(line);
		
		// On constitue une liste avec chaque requ�te enti�re
		extractRequest( lines );
		cleanRequest();
		
		return true;
	}

	/**
	 * Supprime les chaines caract�res voulues 
	 * @param lines
	 * @param toremove
	 */
	public void removeSpecificString(String toremove)
	{
		if( toremove == null){
			return;
		}
		
		ArrayList<String> liste = getContenuFichier(); 
		for( int i = 0; i < liste.size(); i++ ){
			liste.set(i, liste.get(i).replaceAll(toremove, ""));
		}
	}

	public void dispose()
	{
		super.dispose();
		if( listQueries != null ){
			listQueries.clear();
		}
	}
	
	// -- M�thodes priv�es ----------------------------------------------------
	
	/**
	 * Supprime les lignes vides du source
	 * @param lines
	 */
	private void removeBlanksLine(ArrayList<String> lines)
	{
		int i   = 0;
		//int max = lines.size();
		
		while( i < lines.size() ){
			if( lines.get(i).trim().equals("")){
				lines.remove(i);
			} else{
				i++;
			}
		}
	}
	
	/**
	 * Supprime les commentaires du source
	 * @param lines
	 */
	private void removeComments(ArrayList<String> lines)
	{
		int i   = 0;
		//int max = lines.size();
		boolean remove = false;
		boolean lastremove = true;
		
		while( i < lines.size() ){
			// On d�tecte les marqueurs de commentaires
			if( lines.get(i).trim().startsWith("//")){
				remove = true;
			} else{
				if( lines.get(i).trim().startsWith("/*") ){
					remove = true;
					lastremove = false;
				}
				if( lines.get(i).trim().endsWith("*/") ){
					lastremove = true;
				}
			}
			
			// On supprime effectivement la ligne
			if( remove ){
				lines.remove(i);
				if( lastremove ){
					remove = false;
				}
			} else{
				i++;
			}
		}
	}
	
	/**
	 * Constitue une liste contenant les requ�tes enti�res
	 * @param lines
	 * @return
	 */
	public void extractRequest(ArrayList<String> lines)
	{
		boolean currentrequest = false;
		boolean nextrequest = false; 
		StringBuilder sb = new StringBuilder(1024);
		
		for( String line : lines){
			if( testKeywords( line ) ){
				if( !currentrequest ){
					currentrequest = true;
				} else{
					nextrequest = true;
					currentrequest = false;
				}
			} else{
				nextrequest = false;
				currentrequest = true;
			}
			
			if( nextrequest ){
				listQueries.add( sb.toString() );
				sb.setLength( 0 );
				sb.append( line );
			} else if( currentrequest ){
				sb.append( line );
			}

		}
		listQueries.add( sb.toString() );
	}

	/**
	 * Teste si une chaine commence par un mot clef SQL
	 * @param line
	 * @return
	 */
	public boolean testKeywords(String line)
	{
		line = line.trim().toUpperCase();
		for( String word: KEYWORDS){
			if( line.startsWith(word) ){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Nettoyage final des requ�tes afin qu'elles soient utilisables
	 */
	private void cleanRequest()
	{
		String request;
		int max = listQueries.size();
		
		if( max == 0 ){
			return;
		}
		
		for( int i = 0; i < max; i++ ){
			request = listQueries.get( i ).trim();
			// On suppprime le point-virgule � la fin de la requ�te
			if( request.endsWith( ";" )  ){
				request = request.substring( 0, request.length()-1 );
			}
			listQueries.set( i, request );
		}
	}

	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le listQueries
	 */
	public ArrayList<String> getListQueries()
	{
		return listQueries;
	}

	/**
	 * @param listQueries le listQueries � d�finir
	 */
	public void setListQueries(ArrayList<String> listQueries)
	{
		this.listQueries = listQueries;
	}
}
