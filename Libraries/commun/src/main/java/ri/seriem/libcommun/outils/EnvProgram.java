/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.util.ArrayList;

/**
 * Gestion de l'environnement du programme lanc� sur l'I5
 */
public class EnvProgram {
  // Constantes
  public final static char JAVA = 'J';
  public final static char AS400 = ' ';
  
  // Variables
  private String program = "";
  private String libraryEnv = "";
  private String library = "";
  private ArrayList<String> listeparam = null;
  private ArrayList<String> listebib = null;
  private String ptmenu = "inconnu";
  private String libmenu = "inconnu";
  private char type = AS400;
  private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  protected EnvUser infoUser = null;
  
  /**
   * Constructeur
   */
  public EnvProgram() {
  }
  
  /**
   * Constructeur
   * Note: on met QGPL & la bib d'environnement en ligne syst�matiquement
   */
  public EnvProgram(final EnvUser infos) {
    setEnvUser(infos); // A revoir suite modif du 25/09/2009
  }
  
  /**
   * Constructeur (Attention il manque la lettre d'environnement : mais normalement pas la peine car c'est le SEXG03CL qui fait le boulot
   * dans les IXXX.
   * Par contre, il faut s'assurer que le IXXX soit execut� en premier)
   */
  public EnvProgram(String valeur) {
    String infos[] = null;
    int nbrElt = 0;
    int i = 0;
    int k = 0;
    
    // D�chiffrage de la chaine
    Chiffrage bf = new Chiffrage();
    valeur = bf.decryptInString(valeur);
    
    // On d�coupe la chaine et on constitue un tableau de string � partir des diff�rents �l�ments
    // 0:Config RT 1:Profil 2:Mdp 3:prefixdevice 4:subsystem 5:program 6:libraryEnv 7:library 8:curlib 9:folder
    // 10:device 11:programme 12:biblioth�que 13:nbr de param...suivi des params ??:nbr de bib...suivi des bib xx:Point de menu
    infos = Constantes.splitString(valeur, Constantes.SEPARATEUR_CHAINE_CHAR);
    
    if ((infos != null) && (infos.length > 16)) {
      infoUser = new EnvUser();
      infoUser.setNomConfig(infos[0]);
      infoUser.setProfil(infos[1]);
      infoUser.setMotDePasse(infos[2]);
      infoUser.setPrefixDevice(infos[3]);
      infoUser.setJobQ(infos[4]);
      infoUser.setProgram(infos[5]);
      infoUser.setLibraryEnv(infos[6]);
      infoUser.setLibrary(infos[7]);
      infoUser.setCurlib(infos[8]);
      infoUser.setFolder(infos[9]);
      infoUser.setDeviceImpression(infos[10]);
      infoUser.setDevice(infos[11]);
      // On teste s'il s'agit du programme pour les menus et qu'il est java alors on le traite comme un programme normal
      if (infos[12].equals("")
          && ((infos[5].toLowerCase().lastIndexOf(".jar") != -1) || (infos[5].toLowerCase().lastIndexOf(".class") != -1))) {
        setProgram(infos[5]);
        setLibrary(infos[7]);
      }
      else {
        setProgram(infos[12]);
        setLibrary(infos[13]);
      }
      // On passe aux listes
      nbrElt = Integer.parseInt(infos[14]);
      for (i = 0; i < nbrElt; i++) {
        addParametre(infos[i + 15]);
      }
      k = i + 15;
      nbrElt = Integer.parseInt(infos[k]);
      for (i = 0, k++; i < nbrElt; i++) {
        addBib(infos[i + k]);
      }
      
      k += i;
      // Le point de menu que l'on formate � la sauce SERIE N
      setPointMenu(infos[k].replace(" ", "").replace('.', ' '));
    }
  }
  
  // -- M�thodes priv�es ----------------------------------------------------
  
  /**
   * Initialise l'environnement utilisateur
   */
  public void setEnvUser(final EnvUser infos) {
    infoUser = infos;
    // Ajout syst�matique dans la liste des bib � mettre en ligne !! l'ordre des bib est important QGPL apres environnement � cause du
    // psemussm
    if (!infoUser.getLibrary().trim().equals("")) {
      if (listebib == null) {
        listebib = new ArrayList<String>();
      }
      listebib.add(infoUser.getLibrary());
      listebib.add(infoUser.getLibraryEnv());
      listebib.add("QGPL");
      listebib.add("QTEMP"); // Ajout� apr�s coup
    }
  }
  
  /**
   * Ajoute une biblioth�que � la liste (toujours en t�te pour qu'elle soit avant QTEMP/QGPL/...)
   */
  public void addBib(String valeur) {
    if (listebib == null) {
      listebib = new ArrayList<String>();
    }
    // On change la lettre d'environnement de S�rie N
    if (infoUser.getLetter() != ' ') {
      listebib.add(0, infoUser.getLetter() + valeur.substring(1).trim());
    }
    else {
      listebib.add(0, valeur.trim());
    }
  }
  
  /**
   * Ajoute un param�tre � la liste
   */
  public void addParametre(String valeur) {
    if (listeparam == null) {
      listeparam = new ArrayList<String>();
    }
    listeparam.add(valeur);
  }
  
  /**
   * Retourne une chaine avec toutes les infos n�cessaire pour l'ouverture d'une session
   */
  public String getDemandeSession() {
    int i = 0;
    final StringBuffer sb = new StringBuffer(1024);
    
    sb.append(infoUser.getDevice()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(program).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        .append(library).append(Constantes.SEPARATEUR_CHAINE_CHAR);
    if (listeparam != null) {
      sb.append(listeparam.size()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      for (i = 0; i < listeparam.size(); i++) {
        sb.append(listeparam.get(i)).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      }
    }
    else {
      sb.append("0").append(Constantes.SEPARATEUR_CHAINE_CHAR);
    }
    if (listebib != null) {
      sb.append(listebib.size()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      // On �crit les biblioth�ques en ordre invers� afin qu'elles soient dans le bon ordre sur le serveur � cause de la m�thode addBib
      // for (i=0; i<listebib.size(); i++)
      for (i = listebib.size() - 1; i >= 0; i--) {
        sb.append(listebib.get(i)).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      }
    }
    else {
      sb.append("0").append(Constantes.SEPARATEUR_CHAINE_CHAR);
    }
    
    // Ajout du point de menu
    sb.append(getPointMenu()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
    
    // A enlever plus tard (c'est pour trouver le bug rare qui envoi les bibs avec la mauvaise lettre de l'environnement)
    String chaine = sb.toString();
    getEnvUser().log.ecritureMessage("-[EnvProgram] (getDemandeSession)-> " + chaine);
    return chaine;
  }
  
  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Lib�re les ressources
   */
  public void dispose() {
    infoUser = null;
    if (listeparam != null) {
      listeparam.clear();
      listeparam = null;
    }
    if (listebib != null) {
      listebib.clear();
      listebib = null;
    }
  }
  
  // -- M�thodes prot�g�es --------------------------------------------------
  
  // -- M�thodes publiques --------------------------------------------------
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * Initialise le programme initial
   */
  public void setProgram(String valeur) {
    program = valeur.trim();
    
    // On v�rifie le type du programme (Java ou autres)
    if (program == null) {
      return;
    }
    
    boolean isClass = (program.toLowerCase().lastIndexOf(".class") != -1) || (program.toLowerCase().lastIndexOf(".jar") != -1);
    if (isClass) {
      setType(JAVA);
      program = program.replaceAll(".class", "").replaceAll(".jar", "");
    }
    else {
      setType(AS400);
    }
  }
  
  /**
   * Initialise la biblioth�que du programme
   */
  public void setLibraryEnv(String valeur) {
    libraryEnv = valeur.trim();
  }
  
  /**
   * Initialise la biblioth�que du programme
   */
  public void setLibrary(String valeur) {
    library = valeur.trim();
  }
  
  /**
   * Initialise le point de menu
   */
  public void setPointMenu(String valeur) {
    ptmenu = valeur.trim();
  }
  
  /**
   * Retourne l'environnement utilisateur
   */
  public EnvUser getEnvUser() {
    return infoUser;
  }
  
  /**
   * Retourne la liste des biblioth�ques
   */
  public ArrayList<String> getListeBib() {
    return listebib;
  }
  
  /**
   * Retourne la liste des param�tres
   */
  public ArrayList<String> getListeParam() {
    return listeparam;
  }
  
  /**
   * Retourne le programme
   */
  public String getProgram() {
    return program;
  }
  
  /**
   * Retourne la biblioth�que du programme
   */
  public String getLibraryEnv() {
    return libraryEnv;
  }
  
  /**
   * Retourne la biblioth�que du programme
   */
  public String getLibrary() {
    return library;
  }
  
  /**
   * Retourne le point de menu
   */
  public String getPointMenu() {
    return ptmenu;
  }
  
  /**
   * @param type the type to set
   */
  public void setType(char type) {
    this.type = type;
  }
  
  /**
   * @return type
   */
  public char getType() {
    return type;
  }
  
  /**
   * @param libmenu the libmenu to set
   */
  public void setLibMenu(String libmenu) {
    if (libmenu != null) {
      this.libmenu = libmenu.trim();
    }
  }
  
  /**
   * @return the libmenu
   */
  public String getLibMenu() {
    return libmenu;
  }
  
}
