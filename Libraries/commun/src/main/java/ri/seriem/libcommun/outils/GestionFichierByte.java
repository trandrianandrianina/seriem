//=================================================================================================
//==>                                                                       03/11/2009 - 12/02/2015
//==> Gestion des fichiers byte
//==> A faire:
//=================================================================================================

package ri.seriem.libcommun.outils;

//ATTENTION LES CLES SONT SENSIBLES A LA CASSE
import java.io.File;
import java.io.RandomAccessFile;


public class GestionFichierByte
{
	// Constantes erreurs de chargement
	private final static String ERREUR_FICHIER_INTROUVABLE="Le fichier est introuvable.";
	private final static String ERREUR_LECTURE_FICHIER="Erreur lors de la lecture du fichier.";
//	private final static String ERREUR_ENREGISTREMENT_FICHIER="Erreur lors de l'enregistrement du fichier.";
//	private final static String ERREUR_BUFFER_VIDE="Le buffer est vide.";

	// Variables
	private String nomFichier=null;
	protected byte[] contenuFichier=null;
	protected File fichier = null;

	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

	/**
	 * Constructeur de la classe
	 */
	public GestionFichierByte()
	{
	}

	/**
	 * Constructeur de la classe
	 * @param nomFichier
	 */
	public GestionFichierByte(String nomFichier)
	{
		setNomFichier(nomFichier);
	}

	/**
	 * Constructeur de la classe
	 * @param fichier
	 */
	public GestionFichierByte(File nomFichier)
	{
		setNomFichier(nomFichier);
	}

	/**
	 * Initialise le nom du fichier
	 * @param nomFichier
	 */
	public void setNomFichier(String nomFichier)
	{
		setNomFichier(new File(nomFichier));
	}

	/**
	 * Initialise le nom du fichier
	 * @param afichier
	 */
	public void setNomFichier(File afichier)
	{
		if( afichier == null ){
			fichier = null;
			nomFichier = null;
		}
		nomFichier = afichier.getAbsolutePath();
		fichier = afichier;
		videContenuFichier();
	}

	/**
	 * Retourne le nom du fichier
	 * @return
	 */
	public String getNomFichier()
	{
		return nomFichier;
	}

	/**
	 * V�rifie l'existence d'un fichier
	 * @return
	 */
	private boolean isPresent()
	{
		if (fichier == null) return false;
		return fichier.exists();
	}

	/**
	 * Vide la contenu du buffer
	 */
	public void videContenuFichier()
	{
		this.contenuFichier = null;
	}
	
	/**
	 * Initialise le buffer 
	 * @param contenuFichier
	 */
	public void setContenuFichier(byte[] contenuFichier)
	{
		this.contenuFichier = contenuFichier; 
	}

	/**
	 * Retourne le buffer
	 * @param contenuFichier
	 */
	public byte[] getContenuFichier()
	{
		if (contenuFichier == null)
			if (lectureFichier() == Constantes.ERREUR) 
				return null;
		
		return this.contenuFichier; 
	}

	/**
	 * Lecture du fichier texte
	 * @return
	 */
	public int lectureFichier()
	{
		// On v�rifie que le fichier existe
		if (!isPresent())
		{
			msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE;
			return Constantes.FALSE;
		}

		// On lit le fichier
		if (contenuFichier == null) contenuFichier = new byte[(int)fichier.length()];
		try
		{
			RandomAccessFile raf = new RandomAccessFile(nomFichier, "r");
			raf.read(contenuFichier);//, 0, Constantes.TAILLE_BUFFER);
			raf.close();
		}
		catch (Exception e)
		{
			msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + e;
			return Constantes.FALSE;
		}

		return Constantes.OK;
	}

	/**
	 * Retourne le file
	 * @return
	 */
	public File getFile()
	{
		return fichier;
	}
	/**
	 * D�termine le retour chariot
	 * @param crlf
	 *
	public void setRetourChariot(String acrlf)
	{
		crlf = acrlf;
	}*/
	
	/**
	 * Ecrit le fichier texte
	 * @return
	 *
	public int ecritureFichier()
	{
		int i=0;

		if (contenuFichier == null)
		{
			msgErreur = ERREUR_BUFFER_VIDE;
			return Constantes.ERREUR;
		}

		// On cr�� le fichier
		try
		{
			FileWriter f = new FileWriter(nomFichier);
			for (i=0; i<contenuFichier.size(); i++) 
				f.write(contenuFichier.get(i)+ crlf);
			f.flush();
			f.close();
		}
		catch (IOException e) 
		{
			msgErreur = ERREUR_ENREGISTREMENT_FICHIER + Constantes.crlf + e;
			return Constantes.ERREUR;
		}

		return Constantes.OK;
	}*/

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
