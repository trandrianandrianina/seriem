//=================================================================================================
//==>                                                                       27/10/2006 - 13/05/2015
//==> Gestion des fichiers texte
//==> A faire:
//=================================================================================================

package ri.seriem.libcommun.outils;

//ATTENTION LES CLES SONT SENSIBLES A LA CASSE
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JFileChooser;


public class GestionFichierTexte
{
	// Constantes erreurs de chargement
	private final static String ERREUR_FICHIER_INTROUVABLE="Le fichier est introuvable.";
	private final static String ERREUR_LECTURE_FICHIER="Erreur lors de la lecture du fichier.";
	private final static String ERREUR_ENREGISTREMENT_FICHIER="Erreur lors de l'enregistrement du fichier.";
	private final static String ERREUR_BUFFER_VIDE="Le buffer est vide.";

	// Constantes
	public final static String FILTER_EXT_TXT="txt";
	public final static String FILTER_DESC_TXT="Fichier texte (*.txt)";

	// Variables
	private String crlf=Constantes.crlf;
	private String nomFichier=null;
	private URL urlFichier=null;
	protected ArrayList<String> contenuFichier=null;
	protected File fichier = null;
	//private int nombreLignes = 0;

	protected String msgErreur="";          // Conserve le dernier message d'erreur émit et non lu
	

	/**
	 * Constructeur de la classe
	 */
	public GestionFichierTexte()
	{
	}

	/**
	 * Constructeur de la classe
	 * @param nomFichier
	 */
	public GestionFichierTexte(String nomFichier)
	{
		setNomFichier(nomFichier);
	}

	/**
	 * Constructeur de la classe
	 * @param fichier
	 */
	public GestionFichierTexte(File afichier)
	{
		setNomFichier(afichier);
	}

	/**
	 * Constructeur de la classe
	 * @param urlFichier
	 */
	public GestionFichierTexte(URL urlFichier)
	{
		nomFichier = null;
		fichier = null;
		this.urlFichier = urlFichier;
	}

	/**
	 * Initialise le nom du fichier
	 * @param nomFichier
	 */
	public void setNomFichier(String nomFichier)
	{
		setNomFichier(nomFichier, true);
	}

	/**
	 * Initialise le nom du fichier
	 * @param nomFichier
	 */
	public void setNomFichier(File afichier)
	{
		setNomFichier(afichier, true);
	}

	/**
	 * Initialise le nom du fichier
	 * @param nomFichier
	 * @param videbuffer
	 */
	public void setNomFichier(String nomFichier, boolean videbuffer)
	{
		setNomFichier(new File(nomFichier), videbuffer);
	}

	/**
	 * Initialise le nom du fichier
	 * @param afichier
	 * @param videbuffer
	 */
	public void setNomFichier(File afichier, boolean videbuffer)
	{
		if( afichier == null ){
			fichier = null;
			nomFichier = null;
		}
		nomFichier = afichier.getAbsolutePath();
		fichier = afichier;
		if( videbuffer ) videContenuFichier();
	}

	/**
	 * Retourne le nom du fichier
	 * @return
	 */
	public String getNomFichier()
	{
		return nomFichier;
	}

	/**
	 * Vérifie l'existence d'un fichier
	 * @return
	 */
	public boolean isPresent()
	{
		if (fichier == null) return false;
		return fichier.exists();
	}

	/**
	 * Vide le contenu du buffer
	 */
	public void videContenuFichier()
	{
		//if( this.contenuFichier != null ) this.contenuFichier.clear();
		this.contenuFichier = null;
	}
	
	/**
	 * Initialise le buffer 
	 * @param contenuFichier
	 */
	public void setContenuFichier(ArrayList<String> contenuFichier)
	{
		this.contenuFichier = contenuFichier; 
	}

	/**
	 * Initialise le buffer 
	 * @param contenuFichier
	 */
	public void setContenuFichier(String[] contenuFichier)
	{
		this.contenuFichier = new ArrayList<String>(contenuFichier.length);
		for (int i=0; i<contenuFichier.length; i++)
			this.contenuFichier.add(contenuFichier[i]); 
	}

	/**
	 * Initialise le buffer 
	 * @param contenuFichier
	 */
	public void setContenuFichier(String contenuFichier)
	{
		if (contenuFichier == null) return;
		String[] liste = contenuFichier.split("\n");
		setContenuFichier(liste);
	}

	/**
	 * Retourne le buffer
	 * @param contenuFichier
	 */
	public ArrayList<String> getContenuFichier()
	{
		if (contenuFichier == null)
			if (lectureFichier() == Constantes.ERREUR) 
				return null;
		
		return this.contenuFichier; 
	}

	/**
	 * Retourne le buffer
	 * @param contenuFichier
	 */
	public String[] getContenuFichierTab()
	{
		if (contenuFichier == null)
			if (lectureFichier() == Constantes.ERREUR) 
				return null;

		String[] tab = new String[contenuFichier.size()]; 
		return (String[]) contenuFichier.toArray(tab); 
	}

	/**
	 * Retourne le buffer
	 * @param contenuFichier
	 */
	public String getContenuFichierString(boolean crlf)
	{
		if (contenuFichier == null)
			if (lectureFichier() == Constantes.ERREUR) 
				return null;
		
		// On concatène les lignes
		StringBuilder sb = new StringBuilder(contenuFichier.size() * 512);
		if( crlf ){
			for (String chaine: contenuFichier)
				sb.append(chaine).append('\n');
		} else{
			for (String chaine: contenuFichier)
				sb.append(chaine);
		}	
		return sb.toString(); 
	}
	

	/**
	 * Lecture du fichier texte
	 * @return
	 */
	public int lectureFichier()
	{
		String chaine=null;

		// On vérifie que le fichier existe
		if ((urlFichier == null) && !isPresent())
		{
			msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE;
			return Constantes.FALSE;
		}

		// On lit le fichier
		if (contenuFichier == null) contenuFichier = new ArrayList<String>();
		try
		{
			BufferedReader f = null;
			if (fichier != null)
				f = new BufferedReader(new FileReader(fichier));
			else
				f = new BufferedReader(new InputStreamReader(urlFichier.openStream()));

			// Lecture du fichier
			chaine = f.readLine();
			while(chaine != null)
			{
				contenuFichier.add(chaine);
				chaine = f.readLine();
				//nombreLignes++;
			}
			f.close();
		}
		catch (Exception e)
		{
			msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + e;
			return Constantes.FALSE;
		}

		return Constantes.OK;
	}
	
	/**
	 * Retourne le nombre de lignes contenues par le fichier
	 */
	public int getNombreLignes() {
		if (contenuFichier == null)
			return 0;
		return contenuFichier.size();
		//return nombreLignes;
	}

	/*
	public void setNombreLignes(int nombreLignes) {
		this.nombreLignes = nombreLignes;
	}*/

	/**
	 * Détermine le retour chariot
	 * @param crlf
	 */
	public void setRetourChariot(String acrlf)
	{
		crlf = acrlf;
	}
	
	/**
	 * Ecrit le fichier texte
	 * @return
	 */
	public int ecritureFichier()
	{
		int i=0;

		if (contenuFichier == null)
		{
			msgErreur = ERREUR_BUFFER_VIDE;
			return Constantes.ERREUR;
		}

		// On créé le fichier
		try
		{
		  FileWriter f = new FileWriter(nomFichier);
      for (i=0; i<contenuFichier.size(); i++) {
        // On n'ajoute pas de retour à ligne à la fin du fichier
        if (i == contenuFichier.size() - 1) {
          f.write(contenuFichier.get(i));
        }
        else {
          f.write(contenuFichier.get(i)+ crlf);
        }       
      }
      f.flush();
      f.close();
		}
		catch (IOException e) 
		{
			msgErreur = ERREUR_ENREGISTREMENT_FICHIER + Constantes.crlf + e;
			return Constantes.ERREUR;
		}

		return Constantes.OK;
	}

	/**
	 * Retourne les filtres possible pour les boites de dialogue
	 * @param jfc
	 */
	public void initFiltre(JFileChooser jfc)
	{
		jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_TXT}, FILTER_DESC_TXT));
	}

	/**
	 * Libère la mémoire 
	 */
	public void dispose()
	{
		if (contenuFichier != null)
			contenuFichier.clear();
		contenuFichier = null;
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
