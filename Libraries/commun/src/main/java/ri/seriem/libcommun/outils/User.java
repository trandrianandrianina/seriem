//=================================================================================================
//==>                                                                       24/06/2010 - 19/11/2015
//==> Gestion des informations importantes pour un utilisateur
//==> ATTENTION: Ne pas utiliser d'instruction au dela de java 1.5
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.File;
import java.util.UUID;

public class User
{
	// Variables
    protected LogMessage log=null;
    protected String profil="";							// Profil de l'utilisateur
    protected String mdp="";							// Mot de passe
	protected StringIP adresseIP = new StringIP("");	// Adresse IP de l'AS400

    protected String dossierRacine=File.separator + Constantes.DOSSIER_RACINE;		// Chemin racine o� sont stock� les runtimes et autres donn�es
    protected String dossierTravail=null;											// Chemin de travail de l'environnement choisit
    protected String codepageServeur=System.getProperty("file.encoding");
    protected String id = UUID.randomUUID().toString();
    private String suffixeLog="";


    /**
     * Constructeur
     */
    public User()
    {
    	this("");
    }
    
    /**
     * Constructeur
     * @param suf
     */
    public User(String suf)
    {
    	setSuffixeLog(suf);
    }

    /**
     * Initialise le dossier racine
     */
    public void setSuffixeLog(String suf)
    {
    	suffixeLog = suf;
    }

    /**
     * Initialise le dossier racine
     */
    public void setDossierRacine(String valeur)
    {
    	dossierRacine = valeur.trim();
    	if ((dossierRacine != null) && (log != null)) log = new LogMessage(dossierRacine + File.separator + Constantes.DOSSIER_LOG, suffixeLog);
    }

    /**
     * Retourne le dossier racine
     */
    public String getDossierRacine()
    {
    	return dossierRacine;
    }

    /**
     * Initialise le profil
     */
    public void setProfil(String valeur)
    {
    	if (valeur != null)
    		profil = valeur.trim();
    }

    /**
     * Retourne le profil
     */
    public String getProfil()
    {
        return profil;
    }

    /**
     * Initialise le mot de passe
     */
    public void setMotDePasse(String valeur)
    {
    	mdp = valeur;
    }

    /**
     * Retourne le mot de passe
     */
    public String getMotDePasse()
    {
        return mdp;
    }

	/**
	 * @return codepageServeur
	 */
	public String getCodepageServeur()
	{
		return codepageServeur;
	}

	/**
	 * @param codepageServeur codepageServeur � d�finir
	 */
	public void setCodepageServeur(String codepageServeur)
	{
		this.codepageServeur = codepageServeur;
	}

	/**
	 * @return the id
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		if (id == null) id = UUID.randomUUID().toString();
		return id;
	}

    /**
     * Initialise le dossier travail
     */
    public void setDossierTravail(String valeur)
    {
    	dossierTravail = valeur.trim();
    }

    /**
     * Initialise le fichier de log
     */
    public void setLog(LogMessage log)
    {
    	this.log = log;
    }

    /**
     * Retourne le dossier travail de l'environnement en cours
     */
    public String getDossierTravail()
    {
    	return dossierTravail;
    }

    /**
     * Retourne le dossier de config
     */
    public String getDossierConfig()
    {
    	File dossier = new File(getDossierRacine() + File.separatorChar + Constantes.DOSSIER_CONFIG); 
    	if (!dossier.exists()) dossier.mkdirs();
    	return dossier.getAbsolutePath();
    }

    /**
     * Retourne le dossier des menus
     */
    public String getDossierMenus()
    {
    	File dossier = new File(getDossierRacine() + File.separatorChar + Constantes.DOSSIER_CONFIG + File.separatorChar + Constantes.DOSSIER_MENUS ); 
    	if (!dossier.exists()) dossier.mkdirs();
    	return dossier.getAbsolutePath();
    }

    /**
     * Retourne le dossier temporaire g�n�ral
     */
    public String getDossierTemp()
    {
    	File dossier = new File(getDossierRacine() + File.separatorChar + Constantes.DOSSIER_TMP); 
    	if (!dossier.exists()) dossier.mkdirs();
    	return dossier.getAbsolutePath();
    }

    /**
     * Retourne le dossier temporaire pour l'utilisateur
     */
    public String getDossierTempUser()
    {
    	File dossier = new File(getDossierTemp() + File.separatorChar + getId()); 
    	if (!dossier.exists()) dossier.mkdirs();
    	return dossier.getAbsolutePath();
    }

    /**
     * Retourne le fichier de log
     */
    public LogMessage getLog()
    {
    	if (log == null) log = new LogMessage(dossierRacine + File.separator + Constantes.DOSSIER_LOG, suffixeLog);
        return log;
    }

	/**
	 * Retourne l'adresse IP du client
	 * @return
	 */
	public String getAdresseIP()
	{
		return adresseIP.getIP();
	}

	/**
	 * Initialise l'adresse IP du client
	 * @param adresseIP
	 */
	public void setAdresseIP(String adresseIP)
	{
		this.adresseIP.setIP(adresseIP);
	}
	
	/**
	 * Permet de tester les droits d'�criture sur le dossier racine (cas du home sur certaines configurations de sessions Windows)
	 * @return
	 */
	public boolean isCanWrite()
	{
		File racine = new File(getDossierRacine());
		return racine.canWrite();
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
	    log = null;
		adresseIP.dispose();
		adresseIP = null;
	}
}
