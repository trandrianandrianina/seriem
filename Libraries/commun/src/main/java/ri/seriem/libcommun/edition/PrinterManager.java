/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.util.ArrayList;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaTray;
import javax.print.attribute.standard.PrinterName;

/**
 * Gestion des imprimantes sur un poste
 */
public class PrinterManager {
  // Variables
  private PrintService printService[] = null;
  private PrintService defaultPrintService = null;
  
  /**
   * Retourne le service d'impression par d�faut
   * @param refresh
   * @return
   */
  public PrintService getDefaultService(boolean refresh) {
    if ((defaultPrintService == null) || refresh) {
      defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
    }
    return defaultPrintService;
  }
  
  /**
   * Retourne le nom de l'imprimante par d�faut
   * @param refresh
   * @return
   */
  public String getDefaultPrinter(boolean refresh) {
    if ((defaultPrintService == null) || refresh) {
      getDefaultService(refresh);
    }
    if (defaultPrintService != null) {
      return defaultPrintService.getName();
    }
    return "";
  }
  
  /**
   * Retourne la liste de tous les services d'impression
   * @param refresh
   * @return
   */
  public PrintService[] getListServices(boolean refresh) {
    if ((defaultPrintService == null) || refresh) {
      PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
      DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
      printService = PrintServiceLookup.lookupPrintServices(flavor, pras);
    }
    return printService;
  }
  
  /**
   * Retourne la liste des imprimantes
   * @param refresh
   * @param withemptyline
   * @return
   */
  public String[] getListPrinters(boolean refresh, boolean withemptyline) {
    if ((defaultPrintService == null) || refresh) {
      PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
      DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
      printService = PrintServiceLookup.lookupPrintServices(flavor, pras);
    }
    String[] listPrinter = null;
    if (printService != null) {
      int i = 0;
      if (withemptyline) {
        listPrinter = new String[printService.length + 1];
        listPrinter[i++] = "";
      }
      else {
        listPrinter = new String[printService.length];
      }
      for (PrintService service : printService) {
        listPrinter[i++] = service.getName();
      }
    }
    return listPrinter;
  }
  
  /**
   * Retourne la liste des tiroirs d'entr�es
   * @param printerName
   * @return
   */
  public Media[] getListInputTray(String printerName) {
    AttributeSet aset = new HashAttributeSet();
    aset.add(new PrinterName(printerName, null));
    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, aset);
    ArrayList<Media> inputTray = new ArrayList<Media>();
    
    // we chose something compatible with the printable interface
    DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
    
    for (PrintService service : services) {
      // we retrieve all the supported attributes of type Media
      // we can receive MediaTray, MediaSizeName, ...
      Object o = service.getSupportedAttributeValues(Media.class, flavor, null);
      if (o != null && o.getClass().isArray()) {
        for (Media media : (Media[]) o) {
          // we collect the MediaTray available
          if (media instanceof MediaTray) {
            inputTray.add(media);
          }
        }
      }
    }
    Media[] list = new Media[inputTray.size()];
    return inputTray.toArray(list);
  }
  
}
