package ri.seriem.libcommun.serien.tableModel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.table.AbstractTableModel;

public class ParametersTableModel extends AbstractTableModel implements	PropertyChangeListener
{
	private static final long serialVersionUID = 1L;

	private final MyObjectManager manager;


	public ParametersTableModel()
	{
		super();
		this.manager = new MyObjectManager();
		manager.propertyChangeSupport.addPropertyChangeListener(this);
		for (MyObject object : manager.getObjects())
		{
			object.getPropertyChangeSupport().addPropertyChangeListener(this);
		}
	}

	public ParametersTableModel(MyObjectManager amanager)
	{
		super();
		this.manager = amanager;
		manager.propertyChangeSupport.addPropertyChangeListener(this);
		for (MyObject object : manager.getObjects())
		{
			object.getPropertyChangeSupport().addPropertyChangeListener(this);
		}
	}

	public MyObjectManager getMyObjectManager()
	{
		return manager;
	}
	
	public void propertyChange(PropertyChangeEvent evt)
	{
		if (evt.getSource() == manager)
		{
			// OK, not the cleanest thing, just to get the gist of it.
			if (evt.getPropertyName().equals("objects"))
			{
				((MyObject) evt.getNewValue()).getPropertyChangeSupport().addPropertyChangeListener(this);
			}
			fireTableDataChanged();
		}
		else
			if (evt.getSource() instanceof MyObject)
			{
				int index = manager.getObjects().indexOf(evt.getSource());
				fireTableRowsUpdated(index, index);
			}
	}

	public int getColumnCount()
	{
		return 3;
	}

	public int getRowCount()
	{
		return manager.getObjects().size();
	}

	public MyObject getValueAt(int row)
	{
		return manager.getObjects().get(row);
	}

	public void removeRow(int rowIndex)
	{
		manager.removeObject(rowIndex);
	}
	
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
				return getValueAt(rowIndex).getValue(columnIndex);
			case 1:
				return getValueAt(rowIndex).getValue(columnIndex);
			case 2:
				return getValueAt(rowIndex).isSelected();
		}
		return null;
	}

	public void setValueAt(Object value, int rowIndex, int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
				getValueAt(rowIndex).setValue(columnIndex, (String) value);
				break;
			case 1:
				getValueAt(rowIndex).setValue(columnIndex, (String) value);
				break;
			case 2:
				getValueAt(rowIndex).setSelected(Boolean.TRUE.equals(value));
				break;
		}
	}

	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		return columnIndex == 2;
	}

	public Class<?> getColumnClass(int column)
	{
		switch (column)
		{
			case 0:
				return String.class;
			case 1:
				return String.class;
			case 2:
				return Boolean.class;
		}
		return Object.class;
	}

	public String getColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "R�f�rence";
			case 1:
				return "Adresse IP";
			case 2:
				return "D�faut";
		}
		return null;
	}

}
