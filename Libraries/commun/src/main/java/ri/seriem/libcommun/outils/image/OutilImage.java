/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

/**
 * Classe statique contenant des m�thodes utilitaires pour la manipulation des images.
 * 
 * Des m�thodes statiques permettent de travailler directement avec des instances de Color :
 * - isCouleurSombre(...)
 * - assombrirCouleur(...)
 * - eclaircirCouleur(...)
 * - getCouleurComplementaire(...)
 * 
 * Des m�thodes statiques permettent de manipuler des instances de ImageIcon :
 * - assombriIcone()
 * - eclaircirIcone()
 * - griserIcone()
 * - redimensionnerIcone()
 * - reduireIcone()
 */
public class OutilImage {
  /**
   * Indiquer si une couleur est consid�r�e comme sombre (luminosit� < 50%)
   * @return true=la couleur est sombre, false=la couleur est claire.
   */
  public static boolean isCouleurSombre(Color pColorRGB) {
    // Convertir la couleur en HSL
    HSLColor hslColor = new HSLColor(pColorRGB);
    
    // Tester la luminosit�
    return hslColor.getLuminosite() < 50;
  }
  
  /**
   * Assombir une couleur RGB d'un certain pourcentage.
   *
   * @param pColorRGB La couleur RGB � �claircir.
   * @param pPourcentage Pourcentage d'assombrissement entre 0 et 1 (0=ne change rien, 1=couleur noire).
   * @return La couleur �claircie au format RGB.
   */
  public static Color assombrirCouleur(Color pColorRGB, float pPourcentage) {
    // Retourner du noir si aucune couleur n'est fournie en param�tre
    if (pColorRGB == null) {
      return Color.BLACK;
    }
    
    // Retourner la couleur inchang�e si la pourcentage est invalide
    if (pPourcentage < 0) {
      return pColorRGB;
    }
    
    // Calculer la couleur assombrie
    HSLColor hslColor = new HSLColor(pColorRGB);
    hslColor.assombrir(pPourcentage);
    return hslColor.getRGB();
  }
  
  /**
   * Eclaircir une couleur RGB d'un certain pourcentage.
   *
   * @param pColorRGB La couleur RGB � assombrir.
   * @param pPourcentage Pourcentage d'�claircissement entre 0 et 1 (0=ne change rien, 1=couleur blanche).
   * @return La couleur assombrie au format RGB.
   */
  public static Color eclaircirCouleur(Color pColorRGB, float pPourcentage) {
    // Retourner du blanc si aucune couleur n'est fournie en param�tre
    if (pColorRGB == null) {
      return Color.WHITE;
    }
    
    // Retourner la couleur inchang�e si la pourcentage est invalide
    if (pPourcentage < 0) {
      return pColorRGB;
    }
    
    // Calculer la couleur �claircie
    HSLColor hslColor = new HSLColor(pColorRGB);
    hslColor.eclaircir(pPourcentage);
    return hslColor.getRGB();
  }
  
  /**
   * Griser une couleur RGB.
   *
   * @param pColorRGB La couleur RGB � assombrir. *
   * @return La couleur gris�e au format RGB.
   */
  public static Color griserCouleur(Color pColorRGB) {
    // Retourner du blanc si aucune couleur n'est fournie en param�tre
    if (pColorRGB == null) {
      return null;
    }
    
    // Calculer la couleur �claircie
    HSLColor hslColor = new HSLColor(pColorRGB);
    hslColor.griser();
    return hslColor.getRGB();
  }
  
  /**
   * Retourner la couleur RGB compl�mentaire.
   * La couleur compl�mentaire est d�termin�e en ajoutant 180 degr� � la teinte (modulo 360).
   * @return Couleur compl�mentaire au format RGB.
   */
  public static Color getCouleurComplementaire(Color pColorRGB) {
    // Convertir la couleur en HSL
    HSLColor hslColor = new HSLColor(pColorRGB);
    
    // Calculer la couleur compl�mentaire
    float nouvelleTeinte = (hslColor.getTeinte() + 180.0f) % 360.0f;
    hslColor.setTeinte(nouvelleTeinte);
    return hslColor.getRGB();
  }
  
  /**
   * Assombir l'image d'une ic�ne.
   * 
   * @Param pImageIcon L'ic�ne � assombrir.
   * @param pPourcentage Pourcentage d'assombrissement entre 0 et 1 (0=ne change rien, 1=couleur noire).
   * @return Ic�ne modifi�e.
   */
  public static ImageIcon assombrirIcone(ImageIcon pImageIcon, float pPourcentage) {
    // V�rifier les param�tres
    if (pImageIcon == null) {
      return null;
    }
    
    // R�cup�rer les dimensions de l'ic�ne � modifier
    int largeur = pImageIcon.getIconWidth();
    int hauteur = pImageIcon.getIconHeight();
    
    // Cr�er une image manipulable en m�moire de la m�me taille
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Copier l'image de l'ic�ne dans cette image
    Graphics g = bufferedImage.createGraphics();
    g.drawImage(pImageIcon.getImage(), 0, 0, null);
    g.dispose();
    
    // Parcourir les pixels un par un
    for (int y = 0; y < hauteur; y++) {
      for (int x = 0; x < largeur; x++) {
        int rgb = bufferedImage.getRGB(x, y);
        Color c = new Color(rgb, true);
        c = OutilImage.assombrirCouleur(c, pPourcentage);
        bufferedImage.setRGB(x, y, c.getRGB());
      }
    }
    
    // Convertir le r�sultat en ImageIcon
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Eclaircir l'image d'une ic�ne.
   * 
   * @Param pImageIcon L'ic�ne � �claircir.
   * @param pPourcentage Pourcentage d'�claircissement entre 0 et 1 (0=ne change rien, 1=couleur blanche).
   * @return Ic�ne modifi�e.
   */
  public static ImageIcon eclaircirIcone(ImageIcon pImageIcon, float pPourcentage) {
    // V�rifier les param�tres
    if (pImageIcon == null) {
      return null;
    }
    
    // R�cup�rer les dimensions de l'ic�ne � modifier
    int largeur = pImageIcon.getIconWidth();
    int hauteur = pImageIcon.getIconHeight();
    
    // Cr�er une image manipulable en m�moire de la m�me taille
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Copier l'image de l'ic�ne dans cette image
    Graphics g = bufferedImage.createGraphics();
    g.drawImage(pImageIcon.getImage(), 0, 0, null);
    g.dispose();
    
    // Parcourir les pixels un par un
    for (int y = 0; y < hauteur; y++) {
      for (int x = 0; x < largeur; x++) {
        int rgb = bufferedImage.getRGB(x, y);
        Color c = new Color(rgb, true);
        c = eclaircirCouleur(c, pPourcentage);
        bufferedImage.setRGB(x, y, c.getRGB());
      }
    }
    
    // Convertir le r�sultat en ImageIcon
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Griser l'image d'une ic�ne.
   * 
   * @Param pImageIcon L'ic�ne � griser.
   * @return Ic�ne modifi�e.
   */
  public static ImageIcon griserIcone(ImageIcon pImageIcon) {
    // V�rifier les param�tres
    if (pImageIcon == null) {
      return null;
    }
    
    // R�cup�rer les dimensions de l'ic�ne � modifier
    int largeur = pImageIcon.getIconWidth();
    int hauteur = pImageIcon.getIconHeight();
    
    // Cr�er une image manipulable en m�moire de la m�me taille
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Copier l'image de l'ic�ne dans cette image
    Graphics g = bufferedImage.createGraphics();
    g.drawImage(pImageIcon.getImage(), 0, 0, null);
    g.dispose();
    
    // Parcourir les pixels un par un
    for (int y = 0; y < hauteur; y++) {
      for (int x = 0; x < largeur; x++) {
        int rgb = bufferedImage.getRGB(x, y);
        Color c = new Color(rgb, true);
        c = griserCouleur(c);
        bufferedImage.setRGB(x, y, c.getRGB());
      }
    }
    
    // Convertir le r�sultat en ImageIcon
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Agrandir ou diminuer la taille d'une image en fonction d'un ratio.
   *
   * @param pImageIcon Image dont il faut changer la taille.
   * @param pRatio Si > 0, pourcentage d'agrandissement de l'image. Si < 0, pourcentage de diminution de l'image.
   * @return Image redimensionn�e.
   */
  static public ImageIcon redimensionnerIcone(ImageIcon pImageIcon, double pRatio) {
    // V�rifier les param�tres
    if (pImageIcon == null) {
      return null;
    }
    
    // Calculer la taille de la nouvelle image
    int largeur = pRatio > 0 ? (int) (pImageIcon.getIconWidth() * pRatio) : (int) (pImageIcon.getIconWidth() * (1 + pRatio));
    int hauteur = pRatio > 0 ? (int) (pImageIcon.getIconHeight() * pRatio) : (int) (pImageIcon.getIconHeight() * (1 + pRatio));
    
    // Cr�er une nouvelle image
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Dessiner sur le Graphics2D de l'image bufferis�e
    Graphics2D graphics2D = bufferedImage.createGraphics();
    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    graphics2D.drawImage(pImageIcon.getImage(), 0, 0, largeur, hauteur, null);
    graphics2D.dispose();
    
    // Retourner l'image modifi�e
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Diminuer la taille d'une image pour atteindre une largeur et une hauteur maximum.
   * 
   * L'image est redimensionn�e seulement si elle est plus grande que la largeur ou la hauteur souhait�e. Elle est redimensionn�e en
   * respectant les proportions (la hauteur et la largeur �voluent proportionnellement).
   *
   * @param pImageIcon Image dont il faut changer la taille.
   * @param pLargeurMax Largeur maximum de la nouvelle image.
   * @param pHauteurMax Hauteur maximum de la nouvelle image.
   * @return Image redimensionn�e.
   */
  static public ImageIcon reduireIcone(ImageIcon pImageIcon, int pLargeurMax, int pHauteurMax) {
    // V�rifier les param�tres
    if (pImageIcon == null || pLargeurMax < 0 || pHauteurMax < 0) {
      return null;
    }
    
    // Calculer le ratio pour ne pas d�passer la hauteur maximum
    double ratioHauteur = 0;
    if (pImageIcon.getIconHeight() > pHauteurMax) {
      ratioHauteur = (double) pHauteurMax / pImageIcon.getIconHeight();
    }
    else {
      ratioHauteur = 1;
    }
    
    // Calculer le ratio pour ne pas d�passer la largeur maximum
    double ratioLargeur = 0;
    if (pImageIcon.getIconHeight() > pLargeurMax) {
      ratioLargeur = (double) pLargeurMax / pImageIcon.getIconWidth();
    }
    else {
      ratioLargeur = 1;
    }
    
    // Redimensionner l'image suivant le plus petit des ratios
    if (ratioHauteur < ratioLargeur) {
      return redimensionnerIcone(pImageIcon, ratioHauteur);
    }
    else {
      return redimensionnerIcone(pImageIcon, ratioLargeur);
    }
  }
}
