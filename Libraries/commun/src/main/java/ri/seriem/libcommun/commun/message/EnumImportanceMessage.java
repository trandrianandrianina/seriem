/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.commun.message;

import ri.seriem.libcommun.outils.MessageErreurException;

/**
 * Importance d'un message.
 * 
 * Cela permet de qualifier le niveau d'importance d'un message textuel : soit normal, soit important. Les messages normaux sont
 * destin�s � �tre affich�s en noir tandis que les messages importants sont affich�s en rouge.
 */
public enum EnumImportanceMessage {
  NORMAL(0, "Normal"),
  MOYEN(1, "Moyen"),
  HAUT(2, "Haut");
  
  private final int code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumImportanceMessage(int pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persist�e en base de donn�es.
   */
  public int getCode() {
    return code;
  }
  
  /**
   * Le libell� associ� au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le code associ� dans une cha�ne de caract�re.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet �num par son code.
   */
  static public EnumImportanceMessage valueOfByCode(int pCode) {
    for (EnumImportanceMessage value : values()) {
      if (pCode == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("Le code du niveau d'importance du message est invalide : " + pCode);
  }
}
