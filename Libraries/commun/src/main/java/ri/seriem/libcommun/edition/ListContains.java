/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.table.DefaultTableModel;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ri.seriem.libcommun.outils.Constantes;

/**
 * Permet d'initialiser les donn�es d'une JTable et de g�n�rer son mod�le
 */
public class ListContains {
  // Variables
  private Object[][] Data = null;
  private String[] Title = null;
  private String[] Type = null;
  
  private JsonParser parser = new JsonParser();
  
  // private static SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
  private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
  
  /**
   * Constructeur
   */
  public ListContains() {
  }
  
  /**
   * Constructeur
   * @param data
   * @param title
   */
  public ListContains(Object[][] data, String[] title) {
    setData(data);
    setTitle(title);
  }
  
  public ListContains(String datajson) {
    convertJSONtoModel(datajson);
  }
  
  /**
   * Retourne les donn�es de la liste
   * @return the data
   */
  public Object[][] getData() {
    return Data;
  }
  
  /**
   * Initialise les donn�es
   * @param data the data to set
   */
  public void setData(Object[][] data) {
    Data = data;
  }
  
  /**
   * Retourne les titres
   * @return the title
   */
  public String[] getTitle() {
    return Title;
  }
  
  /**
   * Initialise les titres
   * @param title the title to set
   */
  public void setTitle(String[] title) {
    Title = title;
  }
  
  /**
   * @return le type
   *
   *         public Class<?>[] getType()
   *         {
   *         return Type;
   *         }
   * 
   *         /**
   * @param type le type � d�finir
   *
   *          public void setType(Class<?>[] type)
   *          {
   *          Type = type;
   *          }
   */
  
  /**
   * Retourne le mod�le pour la JTable
   * @return the modeleTable
   */
  public DefaultTableModel getModeleTable() {
    return new DefaultTableModel(Data, Title) {
      public Class<?> getColumnClass(int columnIndex) {
        if (Type != null) {
          if (Type[columnIndex].equals("Integer")) {
            return Integer.class;
          }
          else {
            if (Type[columnIndex].equals("Date")) {
              return Date.class;
            }
            else {
              return String.class;
            }
          }
        }
        else {
          if (Data != null) {
            return Data[0][columnIndex].getClass();
          }
          else {
            return String.class;
          }
        }
      }
      
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
  }
  
  /**
   * Converti une chaine JSON en titre et donn�es pour la table
   * @param data
   */
  public void convertJSONtoModel(String data) {
    JsonObject objet;
    int i = 0;
    int j = 0;
    try {
      objet = (JsonObject) parser.parse(data);
      // Titre
      JsonArray array = objet.get("TYPE").getAsJsonArray();
      if (array != null) {
        Type = new String[array.size()];
        for (i = 0; i < array.size(); i++) {
          Type[i] = array.get(i).getAsString();
        }
      }
      
      // Titre
      array = (JsonArray) objet.get("TITLE");
      Title = new String[array.size()];
      for (i = 0; i < array.size(); i++) {
        Title[i] = Constantes.code2unicode(array.get(i).getAsString());
      }
      
      // Donn�es
      JsonArray arraydata = objet.get("DATA").getAsJsonArray();
      Data = new Object[arraydata.size()][Title.length];
      for (i = 0; i < arraydata.size(); i++) {
        JsonArray arrayligne = arraydata.get(i).getAsJsonArray();
        for (j = 0; j < arrayligne.size(); j++) {
          if (Type[j].equals("Date")) {
            try {
              Date date = dateFormat.parse(arrayligne.get(j).getAsString());
              Data[i][j] = date;// DateFormat.getDateInstance( DateFormat.MEDIUM ).format(date);
            }
            catch (Exception ed) {
              ed.printStackTrace();
            }
          }
          else {
            Data[i][j] = arrayligne.get(j).getAsString();
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
}
