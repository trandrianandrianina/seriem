//=================================================================================================
//==> Utilitaire pour gestion des jars                                      06/06/2008 - 06/06/2008
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class JarUtils
{
	/**
	 * Retourne le numéro d'implémentation
	 * @param jar
	 * @return
	 * @throws java.io.IOException
	 */
	public static String getJarImplementationVersion(String jar)
	{
		JarFile jarfile;
		Manifest manifest;
		try
		{
			jarfile = new JarFile(jar);
			manifest = jarfile.getManifest();
		}
		catch (IOException e)
		{
			return "";
		}
		Attributes att = manifest.getMainAttributes();
		return att.getValue("Implementation-Version");
	}

	/**
	 * Retourne le numéro de spécification
	 * @param jar
	 * @return
	 * @throws java.io.IOException
	 */
	public static String getJarSpecificationVersion(String jar)
	{
		JarFile jarfile;
		Manifest manifest;
		try
		{
			jarfile = new JarFile(jar);
			manifest = jarfile.getManifest();
		}
		catch (IOException e)
		{
			return "";
		}
		Attributes att = manifest.getMainAttributes();
		return att.getValue("Specification-Version");
	}
	
	/**
	 * Retourne le numéro de spécification sous forme d'entier
	 * @param jar
	 * @return
	 */
	public static int getJarSpecificationVersionToInt(String jar)
	{
		try
		{
			return Integer.parseInt(getJarSpecificationVersion(jar));
		}
		catch(Exception e)
		{
			return 0;
		}
	}
}