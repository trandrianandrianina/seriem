/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.util.Date;

/**
 * Retourne la date et l'heure
 */
public class DateHeure {
  // Constantes pour la m�thode getFormateDateHeure
  // Constantes pour la m�thode getJourHeure
  public static final int AAAAMMJJ = 0;
  public static final int AAMMJJ = 1;
  public static final int JJMMAAAA = 2;
  public static final int JJMMAA = 3;
  public static final int HHMMSS = 4;
  public static final int HH_MM_SS = 5;
  public static final int HHMMSSMS = 6;
  public static final int HH_MM_SS_MS = 7;
  
  public static final int JJ_MM_AAAA = 12; // O� '_' doit �tre remplac� par '/'
  public static final int SAAMMJJ = 13;
  public static final int HHMM = 14;
  public static final int HH_MM = 15; // O� '_' doit �tre remplac� par ':'
  public static final int HH_MM_SS_ = 16; // O� '_' doit �tre remplac� par ':'
  
  /**
   * Formate la date et l'heure sous forme de chaine de caract�res
   * @param type
   * @return
   */
  public static String getJourHeure(int type) {
    return getJourHeure(type, new Date());
  }
  
  /**
   * Formate la date et l'heure sous forme de chaine de caract�res
   * @param type
   * @param date
   * @return
   */
  public static String getJourHeure(int type, Date date) {
    switch (type) {
      case 0:
        // (jour+"/"+mois+"/"+annee+" "+heure+":"+minute+":"+seconde+"."+milliseconde);
        return String.format("%1$td/%1$tm/%1$tY %1$tH:%1$tM:%1$tS.%1$tL", date);
      case 1:
        return String.format("%1$tH:%1$tM:%1$tS.%1$tL", date); // (heure+":"+minute+":"+seconde+"."+milliseconde);
      case 2:
        return String.format("%1$tY%1$tm%1$td,%1$tH%1$tM%1$tS", date); // (annee+mois+jour+","+heure+minute+seconde);
      case 3:
        return String.format("%1$tY%1$tm%1$td%1$tH%1$tM", date); // (annee+mois+jour+heure+minute);
      case 4:
        return String.format("%1$tY%1$tm%1$td", date); // (annee+mois+jour);
      case 5:
        return String.format("%1$ty%1$tm%1$td", date); // (annee.substring(2, 4)+mois+jour);
      case 6:
        return String.format("%1$td%1$tm%1$tY", date); // (jour+mois+annee);
      case 7:
        return String.format("%1$td%1$tm%1$ty", date); // (jour+mois+annee.substring(2, 4));
      case 8:
        return String.format("%1$tH%1$tM%1$tS", date); // (heure+minute+seconde);
      case 9:
        return String.format("%1$tH.%1$tM.%1$tS", date); // (heure+'.'+minute+'.'+seconde);
      case 10:
        return String.format("%1$tH%1$tM%1$tS%1$tL", date); // (heure+minute+seconde+milliseconde);
      case 11:
        return String.format("%1$tH.%1$tM.%1$tS.%1$tL", date); // (heure+'.'+minute+'.'+seconde+'.'+milliseconde);
      case 12:
        return String.format("%1$td/%1$tm/%1$tY", date); // (jour+"/"+mois+"/"+annee);
      case 13:
        String chaine = String.format("%1$tY%1$tm%1$td", date); // (siecle+annee+mois+jour);
        if (chaine.substring(0, 2).equals("19")) {
          return "0" + chaine.substring(2);
        }
        else {
          return "1" + chaine.substring(2);
        }
      case 14:
        return String.format("%1$tH%1$tM", date); // (heure+minute);
      case 15:
        return String.format("%1$tH:%1$tM", date); // (heure+":"+minute);
      case 16:
        return String.format("%1$tH:%1$tM:%1$tS", date); // (heure+":"+minute+":"+seconde);
    }
    return "";
  }
  
  /**
   * Converti une chaine dateheure d'un format vers un autre
   * @param formatori
   * @param formatconv
   * @param dateheure
   * @return
   */
  public static String getConvertJourHeure(int formatori, int formatconv, String dateheure, String valeurpardefaut) {
    if (dateheure == null) {
      return valeurpardefaut;
    }
    
    switch (formatori) {
      // (siecle+annee+mois+jour)
      case 13:
        if (dateheure.length() == 7) {
          switch (formatconv) {
            case 12: // (jour+"/"+mois+"/"+annee)
              dateheure = dateheure.substring(5) + '/' + dateheure.substring(3, 5) + '/' + (dateheure.length() == 6 ? "19" : "20")
                  + dateheure.substring(1, 3);
              break;
          }
        }
        else {
          dateheure = valeurpardefaut;
        }
        break;
      
      // (heure+minute)
      case 14:
        switch (formatconv) {
          case 15: // (heure+":"+minute)
            if (dateheure.length() == 4) {
              dateheure = dateheure.substring(0, 2) + ':' + dateheure.substring(2);
            }
            else if (dateheure.length() == 3) {
              dateheure = '0' + dateheure.substring(0, 1) + ':' + dateheure.substring(1);
            }
            else if (dateheure.equals("0")) {
              dateheure = "";
            }
            else {
              dateheure = "Err";
            }
            break;
          default:
            dateheure = valeurpardefaut;
        }
        break;
    }
    
    return dateheure;
  }
  
  /**
   * Formate la date et l'heure sous forme d'entier: AAMMJJHHMM
   * @return
   */
  public int getintJourHeure() {
    return Integer.parseInt(getJourHeure(3).substring(2));
  }
  
  /**
   * Remplace la variable date par la date exacte format�e � la demande
   * @param var
   * @return
   */
  public static String getFormateDateHeure(String var) {
    if (var == null) {
      return "";
    }
    
    var = var.trim();
    if (var.equals("@_AAAAMMJJ_@")) {
      return getJourHeure(4);
    }
    else if (var.equals("@_AAMMJJ_@")) {
      return getJourHeure(5);
    }
    else if (var.equals("@_JJMMAAAA_@")) {
      return getJourHeure(6);
    }
    else if (var.equals("@_JJMMAA_@")) {
      return getJourHeure(7);
    }
    else if (var.equals("@_HHMMSS_@")) {
      return getJourHeure(8);
    }
    else if (var.equals("@_HH.MM.SS_@")) {
      return getJourHeure(9);
    }
    else if (var.equals("@_HHMMSSMS_@")) {
      return getJourHeure(10);
    }
    else if (var.equals("@_HH.MM.SS.MS_@")) {
      return getJourHeure(11);
    }
    return "";
  }
  
  /**
   * Scanne une chaine et remplace les variables date par les dates exactes format�es � la demande
   * @param var
   * @return
   */
  public static String getScanDateHeure(String var) {
    if (var == null) {
      return "";
    }
    int pos = var.indexOf("@_AAAAMMJJ_@");
    if (pos != -1) {
      var = var.replaceAll("@_AAAAMMJJ_@", getJourHeure(4));
    }
    pos = var.indexOf("@_AAMMJJ_@");
    if (pos != -1) {
      var = var.replaceAll("@_AAMMJJ_@", getJourHeure(5));
    }
    pos = var.indexOf("@_JJMMAAAA_@");
    if (pos != -1) {
      var = var.replaceAll("@_JJMMAAAA_@", getJourHeure(6));
    }
    pos = var.indexOf("@_JJMMAA_@");
    if (pos != -1) {
      var = var.replaceAll("@_JJMMAA_@", getJourHeure(7));
    }
    pos = var.indexOf("@_HHMMSS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HHMMSS_@", getJourHeure(8));
    }
    pos = var.indexOf("@_HH.MM.SS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HH.MM.SS_@", getJourHeure(9));
    }
    pos = var.indexOf("@_HHMMSSMS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HHMMSSMS_@", getJourHeure(10));
    }
    pos = var.indexOf("@_HH.MM.SS.MS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HH.MM.SS.MS_@", getJourHeure(11));
    }
    
    return var;
  }
  
  /**
   * Formate la date
   * @param type
   * @return
   */
  public static String getFormateDateHeure(int type) {
    switch (type) {
      case AAAAMMJJ:
        return getJourHeure(4);
      case AAMMJJ:
        return getJourHeure(5);
      case JJMMAAAA:
        return getJourHeure(6);
      case JJMMAA:
        return getJourHeure(7);
      case HHMMSS:
        return getJourHeure(8);
      case HH_MM_SS:
        return getJourHeure(9);
      case HHMMSSMS:
        return getJourHeure(10);
      case HH_MM_SS_MS:
        return getJourHeure(11);
      case JJ_MM_AAAA:
        return getJourHeure(12);
    }
    return "";
  }
  
  /**
   * Retourne la variable format�e
   * @param type
   * @return
   */
  public static String getNomVariable(int type) {
    switch (type) {
      // Date
      case JJMMAAAA:
        return "@_JJMMAAAA_@";
      case JJMMAA:
        return "@_JJMMAA_@";
      case AAAAMMJJ:
        return "@_AAAAMMJJ_@";
      case AAMMJJ:
        return "@_AAMMJJ_@";
      // Heure
      case HHMMSS:
        return "@_HHMMSS_@";
      case HH_MM_SS:
        return "@_HH.MM.SS_@";
      case HHMMSSMS:
        return "@_HHMMSSMS_@";
      case HH_MM_SS_MS:
        return "@_HH.MM.SS.MS_@";
      
      default:
        return "";
    }
  }
  
}
