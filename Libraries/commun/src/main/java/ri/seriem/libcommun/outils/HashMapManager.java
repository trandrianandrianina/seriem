//=================================================================================================
//==>                                                                       22/04/2010 - 03/11/2010
//==> Gestion dd'�v�nement sur une HashMap
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.event.EventListenerList;

public class HashMapManager
{
    // Variables
    private EventListenerList listeners=new EventListenerList();
    private HashMap map = new HashMap();
    private boolean avtClear=false, avtAdd=false, avtRemove=true; // Param�tre par d�faut (avant la modif), 
    															  // l'execution des �couteurs avant l'action
 
    /**
     * Constructeur
     */
    public HashMapManager()
    {
    	this(false, false, true);
    }
    
    /**
     * Constructeur
     * @param avtclear
     * @param avtadd
     * @param avtremove
     */
    public HashMapManager(boolean avtclear, boolean avtadd, boolean avtremove)
    {
    	avtClear = avtclear;
    	avtAdd = avtadd;
    	avtRemove = avtremove;
    }

    public static interface Listener extends EventListener
    {
        public void onDataCleared();
        public void onDataAdded(Object cle, Object val);
        public void onDataRemoved(Object cle);
    }

    public void addListener(Listener l)
    {
        listeners.add(Listener.class, l);
    }
    
    public void removeListener(Listener l)
    {
        listeners.remove(Listener.class, l);
    }

    public HashMap getHashMap()
    {
    	return map;
    }
    
    public void clearObject()
    {
    	if (avtClear)
    	{
            //for (Listener l : listeners.getListeners(Listener.class))
            EventListener[] el = listeners.getListeners(Listener.class);
            for(int i=0; i<el.length; i++)
            	((Listener) el[i]).onDataCleared();
            map.clear();
    	}
    	else
    	{
    		map.clear();
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(Listener.class);
    		for(int i=0; i<el.length; i++)
    			((Listener) el[i]).onDataCleared();
    	}
    }
    
    public void addObject(Object cle, Object val)
    {
    	if (avtAdd)
    	{
            //for (Listener l : listeners.getListeners(Listener.class))
            EventListener[] el = listeners.getListeners(Listener.class);
            for(int i=0; i<el.length; i++)
            	((Listener) el[i]).onDataAdded(cle, val);
            map.put(cle, val);
    	}
    	else
    	{
    		map.put(cle, val);
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(Listener.class);
    		for(int i=0; i<el.length; i++)
    			((Listener) el[i]).onDataAdded(cle, val);
    	}
    }

    public void removeObject(Object cle)
    {
    	if (avtRemove)
    	{
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(Listener.class);
    		for(int i=0; i<el.length; i++)
    			((Listener) el[i]).onDataRemoved(cle);
    		map.remove(cle);
    	}
    	else
    	{
            map.remove(cle);
            //for (Listener l : listeners.getListeners(Listener.class))
            EventListener[] el = listeners.getListeners(Listener.class);
            for(int i=0; i<el.length; i++)
            	((Listener) el[i]).onDataRemoved(cle);
    	}
    }

    public Object getObject(Object cle)
    {
        return map.get(cle);
    }

    public Object getKeyAtIndex(int index)
    {
    	if (index < map.size())
    	{
        	Set myKeys = map.keySet();
        	Iterator myKeysIterator = myKeys.iterator();

    		while (myKeysIterator.hasNext())
    		{
    			Object cle = myKeysIterator.next();
    			if (index-- == 0) return cle;
    		}
    	}
    	return null;
    }

    public Object getValueAtIndex(int index)
    {
    	return map.get(getKeyAtIndex(index));
    }
}
