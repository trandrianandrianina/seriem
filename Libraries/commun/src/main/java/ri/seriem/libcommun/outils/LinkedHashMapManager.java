//=================================================================================================
//==>                                                                       01/06/2010 - 03/11/2010
//==> Gestion dd'�v�nement sur une LinkedHashMap
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.util.EventListener;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.swing.event.EventListenerList;

public class LinkedHashMapManager
{
    // Variables
    private EventListenerList listeners=new EventListenerList();
    private LinkedHashMap map = new LinkedHashMap();
    private boolean avtClear=false, avtAdd=false, avtRemove=true; // Param�tre par d�faut (avant la modif), l'execution des �couteurs avant l'action

    /**
     * Constructeur
     */
    public LinkedHashMapManager()
    {
    	this(false, false, true);
    }
    
    /**
     * Constructeur
     * @param avtclear
     * @param avtadd
     * @param avtremove
     */
    public LinkedHashMapManager(boolean avtclear, boolean avtadd, boolean avtremove)
    {
    	avtClear = avtclear;
    	avtAdd = avtadd;
    	avtRemove = avtremove;
    }
    
    public static interface lhdListener extends EventListener
    {
        public void onDataCleared();
        public void onDataAdded(Object cle, Object val);
        public void onDataRemoved(Object cle);
    }

    public void addListener(lhdListener l)
    {
        listeners.add(lhdListener.class, l);
    }
    
    public void removeListener(lhdListener l)
    {
        listeners.remove(lhdListener.class, l);
    }

    public LinkedHashMap getHashMap()
    {
    	return map;
    }
    
    public void clearObject()
    {
    	if (avtClear)
    	{
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(lhdListener.class);
    		for(int i=0; i<el.length; i++)
    			((lhdListener) el[i]).onDataCleared();
    		map.clear();
    	}
    	else
    	{
    		map.clear();
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(lhdListener.class);
    		for(int i=0; i<el.length; i++)
    			((lhdListener) el[i]).onDataCleared();
    	}
    }
    
    public void addObject(Object cle, Object val)
    {
    	if (avtAdd)
    	{
            //for (Listener l : listeners.getListeners(Listener.class))
            EventListener[] el = listeners.getListeners(lhdListener.class);
            for(int i=0; i<el.length; i++)
            	((lhdListener) el[i]).onDataAdded(cle, val);
            map.put(cle, val);
    	}
    	else
    	{
            map.put(cle, val);
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(lhdListener.class);
    		for(int i=0; i<el.length; i++)
    			((lhdListener) el[i]).onDataAdded(cle, val);
    	}
    }

    public void removeObject(Object cle)
    {
    	if (avtRemove)
    	{
    		//for (Listener l : listeners.getListeners(Listener.class))
    		EventListener[] el = listeners.getListeners(lhdListener.class);
    		for(int i=0; i<el.length; i++)
    			((lhdListener) el[i]).onDataRemoved(cle);
    		map.remove(cle);
    	}
    	else
    	{
            map.remove(cle);
            //for (Listener l : listeners.getListeners(Listener.class))
            EventListener[] el = listeners.getListeners(lhdListener.class);
            for(int i=0; i<el.length; i++)
            	((lhdListener) el[i]).onDataRemoved(cle);
    	}
    }

    public Object getObject(Object cle)
    {
        return map.get(cle);
    }

    public Object getValue(Object val)
    {
    	Set myKeys = map.keySet();
    	Iterator myKeysIterator = myKeys.iterator();

    	while (myKeysIterator.hasNext())
    	{
    		Object cle = myKeysIterator.next();
    		Object valeur = map.get(cle);
    		if (valeur.equals(val)) return cle;
    	}
    	return null;
    }

    public Object getKeyAtIndex(int index)
    {
    	if (index < map.size())
    	{
        	Set myKeys = map.keySet();
        	Iterator myKeysIterator = myKeys.iterator();

    		while (myKeysIterator.hasNext())
    		{
    			Object cle = myKeysIterator.next();
    			if (index-- == 0) return cle;
    		}
    	}
    	return null;
    }

    public Object getValueAtIndex(int index)
    {
    	return map.get(getKeyAtIndex(index));
    }
    
}
