package ri.seriem.libcommun.serien.tableModel;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class RadioButtonCellEditorRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor, ActionListener
{
	private static final long serialVersionUID = 1L;
	private JPanel panel; 
	private JRadioButton radioButton;

    public RadioButtonCellEditorRenderer()
    {
        radioButton = new JRadioButton();
        radioButton.addActionListener(this);
        //radioButton.setOpaque(false);
    	panel = new JPanel(new FlowLayout(FlowLayout.CENTER,0 , 0));
    	panel.add(radioButton);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        radioButton.setSelected(Boolean.TRUE.equals(value));
        if (isSelected)
        {
        	panel.setBackground(table.getSelectionBackground());
        	panel.setForeground(table.getSelectionForeground());
        }
        return panel;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        radioButton.setSelected(Boolean.TRUE.equals(value));
        if (isSelected)
        {
        	panel.setBackground(table.getSelectionBackground());
        	panel.setForeground(table.getSelectionForeground());
        }
        return panel;
    }

    public void actionPerformed(ActionEvent e)
    {
        stopCellEditing();
    }

    public Object getCellEditorValue()
    {
        return radioButton.isSelected();
    }

}
