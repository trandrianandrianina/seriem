//=================================================================================================
//==> Gestion des fichiers binaires simple                                  13/06/2008 - 18/03/2014
//==> (version light de GestionFichierBIN)
//==> A faire:
//==>  Ajouter test qui determine qu'on lit bien un fichier BIN car sinon il se plante ...
//==>  Attention la classe est SENSIBLE � la CASSE (pb sur AS/400 & linux) � corriger
//==>  Am�liorer de mani�re g�n�rale le source comme GestionFichierTexte
//==>  Ajouter test de validit� lors de la lecture d'un fichier BIN
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.Deflater;
import java.util.zip.Inflater;


public class GestionFichierDAT
{
	// Constantes erreurs de chargement
	private final static String ERREUR_FICHIER_INTROUVABLE="Le fichier est introuvable.";
	private final static String ERREUR_LECTURE_FICHIER="Erreur lors de la lecture du fichier.";
	private final static String ERREUR_TAILLE_ENTETE_INCORRECTE="Taille de l'ent\u00eate du fichier incorrecte.";
	private final static String ERREUR_TAILLE_FICHIER_INCORRECTE="Taille du fichier incorrecte.";
	private final static String ERREUR_ENREGISTRMENT_FICHIER="Erreur lors de l'enregistrement du fichier binaire.";
	private final static String ERREUR_RETOUR_TAILLEFIC="Erreur lors du retour de la taille du fichier.";
	private final static String ERREUR_RETOUR_PROTOCOLE="Erreur lors du retour du protocole.";
	private final static String ERREUR_RETOUR_DATE="Erreur lors du retour de la date.";
	private final static String ERREUR_RETOUR_HEURE="Erreur lors du retour de l'heure.";
	private final static String ERREUR_RETOUR_DATA="Erreur lors du retour des donn\u00e9es.";
	private final static String ERREUR_RETOUR_LISTEFIC="Erreur lors du retour de la liste des fichiers.";

	private final static int NO_COMPRESS=0;
	private final static int COMPRESS=1;

	// Description de l'ent�te du fichier
	private final static int TAILLE_ENTETE_BASE=7;         // On r�serve x (= 4*x) octets pour l'ent�te fichier
	private final static int INDICE_TAILLEENTETE=0;
	private final static int INDICE_TAILLEFIC=1;
	private final static int INDICE_COMPTEUR=2;
	private final static int INDICE_INFOBIN=3;
	private final static int INDICE_DATE=4;
	private final static int INDICE_HEURE=5;
	private final static int INDICE_LISTE_FICHIER=6;

	// Variables
	private int enteteFic[]=null;		// Contient: taille fichier, compteur, num protocole, date & heure, ...
	protected int dataFic[]=null;           	// Contient: les donn�es
	private String nomFichierDat=null;
	private URL urlFichierDat=null;
	private int etat=COMPRESS;

	private int tailleentete=0;
	private int taillefic=0;			// Variables ent�te fichier binaire
	private int tailletrt=0;
	private int compteur=0;
	private int infosbin=NO_COMPRESS;		// 0=pas de compression 1=compression
	private int date=0;
	private int heure=0;
	private ArrayList<Integer> tabintListeFichier=null;
	protected String tabListFichier[]=null;

	private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu
	

	/**
	 * Constructeur
	 */
	public GestionFichierDAT()
	{
	}

	/**
	 * Constructeur
	 * @param fchdat
	 * @param tableau
	 */
	public GestionFichierDAT(String fchdat, int tableau[])
	{
		nomFichierDat = fchdat;
		dataFic = tableau;
	}

	/**
	 * Constructeur
	 * @param fchdat
	 * @param tableau
	 */
	public GestionFichierDAT(String fchdat, Integer tableau[])
	{
		int i=0;

		nomFichierDat = fchdat;
		dataFic = new int[tableau.length];
		for (i=0; i<tableau.length; i++)
			dataFic[i] = tableau[i].intValue();

		//lectureEntete();
	}

	/**
	 * Constructeur
	 * @param fchdat
	 */
	public GestionFichierDAT(String fchdat)
	{
		nomFichierDat = fchdat;
		//lectureEntete();
	}

	/**
	 * Constructeur
	 * @param ufchdat
	 */
	public GestionFichierDAT(URL ufchdat)
	{
		urlFichierDat = ufchdat;
		//lectureEntete();
	}

	/**
	 * Initialise la variable qui indique si l'on souhaite une compression ou non du fichier g�n�r�
	 */
	public void setCompress(boolean iscompress)
	{
		if (iscompress) etat = COMPRESS;
		else etat = NO_COMPRESS;
	}

	/**
	 * V�rifie l'existence d'un fichier
	 * @param fch
	 * @return
	 */
	private boolean isPresent()
	{
		if (nomFichierDat == null) return false;

		File fichier = new File(nomFichierDat);
		return fichier.exists();
	}

	/**
	 * Lit l'ent�te du fichier compil�
	 * @return
	 */
	private int lectureEntete()
	{
		int i=0, j=0, k=0, m=0, taille=0, nbrfic=0;
		DataInputStream f = null;
		byte tabByte[]=null;

		// On v�rifie que le fichier existe
		if (nomFichierDat != null)
			if (!isPresent())
			{
				setAZeroEntete();
				msgErreur = ERREUR_FICHIER_INTROUVABLE + nomFichierDat;
				return Constantes.ERREUR;
			}

		//  On lit l'ent�te du fichier
		try
		{
			if (nomFichierDat != null)
				f = new DataInputStream(new FileInputStream(nomFichierDat));
			else
				f = new DataInputStream(urlFichierDat.openStream());

			// Lecture de la taille de l'ent�te du fichier
			tailleentete = f.readInt();
			if (tailleentete <= 0)
			{
				f.close();
				msgErreur = ERREUR_TAILLE_ENTETE_INCORRECTE + nomFichierDat;
				return Constantes.ERREUR;
			}
			enteteFic = new int[tailleentete / 4];
			enteteFic[INDICE_TAILLEENTETE] = tailleentete; // ATTENTION: taille en octet
			// Lecture de la suite de l'ent�te du fichier
			for (i=1; i<(tailleentete/4); i++)
				enteteFic[i] = f.readInt();
			f.close();
		}
		catch (Exception e)
		{
			setAZeroEntete();
			msgErreur = ERREUR_LECTURE_FICHIER + nomFichierDat + "\n" + e.toString();
			return Constantes.ERREUR;
		}

		// On affecte les valeurs lues
		taillefic = enteteFic[INDICE_TAILLEFIC]; // ATTENTION: taille en octet
		tailletrt = taillefic;
		compteur = enteteFic[INDICE_COMPTEUR];
		infosbin = enteteFic[INDICE_INFOBIN];
		date = enteteFic[INDICE_DATE];
		heure = enteteFic[INDICE_HEURE];
		taille = enteteFic[INDICE_LISTE_FICHIER];
		// On stocke la liste des fichiers
		if (taille != 0)
		{
			nbrfic = enteteFic[INDICE_LISTE_FICHIER+1];
			tabListFichier = new String[nbrfic];
			i = INDICE_LISTE_FICHIER+2;
			m = 0;
			do
			{
				// longueur du texte
				k = enteteFic[i];
				tabByte = new byte[k];
				// r�cup�ration du texte
				i++;
				k = k + i;
				for (j=0; i<k; i++)
					tabByte[j++] = (byte)enteteFic[i];
				// r�cup�ration du texte
				tabListFichier[m++] = (new String(tabByte));
			}
			while (m < nbrfic);
		}

		return Constantes.OK;
	}

	/**
	 * Lit le fichier compil�
	 * @return
	 */
	public int lectureFichier()
	{
		int i=0, j=0, k=0, m=0, taille=0, nbrfic=0;
		DataInputStream f = null;
		byte tabByte[]=null;
		int compressedDataLength=0;
		byte[] input=null;

		// On v�rifie que le fichier existe
		//if ( ((nomFichierDat != null) && (!isPresent(nomFichierDat))) || (urlFichierDat == null) )
		if ((urlFichierDat == null) && !isPresent())
			{
				setAZeroEntete();
				msgErreur = ERREUR_FICHIER_INTROUVABLE;
				return Constantes.ERREUR;
			}

		try
		{
			if (nomFichierDat != null)
				f = new DataInputStream(new FileInputStream(nomFichierDat));
			else
				f = new DataInputStream(urlFichierDat.openStream());

			//  On lit l'ent�te du fichier
			// Lecture de la taille de l'ent�te du fichier
			tailleentete = f.readInt();
			if (tailleentete <= 0)
			{
				f.close();
				msgErreur = ERREUR_TAILLE_ENTETE_INCORRECTE + nomFichierDat;
				return Constantes.ERREUR;
			}
			enteteFic = new int[tailleentete / 4];
			enteteFic[INDICE_TAILLEENTETE] = tailleentete; // ATTENTION: taille en octet
			// Lecture de la suite de l'ent�te du fichier
			for (i=1; i<(tailleentete/4); i++)
				enteteFic[i] = f.readInt();

			//  On lit les donn�es du fichier
			if ((enteteFic[INDICE_TAILLEFIC] <= 0) || (enteteFic[INDICE_TAILLEFIC] <= enteteFic[INDICE_TAILLEENTETE]))
			{
				f.close();
				msgErreur = ERREUR_TAILLE_FICHIER_INCORRECTE + nomFichierDat;
				return Constantes.ERREUR;
			}

			// Lecture des donn�es du fichier
			etat = enteteFic[INDICE_INFOBIN];
//			System.out.println("[GestionFichierBIN] " + etat);            
			if (etat != COMPRESS)
			{
				dataFic = new int[(enteteFic[INDICE_TAILLEFIC]-enteteFic[INDICE_TAILLEENTETE])/4];
				for (i=0; i<dataFic.length; i++)
					dataFic[i] = f.readInt();
			}
			else
			{
				compressedDataLength = enteteFic[INDICE_TAILLEFIC]-enteteFic[INDICE_TAILLEENTETE];
//System.out.println("[GestionFichierBIN] compressedDataLength=" + compressedDataLength);        	
				input = new byte[compressedDataLength];
				for (i=0; i<compressedDataLength; i++)
					input[i] = f.readByte();
				// D�compression du buffer
				byte[] output = new byte[enteteFic[INDICE_TAILLEFIC]*30]; // TODO 30 car la compression n'atteint pas ce taux l� (� voir car 20 trop juste VGVX09 A51) 
				Inflater decompresser = new Inflater();
				decompresser.setInput(input, 0, compressedDataLength);
				tailletrt = decompresser.inflate(output);

//System.out.println("[GestionFichierBIN] taille est=" + enteteFic[INDICE_TAILLEFIC]*30 +  " taille trt="+tailletrt);        	
				decompresser.end();
				dataFic = new int[tailletrt/4];
				for (i=0; i<dataFic.length; i++)
				{
					k = i << 2;
					dataFic[i] = (output[k]<<24) | ((output[k+1]&0xFF) << 16) | ((output[k+2]&0xFF) << 8) | ((output[k+3])&0xFF);
				}
			}
			f.close();
		}
		catch (Exception e)
		{
			setAZeroEntete();
			msgErreur = ERREUR_LECTURE_FICHIER + nomFichierDat + "\n" + e.toString();
			return Constantes.ERREUR;
		}

		// On affecte les valeurs lues
		tailleentete = enteteFic[INDICE_TAILLEENTETE]; // ATTENTION: taille en octet
		taillefic = enteteFic[INDICE_TAILLEFIC];  // ATTENTION: taille en octet
		tailletrt += enteteFic[INDICE_TAILLEENTETE];
		compteur = enteteFic[INDICE_COMPTEUR];
		infosbin = enteteFic[INDICE_INFOBIN];
		date = enteteFic[INDICE_DATE];
		heure = enteteFic[INDICE_HEURE];
//		System.out.println("Date: " + date + " heure: " + heure);
//		System.out.println("Date: " + getDate() + " heure: " + getHeure());
		taille = enteteFic[INDICE_LISTE_FICHIER];
		// On stocke la liste des fichiers
		if (taille != 0)
		{

			nbrfic = enteteFic[INDICE_LISTE_FICHIER+1];
			tabListFichier = new String[nbrfic];
			i = INDICE_LISTE_FICHIER+2;
			m = 0;
			do
			{
				// longueur du texte
				k = enteteFic[i];
				tabByte = new byte[k];
				// r�cup�ration du texte
				i++;
				k = k + i;
				for (j=0; i<k; i++)
					tabByte[j++] = (byte)enteteFic[i];
				// r�cup�ration du texte
				tabListFichier[m++] = (new String(tabByte));
			}
			while (m < nbrfic);
		}

		return Constantes.OK;
	}

	/**
	 * G�n�re le fichier Binaire
	 * @return
	 */
	public int ecritureFichier()
	{
		int i=0, j=0, k=0;
		DataOutputStream f = null;
		int compressedDataLength=dataFic.length*4;
		byte[] output=null;

		// Calcul de la taille de l'entete
		if (tabintListeFichier != null)
			j = tabintListeFichier.size();
		else
			j = 0;

		// Compression du buffer
		tailletrt=compressedDataLength;
		if (etat == COMPRESS)
		{
			Deflater compresser = new Deflater();
			byte[] input = new byte[compressedDataLength];
			output = new byte[compressedDataLength];

			for (i=0; i<dataFic.length; i++)
			{
				k =i << 2;
				input[k]   = (byte)(dataFic[i] >>> 24);
				input[k+1] = (byte)(dataFic[i] >>> 16);
				input[k+2] = (byte)(dataFic[i] >>> 8);
				input[k+3] = (byte)(dataFic[i]);
			}
			compresser.setInput(input);
			compresser.finish();
			compressedDataLength = compresser.deflate(output);
		}

		// On g�n�re l'ent�te
		enteteFic = new int[TAILLE_ENTETE_BASE + j];
		enteteFic[INDICE_TAILLEENTETE] = (TAILLE_ENTETE_BASE + j) * 4;
		tailletrt += enteteFic[INDICE_TAILLEENTETE];
		enteteFic[INDICE_TAILLEFIC] = (enteteFic.length*4) + compressedDataLength;
		taillefic = enteteFic[INDICE_TAILLEFIC];  // ATTENTION: taille en octet
		enteteFic[INDICE_COMPTEUR] = ++compteur;
		enteteFic[INDICE_INFOBIN] = etat;
		date = Constantes.getDate();
		heure = Constantes.getHeure();
		enteteFic[INDICE_DATE] = date;
		enteteFic[INDICE_HEURE] = heure;
		enteteFic[INDICE_LISTE_FICHIER] = j * 4;
		// On stocke la liste des fichiers
		for (i=0; i<j; i++)
			enteteFic[INDICE_LISTE_FICHIER+1+i] = ((Integer)tabintListeFichier.get(i)).intValue();

		// On cr�� le fichier
		try
		{
			f = new DataOutputStream(new FileOutputStream(nomFichierDat));
			// Ecriture de l'ent�te du fichier
			for (i=0; i<enteteFic.length; i++)
				f.writeInt(enteteFic[i]);

			// Compression du fichier si demand�
			if (etat == COMPRESS)
			{
				// Ecriture des enregistrements dans le fichier
				for (i=0; i<compressedDataLength; i++)
					f.writeByte(output[i]);
			}
			else
				// Ecriture des enregistrements dans le fichier
				for (i=0; i<dataFic.length; i++)
					f.writeInt(dataFic[i]);
			f.flush();
			f.close();
		}
		catch (IOException e) 
		{
			msgErreur = ERREUR_ENREGISTRMENT_FICHIER + e;
			return Constantes.ERREUR;
		}

		// On vide le tableau ent�te pour �tre sur qu'il soit re-lu si besoin
		enteteFic = null;

		return Constantes.OK; 
	}

	/**
	 * Initialise le tableau
	 */
	private void setAZeroEntete()
	{
		taillefic = 0;
		compteur = 0;
		infosbin = etat;
		date = 0;
		heure = 0;
		enteteFic = null;
		tabListFichier = null;
	}

	/**
	 * Initialise le tableau
	 * @param tableau
	 * @throws UnsupportedEncodingException
	 */
	public void setListeFichier(ArrayList<String> tableau) throws UnsupportedEncodingException
	{
		int i=0, j=0;
		String chaine=null;
		byte tabchaine[]=null;

		if (!tableau.isEmpty())
		{
			// Allocation du tableau avec valeur corrig�e
			tableau.trimToSize();
			tabintListeFichier = new ArrayList<Integer>(Constantes.MAXVAR);

			// Nombre de fichier dans la liste
			tabintListeFichier.add(new Integer(tableau.size()));

			// Pour chaque fichier
			for (j=0; j<tableau.size(); j++)
			{
				// Petit traitement qui le rend ind�pendant de la plateforme
				chaine = ((String)tableau.get(j)).replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR);
//				System.out.println("[GestionFichierBIN] (setListeFichier) " + chaine);                
				tabintListeFichier.add(new Integer(chaine.length()));

				// Donn�es
				//tabchaine = chaine.getBytes(Constantes.CODEPAGE);
				tabchaine = chaine.getBytes();
				i = 0;
				while(i < tabchaine.length)
				{
					tabintListeFichier.add(new Integer((int)tabchaine[i]));
					i++;
				}
			}
		}
		else
			tabintListeFichier = null;
	}

	/**
	 * Initialise le tableau
	 * @param tableau
	 */
	public void setData(int tableau[])
	{
		dataFic = tableau;
	}

	/**
	 * Initialise le tableau
	 * @param tableau
	 */
	public void setData(Integer tableau[])
	{
		int i=0;

		dataFic = new int[tableau.length];
		for (i=0; i<tableau.length; i++)
			dataFic[i] = tableau[i].intValue();
	}

	/**
	 * Initialise le nom du fichier binaire
	 * @param fchsrc
	 * @param fchbin
	 */
	public void setNomfichier(String fchsrc, String fchbin)
	{
		int i=0;

		nomFichierDat = fchbin;
		if (nomFichierDat == null)
		{
			// Cr�ation du nouveau nom du fichier
			i = fchsrc.lastIndexOf('.');
			if (i != -1)
				nomFichierDat = fchsrc.substring(0, i) + Constantes.EXT_BIN;
			else
				nomFichierDat = fchsrc + Constantes.EXT_BIN;
		}

		//lectureEntete();
	}

	/**
	 * Initialise le nom du fichier binaire
	 * @param fchbin
	 */
	public void setNomfichier(String fchbin)
	{
		nomFichierDat = fchbin;
		//lectureEntete();
	}

	/**
	 * Retourne la liste des fichiers
	 * @return
	 */
	public String[] getListeFichier()
	{
		if (tabListFichier == null)
			if (lectureFichier() == Constantes.ERREUR)
			{
				msgErreur = msgErreur + Constantes.crlf + ERREUR_RETOUR_LISTEFIC;
				return null;
			}
		return tabListFichier;
	}

	/**
	 * Retourne les donn�es du fichier
	 * @return
	 */
	public int[] getData()
	{
		if (dataFic == null)
			if (lectureFichier() == Constantes.ERREUR)
			{
				msgErreur = msgErreur + Constantes.crlf + ERREUR_RETOUR_DATA;
				return null;
			}
		return dataFic;
	}

	/**
	 * Retourne la taille du fichier
	 * @return
	 */
	public int getTaille()
	{
		if (enteteFic == null)
			if (lectureEntete() == Constantes.ERREUR)
			{
				msgErreur = msgErreur + Constantes.crlf + ERREUR_RETOUR_TAILLEFIC;
				return Constantes.ERREUR;
			}
		return taillefic;
	}

	/**
	 * Retourne la valeur du compteur
	 * @return
	 */
	public int getCompteur()
	{
		if (enteteFic == null)
			if (lectureEntete() == Constantes.ERREUR)
				return 0;
		return compteur;
	}

	/**
	 * Retourne la version du protocole
	 * @return
	 */
	public int getInfosbin()
	{
		if (enteteFic == null)
			if (lectureEntete() == Constantes.ERREUR)
			{
				msgErreur = msgErreur + Constantes.crlf + ERREUR_RETOUR_PROTOCOLE;
				return Constantes.ERREUR;
			}
		return infosbin;
	}

	/**
	 * Retourne la date de compilation
	 * @return
	 */
	public String getDate()
	{
		if (enteteFic == null)
			if (lectureEntete() == Constantes.ERREUR)
			{
				msgErreur = msgErreur + Constantes.crlf + ERREUR_RETOUR_DATE;
				return null;
			}
		StringBuffer chaine = new StringBuffer(Integer.toString(date));
		while (chaine.length() != 8)
			chaine.insert(0, '0');
		return chaine.substring(6) + "/" + chaine.substring(4, 6) + "/" + chaine.substring(0, 4);
	}

	/**
	 * Retourne l'heure de compilation
	 * @return
	 */
	public String getHeure()
	{
		if (enteteFic == null)
			if (lectureEntete() == Constantes.ERREUR)
			{
				msgErreur = msgErreur + Constantes.crlf + ERREUR_RETOUR_HEURE;
				return null;
			}
		StringBuffer chaine = new StringBuffer(Integer.toString(heure));
		while (chaine.length() != 6)
			chaine.insert(0, '0');
		return chaine.substring(0, 2) + ":" + chaine.substring(2, 4) + ":" + chaine.substring(4);
	}

	/**
	 * Retourne la date, l'heure de compilation, si compress� et le compteur
	 * @return
	 */
	public String getStatus()
	{
		if (etat == COMPRESS)
			return "Date: " + getDate() + " - Heure: " + getHeure() + " - Compteur: " + compteur + " - Compress�: " + etat + " ("+taillefic+" <-> "+tailletrt+" octets)";
		else
			return "Date: " + getDate() + " - Heure: " + getHeure() + " - Compteur: " + compteur + " - Compress�: " + etat + " ("+taillefic+" octets)";
	}
	
	/**
	 * Lib�re la m�moire
	 */
	public void dispose()
	{
		if (tabintListeFichier != null)
			tabintListeFichier.clear();
		tabintListeFichier = null;
		urlFichierDat = null;
		dataFic = null;
		enteteFic = null;
		tabListFichier = null;
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
