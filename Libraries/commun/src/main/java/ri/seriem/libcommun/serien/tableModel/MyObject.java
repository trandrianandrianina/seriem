package ri.seriem.libcommun.serien.tableModel;

import java.beans.PropertyChangeSupport;


public class MyObject
{
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    private MyObjectManager manager;
    private String name;
    private String host;
    private boolean selected;

    public MyObject(String name, String host)
    {
        this.name = name;
        this.host = host;
    }

    public PropertyChangeSupport getPropertyChangeSupport()
    {
        return propertyChangeSupport;
    }

    public String getValue(int col) {
    	switch (col) {
		case 0: return name;
		case 1: return host;
		default:
			break;
		}
    	return "";
    }

    public void setValue(int col, String value) {
    	switch(col)
    	{
    	case 0:
            this.name = value;
            propertyChangeSupport.firePropertyChange("name", null, name);
    	case 1:
            this.host = value;
            propertyChangeSupport.firePropertyChange("host", null, host);

    	}
    }

    public MyObjectManager getManager() {
        return manager;
    }

    public void setManager(MyObjectManager manager) {
        this.manager = manager;
        propertyChangeSupport.firePropertyChange("manager", null, manager);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected)
    {
//System.out.println("--> selected:" + selected);    	
    	if (this.selected != selected)
        {
            this.selected = selected;
            if (selected)
            {
                manager.setAsSelected(this);
            }
            propertyChangeSupport.firePropertyChange("selected", !selected, selected);
        }
    }

}