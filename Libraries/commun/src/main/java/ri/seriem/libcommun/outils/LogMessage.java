//=================================================================================================
//==>                                                                       11/07/2006 - 22/11/2010
//==> Gestion des fichier de logs
//==> A faire: 
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;



public class LogMessage
{
	// Constantes
    private final static String NOM_CLASSE="[LogMessage]";
	public final static String EXTENSION=".txt";
	private final static String DOSSIER_LOG="logs";
	
    // Constantes erreurs
    private final static String ERREUR_ECRITURE_LOG="Erreur lors de l'�criture de l'enregistrement dans le fichier de logs.";
    private final static String ERREUR_OUVERTURE_LOG="Erreur lors de l'ouverture du fichier de logs.";

    // Variables
    private String user="";
    private File fichier=null;
    private String dossier=null;
    private String suffixe=null;
    private FileWriter fw_fichier=null;
    private boolean isconsole=false;
    private PrintStream console=null, console_out=null, console_err=null;
    private Thread nettoyage=null;

    private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

    /**
     * Constructeur
     */
    public LogMessage()
    {
    	this(null, null);
        createAndOpenFile();
    }

    /**
     * Constructeur de la classe
     * @param rep
     * @param suffixe
     */
    public LogMessage(String rep, String suf)
    {
    	dossier = rep;
    	suffixe = suf; 
        createAndOpenFile();
    }

    /**
     * Initialisation du dossier & fichier
     *
     */
    private void createAndOpenFile()
    {
    	// Construction du nom du fichier
        String prefixe = DateHeure.getJourHeure(4);
        if (suffixe != null) prefixe += suffixe.trim();
        if (prefixe.lastIndexOf('.') == -1)	prefixe += EXTENSION;
        if (dossier == null) dossier = getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + File.separatorChar + DOSSIER_LOG;
        fichier = new File(dossier.trim() + File.separatorChar + prefixe);

        // Cr�ation du dossier si besoin
    	if (!fichier.getParentFile().exists()) fichier.getParentFile().mkdirs();
        
        // Initialisations diverses
        try
        {
            fw_fichier = new FileWriter(fichier, true);
        }
        catch (IOException e) 
        {
            msgErreur += ERREUR_OUVERTURE_LOG + Constantes.crlf + fichier.getAbsolutePath() + Constantes.crlf + e.getMessage() + Constantes.crlf;
        }
    }

    /**
     * V�rifie la date du fichier de log et son existence
     * @throws IOException
     */
    private void verifValiditeFichier() throws IOException
    {
    	// On v�rifie que l'on ait pas chang� de jour
    	if (!fichier.getName().startsWith(DateHeure.getJourHeure(4)))
    	{
    		fw_fichier.flush();
            fw_fichier.close();
    		createAndOpenFile();
    	}
    	else // Sinon on se contente de v�rifier que le fichier existe toujours
    		if (!fichier.exists()) fw_fichier = new FileWriter(fichier, true);
    }
    
    /**
     * Initialisation de l'utilisateur
     * @param usr
     */
    public void setUser(String usr)
    {
        user = usr;
    }

    /**
     * Redirige la console vers le fichier de log
     */
    public void setRedirigeConsole(boolean tofile)
    {
    	this.isconsole = tofile;
    	if (tofile)
    	{
    		// On planque les sorties originales
    		console_out = System.out;
    		console_err = System.err;
    		// On affecte les nouvelles sorties
    		try
			{
				console = new PrintStream(new FileOutputStream(fichier, true), true);
	    		System.setOut(console);
	    		System.setErr(console);
			}
			catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else
    		if (console_out != null)
    		{
    			System.setOut(console_out);
    			System.setErr(console_err);
    		}
    }
    
    /**
     * Stockage du message re�u dans un fichier de log
     * @param mes
     */
    public synchronized void ecritureMessage(String mes)
    {
    	if (isconsole)
    		System.out.println(DateHeure.getJourHeure(1) + Constantes.SEPARATEUR + user + Constantes.SEPARATEUR + mes);
    	else
        try
        {
        	verifValiditeFichier();
            // Ecriture de l'enregistrement dans le fichier
            fw_fichier.write(DateHeure.getJourHeure(1) + Constantes.SEPARATEUR + user + Constantes.SEPARATEUR + mes + Constantes.crlf);
            fw_fichier.flush();
        }
        catch (IOException e) 
        {
            System.out.println(NOM_CLASSE +  " " + ERREUR_ECRITURE_LOG + Constantes.crlf + e);
        }
    }    

    /**
     * Stockage du message re�u dans un fichier de log
     * @param usr
     * @param mes
     */
    public synchronized void ecritureMessage(String usr, String mes)
    {
    	if (isconsole)
    		System.out.println(DateHeure.getJourHeure(1) + Constantes.SEPARATEUR + usr + Constantes.SEPARATEUR + mes);
    	else
        try
        {
        	verifValiditeFichier();
            // Ecriture de l'enregistrement dans le fichier
            fw_fichier.write(DateHeure.getJourHeure(1) + Constantes.SEPARATEUR + usr + Constantes.SEPARATEUR + mes + Constantes.crlf);
            fw_fichier.flush();
        }
        catch (IOException e) 
        {
            System.out.println(NOM_CLASSE +  " " + ERREUR_ECRITURE_LOG + Constantes.crlf + e);
        }
    }    

    /**
     * Fermeture du fichier de log
     *
     */
    public synchronized void fermetureLog()
    {
    	// Arr�t de la proc�dure de nettoyage des logs
    	if ((nettoyage != null) && (nettoyage.isAlive()))
    	{
    		nettoyage.interrupt();
    		nettoyage = null;
    	}
    	// Fermeture du fichier
    	try
        {
            fw_fichier.flush();
            fw_fichier.close();
        }
        catch (IOException e) 
        {
        }
        // Remet les redirections stdin/sdtout comme � l'origine
        if (isconsole)
        	setRedirigeConsole(false);
    }
    
    /**
     * Retourne le dossier o� est stock� le fichier de log
     * @return
     */
    public String getDossier()
    {
    	return fichier.getParent();
    }

    /**
     * Retourne le nom du fichier
     * @return
     */
    public String getFichier()
    {
    	return fichier.getAbsolutePath();
    }
    
    /**
     * Lance une tache de fond qui surveillera les logs � conserver
     * @param nbrjours
     */
    public void nettoyageLogs(final int nbrjours)
    {
        // On v�rifie si on a pas un peu de m�nage � faire dans les logs (d�clench� toutes les 24 heures)
        nettoyage = new Thread()
        {
        	public void run()
        	{
        		while (true)
        		{
        			// Nettoyage des logs si n�cessaire
        			Constantes.SuppressionLogMessage(getDossier(), nbrjours);
        			try
        			{
        				Thread.sleep(86400000); // 24h = 86400 secondes
        			}
        			catch (InterruptedException ex)
    				{
    					Thread.currentThread().interrupt(); // Tr�s important de r�interrompre
    					break;
    				}
        		}
        	}
        };
        nettoyage.start();
    }
    
    /**
     * Retourne le message d'erreur
     * @return
     */
    public String getMsgErreur()
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }

}