/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

/**
 * Description d'une imprimante (AS/400)
 */
public class Imprimante {
  // Constantes
  public static final byte STOPPED = 0;
  public static final byte STARTED = 1;
  
  // Variables
  private String nom = null;
  private String outq = null;
  private byte status = STARTED;
  private int nbrexemplaire = 1;
  private boolean selection = false;
  
  /**
   * @return the nom
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  /**
   * @return the outq
   */
  public String getOutq() {
    return outq;
  }
  
  /**
   * @param outq the outq to set
   */
  public void setOutq(String outq) {
    this.outq = outq;
  }
  
  /**
   * @return the status
   */
  public byte getStatus() {
    return status;
  }
  
  /**
   * @param status the status to set
   */
  public void setStatus(byte status) {
    this.status = status;
  }
  
  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    if (status == null) {
      return;
    }
    status = status.trim();
    if (status.length() == 1) {
      setStatus((byte) Integer.parseInt(status));
    }
  }
  
  /**
   * @return le nbrexemplaire
   */
  public int getNbrexemplaire() {
    return nbrexemplaire;
  }
  
  /**
   * @param nbrexemplaire le nbrexemplaire � d�finir
   */
  public void setNbrexemplaire(int nbrexemplaire) {
    this.nbrexemplaire = nbrexemplaire;
  }
  
  /**
   * @return le selection
   */
  public boolean isSelection() {
    return selection;
  }
  
  /**
   * @param selection le selection � d�finir
   */
  public void setSelection(boolean selection) {
    this.selection = selection;
  }
  
}
