/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

/**
 * Description d'une outq (AS/400)
 */
public class OutQ {
  // Variables
  private String nom = null;
  private String outq = null;
  private String description = null;
  private boolean selection = false;
  
  /**
   * @return the nom
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  /**
   * @return the outq
   */
  public String getOutq() {
    return outq;
  }
  
  /**
   * @param outq the outq to set
   */
  public void setOutq(String outq) {
    this.outq = outq;
  }
  
  /**
   * @return le description
   */
  public String getDescription() {
    return description;
  }
  
  /**
   * @param description le description � d�finir
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  /**
   * @return le selection
   */
  public boolean isSelection() {
    return selection;
  }
  
  /**
   * @param selection le selection � d�finir
   */
  public void setSelection(boolean selection) {
    this.selection = selection;
  }
  
}
