//=================================================================================================
//==>                                                                       11/05/2016 - 11/05/2016
//==> Informations concernant le serveur de mail 
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

public class InfosMailServer
{
	// Constantes
	private		final static	String						NOM_CLASSE				= "[InfosMailServer]";
	
	// Variables
	private						String						hosto					= null;			// Serveur sortant
	private						int							porto					= 25;
	private						String						hosti					= null;			// Serveur entrant
	private						int							porti					= 110;
	private						String						email					= null;			// Adresse email �metteur
	private						String						login					= null;			// identifiant du compte
	private						String						password				= null;			// mdp du compte
	
	
	/**
	 * @return le hosto
	 */
	public String getHosto()
	{
		return hosto;
	}
	/**
	 * @param host le hosto � d�finir
	 */
	public void setHosto(String hosto)
	{
		this.hosto = hosto;
	}
	/**
	 * @return le porto
	 */
	public int getPorto()
	{
		return porto;
	}
	/**
	 * @param porto le porto � d�finir
	 */
	public void setPorto(int porto)
	{
		this.porto = porto;
	}
	/**
	 * @return le hosti
	 */
	public String getHosti()
	{
		return hosti;
	}
	/**
	 * @param hosti le hosti � d�finir
	 */
	public void setHosti(String hosti)
	{
		this.hosti = hosti;
	}
	/**
	 * @return le porti
	 */
	public int getPorti()
	{
		return porti;
	}
	/**
	 * @param porti le porti � d�finir
	 */
	public void setPorti(int porti)
	{
		this.porti = porti;
	}
	/**
	 * @return le email
	 */
	public String getEmail()
	{
		return email;
	}
	/**
	 * @param email le email � d�finir
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}
	/**
	 * @return le login
	 */
	public String getLogin()
	{
		return login;
	}
	/**
	 * @param login le login � d�finir
	 */
	public void setLogin(String login)
	{
		this.login = login;
	}
	/**
	 * @return le password
	 */
	public String getPassword()
	{
		return password;
	}
	/**
	 * @param password le password � d�finir
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}
	
}
