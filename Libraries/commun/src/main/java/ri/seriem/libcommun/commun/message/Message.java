/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.commun.message;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.MessageErreurException;

/**
 * Message associ� � un niveau d'importance.
 * 
 * Les messages normaux sont destin�s � �tre affich�s en noir tandis que les messages importants sont affich�s en rouge.
 * Il est possible de formatter le texte en html.
 */
public class Message {
  final private EnumImportanceMessage importanceMessage;
  final private String texte;
  
  /**
   * Constructeur permettant de d�finir un message avec son niveau d'importance normale au format choisi.
   */
  private Message(String pTexte, EnumImportanceMessage pImportanceMessage, boolean pEnHtml) {
    if (pImportanceMessage == null) {
      throw new MessageErreurException("L'importance du message est invalide.");
    }
    // Au format html
    if (pEnHtml) {
      pTexte = Constantes.normerTexte(pTexte);
      // Rend inactif les bornes des �ventuels tags contenus dans le texte et ajout des tags indiquant qu'il s'agit de html
      texte = "<html>" + pTexte.replaceAll("<", "&lt;").replaceAll(">", "&gt;") + "</html>";
    }
    // Au format texte
    else {
      texte = Constantes.normerTexte(pTexte);
    }
    importanceMessage = pImportanceMessage;
  }
  
  /**
   * Cr�er un message d'importance normale.
   */
  static public Message getMessageNormal(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.NORMAL, false);
  }
  
  /**
   * Cr�er un message d'importance moyenne.
   */
  static public Message getMessageMoyen(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.MOYEN, false);
  }
  
  /**
   * Cr�er un message important.
   */
  static public Message getMessageImportant(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.HAUT, false);
  }
  
  /**
   * Cr�er un message en pr�cisant le niveau d'importance.
   */
  static public Message getMessage(String pTexte, EnumImportanceMessage pEnumImportanceMessage) {
    return new Message(pTexte, pEnumImportanceMessage, false);
  }
  
  /**
   * Cr�er un message au format html d'importance normale.
   */
  static public Message getMessageHtmlNormal(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.NORMAL, true);
  }
  
  /**
   * Cr�er un message au format html d'importance moyenne.
   */
  static public Message getMessageHtmlMoyen(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.MOYEN, true);
  }
  
  /**
   * Cr�er un message important au format html.
   */
  static public Message getMessageHtmlImportant(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.HAUT, true);
  }
  
  /**
   * Cr�er un message au format html en pr�cisant le niveau d'importance.
   */
  static public Message getMessageHtml(String pTexte, EnumImportanceMessage pEnumImportanceMessage) {
    return new Message(pTexte, pEnumImportanceMessage, true);
  }
  
  /**
   * Texte du message.
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * Importance du message : normal ou important.
   */
  public EnumImportanceMessage getImportanceMessage() {
    return importanceMessage;
  }
  
  /**
   * Indique si le message est important.
   */
  public boolean isImportant() {
    return importanceMessage.equals(EnumImportanceMessage.HAUT);
  }
}
