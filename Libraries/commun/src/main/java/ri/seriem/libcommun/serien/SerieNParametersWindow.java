package ri.seriem.libcommun.serien;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;

/**
 * @author St�phane V�n�ri
 */
public class SerieNParametersWindow extends JDialog {

	// Constantes
	private static final long serialVersionUID = 1L;
	
	// Variables
	private SerieNParameters parameters=null;
	
	
	/**
	 * Constructeur
	 */
	public SerieNParametersWindow() {
		initComponents();
		//prepareMask();
	}

	/**
	 * Constructeur
	 * @param owner
	 */
	public SerieNParametersWindow(Frame owner) {
		super(owner);
		initComponents();
		//prepareMask();
	}

	/**
	 * Constructeur
	 * @param owner
	 */
	public SerieNParametersWindow(Dialog owner) {
		super(owner);
		initComponents();
		//prepareMask();
	}

	/**
	 * Initialise les param�tres
	 * @param aparameters
	 */
	public void setParameters(SerieNParameters aparameters)
	{
		parameters = aparameters;
		initWindow();
	}
	
	public void setHideOptions(boolean hide)
	{
		p_Options.setVisible(!hide);
		l_AfficheDossierRacine.setText("<html>Le dossier racine est <b>" + tf_DossierRacine.getText()+ "</b></html>");
		p_Fond.setVisible(hide);
		pack();
		/*
		tf_DossierRacine.setEditable(!ro);
		chk_Debug.setEnabled(!ro);
		tf_ConservLogs.setEditable(!ro);
		*/
	}
	
	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Pr�pare le masque de saisie de l'adresse IP 
	 *
	private void prepareMask()
	{
		try
		{
			MaskFormatter madrip = new MaskFormatter("###.###.###.###");
			madrip .setPlaceholderCharacter('0');
			madrip.install(tf_Host);
		}
		catch (ParseException e)
		{} 
	}*/
	
	/**
	 * Initialise les donn�es de la fen�tre avec les param�tres
	 */
	private void initWindow()
	{
		p_Fond.setVisible(false);
		l_Erreur.setText("");
		tf_Host.requestFocusInWindow();
		getRootPane().setDefaultButton(okButton);

		if (parameters == null) return;
		
		tf_Name.setText(parameters.getName());
		tf_Host.setText(parameters.getHost());
		tf_Port.setText(""+parameters.getPort());
		tf_DossierRacine.setText(parameters.getDossierRacine());
		tf_ConservLogs.setText(""+parameters.getDelaiLogs());
		chk_Debug.setSelected(parameters.isDebug());
	}
	
	/**
	 * Initialise les param�tres avec les donn�es de la fen�tre
	 */
	private void initParameters()
	{
		if (parameters == null) return;

		// Faire un controle de la validit� des donn�es
		parameters.setName(tf_Name.getText().trim());
		parameters.setHost(tf_Host.getText().trim());
		parameters.setPort(Integer.parseInt(tf_Port.getText().trim()));
		parameters.setDossierRacine(tf_DossierRacine.getText().trim());
		parameters.setDelaiLogs(Integer.parseInt(tf_ConservLogs.getText().trim()));
		parameters.setDebug(chk_Debug.isSelected());
	}
	
	// -- M�thodes �v�nementielles --------------------------------------------
	
	private void cancelButtonActionPerformed(ActionEvent e) {
		setVisible(false);
	}

	private void okButtonActionPerformed(ActionEvent e) {
		initParameters();
		if (parameters.isValid())
			setVisible(false);
		else
			l_Erreur.setText(parameters.getMsgErreur(true));
	}

	private void genericFocusGained(FocusEvent e) {
		((JTextComponent) e.getComponent()).selectAll();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		buttonBar = new JPanel();
		l_Erreur = new JLabel();
		okButton = new JButton();
		cancelButton = new JButton();
		p_Centre = new JPanel();
		contentPanel = new JPanel();
		l_Name = new JLabel();
		tf_Name = new JTextField();
		l_host = new JLabel();
		tf_Host = new JTextField();
		l_Port = new JLabel();
		tf_Port = new JTextField();
		p_Options = new JPanel();
		l_DossierRacine = new JLabel();
		tf_DossierRacine = new JTextField();
		chk_Debug = new JCheckBox();
		l_Conservlogs = new JLabel();
		tf_ConservLogs = new JTextField();
		l_Jours = new JLabel();
		p_Fond = new JPanel();
		l_AfficheDossierRacine = new JLabel();

		//======== this ========
		setTitle("Saisie des param\u00e8tres de S\u00e9rie N");
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setModal(true);
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setName("dialogPane");
			dialogPane.setLayout(new BorderLayout());

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setName("buttonBar");
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
				((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

				//---- l_Erreur ----
				l_Erreur.setText("Erreur");
				l_Erreur.setForeground(Color.red);
				l_Erreur.setName("l_Erreur");
				buttonBar.add(l_Erreur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- okButton ----
				okButton.setText("OK");
				okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				okButton.setName("okButton");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okButtonActionPerformed(e);
					}
				});
				buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Annuler");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.setName("cancelButton");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelButtonActionPerformed(e);
					}
				});
				buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);

			//======== p_Centre ========
			{
				p_Centre.setBorder(LineBorder.createBlackLineBorder());
				p_Centre.setName("p_Centre");
				p_Centre.setLayout(new BorderLayout());

				//======== contentPanel ========
				{
					contentPanel.setName("contentPanel");
					contentPanel.setLayout(null);

					//---- l_Name ----
					l_Name.setText("R\u00e9f\u00e9rence");
					l_Name.setName("l_Name");
					contentPanel.add(l_Name);
					l_Name.setBounds(5, 15, 155, l_Name.getPreferredSize().height);

					//---- tf_Name ----
					tf_Name.setName("tf_Name");
					tf_Name.addFocusListener(new FocusAdapter() {
						@Override
						public void focusGained(FocusEvent e) {
							genericFocusGained(e);
						}
					});
					contentPanel.add(tf_Name);
					tf_Name.setBounds(165, 10, 115, tf_Name.getPreferredSize().height);

					//---- l_host ----
					l_host.setText("Adresse IP");
					l_host.setName("l_host");
					contentPanel.add(l_host);
					l_host.setBounds(5, 48, 155, l_host.getPreferredSize().height);

					//---- tf_Host ----
					tf_Host.setName("tf_Host");
					tf_Host.addFocusListener(new FocusAdapter() {
						@Override
						public void focusGained(FocusEvent e) {
							genericFocusGained(e);
						}
					});
					contentPanel.add(tf_Host);
					tf_Host.setBounds(165, 42, 200, tf_Host.getPreferredSize().height);

					//---- l_Port ----
					l_Port.setText("Port");
					l_Port.setName("l_Port");
					contentPanel.add(l_Port);
					l_Port.setBounds(5, 80, 155, l_Port.getPreferredSize().height);

					//---- tf_Port ----
					tf_Port.setHorizontalAlignment(SwingConstants.RIGHT);
					tf_Port.setName("tf_Port");
					tf_Port.addFocusListener(new FocusAdapter() {
						@Override
						public void focusGained(FocusEvent e) {
							genericFocusGained(e);
						}
					});
					contentPanel.add(tf_Port);
					tf_Port.setBounds(165, 74, 65, tf_Port.getPreferredSize().height);

					{ // compute preferred size
						Dimension preferredSize = new Dimension();
						for(int i = 0; i < contentPanel.getComponentCount(); i++) {
							Rectangle bounds = contentPanel.getComponent(i).getBounds();
							preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
							preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
						}
						Insets insets = contentPanel.getInsets();
						preferredSize.width += insets.right;
						preferredSize.height += insets.bottom;
						contentPanel.setMinimumSize(preferredSize);
						contentPanel.setPreferredSize(preferredSize);
					}
				}
				p_Centre.add(contentPanel, BorderLayout.NORTH);

				//======== p_Options ========
				{
					p_Options.setName("p_Options");
					p_Options.setLayout(null);

					//---- l_DossierRacine ----
					l_DossierRacine.setText("Dossier racine");
					l_DossierRacine.setName("l_DossierRacine");
					p_Options.add(l_DossierRacine);
					l_DossierRacine.setBounds(5, 20, 155, l_DossierRacine.getPreferredSize().height);

					//---- tf_DossierRacine ----
					tf_DossierRacine.setName("tf_DossierRacine");
					tf_DossierRacine.addFocusListener(new FocusAdapter() {
						@Override
						public void focusGained(FocusEvent e) {
							genericFocusGained(e);
						}
					});
					p_Options.add(tf_DossierRacine);
					tf_DossierRacine.setBounds(165, 15, 330, tf_DossierRacine.getPreferredSize().height);

					//---- chk_Debug ----
					chk_Debug.setText("Activation du d\u00e9bug");
					chk_Debug.setName("chk_Debug");
					p_Options.add(chk_Debug);
					chk_Debug.setBounds(5, 50, 160, chk_Debug.getPreferredSize().height);

					//---- l_Conservlogs ----
					l_Conservlogs.setText("Conservation des logs");
					l_Conservlogs.setName("l_Conservlogs");
					p_Options.add(l_Conservlogs);
					l_Conservlogs.setBounds(5, 85, 155, l_Conservlogs.getPreferredSize().height);

					//---- tf_ConservLogs ----
					tf_ConservLogs.setHorizontalAlignment(SwingConstants.RIGHT);
					tf_ConservLogs.setName("tf_ConservLogs");
					tf_ConservLogs.addFocusListener(new FocusAdapter() {
						@Override
						public void focusGained(FocusEvent e) {
							genericFocusGained(e);
						}
					});
					p_Options.add(tf_ConservLogs);
					tf_ConservLogs.setBounds(165, 80, 35, tf_ConservLogs.getPreferredSize().height);

					//---- l_Jours ----
					l_Jours.setText("jours(s)");
					l_Jours.setName("l_Jours");
					p_Options.add(l_Jours);
					l_Jours.setBounds(215, 85, 50, l_Jours.getPreferredSize().height);

					{ // compute preferred size
						Dimension preferredSize = new Dimension();
						for(int i = 0; i < p_Options.getComponentCount(); i++) {
							Rectangle bounds = p_Options.getComponent(i).getBounds();
							preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
							preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
						}
						Insets insets = p_Options.getInsets();
						preferredSize.width += insets.right;
						preferredSize.height += insets.bottom;
						p_Options.setMinimumSize(preferredSize);
						p_Options.setPreferredSize(preferredSize);
					}
				}
				p_Centre.add(p_Options, BorderLayout.CENTER);

				//======== p_Fond ========
				{
					p_Fond.setName("p_Fond");

					//---- l_AfficheDossierRacine ----
					l_AfficheDossierRacine.setText("Affiche le dossier racine");
					l_AfficheDossierRacine.setName("l_AfficheDossierRacine");

					GroupLayout p_FondLayout = new GroupLayout(p_Fond);
					p_Fond.setLayout(p_FondLayout);
					p_FondLayout.setHorizontalGroup(
						p_FondLayout.createParallelGroup()
							.addGroup(p_FondLayout.createSequentialGroup()
								.addGap(5, 5, 5)
								.addComponent(l_AfficheDossierRacine, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE))
					);
					p_FondLayout.setVerticalGroup(
						p_FondLayout.createParallelGroup()
							.addGroup(p_FondLayout.createSequentialGroup()
								.addGap(5, 5, 5)
								.addComponent(l_AfficheDossierRacine, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					);
				}
				p_Centre.add(p_Fond, BorderLayout.SOUTH);
			}
			dialogPane.add(p_Centre, BorderLayout.CENTER);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);

		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel buttonBar;
	private JLabel l_Erreur;
	private JButton okButton;
	private JButton cancelButton;
	private JPanel p_Centre;
	private JPanel contentPanel;
	private JLabel l_Name;
	private JTextField tf_Name;
	private JLabel l_host;
	private JTextField tf_Host;
	private JLabel l_Port;
	private JTextField tf_Port;
	private JPanel p_Options;
	private JLabel l_DossierRacine;
	private JTextField tf_DossierRacine;
	private JCheckBox chk_Debug;
	private JLabel l_Conservlogs;
	private JTextField tf_ConservLogs;
	private JLabel l_Jours;
	private JPanel p_Fond;
	private JLabel l_AfficheDossierRacine;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
