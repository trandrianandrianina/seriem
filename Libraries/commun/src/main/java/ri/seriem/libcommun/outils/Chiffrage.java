/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * Cette classe propose des m�thodes permettant de crypter et d�crypter des messages avec l'algorithme de Blowfish.
 * Et un chiffrage en MD5 et en SHA-1
 */
public class Chiffrage {
  public final static int KEY_SIZE = 128; // [32..448]
  public final static byte[] nop = { 97, 22, 28, 120, 66, 30, 101, 109, 109, 15, 39, 44, 74, 107, 64, 9 };
  private Key secretKey;
  private String charsetName = System.getProperty("file.encoding");
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  /**
   * Constructeur
   *
   */
  public Chiffrage() {
    setSecretKey(nop);
  }
  
  /**
   * Construteur
   * @param acharsetName
   */
  public Chiffrage(String acharsetName) {
    this();
    if (acharsetName != null) {
      charsetName = acharsetName;
    }
  }
  
  public Key getSecretKey() {
    return secretKey;
  }
  
  /**
   * Retourne toutes les informations de la cl� sous forme d'un tableau de
   * bytes. Elle peut ainsi �tre stock�e puis reconstruite ult�rieurement en
   * utilisant la m�thode setSecretKey(byte[] keyData)
   */
  public byte[] getSecretKeyInBytes() {
    return secretKey.getEncoded();
  }
  
  public void setSecretKey(Key secretKey) {
    this.secretKey = secretKey;
  }
  
  /**
   * Permet de reconstruire la cl� secr�te � partir de ses donn�es, stock�es
   * dans un tableau de bytes.
   */
  public void setSecretKey(byte[] keyData) {
    if (keyData == null) {
      keyData = nop;
    }
    secretKey = new SecretKeySpec(keyData, "Blowfish");
  }
  
  public void generateKey() {
    try {
      KeyGenerator keyGen = KeyGenerator.getInstance("Blowfish");
      keyGen.init(KEY_SIZE);
      secretKey = keyGen.generateKey();
    }
    catch (Exception e) {
      msgErreur += e.getMessage();
    }
  }
  
  public byte[] cryptInBytes(byte[] plaintext) {
    try {
      Cipher cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.ENCRYPT_MODE, secretKey);
      return cipher.doFinal(plaintext);
    }
    catch (Exception e) {
      msgErreur += e.getMessage();
    }
    return null;
  }
  
  public byte[] cryptInBytes(String plaintext) {
    try {
      return cryptInBytes(plaintext.getBytes(charsetName));
    }
    catch (Exception e) {
      return null;
    }
  }
  
  public String cryptInString(String plaintext) {
    try {
      byte[] tabchaine = cryptInBytes(plaintext.getBytes(charsetName));
      return new String(tabchaine, 0, tabchaine.length, charsetName);
    }
    catch (Exception e) {
      return null;
    }
  }
  
  public byte[] decryptInBytes(byte[] ciphertext) {
    try {
      Cipher cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.DECRYPT_MODE, secretKey);
      return cipher.doFinal(ciphertext);
    }
    catch (Exception e) {
      msgErreur += e.getMessage();
    }
    return null;
  }
  
  public byte[] decryptInBytes(String ciphertext) {
    if ((ciphertext == null) || (ciphertext.equals(""))) {
      return null;
    }
    try {
      Cipher cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.DECRYPT_MODE, secretKey);
      return cipher.doFinal(ciphertext.getBytes(charsetName));
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += e.getMessage();
    }
    return null;
  }
  
  public String decryptInString(byte[] ciphertext) {
    byte[] tab = decryptInBytes(ciphertext);
    if (tab == null) {
      return null;
    }
    return new String(tab);
  }
  
  public String decryptInString(String ciphertext) {
    byte[] tab = decryptInBytes(ciphertext);
    if (tab == null) {
      return null;
    }
    return new String(tab);
  }
  
  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Chiffrage en MD5
   * @param input
   * @return
   */
  public static String md5(String input) {
    String result = input;
    if (input != null) {
      try {
        MessageDigest md = MessageDigest.getInstance("MD5"); // or "SHA-1"
        md.update(input.getBytes());
        BigInteger hash = new BigInteger(1, md.digest());
        result = hash.toString(16);
        while (result.length() < 32) { // 40 for SHA-1
          result = "0" + result;
        }
      }
      catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
    }
    return result;
  }
  
  /**
   * Chiffrage en SHA-1
   * @param input
   * @return
   */
  public static String sha1(String input) {
    String result = input;
    if (input != null) {
      try {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(input.getBytes());
        BigInteger hash = new BigInteger(1, md.digest());
        result = hash.toString(16);
        while (result.length() < 40) {
          result = "0" + result;
        }
      }
      catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
    }
    return result;
  }
  
}
