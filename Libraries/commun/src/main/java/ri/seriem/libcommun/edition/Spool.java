/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

/**
 * Description d'un spool
 */
public class Spool {
  // Variables
  private String nom = null;
  private int nbrLignes = 0;
  private int nbrColonnes = 0;
  private int nbrLigneInch = 0;
  private boolean portrait = true;
  private int nbrPages = 0;
  private String[] pages = null;
  
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    if (nom != null) {
      this.nom = nom.trim();
    }
    else {
      this.nom = null;
    }
  }
  
  /**
   * @return the nom
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * @return the nbrLignes
   */
  public int getNbrLignes() {
    return nbrLignes;
  }
  
  /**
   * @param nbrLignes the nbrLignes to set
   */
  public void setNbrLignes(int nbrLignes) {
    this.nbrLignes = nbrLignes;
  }
  
  /**
   * @return the nbrColonnes
   */
  public int getNbrColonnes() {
    return nbrColonnes;
  }
  
  /**
   * @param nbrColonnes the nbrColonnes to set
   */
  public void setNbrColonnes(int nbrColonnes) {
    this.nbrColonnes = nbrColonnes;
  }
  
  /**
   * @return the nbrLigneInch
   */
  public int getNbrLigneInch() {
    return nbrLigneInch;
  }
  
  /**
   * @param nbrLigneInch the nbrLigneInch to set
   */
  public void setNbrLigneInch(int nbrLigneInch) {
    this.nbrLigneInch = nbrLigneInch;
  }
  
  /**
   * @return the portrait
   */
  public boolean isPortrait() {
    return portrait;
  }
  
  /**
   * @param portrait the portrait to set
   */
  public void setPortrait(boolean portrait) {
    this.portrait = portrait;
  }
  
  /**
   * @return the nbrPages
   */
  public int getNbrPages() {
    return nbrPages;
  }
  
  /**
   * @param nbrPages the nbrPages to set
   */
  public void setNbrPages(int nbrPages) {
    this.nbrPages = nbrPages;
  }
  
  /**
   * @return the pages
   */
  public String[] getPages() {
    return pages;
  }
  
  /**
   * @param pages the pages to set
   */
  public void setPages(String[] pages) {
    this.pages = pages;
    if (pages != null) {
      setNbrPages(pages.length);
    }
  }
  
  /**
   * @param pages the pages to set
   */
  public void setPages(StringBuffer[] pages) {
    if (pages != null) {
      this.pages = new String[pages.length];
      for (int i = 0; i < pages.length; i++) {
        this.pages[i] = pages[i].toString();
      }
      setNbrPages(this.pages.length);
    }
    else {
      this.pages = null;
    }
  }
  
}
