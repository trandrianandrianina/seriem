//=================================================================================================
//==>                                                                       28/11/2013 - 28/11/2013
//==> Affiche un �cran avec barre de progression si n�cessaire 
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

public class SplashScreen extends JWindow
{
	// Constantes
	private static final long serialVersionUID = 1L;

	// Variables
	private JProgressBar progressBar = new JProgressBar();

	/**
	 * Cosntructeur
	 * @param message
	 * @param imageIcon
	 */
	public SplashScreen(String amessage, ImageIcon aimage)
	{
		try
		{
			init(amessage, aimage);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * Initialise la fen�tre
	 * @param amessage
	 * @param aimage
	 * @throws Exception
	 */
	private void init(String amessage, ImageIcon aimage) throws Exception
	{
		getRootPane().setBorder(BorderFactory.createLineBorder(Color.black));
		getContentPane().setLayout(new BorderLayout());
		getContentPane().setBackground(Color.WHITE);

		// Le texte
		if ((amessage != null) && (!amessage.trim().equals("")))
		{
			JLabel l_text = new JLabel();
			l_text.setText(amessage);
			l_text.setOpaque(false);
			getContentPane().add(l_text, BorderLayout.NORTH);
		}
		// L'image
		if (aimage != null)
		{
			JLabel l_image = new JLabel();
			l_image.setIcon(aimage);
			getContentPane().add(l_image, BorderLayout.CENTER);
		}
		// La progressBar
		getContentPane().add(progressBar, BorderLayout.SOUTH);
		progressBar.setOpaque(false);
		
		pack();
		setLocationRelativeTo(getOwner());
	}

	public void setProgressMax(int maxProgress)
	{
		progressBar.setMaximum(maxProgress);
	}

	public void setProgress(int progress)
	{
		final int theProgress = progress;
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				progressBar.setValue(theProgress);
			}
		});
	}

	public void setProgress(String textinprogress, int progress)
	{
		final int theProgress = progress;
		final String theMessage = textinprogress;
		setProgress(progress);
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				progressBar.setValue(theProgress);
				setMessage(theMessage);
			}
		});
	}

	public void setScreenVisible(boolean b)
	{
		final boolean boo = b;
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				setVisible(boo);
			}
		});
	}

	private void setMessage(String textinprogress)
	{
		if (textinprogress == null)
		{
			textinprogress = "";
			progressBar.setStringPainted(false);
		}
		else
			progressBar.setStringPainted(true);
		progressBar.setString(textinprogress);
	}

}
