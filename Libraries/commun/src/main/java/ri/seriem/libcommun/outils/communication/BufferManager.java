//=================================================================================================
//==>                                                                       04/11/2015 - 06/01/2016
//==> Stocke les messages au sens brut (int, byte[], ...) dans un buffer
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils.communication;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import ri.seriem.libcommun.outils.Constantes;

public class BufferManager
{
	// Constantes
	public		final static	int							LEN_INTEGER				= 4;
	public		final static	int							LEN_LONG				= LEN_INTEGER * 2;
	
	// Variables
	protected	static			String						codepage				= System.getProperty("file.encoding");
	
	protected					int							tailleByteArray			= 0;
	protected 					ByteBuffer					bbinteger				= ByteBuffer.allocate(LEN_INTEGER);
	protected					BufferHeader				bheader					= new BufferHeader();
	protected					ByteArrayOutputStream		bbody 					= new ByteArrayOutputStream(Constantes.TAILLE_BUFFER);
	protected					String						msgError				= "";


	/**
	 * Constructeur
	 */
	public BufferManager(){}

	/**
	 * Constructeur
	 * @param idpart1
	 * @param idpart2
	 */
	public BufferManager(int idpart1, int idpart2)
	{
		//bheader.setIdpart1(idpart1);
		bheader.setIdpart1(0); // On force � z�ro pour ins�rer un id calcul� avec l'horloge directement dans BufferHeader (TODO � am�liorer plus tard) 
		bheader.setIdpart2(idpart2);
		bheader.reset();
	}


	// -- M�thodes priv�es ----------------------------------------------------

	
	/**
	 * Ajout de donn�es dans le buffer
	 * @param objet
	 */
	private void addInBuffer(Object objet){
		if( objet instanceof Integer ){
			bbody.write( bbinteger.putInt(0, (Integer)objet).array(), 0, LEN_INTEGER );
			tailleByteArray += LEN_INTEGER;
		}
		else if( objet instanceof byte[] ){
			bbody.write( bbinteger.putInt(0, ((byte[])objet).length).array(), 0, LEN_INTEGER );
			bbody.write( (byte[])objet, 0, ((byte[])objet).length );
			tailleByteArray += LEN_INTEGER + ((byte[])objet).length;
		}
		else if( objet instanceof String ){
			bbody.write( bbinteger.putInt(0, ((String)objet).length()).array(), 0, LEN_INTEGER );
			try{ bbody.write( ((String)objet).getBytes(codepage), 0, ((String)objet).length() );	}catch (UnsupportedEncodingException e){e.printStackTrace();}
			tailleByteArray += LEN_INTEGER + ((String)objet).length();
		}
		bheader.setArraysize(tailleByteArray);
	}


	// -- M�thodes prot�g�es --------------------------------------------------

	
	

	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Vide le buffer du message
	 */
	public void clearBuffer(boolean clearhead)
	{
		tailleByteArray = 0;
		bheader.setArraysize(tailleByteArray);
		bbody.reset();
		if( clearhead ){
			bheader.reset();
		}
	}

	/**
	 * Ajoute un �l�ment dans le buffer du message
	 * @param object
	 */
	public void addBuffer(Object object)
	{
		addInBuffer(object);
	}

	/**
	 * Ajoute un ou plusieurs �l�ments dans le buffer du message
	 * @param object
	 */
	public void addBuffer(Object... objects)
	{
		for( Object o : objects ){
			addInBuffer( o );
		}
	}

	/**
	 * Pr�pare le buffer final en y incorporant l'id session
	 * @return le bbuffer
	 */
	public ByteArrayOutputStream getBbufferToSend()
	{
		return bbody;
	}

	/**
	 * Retourne l'ent�te
	 * @return
	 */
	public BufferHeader getHeader()
	{
		return bheader;
	}

	/**
	 * Retourne l'ent�te sous la forme d'un tableau de byte
	 * @return
	 */
	public byte[] getBHeader()
	{
		return bheader.getByte();
	}

	/**
	 * @return le tailleByteArray
	 */
	public int getTotalSize()
	{
		return bheader.getArraysize();
	}

	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgError;
		msgError = "";

		return chaine;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		bheader.dispose();
		bheader = null;
		bbody = null;
		bbinteger = null;
	}


 	// -- Accesseurs ----------------------------------------------------------


	/**
	 * @return le codepage
	 */
	public static String getCodepage()
	{
		return codepage;
	}

	/**
	 * @param codepage le codepage � d�finir
	 */
	public static void setCodepage(String acodepage)
	{
		codepage = acodepage;
	}

	/**
	 * @return le idgroup
	 */
	public int getIdgroup()
	{
		return bheader.getIdGroup();
	}

	/**
	 * @param idgroup le idgroup � d�finir
	 */
	public void setIdgroup(int idgroup)
	{
		bheader.setIdGroup(idgroup);
	}

	/**
	 * @return le idsession
	 */
	public long getIdsession()
	{
		return bheader.getIdSession();
	}

	/**
	 * @param idsession le idsession � d�finir
	 */
	public void setIdsession(long idsession)
	{
		bheader.setIdSession(idsession);
	}

	/**
	 * @return le idsession
	 */
	public int getIdmessage()
	{
		return bheader.getIdmessage();
	}

	/**
	 * @param idsession le idsession � d�finir
	 */
	public void setIdmessage(int idmsg)
	{
		bheader.setIdmessage(idmsg);
		clearBuffer(false);				// TODO � faire peut �tre lors du getByte (le send en gros)
	}

}
