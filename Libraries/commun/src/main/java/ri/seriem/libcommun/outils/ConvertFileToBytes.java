/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ConvertFileToBytes {
  public ConvertFileToBytes() {
  }
  
  public String encodeFileToBase64Binary(String fileName) throws IOException {
    File file = new File(fileName);
    byte[] bytes = loadFile(file);
    byte[] encoded = Base64.encodeToByte(bytes, false);
    String encodedString = new String(encoded);
    
    return encodedString;
  }
  
  @SuppressWarnings("resource")
  private byte[] loadFile(File file) {
    byte[] bytes = null;
    InputStream is;
    try {
      is = new FileInputStream(file);
      long length = file.length();
      if (length > Integer.MAX_VALUE) {
        // File is too large
      }
      bytes = new byte[(int) length];
      
      int offset = 0;
      int numRead = 0;
      while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
        offset += numRead;
      }
      
      if (offset < bytes.length) {
        throw new IOException("Could not completely read file " + file.getName());
      }
      
      is.close();
      
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    
    return bytes;
  }
}
