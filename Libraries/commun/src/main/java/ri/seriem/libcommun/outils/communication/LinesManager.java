//=================================================================================================
//==>                                                                       30/10/2015 - 15/04/2016
//==> Gestion des connexions avec un serveur
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils.communication;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.Session;
import ri.seriem.libcommun.outils.LinkedHashMapManager.lhdListener;

public class LinesManager
{
	// Constantes
	private		final static 	String						NOM_CLASSE					= "[LineManager]";
	private		final static	int							TIMEOUT						= 1000;
	private		final static	int							NBR_TRY_SEND				= 10;

	public		final static	int							ACTION_NOP					= 0;
	public		final static	int							ACTION_CONNEXION_INPROGRESS	= 1;
	public		final static	int							ACTION_STOP_APPLICATION		= 2;
	public		final static	int							NBR_MAX_CONNEXIONS			= 6;
	public		final static	int							NBR_DEFAULT_CONNEXIONS		= 1;
	
	// Variables
	public		static		ConcurrentHashMap<Long, Session>listSessions				= new ConcurrentHashMap<Long, Session>(); 

	private						String						host						= "127.0.0.1";
	private						int							port						= Constantes.SERVER_PORT;
	public						ArrayList<LineBase>			listLines					= new ArrayList<LineBase>();	// TODO Public le temps du debug
	private						LinkedHashMapManager		listEvents					= new LinkedHashMapManager();	// TODO Voir si pas plus judicieux de la transformer en ArrayList
	private						int							action						= ACTION_NOP;
	private						int							idgroup						= -1;
	private						String						msgError					= "";
	

	/**
	 * Constructeur
	 * @param aline
	 * @param aidgroup
	 */
	public LinesManager(LineBase aline, int aidgroup)
	{
		setIdgroup(aidgroup);
		addLine(aline);
	}
	
	/**
	 * Constructeur
	 * @param host
	 * @param port
	 */
	public LinesManager(String ahost, int aport)
	{
		setHost(ahost);
		setPort(aport);
		listEvents.addListener(new lhdListener() {
			public void onDataCleared()	{}
			public void onDataRemoved(Object cle){}
			public void onDataAdded(final Object cle, final Object val){
//System.out.println("-LinesManager->R�ception d'une action � mener " + listEvents.getHashMap().size());
				action2Do(((Integer)val).intValue(), cle);
			}
		});
	}
	
	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Envoi un message sur une ligne d�sign�e
	 * @param aline
	 * @param aobject
	 * @return
	 */
	private boolean send(LineBase aline, BufferManager abufferm)
	{
		if( aline == null ){
			msgError += "\nLa ligne est � null.";
			return false;
		}

		abufferm.setIdgroup(idgroup);
		boolean ret = aline.send(abufferm, false);
		if( !ret ){
			msgError += aline.getMsgErreur();
		}
		return ret;
	}

	/**
	 * Attend une ligne disponible
	 * @return
	 */
	private LineBase waitReadyLine()
	{
		// On cherche une ligne disponible dans le pool
		int cpt = NBR_TRY_SEND;
		LineBase line = getReadyLine();
		while( line == null ){
System.out.println("-LinesManager(waitReadyLine)-> Attente d'une ligne dispo.");
			try{Thread.sleep(TIMEOUT);}catch(Exception e){}
			line = getReadyLine();
			cpt--;
			if( cpt == 0 ){
				// Ajout d'une nouvelle ligne au bout de 10 secondes d'attente
				addLine();		// Nouveau pour �viter certains lock (15/04/2016) 
System.out.println("-LinesManager(waitReadyLine)-> Ajout d'une nouvelle ligne.");
				break;
			}
		}
		return line;
	}
	
	/**
	 * Retourne une line disponible
	 * @return
	 */
	private LineBase getReadyLine()
	{
		for( LineBase line : listLines ){
			if( line.isReady() ){
				return line;
			}
		}
		
		return null;
	}
	
	/**
	 * Teste les lines et retente une connexion si elles sont ferm�es
	 * @return
	 */
	private boolean testLines()
	{
		boolean ret = true;
		for( LineBase line : listLines ){
			if( line.isClosed() ){
				ret = ret & line.open();
			}
		}
		return ret;
	}

	/**
	 * Reconnecte les lines ferm�es
	 * @return
	 */
	private boolean reconnectionLines()
	{
		boolean ret = true;
		for( LineBase line : listLines ){
			if( line.isClosed() ){
				ret = ret & line.open();
			}
		}
//System.out.println("-LineManager->reconnectionLines " + ret);		
		return ret;
	}

	/**
	 * Ajoute une connexion au pool
	 * @return
	 */
	private boolean addLine()
	{
		if( listLines.size() == NBR_MAX_CONNEXIONS ){
			msgError += "\nAjout impossible car le pool de connexion a atteint le maximum d�finit.";
			return false;
		}

		LineClient line = new LineClient(listEvents);
		line.setHost(host);
		line.setPort(port);
		LineBase.listSessions = listSessions;
		boolean ret = line.open();
		if( !ret ){
			msgError += line.getMsgErreur();
			return false;
		}
		listLines.add(line);

//System.out.println("-nbr line-> " + listLines.size() + " " + line);		
		return true;
	}

	/**
	 * Supprime une connexion du pool
	 * @return
	 */
	private boolean removeLine(boolean force)
	{
		if( listLines.isEmpty() ){
			msgError += "\nSuppression impossible car le pool de connexion est vide.";
			return true;
		}
			
		LineBase line = listLines.get(listLines.size()-1);
		boolean ret = line.disconnect();
		if( !ret ){
			msgError += line.getMsgErreur();
			if( force ){
				listLines.remove(line);
			}
			return false;
		}
		listLines.remove(line);
		
		return true;
	}

	/**
	 * Ferme et supprime toutes les connexions 
	 */
	private void removeAllLine()
	{
		for( LineBase line: listLines ){
			if( line.getState() > LineBase.STATE_CLOSED ) line.disconnect();
			line.dispose();
		}
		listLines.clear();
	}
	
	/**
	 * Action � mener 
	 * @param action
	 * @param obj
	 */
	private void action2Do(int anaction, Object obj)
	{
		// On traite l'action
		switch( anaction ){
			case LineBase.DO_RECONNEXION :
					if( action == ACTION_NOP ){
//System.out.println("-LineManager->ACTION RECONNEXION");
						waitTheReconnexion();
					}
					break;
			case ACTION_STOP_APPLICATION :
					// TODO Trouver un moyen de fermer l'application
//System.out.println("-LineManager->STOP APPLICATION");
					break;
		}
		// On enl�ve l'action de la liste 
		listEvents.removeObject(obj);
//System.out.println("-LineManager->nbr evt : " + listEvents.getHashMap().size());		
	}

	/**
	 * Active ou non la mire d'attente (en utilisant la r�flexion)
	 * TODO Ne fonctionne que pour les sessions de type RPG, il va falloir trouver une solution pour les session de type SQL 
	 * @param active
	 */
	private void activeWaitMire(boolean active)
	{
		for(Entry<Long, Session> entry : listSessions.entrySet()) {
//System.out.println("-activeWaitMire-> " + entry.getValue().getClass().getName());
			if( entry.getValue().getClass().getName().equals("sessions.SessionPanelRPG") ){
				try {
					Class c = Class.forName("sessions.SessionPanelBase");
					Method method = c.getDeclaredMethod("activeGlassPane", boolean.class);
					method.invoke(c.cast(entry.getValue()), active);
				} catch (Exception e){
					e.printStackTrace();
				}
				break;
			}
		}
	}
	
	/**
	 * Tente de reconnecter les lignes ferm�es 
	 */
	private void waitTheReconnexion()
	{
		// D�marre la mire d'attente de reconnexion
		activeWaitMire(true);
		
		action = ACTION_CONNEXION_INPROGRESS;
		boolean ret = false;
		int cpt = NBR_TRY_SEND;
		
		do{
			// On v�rifie l'�tat des lignes (si elles ne sont pas ferm�es)
			if( !testLines() ){
				msgError += "\nOn tente de connecter les lignes ferm�es.";
				try{Thread.sleep(TIMEOUT);}catch(Exception e){}
				ret = reconnectionLines(); // Elles ne sont pas Ok donc on tente une reconnexion
			} else{ // La v�rification est Ok on peut sortir
				cpt = 0;
			}
			if( !ret ){
				cpt--;
				if( cpt == 0){
					if( sendQuestionToUser("Tentative de reconnection", "Cela fait " + ((NBR_TRY_SEND * TIMEOUT)/1000) + " secondes que l'application tente de se reconnecter au serveur sans succ�s. Souhaitez vous attendre encore ?") )
						cpt = NBR_TRY_SEND;
				}
			}
		} while(cpt > 0);
		
		if( testLines() ){
			action = ACTION_NOP;
			// Stoppe la mire d'attente de reconnexion
			activeWaitMire(false);
//System.out.println("-LinesManager->waitTheReconnexion line ready:" + getReadyLine().isReady());		
		}
		else{
			action = ACTION_STOP_APPLICATION;
			listEvents.addObject(this, ACTION_STOP_APPLICATION);
		}
//System.out.println("-LinesManager->waitTheReconnexion action:" + action);		
	}
	
	/**
	 * Envoie une question � l'utilisateur
	 * @param titre
	 * @param message
	 * @return
	 */
	private boolean sendQuestionToUser(String titre, String message)
	{
		Object[] options = {UIManager.getString("OptionPane.yesButtonText", Locale.getDefault()), UIManager.getString("OptionPane.noButtonText", Locale.getDefault())};
		int rep = JOptionPane.showOptionDialog(null, message, 
				titre,
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE,
				null,
				options,
				options[1]);
		if (rep == JOptionPane.NO_OPTION) return false;
		return true;
	}

	// -- M�thodes publiques --------------------------------------------------

	/**
	 * Initialise le pool de connexions
	 * @return
	 */
	public boolean initPool()
	{
		while( listLines.size() < NBR_DEFAULT_CONNEXIONS ){
			if( !addLine() ){
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Ajoute une connexion au pool
	 * @return
	 */
	public boolean addLine(LineBase aline)
	{
		listLines.add(0, aline);
//System.out.println("-nbr line-> " + listLines.size() + " " + aline);		
		return true;
	}

	/**
	 * Envoi un buffer au serveur
	 * @param aobject
	 * @return
	 */
	public boolean send(BufferManager abufferm)
	{
//System.out.println("-LinesManager-send " + abufferm.getIdsession() + " " + abufferm.getIdmessage());
		boolean ret = false;

		// On cherche une ligne disponible dans le pool
		LineBase line = waitReadyLine();
//System.out.println("-LinesManager-send with " + line + " " + listLines.size());
		// On a trouv� une ligne Ready
		if( line != null ){
			ret = send(line, abufferm);
		} else{
			msgError += "\nAucune ligne disponible pour l'envoi.";
		}

		return ret;
	}

	/**
	 * Supprime une connexion identifi�e du pool
	 * @return
	 */
	public boolean removeLine(LineBase aline, boolean force)
	{
		if( listLines.isEmpty() ){
			msgError += "\nSuppression impossible car le pool de connexion est vide.";
			return true;
		}
			
		/*boolean ret = aline.disconnect();
		if( !ret ){
			msgError += aline.getMsgErreur();
			if( force ){
				listLines.remove(aline);
			}
			return false;
		}*/
		listLines.remove(aline);
		
		return true;
	}
	
	/**
	 * Retourne l'adresse IP du client connect�
	 * @return
	 */
	public String getClientIP()
	{
		for( LineBase line: listLines ){
			if( line.getState() > LineBase.STATE_CLOSED ){
				return line.getIpsocket().getIP();
			}
		}
		return "";
	}

	/**
	 * Retourne l'adresse IP du client connect� sans les points
	 * @return
	 */
	public String getClientIPwoPoint()
	{
		for( LineBase line: listLines ){
			if( line.getState() > LineBase.STATE_CLOSED ){
				return line.getIpsocket().getIPwoPoint();
			}
		}
		return "";
	}


	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgError;
		msgError = "";

		return chaine;
	}

	/**
	 * Ajoute une session � la liste
	 * @param asession
	 */
 	public void addSession2List(Session asession)
 	{
 		listSessions.put(asession.getIdSession(), asession);
 	}

	/**
	 * Supprime une session � la liste
	 * @param asession
	 */
 	public void removeSession2List(long id)
 	{
//System.out.println("-LinesManager-removeSession2List->id:" + id + " " + listSessions.size()); 		
 		listSessions.remove(id);
//System.out.println("-LinesManager-removeSession2List->id:" + id + " " + listSessions.size()); 		
 	}

	/**
	 * Lib�re les ressources
	 */
 	public void dispose()
	{
 		listSessions = null;
 		removeAllLine();
		listLines = null;
		listEvents.clearObject();
		listEvents = null;
	}

 	// -- Accesseurs ----------------------------------------------------------
 	
	/**
	 * @return le host
	 */
	public String getHost()
	{
		return host;
	}

	/**
	 * @param host le host � d�finir
	 */
	public void setHost(String host)
	{
		this.host = host;
	}

	/**
	 * @return le port
	 */
	public int getPort()
	{
		return port;
	}

	/**
	 * @param port le port � d�finir
	 */
	public void setPort(int port)
	{
		this.port = port;
	}

	/**
	 * @return le idgroup
	 */
	public int getIdgroup()
	{
		return idgroup;
	}

	/**
	 * @param idgroup le idgroup � d�finir
	 */
	public void setIdgroup(int idgroup)
	{
		this.idgroup = idgroup;
	}

}
