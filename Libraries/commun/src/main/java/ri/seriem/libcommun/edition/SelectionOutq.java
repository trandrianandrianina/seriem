/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.jdesktop.swingx.VerticalLayout;

/**
 * Affiche la liste des imprimantes connect�es au System I
 */
public class SelectionOutq extends JDialog {
  // Variables
  private GfxOutQPanel[] listePanelOutq = null;
  private ActionListener action = null;
  private boolean modeSelection = false;
  private ArrayList<OutQ> listeOutq = null;
  
  /**
   * Constructeur
   * @param owner
   * @param alisteoutq
   * @param modeselection
   */
  public SelectionOutq(Frame owner, ArrayList<OutQ> alisteoutq, boolean modeselection) {
    super(owner);
    modeSelection = modeselection;
    initComponents();
    listeOutq = alisteoutq;
    generepanel();
  }
  
  /**
   * Constructeur
   * @param owner
   * @param alisteoutq
   * @param modeselection
   */
  public SelectionOutq(Dialog owner, ArrayList<OutQ> alisteoutq, boolean modeselection) {
    super(owner);
    modeSelection = modeselection;
    initComponents();
    listeOutq = alisteoutq;
    generepanel();
  }
  
  /**
   * Initialise le panel principal avec la liste des outqs et en fonction du mode
   */
  private void generepanel() {
    if (modeSelection) {
      setTitle("S�lection des outq");
    }
    else {
      setTitle("S�lection de l'outq");
    }
    
    if (listeOutq == null) {
      return;
    }
    
    // On g�n�re l'action � effectuer lors de la s�lection d'une outq
    action = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    };
    
    listePanelOutq = new GfxOutQPanel[listeOutq.size()];
    for (int i = 0; i < listeOutq.size(); i++) {
      listePanelOutq[i] = new GfxOutQPanel(listeOutq.get(i), modeSelection);
      listePanelOutq[i].setAction(action);
      p_ListeOutq.add(listePanelOutq[i]);
    }
    
    validate();
    repaint();
  }
  
  public void setAlreadySelectionned(ArrayList<String> listeoutqactuelles) {
    if (listeoutqactuelles == null) {
      return;
    }
    
    for (int i = 0; i < listeOutq.size(); i++) {
      for (int j = 0; j < listeoutqactuelles.size(); j++) {
        if (listeOutq.get(i).getNom().equals(listeoutqactuelles.get(j))) {
          listePanelOutq[i].setSelectionned(true);
          listeOutq.get(i).setSelection(true);
        }
      }
    }
  }
  
  /**
   * Gestion du filtre pour la recherche d'une outq
   */
  private void filtrepanel(String filtre) {
    if ((filtre == null) || (listePanelOutq == null)) {
      return;
    }
    
    // Pas de filtre, on affiche tout
    if (filtre.length() == 0) {
      for (int i = 0; i < listePanelOutq.length; i++) {
        listePanelOutq[i].setVisible(true);
      }
    }
    // Sinon on affiche les outqs qui commence par le filtre
    else {
      for (int i = 0; i < listePanelOutq.length; i++) {
        listePanelOutq[i].setVisible(listePanelOutq[i].getOutq().getNom().startsWith(filtre));
      }
    }
    
    validate();
    repaint();
  }
  
  private void valideSelection() {
    for (int i = 0; i < listeOutq.size(); i++) {
      listeOutq.get(i).setSelection(listePanelOutq[i].isSelectionned());
    }
  }
  
  private void cancelButtonActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void tf_NomImprimanteCaretUpdate(CaretEvent e) {
    filtrepanel(tf_NomOutq.getText().toUpperCase());
  }
  
  private void bt_EnregistrerActionPerformed(ActionEvent e) {
    valideSelection();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    l_NomOutq = new JLabel();
    tf_NomOutq = new JTextField();
    scrp_ListeOutq = new JScrollPane();
    p_ListeOutq = new JPanel();
    buttonBar = new JPanel();
    bt_Enregistrer = new JButton();
    cancelButton = new JButton();
    
    // ======== this ========
    setTitle("S\u00e9lection d'une outq");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setMinimumSize(new Dimension(550, 400));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setName("dialogPane");
      dialogPane.setLayout(new BorderLayout());
      
      // ======== contentPanel ========
      {
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(new BorderLayout());
        
        // ======== panel1 ========
        {
          panel1.setName("panel1");
          panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
          
          // ---- l_NomOutq ----
          l_NomOutq.setText("Nom de l'outq");
          l_NomOutq.setName("l_NomOutq");
          panel1.add(l_NomOutq);
          
          // ---- tf_NomOutq ----
          tf_NomOutq.setPreferredSize(new Dimension(115, 28));
          tf_NomOutq.setName("tf_NomOutq");
          tf_NomOutq.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent e) {
              tf_NomImprimanteCaretUpdate(e);
            }
          });
          panel1.add(tf_NomOutq);
        }
        contentPanel.add(panel1, BorderLayout.NORTH);
        
        // ======== scrp_ListeOutq ========
        {
          scrp_ListeOutq.setName("scrp_ListeOutq");
          
          // ======== p_ListeOutq ========
          {
            p_ListeOutq.setName("p_ListeOutq");
            p_ListeOutq.setLayout(new VerticalLayout());
          }
          scrp_ListeOutq.setViewportView(p_ListeOutq);
        }
        contentPanel.add(scrp_ListeOutq, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      
      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setName("buttonBar");
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 85, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0 };
        
        // ---- bt_Enregistrer ----
        bt_Enregistrer.setVisible(modeSelection);
        bt_Enregistrer.setText("Enregistrer");
        bt_Enregistrer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Enregistrer.setName("bt_Enregistrer");
        bt_Enregistrer.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_EnregistrerActionPerformed(e);
          }
        });
        buttonBar.add(bt_Enregistrer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cancelButton ----
        cancelButton.setText("Fermer");
        cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cancelButton.setName("cancelButton");
        cancelButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            cancelButtonActionPerformed(e);
          }
        });
        buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel l_NomOutq;
  private JTextField tf_NomOutq;
  private JScrollPane scrp_ListeOutq;
  private JPanel p_ListeOutq;
  private JPanel buttonBar;
  private JButton bt_Enregistrer;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
