//=================================================================================================
//==>                                                                       12/01/2015 - 14/03/2016
//==> Lanceur de requ�te SQL / Dataarea 
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import ri.seriem.libcommun.outils.ArrayListManager.alListener;
import ri.seriem.libcommun.protocoleMsg.DataareaRequest;
import ri.seriem.libcommun.protocoleMsg.Db2Request;
import ri.seriem.libcommun.protocoleMsg.MessageManager;
import ri.seriem.libcommun.protocoleMsg.UserspaceRequest;

public abstract class LaunchRequest
{
	// Variables
	protected EnvUser infoUser=null;
	private int hashcodecaller = this.hashCode();
	private MessageManager mc = new MessageManager(hashcodecaller);
	private alListener listener = null;
	//private long t1=0;

	/**
	 * Constructeur
	 * @param ainfoUser
	 */
	public LaunchRequest(EnvUser ainfoUser)
	{
		infoUser = ainfoUser;
		initSystem();
	}
	
	/**
	 * Initialisation du syst�me
	 */
	private void initSystem()
	{
		listener = new alListener() {
			public void onDataRemoved(Object val) {}
			public void onDataRemoved(int indice) {}
			public void onDataCleared() {}
			public void onDataAdded(Object val)
			{
				traitementMessage((String)val);
			}
		}; 
		infoUser.getTransfertSession().getEvenementSysteme().addListener(listener);
	}

	/**
	 * Traitement d'un message re�u
	 * @param message
	 */
	protected void traitementMessage(String message)
	{
//System.out.println(message);
//infoUser.getLog().ecritureMessage(message);
		if (!mc.getMessageReceived(message, hashcodecaller))
			return;

		if (mc.getBody() != null){
			if( mc.getBody() instanceof Db2Request ){
				Db2Request db2 = (Db2Request) mc.getBody();
				traitementData(db2);
			} else if( mc.getBody() instanceof DataareaRequest ){
				DataareaRequest	dta = (DataareaRequest) mc.getBody();
				traitementData(dta);
			} else if( mc.getBody() instanceof UserspaceRequest ){
				UserspaceRequest uspc = (UserspaceRequest) mc.getBody();
				traitementUserspace(uspc);
			} //else
				
		}

		// Suppression de l'objet de la liste, une fois trait�
		infoUser.getTransfertSession().getEvenementSysteme().removeObject(message);

		fintraitement();
		//double delai = System.currentTimeMillis() - t1; 
		//System.out.println(" R�ponse Temps d'execution : " + (delai<1000?delai:(delai/1000)) + (delai<1000?" ms":" sec"));
	}

	protected abstract boolean traitementData(Db2Request db2);
	protected abstract boolean traitementData(DataareaRequest dta);
	protected abstract boolean traitementUserspace(UserspaceRequest uspc);

	/**
	 * Op�rations � effecter en fin de Traitement 
	 */
	protected abstract void fintraitement();

	/**
	 * Envoi une requ�te Db2 syst�me au serveur
	 * @param requete
	 * @param action
	 */
	public void sendRequestDb2(String requete, int action)
	{
		mc.createMessage(false, new Db2Request(requete, action));
		//listeRequete.put(compteurID, null);
		
		//t1 = System.currentTimeMillis();
		//infoUser.getTransfertSession().EnvoiMessageSocket(Constantes.ENVOI_REQUETE_SYSTEME, mc.getMessageToSend());
		infoUser.getTransfertSession().sendBuffer(Constantes.ENVOI_REQUETE_SYSTEME, mc.getMessageToSend());
	}

	/**
	 * Envoi une requ�te Dataarea syst�me au serveur (� �liminer)
	 * @param adataarea
	 * @param alibrary
	 * @param action
	 */
	public void sendRequestDta(String adataarea, String alibrary, int action)
	{
		mc.createMessage(false, new DataareaRequest(adataarea, alibrary, action));

		infoUser.getTransfertSession().sendBuffer(Constantes.ENVOI_REQUETE_SYSTEME, mc.getMessageToSend());
	}

	/**
	 * Envoi une requ�te Dataarea syst�me au serveur (non test�)
	 * @param adataarea
	 * @param alibrary
	 * @param data
	 * @param action
	 */
	public void sendRequestDta(String adataarea, String alibrary, String data, int action)
	{
		DataareaRequest dta = new DataareaRequest(adataarea, alibrary, action);
		dta.setData(data);
		mc.createMessage(false, dta);

		infoUser.getTransfertSession().sendBuffer(Constantes.ENVOI_REQUETE_SYSTEME, mc.getMessageToSend());
	}

	/**
	 * Envoi une requ�te Userspace syst�me au serveur
	 * @param auserspace
	 * @param alibrary
	 * @param data
	 * @param action
	 */
	public void sendRequestUspc(String auserspace, String alibrary, String data, int action)
	{
		UserspaceRequest uspc = new UserspaceRequest(auserspace, alibrary, action);
		uspc.setData(data);
		mc.createMessage(false, uspc);

		infoUser.getTransfertSession().sendBuffer(Constantes.ENVOI_REQUETE_SYSTEME, mc.getMessageToSend());
	}

	/**
	 * Lib�re la m�moire
	 */
	public void dispose()
	{
		infoUser.getTransfertSession().getEvenementSysteme().removeListener(listener);
	}

}
