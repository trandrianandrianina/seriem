//=================================================================================================
//==>                                                                       05/07/2006 - 19/11/2015
//==> Gestion des environnements des RunTimes 
//==> A faire:
//==>    - Faire une methode qui v�rifie l'integrit� des donn�es lues (au moins qu'il y ait le mini)
//==>    - au moins du fichier INI que toutes les propri�t�s soient bien renseign�es 
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class GestionRTConfig
{
    private final static String NOM_CLASSE="[GestionRTConfig]";
    
    // Constantes erreurs de chargement
    private final static String ERREUR_RENCONTREE="Erreur rencontr\u00e9e.";
    private final static String ERREUR_RUNTIME_NONTROUVE="Le dossier des Runtimes est vide : ";
    private final static String ERREUR_CONSTITUTION_CHAINE="Erreur lors de la constitution de la chaine contenant la liste.";
//    private final static String ERREUR_DECOUPAGE_CHAINE="Erreur lors du d\u00e9coupage de la chaine contenant la liste.";
//    private final static String ERREUR_NOMBRE_ELT="Erreur le nombre d'\u00e9l\u00e9ments du tableau est incorrect (non divisible par 3) : ";
    

    // Variables
    private LinkedHashMap<String, HashMap<String, String>> listeConfig=null;
    private String dossierRacine=null;
    private String nomFichier=Constantes.FIC_CONFIG;
    private HashMap<String, GestionRT> gestionRT=new HashMap<String, GestionRT>();
	private JsonParser parser = new JsonParser();
	private Gson gson = new Gson();

    private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

    /**
     * Constructeur de la classe
     */
    public GestionRTConfig()
    { }

    /**
     * Constructeur de la classe
     * @param dossier
     * @param nomfic
     */
    public GestionRTConfig(String dossier, String nomfic)
    {
        dossierRacine = dossier;
        nomFichier = nomfic;
    }

    /**
     * Constructeur de la classe
     * @param dossier
     */
    public GestionRTConfig(String dossier)
    {
        this(dossier, Constantes.FIC_CONFIG);
    }

    /**
     * V�rifie l'existence d'un fichier et le lit s'il existe
     */
    private int chargeConfig()
    {
    	// On lit le fichier INI
    	GestionFichierINI gfi = new GestionFichierINI(dossierRacine + File.separator + nomFichier);
    	if (gfi.lectureFichier() == Constantes.FALSE)
    	{
    		msgErreur =  gfi.getMsgErreur();
    		return Constantes.ERREUR;
    	}
       
       // R�cup�ration des infos
       listeConfig = gfi.getSections();
       
       return Constantes.OK;
    }

    /**
     * Ecrit le fichier rt.ini
     */
    private void writeFile()
    {
    	ArrayList<String> listeINI=new ArrayList<String>(), tempo=null;
    	GestionRT gRT=null;
    	
    	// Pr�paration enregistrement
        //Set cles = gestionRT.keySet();
        Iterator<String> it = gestionRT.keySet().iterator();
        while (it.hasNext())
        {
        	gRT = gestionRT.get(it.next());
        	tempo = gRT.getArrayList();
            for (int i=0; i<tempo.size(); i++)
            	listeINI.add(tempo.get(i));
        	listeINI.add("");
        }        

        // Enregistrement
    	GestionFichierINI gfi = new GestionFichierINI(dossierRacine + File.separator + nomFichier);
    	gfi.setContenuFichier(listeINI);
    	gfi.ecritureFichier();
    }
    
    /**
     * Ajoute un environnement
     * @param gRT
     */
    public void addGestionRT(GestionRT gRT)
    {
    	if (gRT != null)
    	{
    		gestionRT.put(gRT.getNomConfig(), gRT);
    		writeFile();
    	}
    }

    /**
     * Supprime un environnement
     * @param nomconfig
     */
    public void removeGestionRT(String nomconfig)
    {
    	if (nomconfig != null)
    	{
    		gestionRT.remove(nomconfig);
    		writeFile();
    	}
    }
 
    /**
     * Initialise la liste des Runtimes d'une config via une chaine
     * @param config
     * @param valeur
     * @return
     */
    public int setListeRT(String config, String valeur)
    {
        // Constitution de la chaine
        GestionRT gRT = gestionRT.get(config.trim());
        if (gRT == null)
        {
            msgErreur =  NOM_CLASSE + Constantes.SEPARATEUR + "Pas de config avec ce nom : " + config;
            return Constantes.ERREUR;
        }
        else
        	if (gRT.setListeRTJSON(valeur) == Constantes.OK)
        		return Constantes.OK;
        	else
        	{
        		msgErreur = gRT.getMsgErreur();
        		return Constantes.ERREUR;
        	}
    }

    /**
     * Traitement pour chaque environnement des RT
     */
    private void getInitConfig()
    {
        GestionRT gRT=null;
        String nomEnv=null;
        HashMap<String, String> detailEnv=null;

        // On v�rifie que l'on a bien lu le fichier RT.INI dans le cas contraire on le lit
        if (listeConfig == null)
            if (chargeConfig() == Constantes.ERREUR)
            {
                msgErreur = getMsgErreur();
                return;
            }

        // On charge les infos de chaque environnement dans un tableau
        Iterator<Entry<String, HashMap<String, String>>> iterateur = listeConfig.entrySet().iterator();
        while(iterateur.hasNext())
        {
            Map.Entry donnee = iterateur.next();
            nomEnv = (String) donnee.getKey();
            detailEnv = (HashMap<String, String>) donnee.getValue();
            // On test la cl� qui a chang� entre temps car le nouveau nom est plus logique (20/05/2014) 
            String jobq = detailEnv.get("jobq");
            if (jobq == null)
            	jobq = detailEnv.get("subsystem");
            String val = detailEnv.get("titlebarinformations");
            if (val == null)
            	val = "";
            String ctrlprf = detailEnv.get("ctrlprf");
            if (ctrlprf == null)
            	ctrlprf = "";
            // Pour chaque config trouv�e, on charge le tableau avec les valeurs lues dans le fichier INI
            gRT = new GestionRT(dossierRacine, nomEnv, 
                                         detailEnv.get("host"), 
                                         detailEnv.get("prefix_device"),
                                         jobq,
                                         detailEnv.get("program"),
                                         detailEnv.get("library"),
                                         detailEnv.get("library_env"),
                                         detailEnv.get("folder"),
                                         val.equals("true"),
                                         detailEnv.get("typemenu"),
                                         ctrlprf.equals("true")
                                         );
            if (detailEnv.containsKey("accepted"))
            	gRT.setAccepted(detailEnv.get("accepted"));
            // On v�rifie l'existence du dossier contenant les RunTimes de l'environnement courant
            if (!gRT.isPresent())
                msgErreur = NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RUNTIME_NONTROUVE + gRT.getNomConfig();
            else
                if (gRT.balayageDossier() == Constantes.ERREUR)
                   msgErreur = gRT.getMsgErreur();
            gestionRT.put(nomEnv, gRT);
        }
   }

    /**
     * Retourne le tableau (des environnements) des infos pour chaque version de Runtime disponible
     */
    public HashMap<String, GestionRT> getInfosConfig()
    {
    	getInitConfig(); 
        return gestionRT;
    }

    /**
     * Initialise la liste des environnements via une chaine
     * @param valeur
     * @return
	 * API JsonSimple
    public int setListeRTConfig(String valeur)
    {
        int i=0, j=0;
        int nbrelt=0;
        String liste[]=null;

        // On d�coupe la chaine et on constitue un tableau de string � partir des diff�rents �l�ments
//System.out.println("[GestionRTConfig] (setListeRTConfig) " + valeur);        
        liste = Constantes.splitString(valeur, Constantes.SEPARATEUR_CHAINE_CHAR);
        if ((liste == null) || (liste.equals("")))
        {
            msgErreur =  NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_DECOUPAGE_CHAINE;
            return Constantes.ERREUR;
        }

        // On v�rifie la validit� du tableau
        nbrelt = Integer.parseInt(liste[liste.length-1]) + 2; // pour le 1� & le dernier elt de la chaine (nbr de config + nbr de prop)

        if (nbrelt != liste.length)
        {
            msgErreur =  NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_NOMBRE_ELT + liste.length;
            return Constantes.ERREUR;
        }
        else
            nbrelt = (nbrelt-2) / GestionRT.NBR_PROPRIETES;

        // On charge la liste des configs � partir de la liste obtenue
        for (j=0, i=1; j<nbrelt; i+=GestionRT.NBR_PROPRIETES, j++)
        	gestionRT.put(liste[i], new GestionRT(liste[i], liste[i+1], liste[i+2], liste[i+3], liste[i+4], liste[i+5], liste[i+6], liste[i+7]));

        return Constantes.OK;
    }*/

    /**
     * Initialise la liste des environnements via une chaine JSON
     * @param valeur
     * @return
     *
    public int setListeRTConfigJSON(String valeur)
    {
		JSONParser parser = new JSONParser();

//System.out.println("/////////////////////////> chaine " + valeur);
		
		try
		{
			JSONArray array = (JSONArray) parser.parse(valeur);
			for (int i=0; i<array.size(); i++)
			{
				GestionRT gRT = new GestionRT((JSONObject) array.get(i));
				gestionRT.put(gRT.getNomConfig(), gRT);
			}
			
	        // Permet de r�cup�rer la m�moire
	        array.clear();
		}
		catch (Exception e)
		{
            return Constantes.ERREUR;
		}

        return Constantes.OK;
    }*/

    /**
     * Initialise la liste des environnements via une chaine JSON
     * @param valeur
     * @return
     */
    public int setListeRTConfigJSON(String valeur)
    {
		try
		{
			JsonArray array = (JsonArray) parser.parse(valeur);
			for (int i=0; i<array.size(); i++)
			{
				GestionRT gRT = new GestionRT((JsonObject) array.get(i));
				gestionRT.put(gRT.getNomConfig(), gRT);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();			
            return Constantes.ERREUR;
		}

        return Constantes.OK;
    }

    /**
     * Retourne la liste des environnements disponibles & op�rationnels sous forme d'une chaine de caract�res
     *
    public String getListeRTConfig(String ipclient, ArrayList<String> sbsactifs)
    {
        int nbrConfig=0;
        StringBuffer chaine=null;
        GestionRT gRT=null;

        if (gestionRT.size() == 0)
        {
            msgErreur = ERREUR_CONSTITUTION_CHAINE;
            return null;
        }
        
        // Constitution de la chaine nombre d'environnements
        chaine = new StringBuffer(Constantes.MAXVAR);
        // D�tail de chaque environnement
        //Set cles = gestionRT.keySet();
        Iterator<String> it = gestionRT.keySet().iterator();
        while (it.hasNext())
        {
        	gRT = gestionRT.get(it.next());
//System.out.println("-[GestionRTConfig] - (getListeRTConfig) ->#" + gRT.getLibrary()+Constantes.SEPARATEUR_CHAINE_CHAR+gRT.getSubSystem()+"#");        	
//System.out.println("-[GestionRTConfig] - (getListeRTConfig) ->#" + gRT.getNomConfig());
            if (gRT.isPresent() && gRT.isAccepted(ipclient) && (sbsactifs.contains(gRT.getSubSystem())) )
            {// ATTENTION l'ordre est super important voir GestionRT (constructeur)
            	nbrConfig++;
                chaine.append(gRT.getNomConfig()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getHost()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getPrefixDevice()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getSubSystem()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getProgram()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getLibrary()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getLibraryEnv()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
                      .append(gRT.getFolder()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
            }
        }
        sbsactifs = null;
        return chaine.insert(0, Constantes.SEPARATEUR_CHAINE_CHAR).insert(0, nbrConfig).append(nbrConfig * GestionRT.NBR_PROPRIETES).toString();
    }*/

    /**
     * Retourne la liste des environnements disponibles & op�rationnels sous forme d'une chaine JSON
	 * API JsonSimple
    @SuppressWarnings("unchecked")
	public String getListeRTConfigJSON(String ipclient, ArrayList<String> sbsactifs)
    {
        GestionRT gRT=null;
        String chaine=null;

        if (gestionRT.size() == 0)
        {
            msgErreur = ERREUR_CONSTITUTION_CHAINE;
            return null;
        }

        // On cr�� le tableau qui va recevoir les envirommenents
		JSONArray arrayEnv = new JSONArray();

        // D�tail de chaque environnement
        for(Entry<String, GestionRT> entry : gestionRT.entrySet())
        {
        	gRT = entry.getValue();
            if (gRT.isPresent() && gRT.isAccepted(ipclient) ) //&& (sbsactifs.contains(gRT.getSubSystem())) ) // Comment� le 09/12/2013 car on a pas le sous-systeme mais la jobq  
            	arrayEnv.add(gRT.getObjetJSON());
        }
        chaine = arrayEnv.toJSONString();
        
        // Lib�ration de la m�moire
        sbsactifs.clear();
        sbsactifs = null;
        arrayEnv.clear();
        
        return chaine;
    }*/

    /**
     * Retourne la liste des environnements disponibles & op�rationnels sous forme d'une chaine JSON
     */
	public String getListeRTConfigJSON(String ipclient, ArrayList<String> sbsactifs)
    {
        GestionRT gRT=null;
        String chaine=null;

        if (gestionRT.size() == 0)
        {
            msgErreur = ERREUR_CONSTITUTION_CHAINE;
            return null;
        }

        // On cr�� le tableau qui va recevoir les envirommenents
		JsonArray arrayEnv = new JsonArray();

        // D�tail de chaque environnement
        for(Entry<String, GestionRT> entry : gestionRT.entrySet())
        {
        	gRT = entry.getValue();
            if (gRT.isPresent() && gRT.isAccepted(ipclient) ) //&& (sbsactifs.contains(gRT.getSubSystem())) ) // Comment� le 09/12/2013 car on a pas le sous-systeme mais la jobq  
            	arrayEnv.add(gRT.getObjetJSON());
        }
        chaine = gson.toJson(arrayEnv);
        
        // Lib�ration de la m�moire
        sbsactifs.clear();
        sbsactifs = null;
        //arrayEnv.clear();
        
        return chaine;
    }

    /**
     * Retourne la liste des fichiers pour une config en particulier sous forme d'une chaine de caract�res
     * @param config
     * @param relecture
     * @return
     */
    public String getListeRT(String config, boolean relecture)
    {
    	if (config == null) return null;
    	return gestionRT.get(config.trim()).getListeRTJSON(relecture);
    }

    /**
     * Retourne une liste avec les noms des environnements (du fichier rt.ini)
     */
    public String[] getNomConfig()
    {
        int i=0;
        String liste[]=null;
        GestionRT gRT=null;
        
        if (gestionRT.size() == 0)
        {
            msgErreur = ERREUR_RENCONTREE;
            return null;
        }
        
        liste = new String[gestionRT.size()];
        //Set cles = gestionRT.keySet();
        Iterator<String> it = gestionRT.keySet().iterator();
        while (it.hasNext())
        {
        	gRT = gestionRT.get(it.next());
        	// Recherche du nom de la config
        	liste[i++] = gRT.getNomConfig();
        }

        return liste;
    }

    /**
     * Retourne la liste des environnements possibles cad la liste des dossiers contenus dans rt
     * @return
     */
    public String[] getFolderExistant()
    {
		String dossierRT = dossierRacine + File.separatorChar + Constantes.DOSSIER_RT;
		return new File(dossierRT).list(new FiltreDossier());
    }
    
    /**
     * Retourne la liste des environnements existants mais n'�tant pas dans le rt.ini
     * Attention un param�tre a �t� ajout� FOLDER, il peut y avoir une ambiguit� avec le nomConfig 
     */
    public ArrayList<String> getNomConfigPossible()
    {
    	int i=0;
    	boolean insere=true;
		ArrayList<String> listeEnvPossible = new ArrayList<String>();
    	String[] listeEnvRT = getNomConfig();
    	String[] listeEnvExistant = getFolderExistant();
    	
		// Pas de d'environnments disponible (dossier rt vide ou inexistant)
		if (listeEnvExistant == null) return null;
		// Pas d'environnment dans le rt.ini donc tous les environnments existants sont possibles
		if (listeEnvRT == null)
		{
			for (i=0; i<listeEnvExistant.length; i++)
				listeEnvPossible.add(listeEnvExistant[i]);
			return listeEnvPossible; 
		}
		
		// Sinon
		for (i=0; i<listeEnvExistant.length; i++)
		{
			insere = true;
			for (int j=0; j<listeEnvRT.length; j++)
				if ( listeEnvExistant[i].equals(listeEnvRT[j]) )
				{
					insere = false;
					break;
				}
			if (insere) listeEnvPossible.add(listeEnvExistant[i]);
		}
		
		return listeEnvPossible;
    }

    /**
     * Pointe sur un environnement en particulier
     * @param config
     * @return
     */
    public GestionRT getGestionRT(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim());
    }

    /**
     * Retourne le pr�fixe de la device d'une Config
     *
    public String getPrefixDevice(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getPrefixDevice();
    }*/

    /**
     * Retourne le nom du Host d'une Config
     *
    public String getHost(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getHost();
    }*/

    /**
     * Retourne le nom de la JobQ d'une Config
     *
    public String getJobQ(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getJobQ();
    }*/
    
    /**
     * Retourne le nom du Program d'une Config
     *
    public String getProgram(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getProgram();
    }*/

    /**
     * Retourne le nom de la Bib d'une Config
     *
    public String getLibrary(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getLibrary();
    }*/

    /**
     * Retourne le nom de la BibEnv d'une Config
     *
    public String getLibraryEnv(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getLibraryEnv();
    }*/

    /**
     * Retourne le nom du dossier d'une Config
     *
    public String getFolder(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getFolder();
    }*/

    /**
     * Retourne la liste des adresses accepted d'une Config
     *
    public String getAccepted(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getchAccepted();
    }*/

    /**
     * Retourne le compteur d'un fichier d'une Config
     */
    public int getCptFic(String config, String nomfic)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return Constantes.ERREUR;
    	return gestionRT.get(config.trim()).getCptFic(nomfic);
    }

    /**
     * Retourne un fichier particulier
     */
    public byte[] getFichier(String config, String nomfic)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getFichier(nomfic);
    }

    /**
     * Retourne si on doit afficher les infos dans la barre de titre d'une Config
     *
    public boolean isTitleBarInformations(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return false;
    	return gestionRT.get(config.trim()).isTitleBarInformations();
    }

    /**
     * Retourne le type de menu d'une Config
     *
    public String getTypemenu(String config)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return null;
    	return gestionRT.get(config.trim()).getTypemenu();
    }
    */
    
    /**
     * Initialise les donn�es du user � partir des donn�es de la config
     * @param config
     * @param infoUser
     */
    public void initInfoUser(String config, EnvUser infoUser)
    {
    	if ((config == null) || (gestionRT.size() == 0)) return;
    	
    	gestionRT.get(config.trim()).initInfoUser(infoUser);
    }
    
    /**
     * Retourne le message d'erreur
     */
    public String getMsgErreur()
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }

    /**
     * Lib�re les ressources
     */
    public void dispose()
    {
    	if( listeConfig != null ){
    		for(Entry<String, HashMap<String, String>> entry : listeConfig.entrySet()) {
    		    if( entry.getValue() != null ){
    		    	entry.getValue().clear();
    		    }
    		}
    		listeConfig.clear();
    		listeConfig = null;
    	}
    	if( gestionRT != null ){
    		for(Entry<String, GestionRT> entry : gestionRT.entrySet()) {
    		    if( entry.getValue() != null ){
    		    	entry.getValue().dispose();
    		    }
    		}
    		gestionRT.clear();
    		gestionRT = null;
    	}
    	parser = null;
    	gson = null;
    }
}