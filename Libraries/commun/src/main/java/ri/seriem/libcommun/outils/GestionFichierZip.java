//=================================================================================================
//==>                                                                       15/11/2010 - 26/02/2016
//==> Gestion des fichiers zip (en chantier)
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import ri.seriem.libcommun.edition.ConstantesNewSim;

public class GestionFichierZip
{
	// Constantes erreurs de chargement
	//private final static String ERREUR_FICHIER_INTROUVABLE = "Le fichier est introuvable.";
	//private final static String ERREUR_LECTURE_FICHIER = "Erreur lors de la lecture du fichier.";
	
	// Variables
	private  FileNG nomFichier = null;		// <-- -STEF- Pourquoi des variables statiques ?
	protected byte[] contenuFichier = null;
	private  ZipOutputStream ficZIP = null;
	private  List<String> filesListInDir = new ArrayList<String>();
	private String msgErreur = ""; // Conserve le dernier message d'erreur �mit
									// et non lu

	/**
	 * Initialise le nom du fichier
	 * 
	 * @param nomFichier
	 */
	public void setNomFichier(String nomFichier)
	{
		if (nomFichier != null) this.nomFichier = new FileNG(nomFichier);
	}

	/**
	 * Retourne le nom du fichier
	 * 
	 * @return
	 */
	public String getNomFichier()
	{
		return nomFichier.getAbsolutePath();
	}

	/**
	 * V�rifie l'existence d'un fichier
	 * 
	 * @return
	 *
	private boolean isPresent()
	{
		return nomFichier.exists();
	}*/

	/**
	 * Cr�� l'archive vide
	 * 
	 * @param taux de compression de 1 � 9
	 * @return
	 */
	public  boolean createZip(int taux, File nomFichier)
	{
		// D�claration de l'archive ZIP
		if ((taux < 0) || (taux > 9)) taux = Deflater.BEST_COMPRESSION;
		try
		{
			ficZIP = new ZipOutputStream(new FileOutputStream(nomFichier));
			// M�thode de compression DEFLATED ou STORED
			ficZIP.setMethod(ZipOutputStream.DEFLATED);
			// Niveau de compression
			// de 1 (NO_COMPRESSION) � 9 (BEST_COMPRESSION)
			ficZIP.setLevel(taux);
			// Fermeture de l'archive et des flux
			ficZIP.flush();
			// ficZIP.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * public static void main(String args[]) throws Exception {
	 * GestionFichierZip gfz = new GestionFichierZip();
	 * gfz.setNomFichier("/home/ritousv/Bureau/newsim_prj_hes/test.zip");
	 * gfz.createZip(-1);
	 * gfz.addFichier("/home/ritousv/Bureau/options-parametres-icone-5311-32.png"
	 * ); }
	 */
	 private  void listFilesDir(File dir) throws IOException {
	        File[] files = dir.listFiles();
	      
	        for(File file : files){
	            if(file.isFile()) filesListInDir.add(file.getAbsolutePath());
	            else listFilesDir(file);
	        }
	    }
	
	//ajoute un repertoire � l'archive zip
	 //dir = nom du dossier qu'il faut zipper
	 //zipDirName = nom du dossier zip
	 public  void zipDirectory(File dir, String zipDirName) 
	 {
		 try {
			 
			 listFilesDir(dir);
			 //now zip files one by one
			 //create ZipOutputStream to write to the zip file
			 FileOutputStream fos = new FileOutputStream(zipDirName);
			 ZipOutputStream zos = new ZipOutputStream(fos);
			 for(String filePath : filesListInDir)
			 {
				 System.out.println("Zipping "+filePath);
				 
				 //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
				 ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
				 zos.putNextEntry(ze);
				
				 //read the file and write to ZipOutputStream
				 FileInputStream fis = new FileInputStream(filePath);
				 
				 byte[] buffer = new byte[1024];
				 int len;
				 while ((len = fis.read(buffer)) > 0) 
				 {
					 zos.write(buffer, 0, len);

				 }
				 zos.closeEntry();
				 fis.close();
			 }
			 zos.close();
			 fos.close();
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	 }
	
	
	/**
	 * Ajout d'un fichier dans l'archive
	 * 
	 * @param entreeFichier
	 * @return
	 *
	public  boolean addFichier(String entreeFichier)
	{
		if (entreeFichier == null) return false;
		File fichier = new File(entreeFichier);
		if (!fichier.exists()) return false;

		try
		{
			ZipEntry entreeZIP = new ZipEntry(fichier.getName());
			ficZIP.putNextEntry(entreeZIP);
			// Envoie du contenu de la premiere entr�e dans l'archive � travers
			// un flux
			DataOutputStream ficDonnees = new DataOutputStream(
					new BufferedOutputStream(ficZIP));
			FileInputStream fin = new FileInputStream(entreeFichier);

			byte[] buffer = new byte[ConstantesNewSim.TAILLE_BUFFER];
			while (true)
			{
				int bytesRead = fin.read(buffer);
				if (bytesRead == -1) break;
				ficDonnees.write(buffer, 0, bytesRead);
			}
			ficDonnees.close();
			fin.close();
			// Fermeture de l'archive jusqu'� la procha�ne entr�e
			// ficZIP.closeEntry();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}*/

	/**
	 * Ajout d'un fichier dans l'archive
	 * 
	 * @param entreeFichier nom du fichier � zipper
	 * @return
	 */
	public  boolean addFichier(String entreeFichier)
	{
		File file = new File(entreeFichier);
		return addFichier(entreeFichier, file.getName());
	}
	
	/**
	 * Ajout d'un fichier dans l'archive
	 * 
	 * @param entreeFichier nom du fichier � zipper
	 * @param nameinthezip  nom du fichier que l'on souhaite dans le zip (cas d'un renommage)
	 * @return
	 */
	public  boolean addFichier(String entreeFichier, String nameinthezip)
	{
		if (entreeFichier == null) return false;
		File fichier = new File(entreeFichier);
		if (!fichier.exists()) return false;

		try
		{
			ZipEntry entreeZIP = new ZipEntry(nameinthezip);
			ficZIP.putNextEntry(entreeZIP);
			// Envoie du contenu de la premiere entr�e dans l'archive � travers
			// un flux
			DataOutputStream ficDonnees = new DataOutputStream(
					new BufferedOutputStream(ficZIP));
			FileInputStream fin = new FileInputStream(entreeFichier);

			byte[] buffer = new byte[ConstantesNewSim.TAILLE_BUFFER];
			while (true)
			{
				int bytesRead = fin.read(buffer);
				if (bytesRead == -1) break;
				ficDonnees.write(buffer, 0, bytesRead);
			}
			ficDonnees.close();
			fin.close();
			// Fermeture de l'archive jusqu'� la procha�ne entr�e
			// ficZIP.closeEntry();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	public void listZip()
	{
		try
		{
			// Open the ZIP file
			ZipFile zf = new ZipFile(nomFichier);
			// Enumerate each entry
			for (Enumeration entries = zf.entries(); entries.hasMoreElements();)
			{
				// Get the entry name
				String zipEntryName = ((ZipEntry) entries.nextElement())
						.getName();
			}
			zf.close();
		}
		catch (Exception e)
		{
		}
	}

	/**
	 * Supprime un fichier dans l'archive
	 * 
	 * @param entreeFichier
	 * @return
	 */
	public boolean removeFichier(String entreeFichier)
	{
		if (entreeFichier == null) return false;
		File fichier = new File(entreeFichier);
		if (!fichier.exists()) return false;

		try
		{
			ZipEntry entreeZIP = new ZipEntry(fichier.getName());
			ficZIP.putNextEntry(entreeZIP);
			// Envoie du contenu de la premiere entr�e dans l'archive � travers
			// un flux
			DataOutputStream ficDonnees = new DataOutputStream(
					new BufferedOutputStream(ficZIP));
			FileInputStream fin = new FileInputStream(entreeFichier);

			byte[] buffer = new byte[ConstantesNewSim.TAILLE_BUFFER];
			while (true)
			{
				int bytesRead = fin.read(buffer);
				if (bytesRead == -1) break;
				ficDonnees.write(buffer, 0, bytesRead);
			}
			ficDonnees.close();
			fin.close();
			// Fermeture de l'archive jusqu'� la procha�ne entr�e
			// ficZIP.closeEntry();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean  addFilesToExistingZip(File zipFile, String filename,
			
			boolean fullPath, String pathFolderInZip) throws IOException
	{ 

		// On cr�e l'objet File repr�sentant le fichier � ins�rer dans le ZIP :
		final File file = new File(pathFolderInZip+File.separator+filename);
		System.out.println("nom du fichier :"+ file);
		
		
		
		// On d�termine le nom de l'entr� ZIP selon le param�tre 'fullPath' :
		//final String entryName = fullPath ? filename : file.getName();
		final String entryName= fullPath ? filename: file.getName() ; 
		
		
		System.out.println("Chemin du fichier :"+ entryName);
		// On cr�e le fichier temporaire de travail (dans le m�me r�pertoire que
		// le ZIP)
		final File tmpFile = File.createTempFile("tmp", ".zip", zipFile.getParentFile());
		
		try
		{
			// On ouvre le fichier temporaire en ecriture :
			final ZipOutputStream output = new ZipOutputStream(new FileOutputStream(tmpFile));
			try
			{
				final byte[] buf = new byte[8192];
				int len;

				// On ouvre le fichier ZIP en lecture :
				final ZipInputStream input = new ZipInputStream(new FileInputStream(zipFile));
				try
				{
					ZipEntry entry;
					// Pour chaque fichier du ZIP :
					while ((entry = input.getNextEntry()) != null)
					{
						// Si le nom est diff�rent de celui du fichier � ajouter
						// :
						//if (!entryName.equals(entry.getName()))
						//{
							
							
							// On recopie le fichier dans le fichier ZIP
							// temporaire :
							// output.putNextEntry(new ZipEntry(entry.getName()));
							output.putNextEntry(new ZipEntry(entry.getName()));
							
							while ((len = input.read(buf)) > 0)
							{
								output.write(buf, 0, len);
							}
						//}
					}
				}
				finally
				{
					input.close();
				}
			
				// Puis on ouvre le fichier pour l'ajouter au ZIP :
				
				final FileInputStream fis = new FileInputStream(file);
				
				
				try
				{
					output.putNextEntry(new ZipEntry(entryName));
					while ((len = fis.read(buf)) > 0)
					{
						output.write(buf, 0, len);
					}
				}
				finally
				{
					fis.close();
				}
			}
			finally
			{
				output.close();
			}

			// Si on arrive ici c'est que tout s'est bien pass�
			// => On supprime le fichier ZIP pour le remplacer par le fichier
			// temporaire :
			if (!(zipFile.delete() && tmpFile.renameTo(zipFile)))
			{
				throw new IOException("Unable to replace " + zipFile);
			}
		}
		finally
		{
			// On force la suppression du fichier temporaire s'il existe encore
			// (en cas d'erreur)
			tmpFile.delete();
		}
		return true;
	}

	
	
	/**
	 * D�compresse le fichier zip dans le r�pertoire donn�
	 * 
	 * @param folder
	 *            le r�pertoire o� les fichiers seront extraits
	 * @param zipfile
	 *            le fichier zip � d�compresser
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static boolean unzip(File zipfile, File folder)
	{
		try
		{

			// cr�ation de la ZipInputStream qui va servir � lire les donn�es du fichier zip
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipfile.getCanonicalFile())));

			// extractions des entr�es du fichiers zip (i.e. le contenu du zip)
			ZipEntry ze = null;
			try
			{
				while ((ze = zis.getNextEntry()) != null)
				{

					// Pour chaque entr�e, on cr�e un fichier
					// dans le r�pertoire de sortie "folder"
					File f = new File(folder.getCanonicalPath(), ze.getName());

					// Si l'entr�e est un r�pertoire,
					// on le cr�e dans le r�pertoire de sortie
					// et on passe � l'entr�e suivante (continue)
					if (ze.isDirectory())
					{
						f.mkdirs();
						continue;
					}

					// L'entr�e est un fichier, on cr�e une OutputStream
					// pour �crire le contenu du nouveau fichier
					f.getParentFile().mkdirs();
					OutputStream fos = new BufferedOutputStream(
							new FileOutputStream(f));

					// On �crit le contenu du nouveau fichier
					// qu'on lit � partir de la ZipInputStream
					// au moyen d'un buffer (byte[])
					try
					{
						try
						{
							final byte[] buf = new byte[8192];
							int bytesRead;
							while (-1 != (bytesRead = zis.read(buf)))
								fos.write(buf, 0, bytesRead);
						}
						finally
						{
							fos.close();
						}
					}
					catch (final IOException ioe)
					{
						// en cas d'erreur on efface le fichier
						f.delete();
						ioe.printStackTrace();
						return false;
					}
				}
			}
			finally
			{
				// fermeture de la ZipInputStream
				zis.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param filePath
     * @throws IOException
     *
    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[4096];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
*/
	/**
	 * Retourne le message d'erreur
	 * 
	 * @return
	 */
	public String getMsgErreur()
	{
		String chaine;

		// La r�cup�ration du message est � usage unique
		chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
