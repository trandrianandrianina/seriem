/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import ri.seriem.libcommun.outils.Constantes;

/**
 * Liste des constantes g�n�riques utilis�es dans le projet OndeEdition.
 */
public class ConstantesNewSim {
  public final static double INCH = 2.54;
  
  public final static float A4_WIDTH = 21;
  public final static float A4_HEIGHT = 29.7f;
  
  // Num�ros des requ�tes
  public final static int CONNEXION_OK = 1;
  public final static int DEMANDE_PROTOCOLE_SERVEUR = 2;
  public final static int RECOIT_PROTOCOLE_SERVEUR = DEMANDE_PROTOCOLE_SERVEUR;
  public final static int DEMANDE_REQUETE = 3;
  public final static int RECOIT_REQUETE = DEMANDE_REQUETE;
  public final static int DEMANDE_REQUETE_VEILLE = 4;
  public final static int RECOIT_REQUETE_VEILLE = DEMANDE_REQUETE_VEILLE;
  public final static int DEMANDE_REQUETE_AS = 808464435;
  
  public final static String STOP = "99999";
  
  public final static String LISTE_SPOOL = "A0000";
  public final static String MODIFIER_SPOOL = "A0002";
  public final static String SUSPENDRE_SPOOL = "A0003";
  public final static String SUPPRIMER_SPOOL = "A0004";
  public final static String AFFICHE_SPOOL = "A0005";
  public final static String LIBERER_SPOOL = "A0006";
  public final static String MESSAGE_SPOOL = "A0007";
  public final static String INFOS_SERVER = "A0009";
  public final static String GENERE_DOC_FROM_SPL = "A0010";
  public final static String IMPRIME_DOC_FROM_SPL = "A0011";
  public final static String MULTI_DOC = "A0012";
  public final static String LISTE_PRINTER = "A0013";
  public final static String LISTE_OUTQ = "A0014";
  public final static String GENERE_DOC_FROM_FIC = "A0020"; // Nouvelle fa�on de faire avec PGVMEFM/PGVMLFM
  public final static String IMPRIME_DOC_FROM_FIC = "A0021"; // Nouvelle fa�on de faire avec PGVMEFM/PGVMLFM
  public final static String DDE_DOC = "A0100";
  public final static String ERREUR_CONNEXION = "A9998";
  public final static String DECONNECTION_SPOOL = "A9999";
  
  public final static int HTTP_GET = 1195725856;
  public final static int HTTP_POST = 1347375956;
  
  // Propres au projet
  public final static String MAILTO = "seriem.toulouse@seriem.com";
  public final static int SOCKET_TIMEOUT = 3600000; // en ms = 1H
  
  public final static String VERSION_PROTOCOLE = "00.01";
  public final static String VERSION_SERVER = VERSION_PROTOCOLE + ".01";
  public final static String VERSION_CLIENT = VERSION_PROTOCOLE + ".01";
  public final static int SERVER_PORT = 5080;
  public final static int TAILLE_BUFFER = 32768;
  
  public final static String DOSSIER_RACINE = Constantes.DOSSIER_RACINE;
  public final static String DOSSIER_LOG = Constantes.DOSSIER_LOG;
  public final static String DOSSIER_TMP = Constantes.DOSSIER_TMP;
  public final static String DOSSIER_SIM = "sim";
  public final static String DOSSIER_CTRL = Constantes.DOSSIER_CTRL;
  public final static String DOSSIER_LIB = Constantes.DOSSIER_LIB;
  
  public final static String FIC_CONFIG = "sim.ini";
  
}
