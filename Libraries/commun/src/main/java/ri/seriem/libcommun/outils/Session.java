//=================================================================================================
//==>                                                                       04/11/2015 - 08/06/2016
//==> Interface pour toutes les sessions
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.nio.ByteBuffer;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class Session
{
	// Constantes
	private	final static		int							POSMAX					= 10;	
	// Variables
	//private						List<ByteBuffer>			listBuffer				= Collections.synchronizedList(new ArrayList<ByteBuffer>()); //<- Provoque un lock de la session lors de la r�cup�ration du jar
	//private						ArrayList<ByteBuffer>		listBuffer				= new ArrayList<ByteBuffer>();
    private						CopyOnWriteArrayList<ByteBuffer> listBuffer			= new CopyOnWriteArrayList<ByteBuffer>();
    private						ByteBuffer[]				tabBuffer				= new ByteBuffer[POSMAX];
	private						int							posTabStk				= 0;		// Indice du stockage du buffer circulaire 
	private						int							posTabTrt				= 0;		// Indice du traitement du buffer circulaire
	protected					long						idSession				= -1;
	protected					boolean						inProgress				= false;
	protected					String						msgErreur				= "";		// Conserve le dernier message d'erreur �mit et non lu


	// -- M�thodes abstraites -------------------------------------------------
	
	public		abstract		void						startConnexion();
	public		abstract		void						treatBuffer(ByteBuffer bbuffer);


	// -- M�thodes priv�es ----------------------------------------------------


	/**
	 * Incr�mente l'indice de stockage du tableau circulaire
	 */
	private void incPosTabStk()
	{
		posTabStk++;
		if( posTabStk == POSMAX ){
			posTabStk = 0;
		}
//System.out.println("-indice->posTabStk: " + posTabStk);		
	}

	/**
	 * Incr�mente l'indice de traitement du tableau circulaire
	 */
	private void incPosTabTrt()
	{
		posTabTrt++;
		if( posTabTrt == POSMAX ){
			posTabTrt = 0;
		}
	}


	// -- M�thodes prot�g�es --------------------------------------------------

	
	/**
	 * Ajoute un message d'erreur de traitement
	 * @param classname
	 * @param methodname
	 * @param text
	 */
	protected void addMsgError(String classname, String methodname, String text)
	{
		if( (methodname == null) || (methodname.trim().length() == 0) ){
			msgErreur += "\n" + classname + '\t' + text;
		} else{
			msgErreur += "\n" + classname + "\t(" + methodname + ")\t" + text;
		}
	}


	// -- M�thodes publiques --------------------------------------------------


	/**
	 * Retourne l'id de la session 
	 */
	public long getIdSession()
	{
		return idSession;
	}

	/**
	 * Ajoute le buffer provenant de la socket
	 * @param bbuffer
	 */
	public void addBufferReceivedold(final ByteBuffer bbuffer)
	{
		listBuffer.add(bbuffer);
		if(	inProgress ) return;

		new Thread(){
				public void run(){
					//listBuffer.add(bbuffer); <-- ici cela provoque des pb de lock sur les menus par exemple
//System.out.println("-addBufferReceived-:" + inProgress);		
					synchronized( listBuffer )
					{
						inProgress = true;
						while( !listBuffer.isEmpty() ){
//System.out.println("-addBufferReceived-size:" + listBuffer.size());		
							treatBuffer(listBuffer.get(0));			// TODO � am�liorer car de temps en temps �a coince (null ou pb de taille du buffer)
							listBuffer.remove(0);
						}
						inProgress = false;
					}
				}
		}.start();
	}

	/**
	 * Ajoute le buffer provenant de la socket
	 * @param bbuffer
	 */
	public void addBufferReceived(final ByteBuffer bbuffer)
	{
		tabBuffer[posTabStk] = bbuffer;
		incPosTabStk(); 
//System.out.println("-Session (addBufferReceived)-> Taille listBuffer :" + listBuffer.size() + " inProgress:" + inProgress);		
		if(	inProgress ) return;

		new Thread(){
				public void run(){
					synchronized( tabBuffer )
					{
						inProgress = true;
						while( posTabStk != posTabTrt ){
							treatBuffer(tabBuffer[posTabTrt]);
							tabBuffer[posTabTrt].clear();
							incPosTabTrt();
						}
						inProgress = false;
					}
				}
		}.start();
	}

	/**
	 * Retourne le message d'erreur 
	 * @return
	 */
	public String getMsgErreur()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		listBuffer.clear();
		listBuffer = null;
		//System.out.println("-dispose->Session");
	}


 	// -- Accesseurs ----------------------------------------------------------


}
