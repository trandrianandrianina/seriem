/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

/**
 * Message d'erreur destin� � l'utilisateur du logiciel.
 * <br>
 * Cette exception permet de g�n�rer un message qui sera affich� � l'utilisateur. Il doit donc �tre r�dig� dans un fran�ais clair et
 * simple.
 * Il faut limiter le jargon technique. Ce message ne peut correspondre qu'� un message d'erreur car le fait de lever une exception
 * interrompt le traitement en cours. Ce m�canisme n'est donc pas adapt� pour des messages d'informations ou d'alertes.
 * <br>
 * Ce message peut-�tre g�n�r� c�t� client ou serveur. C�t� serveur, si c'est dans le cadre d'un appel de m�thode RMI, l'exception sera
 * remont�e au client qui pourra l'afficher � l'utilisateur.
 */
public class MessageErreurException extends RuntimeException {
  private static final String PREFIXE = "[MessageErreurException] ";
  public static final String MESSAGE_ERREUR_TECHNIQUE = "Une erreur technique est survenue. Merci de contacter le service assistance.";
  
  /**
   * Constructeur avec un message utilisateur simple.
   * <br>
   * Ce type de message permet de remonter des erreurs fonctionnels, par exemple "La date du document est sup�rieure � la date du jour".
   * Ce message g�n�re une trace de type "Information".
   */
  public MessageErreurException(String ptexteMessage) {
    super(ptexteMessage);
    Trace.erreur(this, PREFIXE + ptexteMessage);
  }
  
  /**
   * Constructeur avec un message utilisateur accompagn� d'une exception.
   * <br>
   * Ce type de message est adapt� aux erreurs techniques pour lesquelles ont veut afficher un message plus compr�hensible pour
   * l'utilisateur, par exemple "Erreur lors de la lecture des informations du client".
   * <br>
   * Les messages plus techniques contenus dans les exceptions ainsi que la stacktrace seront consultables via l'ic�ne engrenage de la
   * bo�te de message standard. Ces informations g�n�rent une trace de type "Erreur".
   */
  public MessageErreurException(Exception e, String ptexteMessage) {
    super(ptexteMessage, e);
    Trace.erreur(e, PREFIXE + ptexteMessage);
  }
}
