/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.io.File;
import java.io.FileReader;
import java.util.EventListener;

import javax.swing.event.EventListenerList;

/**
 * Permet de passer des ordres � une application sans interface graphique
 */
public class ControlApplication {
  // Constantes
  public final static String EXTENSION = ".ctrl";
  private final static String DOSSIER_CTRL = "ctrl";
  
  // Variables
  private File nomFichier = null;
  private Thread surveille = null;
  private final EventListenerList listeners = new EventListenerList();
  
  /**
   * Constructeur
   * @param rep
   * @param nomappli
   */
  public ControlApplication(String rep, String nomappli, int delai) {
    if (nomappli == null) {
      return;
    }
    setNomFichier(rep, nomappli);
    if (delai <= 0) {
      delai = 10;
    }
    surveilleFichier(delai);
  }
  
  /**
   * Construit le nom du fichier
   * @param nomappli the nomappli to set
   */
  private void setNomFichier(String dossier, String nomappli) {
    if (nomappli == null) {
      return;
    }
    
    if (dossier == null) {
      dossier = getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + File.separatorChar + DOSSIER_CTRL;
    }
    nomFichier = new File(dossier.trim() + File.separatorChar + nomappli.trim().toLowerCase() + EXTENSION);
    if (!nomFichier.getParentFile().exists()) {
      nomFichier.getParentFile().mkdirs();
    }
  }
  
  /**
   * @return the nomFichier
   */
  public File getNomFichier() {
    return nomFichier;
  }
  
  /**
   * Lance une tache de fond qui surveillera les fichiers cr��s
   * @param delai en secondes
   */
  public synchronized void surveilleFichier(final int delai) {
    if (nomFichier == null) {
      return;
    }
    surveille = new Thread() {
      public void run() {
        char[] data = new char[Constantes.TAILLE_BUFFER];
        int lg = -1;
        String message = null;
        while (true) {
          if (nomFichier.exists()) {
            try {
              FileReader fr = new FileReader(nomFichier);
              lg = fr.read(data);
              if (lg != -1) {
                message = new String(data, 0, lg);
              }
              fr.close();
              nomFichier.delete();
              nouveauMessage(message);
              message = null;
            }
            catch (Exception e) {
            }
          }
          else {
            try {
              Thread.sleep(delai * 1000);
            }
            catch (InterruptedException ex) {
              Thread.currentThread().interrupt(); // Tr�s important de r�interrompre
              break;
            }
          }
        }
      }
    };
    surveille.start();
  }
  
  /**
   * Arr�t de la surveillance
   */
  public void arret() {
    if ((surveille != null) && (surveille.isAlive())) {
      surveille.interrupt();
      surveille = null;
    }
  }
  
  public static interface Listener extends EventListener {
    public void nouveauMessage(String msg);
  }
  
  public void addListener(Listener l) {
    listeners.add(Listener.class, l);
  }
  
  public void removeListener(Listener l) {
    listeners.remove(Listener.class, l);
  }
  
  public void nouveauMessage(String msg) {
    if (msg == null) {
      return;
    }
    EventListener[] el = listeners.getListeners(Listener.class);
    for (int i = 0; i < el.length; i++) {
      ((Listener) el[i]).nouveauMessage(msg.trim());
    }
  }
}
