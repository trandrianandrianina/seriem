/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * Informations pour une imprimante connect�e � un System I contenu dans un menuitem.
 */
public class GfxImprimanteMenuitem extends JMenuItem {
  // Variables
  private Imprimante printer = null;
  
  /**
   * Constructeur
   */
  public GfxImprimanteMenuitem(Imprimante aprinter) {
    super();
    setImprimante(aprinter);
    initComponents();
  }
  
  public void setImprimante(Imprimante aprinter) {
    printer = aprinter;
  }
  
  public Imprimante getImprimante() {
    return printer;
  }
  
  public int getNombreExemplaire() {
    return (Integer) exemplaire.getValue();
  }
  
  private void setEtatPrinter() {
    if (printer.getStatus() == Imprimante.STARTED) {
      icone.setIcon(new ImageIcon(getClass().getResource("/images/puce_verte-16.png")));
      icone.setToolTipText("L'imprimante est d�marr�");
    }
    else {
      icone.setIcon(new ImageIcon(getClass().getResource("/images/puce_rouge-16.png")));
      icone.setToolTipText("L'imprimante est arr�t�");
    }
  }
  
  private void initComponents() {
    texte = new JLabel();
    exemplaire = new JSpinner();
    icone = new JLabel();
    
    // ---- this ----
    setName("this");
    setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
    
    // ---- texte ----
    texte.setText(printer.getNom());
    texte.setPreferredSize(new Dimension(115, 16));
    texte.setName("texte");
    
    // ---- exemplaire ----
    exemplaire.setPreferredSize(new Dimension(50, 28));
    exemplaire.setModel(new SpinnerNumberModel(1, 1, null, 1));
    exemplaire.setToolTipText("Nombre d'exemplaires souhait\u00e9s");
    exemplaire.setName("exemplaire");
    
    // ---- icone ----
    icone.setPreferredSize(new Dimension(16, 16));
    setEtatPrinter();
    icone.setName("icone");
    
    setPreferredSize(new Dimension(225, 28));
    setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
    add(texte);
    add(exemplaire);
    add(icone);
    
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel texte;
  private JSpinner exemplaire;
  private JLabel icone;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
