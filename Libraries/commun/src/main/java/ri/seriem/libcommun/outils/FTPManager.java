//=================================================================================================
//==>                                                                       24/11/2014 - 25/11/2014
//==> Gestion du FTP
//==> A faire:
//==> Note: Ne fonctionne pas correctement avec l'AS400 (fichier corrompu dans l'IFS lors d'un DL)
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;


public class FTPManager 
{
	// Variables
	private 	FTPClient 	connexion = null;
	protected	String		msgErreur = "";		// Conserve le dernier message d'erreur �mit et non lu 
	
	
	/**
	 * Constructeur
	 */
	public FTPManager()
	{
		connexion = new FTPClient(); 
	}
	
	// -- M�thodes publiques --------------------------------------------------
	
	/**
	 * Connexion au serveur
	 * @param serveur
	 * @param user
	 * @param password
	 * @return
	 */
	public boolean connexion(String serveur, String user, String password)
	{
		if( !testStringVariable( serveur ) || !testStringVariable( user ) || !testStringVariable( password ) ){
			return false;
		}
		try{
			connexion.connect( serveur );
			connexion.login( user, password );
			
			if( connexion.isConnected() ){
				msgErreur += "\nConnexion FTP �tablie";
				return true;
			} else {
				msgErreur += "\nConnexion FTP non �tablie";
				return false;
			}
		}
		catch(Exception e){
			msgErreur += "\nErreur lors de la connexion : " + e;
			return false;
		}
	}

	/**
	 * Teste l'existance d'un fichier
	 * @param remoteFile
	 * @return
	 */
	public boolean exists(String remoteFile)
	{
		try
		{
			FTPFile file = connexion.mlistFile(remoteFile);
			return file != null;
		}
		catch (IOException e)
		{
			msgErreur += "\n" + e;
		}
		return false;
	}

	/**
	 * Liste le contenu d'un dossier
	 * @param folder
	 * @return
	 */
	public String[] listFolder(String folder)
	{
		try
		{
			return connexion.listNames(folder);
		}
		catch (IOException e)
		{
			msgErreur += "\n" + e;
		}
		return null;
	}

	/**
	 * T�l�charge un fichier 
	 * @param remoteFile
	 * @param localFile
	 * @return
	 */
	public boolean downloadFile(String remoteFile, String localFile)
	{
		boolean ret=false;
		try
		{
			OutputStream output = new FileOutputStream(localFile);
	        ret = connexion.retrieveFile(remoteFile, output);
		}
		catch (Exception e)
		{
			msgErreur += "\n" + e;
		}
		return ret;
	}

	/**
	 * T�l�verse un fichier
	 * @param localFile
	 * @param remoteFile
	 * @return
	 */
	public boolean uploadFile(String localFile, String remoteFile)
	{
		boolean ret=false;
		try
		{
			InputStream input = new FileInputStream(localFile);
			ret = connexion.storeFile(remoteFile, input);
	        input.close();
		}
		catch (Exception e)
		{
			msgErreur += "\n" + e;
		}
		return ret;
	}
	
	/**
	 * D�connexion
	 */
	public void disconnect()
	{
		try
		{
			connexion.logout();
			connexion.disconnect();
		}
		catch(Exception e)
		{
			msgErreur += "\nErreur d�connexion: " + e;
		}
	}

	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Teste la validit� des variables de type String
	 * @param varname
	 * @param varvalue
	 * @return
	 */
	private boolean testStringVariable( String varvalue )
	{
		if( ( varvalue == null ) || ( varvalue.trim().equals( "" ) ) ){
			msgErreur += "\nUne ou plusieurs variables incorrectes.";
			return false;
		}
		return true;	
	}

	// -- Accesseurs ----------------------------------------------------------
	
	public FTPClient getConnexion()
	{
		return connexion;
	}
	
	/**
	 * Retourne le message d'erreur
	 * @return
	 */
	public String getMsgError()
	{
		// La r�cup�ration du message est � usage unique
		final String chaine = msgErreur;
		msgErreur = "";

		return chaine;
	}

}
