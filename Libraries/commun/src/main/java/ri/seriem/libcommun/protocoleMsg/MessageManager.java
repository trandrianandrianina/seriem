//=================================================================================================
//==>                                                                       10/04/2014 - 29/01/2015
//==> Description du message complet
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.protocoleMsg;

import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.libcommun.outils.Constantes;

public class MessageManager
{
	private MessageHead head=null;
	private Object body=null;
	private Class<?> classbody=null;
	
	private transient static int id = 0;
	private transient int hashcodecaller = 0;
	private transient Gson gson = new Gson();
	private transient JsonParser parser = new JsonParser();
	private transient StringBuilder sb = new StringBuilder();

	/**
	 * Constructeur
	 * @param hashcodecaller
	 */
	public MessageManager(int ahashcodecaller)
	{
		hashcodecaller = ahashcodecaller;
	}
	
	/**
	 * G�n�re un message complet pour un message de type String
	 * @param idSession
	 * @param simpleString
	 * @param message
	 */
	public int createMessage(boolean simpleString, String message)
	{
		if (head == null){
			head = new MessageHead();
			head.setId(id++);
		}
		if (hashcodecaller != 0)
			head.setHashcodeCaller(hashcodecaller);
		if ((message == null) || (message.length() == 0))
		{
			head.setBodycode(MessageHead.BODY_NO);
			head.setBodydesc(null);
			head.setBodylength(0);
			head.setBodycomp(false);
		}
		else
		{
			if (simpleString)
				head.setBodycode(MessageHead.BODY_SIMPLESTRING);
			else
				head.setBodycode(MessageHead.BODY_COMPLEXSTRING);
			head.setBodylength(message.length());
			head.setBodydesc(message);
			if (head.getBodylength() > Constantes.TAILLE_BUFFER)
				head.setBodycomp(true);
			else
				head.setBodycomp(false);
		}
		body = null;
		return head.getId();
	}
	
	/**
	 * G�n�re un message complet pour un message de type String
	 * @param idSession
	 * @param compress
	 * @param simpleString
	 * @param message
	 */
	public int createMessage(boolean compress, boolean simpleString, String message)
	{
		createMessage(simpleString, message);
		head.setBodycomp(compress);
		return head.getId();
	}

	/**
	 * G�n�re un message complet pour un message de type JSON
	 * @param idSession
	 * @param message
	 */
	public int createMessage(Object message)
	{
		if (head == null){
			head = new MessageHead();
			head.setId(id++);
		}
		if (hashcodecaller != 0)
			head.setHashcodeCaller(hashcodecaller);
		head.setBodycode(MessageHead.BODY_JSON);
		if (message == null) return Constantes.ERREUR;
		head.setBodydesc(message.getClass().getName());
		body = message;
		
		return head.getId();
	}

	/**
	 * G�n�re un message complet pour un message de type JSON
	 * @param idSession
	 * @param compress
	 * @param message
	 */
	public int createMessage(boolean compress, Object message)
	{
		int idmsg = createMessage(message);
		head.setBodycomp(compress);
		
		return idmsg;
	}

	/**
	 * Retourne une chaine contenant le message sous forme d'objet JSON
	 * Erreur de type � comprendre pourquoi plus tard
	 * @return
	 *
	public String getMessageToSend()
	{
		// C'est ici que l'on pr�pare le message � envoyer
		// Notamment si l'option compress a �t� activ�
		if (head.isBodycomp())
		{
			if (head.getBodycode() == MessageHead.BODY_JSON)
			{
				sb.setLength(0);
				String chaine = gson.toJson(body);
				head.setBodylength(chaine.length());
				sb.append("{\"head\":").append(gson.toJson(head)).append(",\"body\":\"").append(encode(chaine)).append("\"}");
				return sb.toString();
			}
			else
				if (head.getBodycode() == MessageHead.BODY_NO)
					return gson.toJson(this);
				else
				{
					head.setBodydesc(encode(head.getBodydesc()));
					return gson.toJson(this);
				}
		}
		else // Pas de compression active
		{
			return gson.toJson(this);
		}
	}*/

	/**
	 * Retourne une chaine contenant le message sous forme d'objet JSON
	 * @return
	 */
	public String getMessageToSend()
	{
		sb.setLength(0);
		sb.append("{\"head\":");
		// C'est ici que l'on pr�pare le message � envoyer
		// Notamment si l'option compress a �t� activ�
		if (head.isBodycomp())
		{
			if (head.getBodycode() == MessageHead.BODY_JSON)
			{
				String chaine = gson.toJson(body);
				head.setBodylength(chaine.length());
				sb.append(gson.toJson(head)).append(",\"body\":\"").append(encode(chaine)).append('\"');
			}
			else
				if (head.getBodycode() == MessageHead.BODY_NO)
					sb.append(gson.toJson(head));
				else
				{
					head.setBodydesc(encode(head.getBodydesc()));
					sb.append(gson.toJson(head));
				}
		}
		else // Pas de compression active
		{
			if (head.getBodycode() == MessageHead.BODY_JSON)
			{
				String chaine = gson.toJson(body);
				head.setBodylength(chaine.length());
				sb.append(gson.toJson(head)).append(",\"body\":").append(chaine);
			}
			else
				sb.append(gson.toJson(head));
		}
		
		return sb.append('}').toString();
	}

	/**
	 * Retourne le message g�n�r� � partir de l'objet JSON
	 * @param amsg
	 * @param hashcodecaller, s'il est �gal � z�ro alors on en tient pas compte dans le traitement  
	 */
	public boolean getMessageReceived(String amsg, int hashcodecaller)
	{
		if (amsg == null) return false;
		
		boolean ret=true;
		JsonObject obj = (JsonObject) parser.parse(amsg);
		
		// On d�s�rialise l'ent�te
		head = gson.fromJson(obj.get("head"), MessageHead.class);
		
		// On v�rifie que ce message est bien destin� � la bonne classe
		if( (hashcodecaller != 0) && (head.getHashcodeCaller() != hashcodecaller) )
			return false;
		
		// On d�s�rialise le corps du message en fonction de la version de son protocole
		switch(head.getVersion())
		{
			case 1 :
				ret = treatmentVersion1(obj);
				break;
		}
		return ret;
	}
	
	// --> M�thodes priv�es <--------------------------------------------------

	/**
	 * Traitement des messages avec le protocole de la version 1
	 * @param obj
	 * @return
	 */
	private boolean treatmentVersion1(JsonObject obj)
	{
		String chaine=null;
		
		// On v�rifie s'il y a un objet JSON � d�s�rialiser
		if (head.getBodycode() == MessageHead.BODY_JSON)
		{
			try
			{
				classbody = Class.forName(head.getBodydesc());
				//classbody = MessageManager.class.getClassLoader().loadClass(head.getBodydesc());
				if (head.isBodycomp())
				{
					chaine = gson.fromJson(obj.get("body"), String.class);
					chaine = decode(chaine, head.getBodylength());
					body = gson.fromJson(chaine, classbody);
				}
				else {
//System.out.println("-treatmentVersion1->classbody:" + classbody);				
//System.out.println("-treatmentVersion1->obj:" + obj);
//System.out.println("-treatmentVersion1->body:" + obj.get("body"));
//System.out.println("-treatmentVersion1->gson:" + gson);
					body = gson.fromJson(obj.get("body"), classbody);
				}
			}
			catch (JsonSyntaxException e)
			{
				System.out.println("-treatmentVersion1->exception1:" + e);
				return false;
			}
			catch (ClassNotFoundException e)
			{
				System.out.println("-treatmentVersion1->exception2:" + e);
				return false;
			}
			catch (Exception e)
			{
				System.out.println("-treatmentVersion1->exception3:" + e);
				e.printStackTrace();
				return false;
			}
		}
		else // Cinon c'est juste une chaine de caract�res
		{
			if (head.isBodycomp())
			{
				chaine = gson.fromJson(head.getBodydesc(), String.class);
				head.setBodydesc(decode(chaine, head.getBodylength()));
			}
			body = null;
			classbody = null;
		}
		return true;
	}
	
	/**
	 * Compresse la chaine avec zip puis l'encode en base64
	 * @param chaine
	 * @return
	 */
	private String encode(String chaine)
	{
		if (chaine == null) return "";
//System.out.println("--> ori " + chaine.length() + " " + chaine);		
		Deflater compresser = new Deflater();
		byte[] output = new byte[chaine.length()*2]; // Car lors de la compression parfois la taille peut etre plus grande que l'originale

		compresser.setInput(chaine.getBytes());
		compresser.finish();
		int compressedDataLength = compresser.deflate(output);
//System.out.println("--> zip " + compressedDataLength);		
		return Base64Coder.encodeString(output, compressedDataLength);
	}
	
	/**
	 * D�compresse la chaine afin de retrouver la valeur originale 
	 * @param chaine
	 * @return
	 */
	private String decode(String chaine, int tailleori)
	{
		if ((chaine == null) || (chaine.equals(""))) return "";
//System.out.println("--> ori " + chaine.length());		
		
		byte[] input = Base64Coder.decode(chaine);
//System.out.println("--> de-b64 " + input.length);		
		byte[] output = new byte[tailleori];
		Inflater decompresser = new Inflater();
		decompresser.setInput(input, 0, input.length);
		int compressedDataLength = 0;
		try
		{
			compressedDataLength = decompresser.inflate(output);
//System.out.println("--> compressedDataLength " + compressedDataLength + " / " + output.length);			
		}
		catch (DataFormatException e) {}
		decompresser.end();
		return new String(output, 0, compressedDataLength);
	}

	//--> Accesseurs <---------------------------------------------------------
	
	/**
	 * @return le head
	 */
	public MessageHead getHead()
	{
		return head;
	}
	/**
	 * @param head le head � d�finir
	 */
	public void setHead(MessageHead head)
	{
		this.head = head;
	}
	/**
	 * @return le body
	 */
	public Object getBody()
	{
		return body;
	}
	/**
	 * @param body le body � d�finir
	 */
	public void setBody(Object body)
	{
		this.body = body;
	}

	/**
	 * @return le classbody
	 */
	public Class<?> getClassbody()
	{
		return classbody;
	}

	/**
	 * @param classbody le classbody � d�finir
	 */
	public void setClassbody(Class<?> classbody)
	{
		this.classbody = classbody;
	}

	/**
	 * Lib�re la m�moire
	 */
	public void dispose()
	{
		head = null;
		body=null;
		classbody=null;
		gson = null;
		parser = null;
	}
}
