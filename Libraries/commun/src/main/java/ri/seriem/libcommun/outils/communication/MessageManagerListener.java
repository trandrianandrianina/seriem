//=================================================================================================
//==>                                                                       29/01/2015 - 29/01/2015
//==> Gestion d'�v�nement sur une String
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils.communication;

import java.util.EventListener;

import javax.swing.event.EventListenerList;

import ri.seriem.libcommun.protocoleMsg.MessageManager;

public class MessageManagerListener
{
    // Variables
    private final EventListenerList listeners=new EventListenerList();
    private MessageManager message = new MessageManager(0);


    public static interface alListener extends EventListener
    {
    	public int getHashCodeCaller();
        public void onMessageReceived(final MessageManager msg);
    }
    
    public void addListener(alListener l)
    {
        listeners.add(alListener.class, l);
    }
    
    public void removeListener(alListener l)
    {
        listeners.remove(alListener.class, l);
    }

    /**
     * Traitement lors de la r�ception d'un message
     * @param val
     */
    public void messageReceived(String val)
    {
    	/*
    	final MessageManager newmessage = message.getTransformStringToMessage(val);
    	final EventListener[] el = listeners.getListeners(alListener.class);
        for(final int i=0; i<el.length; i++) {
        	if( newmessage.getHead().getHashcodeCaller() == ((alListener) el[i]).getHashCodeCaller()  )
        		new Thread()
				{
					public void run()
					{
						((alListener) el[i]).onMessageReceived(newmessage);
					}
				}.start();
        }
        */
    }

    /**
     * Retourne le nombre de listener actifs 
     * @return
     */
    public int getNumberListeners()
    {
    	return listeners.getListeners(alListener.class).length;
    }
}
