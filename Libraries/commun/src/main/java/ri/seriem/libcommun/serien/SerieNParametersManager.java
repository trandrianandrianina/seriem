//=================================================================================================
//==>                                                                       25/11/2013 - 22/04/2014
//==> Gestion des param�tres pour S�rie N
//==> A faire:
//==> A savoir:
//==> 	Il peut arriver que "user.home" ne retourne pas le bon dossier (vu sur des TSE)
//==>	Le fichier SerieN.ini va �tre rechercher dans le dossier d'execution de SerieN.jar
//=================================================================================================
package ri.seriem.libcommun.serien;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.GestionFichierINI;
import ri.seriem.libcommun.outils.GestionFichierTexte;


public class SerieNParametersManager
{
	// Constantes
	private static final String SERIEN_INI = "SerieN.ini";

	// Variables
	private String rootfolder = null;
	private ArrayList<SerieNParameters> listParameters = new ArrayList<SerieNParameters>();
	private File serien_ini = null;

	private boolean isWindows      = false;
	private boolean isMacOsX       = false;
	//private boolean isUnix         = false;


    private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

    /**
     * Constructeur
     * @throws AWTException 
     */
    public SerieNParametersManager()
    {
    	this(Constantes.getUserHome() + File.separatorChar + Constantes.DOSSIER_RACINE);
    }

    /**
     * Constructeur
     * @param arootfolder
     */
    public SerieNParametersManager(String arootfolder)
    {
    	// On effectue un cont�le du dossier racine de l'application (car parfois la valeur retourn�e par Java est fausse cas TSE)
    	rootfolder = arootfolder; 
    	serien_ini = new File(rootfolder + File.separatorChar + Constantes.DOSSIER_WEB + File.separator + SERIEN_INI);
    	whichSystem();
    }
    
    /**
     * Retourne un param�tre de la liste non encore test� ou ok dans le but d'une connexion automatique
     * @return
     */
    public SerieNParameters next()
    {
    	for (int i=0; i<listParameters.size(); i++)
    		if ((listParameters.get(i).getStatus() != SerieNParameters.NO_OK))
    			return listParameters.get(i);
    	return null;
    }
    
	/**
	 * Retourne le choix des param�tres pour l'ex�cution
	 * @return
	 */
	public SerieNParameters getParameters4Execution()
	{
		return getParameters4Execution(null);
	}

	/**
	 * Retourne le choix des param�tres pour l'ex�cution
	 * @param sparameters
	 * @return
	 */
	public SerieNParameters getParameters4Execution(String sparameters)
	{
		// Si on d�tecte la touche CapsLock allum�e alors on force l'affichage de la fen�tre de s�lection des param�tres
		if (!isMacOsX && Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_SCROLL_LOCK))
		{
			loadIniFile();
			return displayChoiceParametersWindow(null);
		}
			
		// Analyse des param�tres potentiels
		if (!analyze(sparameters)) return null;
		
		// R�cup�ration des param�tres qui nous int�ressent
		if (listParameters.size() == 0)
			return null;
		if (listParameters.size() == 1)
			return listParameters.get(0);
		
		// On balaye la liste possible afin voir si l'un est marqu� comme par d�faut
		for (int i=0; i<listParameters.size(); i++)
			if (listParameters.get(i).isDefault_param())
				return listParameters.get(i);
		
		// Il n'y a pas de choix par d�faut on affiche la fen�tre afin que l'utilisateur fasse son choix
		return displayChoiceParametersWindow(null);
	}
	
	/**
	 * Retourne le message d'erreur
	 * @param html
	 * @return
	 */
    public String getMsgErreur(boolean html)
    {
        String chaine;

        // La r�cup�ration du message est � usage unique (TODO encoder les caract�res accentu�s)
        if (html)
        	chaine = "<html>" + msgErreur.replaceAll("\n", "<br>") + "</html>";
        else
        	chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }
	
	// -- M�thodes priv�es ----------------------------------------------------
	
	/**
	 * Analyse et recherche les param�tres pour S�rie N
	 * @param parameters
	 * @return
	 */
	private boolean analyze(String parameters)
	{
		boolean retour=false;
		listParameters.clear();

		// Il n'y a pas de param�tres pass�s en ligne de commande 
		if ((parameters == null) || (parameters.trim().equals("")))
		{
			// On a un fichier ini
			if (serien_ini.exists())
			{
				retour = loadIniFile();
				if (retour) return true;
			}	
			// Pas de fichier ini ou pas correct
			if (displayParametersWindow())
				return writeIniFile();
			else
				return false;
		} 
		else // On a des param�tres en ligne de commande
		{
			listParameters.add(new SerieNParameters(rootfolder));
			retour = listParameters.get(0).setStringParameters(parameters);
			if (!retour)
				msgErreur += listParameters.get(0).getMsgErreur(true);
			return retour;
		}
	}

    /**
     * Charge le fichier SerieN.ini (� partir de DemarrageSerieN)
     * @return
     */
    private boolean loadIniFile()
    {
    	if (!serien_ini.exists()) return false;
    	
    	boolean retour=false;
    	
    	// Lecture du fichier
		GestionFichierINI gfini = new GestionFichierINI(serien_ini.getAbsolutePath());
		LinkedHashMap<String, HashMap<String, String>> sections = gfini.getSections();
		if ((sections == null) || (sections.size() == 0)) return false;

		// Chargement des sections dans la liste des param�tres
		listParameters.clear();
		for(Entry<String, HashMap<String, String>> entry : sections.entrySet())
		{
			SerieNParameters parameters = new SerieNParameters(rootfolder);
			// Les param�tres pour S�rie N
			retour = parameters.setStringParameters(entry.getValue().get("parameters"));
			if (retour)
			{
				parameters.setName(Constantes.code2unicode(entry.getKey()));
				String valeur = entry.getValue().get("default");
				if (valeur != null)
					parameters.setDefault_param(valeur.trim().toLowerCase().equals("true"));
				// Les param�tres pour la JVM
				valeur = entry.getValue().get("jvmparameters");
				if (valeur != null)
					parameters.setJvmParam(valeur); 
				listParameters.add(parameters);
			}
			else
				msgErreur += "Erreur lors de l'analyse des param�tres de la section " + entry.getKey() + '\n';
		}
		return true;
    }
    
    /**
     * Ecriture du fichier ini
     * @return
     */
    private boolean writeIniFile()
    {
    	if (listParameters.size() == 0)
    	{
    		msgErreur += "Il n'y a pas de param�tres � �crire dans le fichier " + serien_ini.getName() + '\n';
    		return false;
    	}
    	
		// Pr�paration des donn�es � �crire
    	ArrayList<String> data = new ArrayList<String>();
    	for (int i=0; i<listParameters.size(); i++)
    	{
    		SerieNParameters parameters = listParameters.get(i);
    		data.add(Constantes.lettreori2code("[" + parameters.getName()+ "]"));
    		if (parameters.isDefault_param())
    				data.add("default=true");
    		data.add("parameters=" + parameters.getStringParameters());
    		data.add("");
    	}
    	
		// Ecriture du fichier 
		GestionFichierTexte gfini = new GestionFichierTexte(serien_ini.getAbsoluteFile());
		gfini.setContenuFichier(data);
		gfini.ecritureFichier();
		data.clear();

    	return true;
    }
    
    /**
     * Affiche une fen�tre de saisie des param�tres lors du premier lancement
     * @return
     */
    private boolean displayParametersWindow()
    {
    	SerieNParameters parameters = new SerieNParameters(rootfolder);
    	SerieNParametersWindow launchParamWindow = new SerieNParametersWindow();
		launchParamWindow.setParameters(parameters);
    	launchParamWindow.setHideOptions(true);
		launchParamWindow.setVisible(true);
		
		boolean retour = parameters.isValid();
		if (!retour)
		{
    		msgErreur = "Aucuns param�tres n'a �t� saisi";
		}
		else
    		listParameters.add(parameters);
		launchParamWindow.dispose();
		
		return retour;
    }
    
    /**
     * Affiche la fen�tre de s�lection des param�tres
     * @return
     */
    public SerieNParameters displayChoiceParametersWindow(String msg)
    {
    	SerieNSelectParametersWindow displaySelection = new SerieNSelectParametersWindow(listParameters);
    	displaySelection.setErrorMsg(msg);
    	displaySelection.setVisible(true);
    	
    	SerieNParameters parameters=null;
    	switch (displaySelection.getCodeOperation())
		{
			case SerieNSelectParametersWindow.SAVE :
				writeIniFile();
				break;

			case SerieNSelectParametersWindow.CONNECT :
				parameters = displaySelection.getParametersSelected();
//System.out.println("--> " + parameters.getHost() + " " + parameters.getPort());				
				break;
			
			case SerieNSelectParametersWindow.SAVE_CONNECT :
				writeIniFile();
				parameters = displaySelection.getParametersSelected();
				break;

			default:
				break;
		}
    	if (parameters != null) parameters.setStatus(SerieNParameters.NO_TESTED);
    	return parameters;
    }

    /**
     * Retourne le dossier racine ou dossier complet dans lequel s'ex�cute la classe 
     * @return
     *
    private String getExecutionFolder(boolean justroot)
    {
		// Contr�le du nom de ce fichier jar en cours d'ex�cution
		String folder = getClass().getResource('/' + getClass().getName().replace('.', '/') + ".class").toString();
//System.out.println(folder);		
		// Calcule la position de d�but
		int deb = folder.indexOf("file:/") + "file:/".length();
		if (File.separatorChar == '/') deb--;
		// On v�rifie si c'est un jar
		int fin = folder.lastIndexOf(".jar");
		if (fin == -1) // Ce n'est pas un jar, c'est surement eclipse alors on force la lecture du dossier normal (home du user) 
			//fin = folder.lastIndexOf('/'); // Dans le cas de fichier class
			folder = SerieNParameters.DOSSIERRACINE + File.separatorChar + Constantes.DOSSIER_WEB;
		else
		{
			fin = folder.lastIndexOf('/', fin);
			folder = folder.substring(deb, fin).replaceAll("%20", " ").replaceAll("/", "\\"+File.separator);
		}

		if (justroot)
		{
			fin = folder.lastIndexOf(Constantes.DOSSIER_WEB);
			if (fin != -1)
				return folder.substring(0, --fin);
		}
		
		return folder;
    }*/
    
	/**
	 * Identifie le syst�me d'exploitation du poste
	 */
	private void whichSystem()
	{
		final String OS = System.getProperty("os.name").toLowerCase();
		
		// Windows
		isWindows = (OS.indexOf("win") >= 0);
		
		// Mac
		isMacOsX = (OS.indexOf("mac") >= 0);
		
		// Unix
		//isUnix = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}

	

}
