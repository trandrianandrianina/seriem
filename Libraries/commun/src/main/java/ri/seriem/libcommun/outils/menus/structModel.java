package ri.seriem.libcommun.outils.menus;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class structModel
{
	// Variables
	private						ArrayList<baseModel>		listBaseModel			= new ArrayList<baseModel>();
	private						String						date					= null;
	private						String						time					= null;
	private						String						profil					= null;
	private						int							count					= 0;
	
	
	public ArrayList<baseModel> getBaseModel()
	{
		return listBaseModel;
	}

	/***
	 * Ajoute un mod�le de base
	 * @param lib
	 * @param file
	 */
	public void addBaseModel(String lib, String fileem, String filepm){
		baseModel bm = new baseModel();
		bm.setLibrary(lib);
		bm.setFileem(fileem);
		bm.setFilepm(filepm);
		listBaseModel.add(bm);
	}

	/**
	 * Ajoute des donn�es dans le dernier mod�le de base de la liste
	 * @param ssm
	 * @param alistOrd
	 */
	public void putIntoLastBaseModel(String ssm, ArrayList<String> alistOrd){
		if( listBaseModel.isEmpty() ) return;
		
		LinkedHashMap<String, ArrayList<String>> listMenu4Model = listBaseModel.get(listBaseModel.size()-1).getListMenu4Model();
		listMenu4Model.put(ssm, alistOrd);
	}

	/**
	 * Contr�le qu'un module donn� soit bien dans la liste
	 * @param wgrp
	 * @return
	 */
	public ArrayList<String> checkSsModule(String lib, String ord){
		if( listBaseModel.isEmpty() ) return null;

		for( baseModel bm : listBaseModel )
			if( bm.getLibrary().equalsIgnoreCase(lib))
				return bm.checkSsModule(ord);
		return null;
	}

	public ArrayList<String> getListOrd(String lib, String ssm)
	{
		if( listBaseModel.isEmpty() ) return null;

		for( baseModel bm : listBaseModel )
			if( bm.getLibrary().equalsIgnoreCase(lib))
				return bm.getListOrd(ssm);
		return null;
	}

	public String getFileEm(String lib)
	{
		if( listBaseModel.isEmpty() ) return null;

		for( baseModel bm : listBaseModel )
			if( bm.getLibrary().equalsIgnoreCase(lib))
				return bm.getFileem();
		return null;
	}

	public String getFilePm(String lib)
	{
		if( listBaseModel.isEmpty() ) return null;

		for( baseModel bm : listBaseModel )
			if( bm.getLibrary().equalsIgnoreCase(lib))
				return bm.getFilepm();
		return null;
	}

	public boolean isBaseModelExists(String lib)
	{
		if( listBaseModel.isEmpty() ) return false;

		for( baseModel bm : listBaseModel )
			if( bm.getLibrary().equalsIgnoreCase(lib))
				return true;
		return false;
	}

	/**
	 * Retourne si on a stock� un mod�le 
	 * @return
	 */
	public boolean isEmpty()
	{
		return listBaseModel.isEmpty();
	}


	/**
	 * Incr�mente le compteur
	 */
	public void incCount()
	{
		count++;
	}
	
	// -- Accesseurs ----------------------------------------------------------
	
	
	/**
	 * @return le date
	 */
	public String getDate()
	{
		return date;
	}


	/**
	 * @param date le date � d�finir
	 */
	public void setDate(String date)
	{
		this.date = date;
	}


	/**
	 * @return le time
	 */
	public String getTime()
	{
		return time;
	}


	/**
	 * @param time le time � d�finir
	 */
	public void setTime(String time)
	{
		this.time = time;
	}


	/**
	 * @return le profil
	 */
	public String getProfil()
	{
		return profil;
	}


	/**
	 * @param profil le profil � d�finir
	 */
	public void setProfil(String profil)
	{
		this.profil = profil;
	}


	/**
	 * @return le count
	 */
	public int getCount()
	{
		return count;
	}


	/**
	 * @param count le count � d�finir
	 */
	public void setCount(int count)
	{
		this.count = count;
	}


	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		for( baseModel bm : listBaseModel ){
			bm.dispose();
		}
		listBaseModel.clear();
	}
}
