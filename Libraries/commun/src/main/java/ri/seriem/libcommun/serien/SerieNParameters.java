//=================================================================================================
//==>                                                                       26/11/2013 - 03/12/2013
//==> Ensemble des param�tres pour Serie N
//==> A faire:
//==> A savoir:
//==>	Le fichier SerieN.ini va �tre rechercher dans le dossier d'ex�cution de SerieN.jar
//=================================================================================================
package ri.seriem.libcommun.serien;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ri.seriem.libcommun.outils.Constantes;

public class SerieNParameters
{
	// Constantes
	public static final String IP_ADDRESS_PATTERN = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}";
	
	public static final int NO_OK=-1;
	public static final int NO_TESTED=0;
	public static final int OK=1;
	
	public static final String DOSSIERRACINE=Constantes.getUserHome() + File.separator + Constantes.DOSSIER_RACINE;

	private static final int DELAILOGS=10; // En jours

	// Variables
	private String name="R�seau local";			// Nom permettant d'identifier les param�tres
	private boolean default_param=false;	// Indique si ce sont les param�tres par d�faut (lacement automatique)
	
	private String host=null;
	private int port=Constantes.SERVER_PORT;
	private String dossierRacine=DOSSIERRACINE;
	private boolean debug=false;
	private int delaiLogs=DELAILOGS;
	private int status=NO_TESTED;
	private String jvmParam="-jar";
	
    private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu

    /**
     * Constructeur
     * @param rootfolder, dossier root par d�faut mais pass� en param�tres car parfois user.home ne ram�ne pas le bon
     */
    public SerieNParameters(String rootfolder)
	{
   		setDossierRacine(rootfolder);
	}
    
	/**
	 * Contr�le la validit� des param�tres 
	 * @return
	 */
	public boolean isValid()
	{
		if ((name == null) || (name.trim().equals("")))
		{
			msgErreur += "Erreur le nom est incorrect " + name +'\n';
			return false;
		}
		if ((host == null) || (host.trim().equals("")))
		{
			msgErreur += "Erreur l'adresse IP est nulle ou vide\n";
			return false;
		}
		else
		{
			Matcher matcher = Pattern.compile(IP_ADDRESS_PATTERN).matcher(host);
			if (!matcher.find())
			{
				msgErreur += "Erreur l'adresse IP est invalide " + host +'\n';
				return false;
			}
		}
		if ((port <= 0) || (port > 65536))
		{
			msgErreur += "Erreur le port est incorrect " + port +'\n';
			return false;
		}
		if ((dossierRacine == null) || (dossierRacine.trim().equals("")))
		{
			msgErreur += "Erreur le dossier racine est incorrect " + dossierRacine +'\n';
			return false;
		}
		if ((delaiLogs <= 0) || (delaiLogs > 365))
		{
			msgErreur += "Erreur le delai de conservation des logs est incorrect " + delaiLogs +'\n';
			return false;
		}
		
		return true;
	}
	
	/**
	 * R�cup�re les param�tres en analysant la chaine
	 * @param parameters
	 */
	public boolean setStringParameters(String parameters)
	{
		if (parameters == null) return false;

		String listeparametres[]=null;
		StringBuffer param=new StringBuffer(parameters.trim());
		String chaine=null;

		// On arrange la chaine pour r�cup�rer les param�tres correctement
		int pos=param.indexOf(" ");
		while(pos != -1)
		{
			if (param.charAt(pos+1) != '-')
				param.replace(pos, pos+1, "%20");
			pos=param.indexOf(" ", pos+1);
		}	

		// On d�coupe la chaine et on stocke les diff�rents param�tres dans un tableau
		listeparametres = Constantes.splitString(param.toString(), ' ');

		// On parcourt la liste afin d'en extraire les infos
		for (int i=0; i<listeparametres.length; i++)
		{
			chaine = listeparametres[i].replaceAll("%20", " ").trim();

			// On recherche l'adresse IP
			if (chaine.toLowerCase().startsWith("-ip="))
				host = chaine.substring(4);
			else
				// On recherche le port 
				if (chaine.toLowerCase().startsWith("-p="))
					port = Integer.parseInt(chaine.substring(3));
				else
					// On recherche le dossier utilisateur
					if (chaine.toLowerCase().startsWith("-dir="))
						dossierRacine = chaine.substring(5);
					else
						// On recherche le delai de conservation des logs (en jours) celui du jour inclu
						if (chaine.toLowerCase().startsWith("-kplogs="))
							delaiLogs = Integer.parseInt(chaine.substring(8));
						else
							// On recherche si on veut d�bogguer ou pas
							if (chaine.toLowerCase().startsWith("-d"))
								debug = true;
		}
		
		// On v�rifie que les param�tres obligatoires soient bien renseign�s
		/* Remplacer par la m�thode isValid()
		if ( (serveurSerieN == null) || (serveurSerieN.trim().equals("")) )
		{
			msgErreur = IP_MANQUANTE;
			return false;
		}*/
		
		return isValid();
	}
	
	/**
	 * Retourne une chaine avec les param�tres
	 * @return
	 */
	public String getStringParameters()
	{
		if (!isValid()) return null;

		StringBuffer parameters = new StringBuffer();
		parameters.append("-ip=").append(host).append(' ');
		if (port != Constantes.SERVER_PORT)
			parameters.append("-p=").append(port).append(' ');
		if (!dossierRacine.equals(DOSSIERRACINE))
			parameters.append("-dir=").append(dossierRacine).append(' ');
		if (delaiLogs != DELAILOGS)
			parameters.append("-kplogs=").append(delaiLogs).append(' ');
		if (debug)
			parameters.append("-d").append(' ');
		
		return parameters.toString();
	}
	
	/**
	 * Retourne le message d'erreur
	 * @param html
	 * @return
	 */
    public String getMsgErreur(boolean html)
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        if (html)
        	chaine = "<html>" + msgErreur.replaceAll("\n", "<br>") + "</html>";
        else
        	chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }

    /**
     * Retourne false si les dossiers sont diff�rents
     * @param folder1
     * @param folder2
     * @return
     *
    public boolean checkRootFolder(String folder1, String folder2)
    {
    	if ((folder1 == null) || (folder2 == null)) return false; 
    	return folder1.equals(folder2);
    }*/

	// -- M�thodes priv�es ----------------------------------------------------
	

	// -- Accesseurs ----------------------------------------------------------
	/**
	 * @return le nom
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * @param name le nom � d�finir
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	/**
	 * @return le default_param
	 */
	public boolean isDefault_param()
	{
		return default_param;
	}

	/**
	 * @param default_param le default_param � d�finir
	 */
	public void setDefault_param(boolean default_param)
	{
		this.default_param = default_param;
	}

	/**
	 * @return le host
	 */
	public String getHost()
	{
		if (host == null) host = "";
		return host;
	}
	/**
	 * @param host le host � d�finir
	 */
	public void setHost(String ahost)
	{
		this.host = ahost;
	}
	/**
	 * @return le port
	 */
	public int getPort()
	{
		return port;
	}
	/**
	 * @param port le port � d�finir
	 */
	public void setPort(int aport)
	{
		this.port = aport;
	}
	/**
	 * @return le dossierRacine
	 */
	public String getDossierRacine()
	{
		return dossierRacine;
	}
	/**
	 * @param dossierRacine le dossierRacine � d�finir
	 */
	public void setDossierRacine(String dossierRacine)
	{
		if ((dossierRacine != null) && (!dossierRacine.equals("")))
			this.dossierRacine = dossierRacine;
	}
	/**
	 * @return le debug
	 */
	public boolean isDebug()
	{
		return debug;
	}
	/**
	 * @param debug le debug � d�finir
	 */
	public void setDebug(boolean debug)
	{
		this.debug = debug;
	}
	/**
	 * @return le delaiLogs
	 */
	public int getDelaiLogs()
	{
		return delaiLogs;
	}
	/**
	 * @param delaiLogs le delaiLogs � d�finir
	 */
	public void setDelaiLogs(int delaiLogs)
	{
		this.delaiLogs = delaiLogs;
	}

	/**
	 * @return le status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status le status � d�finir
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @return le jvmParam
	 */
	public String getJvmParam()
	{
		return jvmParam;
	}

	/**
	 * @param jvmParam le jvmParam � d�finir
	 */
	public void setJvmParam(String jvmParam)
	{
		if (jvmParam != null)
			this.jvmParam = jvmParam + " " + this.jvmParam;
		else
			this.jvmParam = jvmParam;
	}

}
