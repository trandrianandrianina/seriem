//=================================================================================================
//==>                                                                       05/07/2006 - 19/11/2015
//==> Gestion d'un environnement de RunTime
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

public class GestionRT
{
    private final static String NOM_CLASSE="[GestionRT]";
    
    // Constantes erreurs de chargement
    private final static String ERREUR_LISTAGE_DOSSIER="Erreur lors du listage du dossier : ";
    private final static String ERREUR_BALAYAGE_DOSSIER="Erreur lors du balayage du dossier : ";
    private final static String ERREUR_NOM_FICHIER_INCORRECT="Erreur nom du fichier incorrect : ";
//    private final static String ERREUR_CONSTITUTION_CHAINE="Erreur lors de la constitution de la chaine contenant la liste.";
//    private final static String ERREUR_DECOUPAGE_CHAINE="Erreur lors du d\u00e9coupage de la chaine contenant la liste.";
    private final static String ERREUR_NOMBRE_ELT="Erreur le nombre d'\u00e9l\u00e9ments du tableau est incorrect : ";
    private final static String ERREUR_FICHIER_INTROUVABLE_RT="Fichier non trouv\u00e9 dans la liste des Runtimes : ";
    private final static String ERREUR_FICHIER_INTROUVABLE="Fichier non trouv\u00e9 sur le disque : ";
    private final static String ERREUR_LECTURE_FICHIER="Erreur lors de la lecture du fichier : ";
    private final static String ERREUR_ECRITURE_FICHIER="Erreur lors de l'\u00e9criture du fichier : ";
    private final static String ERREUR_NOM_FICHIER_NON_TROUVE="Erreur le nom du fichier n'a pas \u00e9t\u00e9 trouv\u00e9 : ";

    // Variables
    private String dossierRT=null;
    private File fdossierRT=null;
    private ArrayList<File> vtabnomcpltFicRT=null;
    private HashMap<String, Integer> hm_cptFicRT=null;

    private String nomConfig=null;
    private String prefixdevice=null;
    private String host=null;
    private String jobq=null;		// <- renommer jobq en subsystem
    private String program=null;
    private String library=null;			// Bib o� s'execute les jobs
    private String ch_listeIPaccepted=null;
    private String[] listeIPaccepted=null;
    private String libraryEnv=null;			// Bib contenant le PSEMUSSM
    private String folder=null;
    private boolean titleBarInformations=false;
    private String typemenu=null;
    private boolean ctrlPrf = false;
	private JsonParser parser = new JsonParser();
	private Gson gson = new Gson();
    
    private String msgErreur="";          // Conserve le dernier message d'erreur �mit et non lu


    /**
     * Constructeur de la classe
     *
     */
    public GestionRT(JsonObject objet)
    {
    	setObjetJSON(objet);
    }


    /**
     * Constructeur de la classe
     * @param dossier
     * @param nConfig
     * @param sHost
     * @param dev
     * @param ajobq
     * @param prg
     * @param bib
     * @param bibenv
     * @param fld
     * @param isTitleBarInfos
     * @param ctrlprf
     */
    public GestionRT(String dossier, String nConfig, String sHost, String dev, String ajobq, String prg, String bib, String bibenv, String fld, boolean isTitleBarInfos, String tmnu, boolean ctrlprf)
    {
    	this(nConfig, sHost, dev, ajobq, prg, bib, bibenv, fld, isTitleBarInfos, tmnu, ctrlprf);
    	
        dossierRT = dossier + File.separator + Constantes.DOSSIER_RT + File.separator + folder;
        fdossierRT = new File(dossierRT);
    }

    /**
     * Constructeur de la classe
     * @param nConfig
     * @param sHost
     * @param dev
     * @param ajobq
     * @param prg
     * @param bib
     * @param bibenv
     * @param fld
     * @param isTitleBarInfos
     * @param tmnu
     * @param ctrlprf
     */
    public GestionRT(String nConfig, String sHost, String dev, String ajobq, String prg, String bib, String bibenv, String fld, boolean isTitleBarInfos, String tmnu, boolean ctrlprf)
    {
        nomConfig = nConfig;
        host = sHost;
        prefixdevice = dev;
        jobq = ajobq;
        program = prg;
        library = bib;
        libraryEnv = bibenv;

        if ((fld == null) || (fld.trim().equals("")))
    		folder = nomConfig;
        else
            folder = fld;

        titleBarInformations = isTitleBarInfos;
        setTypemenu(tmnu);
        setCtrlPrf(ctrlprf);
//System.out.println(nConfig +" " + sHost+" " + dev+" " +  subsys+" " +  prg+" " +  bib);        
    }

    /**
     * V�rifie l'existence du dossier des RT
     * @return
     */
    // 	TODO ajouter un test car si le dossier existe c'est pas sur qu'il y est des trucs dedans
    public boolean isPresent()
    {
//System.out.println("-[GestionRT]isPresent()-> " + fdossierRT.getAbsolutePath());    	
    	return fdossierRT.exists();
    }

    /**
     * Cr�� l'arborescence pour le dossier des RT
     * @return
     */
    public int creationDossier()
    {
        if (fdossierRT.mkdirs()) return Constantes.TRUE;
        else return Constantes.FALSE;
    }

    /**
     * Listage des fichiers d'un dossier & de ses sous dossiers
     * @param repertoire
     */
    private void listeRepertoire(File repertoire) 
    {
        if (repertoire.isDirectory())
        {
            File[] list = repertoire.listFiles();
            for ( int i=0; i<list.length; i++) 
            {
                // Appel r�cursif sur les sous-r�pertoires
                listeRepertoire(list[i]);
            } 
        }
        else
            vtabnomcpltFicRT.add(repertoire);
    }

    /**
     * Listage des fichiers du dossier RT
     * @return
     */
    private File[] listageDossier()
    {
        vtabnomcpltFicRT = new ArrayList<File>(Constantes.MAXVAR);
        listeRepertoire(fdossierRT);
        vtabnomcpltFicRT.trimToSize();
        return (File[])(vtabnomcpltFicRT.toArray(new File[vtabnomcpltFicRT.size()]));
    }

    /**
     * Balayage des fichiers du dossier RT
     * @return
     */
    public int balayageDossier()
    {
        int i=0, cpt=0, lng=0;
        String nom=null;
        File[] listefic=null;
        
        if (hm_cptFicRT == null)
        	hm_cptFicRT = new HashMap<String, Integer>(Constantes.MAXVAR);
        else
        	hm_cptFicRT.clear();
        listefic = listageDossier();
        if (listefic == null)
        {
            msgErreur = NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_LISTAGE_DOSSIER + dossierRT;
            return Constantes.ERREUR;
        }
        
        // Longueur de la racine afin de r�cup�rer le chemin relatif
        lng = dossierRT.length() + 1;
        
        // On analyse les fichiers du dossier & des sous-dossier
        for (i=0; i<listefic.length; i++)
        {
          if (listefic[i].isFile())
          {
            // On r�cup�re juste le chemin relatif (ainsi on est presque ind�pendant de la machine)
//System.out.println("--> " + listefic[i].getPath());            
            nom = listefic[i].getPath().substring(lng).trim();
            // On lit le fichier Manifest de tous les fichiers JAR
            if (nom.toLowerCase().endsWith(".jar"))
            {
            	cpt = JarUtils.getJarSpecificationVersionToInt(listefic[i].getPath()); 
            	// Ce n'est peut �tre pas un fichier JAR conforme alors on prend la taille du fichier 
            	if (cpt == 0)
            		cpt = (int)listefic[i].length();
            }
            else // On recup�re juste la taille du fichier
            	cpt = (int)listefic[i].length();

            hm_cptFicRT.put(nom.replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR), new Integer(cpt));
//System.out.println("[GestionRT] (balayageDossier) "  + nom+ "="+cpt );            
          }
        }
        // Permet de r�cup�rer la m�moire
        listefic=null;
        

        return Constantes.TRUE;
    }

    /**
     * Retourne le compteur pour un fichier particulier
     * @param nomfic
     * @return
     */
    public int getCptFic(String nomfic)
    {
        int trouve=Constantes.FALSE;
        Integer cpt=null;

        if (hm_cptFicRT == null)
            if (balayageDossier() == Constantes.ERREUR)
            {
                msgErreur = getMsgErreur() + Constantes.crlf + ERREUR_BALAYAGE_DOSSIER + dossierRT;
//System.out.println(msgErreur);
                return Constantes.ERREUR;
            }
  
        if ((nomfic != null) && (!nomfic.trim().equals("")))
        {
            cpt = (Integer)hm_cptFicRT.get(nomfic.replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR));
            if (cpt != null) trouve = Constantes.TRUE;
        }
        else
        {
            msgErreur = getMsgErreur() + Constantes.crlf + ERREUR_NOM_FICHIER_INCORRECT + nomfic.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
//System.out.println(msgErreur);            
            return Constantes.ERREUR;
        }

        if (trouve == Constantes.FALSE)
        {
            msgErreur = getMsgErreur() + Constantes.crlf + ERREUR_NOM_FICHIER_NON_TROUVE + nomfic.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
//System.out.println(msgErreur);
            return Constantes.ERREUR;
        }
            
        return cpt.intValue();
    }

    /**
     * Mise � jour d'un fichier RT & de la liste
     * @param infos
     * @param tabfic
     * @return
     */
    public String setFichier(String infos, byte tabfic[])
    {
        DataOutputStream f=null;
        String[] tabinfos=null;
        File fdossier=null;
        String dossier=null;
        
        // D�composition des infos
        tabinfos = Constantes.splitString(infos, Constantes.SEPARATEUR_CHAINE_CHAR);

        // On cr�e le dossier si besoin
        dossier = dossierRT + File.separator + tabinfos[0].replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
        
        fdossier = new File(dossier.substring(0, dossier.lastIndexOf(File.separatorChar)));
        fdossier.mkdirs();
        
        //  On enregistre le fichier
        try
        {
//System.out.println("--> dossier RT:" + dossier);        	
            f = new DataOutputStream(new FileOutputStream(dossier));
            f.write(tabfic, 0, tabfic.length);
            f.close();
        }
        catch (Exception e)
        {
            msgErreur = ERREUR_ECRITURE_FICHIER + tabinfos[0].replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar) + Constantes.crlf + e;
            return null;
        }
        
        // On met � jour la liste des RT en supprimant l'entr�e existante si existe d�j�
        hm_cptFicRT.remove(tabinfos[0]);
        // Puis en ajoutant les nouvelles infos
        hm_cptFicRT.put(tabinfos[0], new Integer(Integer.parseInt(tabinfos[1])));
        
        // On retourne la chaine tel quelle (a voir si pose pb)
        return tabinfos[0]; //.replace(SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
    }

    /**
     * Retourne un fichier particulier
     * @param nomfic
     * @return
     */
    public byte[] getFichier(String nomfic)
    {
        File fichier=null;
        DataInputStream f=null;
        byte tabfic[]=null;
        
        // On v�rifie l'existence du fichier dans la liste des Runtimes
        if (getCptFic(nomfic) == Constantes.ERREUR)
        {
            msgErreur = getMsgErreur() + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE_RT + Constantes.crlf + nomfic.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
            return null;
        }

        // On v�rifie l'existence du fichier sur le disque
//System.out.println("GetFichier:" + dossierRT + File.separator + nomfic.replace(SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
        fichier = new File(dossierRT + File.separator + nomfic.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
        if (!fichier.exists())
        {
            msgErreur = ERREUR_FICHIER_INTROUVABLE + Constantes.crlf + nomfic.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
            return null;
        }
        else
            tabfic = new byte[(int)fichier.length()];
        
        //  On lit le fichier et on le stocke dans un tableau de byte
        try
        {
            f = new DataInputStream(new FileInputStream(fichier));
            if (f.read(tabfic) != tabfic.length)
            {
                msgErreur = ERREUR_LECTURE_FICHIER + nomfic;
                f.close();
                return null;
            }
            f.close();
        }
        catch (Exception e)
        {
            msgErreur = ERREUR_LECTURE_FICHIER + nomfic + Constantes.crlf + e;
            return null;
        }
        
        return tabfic;
    }

    /**
     * Initialise la liste des Runtimes via une chaine JSON
     * @param valeur
     * @return
     */
    public int setListeRTJSON(String valeur)
    {
		JsonObject objet;

//System.out.println("/////////////////////////> longeur chaine " + valeur.length());
		
		try
		{
			objet = (JsonObject) parser.parse(Base64Coder.Decompress(valeur, "UTF-8"));
		}
		catch (JsonSyntaxException e)
		{
			objet = null;
		}
		catch (IOException e)
		{
			objet = null;
		}
		if (objet == null)
			return Constantes.ERREUR;

		// Fichiers
		JsonArray arrayFichiers=(JsonArray)objet.get("FILES");
		// Compteurs
		JsonArray arrayCompteurs=(JsonArray)objet.get("COUNTERS");

		if (arrayFichiers.size() != arrayCompteurs.size())
		{
			msgErreur =  NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_NOMBRE_ELT + "fichiers=" + arrayFichiers.size() + " compteurs=" + arrayCompteurs.size();
			return Constantes.ERREUR;
		}

		// On charge la table � partir de la liste obtenue
		int nbrelt = arrayFichiers.size();
		hm_cptFicRT = new HashMap<String, Integer>(nbrelt);
		for (int i=0; i<nbrelt; i++)
			hm_cptFicRT.put(arrayFichiers.get(i).getAsString(), arrayCompteurs.get(i).getAsInt());
		//System.out.println("/////////////////////////> nbr de runtimes " + hm_cptFicRT.size());

		return Constantes.OK;
    }


    /**
     * Retourne la liste des fichiers sous forme d'une chaine JSON zipper et encod�e en base64
     * @return
     */
	public String getListeRTJSON(boolean relecture)
    {
        if (relecture) balayageDossier();

		JsonArray arrayFichiers = new JsonArray();
		JsonArray arrayCompteurs = new JsonArray();
        for(Entry<String, Integer> entry : hm_cptFicRT.entrySet())
        {
        	arrayFichiers.add(new JsonPrimitive(entry.getKey()));
        	arrayCompteurs.add(new JsonPrimitive(entry.getValue()));
        }
        JsonObject objet = new JsonObject();
        objet.add("FILES", arrayFichiers);
        objet.add("COUNTERS", arrayCompteurs);
        
        return Base64Coder.Compress(gson.toJson(objet), "UTF-8");
    }

    /**
     * Retourne le nom de cette Config
     * @return
     */
    public String getNomConfig()
    {
        return nomConfig;
    }

    /**
     * Retourne le pr�fixe de la device
     * @return
     */
    public String getPrefixDevice()
    {
        return prefixdevice;
    }

    /**
     * Retourne le nom du Host
     * @return
     */
    public String getHost()
    {
        return host;
    }

    /**
     * Retourne la jobq
     * @return
     */
    public String getJobQ()
    {
        return jobq;
    }

    /**
     * Retourne le programme
     * @return
     */
    public String getProgram()
    {
        return program;
    }

    /**
     * Retourne la biblioth�que
     * @return
     */
    public String getLibrary()
    {
        return library;
    }

    /**
	 * @return le libraryEnv
	 */
	public String getLibraryEnv()
	{
		return libraryEnv;
	}

	/**
	 * @param libraryEnv le libraryEnv � d�finir
	 */
	public void setLibraryEnv(String libraryEnv)
	{
		this.libraryEnv = libraryEnv;
	}

	/**
	 * @return le folder
	 */
	public String getFolder()
	{
		return folder;
	}

	/**
	 * @param folder le folder � d�finir
	 */
	public void setFolder(String folder)
	{
		this.folder = folder;
	}

	/**
	 * @return le titleBarInformations
	 */
	public boolean isTitleBarInformations()
	{
		return titleBarInformations;
	}


	/**
	 * @param titleBarInformations le titleBarInformations � d�finir
	 */
	public void setTitleBarInformations(boolean titleBarInformations)
	{
		this.titleBarInformations = titleBarInformations;
	}


	/**
	 * @return le typemenu
	 */
	public String getTypemenu()
	{
		return typemenu;
	}


	/**
	 * @param typemenu le typemenu � d�finir
	 */
	public void setTypemenu(String typemenu)
	{
		this.typemenu = typemenu;
	}


	/**
     * Retourne la liste des Accepted
     * @return
     */
    public String getchAccepted()
    {
        return ch_listeIPaccepted;
    }

	/**
	 * @return accepted
	 */
	public String[] getAccepted()
	{
		return listeIPaccepted;
	}

	/**
	 * @param accepted accepted � d�finir
	 */
	public void setAccepted(String accepted)
	{
		if (accepted == null) return;
		ch_listeIPaccepted = accepted.trim();
		listeIPaccepted = Constantes.splitString(ch_listeIPaccepted, ' ');
	}

	/**
	 * 
	 */
	public boolean isAccepted(String ipclient)
	{
		if (listeIPaccepted != null)
		{
			for (int i=0; i<listeIPaccepted.length; i++)
				if (ipclient.startsWith(listeIPaccepted[i])) return true;
			return false;
		}
		else
			return true;
	}
	
	/**
	 * @return le ctrlPrf
	 */
	public boolean isCtrlPrf()
	{
		return ctrlPrf;
	}


	/**
	 * @param ctrlPrf le ctrlPrf � d�finir
	 */
	public void setCtrlPrf(boolean ctrlPrf)
	{
		this.ctrlPrf = ctrlPrf;
	}


	/**
	 * Retourne les infos dans un arraylist
	 * @return
	 */
	public ArrayList<String> getArrayList()
	{
		ArrayList<String> liste = new ArrayList<String>(20);
		
		liste.add("[" + nomConfig + "]");
		if ((host != null) && (!host.equals("")))
			liste.add("host=" + host);
		if ((prefixdevice != null) && (!prefixdevice.equals("")))
			liste.add("prefix_device=" + prefixdevice);
		if ((jobq != null) && (!jobq.equals("")))
			liste.add("jobq=" + jobq);
		if ((program != null) && (!program.equals("")))
			liste.add("program=" + program);
		if ((library != null) && (!library.equals("")))
			liste.add("library=" + library);
		if ((libraryEnv != null) && (!libraryEnv.equals("")))
			liste.add("library_env=" + libraryEnv);
		if ((folder != null) && (!folder.equals("")))
			liste.add("folder=" + folder);
		liste.add("titlebarinformations=" + titleBarInformations);
		if ((typemenu != null) && (!typemenu.equals("")))
			liste.add("typemenu=" + typemenu);
		if ((ch_listeIPaccepted != null) && (!ch_listeIPaccepted.equals("")))
			liste.add("accepted=" + ch_listeIPaccepted);
		liste.add("ctrlprf=" + ctrlPrf);
		
		return liste;
	}
	
    /**
     * Retourne un objet JSON de cet objet
     * @return
     */
	public JsonObject getObjetJSON()
    {
        JsonObject objenv = new JsonObject();
        
        objenv.add("nomConfig", new JsonPrimitive(nomConfig));
		if ((host != null) && (!host.equals("")))
	        objenv.add("host", new JsonPrimitive(host));
		if ((prefixdevice != null) && (!prefixdevice.equals("")))
			objenv.add("prefix_device", new JsonPrimitive(prefixdevice));
		if ((jobq != null) && (!jobq.equals("")))
			objenv.add("jobq", new JsonPrimitive(jobq));
		if ((program != null) && (!program.equals("")))
			objenv.add("program", new JsonPrimitive(program));
		if ((library != null) && (!library.equals("")))
			objenv.add("library", new JsonPrimitive(library));
		if ((libraryEnv != null) && (!libraryEnv.equals("")))
			objenv.add("libraryEnv", new JsonPrimitive(libraryEnv));
		if ((folder != null) && (!folder.equals("")))
			objenv.add("folder", new JsonPrimitive(folder));
		objenv.add("titleBarInformations", new JsonPrimitive(titleBarInformations));
		if ((typemenu != null) && (!typemenu.equals("")))
			objenv.add("typemenu", new JsonPrimitive(typemenu));
		objenv.add("ctrlprf", new JsonPrimitive(ctrlPrf));

        return objenv;
    }

    /**
     * Initialise la classe avec une chaine JSON
     * @param chaineJSON
     */
    public void setObjetJSON(JsonObject objet)
    {
		nomConfig = objet.get("nomConfig").getAsString();
		prefixdevice = objet.get("prefix_device").getAsString();
		jobq = objet.get("jobq").getAsString();
		program = objet.get("program").getAsString();
		library = objet.get("library").getAsString();
		libraryEnv = objet.get("libraryEnv").getAsString();
		folder = objet.get("folder").getAsString();
		titleBarInformations = objet.get("titleBarInformations").getAsBoolean();
		if (objet.get("typemenu") != null)
			typemenu = objet.get("typemenu").getAsString();
		if (objet.get("ctrlprf") != null)
			ctrlPrf = objet.get("ctrlprf").getAsBoolean();
/*System.out.println("-nomConfig-> " + nomConfig);		
System.out.println("-prefixdevice-> " + prefixdevice);		
System.out.println("-subsystem-> " + subsystem);		
System.out.println("-program-> " + program);		
System.out.println("-library-> " + library);		
System.out.println("-libraryEnv-> " + libraryEnv);		
System.out.println("-folder-> " + folder);*/		
    }

    /**
     * Initialise les donn�es du user � partir des donn�es de la config
     * @param infoUser
     */
    public void initInfoUser(EnvUser infoUser)
    {
		infoUser.setHost(getHost());
		infoUser.setPrefixDevice(getPrefixDevice());
		infoUser.setJobQ(getJobQ());
		infoUser.setLibrary(getLibrary());
		infoUser.setLibraryEnv(getLibraryEnv());
		infoUser.setFolder(getFolder());
		infoUser.setProgram(getProgram());
		infoUser.setTitleBarInformations(isTitleBarInformations());
		infoUser.setTypemenu(getTypemenu());
    }


    /**
     * Retourne le message d'erreur
     * @return
     */
    public String getMsgErreur()
    {
        String chaine;

        // La r�cup�ration du message est � usage unique
        chaine = msgErreur;
        msgErreur = "";
        
        return chaine;
    }

    /**
     * Lib�re les ressources
     */
    public void dispose()
    {
    	if( vtabnomcpltFicRT != null ){
    		vtabnomcpltFicRT.clear();
    		vtabnomcpltFicRT = null;
    	}
    	if( hm_cptFicRT != null ){
    		hm_cptFicRT.clear();
    		hm_cptFicRT = null;
    	}
    	parser = null;
    	gson = null;
    }
}