//=================================================================================================
//==>                                                                       04/01/2016 - 04/01/2016
//=================================================================================================
package ri.seriem.libcommun.outils.menus;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.seriem.libcommun.outils.Constantes;

public class baseModel
{
	// Variables
	private	LinkedHashMap<String, ArrayList<String>>		listMenu4Model			= new LinkedHashMap<String, ArrayList<String>>(Constantes.MAXVAR);
	private						String						library					= null;
	private						String						fileem					= null;
	private						String						filepm					= null;
	
	
	public void put(String ssm, ArrayList<String> alistOrd){
		listMenu4Model.put(ssm, alistOrd);
	}

	public ArrayList<String> getListOrd(String ssm)
	{
		return listMenu4Model.get(ssm);
	}

	public void getConditions(StringBuilder conditions)
	{
		for( Entry<String, ArrayList<String>> entry : listMenu4Model.entrySet() ){
			conditions.append(entry.getKey()).append(", ");
		}
	}
	
	/**
	 * Contr�le qu'un module donn� soit bien dans la liste
	 * @param wgrp
	 * @return
	 */
	public ArrayList<String> checkSsModule(String ord)
	{
		for( Entry<String, ArrayList<String>> entry : listMenu4Model.entrySet() ){
			if( entry.getKey().equals( ord ) ){
				return entry.getValue();
			}
		}
		return null;
	}


	/**
	 * Vide le mod�le 
	 */
	public void clearListMenu4Model()
	{
	   	if( listMenu4Model != null ){
    		for( Entry<String, ArrayList<String>> entry : listMenu4Model.entrySet() ){
    		    if( entry.getValue() != null ){
    		    	entry.getValue().clear();
    		    }
    		}
    		listMenu4Model.clear();
    	}
	}

	/**
	 * @return le listMenu4Model
	 */
	public LinkedHashMap<String, ArrayList<String>> getListMenu4Model()
	{
		return listMenu4Model;
	}
	
	/**
	 * @param listMenu4Model le listMenu4Model � d�finir
	 */
	public void setListMenu4Model(LinkedHashMap<String, ArrayList<String>> listMenu4Model)
	{
		this.listMenu4Model = listMenu4Model;
	}
	
	/**
	 * @return le library
	 */
	public String getLibrary()
	{
		return library;
	}
	/**
	 * @param library le library � d�finir
	 */
	public void setLibrary(String library)
	{
		this.library = library;
	}
	/**
	 * @return le fileem
	 */
	public String getFileem()
	{
		if( fileem == null ){
			if( library.equals( "expas" ) )
				return "mnemsgm";
			else
				return "mnspmem";
		} else
			return fileem;
	}
	/**
	 * @param fileem le file � d�finir
	 */
	public void setFileem(String file)
	{
		this.fileem = file;
	}
	/**
	 * @return le filepm
	 */
	public String getFilepm()
	{
		if( filepm == null ){
			if( library.equals( "expas" ) )
				return "mnpmsgm";
			else
				return "mnspmpm";
		} else
			return filepm;
	}
	/**
	 * @param filepm le file � d�finir
	 */
	public void setFilepm(String file)
	{
		this.filepm = file;
	}
	
	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		clearListMenu4Model();
	}	
}
