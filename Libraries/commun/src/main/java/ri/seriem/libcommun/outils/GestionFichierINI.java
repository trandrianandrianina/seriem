//=================================================================================================
//==> Gestion des fichiers INI                                              24/07/2006 - 18/04/2016
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.swing.JFileChooser;


public class GestionFichierINI extends GestionFichierTexte
{
    //private final static String NOM_CLASSE="[GestionFichierINI]";
    
    // Constantes
	private final static String FILTER_EXT_INI="ini";
	private final static String FILTER_DESC_INI="Fichier INI (*.ini)";

    // Variables
    private LinkedHashMap<String, HashMap<String, String>> listeNomSection=null;

	/**
	 * Constructeur
	 */
	public GestionFichierINI()
	{
	}

	/**
     * Constructeur de la classe
     * @param fch
     */
    public GestionFichierINI(String fch)
    {
    	super(fch);
    }
	
    /**
     * Retourne la hashmap des Sections
     * @return
     */
    public LinkedHashMap<String, HashMap<String, String>> getSections()
    {
        int i=0, pos=0;
        String chaine=null, section=null;
        ArrayList<String> lst=getContenuFichier();
        HashMap<String, String> listeDetailSection=null;
        
        listeNomSection = new LinkedHashMap<String, HashMap<String, String>>();
        if( lst == null ) return listeNomSection; 
        
        // On parcourt la liste afin d'en extraire les donn�es
        for (i=0; i<lst.size(); i++)
        {
            chaine = ((String)lst.get(i)).trim();

            // Les erreurs possibles ou lignes ignor�es
            if (chaine.equals("")) continue;
            if (chaine.startsWith(";")) continue;

            // Traitement section
            if ((chaine.startsWith("[")) && (chaine.endsWith("]")))
            {
            	section = chaine.substring(1, chaine.length() - 1).trim();
            	listeDetailSection = new HashMap<String, String>();
            	listeNomSection.put(section, listeDetailSection);
            }
            else // Traitement propri�t�
            {
                pos = chaine.indexOf('=');
                if (pos != -1)
                    listeDetailSection.put(chaine.substring(0, pos).trim().toLowerCase(), chaine.substring(pos+1).trim());
            }   
        }
        lst.clear();
            
        return listeNomSection;
    }

    /**
     * Retourne la liste des Sections
     * A toujours utiliser apr�s getListeNomSection (TODO � corriger)
     * @return
     */
    public String[] getListeNomSection()
    {
        int i=0;
        String lst[]=null;
        
        if (listeNomSection == null)
        	listeNomSection = getSections();
        
        lst = new String[listeNomSection.size()];
        //Set<String> lSection = listeNomSection.keySet();
    	Iterator<String> iterateur = listeNomSection.keySet().iterator();
    	i = 0;
    	while(iterateur.hasNext())
    		lst[i++] = (String)iterateur.next();
            
        return lst;
    }

	/**
	 * Retourne les filtres possible pour les boites de dialogue
	 * @param jfc
	 */
	public void initFiltre(JFileChooser jfc)
	{
		super.initFiltre(jfc);
		jfc.addChoosableFileFilter(new GestionFiltre(new String[]{FILTER_EXT_INI}, FILTER_DESC_INI));
	}
	
	/**
	 * Lib�re la m�moire
	 */
	public void dispose()
	{
		super.dispose();
		if (listeNomSection != null)
			for(Entry<String, HashMap<String, String>> entry : listeNomSection.entrySet()){
				if( entry.getValue() != null )
					entry.getValue().clear();
			}
			listeNomSection.clear();
		listeNomSection = null;
	}

}
    