/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.libcommun.outils.Constantes;

/**
 * Description d'un message de demande de l'utilisateur.
 */
public class Demande {
  // Constantes
  public static final int CLEF = 0;
  public static final int BUFFER = 1;
  
  // Variables
  private String idm = null; // l'ID message (5)
  private String numjb = null; // le num�ro du job (10)
  private String nomjb = null; // le nom du job (10)
  private String usrjb = null; // le nom du user du job (10)
  private String nomspool = null; // le nom du spool (10)
  private String numspool = null; // le num�ro du spool (10)
  private String prf = null; // le profil (10)
  private String bib = null; // la biblioth�que (10)
  private String idusr = null; // l'ID utilisateur (50)
  private char compress = ' '; // la compression de dat ('1' oui, ' ', non)
  private char obj = ' '; // le type de dat ('1' objet s�rialis�, ' ' string, '2' json)
  private String tdat = null; // la taille des donn�es originales (avant compression) (10)
  private String databrut = ""; // donn�es brutes (zipp�es + base64)
  private String data = null; // donn�es d�cod�es (texte)
  
  /**
   * Constructeur
   */
  public Demande() {
  }
  
  /**
   * Constructeur.
   */
  public Demande(int type, String chaine) {
    switch (type) {
      case CLEF:
        setClef(chaine);
        break;
      case BUFFER:
        setReceptionBuffer(chaine);
        break;
    }
  }
  
  /**
   * Initialise la demande et fait le d�coupage
   * @param msg
   */
  public boolean setReceptionBuffer(String msg) {
    clear();
    
    if (msg == null) {
      return false;
    }
    msg = msg.trim();
    
    setIdm(msg.substring(0, 5));
    
    if (msg.length() < 5) {
      return false;
    }
    if (msg.length() < 15) {
      setNumjb(msg.substring(5));
    }
    else {
      setNumjb(msg.substring(5, 15));
    }
    
    if ((msg.length() < 15)) {
      return false;
    }
    if (msg.length() < 25) {
      setNomjb(msg.substring(15));
    }
    else {
      setNomjb(msg.substring(15, 25));
    }
    
    if ((msg.length() < 25)) {
      return false;
    }
    if (msg.length() < 35) {
      setUsrjb(msg.substring(25));
    }
    else {
      setUsrjb(msg.substring(25, 35));
    }
    
    if ((msg.length() < 35)) {
      return false;
    }
    if (msg.length() < 45) {
      setNomspool(msg.substring(35));
    }
    else {
      setNomspool(msg.substring(35, 45));
    }
    
    if ((msg.length() < 45)) {
      return false;
    }
    if (msg.length() < 55) {
      setNumspool(msg.substring(45));
    }
    else {
      setNumspool(msg.substring(45, 55));
    }
    
    if ((msg.length() < 55)) {
      return false;
    }
    if (msg.length() < 65) {
      setPrf(msg.substring(55));
    }
    else {
      setPrf(msg.substring(55, 65));
    }
    
    if ((msg.length() < 65)) {
      return false;
    }
    if (msg.length() < 75) {
      setBib(msg.substring(65));
    }
    else {
      setBib(msg.substring(65, 75));
    }
    
    if ((msg.length() < 75)) {
      return true;
    }
    if (msg.length() < 125) {
      setIdusr(msg.substring(75));
    }
    else {
      setIdusr(msg.substring(75, 125));
    }
    
    if ((msg.length() < 125)) {
      return true;
    }
    setCompress(msg.charAt(125));
    setObj(msg.charAt(126));
    
    if ((msg.length() < 130)) {
      return true;
    }
    if (msg.length() < 140) {
      setTdat(msg.substring(130));
    }
    else {
      setTdat(msg.substring(130, 140));
    }
    
    if ((msg.length() < 140)) {
      return true;
    }
    setDataBrut(msg.substring(140));
    
    return true;
  }
  
  /**
   * Nettoie toutes les donn�es
   */
  public void clear() {
    setIdm("");
    setNumjb("");
    setNomjb("");
    setUsrjb("");
    setNomspool("");
    setNumspool("");
    setPrf("");
    setBib("");
    setIdusr("");
    setTdat("");
    setData(' ', ' ', null);
  }
  
  /**
   * Retourne la demande/reponse sous forme de String
   * @return
   */
  public String getReponse() {
    StringBuffer chaine = new StringBuffer(new String(Constantes.spaces));
    chaine.insert(0, getIdm()).insert(5, getNumjb()).insert(15, getNomjb()).insert(25, getUsrjb()).insert(35, getNomspool())
        .insert(45, getNumspool()).insert(55, getPrf()).insert(65, getBib()).insert(75, getIdusr()).insert(125, getCompress())
        .insert(126, getObj()).insert(130, getTdat());
    if (getCompress() == '1') {
      chaine.insert(140, getDataBrut());
    }
    else {
      chaine.insert(140, getData());
    }
    return chaine.toString();
  }
  
  /**
   * @return the idm
   */
  public String getIdm() {
    return idm;
  }
  
  /**
   * @param idm the idm to set
   */
  public void setIdm(String idm) {
    this.idm = idm;
  }
  
  /**
   * @return the numjb
   */
  public String getNumjb() {
    if (numjb == null) {
      return "";
    }
    return numjb.trim();
  }
  
  /**
   * @param numjb the numjb to set
   */
  public void setNumjb(String numjb) {
    this.numjb = numjb;
  }
  
  /**
   * @return the nomjb
   */
  public String getNomjb() {
    if (nomjb == null) {
      return "";
    }
    return nomjb.trim();
  }
  
  /**
   * @param nomjb the nomjb to set
   */
  public void setNomjb(String nomjb) {
    this.nomjb = nomjb;
  }
  
  /**
   * @return the usrjb
   */
  public String getUsrjb() {
    if (usrjb == null) {
      return "";
    }
    return usrjb.trim();
  }
  
  /**
   * @param usrjb the usrjb to set
   */
  public void setUsrjb(String usrjb) {
    this.usrjb = usrjb;
  }
  
  /**
   * @return the nomspool
   */
  public String getNomspool() {
    if (nomspool == null) {
      return "";
    }
    return nomspool.trim();
  }
  
  /**
   * @param nomspool the nomspool to set
   */
  public void setNomspool(String nomspool) {
    this.nomspool = nomspool;
  }
  
  /**
   * @return the numspool
   */
  public String getNumspool() {
    if (numspool == null) {
      return "";
    }
    return numspool.trim();
  }
  
  /**
   * @param numspool the numspool to set
   */
  public void setNumspool(String numspool) {
    this.numspool = numspool;
  }
  
  /**
   * @return the prf
   */
  public String getPrf() {
    if (prf == null) {
      return "";
    }
    return prf;
  }
  
  /**
   * @param prf the prf to set
   */
  public void setPrf(String prf) {
    this.prf = prf.trim();
  }
  
  /**
   * @param bib the bib to set
   */
  public void setBib(String bib) {
    this.bib = bib;
  }
  
  /**
   * @return the bib
   */
  public String getBib() {
    if (bib == null) {
      return "";
    }
    return bib.trim();
  }
  
  /**
   * @return the idusr
   */
  public String getIdusr() {
    if (idusr == null) {
      return "";
    }
    return idusr.trim();
  }
  
  /**
   * @param idusr the idusr to set
   */
  public void setIdusr(String idusr) {
    this.idusr = idusr.trim();
  }
  
  /**
   * @param tdat the tdat to set
   */
  public void setTdat(String tdat) {
    if (tdat != null) {
      this.tdat = tdat.trim();
    }
    else {
      this.tdat = "0";
    }
  }
  
  /**
   * @return the tdat
   */
  public String getTdat() {
    return tdat;
  }
  
  /**
   * @param compress the compress to set
   */
  public void setCompress(char compress) {
    if (compress != ' ') {
      this.compress = '1';
    }
    else {
      this.compress = ' ';
    }
  }
  
  /**
   * @return the compress
   */
  public char getCompress() {
    return compress;
  }
  
  /**
   * @param obj the obj to set
   */
  public void setObj(char obj) {
    this.obj = obj;
  }
  
  /**
   * @return the obj
   */
  public char getObj() {
    return obj;
  }
  
  /**
   * @return the dat
   */
  public String getDataBrut() {
    return databrut;
  }
  
  /**
   * @param data the dat to set
   */
  public void setDataBrut(String datbrut) {
    if (datbrut == null) {
      this.databrut = "";
      this.data = "";
    }
    else {
      this.databrut = datbrut;
      if (getCompress() == '1') {
        this.data = decode(datbrut);
      }
      else {
        this.data = datbrut;
      }
    }
  }
  
  /**
   * @return the dat
   */
  public String getData() {
    return data;
  }
  
  /**
   * @param dat the dat to set
   * @param compress, 1 compression sinon non
   * @param obj, 1 si c'est un objet s�rialis�
   * @param dat, les donn�es
   */
  public void setData(char compress, char obj, String dat) {
    setCompress(compress);
    setObj(obj);
    if (dat == null) {
      this.data = "";
      this.databrut = "";
      setTdat("0");
    }
    else {
      this.data = dat.trim();
      if (getCompress() == '1') {
        this.databrut = encode(this.data);
      }
      else {
        this.databrut = this.data;
      }
      setTdat(String.valueOf(this.data.length()));
    }
  }
  
  /**
   * Retourne une cl� avec la partie des infos int�ressante sur le spool sous forme de chaine
   * @return
   */
  public String getGenereClef() {
    StringBuffer chaine = new StringBuffer();
    chaine.append(getNumjb()).append('|').append(getNomjb()).append('|').append(getUsrjb()).append('|').append(getNomspool()).append('|')
        .append(getNumspool());
    return chaine.toString();
  }
  
  /**
   * Initialise les valeurs gr�ce � la clef (qui contient des infos sur le spool)
   * @return
   */
  public void setClef(String clef) {
    if (clef == null) {
      return;
    }
    
    String[] tab = clef.split("\\|");
    if (tab.length > 0) {
      setNumjb(tab[0]);
    }
    if (tab.length > 1) {
      setNomjb(tab[1]);
    }
    if (tab.length > 2) {
      setUsrjb(tab[2]);
    }
    if (tab.length > 3) {
      setNomspool(tab[3]);
    }
    if (tab.length > 4) {
      setNumspool(tab[4]);
    }
  }
  
  /**
   * Compresse la chaine avec zip puis l'encode en base64
   * @param chaine
   * @return
   */
  private String encode(String chaine) {
    if (chaine == null) {
      return "";
    }
    Deflater compresser = new Deflater();
    byte[] output = new byte[chaine.length() * 2]; // Car parfois la chaine zipp�e peut etre plus grande que l'originale
    
    compresser.setInput(chaine.getBytes());
    compresser.finish();
    int compressedDataLength = compresser.deflate(output);
    return Base64Coder.encodeString(output, compressedDataLength);
  }
  
  /**
   * D�compresse la chaine afin de retrouver la valeur originale
   * @param chaine
   * @return
   */
  private String decode(String chaine) {
    if ((chaine == null) || (chaine.equals(""))) {
      return "";
    }
    
    byte[] input = Base64Coder.decode(chaine);
    byte[] output = new byte[Integer.parseInt(getTdat())];
    Inflater decompresser = new Inflater();
    decompresser.setInput(input, 0, input.length);
    int compressedDataLength = 0;
    try {
      compressedDataLength = decompresser.inflate(output);
    }
    catch (DataFormatException e) {
    }
    decompresser.end();
    return new String(output, 0, compressedDataLength);
  }
}
