//=================================================================================================
//==>                                                                       10/04/2014 - 28/01/2015
//==> Description de l'ent�te du message
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.protocoleMsg;

public class MessageHead
{
	// Constantes
	public static final byte BODY_NO=0;				// Le message ne contient pas aucun message			
	public static final byte BODY_SIMPLESTRING=1;	// Le message contient un message sous la forme d'une chaine simple  
	public static final byte BODY_COMPLEXSTRING=2;	// Le message contient un message sous la forme d'une chaine qui contient des s�parateurs du type | ou du JSON mais sans classe associ�e
	public static final byte BODY_JSON=3;			// Le message contient un message sous la forme de JSON dont le nom de la classe est dans bodydesc
	
	public static final int VERSION=1;				// Version courante du protocole
	
	// Variables
	private int id;
	private int hashcodeCaller=0;					// Hascode du demandeur afin de faciliter l'identification pour le traitement au retour
	private int version=VERSION;
	private boolean bcomp=false;					// True ou false en fonction si l'on souhate une compression ou pas
	private byte bcode;
	private int blength;							// La longueur de la chaine ou du body original
	private String bdesc;							// Contient la chaine de caract�res ou le nom de la classe contenu dans body 
	
	/**
	 * Constructeur
	 * @param ahashcode
	 *
	public MessageHead(int ahashcode)
	{
		hashcodeCaller = ahashcode;
	}*/
	
	/**
	 * @return le id
	 */
	public int getId()
	{
		return id;
	}
	/**
	 * @param id le id � d�finir
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	/**
	 * @return le hashcodeCaller
	 */
	public int getHashcodeCaller()
	{
		return hashcodeCaller;
	}
	/**
	 * @param hashcodeCaller le hashcodeCaller � d�finir
	 */
	public void setHashcodeCaller(int hashcodeCaller)
	{
		this.hashcodeCaller = hashcodeCaller;
	}
	/**
	 * @return le version
	 */
	public int getVersion()
	{
		return version;
	}
	/**
	 * @param version le version � d�finir
	 */
	public void setVersion(int version)
	{
		this.version = version;
	}
	/**
	 * @return le bodycomp
	 */
	public boolean isBodycomp()
	{
		return bcomp;
	}
	/**
	 * @param bodycomp le bodycomp � d�finir
	 */
	public void setBodycomp(boolean bodycomp)
	{
		this.bcomp = bodycomp;
	}
	/**
	 * @return le bodycode
	 */
	public byte getBodycode()
	{
		return bcode;
	}
	/**
	 * @param bodycode le bodycode � d�finir
	 */
	public void setBodycode(byte bodycode)
	{
		this.bcode = bodycode;
	}
	/**
	 * @return le bodylength
	 */
	public int getBodylength()
	{
		return blength;
	}
	/**
	 * @param bodylength le bodylength � d�finir
	 */
	public void setBodylength(int bodylength)
	{
		this.blength = bodylength;
	}
	/**
	 * @return le bodydesc
	 */
	public String getBodydesc()
	{
		return bdesc;
	}
	/**
	 * @param bodydesc le bodydesc � d�finir
	 */
	public void setBodydesc(String bodydesc)
	{
		this.bdesc = bodydesc;
	}
	
}
