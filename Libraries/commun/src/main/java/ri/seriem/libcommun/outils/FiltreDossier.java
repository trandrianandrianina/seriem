package ri.seriem.libcommun.outils;

import java.io.File;
import java.io.FilenameFilter;

public class FiltreDossier implements FilenameFilter
{
	public boolean accept(File dir, String name)
	{
		File folder = new File(dir.getAbsoluteFile() + File.separator + name);
		return (folder.isDirectory());
	}
}
