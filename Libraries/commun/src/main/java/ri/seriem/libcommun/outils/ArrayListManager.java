/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.util.ArrayList;
import java.util.EventListener;

import javax.swing.event.EventListenerList;

/**
 * Gestion d'�v�nement sur une ArrayList
 */
public class ArrayListManager {
  // Variables
  private final EventListenerList listeners = new EventListenerList();
  private final ArrayList<Object> map = new ArrayList<Object>();
  // Param�tre par d�faut (avant la modif), l'execution des �couteurs avant l'action
  private boolean avtClear = false;
  private boolean avtAdd = false;
  private boolean avtRemove = true;
  
  /**
   * Constructeur
   */
  public ArrayListManager() {
    this(false, false, true);
  }
  
  /**
   * Constructeur
   * @param avtclear
   * @param avtadd
   * @param avtremove
   */
  public ArrayListManager(boolean avtclear, boolean avtadd, boolean avtremove) {
    avtClear = avtclear;
    avtAdd = avtadd;
    avtRemove = avtremove;
  }
  
  public static interface alListener extends EventListener {
    public void onDataCleared();
    
    public void onDataAdded(Object val);
    
    public void onDataRemoved(Object val);
    
    public void onDataRemoved(int indice);
  }
  
  public void addListener(alListener l) {
    listeners.add(alListener.class, l);
  }
  
  public void removeListener(alListener l) {
    listeners.remove(alListener.class, l);
  }
  
  public ArrayList<Object> getArrayList() {
    return map;
  }
  
  public void clearObject() {
    if (avtClear) {
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataCleared();
      }
      map.clear();
    }
    else {
      map.clear();
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataCleared();
      }
    }
  }
  
  public void addObject(Object val) {
    if (avtAdd) {
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataAdded(val);
      }
      map.add(val);
    }
    else {
      map.add(val);
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataAdded(val);
      }
    }
  }
  
  public void removeObject(Object val) {
    if (avtRemove) {
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataRemoved(val);
      }
      map.remove(val);
    }
    else {
      map.remove(val);
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataRemoved(val);
      }
    }
  }
  
  public void removeObject(int indice) {
    if (avtRemove) {
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataRemoved(indice);
      }
      map.remove(indice);
    }
    else {
      map.remove(indice);
      final EventListener[] el = listeners.getListeners(alListener.class);
      for (int i = 0; i < el.length; i++) {
        ((alListener) el[i]).onDataRemoved(indice);
      }
    }
  }
  
  public Object getObject(int indice) {
    return map.get(indice);
  }
  
  /**
   * Retourne le nombre de listener actifs
   * @return
   */
  public int getNumberListeners() {
    return listeners.getListeners(alListener.class).length;
  }
}
