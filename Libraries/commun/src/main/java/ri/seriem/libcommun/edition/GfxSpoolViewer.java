/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.EnvUser;

/**
 * Viewer de spool (Attention su prob�me avec les accents voir le codepage dans GestionSpoolAS400)
 */
public class GfxSpoolViewer extends JFrame {
  // Variables
  private Spool spool = null;
  private int pageCourante = 1;
  private boolean isConnected = false;
  
  private EnvUser infoUser = null;
  private Demande demandeEdition = new Demande();
  private String[] infosSpoolSelected = null;
  
  /**
   * Constructeur
   */
  public GfxSpoolViewer(String nomspool, Spool aspool, Demande ademandeEdition, EnvUser ainfoUser, String[] ainfosSpoolSelected) {
    int deltaX = 0;
    int deltaY = 0;
    pack();
    deltaX = getWidth();
    deltaY = getHeight();
    
    spool = aspool;
    initComponents();
    if (nomspool != null) {
      setTitle(getTitle() + " - " + nomspool);
    }
    
    // Dimensionnement du TextArea
    initTailleTextArea();
    initPage(pageCourante);
    
    // Dimensionnement correct de la fen�tre
    Dimension d = getSize();
    setSize(d.width + deltaX + p_MargeDroite.getSize().width + p_MargeGauche.getSize().width,
        d.height + deltaY + p_MargeDroite.getSize().height + p_MargeGauche.getSize().height);
    
    // Init des variables
    demandeEdition = ademandeEdition;
    infoUser = ainfoUser;
    infosSpoolSelected = ainfosSpoolSelected;
    
    setVisible(true);
  }
  
  /**
   * Renvoi la page courante du spool
   * @return
   */
  public StringBuffer[] getPageCouranteSpool() {
    if (spool == null) {
      return null;
    }
    StringBuffer[] sb = new StringBuffer[1];
    sb[0] = new StringBuffer(spool.getPages()[pageCourante - 1]);
    return sb;
  }
  
  /**
   * Renvoi le texte complet du spool
   * @return
   */
  public String[] getPagesSpool() {
    if (spool == null) {
      return null;
    }
    return spool.getPages();
  }
  
  /**
   * Dimensionnement du composant TextArea
   */
  private void initTailleTextArea() {
    int size = ((Integer) sp_TaillePolice.getValue()).intValue();
    if (size > 0) {
      ta_Page.setFont(new Font(ta_Page.getFont().getName(), ta_Page.getFont().getStyle(), size));
    }
    int l = ta_Page.getFontMetrics(ta_Page.getFont()).charWidth('M');
    int h = ta_Page.getFontMetrics(ta_Page.getFont()).getHeight();
    Dimension d = new Dimension(spool.getNbrColonnes() * l, spool.getNbrLignes() * h);
    ta_Page.setMinimumSize(d);
    ta_Page.setMaximumSize(d);
    ta_Page.setPreferredSize(d);
    pack();
  }
  
  /**
   * Initialise la page
   */
  private void initPage(int numpage) {
    if ((spool == null) && (numpage > 0) && (numpage >= spool.getNbrPages())) {
      return;
    }
    
    l_NbrPages.setText(numpage + "/" + String.valueOf(spool.getNbrPages()));
    sp_TaillePolice.setValue(new Integer(ta_Page.getFont().getSize()));
    
    ta_Page.setText(spool.getPages()[numpage - 1]);
    
    // On positionne le curseur en haut de la page
    ta_Page.setCaretPosition(0);
    
    // Grisage des boutons Suivant & Pr�cedent
    bt_Precedent.setEnabled(!(numpage == 1));
    bt_Suivant.setEnabled(!(numpage == spool.getNbrPages()));
  }
  
  /**
   * Lance l'exportation du spool en PDF
   */
  private void exportPdf() {
    demandeEdition.clear();
    demandeEdition.setIdm(ConstantesNewSim.GENERE_DOC_FROM_SPL);
    demandeEdition.setNumjb(infosSpoolSelected[0]);
    demandeEdition.setNomjb(infosSpoolSelected[1]);
    demandeEdition.setUsrjb(infosSpoolSelected[2]);
    demandeEdition.setNomspool(infosSpoolSelected[3]);
    demandeEdition.setNumspool(infosSpoolSelected[4]);
    demandeEdition.setPrf(infosSpoolSelected[5]);
    String data = "PDF||" + infoUser.getInfosServerEdition().getPathDocTempoOnServerEdition(infoUser.getCurlib()) + "|"
        + demandeEdition.getNomspool() + '_' + demandeEdition.getNumjb() + "|3|";
    boolean compressdata = false;
    demandeEdition.setData(compressdata ? '1' : ' ', ' ', data);
    // infoUser.getTransfertSession().EnvoiMessageSocket(Constantes.ENVOI_REQUETE_EDITION, demandeEdition.getReponse());
    infoUser.getTransfertSession().sendBuffer(Constantes.ENVOI_REQUETE_EDITION, demandeEdition.getReponse());
  }
  
  private void fermeture() {
    setVisible(false);
    dispose();
  }
  
  /**
   * @return the isConnected
   */
  public boolean isConnected() {
    return isConnected;
  }
  
  private void bt_FermerActionPerformed(ActionEvent e) {
    fermeture();
  }
  
  private void sp_TaillePoliceStateChanged(ChangeEvent e) {
    initTailleTextArea();
  }
  
  private void bt_PrecedentActionPerformed(ActionEvent e) {
    initPage(--pageCourante);
  }
  
  private void bt_SuivantActionPerformed(ActionEvent e) {
    initPage(++pageCourante);
  }
  
  private void thisWindowClosing(WindowEvent e) {
    fermeture();
  }
  
  private void bt_ExportPDFActionPerformed(ActionEvent e) {
    exportPdf();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel2 = new JScrollPane();
    panel8 = new JPanel();
    p_MargeHaute = new JPanel();
    p_MargeDroite = new JPanel();
    p_MargeGauche = new JPanel();
    p_MargeBasse = new JPanel();
    ta_Page = new JTextArea();
    panel1 = new JPanel();
    bt_Fermer = new JButton();
    panel9 = new JPanel();
    label1 = new JLabel();
    l_NbrPages = new JLabel();
    bt_Precedent = new JButton();
    bt_Suivant = new JButton();
    label2 = new JLabel();
    sp_TaillePolice = new JSpinner();
    hSpacer1 = new JPanel(null);
    bt_ExportPDF = new JButton();
    
    // ======== this ========
    setTitle("Visionneuse de spool");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel2 ========
    {
      panel2.setName("panel2");
      
      // ======== panel8 ========
      {
        panel8.setName("panel8");
        panel8.setLayout(new BorderLayout());
        
        // ======== p_MargeHaute ========
        {
          p_MargeHaute.setBackground(Color.gray);
          p_MargeHaute.setName("p_MargeHaute");
          p_MargeHaute.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeHaute, BorderLayout.SOUTH);
        
        // ======== p_MargeDroite ========
        {
          p_MargeDroite.setBackground(Color.gray);
          p_MargeDroite.setName("p_MargeDroite");
          p_MargeDroite.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeDroite, BorderLayout.EAST);
        
        // ======== p_MargeGauche ========
        {
          p_MargeGauche.setBackground(Color.gray);
          p_MargeGauche.setName("p_MargeGauche");
          p_MargeGauche.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeGauche, BorderLayout.WEST);
        
        // ======== p_MargeBasse ========
        {
          p_MargeBasse.setBackground(Color.gray);
          p_MargeBasse.setName("p_MargeBasse");
          p_MargeBasse.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeBasse, BorderLayout.NORTH);
        
        // ---- ta_Page ----
        ta_Page.setFont(new Font("Monospaced", Font.PLAIN, 12));
        ta_Page.setName("ta_Page");
        panel8.add(ta_Page, BorderLayout.CENTER);
      }
      panel2.setViewportView(panel8);
    }
    contentPane.add(panel2, BorderLayout.CENTER);
    
    // ======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(new FlowLayout());
      
      // ---- bt_Fermer ----
      bt_Fermer.setText("Fermer");
      bt_Fermer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Fermer.setName("bt_Fermer");
      bt_Fermer.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_FermerActionPerformed(e);
        }
      });
      panel1.add(bt_Fermer);
    }
    contentPane.add(panel1, BorderLayout.SOUTH);
    
    // ======== panel9 ========
    {
      panel9.setName("panel9");
      panel9.setLayout(new FlowLayout(FlowLayout.LEFT));
      
      // ---- label1 ----
      label1.setText("Page");
      label1.setName("label1");
      panel9.add(label1);
      
      // ---- l_NbrPages ----
      l_NbrPages.setText("??");
      l_NbrPages.setName("l_NbrPages");
      panel9.add(l_NbrPages);
      
      // ---- bt_Precedent ----
      bt_Precedent.setIcon(new ImageIcon(getClass().getResource("/images/fleche-erreur-a-gauche-precedente-icone-3917-16.png")));
      bt_Precedent.setToolTipText("Page pr\u00e9c\u00e9dente");
      bt_Precedent.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Precedent.setName("bt_Precedent");
      bt_Precedent.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_PrecedentActionPerformed(e);
        }
      });
      panel9.add(bt_Precedent);
      
      // ---- bt_Suivant ----
      bt_Suivant.setIcon(new ImageIcon(getClass().getResource("/images/fleche-a-cote-a-droite-icone-6873-16.png")));
      bt_Suivant.setToolTipText("Page suivante");
      bt_Suivant.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Suivant.setName("bt_Suivant");
      bt_Suivant.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_SuivantActionPerformed(e);
        }
      });
      panel9.add(bt_Suivant);
      
      // ---- label2 ----
      label2.setText("Zoom");
      label2.setName("label2");
      panel9.add(label2);
      
      // ---- sp_TaillePolice ----
      sp_TaillePolice.setToolTipText("Taille de la police");
      sp_TaillePolice.setName("sp_TaillePolice");
      sp_TaillePolice.addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent e) {
          sp_TaillePoliceStateChanged(e);
        }
      });
      panel9.add(sp_TaillePolice);
      
      // ---- hSpacer1 ----
      hSpacer1.setName("hSpacer1");
      panel9.add(hSpacer1);
      
      // ---- bt_ExportPDF ----
      bt_ExportPDF.setIcon(new ImageIcon(getClass().getResource("/images/Adobe Acrobat Reader_16.png")));
      bt_ExportPDF.setToolTipText("Exporter au format PDF");
      bt_ExportPDF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_ExportPDF.setName("bt_ExportPDF");
      bt_ExportPDF.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_ExportPDFActionPerformed(e);
        }
      });
      panel9.add(bt_ExportPDF);
    }
    contentPane.add(panel9, BorderLayout.NORTH);
    pack();
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane panel2;
  private JPanel panel8;
  private JPanel p_MargeHaute;
  private JPanel p_MargeDroite;
  private JPanel p_MargeGauche;
  private JPanel p_MargeBasse;
  private JTextArea ta_Page;
  private JPanel panel1;
  private JButton bt_Fermer;
  private JPanel panel9;
  private JLabel label1;
  private JLabel l_NbrPages;
  private JButton bt_Precedent;
  private JButton bt_Suivant;
  private JLabel label2;
  private JSpinner sp_TaillePolice;
  private JPanel hSpacer1;
  private JButton bt_ExportPDF;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
