package ri.seriem.libcommun.protocoleMsg;


public class BaseRequest
{
	// Constantes
	public static final int NOACTION=0;

	// Variables
	protected int actions=NOACTION;
	protected boolean success=false;
	protected String msgError=null;
	


	// --> Accesseurs <--------------------------------------------------------
	
	/**
	 * @return le action
	 */
	public int getActions()
	{
		return actions;
	}

	/**
	 * @param action le action � d�finir
	 */
	public void setActions(int actions)
	{
		this.actions = actions;
	}

	/**
	 * @return le success
	 */
	public boolean isSuccess()
	{
		return success;
	}

	/**
	 * @param success le success � d�finir
	 */
	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	/**
	 * @return le msgError
	 */
	public String getMsgError()
	{
		return msgError;
	}

	/**
	 * @param msgError le msgError � d�finir
	 */
	public void setMsgError(String msgError)
	{
		this.msgError = msgError;
	}
	
}
