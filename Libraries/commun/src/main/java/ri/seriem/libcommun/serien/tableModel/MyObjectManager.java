package ri.seriem.libcommun.serien.tableModel;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;


public class MyObjectManager
{
    PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private List<MyObject> objects = new ArrayList<MyObject>();

    public void addObject(MyObject object)
    {
        objects.add(object);
        object.setManager(this);
        propertyChangeSupport.firePropertyChange("objects", null, object);
    }

    public void removeObject(MyObject object)
    {
        objects.remove(object);
        propertyChangeSupport.firePropertyChange("manager", null, this);
    }

    public void removeObject(int rowIndex)
    {
    	MyObject object = objects.get(rowIndex);
        objects.remove(object);
        propertyChangeSupport.firePropertyChange("manager", null, this);
    }

    public List<MyObject> getObjects()
    {
        return objects;
    }

    // je ne vois pas � quoi �a sert -> si �a sert � donner un comportement de radiobutton au radiobutton 
    public void setAsSelected(MyObject myObject)
    {
        for (MyObject o : objects)
        {
        	//o.setManager(this); // TODO A voir si judicieux 
            o.setSelected(myObject == o);
        }
    }
}
