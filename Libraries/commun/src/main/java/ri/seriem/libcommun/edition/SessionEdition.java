/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.LogMessage;
import ri.seriem.libcommun.outils.communication.BufferManager;
import ri.seriem.libcommun.outils.communication.LinesManager;

/**
 * Menu session pour l'identification d'un utilisateur
 */
public class SessionEdition {
  // Constantes erreurs
  private final static String ERREUR_RENCONTREE = "Erreur pendant l'ex\u00e9cution : ";
  private final static String ERREUR_CONNEXION_IMPOSSIBLE = "Impossible de se connecter au serveur sur : ";
  private final static String ERREUR_ENVOI_SOCKET = "Probl\u00e8me lors de l'envoi du message sur la socket.";
  private final static String DECONNEXION_SESSION = "Fermeture de la session \u00e9dition.";
  
  // Constantes
  private final static String NOM_CLASSE = "[SessionEdition]";
  
  // Variables
  public static LinkedHashMapManager fileAttenteClient = null;
  
  private DataInputStream fromServer;
  private DataOutputStream toServer;
  private LogMessage log = null;
  private Socket soccli = null;
  private boolean isConnect = false;
  private LinesManager lineManager = null;
  private InetAddress iaserveurOndeEdition;
  private int portServeurEdition = ConstantesNewSim.SERVER_PORT;
  private BufferManager bufferToSend = new BufferManager();
  private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  /**
   * Constructeur (pour VeilleOutQ)
   * @param iaserveur
   * @param port
   * @param log
   */
  public SessionEdition(InetAddress iaserveur, int port, LogMessage log) {
    iaserveurOndeEdition = iaserveur;
    this.log = log;
  }
  
  /**
   * Constructeur
   */
  public SessionEdition(LinesManager almgr, LogMessage log, long aid, String adrSIM, int portSIM) {
    this(log, aid, adrSIM, portSIM);
    lineManager = almgr;
  }
  
  /**
   * Constructeur
   */
  public SessionEdition(LogMessage log, long aid, String adrSIM, int portSIM) {
    this.log = log;
    bufferToSend.setIdsession(aid);
    setServeurOndeEdition(adrSIM);
    setPortServeurEdition(portSIM);
  }
  
  // -- M�thodes priv�es ----------------------------------------------------
  
  /**
   * Lecture des octets re�us sur la Socket
   */
  private /*synchronized*/ byte[] lectureSocket() {
    int octet_alire = 0;
    int octet_lus = 0;
    int compteur = 0;
    byte buffercomplet[] = null;
    
    // On r�cup�re tous les octets envoy�s par le client
    try {
      octet_alire = fromServer.readInt();
      buffercomplet = new byte[octet_alire];
      do {
        octet_lus = fromServer.read(buffercomplet, compteur, octet_alire);
        compteur += octet_lus;
        octet_alire -= octet_lus;
      }
      while (octet_alire != 0);
    }
    catch (Exception e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
      return null;
    }
    
    return buffercomplet;
  }
  
  // -- M�thodes prot�g�es --------------------------------------------------
  
  // -- M�thodes publiques --------------------------------------------------
  
  /**
   * Initialise une nouvelle session
   */
  public int setSession() {
    // Connexion au serveur NewSim
    try {
      soccli = new Socket(iaserveurOndeEdition, portServeurEdition);
      
      // Cr�ation des �couteurs
      fromServer = new DataInputStream(new BufferedInputStream(soccli.getInputStream(), Constantes.TAILLE_BUFFER));
      toServer = new DataOutputStream(new BufferedOutputStream(soccli.getOutputStream(), Constantes.TAILLE_BUFFER));
      isConnect = true;
    }
    catch (IOException ioe) {
      // JOptionPane.showMessageDialog(this, ERREUR_CONNEXION_IMPOSSIBLE + infoUser.getServeurSGM(), SESSION_AUTHENTIFICATION,
      // JOptionPane.ERROR_MESSAGE);
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_CONNEXION_IMPOSSIBLE + portServeurEdition);
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ioe);
      return Constantes.ERREUR;
    }
    
    // Mise en place de l'�couteur de Socket
    Thread runner = new Thread() {
      int idmessage = 0;
      byte[] buffer = new byte[Constantes.TAILLE_BUFFER];
      String message;
      Demande demandeEdition = null;
      // String dossierRacineServeur="/sgm";
      
      public void run() {
        // TODO � mettre au dessus du while (� voir, erreur si arr�t du serveur)
        try {
          // Lecture continue
          while (isConnect) {
            idmessage = fromServer.readInt();
            switch (idmessage) {
              // Le client se connecte
              case ConstantesNewSim.CONNEXION_OK:
                isConnect = true;
                break;
              
              // Le client se d�connecte
              case Constantes.STOP:
                isConnect = false;
                closeConnexion();
                log.ecritureMessage(
                    NOM_CLASSE + Constantes.SEPARATEUR + "idS:" + bufferToSend.getIdsession() + " " + DECONNEXION_SESSION);
                break;
              
              // Le serveur nous renvoie une r�ponse ERREUR
              case Constantes.ERREUR:
                idmessage = fromServer.readInt();
                buffer = lectureSocket();
                message = new String(buffer, 0, buffer.length);
                log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "idS:" + bufferToSend.getIdsession() + " - Erreur:" + message);
                break;
              
              // Rec�ption d'un message du serveur d'�dition pour SGM
              case ConstantesNewSim.RECOIT_REQUETE:
                buffer = lectureSocket();
                message = new String(buffer, 0, buffer.length);
                EnvoiMessageClient(Constantes.ENVOI_REQUETE_EDITION, message);
                break;
              
              // Rec�ption d'un message du serveur d'�dition pour la surveillance des outqs
              case ConstantesNewSim.RECOIT_REQUETE_VEILLE:
                buffer = lectureSocket();
                message = new String(buffer, 0, buffer.length);
                demandeEdition = new Demande(Demande.BUFFER, message);
                if (fileAttenteClient != null) {
                  fileAttenteClient.removeObject(demandeEdition.getGenereClef());
                }
                break;
              
              // Valeur non trait�e
              default:
                break;
            }
          }
        }
        catch (IOException ioe) {
          if (log != null) {
            log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE);
            log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ioe);
            closeConnexion();
          }
        }
      }
    };
    runner.start();
    
    return Constantes.OK;
  }
  
  /**
   * Envoi un message au Serveur NewSim
   */
  public /*synchronized*/ int EnvoiMessageSocket(int idmessage, String message) {
    try {
      toServer.writeInt(idmessage);
      if (message != null) {
        toServer.writeInt(message.length());
        toServer.write(message.getBytes(/*infoUser.getCodepageServeur()*/), 0, message.length());
      }
      toServer.flush();
    }
    catch (Exception e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_ENVOI_SOCKET);
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + message);
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + e.getMessage());
      msgErreur = ERREUR_ENVOI_SOCKET;
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Envoi un message vers le client
   */
  public /*synchronized*/ int EnvoiMessageClient(int idmessage, String message) {
    if (lineManager == null) {
      return Constantes.ERREUR;
    }
    
    bufferToSend.setIdmessage(idmessage);
    bufferToSend.addBuffer(message);
    if (!lineManager.send(bufferToSend)) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + lineManager.getMsgErreur());
      msgErreur = ERREUR_ENVOI_SOCKET;
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Ferme les sockets
   */
  public void closeConnexion() {
    // Fermeture propre des connexions socket
    try {
      toServer.close();
      fromServer.close();
      // soccli.close(); Peut provoquer un probl�me avec CtrlSerieN
      soccli = null;
    }
    catch (IOException ioe) {
    }
  }
  
  /**
   * Indique si la session est connect�e ou pas
   */
  public boolean isConnected() {
    return isConnect;
  }
  
  /**
   * @param serveurOndeEdition the serveurOndeEdition to set
   */
  public void setServeurOndeEdition(String serveurOndeEdition) {
    try {
      iaserveurOndeEdition = InetAddress.getByName(serveurOndeEdition);
    }
    catch (UnknownHostException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Lib�re les ressources
   */
  public void dispose() {
    fromServer = null;
    toServer = null;
    log = null;
    soccli = null;
    lineManager = null;
    iaserveurOndeEdition = null;
    if (bufferToSend != null) {
      bufferToSend.dispose();
      bufferToSend = null;
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return the portServeurEdition
   */
  public int getPortServeurEdition() {
    return portServeurEdition;
  }
  
  /**
   * @param portServeurEdition the portServeurEdition to set
   */
  public void setPortServeurEdition(int portServeurEdition) {
    this.portServeurEdition = portServeurEdition;
  }
  
}
