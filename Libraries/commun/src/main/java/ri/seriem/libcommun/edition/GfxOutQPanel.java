/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Informations pour une imprimante connect�e � un System I contenu dans un panel
 */
public class GfxOutQPanel extends JPanel {
  // Variables
  private OutQ outq = null;
  private ActionListener action = null;
  private boolean modeSelection = false;
  
  /**
   * Constructeur
   * @param aprinter
   * @param modeselection
   */
  public GfxOutQPanel(OutQ aoutq, boolean modeselection) {
    super();
    modeSelection = modeselection;
    setOutq(aoutq);
    initComponents();
    if (modeselection) {
      setSelectionned(aoutq.isSelection());
    }
  }
  
  /**
   * Initialise les donn�es d'une imprimante
   * @param aprinter
   */
  public void setOutq(OutQ aoutq) {
    outq = aoutq;
  }
  
  /**
   * Retourne les donn�es d'une imprimante
   * @return
   */
  public OutQ getOutq() {
    return outq;
  }
  
  /**
   * Initialise l'action lorsque l'on clique sur le bouton Imprimer
   * @param aaction
   */
  public void setAction(ActionListener aaction) {
    action = aaction;
  }
  
  public void setSelectionned(boolean etat) {
    chk_Selectionner.setSelected(etat);
  }
  
  public boolean isSelectionned() {
    return chk_Selectionner.isSelected();
  }
  
  private void bt_ChoisirActionPerformed(ActionEvent e) {
    outq.setSelection(true);
    if (action != null) {
      action.actionPerformed(e);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    chk_Selectionner = new JCheckBox();
    bt_Choisir = new JButton();
    texte = new JLabel();
    description = new JLabel();
    
    // ======== this ========
    setPreferredSize(new Dimension(310, 28));
    setName("this");
    setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
    
    // ---- chk_Selectionner ----
    chk_Selectionner.setVisible(modeSelection);
    chk_Selectionner.setName("chk_Selectionner");
    add(chk_Selectionner);
    
    // ---- bt_Choisir ----
    bt_Choisir.setVisible(!modeSelection);
    bt_Choisir.setText("Choisir");
    bt_Choisir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_Choisir.setName("bt_Choisir");
    bt_Choisir.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        bt_ChoisirActionPerformed(e);
      }
    });
    add(bt_Choisir);
    
    // ---- texte ----
    texte.setPreferredSize(new Dimension(130, 16));
    texte.setName("texte");
    texte.setText(outq.getNom());
    add(texte);
    
    // ---- description ----
    description.setText("Description");
    description.setFont(description.getFont().deriveFont(Font.BOLD | Font.ITALIC));
    description.setName("description");
    description.setText(outq.getDescription());
    add(description);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox chk_Selectionner;
  private JButton bt_Choisir;
  private JLabel texte;
  private JLabel description;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
