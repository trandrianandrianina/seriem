package ri.seriem.libcommun.protocoleMsg;

import java.util.ArrayList;



public class SavfRequest extends BaseRequest
{
	// Constantes
	public static final int NOACTION=0;
	public static final int RESTORE=1;
	// Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
	//public static final int[] ACTIONS={NOACTION, RESTORE};
	public static final ArrayList<Integer> ACTIONS=new ArrayList<Integer>(){
		private static final long serialVersionUID = -5223936552676156713L;
		{ add(NOACTION); }
		{ add(RESTORE);  }
	};

	// Variables
	private String library_savf=null; 
	//private boolean exist=false;

	
	public SavfRequest()
	{}
	
	public SavfRequest(String alibrary_savf, int anactions)
	{
		setLibrary_savf(alibrary_savf);
		setActions(anactions);
	}


	// --> Accesseurs <--------------------------------------------------------
	
	/**
	 * @return le library_savf
	 */
	public String getLibrary_savf()
	{
		return library_savf;
	}

	/**
	 * @param library_savf le library_savf � d�finir
	 */
	public void setLibrary_savf(String library_savf)
	{
		this.library_savf = library_savf;
	}
	

}
