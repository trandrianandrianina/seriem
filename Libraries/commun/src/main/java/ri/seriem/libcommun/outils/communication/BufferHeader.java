package ri.seriem.libcommun.outils.communication;

import java.nio.ByteBuffer;

public class BufferHeader
{
	// Constantes
	public static final	int					LEN_INTEGER			= 4;
	public static final int					NBR_INTEGER			= 6;
	public static final	int					TYPE_BUFFER			= 0;
	public static final	int					SIZE_HEADER			= LEN_INTEGER * (NBR_INTEGER - 2); // Le type et la taille ne sont pas comptabilis�s
	
	
	// Variables
	private				int					type				= TYPE_BUFFER;		// Le type de buffer envoy�
	private				int					arraysize			= 0;				// La taille totale : header(sans type et taille) + body
	private				int					idgroup				= -1;				// l'ID de groupe (unique pour l'application)
	private				long				idsession			= -1;				// l'ID de session
	private				int					idmessage			= -1;				// l'ID du message
	protected 			ByteBuffer			bhead				= ByteBuffer.allocate(LEN_INTEGER * NBR_INTEGER);
	private				int					idpart1				= 0;
	private				int					idpart2				= 0;

	
	// -- M�thodes priv�es ----------------------------------------------------

	/**
	 * Retourne l'id assembl�
	 * @return
	 */
	private long computeId()
	{
		if( idpart1 != 0 )
			idsession = idpart1;
		else{
			idsession = (int)System.currentTimeMillis();
		}
			
		idsession = (idsession << 32) | idpart2;
		return idsession;
	}

	
	// -- M�thodes publiques --------------------------------------------------

	/**
	 * R�initialise l'ent�te
	 */
	public void reset()
	{
		bhead.clear();
		if( idsession == -1 ){
			computeId();
		}
	}

	/**
	 * Initialise l'ent�te du buffer
	 * 
	 */
	public void init()
	{
		reset();
		bhead.putInt(type);
		bhead.putInt(arraysize);
//System.out.println("-BufferHeader->init idgroup:" + idgroup+ " idsession" + idsession);		
		bhead.putInt(idgroup);
		bhead.putLong(idsession);
		bhead.putInt(idmessage);
	}

	/**
	 * Initialise l'idmessage et la taille du buffer dans l'ent�te courante
	 * 
	 */
	public void initIdMsgAndSize(int idmsg, int size)
	{
		setIdmessage(idmsg);
		setArraysize(size);
		bhead.position(LEN_INTEGER * 1);
		bhead.putInt(arraysize);
		bhead.position(LEN_INTEGER * 5);
		bhead.putInt(idmessage);
	}

	/**
	 * Retourne l'ent�te du buffer
	 * @return
	 */
	public byte[] getByte(){
		init();
		return bhead.array();
	}
	
	/**
	 * Lib�re les ressources
	 */
	public void dispose()
	{
		bhead.clear();
		bhead = null;
	}
	
	// -- Accesseurs ----------------------------------------------------------
	
	/**
	 * @return le idpart1
	 */
	public int getIdpart1()
	{
		return idpart1;
	}
	
	/**
	 * @param idpart1 le idpart1 � d�finir
	 */
	public void setIdpart1(int idpart1)
	{
		this.idpart1 = idpart1;
	}
	
	/**
	 * @return le idpart2
	 */
	public int getIdpart2()
	{
		return idpart2;
	}
	
	/**
	 * @param idpart2 le idpart2 � d�finir
	 */
	public void setIdpart2(int idpart2)
	{
		this.idpart2 = idpart2;
	}

	/**
	 * @return le idgroup
	 */
	public int getIdGroup()
	{
		return idgroup;
	}


	/**
	 * @param idgroup le idgroup � d�finir
	 */
	public void setIdGroup(int idgroup)
	{
		this.idgroup = idgroup;
	}


	/**
	 * @return le id
	 */
	public long getIdSession()
	{
		return idsession;
	}


	/**
	 * @param id le id � d�finir
	 */
	public void setIdSession(long id)
	{
		this.idsession = id;
	}


	/**
	 * @return le idmessage
	 */
	public int getIdmessage()
	{
		return idmessage;
	}

	/**
	 * @param idmessage le idmessage � d�finir
	 */
	public void setIdmessage(int idmessage)
	{
		this.idmessage = idmessage;
	}

	/**
	 * @return le arraysize
	 */
	public int getArraysize()
	{
		return arraysize;
	}

	/**
	 * @param arraysize le arraysize � d�finir
	 */
	public void setArraysize(int arraysize)
	{
		this.arraysize = arraysize + SIZE_HEADER;
	}

	/**
	 * @return le bentete
	 */
	public ByteBuffer getBentete()
	{
		return bhead;
	}

	/**
	 * @param bentete le bentete � d�finir
	 */
	public void setBentete(ByteBuffer bentete)
	{
		this.bhead = bentete;
	}
	
	
	
}
