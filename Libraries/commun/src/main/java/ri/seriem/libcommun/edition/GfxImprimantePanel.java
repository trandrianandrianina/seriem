/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.edition;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * Informations pour une imprimante connect�e � un System I contenu dans un panel
 */
public class GfxImprimantePanel extends JPanel {
  // Variables
  private Imprimante printer = null;
  private ActionListener action = null;
  private boolean modeSelection = false;
  
  /**
   * Constructeur
   * @param aprinter
   * @param modeselection
   */
  public GfxImprimantePanel(Imprimante aprinter, boolean modeselection) {
    super();
    modeSelection = modeselection;
    setImprimante(aprinter);
    initComponents();
    if (modeselection) {
      setSelectionned(aprinter.isSelection());
    }
  }
  
  /**
   * Initialise les donn�es d'une imprimante
   * @param aprinter
   */
  public void setImprimante(Imprimante aprinter) {
    printer = aprinter;
  }
  
  /**
   * Retourne les donn�es d'une imprimante
   * @return
   */
  public Imprimante getImprimante() {
    return printer;
  }
  
  /**
   * Initialise l'action lorsque l'on clique sur le bouton Imprimer
   * @param aaction
   */
  public void setAction(ActionListener aaction) {
    action = aaction;
  }
  
  public void setSelectionned(boolean etat) {
    chk_Selectionner.setSelected(etat);
  }
  
  public boolean isSelectionned() {
    return chk_Selectionner.isSelected();
  }
  
  /**
   * Affiche l'�tat de l'imprimante
   */
  private void setEtatPrinter() {
    if (printer.getStatus() == Imprimante.STARTED) {
      icone.setIcon(new ImageIcon(getClass().getResource("/images/puce_verte-16.png")));
      icone.setToolTipText("L'imprimante est d�marr�");
    }
    else {
      icone.setIcon(new ImageIcon(getClass().getResource("/images/puce_rouge-16.png")));
      icone.setToolTipText("L'imprimante est arr�t�");
    }
  }
  
  private void bt_ImprimerActionPerformed(ActionEvent e) {
    printer.setSelection(true);
    printer.setNbrexemplaire((Integer) exemplaire.getValue());
    if (action != null) {
      action.actionPerformed(e);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    chk_Selectionner = new JCheckBox();
    bt_Imprimer = new JButton();
    texte = new JLabel();
    exemplaire = new JSpinner();
    icone = new JLabel();
    
    // ======== this ========
    setPreferredSize(new Dimension(310, 28));
    setName("this");
    setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
    
    // ---- chk_Selectionner ----
    chk_Selectionner.setVisible(modeSelection);
    chk_Selectionner.setName("chk_Selectionner");
    add(chk_Selectionner);
    
    // ---- bt_Imprimer ----
    bt_Imprimer.setVisible(!modeSelection);
    bt_Imprimer.setText("Imprimer");
    bt_Imprimer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_Imprimer.setName("bt_Imprimer");
    bt_Imprimer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        bt_ImprimerActionPerformed(e);
      }
    });
    add(bt_Imprimer);
    
    // ---- texte ----
    texte.setPreferredSize(new Dimension(115, 16));
    texte.setName("texte");
    texte.setText(printer.getNom());
    add(texte);
    
    // ---- exemplaire ----
    exemplaire.setVisible(!modeSelection);
    exemplaire.setPreferredSize(new Dimension(50, 28));
    exemplaire.setModel(new SpinnerNumberModel(1, 1, null, 1));
    exemplaire.setToolTipText("Nombre d'exemplaires souhait\u00e9s");
    exemplaire.setName("exemplaire");
    add(exemplaire);
    
    // ---- icone ----
    setEtatPrinter();
    icone.setName("icone");
    add(icone);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox chk_Selectionner;
  private JButton bt_Imprimer;
  private JLabel texte;
  private JSpinner exemplaire;
  private JLabel icone;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
