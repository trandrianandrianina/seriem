/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.libcommun.outils;

import java.awt.Color;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

/**
 * Liste des constantes g�n�riques utilis�es dans le projet
 */
public class Constantes {
  // G�n�riques
  public final static int ON = 1;
  public final static int OFF = 0;
  public final static int OK = 0;
  public final static int ERREUR = -1;
  public final static int TRUE = 0;
  public final static int FALSE = -1;
  public final static int TRAITEE = -2;
  public final static int EN_COURS_TRAITEMENT = -3;
  public final static int BOTH = 3;
  public final static int HORIZONTAL = 2;
  public final static int VERTICAL = 1;
  public final static int NONE = -1;
  public final static int LEFT = 0;
  public final static int CENTER = 1;
  public final static int RIGHT = 2;
  public final static int TOP = 3;
  public final static int BOTTOM = 4;
  public final static int TAILLE_BUFFER = 32768;
  public final static int DATAQLONG = 5120;// 4096;
  public final static int MAXVAR = 1000;
  public final static int DEBUG = TRUE;
  public final static String SEPARATEUR = "\t";
  public final static String SEPARATEUR_CHAINE = "|";
  public final static String SEPARATEUR_DOSSIER = "&";
  public final static char SEPARATEUR_CHAR = '\t';
  public final static char SEPARATEUR_CHAINE_CHAR = '|';
  public final static char SEPARATEUR_DOSSIER_CHAR = '&';
  // public final static String CODEPAGE = "Cp1252"; //"ISO-8859-1";//"UTF-8";
  public final static String COD_CRLF = "#CRLF#";
  public final static String crlf = System.getProperty("line.separator");
  public final static char[] SAUTLIGNE_CHAR = crlf.toCharArray();
  public final static String SAUTPAGE = "�&&";
  public final static char[] SAUTPAGE_CHAR = SAUTPAGE.toCharArray();
  
  public final static char SEPARATEUR_DECIMAL_CHAR = ',';
  public final static String SEPARATEUR_DECIMAL = ",";
  public final static char SEPARATEUR_MILLIER_CHAR = '.';
  public final static String SEPARATEUR_MILLIER = ".";
  public final static int IMG_SYS = 0;
  public final static int IMG_EQU = 1;
  public final static int IMG_VAR = 2;
  public final static int PLAIN = 0;
  public final static int BOLD = 1;
  public final static int ITALIC = 2;
  public final static String THUMBAIL = "p_";
  public final static char SEPARATEUR_DATE_CHAR = '/';
  
  public final static char TYPE_ERR_SIMPLE = ' ';
  public final static char TYPE_WRNG_SIMPLE = 'W';
  // public final static char TYPE_ERR_COMPLEXE=' ';
  
  // Num�ros des requ�tes
  public final static int STOP = 0;
  public final static int CONNEXION_OK = 1;
  public final static int DEMANDE_OUVERTURE_PANEL_SESSION = 2;
  public final static int DEMANDE_LISTE_RT = 3;
  public final static int ENVOI_LISTE_RT = 4;
  public final static int DEMANDE_FICHIER_RT = 5;
  public final static int ENVOI_FICHIER_RT = 6;
  public final static int RECOIT_FICHIER_RT = ENVOI_FICHIER_RT;
  public final static int ENVOI_BUFFER_GFX = 7;
  public final static int RECOIT_BUFFER_GFX = ENVOI_BUFFER_GFX;
  public final static int ENVOI_BUFFER_RPG = 8;
  public final static int RECOIT_BUFFER_RPG = ENVOI_BUFFER_RPG;
  public final static int DEMANDE_LISTE_RTCONFIG = 9;
  public final static int ENVOI_LISTE_RTCONFIG = 10;
  public final static int OUVERTURE_SESSION_OK = 11;
  public final static int DEMANDE_DEMARRE_SESSION = 12;
  public final static int DEMANDE_OUVERTURE_TRANSFERT_SESSION = 19;
  public final static int FORCE_AFFICHE_PANEL = 20;
  public final static int TRF_LIGNE_FIC_CSV = 21;
  public final static int ENVOI_ENREGISTREMENT_MENU = 22;
  public final static int RECOIT_ENREGISTREMENT_MENU = ENVOI_ENREGISTREMENT_MENU;
  public final static int RECOIT_CHOIX_MENU = 23;
  public final static int ENVOI_CHOIX_MENU = RECOIT_CHOIX_MENU;
  public final static int DEMANDE_STOP_SESSION = 24;
  public final static int RECOIT_INFOS_USER = 25;
  public final static int ENVOI_INFOS_USER = RECOIT_INFOS_USER;
  public final static int DEMANDE_OUVERTURE_MIRE_SESSION = 26;
  public final static int DEMANDE_FICHIER = 27;
  public final static int ENVOI_FICHIER = 28;
  public final static int RECOIT_FICHIER = ENVOI_FICHIER;
  public final static int DEMANDE_CONTENU_DOSSIER = 29;
  public final static int ENVOI_CONTENU_DOSSIER = 30;
  public final static int RECOIT_CONTENU_DOSSIER = ENVOI_CONTENU_DOSSIER;
  public final static int DEMANDE_PROTOCOLE_SERVEUR = 31;
  public final static int RECOIT_PROTOCOLE_SERVEUR = DEMANDE_PROTOCOLE_SERVEUR;
  public final static int ENVOI_DSPF = 32;
  public final static int RECOIT_DSPF = ENVOI_DSPF;
  public final static int ENVOI_REQUETE_EDITION = 33;
  public final static int RECOIT_REQUETE_EDITION = ENVOI_REQUETE_EDITION;
  public final static int PING = 34; // Permet de tester la validit� d'une connexion de la part du serveur
  public final static int PONG = PING;
  public final static int UPLOAD_FICHIER = 35;
  public final static int SUPPRIME_FICHIER = 36;
  public final static int ENVOI_REQUETE_SYSTEME = 37;
  public final static int RECOIT_REQUETE_SYSTEME = ENVOI_REQUETE_SYSTEME;
  public final static int NOTIFICATION_FROM_RPG = 38;
  
  public final static int ANALYSE_DSPF = 1000000;
  public final static int HTTP_GET = 1195725856;
  public final static int HTTP_POST = 1347375956;
  public final static int HTTP_HEAD = 1212498244;
  
  // Code message DTAQ
  public final static String RPGMSG_BUFFER = "00001";
  public final static String RPGMSG_BUFFER_FRC = "00002";
  public final static String RPGMSG_CSV_TITLE = "10001";
  public final static String RPGMSG_CSV_DETAIL = "10002";
  public final static String RPGMSG_CSV_END = "10003";
  public final static String RPGMSG_MNU_GRP = "20001";
  public final static String RPGMSG_MNU_GRP_END = "20011";
  public final static String RPGMSG_MNU_MOD = "20002";
  public final static String RPGMSG_MNU_MOD_END = "20012";
  public final static String RPGMSG_MNU_REFRESH = "20013";
  public final static String RPGMSG_USR_INFOS = "20000";
  public final static String RPGMSG_NOTIFICATION = "30000";
  public final static String RPGMSG_ENDJOB = "99999";
  
  // Codes notification
  public final static int NOTIF_DLFILE = 1;
  
  // Propres au projet
  public final static String MAILTO = "assistance@resolution-informatique.com";
  public final static int SOCKET_TIMEOUT = 0; // en ms = 1H si =0 alors ne coupe jamais
  public final static int DATAQ_TIMEOUT = 3600000;
  public final static int MAXRECONNEXION = 1000;
  
  public final static String VERSION_PROTOCOLE = "01.02";
  public final static String VERSION_SERVER = VERSION_PROTOCOLE + ".02";
  public final static String VERSION_CLIENT = VERSION_PROTOCOLE + ".02";
  public final static int SERVER_PORT = 8888;
  public final static String INIT_CONFIG = "config.equ";
  public final static String INIT_LIBIMG = "libsimages.equ";
  public final static String INIT_TRADUCTION = "traduction_fr.equ";
  public final static String DOSSIER_IMAGE = "images";
  public final static String DOSSIER_RACINE = "serien";
  public final static String DOSSIER_LOG = "logs";
  public final static String DOSSIER_LIB = "lib";
  public final static String DOSSIER_RT = "rt";
  public final static String DOSSIER_WEB = "www";
  public final static String DOSSIER_TMP = "tmp";
  public final static String DOSSIER_CONFIG = "config";
  public final static String DOSSIER_CTRL = "ctrl";
  public final static String DOSSIER_OTHERS = "others";
  public final static String DOSSIER_METIER = "metier";
  public final static String DOSSIER_MENUS = "menus";
  public final static String FIC_CONFIG = "rt.ini";
  
  public final static String FICHIER_ID = "/console/conf.xml";
  
  // Couleurs d�finies dans le nouveau look.
  public final static Color COULEUR_CONTENU = new Color(239, 239, 222);
  public final static Color COULEUR_POP_CONTENU = new Color(238, 238, 210);
  public final static Color COULEUR_MENUS = new Color(238, 239, 241);
  public final static int[] COULEUR_CENTRAGE_FONCE = { 90, 90, 90 };
  public final static int[] COULEUR_CENTRAGE_CLAIR = { 198, 198, 200 };
  public final static Color COULEUR_F1 = new Color(90, 90, 90);
  public final static Color COULEUR_ERREURS = new Color(106, 23, 21);
  public final static Color COULEUR_NEGATIF = new Color(204, 0, 0);
  public final static Color COULEUR_LISTE_LETTRAGE = new Color(160, 70, 160);
  public final static Color COULEUR_LISTE_COMMENTAIRE = new Color(0, 102, 255);
  public final static Color COULEUR_LISTE_ANNULATION = new Color(140, 140, 140);
  public final static Color COULEUR_LISTE_FOND_BILAN = new Color(57, 105, 138);
  public final static Color COULEUR_LISTE_FOND_COMMENTAIRE = new Color(230, 240, 250);
  public final static Color COULEUR_LISTE_FOND_ANNULATION = new Color(250, 210, 210);
  public final static Color COULEUR_LISTE_BILAN = Color.white;
  public final static Color COULEUR_LISTE_ERREUR = new Color(255, 44, 47);
  public final static Color COULEUR_LISTE_GRATUIT = new Color(55, 80, 50);
  public final static Color COULEUR_TEXTE_DESACTIVE = new Color(77, 77, 80);
  public final static Color CL_DEGRADE_CLAIR = new Color(198, 198, 198);
  public final static Color CL_DEGRADE_FONCE = new Color(130, 130, 130);
  public final static Color CL_BT_CLAIR = new Color(235, 235, 235);
  public final static Color CL_BT_FONCE = new Color(195, 195, 195);
  public final static Color CL_BT_BORD = new Color(95, 95, 95);
  public final static Color CL_ZONE_SORTIE = new Color(243, 243, 236);
  public final static Color CL_TEXT_SORTIE = new Color(0, 0, 0);
  public final static Color CL_BANDEAUF_COMPTA = new Color(128, 204, 40);
  public final static Color CL_BANDEAUF_GESCOM = new Color(35, 93, 193); // (56, 113, 193)
  public final static Color CL_BANDEAUF_ACHAT = new Color(80, 173, 229);
  public final static Color CL_BANDEAUF_EXPLOITATION = new Color(204, 227, 16);
  public final static Color CL_BANDEAUF_DEFAUT =
      new Color(COULEUR_CENTRAGE_CLAIR[0], COULEUR_CENTRAGE_CLAIR[1], COULEUR_CENTRAGE_CLAIR[2]);
  public final static Color CL_BANDEAU_TEXTE = Color.BLACK;
  
  // Liste contenant les correspondances des codes couleurs utilis�s notamment dans les listes
  // Voir la doc http://88.188.171.20/documentation/?p=6417190 pour la mise � jour
  // l'ordre est couleur du texte puis couleur du fond
  public static final HashMap<String, Color> CORRESPONDANCE_COULEURS = new HashMap<String, Color>() {
    {
      put("PK", new Color(254, 120, 180));
    }
    
    {
      put("RD", new Color(205, 20, 20));
    }
    
    {
      put("TQ", new Color(211, 240, 254));
    }
    
    {
      put("YL", new Color(240, 200, 80));
    }
    
    {
      put("BL", new Color(70, 70, 255));
    }
    
    {
      put("GR", new Color(0, 130, 70));
    }
    
    {
      put("OR", new Color(255, 140, 20));
    }
    
    {
      put("WH", new Color(255, 255, 255));
    }
    
  };
  
  // Num�ro des requ�tes trait�es par la classe TraitementRequeteSystemeMetier
  public final static int ID_TEXTTITLEBARINFORMATIONS = 1;
  
  public final static String EXT_BIN = ".dat";
  
  // polices de XRITABLE non utilis�es car le fichier Constantes est dans le projet riComposants
  // public final static Font POLICE_HEAD_LIST = new Font("Courrier New", Font.PLAIN, 12);
  // public final static Font POLICE_LIST = new Font("Courrier New", Font.PLAIN, 12);
  // public final static Font POLICE_HEAD_LIST = new Font("SansSerif", Font.PLAIN, 12);
  // public final static Font POLICE_LIST = new Font("SansSerif", Font.PLAIN, 12);
  // public final static Font POLICE_HEAD_LIST = CustomFont.loadFont("fonts/VeraMono.ttf", 12); //new Font("Monospaced", Font.PLAIN, 12);
  // // Ne pas mettre Courier New car pose soucis dans les listes
  // public final static Font POLICE_LIST = CustomFont.loadFont("fonts/VeraMono.ttf", 12); // notamment VCGM73
  
  // Caract�res interdits pour les noms de fichers
  public static char[] caracteresNonAutorisesPourFichier = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };
  
  // Variables pour la conversion des caract�res unicodes
  private static String code[] = { "\\u00e9", "\\u00e8", "\\u00e0", "\\u00f4", "\\u00e2", "\\u00e4", "\\u00e7", "\\u00ea", "\\u00eb",
      "\\u00ee", "\\u00ef", "\\u00f6", "\\u00f9", "\\u00fb", "\\u00fc", "\\u00b0", "\\u00f3", "\\u20ac" };
  private static String lettre[] = { "\u00e9", "\u00e8", "\u00e0", "\u00F4", "\u00e2", "\u00e4", "\u00e7", "\u00ea", "\u00eb", "\u00ee",
      "\u00ef", "\u00f6", "\u00f9", "\u00fb", "\u00fc", "\u00b0", "\u00f3", "\u20ac" };
  private static String lettreori[] = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�" };
  public static char[] spaces = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', // 260 espaces
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', };
  
  /**
   * Conversion des caract�res unicodes
   * @param liste
   * @return
   */
  public static StringBuffer code2unicode(StringBuffer bchaine) {
    // Analyse de la liste
    for (int j = 0; j < code.length; j++) {
      int k = bchaine.indexOf(code[j]);
      while (k != -1) {
        bchaine.replace(k, k + 6, lettre[j]);
        k = bchaine.indexOf(code[j], k);
      }
    }
    return bchaine;
  }
  
  /**
   * Conversion des caract�res unicodes
   * @param liste
   * @return
   */
  public static String[] code2unicode(String liste[]) {
    // Analyse de la liste
    for (int i = 0; i < liste.length; i++) {
      liste[i] = code2unicode(new StringBuffer(liste[i])).toString();
    }
    return liste;
  }
  
  /**
   * Conversion des caract�res unicodes
   * @param chaine
   * @return
   */
  public static String code2unicode(String chaine) {
    // Analyse da la chaine
    return code2unicode(new StringBuffer(chaine)).toString();
  }
  
  /**
   * TODO renommer la m�thode en ascii2code ou mieux
   * Conversion des caract�res ascii en code
   * @param chaine
   * @return
   */
  public static StringBuffer lettreori2code(StringBuffer bchaine) {
    // Analyse da la chaine
    for (int j = 0; j < code.length; j++) {
      int k = bchaine.indexOf(lettreori[j]);
      while (k != -1) {
        bchaine.replace(k, k + 1, code[j]);
        k = bchaine.indexOf(lettreori[j], k);
      }
    }
    
    return bchaine;
  }
  
  /**
   * TODO renommer la m�thode en ascii2code ou mieux
   * Convertion des caract�res ascii en code
   * @param chaine
   * @return
   */
  public static String lettreori2code(String chaine) {
    return lettreori2code(new StringBuffer(chaine)).toString();
  }
  
  /**
   * Retourne une liste g�n�r� depuis une chaine
   */
  public static int[] splitString2Int(String chaine, char separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    int[] liste = new int[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = Integer.parseInt(vlisting.get(i));
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste g�n�r� depuis une chaine
   */
  public static String[] splitString(String chaine, char separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    String[] liste = new String[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = (String) vlisting.get(i);
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste g�n�r� depuis une chaine
   */
  public static String[] splitString(String chaine, String separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    String[] liste = new String[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = (String) vlisting.get(i);
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste g�n�r� depuis une chaine
   */
  public static ArrayList<String> splitString2List(String chaine, char separateur) {
    int position = 0;
    int i = 0;
    ArrayList<String> vlisting = new ArrayList<String>(50);
    
    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + 1;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));
    
    return vlisting;
  }
  
  /**
   * Retourne une liste g�n�r� depuis une chaine
   */
  public static ArrayList<String> splitString2List(String chaine, String separateur) {
    int position = 0;
    int i = 0;
    int lg_sep = separateur.length();
    ArrayList<String> vlisting = new ArrayList<String>(50);
    
    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + lg_sep;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));
    
    return vlisting;
  }
  
  /**
   * Conversion des couleurs RGB en entier
   * @param r,g,b
   * @return
   */
  public static int RGB2int(int r, int g, int b) {
    return (r << 16) + (g << 8) + b;
  }
  
  /**
   * Affichage d'un tableau de byte sous forme de chaine hexad�cimale
   * @param buf
   * @return
   */
  public static String hexString(byte[] buf) {
    char[] TAB_BYTE_HEX = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    
    StringBuffer sb = new StringBuffer(buf.length * 2);
    
    for (int i = 0; i < buf.length; i++) {
      sb.append(TAB_BYTE_HEX[(buf[i] >>> 4) & 0xf]);
      sb.append(TAB_BYTE_HEX[buf[i] & 0x0f]);
    }
    return sb.toString();
  }
  
  /**
   * Permet de mettre le look du syst�me par d�fault
   * @param panel
   */
  public static void setLook(String lf) {
    // Ou en argument sur la ligne de commande
    // java -Dswing.defaultlaf=com.sun.java.swing.plaf.windows.WindowsLookAndFeel ...
    // -> javax.swing.plaf.metal.MetalLookAndFeel
    // -> com.sun.java.swing.plaf.motif.MotifLookAndFeel
    // -> com.sun.java.swing.plaf.windows.WindowsLookAndFeel (� priori bug sous Vista avec les JFilechooser [6.03])
    
    // FIXME Probl�me avec Vista au niveau des JFileChooser (ca crache) si execution depuis Eclipse
    // par contre en ligne de commande aucun pb
    // if ((System.getProperty("os.name") + " " +System.getProperty ( "os.version" )).trim().equals("Windows XP 5.1"))
    // if ((lf == null) || ((lf != null) && (lf.equals("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"))))
    // return;
    
    if (lf != null) {
      try {
        UIManager.setLookAndFeel(lf);
      }
      catch (Exception ex) {
      }
    }
    else {
      if (!System.getProperty("os.name").equalsIgnoreCase("Linux")) {
        try {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
      // Sinon on enleve le gras de la police pour le th�me Metal
      else {
        UIManager.put("swing.boldMetal", Boolean.FALSE);
      }
    }
  }
  
  /**
   * Retourne la date avec gestion d'un d�lai en jour (en plus ou moins)
   * @param delai
   * @return
   */
  public static int getDate(int delai) {
    Calendar c = GregorianCalendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.DAY_OF_MONTH, delai);
    
    return c.get(Calendar.YEAR) * 10000 + (c.get(Calendar.MONTH) + 1) * 100 + c.get(Calendar.DAY_OF_MONTH);
  }
  
  /**
   * Retourne la date
   * @return
   */
  public static int getDate() {
    return getDate(0);
  }
  
  /**
   * Retourne l'heure avec gestion d'un d�lai en seconde (en plus ou moins)
   * @param delai
   * @return
   */
  public static int getHeure(int delai) {
    Calendar c = GregorianCalendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.SECOND, delai);
    
    return c.get(Calendar.HOUR_OF_DAY) * 10000 + (c.get(Calendar.MINUTE) + 1) * 100 + c.get(Calendar.SECOND);
  }
  
  /**
   * Retourne l'heure
   * @return
   */
  public static int getHeure() {
    return getHeure(0);
  }
  
  /**
   * Supprime les fichiers de log trop anciens
   * @param dossier
   * @param tpsconser
   */
  public static void SuppressionLogMessage(String dossier, int tpsconser) {
    int i = 0;
    if (dossier == null) {
      return;
    }
    
    // Construction du nom du fichier de log
    int intfichier = getDate(-tpsconser);
    
    File dossierlog = new File(dossier);
    if (!dossierlog.exists()) {
      return;
    }
    
    // On liste le contenu du dossier & on le tri
    File[] listefichierlog = dossierlog.listFiles();
    int[] listefichierlogint = new int[listefichierlog.length];
    for (i = 0; i < listefichierlog.length; i++) {
      if (listefichierlog[i].isFile()) {
        try {
          listefichierlogint[i] = Integer.parseInt(listefichierlog[i].getName().substring(0, 8));
        }
        catch (Exception e) {
        }
      }
    }
    // On supprime les fichiers de log sup�rieurs au d�lai
    i = 0;
    while (i < listefichierlog.length) {
      if (intfichier >= listefichierlogint[i]) {
        listefichierlog[i].delete();
      }
      i++;
    }
  }
  
  /**
   * Retourne le dossier racine du serveur S�rie N en cours d'ex�cution
   * @return
   */
  public static String getApplicationRootDirectory(final Class<?> classe) {
    String u = classe.getResource('/' + classe.getName().replace('.', '/') + ".class").toString();
    int deb = u.lastIndexOf(':');
    int fin = u.indexOf(Constantes.DOSSIER_RT);
    return u.substring(deb + 1, fin - 1);
  }
  
  /**
   * Retourne le dossier home de l'utilisateur
   * (quelques soit le syst�me et surtout la configuration de Windows)
   * @return
   */
  public static String getUserHome() {
    String home = System.getenv("USERPROFILE");
    if (home == null) {
      return System.getProperty("user.home");
    }
    return home;
  }
  
  /**
   * Retourne les informations sur la consommation m�moire
   * @return
   */
  public static String getInfosMemory() {
    StringBuffer sb = new StringBuffer();
    long totalmem = Runtime.getRuntime().totalMemory();
    sb.append(((totalmem - Runtime.getRuntime().freeMemory()) / (1024 * 1024))).append(" de ").append(totalmem / (1024 * 1024))
        .append(" Mo / ").append(Runtime.getRuntime().maxMemory() / (1024 * 1024)).append(" Mo max");
    return sb.toString();
  }
  
  /**
   * Analyse et r�cup�re les param�tres contenus dans une chaine
   * @param ligne
   * @param parametres
   */
  public static void analyseParametres(String ligne, HashMap<String, String> parametres) {
    if ((ligne == null) || (ligne.trim().equals(""))) {
      return;
    }
    
    ligne = ligne.trim();
    String[] liste = ligne.split(" ");
    String key = null;
    String value = null;
    int pos = -1;
    for (String chaine : liste) {
      if (chaine.startsWith("-")) {
        pos = chaine.indexOf('=');
        if (pos != -1) {
          key = chaine.substring(1, pos);
          value = chaine.substring(pos + 1);
          if (value.charAt(0) != '"') {
            parametres.put(key, value);
            key = null;
            value = null;
          }
          else if (value.charAt(value.length() - 1) == '"') {
            parametres.put(key, value.substring(1, value.length() - 1));
            key = null;
            value = null;
          }
        }
        else {
          key = chaine.substring(1);
          value = "true";
          parametres.put(key, value);
          key = null;
          value = null;
        }
      }
      else if (key != null) {
        value += " " + chaine;
        if (value.charAt(value.length() - 1) == '"') {
          parametres.put(key, value.substring(1, value.length() - 1));
          key = null;
          value = null;
        }
      }
    }
  }
  
  /**
   * Retourne s'il s'agit de l'OS400
   * @return
   */
  public static boolean isOs400() {
    return System.getProperty("os.name").toLowerCase().equalsIgnoreCase("OS/400");
  }
  
  /**
   * Retourne s'il s'agit de Windows
   * @return
   */
  public static boolean isWindows() {
    return (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0);
  }
  
  /**
   * Retourne s'il s'agit de Os X
   * @return
   */
  public static boolean isMac() {
    return (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0);
  }
  
  /**
   * Retourne s'il s'agit d'unix
   * @return
   */
  public static boolean isUnix() {
    String OS = System.getProperty("os.name").toLowerCase();
    return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
  }
  
  /**
   * Tester l'�galit� entre deux objets en g�rant correctement les valeurs null.
   * Si la deux valeurs pass�es sont null, la m�tohde retourne true.
   * Si les deux valeurs, sont des BigDecimal, cela compare les valeurs.
   */
  public static boolean equals(Object pObject1, Object pObject2) {
    if (pObject1 == null) {
      return pObject2 == null;
    }
    else if (pObject1 instanceof BigDecimal && pObject2 instanceof BigDecimal) {
      return ((BigDecimal) pObject1).compareTo((BigDecimal) pObject2) == 0;
    }
    else {
      return pObject1.equals(pObject2);
    }
  }
  
  /**
   * Mettre la premi�re lettre d'une cha�ne de caract�re en majuscule et les autres en minuscules.
   *
   * @param pTexte Texte dont il faut mettre la premi�re lettre en majuscules.
   * @return Texte transform�.
   */
  public static String capitaliserPremiereLettre(String pTexte) {
    if (pTexte == null) {
      return null;
    }
    else if (pTexte.isEmpty()) {
      return "";
    }
    else if (pTexte.length() == 1) {
      return pTexte.substring(0, 1).toUpperCase();
    }
    else {
      return pTexte.substring(0, 1).toUpperCase() + pTexte.substring(1).toLowerCase();
    }
  }
  
  /**
   * Permet de normaliser le contenu d'une cha�ne de texte saisie.
   * Remplacer null par une cha�ne vide, enlever les espaces, g�rer les caract�res sp�ciaux.
   */
  public static String normerTexte(String pTexte) {
    if (pTexte == null) {
      return "";
    }
    return pTexte.trim();
  }
  
  /**
   * Forcer le look et feel Nimbus.
   */
  public static void forcerLookAndFeelNimbus() {
    // On force le L&F Nimbus s'il est disponible
    try {
      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la mise en place du look&feel Nimbus.");
    }
  }
  
}
