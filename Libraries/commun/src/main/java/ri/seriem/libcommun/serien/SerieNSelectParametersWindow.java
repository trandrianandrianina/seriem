//=================================================================================================
//==>                                                                       25/11/2013 - 25/07/2014
//==> Fen�tre de s�lection des param�tres
//==> A faire:
//==> A savoir:
//=================================================================================================
package ri.seriem.libcommun.serien;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.seriem.libcommun.serien.tableModel.MyObject;
import ri.seriem.libcommun.serien.tableModel.ParametersTableModel;
import ri.seriem.libcommun.serien.tableModel.RadioButtonCellEditorRenderer;


public class SerieNSelectParametersWindow extends JDialog
{
	// Constantes
	private static final long serialVersionUID = 1L;
	
	public static final int NOTHING=0;
	public static final int SAVE=1;
	public static final int CONNECT=2;
	public static final int SAVE_CONNECT=3;
	
	private static final Color ALTER1 = new Color(222,222,222);
	private static final Color ALTER2 = new Color(250,250,250);

	// Variables
	private JTable Liste;
	private ArrayList<SerieNParameters> listParameters=null;
	private int codeOperation=0;
	private boolean modif=false;

	
	public SerieNSelectParametersWindow(ArrayList<SerieNParameters> alistParameters)
	{
		initComponents();
	
		listParameters = alistParameters;
		initWindow();
	}

	/**
	 * Retourne les param�tres s�lectionn�s
	 * @return
	 */
	public SerieNParameters getParametersSelected()
	{
		if (listParameters.size() == 1)
			return listParameters.get(0);
		
		int index = Liste.getSelectedRow();
		if (index == -1)
		{
			// Recherche les param�tres par d�faut
			for (int i=0; i<listParameters.size(); i++)
				if(listParameters.get(i).isDefault_param())
					return listParameters.get(i);
			return null;
		}
		return listParameters.get(index);
	}
	
	/**
	 * Retourne le code op�ration qui indique l'op�ration � effectuer
	 * @return
	 */
	public int getCodeOperation()
	{
		return codeOperation;
	}
	
	/**
	 * Met � jour le texte du message d'erreur
	 * @param msg
	 */
	public void setErrorMsg(String msg)
	{
		if (msg == null)
			l_Erreur.setText("");
		else
			l_Erreur.setText(msg);
		validate();
		repaint();
	}
	
	// -- M�thodes priv�es ----------------------------------------------------
	
	/**
	 * Initialise les donn�es de la fen�tre avec la listes des param�tres
	 */
	private void initWindow()
	{
		if ((listParameters == null) && (listParameters.size() == 0)) return;
		
		// Chargement de la liste
		int index = -1;
		//MyObjectManager manager = new MyObjectManager();
		ParametersTableModel model = new ParametersTableModel();
		for (int i=0; i<listParameters.size(); i++)
        {
//System.out.println("--> " + listParameters.get(i).getName());			
            MyObject object = new MyObject(listParameters.get(i).getName(), listParameters.get(i).getHost());
            model.getMyObjectManager().addObject(object);
            object.setSelected(listParameters.get(i).isDefault_param());
            if (object.isSelected())
            	index = i;
        }
		initTable(model);
		if (index != -1) Liste.getSelectionModel().addSelectionInterval(index, index); 


		// Le bouton bt_Connexion re�oit le focus et devient la touche par d�faut
		//bt_Connexion.requestFocusInWindow();
		getRootPane().setDefaultButton(bt_Connexion);
	}
	
	/**
	 * Initialise la table
	 * @param manager
	 */
	@SuppressWarnings("serial")
	private void initTable(ParametersTableModel model)
	{
		Liste = new JTable(model)
        {
			@Override
        	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        	Component comp = super.prepareRenderer(renderer, row, column);
           	if (!isRowSelected(row))
           		comp.setBackground(row % 2 == 0 ? ALTER1 : ALTER2);
        	return comp;
        	}
        };
        Liste.setRowHeight(Liste.getRowHeight()+2);
        Liste.setFocusable(false);
		Liste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Liste.setName("Liste");
		Liste.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ListeMouseClicked(e);
			}
		});
        
        TableColumnModel cm = Liste.getColumnModel();
		cm.getColumn(0).setResizable(false);
		cm.getColumn(0).setPreferredWidth(150);
		cm.getColumn(1).setResizable(false);
		cm.getColumn(1).setPreferredWidth(240);
		cm.getColumn(2).setResizable(false);
		cm.getColumn(2).setPreferredWidth(60);
        cm.getColumn(2).setCellEditor(new RadioButtonCellEditorRenderer());
        cm.getColumn(2).setCellRenderer(new RadioButtonCellEditorRenderer());
        
		scrollPane1.setViewportView(Liste);
	}

	/**
     * Affiche une fen�tre de saisie des param�tres
     * @return
     */
    private boolean displayAddParametersWindow()
    {
    	SerieNParameters parameters = new SerieNParameters(null);
    	parameters.setName("");
    	SerieNParametersWindow launchParamWindow = new SerieNParametersWindow();
		launchParamWindow.setParameters(parameters);
		launchParamWindow.setVisible(true);
		
		boolean retour = parameters.isValid();
		if (retour)
		{
			setErrorMsg(null);
			listParameters.add(parameters);
            MyObject object = new MyObject(parameters.getName(), parameters.getHost());
            ((ParametersTableModel)Liste.getModel()).getMyObjectManager().addObject(object);

			modif = true;
		}
		launchParamWindow.dispose();
		
		return retour;
    }

    /**
     * Affiche une fen�tre de modification des param�tres
     * @return
     */
    private boolean displayModifParametersWindow(SerieNParameters parameters, int index)
    {
    	SerieNParametersWindow launchParamWindow = new SerieNParametersWindow();
		launchParamWindow.setParameters(parameters);
		launchParamWindow.setVisible(true);
		
		boolean retour = parameters.isValid();
		if (retour)
		{
			setErrorMsg(null);
			((ParametersTableModel)Liste.getModel()).setValueAt(parameters.getName(), index, 0);
			((ParametersTableModel)Liste.getModel()).setValueAt(parameters.getHost(), index, 1);
			modif = true;
		}
		launchParamWindow.dispose();
		
		return retour;
    }

    /**
     * Met � jour l'attribut Default_param dans les param�tres de la liste
     */
    private void updateDefautParameters()
    {
    	for (int i=0; i<Liste.getRowCount(); i++)
    	{
    		if ((Boolean) ((ParametersTableModel)Liste.getModel()).getValueAt(i, 2))
    		{
    			if (!listParameters.get(i).isDefault_param())
    				modif = true;
    			listParameters.get(i).setDefault_param(true);
    		}
    		else
    		{
       			if (listParameters.get(i).isDefault_param())
    				modif = true;
    			listParameters.get(i).setDefault_param(false);
    		}
    	}
    }
    
	// -- M�thodes �v�nementielles --------------------------------------------
	
	private void ListeMouseClicked(MouseEvent e) {
		if ((Liste.getSelectedRow() >= 0) && (e.getClickCount() == 2))
		{
			if (modif)
				codeOperation = CONNECT;
			else
				codeOperation = SAVE_CONNECT;
			setVisible(false);
		}
	}

	private void bt_AjouterActionPerformed(ActionEvent e) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run()
			{
				displayAddParametersWindow();
			}
		});
	}

	private void bt_EditerActionPerformed(ActionEvent e) {
		int index = Liste.getSelectedRow();
		if (index > -1)
			displayModifParametersWindow(listParameters.get(index), index);
		modif = true;
	}

	private void bt_SupprimerActionPerformed(ActionEvent e) {
		int index = Liste.getSelectedRow();
		if (index > -1)
		{
			setErrorMsg(null);
			listParameters.remove(index);
			((ParametersTableModel)Liste.getModel()).removeRow(index);
		}
		modif = true;
	}

	private void bt_AnnulerActionPerformed(ActionEvent e) {
		codeOperation = NOTHING;
		setVisible(false);
	}

	private void bt_EnregistrerActionPerformed(ActionEvent e) {
		updateDefautParameters();
		codeOperation = SAVE;
		setVisible(false);
	}

	private void bt_ConnexionActionPerformed(ActionEvent e) {
		if ((listParameters.size() > 1) && (Liste.getSelectedRow() == -1))
		{
			JOptionPane.showMessageDialog(null, "Veuillez s�lectionner un des param�tres de la liste", "S�lection des param�tres", JOptionPane.WARNING_MESSAGE);
			return;
		}
		// On d�sactive le SCROLL LOCK pour ne pas que la fen�tre monte en boucle
		Toolkit.getDefaultToolkit().setLockingKeyState(KeyEvent.VK_SCROLL_LOCK, false);
		updateDefautParameters();
		if (!modif)
			codeOperation = CONNECT;
		else
			codeOperation = SAVE_CONNECT;
		setVisible(false);
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		scrollPane1 = new JScrollPane();
		panel1 = new JPanel();
		bt_Ajouter = new JButton();
		bt_Editer = new JButton();
		bt_Supprimer = new JButton();
		bt_Connexion = new JButton();
		bt_Enregistrer = new JButton();
		bt_Annuler = new JButton();
		l_Erreur = new JLabel();

		//======== this ========
		setTitle("S\u00e9lection des param\u00e8tres pour S\u00e9rie N");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setModal(true);
		setResizable(false);
		setName("this");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setName("dialogPane");
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setName("contentPanel");
				contentPanel.setLayout(new BorderLayout());

				//======== scrollPane1 ========
				{
					scrollPane1.setName("scrollPane1");
				}
				contentPanel.add(scrollPane1, BorderLayout.CENTER);

				//======== panel1 ========
				{
					panel1.setName("panel1");
					panel1.setLayout(new GridLayout(2, 3, 10, 5));

					//---- bt_Ajouter ----
					bt_Ajouter.setText("Ajouter");
					bt_Ajouter.setPreferredSize(new Dimension(100, 28));
					bt_Ajouter.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					bt_Ajouter.setName("bt_Ajouter");
					bt_Ajouter.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							bt_AjouterActionPerformed(e);
						}
					});
					panel1.add(bt_Ajouter);

					//---- bt_Editer ----
					bt_Editer.setText("Editer");
					bt_Editer.setPreferredSize(new Dimension(100, 28));
					bt_Editer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					bt_Editer.setName("bt_Editer");
					bt_Editer.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							bt_EditerActionPerformed(e);
						}
					});
					panel1.add(bt_Editer);

					//---- bt_Supprimer ----
					bt_Supprimer.setText("Supprimer");
					bt_Supprimer.setPreferredSize(new Dimension(100, 28));
					bt_Supprimer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					bt_Supprimer.setName("bt_Supprimer");
					bt_Supprimer.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							bt_SupprimerActionPerformed(e);
						}
					});
					panel1.add(bt_Supprimer);

					//---- bt_Connexion ----
					bt_Connexion.setText("Connecter");
					bt_Connexion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					bt_Connexion.setName("bt_Connexion");
					bt_Connexion.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							bt_ConnexionActionPerformed(e);
						}
					});
					panel1.add(bt_Connexion);

					//---- bt_Enregistrer ----
					bt_Enregistrer.setText("Enregistrer");
					bt_Enregistrer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					bt_Enregistrer.setName("bt_Enregistrer");
					bt_Enregistrer.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							bt_EnregistrerActionPerformed(e);
						}
					});
					panel1.add(bt_Enregistrer);

					//---- bt_Annuler ----
					bt_Annuler.setText("Annuler");
					bt_Annuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					bt_Annuler.setName("bt_Annuler");
					bt_Annuler.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							bt_AnnulerActionPerformed(e);
						}
					});
					panel1.add(bt_Annuler);
				}
				contentPanel.add(panel1, BorderLayout.SOUTH);
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//---- l_Erreur ----
			l_Erreur.setForeground(Color.red);
			l_Erreur.setName("l_Erreur");
			dialogPane.add(l_Erreur, BorderLayout.NORTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);

		setSize(475, 215);
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JScrollPane scrollPane1;
	private JPanel panel1;
	private JButton bt_Ajouter;
	private JButton bt_Editer;
	private JButton bt_Supprimer;
	private JButton bt_Connexion;
	private JButton bt_Enregistrer;
	private JButton bt_Annuler;
	private JLabel l_Erreur;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
