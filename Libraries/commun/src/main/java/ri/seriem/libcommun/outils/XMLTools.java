//=================================================================================================
//==>                                                                       01/04/2010 - 15/04/2016
//==> Encodage/décodage des objets en XML
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;


public final class XMLTools
{
	// Variables
	private static ArrayList<String> listeMsgExceptionDecoder=null;

	/**
	 * Sérialisation d'un objet dans un fichier
	 * @param object objet a serialiser
	 * @param filename chemin du fichier
	 */
	public static void encodeToFile(Object object, String fileName) throws FileNotFoundException, IOException
	{
		// Ouverture de l'encodeur vers le fichier
		XMLEncoder encoder = new XMLEncoder(new FileOutputStream(fileName));
		try
		{
			// Sérialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		}
		finally
		{
			// Fermeture de l'encodeur
			encoder.close();
		}
	}
	
	/**
	 * Désérialisation d'un objet dans un fichier
	 * @param filename chemin du fichier
	 */
	public static Object decodeFromFile(String fileName) throws FileNotFoundException, IOException
	{
	    Object object = null;
	    
	    // Ouverture de décodeur
	    XMLDecoder decoder = new XMLDecoder(new FileInputStream(fileName), null, new ExceptionListener() {
	        public void exceptionThrown(Exception exception)
	        {
	        	if (listeMsgExceptionDecoder == null) listeMsgExceptionDecoder = new ArrayList<String>();
        		listeMsgExceptionDecoder.add(exception.getMessage());
	        }
	    });
	    try
	    {
	        // Désérialisation de l'objet
	        object = decoder.readObject();
	    }
	    finally
	    {
	        // Fermeture du decodeur
	        decoder.close();
	    }
	    return object;
	}

	/**
	 * Sérialisation d'un objet dans un StringBuffer
	 * @param object objet a sérialiser
	 */
	public static StringBuffer encodeToStringBuffer(Object object) throws FileNotFoundException, IOException
	{
		return new StringBuffer(encodeToString(object));
		/* Alternative qui fonctionne mais soucis si les codepages sont différents AS400->Linux pas exemple
		final StringBuffer sb = new StringBuffer();
		
		// Ouverture de l'encodeur vers le fichier
		XMLEncoder encoder = new XMLEncoder(new OutputStream() {
			public void write(int b) throws IOException
			{
				sb.append((char)b);
			}
		});
		try
		{
			// Sérialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		}
		finally
		{
			// Fermeture de l'encodeur
			encoder.close();
		}
		return sb;
		*/
	}
	
	/**
	 * Désérialisation d'un objet dans un StringBuffer
	 * @param sb StringBuffer
	 */
	public static Object decodeFromStringBuffer(final StringBuffer sb) throws FileNotFoundException, IOException
	{
		return decodeFromString(sb.toString());
	    /* Alternative qui fonctionne mais soucis si les codepages sont différents AS400->Linux pas exemple
	    Object object = null;
	 
	    // ouverture de decodeur
	    XMLDecoder decoder = new XMLDecoder(new InputStream() {
	    	private int position = 0;
			public int read() throws IOException
			{
				if (position < sb.length())
					return sb.charAt(position++);
				else
					return -1;
			}
		});
	    try
	    {
	        // deserialisation de l'objet
	        object = decoder.readObject();
	    }
	    finally
	    {
	        // fermeture du decodeur
	        decoder.close();
	    }
	    return object;
	    */
	}

	/**
	 * Sérialisation d'un objet dans un String
	 * @param object objet a sérialiser
	 */
	public static String encodeToString(Object object) throws FileNotFoundException, IOException
	{
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(baos);

		// Ouverture de l'encodeur vers le flux
		try
		{
			// Sérialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		}
		finally
		{
			// Fermeture de l'encodeur
			encoder.close();
		}
		return baos.toString("UTF-8");
	}

	/**
	 * Désérialisation d'un objet dans un String
	 * @param s String
	 */
	public static Object decodeFromString(final String s) throws FileNotFoundException, IOException
	{
	    Object object = null;
	    
	    // ouverture de decodeur
	    XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(s.getBytes("UTF-8")), null, new ExceptionListener() {
	        public void exceptionThrown(Exception exception)
	        {
	        	if (listeMsgExceptionDecoder == null) listeMsgExceptionDecoder = new ArrayList<String>();
        		listeMsgExceptionDecoder.add(exception.getMessage());
	        }
	    });
	    try
	    {
	        // désérialisation de l'objet
	        object = decoder.readObject();
	    }
	    finally
	    {
	        // fermeture du decodeur
	        decoder.close();
	    }
	    return object;
	}

	/**
	 * Retourne la liste des exceptions qui ont pu se déclencher lors du chargement du fichier XML
	 * @return
	 */
	public static ArrayList<String> getListeMsgExceptionDecoder()
	{
		return listeMsgExceptionDecoder;
	}
	
	/***
	 * Sérialise un document
	 * Attention : ne fonctionne pas sur AS400 avec java 5
	 * @param doc
	 * @return
	 */
	/* Non compatible java 5
    public static String serialize(Document doc)
    {
    	if( doc == null ) return null;
    	
        StringWriter writer = new StringWriter();
        OutputFormat format = new OutputFormat(); 
        format.setIndenting(true);

        XMLSerializer serializer = new XMLSerializer(writer, format);
        try
		{
			serializer.serialize(doc);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

        return writer.getBuffer().toString();
    }*/

    /**
     * Enregistre un document dans un fichier xml
     * @param doc
     * @param file
     * @return
     */
    public static boolean toFile(Document doc, String xmlfile)
    {
    	try
		{
	    	Transformer transformer = TransformerFactory.newInstance().newTransformer();
	    	//Result output = new StreamResult(new File(file));
	    	Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xmlfile), "UTF-8"));
	    	Result output = new StreamResult(out);
	    	Source input = new DOMSource(doc);

			transformer.transform(input, output);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
    	return true;
    }
    
    /**
     * Create xml string - fastest, but may have encoding issues
     *
    public static String toXML(ResultSet rs) throws SQLException
    {
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        StringBuffer xml = new StringBuffer();
        xml.append("<Results>");

        while (rs.next())
        {
            xml.append("<Row>");

            for (int i = 1; i <= colCount; i++)
            {
                String columnName = rsmd.getColumnName(i);
                Object value = rs.getObject(i);
                xml.append("<" + columnName + ">");

                if (value != null)
                {
                    xml.append(value.toString().trim());
                }
                xml.append("</" + columnName + ">");
            }
            xml.append("</Row>");
        }

        xml.append("</Results>");

        return xml.toString();
    }

    /**
     * Create document from xml string - slower than using DOM api
     *
    public static Document toDoc(ResultSet rs) throws SQLException, FactoryConfigurationError, ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        String xml = toXML(rs);
        StringReader reader = new StringReader(xml);
        InputSource source = new InputSource(reader);
        return builder.parse(source);
    }*/
}
