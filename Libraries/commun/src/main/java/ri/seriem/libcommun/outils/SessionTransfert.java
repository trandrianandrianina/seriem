//=================================================================================================
//==>                                                                       05/10/2006 - 15/04/2016
//==>  G�re les transferts de fichiers en parall�le entre un client et la serveur 
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;

import ri.seriem.libcommun.outils.LinkedHashMapManager.lhdListener;
import ri.seriem.libcommun.outils.communication.BufferManager;
import ri.seriem.libcommun.outils.communication.LinesManager;


public class SessionTransfert extends Session 
{
	private final static String NOM_CLASSE="[SessionTransfert]";
	 
	// Constantes erreurs de chargement
	//private final static String ERREUR_RENCONTREE="Erreur pendant l'ex\u00e9cution : ";
	//private final static String ERREUR_CONNEXION_IMPOSSIBLE="Impossible de se connecter au serveur sur : ";
//	private final static String ERREUR_OUVERTURE_SESSION_IMPOSSIBLE="Impossible d'ouvrir une session avec le profil : ";
	//private final static String ERREUR_ENVOI_SOCKET="Probl\u00e8me lors de l'envoi du message sur la socket.";

//	private final static String DEMANDE_LISTE_RUNTIME="Demande de la liste des runtimes au serveur.";
//	private final static String RECUPERATION_LISTE_RTCONFIG="R\u00e9cup\u00e9ration de la liste des configs de runtimes du serveur.";
//	private final static String RECUPERATION_LISTE_RUNTIME="R\u00e9cup\u00e9ration de la liste des runtimes du serveur.";
	private final static String DECONNEXION_SESSION="Fermeture de la session client.";

	// Variables
	//private DataInputStream fromServer;
	//private DataOutputStream toServer;

	private LogMessage log=null;
	//private String serveur=null;
	//private int port=Constantes.SERVER_PORT;
	private String codepageServeur=System.getProperty("file.encoding");
	private String nomConfig=null;
	
	//private Socket soccli=null;
	private GestionRT cliRT=null, srvRT=null;
	private int isConnect=Constantes.FALSE, isSession=Constantes.FALSE;
	private LinkedHashMapManager listeFichierATelecharger=null; // Liste des fichiers normaux � r�cup�rer
	private LinkedHashMapManager listeFichierRTATelecharger=null; // Liste des fichiers RunTimes � r�cup�rer
	private LinkedHashMapManager listeFichierAUploader=null;
	private LinkedHashMapManager listeFichierASupprimer=null;

	private LinkedList<String> listeContenuDossierARecup=null;   // Liste des dossiers � lister
	private HashMap<String, String[]> listeContenuDossierRecup=null;    // Liste des contenu des dossiers r�cup�r�s
	private boolean transfertEnCours=false;
	private boolean transfertRTEnCours=false;
	private boolean uploadEnCours=false;
	private boolean supprEnCours=false;
	private int transfertDossierEnCours=Constantes.FALSE;

	private EnvUser infoUser=null;
	private ArrayListManager evenementEdition = null; //new ArrayListManager();	// Liste des �v�nements concernant les �ditions (Gestion des impressions)
	private ArrayListManager evenementSysteme = new ArrayListManager(); // Liste des �v�nements concernant le syst�me ( package protocoleMsg )
	//private int idThisSession = 0;

	//private MessageBuffer bufferManager = new MessageBuffer();
	private LinesManager linesManager = null;
	private BufferManager bufferToSend = null;


	/**
	 * Constructeur
	 *
	public SessionTransfert(String serveur, int port, LogMessage log, String nomConfig)
	{
		// Initialisation des variables
		this.serveur = serveur;
		this.port = port;
		this.log = log;
		this.nomConfig = nomConfig; 
	}*/

	/**
	 * Constructeur
	 */
	public SessionTransfert(EnvUser ainfoUser)
	{
		//id = hashCode();
		bufferToSend = new BufferManager(hashCode(), ainfoUser.hashCode());
		idSession = bufferToSend.getIdsession();
		linesManager = ainfoUser.getLinesManager();
		linesManager.setIdgroup(ainfoUser.getId().hashCode());
		linesManager.addSession2List(this);
//System.out.println("----------\n-sessiontransfert->" + idSession + "\n----------");

		// Initialisation des variables
		infoUser = ainfoUser;
		log = infoUser.getLog();
		nomConfig = infoUser.getNomConfig(); 
	}

	/**
	 * Initialisation des listes de RT Serveur & Client
	 * @param serveurRT
	 * @param clientRT
	 */
	public void setGestionRT(GestionRT serveurRT, GestionRT clientRT)
	{
		srvRT = serveurRT;
		cliRT = clientRT;
	}
	/*
	private int connexion()
	{
		// Connexion au serveur VGM
		try
		{
			soccli = new Socket(infoUser.getIa_serveurSGM(), infoUser.getPortSGM());
			soccli.setSoTimeout(Constantes.SOCKET_TIMEOUT);
			soccli.setKeepAlive(true);
			//soccli = new Socket(serveur, port);
			
			// Cr�ation des �couteurs
			fromServer = new DataInputStream(new BufferedInputStream(soccli.getInputStream(), Constantes.TAILLE_BUFFER));
			toServer = new DataOutputStream(new BufferedOutputStream(soccli.getOutputStream(), Constantes.TAILLE_BUFFER));
			isConnect = Constantes.TRUE;
			
			// On amorce le dialogue avec le serveur
			bufferManager.sendBuffer(toServer, Constantes.CONNEXION_OK, idThisSession, 0);
			infoUser.resetCompteurReconnexion();
		}
		catch (IOException ioe)
		{
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_CONNEXION_IMPOSSIBLE + infoUser.getServeurSGM());
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ioe);
			return Constantes.ERREUR;
		}
		return Constantes.OK;
	}*/
	
	/**
	 * Initialise une nouvelle session
	 */
	public int setSession(EnvUser infouser)
	{
		infoUser = infouser;
		//if( connexion() == Constantes.ERREUR ) return Constantes.ERREUR;
//System.out.println("[SessionTransfert] (setSession) dostrav : " + dossierTravail+ " dostempo : " + dossierTempo);        

		// Initialisation des listes de fichiers
		//listeFichierArecup = Collections.synchronizedList(new LinkedList());
		//listeFichierRecup = Collections.synchronizedList(new LinkedList());
		listeContenuDossierRecup = new HashMap<String, String[]>();
		listeContenuDossierARecup = new LinkedList<String>();
		listeFichierATelecharger=new LinkedHashMapManager();
		listeFichierATelecharger.addListener(new lhdListener() {
			public void onDataCleared()	{}
			public void onDataRemoved(Object cle)
			{
				demarreTelechargement();
			}
			public void onDataAdded(Object cle, Object val)
			{
				demarreTelechargement();
			}
		});
		listeFichierRTATelecharger=new LinkedHashMapManager();
		listeFichierRTATelecharger.addListener(new lhdListener() {
			public void onDataCleared()	{}
			public void onDataRemoved(Object cle)
			{
				demarreTelechargementRT();
			}
			public void onDataAdded(Object cle, Object val)
			{
				demarreTelechargementRT();
			}
		});
		listeFichierAUploader=new LinkedHashMapManager();
		listeFichierAUploader.addListener(new lhdListener() {
			public void onDataCleared()	{}
			public void onDataRemoved(Object cle)
			{
				demarreUpload();
			}
			public void onDataAdded(Object cle, Object val)
			{
				demarreUpload();
			}
		});
		listeFichierASupprimer=new LinkedHashMapManager();
		listeFichierASupprimer.addListener(new lhdListener() {
			public void onDataCleared()	{}
			public void onDataRemoved(Object cle)
			{
				demarreSuppr();
			}
			public void onDataAdded(Object cle, Object val)
			{
				demarreSuppr();
			}
		});

		// On d�marre le thread
		log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Lancement de la session Transfert.");
		//start();
		startConnexion();


		return Constantes.OK;
	}

	/**
	 * Initialise une nouvelle session
	 *
	public void run()
	{
		int result = 0;
		do{
			
		String chaine="";
		int idmessage=0;
		byte[] buffer = new byte[Constantes.TAILLE_BUFFER];
		ByteBuffer newbuffer = null;
		String message;

		// Lecture continue
		while (isConnect == Constantes.TRUE)
		{
			try
			{
//System.out.println("[SessionTransfert] Attente d'un idmessage");				
				//idmessage = fromServer.readInt();
				newbuffer = bufferManager.receiveBuffer(fromServer);
				idmessage = newbuffer.getInt();

//System.out.println("[SessionTransfert] idmessage recu:"+ idmessage);
				switch(idmessage)
				{
					// Le client se d�connecte <-----------------------------------------------
					case Constantes.STOP :
						etatReconnexion = false;
						isConnect=Constantes.FALSE;
						isSession=Constantes.FALSE;
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + DECONNEXION_SESSION);
						break;

						// Le serveur nous renvoie une r�ponse ERREUR --------------------------------
					case Constantes.ERREUR :
						idmessage = newbuffer.getInt();
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, codepageServeur);
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "idmessage:" +idmessage + "Erreur:" + message);
						break;

						// Le serveur nous renvoie une r�ponse OK --------------------------------
					case Constantes.CONNEXION_OK :
						idThisSession = newbuffer.getInt();
						if( etatReconnexion ){
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Reconnexion au serveur S�rie N (idThisSession:" + idThisSession + ")" );
							break;
						}
						etatReconnexion = true;

						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Connexion au serveur S�rie N (idThisSession:" + idThisSession + ")" );
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande l'ouverture de la session Transfert.");
						//EnvoiMessageSocket(Constantes.DEMANDE_OUVERTURE_TRANSFERT_SESSION, nomConfig + Constantes.SEPARATEUR_CHAINE_CHAR + infoUser.getId());
						bufferManager.sendBuffer(toServer, Constantes.DEMANDE_OUVERTURE_TRANSFERT_SESSION, nomConfig + Constantes.SEPARATEUR_CHAINE_CHAR + infoUser.getId());
						break;

						// Le serveur nous demande si le client est toujours pr�sent --------------------------------
					case Constantes.PING :
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, infoUser.getCodepageServeur());
						System.out.println("-transfert-> Ping recu " + message);
						//EnvoiMessageSocket(Constantes.PONG, NOM_CLASSE + " " + infoUser.getProfil() + "|" + idThisSession);
						bufferManager.sendBuffer(toServer, Constantes.PONG, NOM_CLASSE + " " + infoUser.getProfil() + "|" + idThisSession);
						break;

						// La session a �t� accept� par le serveur ------------------------------------
					case Constantes.OUVERTURE_SESSION_OK :
						idmessage = newbuffer.getInt();
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, codepageServeur);
						isSession=Constantes.TRUE;
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + message.replace(Constantes.SEPARATEUR_CHAINE_CHAR, Constantes.SEPARATEUR_CHAR));
						break;

					// Le serveur envoi un fichier -------------------------------------------------
					case Constantes.RECOIT_FICHIER_RT :
						idmessage = newbuffer.getInt();
						// R�cup�ration des infos du fichier
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, codepageServeur);
//						System.out.println("RECOIT_FICHIER_RT --> " + message);                                  
						// S'il y a eu une erreur lors de la r�cup�ration du fichier
						if (message.trim().endsWith("-1"))
						{
							chaine  = message.substring(0, message.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR)).trim();
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier non r�cup�r� : " + chaine);
							AjoutItemRTTelecharger(chaine, "9");
						}
						else // Sinon r�cup�ration du fichier + mise � jour de la liste RT
						{

							buffer = new byte[newbuffer.getInt()];
							newbuffer.get(buffer);

							chaine = cliRT.setFichier(message, buffer);
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier r�cup�r� : " + chaine);
							// S'il s'agit d'un panel, on v�rifie que ses fichiers auxiliaires soient pr�sents sur le client
							//if (chaine.toLowerCase().endsWith(".bin")) addListeFicPnl(chaine);
							AjoutItemRTTelecharger(chaine, "1");
						}
						transfertRTEnCours = false;
//						System.out.println("FIN RECOIT_FICHIER_RT");                                       
						break;

						// Le serveur envoi un fichier -------------------------------------------------
					case Constantes.RECOIT_FICHIER :
						idmessage = newbuffer.getInt();
						// R�cup�ration des infos du fichier
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, codepageServeur);
//System.out.println("RECOIT_FICHIER --> " + message);
						// S'il y a eu une erreur lors de la r�cup�ration du fichier
						if (idmessage < 0)
						{
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier non r�cup�r� : " + message);
							// On le supprime de la liste
							AjoutItemTelecharger(message, "9");
						}
						else // Sinon r�cup�ration du fichier
						{
							buffer = new byte[newbuffer.getInt()];
							newbuffer.get(buffer);

							// D�composition des infos
							String dossier = infoUser.getDossierTempUser() + File.separatorChar + message.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
//System.out.println("[SessionTransfert] (run) " + dossier);

							//  On enregistre le fichier
							try
							{
								DataOutputStream f = new DataOutputStream(new FileOutputStream(dossier));
								f.write(buffer, 0, buffer.length);
								f.close();
							}
							catch (Exception e)	{}
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier r�cup�r� : " + dossier);
							// On marque le fichier comme t�l�charg�
							AjoutItemTelecharger(message, "1");
						}
						transfertEnCours = false;
						break;

						// Le serveur envoi le contenu d'un dossier -------------------------------------------------
					case Constantes.RECOIT_CONTENU_DOSSIER :
						idmessage = newbuffer.getInt();
						// R�cup�ration des infos du fichier
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, codepageServeur);
//System.out.println("RECOIT_FICHIER --> " + message);                                  
						// S'il y a eu une erreur lors de la r�cup�ration du fichier
						if (idmessage < 0)
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Probl�me pendant le listage du dossier : " + message);

						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);

						// D�composition des infos
						int pos = message.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR);
						if (pos == -1)
							chaine = message.trim();
						else
							chaine = message.substring(0, pos);
//						System.out.println("RECOIT_CONTENU_DOSSIER --> " + chaine+"|");    				
						listeContenuDossierRecup.put(chaine, Constantes.splitString(message, Constantes.SEPARATEUR_CHAINE_CHAR));

						// On v�rifie s'il y a d'autres fichiers � r�cup�rer
//						System.out.println("RECOIT_CONTENU_DOSSIER --> " + chaine);                                  
						listeContenuDossierARecup.remove(chaine);
//						System.out.println("Nombre de fichier dans la liste : " + listeFichierArecup.size());
						if (listeContenuDossierARecup.size() != 0)
						{
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande la r�cup�raton de  " + listeContenuDossierARecup.get(0));
							//EnvoiMessageSocket(Constantes.DEMANDE_CONTENU_DOSSIER, (String)listeContenuDossierARecup.get(0));
							bufferManager.sendBuffer(toServer, Constantes.DEMANDE_CONTENU_DOSSIER, (String)listeContenuDossierARecup.get(0));
						}
						else
							transfertDossierEnCours = Constantes.FALSE;
//						System.out.println("FIN RECOIT_FICHIER");                                       
						break;
						
					// Confirmation de la fin de l'upload d'un fichier <--------------------------------------------------------------
					case Constantes.UPLOAD_FICHIER :
						// R�cup�ration du nom du fichier et de sa destination 
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						chaine = new String(buffer, 0, buffer.length);
						idmessage = newbuffer.getInt();

						// On v�rifie s'il y a d'autres fichiers � uploader
						uploadEnCours = false;

						listeFichierAUploader.removeObject(chaine);
						//listeFichierAUploader.addObject(chaine, "1");

						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Upload du fichier termin�: " + chaine);
						break;

						// Confirmation de la fin de l'upload d'un fichier <--------------------------------------------------------------
					case Constantes.SUPPRIME_FICHIER :
						// R�cup�ration du nom du fichier et de sa destination 
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						chaine = new String(buffer, 0, buffer.length);
						idmessage = newbuffer.getInt();

						// On v�rifie s'il y a d'autres fichiers � supprimer
						supprEnCours = false;

						listeFichierASupprimer.removeObject(chaine);
						//listeFichierASupprimer.addObject(chaine, "1");

						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Suppression fichier(s) Ok.");
						break;

						
					// Le serveur renvoit la r�ponse du serveur d'�dition  -------------------------------------------------
					case Constantes.RECOIT_REQUETE_EDITION :
						// R�cup�ration des infos du fichier
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						final String msg = new String(buffer, 0, buffer.length, codepageServeur);
//System.out.println("--> RECOIT_REQUETE_EDITION : " + msg);
						new Thread()
						{
							public void run()
							{
								evenementEdition.addObject(msg);
							}
						}.start();

//System.out.println("--> RECOIT_REQUETE_EDITION : Fin ");						
						break;

						// Le serveur ren�oit la r�ponse du serveur  --------------------------------------------------------
					case Constantes.RECOIT_REQUETE_SYSTEME :  
						// R�cup�ration des infos du fichier
						idmessage = newbuffer.getInt();
						buffer = new byte[newbuffer.getInt()];
						newbuffer.get(buffer);
						final String msgsys = new String(buffer, 0, buffer.length, codepageServeur);
//System.out.println("--> RECOIT_REQUETE_SYSTEME : " + msgsys);
						new Thread()
						{
							public void run()
							{
								evenementSysteme.addObject(msgsys);
							}
						}.start();
//System.out.println("--> RECOIT_REQUETE_SYSTEME : Fin ");						
						break;

						// Valeur non trait�e <----------------------------------------------------
					default:
						break;
				}
			}/*
			catch (SocketException e) // Erreur socket on sort
			{
				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "setSession->run : Socket");
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
				isConnect = Constantes.FALSE;
//				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Connect� : " + soccli.isConnected()  + " toServer:" + toServer + " fromServer:" + fromServer);
			}
			catch (SocketTimeoutException ioe) // Voir � supprimer bient�t
			{
				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "setSession->run : TimeOut");
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + ioe);
				if( !soccli.isConnected() )
					isConnect = Constantes.FALSE;
//				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Connect� : " + soccli.isConnected() + " " + isConnect);
			}*
			catch (IOException ioe) // Voir � supprimer bient�t
			{
				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "setSession->run : IOE");
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + ioe);
				//if( !soccli.isConnected() )
					isConnect = Constantes.FALSE;
//				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Connect� : " + soccli.isConnected() + " " + isConnect);
			}
			catch (Exception e) // Le reste on attend de voir (� affiner)
			{
				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "setSession->run : Autre");
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
				if( !soccli.isConnected() )
					isConnect = Constantes.FALSE;
//				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Connect� : " + soccli.isConnected() + " " + isConnect);
			}
		}
		// Fermeture propre des connexions socket
		try
		{
			toServer.close();
			fromServer.close();
			soccli.close();
		}
		catch (IOException ioe)	{}
		
//System.out.println("-SessionTransfert-> "  + etatReconnexion + " " + infoUser);
		if( etatReconnexion ){
			do{
				infoUser.incCompteurReconnexion();
				result = connexion(); 
				if( result == Constantes.ERREUR ){
					try{ Thread.sleep(1000); }catch(Exception e){};
//System.out.println("-SessionTransfert-> Reconnexion Ok");
				} else{
					//infoUser.decCompteurReconnexion();
System.out.println("-SessionTransfert-> Reconnexion (idThisSession:"+idThisSession+") Ok");
					log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "Reconnexion de SessionTransfert (idThisSession:"+idThisSession+") Ok");
				}
			}
			while( (result == Constantes.ERREUR) && !infoUser.isStopReconnexion() );
			if( infoUser.isStopReconnexion() )
				etatReconnexion = false;
		}

		}
		while( etatReconnexion );
	}*/

	
	/**
	 * Lecture des octets re�us sur la Socket
	 *
	private synchronized byte[] lectureSocket()
	{
		int octet_alire=0, octet_lus=0, compteur=0;
		byte buffercomplet[]=null;

		// On r�cup�re tous les octets envoy�s par le client
		try
		{
			octet_alire = fromServer.readInt();
			buffercomplet = new byte[octet_alire];
			do
			{
				octet_lus = fromServer.read(buffercomplet, compteur, octet_alire);
				compteur += octet_lus;
				octet_alire -= octet_lus;
			}
			while (octet_alire != 0);
		}
		catch(Exception e)
		{
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
			return null;
		}

		return buffercomplet;
	}*/

	/**
	 * Lecture des octets re�us sur la Socket
	 *
	private synchronized ByteBuffer newLectureSocket()
	{
		int octet_alire = 0, octet_lus = 0, compteur = 0;
		byte buffercomplet[] = null;

		// On r�cup�re tous les octets envoy�s par le client
		try
		{
			octet_alire = fromServer.readInt();
			//			System.out.println("[SessionIdentification] (lectureSocket) --> octet_alire" + octet_alire);
			buffercomplet = new byte[octet_alire];
			do
			{
				octet_lus = fromServer.read(buffercomplet, compteur, octet_alire);
				//				System.out.println("[SessionIdentification] (lectureSocket) --> octet_lus" + octet_lus);									
				compteur += octet_lus;
				octet_alire -= octet_lus;
			}
			while (octet_alire != 0);
		}
		catch (Exception e)
		{
			//e.getStackTrace();
			log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
			return null;
		}
		
		return ByteBuffer.wrap(buffercomplet);
	}*/

	/**
	 * Lecture des octets re�us sur la Socket
	 * M�thode am�lior�e pour Scroeder (d�tection d'erreurs)
	 *
	private synchronized int getLectureSocket(final byte[] buffercomplet)
	{
		int octet_alire=0, octet_lus=0, compteur=0, taille=buffercomplet.length;

		// On r�cup�re tous les octets envoy�s par le client
		try
		{
			//octet_alire = fromServer.readInt();
			octet_alire = fromServer.available();
			log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "getLectureSocket, nombre octets � lire " + octet_alire);
			if (octet_alire < buffercomplet.length)
				taille = octet_alire;
			
			do
			{
				octet_lus = fromServer.read(buffercomplet, compteur, taille); 
				log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "getLectureSocket, nombre octets lus " + octet_lus);
				compteur += octet_lus;
				octet_alire -= octet_lus;
				if (octet_alire < buffercomplet.length)
					taille = octet_alire;
			}
			while (octet_alire != 0);
		}
		catch (Exception e)
		{
			// Permet de remettre le curseur normal
			//setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

			log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + "getLectureSocket");
			log.ecritureMessage(infoUser.getProfil(), NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
			return -1;
		}
		return compteur;
	}*/

	/**
	 * Envoi un message au Serveur
	 *
	public synchronized int EnvoiMessageSocket(int idmessage, String message)
	{
		try
		{
			toServer.writeInt(idmessage);
			if (message != null)
			{
				toServer.writeInt(message.length());    
				toServer.write(message.getBytes(codepageServeur), 0, message.length());
			}
			toServer.flush();
		}
		catch (Exception e) 
		{ 
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_ENVOI_SOCKET);
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + message);
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + e.getMessage());
//			msgErreur = ERREUR_ENVOI_SOCKET;
			return Constantes.ERREUR;
		}

		return Constantes.OK;
	}*/

	public void sendBuffer(int idmessage, String message)
	{
		//bufferManager.sendBuffer(toServer, idmessage, message);
		bufferToSend.setIdmessage(idmessage);
		bufferToSend.addBuffer(message);
		if( !linesManager.send(bufferToSend) ){
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
		}
	}
	
	/**
	 * V�rifie et attend que la session soit ouverte 
	 * @return
	 */
	private void waitOpenSession()
	{
		if (isSession == Constantes.FALSE)
			do
			{
				try	{ Thread.sleep( 5 ); } catch(Exception e) {}
			}
			while (isSession == Constantes.FALSE);
	}
	
	/**
	 * Ajoute un fichier RT � r�cup�rer dans la liste
	 * ATTENTION � la casse suivant les OS (� corriger +tard)
	 */
	public boolean FichierArecupRT(String fichier)
	{
		String fichierNormalise=null;

//System.out.println("[TransfertSession] (FichierArecupRT) " + fichier);
		// On effectue une v�rification du fichier � r�cup�rer
		if ((fichier == null) || (fichier.indexOf('@') != -1)) return false;

		// Attend que la session Transfert soit bien d�marr� sinon pb avec le transfert du fichier des traductions
//System.out.println("[TransfertSession] (FichierArecupRT) waitOpenSession ");		
		waitOpenSession();
//System.out.println("[TransfertSession] (FichierArecupRT) waitOpenSession fin");		

		// On formate correctement le fichier � t�l�charger
		fichierNormalise = fichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
//System.out.println("[TransfertSession] (FichierArecupRT) " + fichier  +" " +fichierNormalise);
		// On v�rifie si on a besoin de r�cup�rer le fichier
		if (srvRT.getCptFic(fichierNormalise) == cliRT.getCptFic(fichierNormalise))
		{
			// On v�rifie que le fichier existe bien sur le serveur
			if (srvRT.getCptFic(fichierNormalise) == Constantes.FALSE)
			{
//				srvRT.getAffiche(srvRT.getNomConfig() + "srv.txt");
//				cliRT.getAffiche(cliRT.getNomConfig() + "cli.txt");
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Le fichier "+fichierNormalise+" est introuvable sur le serveur: " + fichier);
				return false;
			}

			// On v�rifie qu'il ne soit pas d�j� dans la liste
			synchronized(listeFichierRTATelecharger)
			{ 
				if (listeFichierRTATelecharger.getObject(fichierNormalise) == null)
					listeFichierRTATelecharger.addObject(fichierNormalise, "1");
			}
		}
		else // Le fichier doit �tre r�cup�r�
		{
			// On v�rifie qu'il ne soit pas d�j� dans la liste
			synchronized(listeFichierRTATelecharger)
			{ 
				if (listeFichierRTATelecharger.getObject(fichierNormalise) == null)
					listeFichierRTATelecharger.addObject(fichierNormalise, "0");
			}
		}
		return true;
	}

	/**
	 * V�rifie si le fichier a bien �t� t�l�charg�
	 */
	public int FichierRecupRT(String fichier)
	{
		if (fichier == null) return Constantes.FALSE;
		
		String chaine=null;
		String fichierNormalise = fichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR); 
//System.out.println("[SessionTransfert] (listeFichierATelecharger) avt taille de la liste: " + listeFichierATelecharger.getHashMap().size());            	

		// On v�rifie qu'il y ait un fichier � t�l�charger
		do
		{
			synchronized(listeFichierRTATelecharger)
			{ 
				chaine = (String)listeFichierRTATelecharger.getObject(fichierNormalise);
				if ((chaine != null) && (chaine.equals("0")))
					try	{ Thread.sleep(5);} catch (InterruptedException e) {}
			}
		}
		while((chaine != null) && (chaine.equals("0")));
		synchronized(listeFichierRTATelecharger) { listeFichierRTATelecharger.removeObject(fichierNormalise); }
		
//System.out.println("[SessionTransfert] (listeFichierATelecharger) FIN");            	
		return Constantes.TRUE;
	}

	/**
	 * R�cup�re la liste des fichiers n�cessaire au panel
	 *
	public synchronized void addListeFicPnl(String fichierpanel)
	{
		int i=0;
		GestionFichierDAT gfb=null;
		String tabchaine[]=null;

		gfb = new GestionFichierDAT(infoUser.getDossierTravail() + File.separatorChar + fichierpanel);
		// R�cup�ration de la liste des fichiers n�cessaire au panel
		tabchaine = gfb.getListeFichier();
		if (tabchaine != null)
		{
			// On constitue la liste des fichiers � r�cup�rer
			for (i=0; i<tabchaine.length; i++)
			{
//				System.out.println("[SessionTransfert] (addListeFicPnl) -> " + tabchaine[i]);        	
				if ( (tabchaine[i].indexOf('@') == -1) && (srvRT.getCptFic(tabchaine[i]) != cliRT.getCptFic(tabchaine[i])) && (listeFichierATelecharger.getObject(tabchaine[i])==null))
					listeFichierATelecharger.addObject(tabchaine[i], "0");
			}
		}
	}*/

	/**
	 * Ajoute un fichier � r�cup�rer dans la liste
	 * ATTENTION � la casse suivant les OS (� corriger +tard)
	 */
	public void FichierArecup(String fichier)
	{
		// On effectue une v�rification du fichier � r�cup�rer
		if ((fichier == null) || (fichier.indexOf('@') != -1)) return;

		// Attend que la session Transfert soit bien d�marr� sinon pb avec le transfert du fichier des traductions
		waitOpenSession();

		// On formate correctement le fichier � t�l�charger
		String fichierNormalise = fichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
		// On v�rifie qu'il ne soit pas d�j� dans la liste
//System.out.println("[TransfertSession] (FichierArecup) " + listeFichierATelecharger.getObject(fichierNormalise) + " " + fichier);
		synchronized(listeFichierATelecharger)
		{ 
			if (listeFichierATelecharger.getObject(fichierNormalise) == null)
				listeFichierATelecharger.addObject(fichierNormalise, "0");
		}
//System.out.println("FIN (FichierArecup) " + fichierNormalise + " " + listeFichierATelecharger.getHashMap().size());
	}

	/**
	 * V�rifie si le fichier a bien �t� t�l�charg�
	 */
	public int FichierRecup(String fichier)
	{
		if (fichier == null) return Constantes.FALSE;
		
		String chaine=null;
		String fichierNormalise = fichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR); 
//System.out.println("[SessionTransfert] (listeFichierATelecharger) avt taille de la liste: " + listeFichierATelecharger.getHashMap().size());            	

		// On v�rifie qu'il y ait un fichier � t�l�charger
		do
		{
			synchronized(listeFichierATelecharger) { chaine = (String)listeFichierATelecharger.getObject(fichierNormalise); }
			if ((chaine != null) && (chaine.equals("0")))
				try	{ Thread.sleep(5);} catch (InterruptedException e) {}
		}
		while((chaine != null) && (chaine.equals("0")));
		synchronized(listeFichierATelecharger) { listeFichierATelecharger.removeObject(fichierNormalise); }
		
//System.out.println("[SessionTransfert] (listeFichierATelecharger) FIN");            	
		return Constantes.TRUE;
	}

	/**
	 * Ajoute un fichier � uploader dans la liste
	 * ATTENTION � la casse suivant les OS (� corriger +tard)
	 */
	public void FichierAUploader(File fichier, String chemindest)
	{
		// On effectue une v�rification du fichier � uploader
		if (fichier == null) return;
		String sfichier = fichier.getAbsolutePath();
		if (sfichier.indexOf('@') != -1) return;

		// On construit correctement le fichier � uploader
		String fichierServeur = chemindest.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR) + fichier.getName();
//System.out.println("--> " + fichierServeur + " " + fichier.getAbsolutePath());		
		// On v�rifie qu'il ne soit pas d�j� dans la liste
		synchronized(listeFichierAUploader)
		{ 
			if (listeFichierAUploader.getObject(fichierServeur) == null)
				listeFichierAUploader.addObject(fichierServeur, fichier.getAbsolutePath());
		}
	}

	/**
	 * V�rifie si le fichier a bien �t� upload�
	 */
	public boolean FichierUploade(File fichier, String chemindest)
	{
		if (fichier == null) return false;
		
		String chaine=null;
		
		// On construit correctement le fichier � uploader
		String fichierServeur = chemindest.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR) + fichier.getName();
		// On v�rifie qu'il y ait un fichier � t�l�charger
		do
		{
			synchronized(listeFichierAUploader) { chaine = (String)listeFichierAUploader.getObject(fichierServeur); }
			if (chaine != null)
				try	{ Thread.sleep(5);} catch (InterruptedException e) {}
		}
		while(chaine != null);
		synchronized(listeFichierAUploader) { listeFichierAUploader.removeObject(fichierServeur); }
		
//System.out.println("[SessionTransfert] (listeFichierATelecharger) FIN " + fichier + " " + chaine);            	
		return true;
	}

	/**
	 * Ajoute une liste de fichiers � supprimer dans la liste
	 * ATTENTION � la casse suivant les OS (� corriger +tard)
	 */
	public void FichierASupprimer(String lstfichier)
	{
		// On effectue une v�rification de la lste des fichiers � supprimer
		if (lstfichier == null) return;
		if (lstfichier.indexOf('@') != -1) return;

		// On construit correctement le fichier � supprimer
		lstfichier = lstfichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
		// On v�rifie qu'il ne soit pas d�j� dans la liste
		synchronized(listeFichierASupprimer)
		{ 
			if (listeFichierASupprimer.getObject(lstfichier) == null)
				listeFichierASupprimer.addObject(lstfichier, "0");
		}
	}

	/**
	 * V�rifie si la liste des fichiers a bien �t� supprim�
	 */
	public boolean FichierSuppr(String fichier)
	{
		if (fichier == null) return false;
		
		String chaine=null;
		// On v�rifie qu'il y ait un fichier � supprimer
		do
		{
			synchronized(listeFichierASupprimer) { chaine = (String)listeFichierASupprimer.getObject(fichier); }
			if ((chaine != null) && (chaine.equals("0")))
				try	{ Thread.sleep(5);} catch (InterruptedException e) {}
		}
		while((chaine != null) && (chaine.equals("0")));
		synchronized(listeFichierASupprimer) { listeFichierASupprimer.removeObject(fichier); }
		
//System.out.println("[SessionTransfert] (listeFichierATelecharger) FIN " + fichier + " " + chaine);            	
		return true;
	}


	/**
	 * Ajoute un item dans la liste des objets � t�l�charger
	 * @param chaine
	 * @param etat
	 */
	private synchronized void AjoutItemRTTelecharger(String chaine, String etat)
	{
		listeFichierRTATelecharger.addObject(chaine, etat);
	}

	/**
	 * Ajoute un item dans la liste des objets � t�l�charger
	 * @param chaine
	 * @param etat
	 */
	private synchronized void AjoutItemTelecharger(String chaine, String etat)
	{
		listeFichierATelecharger.addObject(chaine, etat);
	}

	/**
	 * Ajoute un item dans la liste des objets � uploader
	 * @param chaine
	 * @param etat
	 *
	private synchronized void AjoutItemUploader(String chaine, String etat)
	{
		listeFichierAUploader.addObject(chaine, etat);
	}*/

	/**
	 * V�rifie si on peut d�marrer un t�l�chargement de fichier
	 */
	private synchronized void demarreTelechargementRT()
	{
		// Si pas de t�l�chargement en cours
		if (transfertRTEnCours == false) 
		{
			String chaine = (String)listeFichierRTATelecharger.getValue("0");
			if (chaine == null)	return;
//System.out.println("[SessionTransfert] (demarreTelechargement) " + chaine);    		
			transfertRTEnCours = true;
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande la r�cup�ration de " + chaine);
			//if (EnvoiMessageSocket(Constantes.DEMANDE_FICHIER_RT, chaine) == Constantes.FALSE)
			//if( !bufferManager.sendBuffer(toServer, Constantes.DEMANDE_FICHIER_RT, chaine) )
			bufferToSend.setIdmessage(Constantes.DEMANDE_FICHIER_RT);
			bufferToSend.addBuffer(chaine);
			if( !linesManager.send(bufferToSend) ){
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
				transfertRTEnCours = false;
			}
		}
//System.out.println("[SessionTransfert] FIN (demarreTelechargement) ");
	}

	/**
	 * V�rifie si on peut d�marrer un t�l�chargement de fichier
	 */
	private synchronized void demarreTelechargement()
	{
		// Si pas de t�l�chargement en cours
		if (transfertEnCours == false) 
		{
			String chaine = (String)listeFichierATelecharger.getValue("0");
			if (chaine == null)	return;
//System.out.println("[SessionTransfert] (demarreTelechargement) " + chaine);    		
			transfertEnCours = true;
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande la r�cup�ration de " + chaine);
			//if (EnvoiMessageSocket(Constantes.DEMANDE_FICHIER, chaine) == Constantes.FALSE)
			//if( !bufferManager.sendBuffer(toServer, Constantes.DEMANDE_FICHIER, chaine) )
			bufferToSend.setIdmessage(Constantes.DEMANDE_FICHIER);
			bufferToSend.addBuffer(chaine);
			if( !linesManager.send(bufferToSend) ){
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
				transfertEnCours = false;
			}
		}
//System.out.println("[SessionTransfert] FIN (demarreTelechargement) ");
	}

	/**
	 * V�rifie si on peut d�marrer un upload de fichier
	 */
	private synchronized void demarreUpload()
	{
		// Si pas de t�l�chargement en cours
		if (uploadEnCours == false) 
		{
			String cle = (String)listeFichierAUploader.getKeyAtIndex(0);
			if (cle == null) return;
			String valeur = (String)listeFichierAUploader.getObject(cle);
			
			//String[] chaine = Constantes.splitString(cle, Constantes.SEPARATEUR_CHAINE_CHAR);
//System.out.println("[SessionTransfert] (demarreTelechargement) " + chaine);    		
			uploadEnCours = true;
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Upload du fichier " + valeur + " vers " + cle);
			
			File fichier = new File(valeur); 
			byte[] buffer = new byte[(int)fichier.length()];

			//  On lit le fichier et on le stocke dans un tableau de byte
			try
			{
				DataInputStream f = new DataInputStream(new FileInputStream(fichier));
				if (f.read(buffer) == buffer.length)
				{
					//toServer.writeInt(Constantes.UPLOAD_FICHIER);
					//toServer.writeInt(cle.length()); 				// Longueur du nom du fichier
					//toServer.write(cle.getBytes(codepageServeur));	// Nom du fichier
					//toServer.writeInt(buffer.length);				// Fichier
					//toServer.write(buffer, 0, buffer.length);
					//toServer.flush();
					//bufferManager.sendBuffer(toServer, Constantes.UPLOAD_FICHIER, cle, buffer);
					bufferToSend.setIdmessage(Constantes.UPLOAD_FICHIER);
					bufferToSend.addBuffer(cle, buffer);
					if( !linesManager.send(bufferToSend) ){
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
					}
				}
				f.close();
			}
			catch (Exception e)	{}

		}
//System.out.println("[SessionTransfert] FIN (demarreTelechargement) ");
	}

	/**
	 * V�rifie si on peut d�marrer une suppression de fichier
	 */
	private synchronized void demarreSuppr()
	{
//System.out.println("[SessionTransfert] (demarreSuppr) taille:" + listeFichierASupprimer.getHashMap().size());
		// Si pas de t�l�chargement en cours
		if (supprEnCours == false) 
		{
			String cle = (String)listeFichierASupprimer.getValue("0");
			if (cle == null) return;
//System.out.println("[SessionTransfert] (demarreSuppr) " + cle + " " + (String)listeFichierASupprimer.getObject(cle));			
			supprEnCours = true;
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Suppression de" + cle);
			//if (EnvoiMessageSocket(Constantes.SUPPRIME_FICHIER, cle) == Constantes.FALSE)
			//if( !bufferManager.sendBuffer(toServer, Constantes.SUPPRIME_FICHIER, cle) )
			bufferToSend.setIdmessage(Constantes.SUPPRIME_FICHIER);
			bufferToSend.addBuffer(cle);
			if( !linesManager.send(bufferToSend) ){
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
				supprEnCours = false;
			}
		}
//System.out.println("[SessionTransfert] FIN (demarreTelechargement) ");
	}

	/**
	 * Ajoute un dossier dans la liste
	 * ATTENTION � la casse suivant les OS (� corriger +tard)
	 */
	public void ContenuDossierArecup(String dossier)
	{
		String fichierNormalise=null;

//		System.out.println("[TransfertSession] (FichierArecupRT) " + fichier);
		// On effectue une v�rification du fichier � r�cup�rer
		if ((dossier == null) || (dossier.indexOf('@') != -1)) return;

		// Attend que la session Transfert soit bien d�marr� sinon pb avec le transfert du fichier des traductions
		waitOpenSession();

		synchronized(this)
		{
			fichierNormalise = dossier.replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR).trim();
//			System.out.println("[TransfertSession] (ContenuDossierArecup) " + fichierNormalise);    	    
			// On v�rifie qu'il ne soit pas d�j� dans la liste
			if (listeContenuDossierARecup.indexOf(fichierNormalise) == Constantes.FALSE)
				listeContenuDossierARecup.addLast(fichierNormalise);

			// S'il n'y en a qu'un dans la liste alors on d�clenche le t�l�chargement
//			System.out.println(" Nombre de fichier ds la liste: " + listeFichierArecup.size()  +" " +transfertEnCours);        
			if ((transfertDossierEnCours == Constantes.FALSE) && (listeContenuDossierARecup.size() != 0)) 
			{
				transfertDossierEnCours = Constantes.TRUE;
				log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande le listage de " + listeContenuDossierARecup.getFirst());
				//if (EnvoiMessageSocket(Constantes.DEMANDE_CONTENU_DOSSIER, (String)listeContenuDossierARecup.getFirst()) == Constantes.FALSE)
				//if( !bufferManager.sendBuffer(toServer, Constantes.DEMANDE_CONTENU_DOSSIER, (String)listeContenuDossierARecup.getFirst()) )
				bufferToSend.setIdmessage(Constantes.DEMANDE_CONTENU_DOSSIER); 
				bufferToSend.addBuffer((String)listeContenuDossierARecup.getFirst());
				if( !linesManager.send(bufferToSend) ){
					log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
					transfertDossierEnCours = Constantes.FALSE;
				}
			}
		}
//		System.out.println("FIN FichierArecupRT " + fichier);        
	}

	/**
	 * V�rifie si le dossier a bien �t� list�
	 */
	public String[] ContenuDossierRecup(String fichier)
	{
		int j=0;
		String fichierNormalise=null;
		String[] lstdossier=null;

		if (fichier == null) return null;
		fichierNormalise = fichier.trim(); 
//		System.out.println("[SessionTransfert] (ContenuDossierRecup) " + fichierNormalise+"|");     	
//		System.out.println("[SessionTransfert] (FichierRecupRT) avt taille de la liste: " + listeFichierArecup.size());            	

		// On v�rifie si on a r�cup�r� le fichier
		do
		{
			// On cherche dans la liste des fichiers d�j� r�cup�r�s
			if (listeContenuDossierRecup.containsKey(fichierNormalise))
			{
				j = -1;
				//synchronized(this) { return (String[])listeContenuDossierRecup.get(fichierNormalise); }
				lstdossier = (String[])listeContenuDossierRecup.get(fichierNormalise);
				listeContenuDossierRecup.remove(fichierNormalise);
				return lstdossier;
			}
		}
		while (j != -1); // On patiente tant que le fichier est dans la liste des fichiers � r�cup�rer

//		System.out.println("[SessionTransfert] (FichierRecupRT) apr taille de la liste: " + listeFichierArecup.size());
		lstdossier = (String[])listeContenuDossierRecup.get(fichierNormalise);
		listeContenuDossierRecup.remove(fichierNormalise);
		return lstdossier;
	}

	/**
	 * Fermeture de la session
	 */
	public void fermeture()
	{
		bufferToSend.setIdmessage(Constantes.STOP); 
		//bufferToSend.addBuffer(Constantes.STOP);
		if( !linesManager.send(bufferToSend) ){
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
		}

		/*
		if (listeFichierATelecharger != null)
			System.out.println("-listeFichierATelecharger-> " + listeFichierATelecharger.getHashMap().size());
		else
			System.out.println("-listeFichierATelecharger-> 0");
		if (listeFichierRTATelecharger != null)
			System.out.println("-listeFichierRTATelecharger-> " + listeFichierRTATelecharger.getHashMap().size());
		else
			System.out.println("-listeFichierRTATelecharger-> 0");
		if (listeFichierAUploader != null)
			System.out.println("-listeFichierAUploader-> " + listeFichierAUploader.getHashMap().size());
		else
			System.out.println("-listeFichierAUploader-> 0");
		if (listeFichierASupprimer != null)
			System.out.println("-listeFichierASupprimer-> " + listeFichierASupprimer.getHashMap().size());
		else
			System.out.println("-listeFichierASupprimer-> 0");
		if (listeContenuDossierARecup != null)
			System.out.println("-listeContenuDossierARecup-> " + listeContenuDossierARecup.size());
		else
			System.out.println("-listeContenuDossierARecup-> 0");
		if (listeContenuDossierRecup != null)
			System.out.println("-listeContenuDossierRecup-> " + listeContenuDossierRecup.size());
		else
			System.out.println("-listeContenuDossierRecup-> 0");
			*/
	}

	/**
	 * Indique si la session est connect�e ou pas
	 */
	public int isConnected()
	{
		return isConnect;
	}

	/**
	 * Retourne le dossier de travail
	 * @return
	 **/
	public String getDossierTravail()
	{
		return infoUser.getDossierTravail();
	}
	
	/**
	 * Initialise la liste des �v�nements pour la gestion des �dition
	 * @param evtEdition
	 */
	public void setEvenementEdition(ArrayListManager evtEdition)
	{
		evenementEdition = evtEdition;
	}
	
	/**
	 * Initialise la liste des �v�nements pour la gestion des �v�nements syst�mes (TRV, ...)
	 * @param evtEdition
	 *
	public void setEvenementSysteme(ArrayListManager evtSysteme) // TODO � revoir car ce n'est pas logique (dangereux en tout cas !!) 
	{
		evenementSysteme = evtSysteme;
	}*/

	public ArrayListManager getEvenementSysteme() 
	{
//System.out.println("-SessionTransfert-listener actifs->" + evenementSysteme.getNumberListeners());		
		return evenementSysteme;
	}

	/**
	 * D�marre la connexion de la session
	 */
	public void startConnexion()
	{
		bufferToSend.setIdmessage(Constantes.CONNEXION_OK);
		if (!linesManager.send(bufferToSend) ){
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
		}
	}

	public void treatBuffer(ByteBuffer bbuffer)
	{
		String chaine="";
		byte[] buffer = null;
		String message=null;

		try{
			
			int idmessage = bbuffer.getInt();
//System.out.println("[SessionTransfert] idmessage recu:" + idmessage);

				switch(idmessage)
				{
					// Le client se d�connecte <-----------------------------------------------
					case Constantes.STOP :
//System.out.println("transfert-STOP-");
						linesManager.removeSession2List(idSession);
						isConnect=Constantes.FALSE;
						isSession=Constantes.FALSE;
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + DECONNEXION_SESSION);
						break;

						// Le serveur nous renvoie une r�ponse ERREUR --------------------------------
					case Constantes.ERREUR :
//System.out.println("transfert-ERREUR-");
						idmessage = bbuffer.getInt();
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						try{ message = new String(buffer, 0, buffer.length, codepageServeur); }catch(UnsupportedEncodingException e){}
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "idmessage:" +idmessage + "Erreur:" + message);
						break;

						// Le serveur nous renvoie une r�ponse OK --------------------------------
					case Constantes.CONNEXION_OK :
//System.out.println("transfert-CONNEXION_OK-");
						//idThisSession = id;
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Connexion au serveur S�rie N (idThisSession:" + idSession + ")" );
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande l'ouverture de la session Transfert.");
						//EnvoiMessageSocket(Constantes.DEMANDE_OUVERTURE_TRANSFERT_SESSION, nomConfig + Constantes.SEPARATEUR_CHAINE_CHAR + infoUser.getId());
						//bufferManager.sendBuffer(toServer, Constantes.DEMANDE_OUVERTURE_TRANSFERT_SESSION, nomConfig + Constantes.SEPARATEUR_CHAINE_CHAR + infoUser.getId());
//System.out.println("*************************** DEMANDE_OUVERTURE_TRANSFERT_SESSION ******************");
						bufferToSend.setIdmessage(Constantes.DEMANDE_OUVERTURE_TRANSFERT_SESSION);
						bufferToSend.addBuffer(nomConfig + Constantes.SEPARATEUR_CHAINE_CHAR + infoUser.getId());
						if( !linesManager.send(bufferToSend) ){
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
						}
						break;

						// Le serveur nous demande si le client est toujours pr�sent --------------------------------
						/*
					case Constantes.PING :
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						message = new String(buffer, 0, buffer.length, infoUser.getCodepageServeur());
						System.out.println("-transfert-> Ping recu " + message);
						//EnvoiMessageSocket(Constantes.PONG, NOM_CLASSE + " " + infoUser.getProfil() + "|" + idThisSession);
						//bufferManager.sendBuffer(toServer, Constantes.PONG, NOM_CLASSE + " " + infoUser.getProfil() + "|" + idThisSession);
						bufferToSend.addBuffer(Constantes.DEMANDE_OUVERTURE_TRANSFERT_SESSION, nomConfig + Constantes.SEPARATEUR_CHAINE_CHAR + infoUser.getId());
						linesManager.send(bufferToSend);
						break;*/

						// La session a �t� accept� par le serveur ------------------------------------
					case Constantes.OUVERTURE_SESSION_OK :
//System.out.println("transfert-OUVERTURE_SESSION_OK-");
						idmessage = bbuffer.getInt();
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						try{ message = new String(buffer, 0, buffer.length, codepageServeur); }catch(UnsupportedEncodingException e){}
//System.out.println("-OUVERTURE_SESSION_OK->" + message);						
						isSession=Constantes.TRUE;
						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + message.replace(Constantes.SEPARATEUR_CHAINE_CHAR, Constantes.SEPARATEUR_CHAR));
						break;

					// Le serveur envoi un fichier -------------------------------------------------
					case Constantes.RECOIT_FICHIER_RT :
//System.out.println("transfert-RECOIT_FICHIER_RT-" + bbuffer.capacity());
						idmessage = bbuffer.getInt();
						// R�cup�ration des infos du fichier
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						try{ message = new String(buffer, 0, buffer.length, codepageServeur); }catch(UnsupportedEncodingException e){}
//						System.out.println("RECOIT_FICHIER_RT --> " + message);                                  
						// S'il y a eu une erreur lors de la r�cup�ration du fichier
						if (message.trim().endsWith("-1"))
						{
							chaine  = message.substring(0, message.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR)).trim();
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier non r�cup�r� : " + chaine);
							AjoutItemRTTelecharger(chaine, "9");
						}
						else // Sinon r�cup�ration du fichier + mise � jour de la liste RT
						{

							buffer = new byte[bbuffer.getInt()];
							bbuffer.get(buffer);

							chaine = cliRT.setFichier(message, buffer);
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier r�cup�r� : " + chaine);
							// S'il s'agit d'un panel, on v�rifie que ses fichiers auxiliaires soient pr�sents sur le client
							//if (chaine.toLowerCase().endsWith(".bin")) addListeFicPnl(chaine);
							AjoutItemRTTelecharger(chaine, "1");
						}
						transfertRTEnCours = false;
//						System.out.println("FIN RECOIT_FICHIER_RT");                                       
						break;

						// Le serveur envoi un fichier -------------------------------------------------
					case Constantes.RECOIT_FICHIER :
//System.out.println("transfert-RECOIT_FICHIER-");
						idmessage = bbuffer.getInt();
						// R�cup�ration des infos du fichier
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						try{ message = new String(buffer, 0, buffer.length, codepageServeur); }catch(UnsupportedEncodingException e){}
//System.out.println("RECOIT_FICHIER --> " + message);
						// S'il y a eu une erreur lors de la r�cup�ration du fichier
						if (idmessage < 0)
						{
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier non r�cup�r� : " + message);
							// On le supprime de la liste
							AjoutItemTelecharger(message, "9");
						}
						else // Sinon r�cup�ration du fichier
						{
							buffer = new byte[bbuffer.getInt()];
							bbuffer.get(buffer);

							// D�composition des infos
							String dossier = infoUser.getDossierTempUser() + File.separatorChar + message.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
//System.out.println("[SessionTransfert] (run) " + dossier);

							//  On enregistre le fichier
							try
							{
								DataOutputStream f = new DataOutputStream(new FileOutputStream(dossier));
								f.write(buffer, 0, buffer.length);
								f.close();
							}
							catch (Exception e)	{}
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Fichier r�cup�r� : " + dossier);
							// On marque le fichier comme t�l�charg�
							AjoutItemTelecharger(message, "1");
						}
						transfertEnCours = false;
						break;

						// Le serveur envoi le contenu d'un dossier -------------------------------------------------
					case Constantes.RECOIT_CONTENU_DOSSIER :
//System.out.println("transfert-RECOIT_CONTENU_DOSSIER-");
						idmessage = bbuffer.getInt();
						// R�cup�ration des infos du fichier
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						try{ message = new String(buffer, 0, buffer.length, codepageServeur); }catch(UnsupportedEncodingException e){}
//System.out.println("RECOIT_FICHIER --> " + message);                                  
						// S'il y a eu une erreur lors de la r�cup�ration du fichier
						if (idmessage < 0)
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Probl�me pendant le listage du dossier : " + message);

						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);

						// D�composition des infos
						int pos = message.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR);
						if (pos == -1)
							chaine = message.trim();
						else
							chaine = message.substring(0, pos);
//						System.out.println("RECOIT_CONTENU_DOSSIER --> " + chaine+"|");    				
						listeContenuDossierRecup.put(chaine, Constantes.splitString(message, Constantes.SEPARATEUR_CHAINE_CHAR));

						// On v�rifie s'il y a d'autres fichiers � r�cup�rer
//						System.out.println("RECOIT_CONTENU_DOSSIER --> " + chaine);                                  
						listeContenuDossierARecup.remove(chaine);
//						System.out.println("Nombre de fichier dans la liste : " + listeFichierArecup.size());
						if (listeContenuDossierARecup.size() != 0)
						{
							log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Demande la r�cup�raton de  " + listeContenuDossierARecup.get(0));
							//EnvoiMessageSocket(Constantes.DEMANDE_CONTENU_DOSSIER, (String)listeContenuDossierARecup.get(0));
							//bufferManager.sendBuffer(toServer, Constantes.DEMANDE_CONTENU_DOSSIER, (String)listeContenuDossierARecup.get(0));
							bufferToSend.setIdmessage(Constantes.DEMANDE_CONTENU_DOSSIER);
							bufferToSend.addBuffer((String)listeContenuDossierARecup.get(0));
							if( !linesManager.send(bufferToSend) ){
								log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + linesManager.getMsgErreur());
							}
						}
						else
							transfertDossierEnCours = Constantes.FALSE;
//						System.out.println("FIN RECOIT_FICHIER");                                       
						break;
						
					// Confirmation de la fin de l'upload d'un fichier <--------------------------------------------------------------
					case Constantes.UPLOAD_FICHIER :
//System.out.println("transfert-UPLOAD_FICHIER-");
						// R�cup�ration du nom du fichier et de sa destination 
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						chaine = new String(buffer, 0, buffer.length);
						idmessage = bbuffer.getInt();

						// On v�rifie s'il y a d'autres fichiers � uploader
						uploadEnCours = false;

						listeFichierAUploader.removeObject(chaine);
						//listeFichierAUploader.addObject(chaine, "1");

						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Upload du fichier termin�: " + chaine);
						break;

					// Suppression d'un fichier <--------------------------------------------------------------
					case Constantes.SUPPRIME_FICHIER :
//System.out.println("transfert-SUPPRIME_FICHIER-");
						// R�cup�ration du nom du fichier et de sa destination 
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						chaine = new String(buffer, 0, buffer.length);
						idmessage = bbuffer.getInt();

						// On v�rifie s'il y a d'autres fichiers � supprimer
						supprEnCours = false;

						listeFichierASupprimer.removeObject(chaine);
						//listeFichierASupprimer.addObject(chaine, "1");

						log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Suppression fichier(s) Ok.");
						break;

						
					// Le serveur renvoit la r�ponse du serveur d'�dition  -------------------------------------------------
					case Constantes.RECOIT_REQUETE_EDITION :
//System.out.println("transfert-RECOIT_REQUETE_EDITION-");
						// R�cup�ration des infos du fichier
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						//System.out.println("--> RECOIT_REQUETE_EDITION : " + msg);
						try{
							final String msg = new String(buffer, 0, buffer.length, codepageServeur);
							new Thread()
							{
								public void run()
								{
									evenementEdition.addObject(msg);
								}
							}.start();
						}
						catch(UnsupportedEncodingException e){}
//System.out.println("--> RECOIT_REQUETE_EDITION : Fin ");						
						break;

						// Le serveur ren�oit la r�ponse du serveur  --------------------------------------------------------
					case Constantes.RECOIT_REQUETE_SYSTEME :  
//System.out.println("transfert-RECOIT_REQUETE_SYSTEME-");
						// R�cup�ration des infos du fichier
						idmessage = bbuffer.getInt();
						buffer = new byte[bbuffer.getInt()];
						bbuffer.get(buffer);
						//System.out.println("--> RECOIT_REQUETE_SYSTEME : " + msgsys);
						try{
							final String msg = new String(buffer, 0, buffer.length, codepageServeur);
							new Thread()
							{
								public void run()
								{
									evenementSysteme.addObject(msg);
								}
							}.start();
						}
						catch(UnsupportedEncodingException e){}
//System.out.println("--> RECOIT_REQUETE_SYSTEME : Fin ");						
						break;

						// Valeur non trait�e <----------------------------------------------------
					default:
						break;
				}
		}
		catch(Exception e){
			log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Erreur treatBuffer " + e);
		}
	}

}