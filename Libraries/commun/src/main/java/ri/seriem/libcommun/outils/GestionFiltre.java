//=================================================================================================
//==>                                                                       04/12/2006 - 01/12/2009
//==> Description de l'objet Filtre poour les boites de dialogue
//==> A faire:
//=================================================================================================
package ri.seriem.libcommun.outils;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class GestionFiltre extends FileFilter
{
    private String[] Extensions=null;
    private String Description=null;

    /**
     * Constructeur
     * @param ext
     * @param lib
     */
    public GestionFiltre(String[] ext, String lib)
    {
        Extensions = ext;
        Description = lib;
    }

    /**
     * Filtre
     */
    public boolean accept(File f)
    {
        int i=0;
        
        if (f.isDirectory()) return true;
        
        String extension = getExtension(f);
        if ((extension != null) && (Extensions != null))
        {
            for (i=0; i<Extensions.length; i++)
                if(extension.equals(Extensions[i])) return true;
        }
        return false;
    }

    /**
     * Retourne le libell� du filtre choisie
     */
    public String getDescription()
    {
        return Description;
    }

    /**
     * Retourne l'extension du fichier
     * @param f
     * @return
     */
    public String getExtension(File f)
    {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1)
            ext = s.substring(i+1).toLowerCase();
        return ext;
    }

    /**
     * Retourne l'extension du filtre
     * @return
     */
    public String getFiltreExtension()
    {
        return Extensions[0];
    }
    
}
