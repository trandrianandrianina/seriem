--- J Walk translation table ---

(any) BRD (any)	Bordereau
12" C.	Papier 12 Pouces
ABFR.	Abfrage
AFFICH.	Affichage
ANNUL.	Annulation
Adr.Stock.	Adresse de stockage
Aff. � partir de :	Afficher � partir de
Ann.	Annuler
Annul	Annuler
C	Cr�dit
CR	Cr�dit
CREATION	Cr�ation
Coef.:	Coefficient
Compta.	Papier Comptable
D	D�bit
DB	D�bit
DELETTR.	D�lettrage
DUPLIC.	Duplication
DUPLIK.	Kopie
Demande FIN DE TRAV	Demande Fin de Travail
EN COURS	En cours
ERR	Erreur :
EUR	EURO
Ex.Pr�c.non clo.	Exercice Pr�c�dent non clos
Exer	Exercice:
Exer.Pr�c�d.clo.	Exercice Pr�c�dent Clos
Extens�	Extension
F. A4	Feuille A4
FEVR	F�vrier
HISTORIQ	Historique
INTERRO.	Interrogation
Inter	Interroger
JANV	Janvier
JUSTIFIC	Justification
LETTRAGE	Lettrage
LOT	Lot
Lot	Article lot
MAg R�cept	
MODIFIC.	Modification
Mag.R�cept	Magasin r�cepteur
Modif	Modifier
NStk	Non stock�
Op�.diverses	Op�ration diverses
P.U.Moy.Pond	P.U moyen pond�r�
Prix Unit.HT	Prix Unitaire HT
Pr�vision Mens.	Pr�vision mensuelle
RAPPEL	Rappel
R�vis	R�vision
R�vis.	R�vision
SELECT�.	S�lection
Sp�.	Article sp�cial
Stats	Statistiques
Stock Dispo.	Stock disponible
S�l.	S�lection
VISUALIS	Visualisation
Visualis.	Visualisation
Zon�...	Papier Zon�
�NDER.	�nderung
D000001L17	R�glement � payer
D000006L21	Cat�gorie Fournisseur
L000001	Magasin
L000001L12	Repr�sentant
L000001L20	R�glement � recevoir
L000001L25	     Type facture TVA  TP
L000002L12	Transporteur
L000002L18	Condition de vente
L000004L16	Cat�gorie Client
L000004L17	Zone G�ographique
L000005L17	Mode d'exp�dition
L000007L14	Canal de vente
L000034L29	(Depuis d�tail lignes de bon)
L000039	Section
L000075L17	Type repr�sentant
L000094L24	Condit� commissionnement
M000001	Famille
M000001L10	Substitut�
M000001L11	Interro ASB
M000001L12	Unit�s Stock
M000001L13	Type variante
M000001L14	Modifiez Zones
M000001L15	R�f�rence tarif
M000001L16	R�gle calcul PRV
M000001L17	Historique achats
M000001L18	Historique de PUMP
M000001L19	Mot de classement 1
M000001L20	Interro Fournisseurs
M000001L21	Regroupement Colisage
M000001L22	Informations Stocks
M000001L23	(Depuis Fiche Mat�riel)
M000001L24	C:         M:         T:
M000001L25	Gestion extensions lignes
M000001L26	Image et curseur m�moris�s
M000001L27	(Depuis Gestion des Stocks)
M000001L28	Commande invalide � ce stade
M000001L29	Fiche � dupliquer inexistante
M000001L30	Code Article D�but Obligatoire
M000001L31	Article en cours d'utilisation.
M000001L32	ETAT DES TRANSFERTS AUTOMATIQUES
M000001L33	Zones en rose pr�vues pour Invite
M000001L34	CONTROLE STOCK LOT / STOCK ARTICLE
M000001L35	Demande enregistr�e Traitement/LOTS
M000001L36	Code GENCOD Incorrect.......... Art:
M000001L37	(Tri sur 2�me mot classement article)
M000001L38	(depuis Fiche Article)             A/C
M000001L40	Surface                             Surf
M000001L50	Nombre US / 1 UT obligatoire... UT Unit� Transport
M000001L55	Traitement impossible ......... Erreur(s) sur ETB joint
M000001L56	Etablissement obligatoire...... ........................
M000001X	ARTICLE)
M000001XX	� l'unit�
M000002	Magasin
M000002L10	Aiguillage
M000002L11	Restriction
M000002L12	Unit�s Vente
M000002L13	Autres Gencod
M000002L14	Corrigez Zones
M000002L15	Mode Exp�dition
M000002L16	Gestion Libell�s
M000002L17	Gestion Mat�riels
M000002L18	Argument incorrect
M000002L19	Mot de classement 2
M000002L20	La Fiche existe d�j�
M000002L21	Conditionnement Stock
M000002L22	Tarifs
M000002L23	Pas de syst�me variable
M000002L24	Attention: FICHE ERRONEE
M000002L25	Code R�gie              N
M000002L26	(Depuis recherche article)
M000002L27	Pas de gestion des libell�s
M000002L28	M�mo. position hors fonction
M000002L29	Code Article d�but inexistant
M000002L30	Code Famille D�but Obligatoire
M000002L31	P�riode d'�dition non autoris�e
M000002L32	EDITION ARTICLES EN DEPOT CLIENT
M000002L33	HISTORIQUE DES MOUVEMENTS DE LOTS
M000002L34	PROPOSITION DE REAPPROVISIONNEMENT
M000002L35	COMPTE RENDU D'INVENTAIRE / MAGASIN
M000002L36	Mot de classement 2 existant... Art:
M000002L37	Sous-Famille d�but > Sous-Famille fin
M000002L40	Volume                              Vol.
M000002L56	DG/Etb non d�fini.............. ........................
M000002X	Cr�ation
M000002XX	D�sactiv�
M000003	Duplication
M000003L10	Remplacemt
M000003L11	Code Client
M000003L12	Sous-Famille
M000003L13	Date invalide
M000003L14	Codes Familles
M000003L15	Modification LD
M000003L16	Cr�ation Article
M000003L17	Autres Cnd Ventes
M000003L18	D�fil.Av/Ar ou 'X'
M000003L19	Fiche non annulable
M000003L20	La Fiche est annul�e
M000003L21	La Fiche n'existe pas
M000003L22	Conditions d'achats
M000003L23	(Depuis recherche Art.)
M000003L24	Attention: FICHE ANNULEE
M000003L25	R�gime Douanier         N
M000003L26	Au moins une r�ponse � OUI
M000003L27	Article D�but > Article Fin
M000003L28	Pas de traitement sp�cifique
M000003L29	Code Famille d�but inexistant
M000003L30	Ne saisir qu'un seul Code Prix
M000003L31	Date pour chiffrage obligatoire
M000003L35	Tri sur 2�me Mot Classement Article
M000003L36	Code GENCOD d�j� existant ..... Art:
M000003L37      Nb conditionnement mini doit �tre > 0
M000003L40	Valeur                              Val.
M000003L56	Code Article obligatoire....... ........................
M000003X	Modification
M000003XX	Syst.Var.
M000004	Article
M000004L10	< Err.Cod.
M000004L11	Trait./Lots
M000004L12	Nomenclature
M000004L13	(Par magasin)
M000004L14	Codes Magasins
M000004L15	Saisissez Zones
M000004L16	Article non loti
M000004L17	Fiche non annul�e
M000004L18	Appuyez sur ENTREE
M000004L19	ANOMALIE SYSTEME XX
M000004L20	Non trait� � ce jour
M000004L21	Faut-il enregistrer ?
M000004L22	Stats de Ventes
M000004L23	Op�ration non autoris�e
M000004L24	...D�but de la recherche
M000004L25	Types d'Extension Article
M000004L27	Famille D�but > Famille Fin
M000004L28	CHIFFRAGE GENERAL DES STOCKS
M000004L29	Ne saisir qu'une seule Option
M000004L30	Sous-Famille D�but obligatoire
M000004L31	ANALYSE DU STOCK THEORIQUE FIFO
M000004L35	Fournisseur D�but obligatoire ou **
M000004L36	Code Prix pour Chiffrage obligatoire
M000004L40	N� S�rie                            S�r.
M000004L56	Famille non d�finie............ ........................
M000004X	Interrogation
M000004XX	Fonctions
M000005	Annuler
M000005L10	R�afficher
M000005L12	Fournisseurs
M000005L14	Codes Articles
M000005L15	Prochain Code :
M000005L16	Conditions *TGnn
M000005L17	Code ou recherche
M000005L18	Veuillez patienter
M000005L19	Veuillez confirmer.
M000005L20	F4, Option ou Entr�e
M000005L21	Bar.Esp. et/ou Entr�e
M000005L22	Stats d'achats
M000005L23	Code Magasin inexistant
M000005L24	Etablissement non d�fini
M000005L25	COMPTE RENDU D'INVENTAIRE
M000005L27	ETAT DES STOCKS PAR MAGASIN
M000005L28	Option d'�dition obligatoire
M000005L29	(Par familles / Tri par lots)
M000005L30	Famille d�pendante inexistante
M000005L31	S�lectionner Famille ou Article
M000005L35	Fournisseur D�but > Fournisseur Fin
M000005L36	Edit. R�serv� uniquement sur Encours
M000005L40	Non Stk.                            NStk
M000005L56	Famille obligatoire en CREATION ........................
M000005X	� CMDER.
M000005XX	S�lection
M000006	Choisir
M000006L10	Interroger
M000006L12	infructueuse
M000006L14	(Par familles)
M000006L15	Liste Articles)
M000006L16	Etat des Stocks)
M000006L17	Chiffrage Stocks)
M000006L18	Aucun num�ro libre
M000006L19	Code Page incorrect
M000006L20	Codes Etablissements
M000006L21	Pas de Prix de Vente!
M000006L22	Chiffrage
M000006L23	Code Article inexistant
M000006L24	Article non nomenclature
M000006L25	Param�tre '   DG' �rron�.
M000006L27	Code Article fin inexistant
M000006L28	HISTORIQUE DETAILLE PAR LOTS
M000006L30	Sous-Famille d�but inexistante
M000006L35	Groupe ou Famille non d�fini / ETB.
M000006L40	Sp�cial                             Sp�.
M000006L56	Annulation Article g�r�/Achat.. ........................
M000006X	EN CMDE.
M000006XX	Extension
M000007	Extension
M000007L12	Code Famille
M000007L14	Groupe-Famille
M000007L16	Code Fournisseur
M000007L17	Article Incorrect
M000007L18	Unit� de transport
M000007L19	(R�el + Simulation)
M000007L20	Donnez Etablissement
M000007L21	Date D�but > Date Fin
M000007L22	Info. Diverses
M000007L23	1 seule option possible
M000007L24	Code Magasin Obligatoire
M000007L25	Date Chiffrage incorrecte
M000007L27	Code Famille fin inexistant
M000007L28	Sous-Famille fin inexistante
M000007L35	HISTORIQUE DES MOUVEMENTS DE STOCKS
M000007L40	Nomenc.                             Nmc.
M000007L56	Code Prix obligatoire.......... ........................
M000007X	Modifier
M000008	Tableur
M000008L12	Code Magasin
M000008L14	LISTE ARTICLES
M000008L17	Articles Confi�s)
M000008L18	Num�ro Fournisseur
M000008L19	Demande enregistr�e
M000008L20	CHIFFRAGE DES STOCKS
M000008L21	Date D�but incorrecte
M000008L22	Sp�cifique
M000008L23	ETAT GENERAL DES STOCKS
M000008L24	Date Fin > Date Maximale
M000008L25	xxxxxxxxxxxxxxxxxxxxxxxx)
M000008L27	Code Fournisseur Inexistant
M000008L35	(Par codes articles / Tri par lots)
M000008L40	Comment.                            Com.
M000008L56	Choisir Montant ou %........... ........................
M000008X	Libell�s
M000009L12	Code Article
M000009L18	Veuillez confirmer
M000009L19	Date Fin incorrecte
M000009L20	(Par codes articles)
M000009L21	D'ARTICLES EN RUPTURE
M000009L22	Gestion Libell�s
M000009L23	Code Famille Inexistant
M000009L24	POSITION ARTICLE A VENIR
M000009L25	Num�ro D�but > Num�ro Fin
M000009L27	Article d�but > article fin
M000009L40	Verrou.                             Verr
M000009L56	Montant sup�rieur � prix trait� ........................
M000009X	En cours
M00001	Unit�s
M000010L12	par articles
M000010L18	Client  inexistant
M000010L19	Recherche N� Client
M000010L20	Recherche Nom Client
M000010L21	Code D�but > Code Fin
M000010L22	Nomenclature
M000010L23	Code Magasin Inexistant
M000010L24	Transferts Automatiques)
M000010L25	Famille D�but inexistante
M000010L27	Famille d�but > famille fin
M000010L40	D�sact.                             D�s.
M000010L56	Erreur Codification Saison..... Exemple :  01.07 � 31.08
M000011L18	Magasin inexistant
M000011L19	Magasin obligatoire
M000011L20	Historique des lots)
M000011L22	Extension
M000011L23	Code Article Inexistant
M000011L24	Code Magasin obligatoire
M000011L27	(Par familles ou S/Famille)
M000011L40	Epuis�.                             Epu.
M000011L56	Erreur Codification Zone D�c... ........................
M000012L18	Appel Nomenclature
M000012L19	Code Date incorrect
M000012L20	ECARTS D'INVENTAIRES
M000012L22	12
M000012L23	Avec notion de comptage
M000012L24	Client D�but obligatoire
M000012L40	Sys.Var                             Sva.
M000012L56	Erreur Codification Date....... ........................
M000013L18	Codes Sous-Famille
M000013L19	Codes Sous-familles
M000013L22	13
M000013L23	XXX Famille inexistante
M000013L24	Article D�but inexistant
M000013L56	D�signation obligatoire........ ........................
M000014L19	(Par sous-familles)
M000014L22	14
M000014L24	Sous-Famille D�but > Fin
M000014L56	Unit� de Vente non d�finie..... ........................
M000015L22	15
M000015L56	Code TVA incorrect............. ........................
M000016L22	16
M000016L56	Prix de Vente nul ............. ........................
M000017L22	17
M000017L56	Coefficient Vente obligatoire.. ........................
M000018L22	18
M000018L56	Coefficient Vente inf�rieur � 1 ........................
M000019L22	19
M000019L56	Unit� Stock non d�finie........ ........................
M00002	ZP A01
M000020L22	20
M000020L56	Unit� vente non compatible avec top num�ro s�rie........
M000021L20      ** R�appro.possible
M000021L22	SELECTION OPTION
M000021L56	Nombre US / 1 UV obligatoire... Unit�s US=Stock UV=Vente
M000022L19      ** Stock � ce jour
M000022L22	Historique inventaires
M000022L56	R�f�rence incorrecte........... ........................
M000023L22	Calcul prix de revient
M000023L56	R�f�rence non compacte......... ........................
M000024L22	Histo. prix de revient
M000024L56	R�f�rence tarif non d�finie.... param�tre TA............
M000025L22	...Fin de la recherche
M000025L56	R�f�rence > � 13 caract�res.... ........................
M000026L22	Demande FIN DE TRAVAIL
M000026L56	Coefficients non justifi�s..... ........................
M000027L22	Rupture sur Stk Mini 0
M000027L56	Code incorrect................. ........................
M000028L22	Sur Conso. moyenne   1
M000028L56	Prix Vente (pr�pa) interdit.... ........................
M000029L22	R�appro. Manuel      2
M000029L56	Devise incorrecte.............. param�tre devise........
M00003	ZP A02
M000030L22	Sur conso. pr�vues   3
M000030L56	Saison incompl�te.............. D�but ET Fin ou rien....
M000031L22	Conso.moy.plafonn�e  4
M000031L56	Top arrondi sup�rieur � 6...... ........................
M000032L22	Date d�but obligatoire
M000032L56	D�lai incorrect................ ........................
M000033L22	Fournisseur inexistant
M000033L56	Unit� Achat non d�finie........ (Param. 'UN')...........
M000034L22	Fiche 'DG' in�xistante
M000034L56	Nombre UA / 1 UC obligatoire... Unit�s UA=Achat UC=Cde..
M000035L22	Etb. pilote inexistant
M000035L56	Un.de conditionnemt Stk non d�f (Param. 'UN')...........
M000036L22	Historique des Stocks)
M000036L56	Nombre US / 1 USC obligatoire.. UCS Conditionnemnt Stock
M000037L22	Date D�but Obligatoire
M000037L56	Code ABC obligatoire .......... (Utilisation Param.'TT')
M000038L22	Code Groupe Inexistant
M000038L56	Aucun prix..................... ........................
M000039L22	Analyse du stock FIFO)
M000039L56	FRS Interdit / Article Sp�cial. ........................
M00004	ZP A03
M000040L22	Fiche 'DG' inexistante
M000040L56	Article s�rie et lot interdit.. ........................
M000041L22	Recherche Code Magasin
M000041L56	Unit� de commande non d�finie.. ........................
M000042L22	Recherche Article/Code
M000042L56	Fournisseur non d�fini......... ........................
M000043L22	Code Client inexistant
M000043L56	Remise incorrecte.............. ........................
M000044L22	Code D�but obligatoire
M000044L56	Nombre US / 1 UC obligatoire .. ........................
M000045L22	Article Fin inexistant
M000045L56	% Cons. P�riode 1 + 2 > 100 ... ........................
M000046L22	par famille d'articles
M000046L56	Code CPR inexistant............ ........................
M000047L56	Pourcentage sup�rieur � 100.... ........................
M000048L56	Code Prix diff�rent de P,R,E... ........................
M000049L56	Substitution r�cursive......... ........................
M00005	ZP A04
M000050L56	Article de substitution inconnu ........................
M000051L56	Code Article/Lot Incorrect..... ........................
M000052L56	Code Type de Variante incorrect ........................
M000053L56	Code R�gie inexistant.......... Param�tre 'RE' .........
M000054L56	% Volume d'Alcool obligatoire.. ........................
M000055L56	Contenance obligatoire......... ........................
M000056L56	R�gime douanier incorrect ..... Param�tre 'RD' .........
M000057L56	Op�ration non autoris�e........ ........................
M000058L56	Zone personnalis�e non num�riq. (Param�tre ZP)..........
M000059L56	Zone personnalis�e inexistante. (Param�tre ZP)..........
M00006	ZP A05
M000060L56	Magasin non d�fini............. (Param�tre MA)..........
M000061L56	Mode Exp�dition non d�fini..... (Param�tre EX)..........
M000062L56	Zone personnalis�e obligatoire. (Param�tre ZP)..........
M000063L56	Regroup. Colisage non d�fini    (Param�tre RC)..........
M000064L56	Op�ration impossible .......... Art.Sp�cial ou d�sactiv�
M000065L56	ZP N� 5 Sur-conditionnement ... N'est pas un code unit�.
M000066L56	Sous-Famille non d�finie....... (Param�tre SF)..........
M000067L56	........................................................
M000068L56	Cl� Code Barre recalcul�e ..... ........................
M000069L56	Unit� pr�sentation non d�finie  param�tre UN ...........
M00007	Annulation
M000070L56	Syst�me variable non d�finie .. param�tre SV ...........
M000071L56	Taxe additionnelle non d�finie. param�tre TX............
M000072L56	Mot de classement 1 non d�fini. (F4 pour liste).........
M000073L56	Mot de classement 2 non d�fini. (F4.pour liste).........
M000074L56	Unit� transport non d�finie ... (Param. 'UN')...........
M00008	Dispo.
M00009	Epuis�
M0001	Total
M00010	DISPO.
M00011	Invite
M00012	Stocks
M00013	Tarifs
M00014	D�tail
M00015	Retour
M0002	Dispo
M0003	Interroger
M0004	Statistiques
M0005	Modifier
M0006	Annuler
M001	Code
M002	Art.
M003	N.C.
M004	Aide
M005	Lots
M006	Surf
M007	Vol.
M008	Val.
M009	S�r.
M01	SVP
M010	Non stock�
M011	Article sp�cial
M012	Nmc.
M013	Com.
M014	Verr
M015	D�s.
M016	Epu.
M017	SVa.
M02	SYS
M03	DAT
M04	Erreur :
M05	O.K
M06	!!!
M07	OUI
M08	NON
M09	Article lot
M10	...
M11	Fin
