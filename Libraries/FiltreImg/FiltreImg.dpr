program FiltreImg;
{$APPTYPE GUI}
uses Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs;

Function GfApiMainSessionConnect(SessionId: LongInt; AppName: String; CapBits: LongInt; ConMode: LongInt; WinHand: LongInt; Anchor: LongInt; var ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiMainSessionDisconnect(ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiScriptGlobalSet(ApiHand: LongInt; VarName: String; VarValue: String; VarTyp: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';

{$R *.RES}
const
    GF_OK : longint = 0;    // No error / OK
    GF_SCRIPT_VAR_STRING : longint = 0;
    GF_SCRIPT_VAR_NUMERIC : longint = 1;
    GF_EOF : longint = -1;  // End of file
    GF_FAIL : longint = 1;  // Failure
    GF_FALSE : longint = 0; // False
    GF_TRUE : longint = 1; // True

    GF_CONNECT_LOCKED : longint = 1; //One application to session.
    GF_CONNECT_API : longint = 2;    //Connect to API only. No session given
    GF_CONNECT_DLL : longint = 3;    //Connection is with DLL. Need not be done
    GF_CONENCT_NEW : longint = 4;    //Start a blind session when it doesn't exist

var
    api_hand : longint;
    ois_error : longint;
    nomvar : array[0..14] of char;
    buffer : array[0..255] of char;
    handlexplore : hwnd;
    listef: TSearchRec;
    retour, i : integer;
    chaine : string;
    sessionid : char;

begin
    if ParamCount < 1 then
        Application.Terminate;

    // Connexion � la session JWalk en cours
    sessionid := char(ParamStr(1)[1]);
//showmessage( IntToStr(ord(sessionid)) + ' / ' + sessionid);
    handlexplore := FindWindow(nil, 'filtreimg');
    api_hand := 0;
    ois_error := GfApiMainSessionConnect(ord(sessionid), 'filtreimg', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
//    ois_error := GfApiMainSessionConnect(ord('A'), 'RI', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
    if ois_error <> GF_OK then begin
//Showmessage('Pb de connexion ' + IntTostr(ois_error));
        Application.Terminate;
    end;

    // Raclage des images
    i := 0;
    retour := FindFirst(ParamStr(2)+'\*.*', faAnyFile, listef);
    while retour = 0 do begin
        if (listef.name <> '.') and (listef.name <> '..') and (listef.name <> 'Thumbs.db') then begin
            // Alimentation des variables globales dans JWALK
            Inc(i);
            chaine := 'image' + IntToStr(i);
            StrPCopy(nomvar, chaine);
            StrPCopy(buffer, listef.name);
//Showmessage(listef.name);
            ois_error := GfApiScriptGlobalSet(api_hand, nomvar, buffer, GF_SCRIPT_VAR_STRING);
            if ois_error <> GF_OK then begin
                Showmessage('Erreur lors de l''init de la variable JWALK');
            end;
        end;
        retour := FindNext(listef);
    end;
    FindClose(listef);
    // On renseigne le nombre d'images envoy�es
    StrPCopy(nomvar, 'NbrImage');
    StrPCopy(buffer, IntToStr(i));
    ois_error := GfApiScriptGlobalSet(api_hand, nomvar, buffer, GF_SCRIPT_VAR_NUMERIC);
    if ois_error <> GF_OK then
        Showmessage('Erreur lors de l''init de la variable JWALK');

    // D�connection de la session JWalk en cours
    ois_error := GfApiMainSessionDisconnect(api_hand);
end.
