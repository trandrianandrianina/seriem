// Ouvre la popup Windows afin de rechercher un fichier dans l'arborescence du disque,
// modifie la structure du fichier puis le copie dans l'IFS
// Param�tre:
//    entr�e :
//             la session
//             le chemin dans l'IFS
//             interaction avec JWalk

program mailing;
{$APPTYPE GUI}
uses
  Windows, Messages, SysUtils, Classes, ShellApi, StdCtrls, ShlObj, Graphics, Controls, Forms, Dialogs, (*DdeMan,*) FileCtrl;

Function GfApiMainSessionConnect(SessionId: LongInt; AppName: String; CapBits: LongInt; ConMode: LongInt; WinHand: LongInt; Anchor: LongInt; var ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiMainSessionDisconnect(ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiHostFieldPutData(ApiHand: LongInt; VarName: String; line: LongInt; VarValue: String; VarLen: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';

{$R *.RES}

const
  GF_OK : longint = 0;    // No error / OK
  GF_SCRIPT_VAR_STRING : longint = 0;
  GF_SCRIPT_VAR_NUMERIC : longint = 1;
  GF_EOF : longint = -1;  // End of file
  GF_FAIL : longint = 1;  // Failure
  GF_FALSE : longint = 0; // False
  GF_TRUE : longint = 1; // True

  GF_CONNECT_LOCKED : longint = 1; //One application to session.
  GF_CONNECT_API : longint = 2;    //Connect to API only. No session given
  GF_CONNECT_DLL : longint = 3;    //Connection is with DLL. Need not be done
  GF_CONENCT_NEW : longint = 4;    //Start a blind session when it doesn't exist

  MAXLEN=140;

var
  CheminApplication : String;
  CheminFicHTML : String;
  DossierReception : String;
  InterJwalk : Boolean;

procedure AnalyseHTML(fichier : String);
var
    Buffer1, Buffer2 : TStringList;
    i, j, posdeb, posfin : integer;
    chaine, chemin : String;
    ch1 : String;
begin
    // On se positionne dans le dossier o� se trouve le fichier HTML
    CheminFicHTML := ExtractFilePath(fichier);
    ChDir(CheminFicHTML);

    Buffer1 := TStringList.Create;
    Buffer2 := TStringList.Create;
    try
        // Chargement du fichier HTML original
        Buffer1.LoadFromFile(fichier);
        // On traite les lignes une par une
        for i := 0 to Buffer1.count-1 do begin
            chaine := Buffer1.Strings[i];
            Buffer1.Strings[i] := '';
            // On remplace les tabulations par des espaces
            chaine := StringReplace(chaine, #9, '        ', [rfReplaceAll, rfIgnoreCase]);
            // On recherche dans chaque ligne le mot cl� SRC="
            repeat
                posdeb := Pos('SRC="', UpperCase(chaine));
                if posdeb <> 0 then begin
                    posdeb := posdeb + Length('SRC="');
                    Buffer1.Strings[i] := Buffer1.Strings[i] + Copy(chaine, 1, posdeb-1);

                    // Extraction du chemin et du nom de l'image
                    chaine := Copy(chaine, posdeb, Length(chaine)-posdeb+1);
                    posfin := Pos('"', chaine);
//Showmessage('1� '+chaine + ' ' + IntToStr(posfin));
                    // On transforme le chemin en chemin windows
                    chemin := StringReplace(Copy(chaine, 1, posfin-1), '/', '\', [rfReplaceAll, rfIgnoreCase]);
                    // On remplace les %20 du format html en ' '
                    chemin := StringReplace(chemin, '%20', ' ', [rfReplaceAll, rfIgnoreCase]);
                    // On recup�re le reste de la chaine
//Showmessage('2� '+chemin);
                    chaine := Copy(chaine, posfin+1, Length(chaine)-posfin);

                    // Analyse du chemin et du nom de l'image
                    if (Pos('HTTP:', UpperCase(chemin)) = 0) and (Pos('ABOUT:', UpperCase(chemin)) = 0) then begin
                        // Extraction du nom de l'image
                        j := Pos('FILE:', UpperCase(chemin));
                        if j <> 0 then begin
                            j := j + Length('FILE:');
                            chemin := Copy(chemin, j, Length(chemin)-j+1);
                        end;
                        // Copie de l'image dans le dossier reception
                        ch1 := DossierReception + '\' + ExtractFileName(chemin);
                        CopyFile(@chemin[1], @ch1[1], False);
                        chemin := ExtractFileName(chemin);
                        Buffer1.Strings[i] := Buffer1.Strings[i] + 'cid:' + chemin + '"'; end
                    else
                        Buffer1.Strings[i] := Buffer1.Strings[i] + chemin + '"';
                end;
            until posdeb = 0;
            Buffer1.Strings[i] := Buffer1.Strings[i] + chaine;

            // On v�rifie la longueur de la ligne avant la copie dans Buffer2
            chaine := Buffer1.Strings[i];
            if Length(chaine) > MAXLEN then
                repeat
                    Buffer2.Add(Copy(chaine, 1, MAXLEN) + '         -');
                    chaine := Copy(chaine, MAXLEN+1, Length(chaine)-MAXLEN);
                until Length(chaine) <= MAXLEN;
            Buffer2.Add(chaine);
        end;
        // On enregistre le fichier HTML modifi�
        Buffer2.SaveToFile(DossierReception + '\' + ExtractFileName(fichier));
    finally
        Buffer1.Free;
        Buffer2.Free;
    end;

    // On se repositionne dans le dossier de l'application
    ChDir(CheminApplication);
end;

var
  api_hand : longint;
  ois_error : longint;
  nomvar : array[0..14] of char;
  buffer : array[0..255] of char;
  handlexplore : hwnd;
  sr: TSearchRec;

  OD_FichierHTML : TOpenDialog;

  i, limite : Integer;
  chaine : string;
  repertoire : string;
  retour : boolean;
  sessionid : char;

begin
    repertoire := GetCurrentDir;
    chaine := '';
    retour := false;

    // Lecture des param�tres
    if ParamCount = 0 then
        Application.Terminate;

    // Interaction avec des variables JWalk
    InterJwalk := True;
    if ParamCount <> 3 then
        InterJwalk := False
    else
        if Trim(ParamStr(3)) = '' then
            InterJwalk := False;

    // Connexion � la session JWalk en cours
    sessionid := char(ParamStr(1)[1]);
    handlexplore := FindWindow(nil, 'mailing');
    api_hand := 0;
    ois_error := GfApiMainSessionConnect(ord(sessionid), 'RI', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
    if ois_error <> GF_OK then
        Application.Terminate;

    DossierReception := ParamStr(2);
    CheminApplication := ExtractFilePath(Application.EXEName);

    // On v�rifie que le chemin existe et qu'il soit vide
    if not DirectoryExists(DossierReception) then begin
        MessageDlg('Le dossier n''existe pas.', mtError, [mbOk], 0);
        ois_error := GfApiMainSessionDisconnect(api_hand);
        Exit;
    end;
    if FindFirst(DossierReception + '\*.*', faAnyFile, sr) = 0 then begin
        while FindNext(sr) = 0 do begin
            if (sr.Name = '.') or (sr.Name = '..') then Continue
            else begin
                MessageDlg('Le dossier n''est pas vide.' +#10+#13+'Soit vous supprimez le contenu de ce dossier,' +#10+#13+'soit vous changez le nom du dossier.', mtWarning, [mbOk], 0);
                //ShellExecute(Handle, 'open', DossierReception, nil, nil, SW_SHOWNORMAL);
                chaine := 'explorer ' + DossierReception;
                WinExec(@chaine[1], SW_SHOWNORMAL);
                ois_error := GfApiMainSessionDisconnect(api_hand);
                Exit;
            end;
        end;
        FindClose(sr);
    end;

    // Ouvre la popup pour rechercher le fichier
    OD_FichierHTML := TOpenDialog.Create(nil);
    OD_FichierHTML.Title := 'Chercher le fichier HTML';
    OD_FichierHTML.Filter := 'Fichiers HTML|*.htm;*.html';
    OD_FichierHTML.Execute;
    if Trim(OD_FichierHTML.FileName) <> '' then begin
        AnalyseHTML(OD_FichierHTML.FileName);
        // Signale � JWALK qu'il s'agit d'un mailing
        if InterJwalk then begin
            ois_error := GfApiHostFieldPutData(api_hand, 'MAILG', 0, 'X', 1);
            if ois_error <> GF_OK then
                ShowMessage('Erreur lors de l''init de la variable JWALK');
        end;
    end;

    // D�connection de la session JWalk en cours
    ois_error := GfApiMainSessionDisconnect(api_hand);
end.
