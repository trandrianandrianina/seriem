unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gauges, StdCtrls, ExtCtrls;

Function GfApiMainSessionConnect(SessionId: LongInt; AppName: String; CapBits: LongInt; ConMode: LongInt; WinHand: LongInt; Anchor: LongInt; var ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiMainSessionDisconnect(ApiHand: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';
Function GfApiScriptGlobalSet(ApiHand: LongInt; VarName: String; VarValue: String; VarTyp: LongInt): LongInt; stdcall; far; external '..\gf9api.dll';

type
  TF_Main = class(TForm)
    G_Fichiers: TGauge;
    L_Info: TLabel;
    L_Resultat: TLabel;
    T_Debut: TTimer;
    T_Fin: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure T_DebutTimer(Sender: TObject);
    procedure T_FinTimer(Sender: TObject);
  private
    { D�clarations priv�es}
  public
    { D�clarations publiques}
    nbrfichier : integer;
    fichier : array[1..2,1..5] of string;
    sessionid : char;
    chemin : string;
  end;

const
    nbrmaxfichier = 500;

    GF_OK : longint = 0;    // No error / OK
    GF_SCRIPT_VAR_STRING : longint = 0;
    GF_SCRIPT_VAR_NUMERIC : longint = 1;
    GF_EOF : longint = -1;  // End of file
    GF_FAIL : longint = 1;  // Failure
    GF_FALSE : longint = 0; // False
    GF_TRUE : longint = 1; // True

    GF_CONNECT_LOCKED : longint = 1; //One application to session.
    GF_CONNECT_API : longint = 2;    //Connect to API only. No session given
    GF_CONNECT_DLL : longint = 3;    //Connection is with DLL. Need not be done
    GF_CONENCT_NEW : longint = 4;    //Start a blind session when it doesn't exist

var
    F_Main: TF_Main;
    api_hand : longint;
    ois_error : longint;
    nomvar : array[0..14] of char;
    buffer : array[0..255] of char;
    handlexplore : hwnd;

implementation

{$R *.DFM}


//------------------------------------------------------------------------------
//--> Pr�paration avant traitement
//------------------------------------------------------------------------------
procedure TF_Main.FormCreate(Sender: TObject);
var
    i : integer;
begin
    if ParamCount < 2 then begin
        Application.Terminate;
    end;
    nbrfichier := 0;

    sessionid := char(ParamStr(1)[1]);
    chemin := ParamStr(2);
    // Chargement des fichiers � copier
    for i := 3 to ParamCount do begin
        if Trim(ParamStr(i)) <> '' then begin
            Inc(nbrfichier);
            fichier[1,nbrfichier] := ParamStr(i);
            fichier[2,nbrfichier] := chemin + '\\' + ExtractFileName(ParamStr(i));
        end;
    end;

    if nbrfichier = 0 then begin
      // Connexion � la session JWalk en cours
      handlexplore := FindWindow(nil, 'cpyfic');
      api_hand := 0;
      ois_error := GfApiMainSessionConnect(ord(sessionid), 'cpyfic', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
      if ois_error = GF_OK then begin
        StrPCopy(nomvar, 'attente');
        StrPCopy(buffer, '0');
        ois_error := GfApiScriptGlobalSet(api_hand, nomvar, buffer, GF_SCRIPT_VAR_NUMERIC);
        if ois_error <> GF_OK then begin
            Showmessage('Erreur lors de l''init de la variable JWALK');
        end;
        // D�connection de la session JWalk en cours
        ois_error := GfApiMainSessionDisconnect(api_hand);
        Application.Terminate;
        end;
      end
    else
        T_Debut.Enabled := True;

    // On affiche les infos dans la fen�tre
    L_Info.Caption := 'Il y a ' + IntToStr(nbrfichier) + ' fichier(s) � traiter.';
    L_Resultat.Caption := 'Traitement en cours...';
    G_Fichiers.MaxValue := nbrfichier;
end;

//------------------------------------------------------------------------------
//--> Attend une seconde avant de lancer le traitement
//------------------------------------------------------------------------------
procedure TF_Main.T_DebutTimer(Sender: TObject);
var
    i : integer;
begin
    // On stoppe le timer
    T_Debut.Enabled := false;

    for i := 1 to nbrfichier do begin
        if (FileExists(fichier[1,i])) and (not FileExists(fichier[2,i])) then
            if CopyFile(PChar(fichier[1,i]), PChar(fichier[2,i]), False) = False then begin
                ShowMessage('Probl�me lors de la copie du fichier ' + ExtractFileName(fichier[1,i])); end
            else
                F_Main.G_Fichiers.AddProgress(1);
    end;

    T_Fin.Enabled := true;
end;

//------------------------------------------------------------------------------
//--> Ferme l'application deux seconde apr�s la fin du travail
//------------------------------------------------------------------------------
procedure TF_Main.T_FinTimer(Sender: TObject);
begin
  // Connexion � la session JWalk en cours
  handlexplore := FindWindow(nil, 'cpyfic');
  api_hand := 0;
  ois_error := GfApiMainSessionConnect(ord(sessionid), 'cpyfic', $FFFFFFFF, GF_CONNECT_DLL, handlexplore, HINSTANCE, api_hand);
  if ois_error <> GF_OK then begin
      Application.Terminate;
  end;
  StrPCopy(nomvar, 'attente');
  StrPCopy(buffer, '0');
  ois_error := GfApiScriptGlobalSet(api_hand, nomvar, buffer, GF_SCRIPT_VAR_NUMERIC);
  if ois_error <> GF_OK then begin
      Showmessage('Erreur lors de l''init de la variable JWALK');
  end;

  // D�connection de la session JWalk en cours
  ois_error := GfApiMainSessionDisconnect(api_hand);

  Application.Terminate;
end;

end.
