INSERT INTO vue VALUES (4,"images/devis.png","clients?vue=4", "<span class='accueil'><span class='superf'>Clients: les </span>devis</span><span class='metier'><span class='superf'>Vue </span>devis</span>",6, "Devis client", 1, '', 1);;;

INSERT INTO vue_catalogue VALUES (1,6);;;

UPDATE parametre SET para_libelle = 'SQLITE' WHERE para_id = 5;;;

INSERT INTO fichier VALUES ('10','PGVMEBCM','cv','Ent�te bons de vente','','');;;

UPDATE fichier SET fichier_descri = "Ent�te bons d'achat" WHERE fichier_id = 6;;;

UPDATE fichier SET fichier_descri = "Lignes bons d'achat" WHERE fichier_id = 7;;;

INSERT INTO zone VALUES (74,10,1,'E1COD','Code ERL',1,1,'','Code ERL',-1,0,0,'','');;;

INSERT INTO zone VALUES (75,10,2,'E1ETB','Code �tablissement',3,1,'','Code �tablissement',-1,0,0,'','');;;

INSERT INTO zone VALUES (76,10,3,'E1NUM','Num�ro',6,1,'','Num�ro',0,0,1,'','');;;

INSERT INTO zone VALUES (77,10,4,'E1SUF','Suffixe de bon',1,1,'','Suffixe de bon',-1,0,1,'','');;;

INSERT INTO zone VALUES (78,10,0,'E1CLLP','Code client livr�',6,0,'','Code client livr�',0,0,0,'','');;;

INSERT INTO zone VALUES (79,10,0,'E1CLLS','Suffixe client livr�',3,0,'','Suffixe client livr�',0,0,0,'','');;;

INSERT INTO zone VALUES (80,10,0,'E1TOP','Top syst�me',1,1,'','Top syst�me',-1,0,1,'','');;;

INSERT INTO zone VALUES (81,10,0,'E1ETA','Etat du bon',1,1,'','Etat du bon',-1,0,1,'','');;;

INSERT INTO zone VALUES (82,1,0,'DEVISACTIFS','Nombre devis actifs', 6, 0, "(SELECT COUNT(E1NUM) FROM <code>CODEBIBLI</code>.PGVMCLIM c LEFT OUTER JOIN <code>CODEBIBLI</code>.PGVMEBCM ON c.CLCLI = E1CLLP AND c.CLLIV = E1CLLS AND c.CLETB = E1ETB WHERE c.CLCLI = cl.CLCLI AND c.CLLIV = cl.CLLIV AND E1COD = 'D' AND E1TOP = 0 AND E1ETA<= 2) ",'Nombre devis actifs',-1,0,1,'','');;;

INSERT INTO zone VALUES (83,1,0,'CDEACTIVES','Nombre commandes actives', 6, 0, "(SELECT COUNT(E1NUM) FROM <code>CODEBIBLI</code>.PGVMCLIM c LEFT OUTER JOIN <code>CODEBIBLI</code>.PGVMEBCM ON c.CLCLI = E1CLLP AND c.CLLIV = E1CLLS AND c.CLETB = E1ETB WHERE c.CLCLI = cl.CLCLI AND c.CLLIV = cl.CLLIV AND E1COD = 'E' AND E1TOP = 0 AND E1ETA<= 4) ",'Nombre commandes actives',-1,0,1,'','');;;

INSERT INTO zone VALUES (84,10,0,'E1RCC','R�f. longue',18,0,'','R�f�rence longue',0,0,0,'','');;;

INSERT INTO zone VALUES (85,10,0,'E1NCC','R�f�rence courte',8,0,'','R�f�rence courte',0,0,0,'','');;;

INSERT INTO zone VALUES (86,10,0,'E1DLP','Date de livraison pr�vue',7,0,'','Date de livraison pr�vue',0,0,4,'','');;;

INSERT INTO zone VALUES (87,10,0,'E1DAT2','Validit� devis',7,0,'','Validit� devis',0,0,4,'','');;;

INSERT INTO zone VALUES (88,10,0,"E1TTC","Montant TTC",9,0,"","Montant TTC",-1,0,2,'','');;;

INSERT INTO liaison_fichier VALUES (4,1,10,75,'','');;;

INSERT INTO liaison_fichier VALUES (5,1,10,78,'','');;;

INSERT INTO liaison_fichier VALUES (6,1,10,79,'','');;;

INSERT INTO zones_vues VALUES(6,7,1,2,1,0,1,2);;;

INSERT INTO zones_vues VALUES(6,8,2,2,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,9,3,2,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,10,4,2,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,11,5,2,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,14,6,2,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,4,-1,0,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,5,-1,0,2,0,0,2);;;

INSERT INTO zones_vues VALUES(6,6,-1,0,0,0,0,2);;;

INSERT INTO zones_vues VALUES(6,82,-1,0,3,0,0,2);;;

CREATE TABLE vues_fichier (
  vue_id		int not null,
  fichier_id		int not null,
  fic_from text,
  fic_where text, 	
  PRIMARY KEY (vue_id,fichier_id),
  FOREIGN KEY(vue_id) REFERENCES vue(vue_id),
  FOREIGN KEY(fichier_id) REFERENCES fichier(fichier_id)
);;;
	
CREATE INDEX index_vues_x ON vues_fichier(vue_id);;;
CREATE INDEX index_fichier_x ON vues_fichier(fichier_id);;;

INSERT INTO vues_fichier VALUES (6,10," DIGITS(cv.E1NUM)||DIGITS(cv.E1SUF) = (SELECT DIGITS(E1NUM)||(E1SUF) FROM <code>CODEBIBLI</code>.PGVMEBCM cv WHERE E1COD = 'D' AND E1TOP = 0 AND E1ETB = cl.CLETB AND E1CLLP = cl.CLCLI AND E1CLLS = cl.CLLIV AND E1ETA <= 2 ORDER BY E1NUM DESC, E1SUF DESC FETCH FIRST 1 ROWS ONLY)",'');;;         

INSERT INTO zones_vues VALUES (6,76,0,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (6,77,1,1,0,0,0,1);;;

INSERT INTO zones_vues VALUES (6,78,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (6,79,-1,0,0,0,0,1);;;

INSERT INTO zones_vues VALUES (6,84,2,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (6,85,3,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (6,87,4,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (6,88,5,1,0,0,0,1);;;

INSERT INTO vue VALUES (-1,'','','',7,'Liste de devis',4,NULL,'');;;

INSERT INTO zones_vues VALUES (7,76,-1,0,1,0,0,10);;;
INSERT INTO zones_vues VALUES (7,77,-1,0,2,0,0,10);;;
INSERT INTO zones_vues VALUES (7,84,-1,0,3,0,0,10);;;
INSERT INTO zones_vues VALUES (7,87,-1,0,4,0,0,10);;;
INSERT INTO zones_vues VALUES (7,88,-1,0,5,0,0,10);;;

UPDATE vue SET module = 7 WHERE vue_id = 6;;;

INSERT INTO vue VALUES (5,"images/commandes.png","clients?vue=5", "<span class='accueil'><span class='superf'>Clients: les </span>commandes</span><span class='metier'><span class='superf'>Vue </span>commandes </span>",8, "Commandes client", 1, '', 1);;;

INSERT INTO vue_catalogue VALUES (1,8);;;

INSERT INTO zones_vues VALUES(8,7,1,2,1,0,1,2);;;
INSERT INTO zones_vues VALUES(8,8,2,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,9,3,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,10,4,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,11,5,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,14,6,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,4,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,5,-1,0,2,0,0,2);;;
INSERT INTO zones_vues VALUES(8,6,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(8,83,-1,0,3,0,0,2);;;
INSERT INTO zones_vues VALUES (8,76,0,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,77,1,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,78,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,79,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,84,2,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,85,3,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,86,4,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (8,88,5,1,0,0,0,1);;;

INSERT INTO vues_fichier VALUES (8,10," DIGITS(cv.E1NUM)||DIGITS(cv.E1SUF) = (SELECT DIGITS(E1NUM)||(E1SUF) FROM <code>CODEBIBLI</code>.PGVMEBCM cv WHERE E1COD = 'E' AND E1TOP = 0 AND E1ETB = cl.CLETB AND E1CLLP = cl.CLCLI AND E1CLLS = cl.CLLIV AND E1ETA <= 4 ORDER BY E1NUM DESC, E1SUF DESC FETCH FIRST 1 ROWS ONLY)",'');;;         

INSERT INTO vue VALUES (-1,'','','',9,'Liste de commandes',4,NULL,'');;;

INSERT INTO zones_vues VALUES (9,76,-1,0,1,0,0,10);;;
INSERT INTO zones_vues VALUES (9,77,-1,0,2,0,0,10);;;
INSERT INTO zones_vues VALUES (9,84,-1,0,3,0,0,10);;;
INSERT INTO zones_vues VALUES (9,86,-1,0,4,0,0,10);;;
INSERT INTO zones_vues VALUES (9,88,-1,0,5,0,0,10);;;

UPDATE vue SET module = 9 WHERE vue_id = 8;;;

UPDATE constante SET const_valeur = '122' WHERE const_cle = 'VERSION';;;