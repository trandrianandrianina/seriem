INSERT INTO zones_vues VALUES (12,90,-1,0,0,0,0,2);;;

UPDATE zone SET zone_code = "E1THTL", zone_libelle = "Montant HT", zone_descri = "Montant HT" WHERE zone_id = 88;;;

ALTER TABLE metier ADD metier_actif int;;;

UPDATE metier SET metier_actif = 1;;;

INSERT INTO metier VALUES(3,8,"images/metier_contact.png","Contacts",0);;;
INSERT INTO metier VALUES(4,10,"images/metier_bons.png","Commandes/factures",0);;;

INSERT INTO zones_vues VALUES (11,74,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,75,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,76,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,77,-1,0,0,0,0,10);;;

INSERT INTO liaison_fichier VALUES (75,10,1,4,'','');;;
INSERT INTO liaison_fichier VALUES (78,10,1,5,'','');;;
INSERT INTO liaison_fichier VALUES (79,10,1,6,'','');;;

INSERT INTO zones_vues VALUES (11,7,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,8,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,9,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,10,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,11,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (11,14,1,2,0,0,0,10);;;

INSERT INTO zones_vues VALUES (11,85,2,1,0,0,0,10);;;
UPDATE zones_vues SET ordre_zones = 0, bloc_zones = 1 WHERE vue_id = 11 AND zone_id = 89;;;
UPDATE zones_vues SET ordre_zones = 1, bloc_zones = 1 WHERE vue_id = 11 AND zone_id = 84;;;
UPDATE zones_vues SET ordre_zones = 3, bloc_zones = 1 WHERE vue_id = 11 AND zone_id = 93;;;
UPDATE zones_vues SET ordre_zones = 4, bloc_zones = 1 WHERE vue_id = 11 AND zone_id = 88;;;

INSERT INTO vue VALUES (-1,'','','ligne',14,'lignes',4,NULL,'');;;

UPDATE vue SET module = 14 WHERE vue_id = 11;;;
UPDATE vue SET module = 14 WHERE vue_id = 7;;;
UPDATE vue SET module = 14 WHERE vue_id = 9;;;
UPDATE vue SET module = 14 WHERE vue_id = 13;;;

INSERT INTO fichier VALUES (11,'PGVMLBCM','li','Lignes',''," (li.L1ERL = 'C' OR li.L1ERL = 'S') ");;;

INSERT INTO zone VALUES (96,11,1,"L1COD","Code ERL",1,1,"","Code ERL",-1,0,0,'','');;;
INSERT INTO zone VALUES (97,11,2,"L1ETB","Code �tablissement",3,1,"","Code �tablissement",-1,0,0,'','');;;
INSERT INTO zone VALUES (98,11,3,"L1NUM","Num�ro",6,1,"","Num�ro de bon",0,0,1,'','');;;
INSERT INTO zone VALUES (99,11,4,"L1SUF","Suffixe",1,1,"","Suffixe",-1,0,1,'','');;;
INSERT INTO zone VALUES (100,11,0,"L1ART","Code article",20,1,"","Code article",0,0,0,'','');;;
INSERT INTO zone VALUES (101,11,0,"L1QTE","Quantit�",10,1,"","Quantit� command�e",-1,0,1,'','');;;
INSERT INTO zone VALUES (102,11,0,"L1UNV","Unit�",2,1,"","Unit� de vente",-1,0,3,'','');;;
INSERT INTO zone VALUES (103,11,0,"L1PVC","Prix de vente",9,1,"","Prix de vente calcul�",-1,0,2,'','');;;

INSERT INTO zones_vues VALUES (14,85,2,1,0,0,0,10);;;

INSERT INTO liaison_fichier VALUES (74,10,11,96,'','');;;
INSERT INTO liaison_fichier VALUES (75,10,11,97,'','');;;
INSERT INTO liaison_fichier VALUES (76,10,11,98,'','');;;
INSERT INTO liaison_fichier VALUES (77,10,11,99,'','');;;

INSERT INTO zones_vues VALUES (14,74,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (14,75,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (14,76,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (14,77,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (14,100,-1,0,1,0,0,10);;;
INSERT INTO zones_vues VALUES (14,101,-1,0,2,0,0,10);;;
INSERT INTO zones_vues VALUES (14,102,-1,0,3,0,0,10);;;
INSERT INTO zones_vues VALUES (14,103,-1,0,4,0,0,10);;;

INSERT INTO zone VALUES (104,10,0,'E1FAC','Date facturation',7,0,'','Date de facturation',0,0,4,'','');;;

UPDATE zones_vues SET ordre_zones = 6 WHERE vue_id = 12 AND zone_id = 88;;;
INSERT INTO zones_vues VALUES (12,104,5,1,0,0,0,1);;;

UPDATE zones_vues SET zone_id = 104 WHERE vue_id = 13 AND zone_id = 93;;;

UPDATE vue SET vue_name = "devis client" WHERE vue_id = 7;;;
UPDATE vue SET vue_name = "commandes client" WHERE vue_id = 9;;;
UPDATE vue SET vue_name = "bons de livraison" WHERE vue_id = 11;;;
UPDATE vue SET vue_name = "factures client" WHERE vue_id = 13;;;

UPDATE vue SET vue_label = "devis" WHERE vue_id = 7;;;
UPDATE vue SET vue_label = "commande" WHERE vue_id = 9;;;
UPDATE vue SET vue_label = "bon de livraison" WHERE vue_id = 11;;;
UPDATE vue SET vue_label = "facture" WHERE vue_id = 13;;;

INSERT INTO zones_vues VALUES (7,74,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,75,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,76,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,77,-1,0,0,0,0,10);;;

INSERT INTO zones_vues VALUES (7,7,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,8,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,9,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,10,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,11,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (7,14,1,2,0,0,0,10);;;

INSERT INTO zones_vues VALUES (7,85,2,1,0,0,0,10);;;
UPDATE zones_vues SET ordre_zones = 0, bloc_zones = 1 WHERE vue_id = 7 AND zone_id = 89;;;
UPDATE zones_vues SET ordre_zones = 1, bloc_zones = 1 WHERE vue_id = 7 AND zone_id = 84;;;
UPDATE zones_vues SET ordre_zones = 3, bloc_zones = 1 WHERE vue_id = 7 AND zone_id = 87;;;
UPDATE zones_vues SET ordre_zones = 4, bloc_zones = 1 WHERE vue_id = 7 AND zone_id = 88;;;

INSERT INTO zones_vues VALUES (9,74,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,75,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,76,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,77,-1,0,0,0,0,10);;;

INSERT INTO zones_vues VALUES (9,7,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,8,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,9,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,10,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,11,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (9,14,1,2,0,0,0,10);;;

INSERT INTO zones_vues VALUES (9,85,2,1,0,0,0,10);;;
UPDATE zones_vues SET ordre_zones = 0, bloc_zones = 1 WHERE vue_id = 9 AND zone_id = 89;;;
UPDATE zones_vues SET ordre_zones = 1, bloc_zones = 1 WHERE vue_id = 9 AND zone_id = 84;;;
UPDATE zones_vues SET ordre_zones = 3, bloc_zones = 1 WHERE vue_id = 9 AND zone_id = 86;;;
UPDATE zones_vues SET ordre_zones = 4, bloc_zones = 1 WHERE vue_id = 9 AND zone_id = 88;;;

INSERT INTO zones_vues VALUES (13,74,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,75,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,76,-1,0,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,77,-1,0,0,0,0,10);;;

INSERT INTO zones_vues VALUES (13,7,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,8,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,9,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,10,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,11,1,2,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,14,1,2,0,0,0,10);;;

INSERT INTO zones_vues VALUES (13,85,2,1,0,0,0,10);;;
INSERT INTO zones_vues VALUES (13,93,3,1,0,0,0,10);;;
UPDATE zones_vues SET ordre_zones = 0, bloc_zones = 1 WHERE vue_id = 13 AND zone_id = 89;;;
UPDATE zones_vues SET ordre_zones = 1, bloc_zones = 1 WHERE vue_id = 13 AND zone_id = 84;;;
UPDATE zones_vues SET ordre_zones = 4, bloc_zones = 1 WHERE vue_id = 13 AND zone_id = 104;;;
UPDATE zones_vues SET ordre_zones = 5, bloc_zones = 1 WHERE vue_id = 13 AND zone_id = 88;;;

UPDATE vue SET vue_label = "<span class='accueil'><span class='superf'>Clients: </span>bons <span class='superf'>de </span>livraison</span><span class='metier'><span class='superf'>Vue </span>bons de livraison</span>" WHERE vue_id = 10;;;

INSERT INTO vue VALUES (8,"images/facturesNR.png","clients?vue=8", "<span class='accueil'><span class='superf'>Clients: les </span>factures &agrave; r&eacute;gler</span><span class='metier'><span class='superf'>Vue </span>factures &agrave; r&eacute;gler</span>",15, "Factures client &agrave; r&eacute;gler", 1, '', 1);;;

INSERT INTO vue_catalogue VALUES (1,15);;;

INSERT INTO zones_vues VALUES(15,7,1,2,1,0,1,2);;;
INSERT INTO zones_vues VALUES(15,8,2,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,9,3,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,10,4,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,11,5,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,14,6,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,4,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,5,-1,0,2,0,0,2);;;
INSERT INTO zones_vues VALUES(15,6,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(15,95,-1,0,3,0,0,2);;;
INSERT INTO zones_vues VALUES (15,89,0,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,94,1,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,78,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,79,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,84,2,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,85,3,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,93,4,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (15,88,5,1,0,0,0,1);;;

INSERT INTO vues_fichier VALUES (15,10," DIGITS(cv.E1NUM)||DIGITS(cv.E1SUF) = (SELECT DIGITS(E1NUM)||(E1SUF) FROM <code>CODEBIBLI</code>.PGVMEBCM cv WHERE E1COD = 'E' AND E1TOP = 0 AND E1IN19 <> 'P' AND E1ETB = cl.CLETB AND E1CLLP = cl.CLCLI AND E1CLLS = cl.CLLIV AND E1ETA > 4 ORDER BY E1NUM DESC, E1SUF DESC FETCH FIRST 1 ROWS ONLY)",'');;;         


UPDATE constante SET const_valeur = '125' WHERE const_cle = 'VERSION';;;

