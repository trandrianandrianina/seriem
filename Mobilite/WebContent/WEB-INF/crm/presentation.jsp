<% 
	// Variables globales de navigation (menus et fils rouges) - A revoir complètement
	String fil = (String)request.getSession().getAttribute("filRouge");
	String options = (String)request.getSession().getAttribute("optionsCRM");
	Integer filRouge = Integer.parseInt(fil==null?"0":fil); 
	Integer optionsCRM = Integer.parseInt(options==null?"1":options);
	
	String[] tabNavigation = {"Les actions","Planning","Liste de clients","Géolocalisation","Les actions"};
	String[] tabNav2 = {"L'action","L'action","Fiche client","Géolocalisation", "Les contacts"};
	String[] lienNav = {"ActionsCom","Planning","itineraire?retour=1","geolocalisation", "ActionsCom"};
%>

<header>
	<img id='figRI' src='images/ri.png' alt='R&eacute;solution informatique'>
	<!-- <h1>S&eacute;rie N <span>CRM</span></h1> -->
	<h1><span id='suiteH1'>Suite &nbsp;</span>S&eacute;rie N <span>mobilit&eacute;</span></h1>
		<p id='session'><%=request.getAttribute("utilisateur") %> ( <%=request.getAttribute("bibli") %> )</p>
		<nav id='filRouge'>	
			<a href='affichagePrincipal?menu=G' class='filsLien'><span class='textFil'>Accueil</span><img class='imgFil' src='images/accueil.png'/></a>
			<% if(filRouge == 0) {%>
				<span class='fils' id='filSelect'>CRM</span>
			<%} else { %>
				<a href='crm' class='filsLien'><span class='textFil'>CRM</span><img class='imgFil' src='images/CRM.png'/></a>
			<%} %>
			<% if(filRouge == 1) {%>
				<span class='fils' id='filSelect'><%=tabNavigation[optionsCRM-1] %></span>
			<%} else if (filRouge > 1) { %>
				<a href='<%=lienNav[optionsCRM-1] %>' class='filsLien'><span class='textFil'><%=tabNavigation[optionsCRM-1] %></span><img class='imgFil' src='images/liste.png'/></a>
				<span class='fils' id='filSelect'><%=tabNav2[optionsCRM-1] %></span>
			<%} %>
			
			<% if(request.getAttribute("moteurRecherche")!=null) 
			{%>
				<form action='<%=request.getAttribute("moteurRecherche") %>' id='formRechGlob' name='<%=request.getAttribute("moteurRecherche") %>' method='post' onSubmit="return controlerSaisieRecherche(this,'rechGlob');">
					<input name ='recherche' id='rechGlob' type='search' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' placeholder='RECHERCHE CLIENT' title='minimum 3 caract&egrave;res alphanum&eacute;riques non blancs' onFocus=" gererFocus(this);" />
				</form>
		    <%} %>
		</nav>
</header>

<nav id='options'>
<% if(optionsCRM == 1) {%>
	<div class='lienBtLinks'><img src='images/actions_inverse.png'><span class='labelLinks'>Actions comm.</span></div>
<%} else { %>	
	<a class='lienBtLinks' href='ActionsCom'><img src='images/actions.png'><span class='labelLinks'>Actions comm.</span></a>
<%} if(optionsCRM == 2) {%>
	<div class='lienBtLinks'><img src='images/planning_inverse.png'><span class='labelLinks'>Planning</span></div>
<%} else { %>	
	<a class='lienBtLinks' href='planning'><img src='images/planning.png'><span class='labelLinks'>Planning</span></a>
<%} if(optionsCRM == 3) {%>
	<div class='lienBtLinks'><img src='images/itineraire_inverse.png'><span class='labelLinks'>Itinéraire</span></div>
<%} else { %>	
	<a class='lienBtLinks' href='itineraire'><img src='images/itineraire.png'><span class='labelLinks'>Itinéraire</span></a>
<%} if(optionsCRM == 4) {%>
	<div class='lienBtLinks'><img src='images/geoloc_inverse.png'><span class='labelLinks'>Géolocalisation</span></div>
<%} else { %>	
	<a class='lienBtLinks' href='geolocalisation'><img src='images/geoloc.png'><span class='labelLinks'>Géolocalisation</span></a>
<%} %>

	<div class='h1Options'></div>
	
	<a class='lienBtLinks' href='accueil'><img src='images/mobilite.png'><span class='labelLinks'>Mobilité</span></a>
	<a class='lienBtLinks' href='#'><img src='images/mobilite+.png'><span class='labelLinks'>Mobilité +</span></a>
	<a class='lienBtLinks' href='#'><img src='images/report.png'><span class='labelLinks'>Report One</span></a>
</nav>