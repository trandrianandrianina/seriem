<%@page import="ri.seriem.libas400.dao.exp.programs.contact.M_Contact"%>
<%@page import="ri.seriem.mobilite.crm.GestionFormulaire"%>
<% 
	M_Contact unContact	= (M_Contact)request.getAttribute("unContact");
	GestionFormulaire	formulaire	= (GestionFormulaire)request.getAttribute("formulaire");
	String urlRetour 								= (String)request.getAttribute("urlRetour");
%>
<jsp:include page="head.jsp" />

<section class='secContenu' >
	<form action='Contact' method='post' id='formContact' name='formContact' >

	<h1><span class='titreH1'>Contact<%=(unContact.getRENUM()==0?" (Nouveau)":"") %></span>
		<span class='optionsListe'>
			<input type='hidden' name='idcontact' value='<%=unContact.getRENUM() %>'/>
			<% if( formulaire.isConsultation() ){ %>
				<%-- <a href='Contact?idcontact=<%=unContact.getRENUM() %>&action2do=<%=GestionFormulaire.MODIFICATION %>' id='action2Do'>Modifier</a> --%>
			<%} else {%>
				<input type='hidden' name='action2do' value='<%=GestionFormulaire.VALIDATION %>'/>
				<%-- <a href='#' onClick='soumettreUnFormulaire("formContact")' id='action2Do'>Valider</a> --%>
			<% } %>
		</span>
	</h1>

		<% if(unContact == null ) {%>
			<p class='messageInfo'>Pas de contact</p>
		<%} else {%>
		<div id = 'presentationAction'>
			<div class='miniBlocs'>
				<label class='lbContact'>Civilit�</label>
				<!-- <div class='responsive' > -->
					<select name="c_civcontact" class='mesSelectsCRM' <%=formulaire.getState4OGfxbject() %> >
						<%	for(int i=0; i< unContact.getListLabelCivility().length; i++ ) {%>
							<option value="<%=i %>" <%=(unContact.getListCodeCivility()[i].equals(unContact.getRECIV())?"selected=\"selected\"":"") %> >
								<%=unContact.getListLabelCivility()[i] %>
							</option>
						<% } %>
					</select>
				<!-- </div> -->
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>Nom</label>
				<input type="text" name="c_namecontact" maxlength="<%=M_Contact.SIZE_RENOM %>" value="<%=unContact.getRENOM() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>Pr�nom</label>
				<input type="text" name="c_firstnamecontact" maxlength="<%=M_Contact.SIZE_REPRE %>" value="<%=unContact.getREPRE() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>Fonction</label>
				<!-- <div class='responsive' > -->
					<select name="c_catcontact" class='mesSelectsCRM' <%=formulaire.getState4OGfxbject() %> >
						<%	for(int i=0; i< unContact.getListLabelClass().length; i++ ) {%>
							<option value="<%=i %>" <%=(unContact.getListCodeClass()[i].equals(unContact.getRECAT())?"selected=\"selected\"":"") %> >
								<%=unContact.getListLabelClass()[i] %>
							</option>
						<% } %>
					</select>
				<!-- </div> -->
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>T�l�phone</label>
				<input type="text" name="c_tel1contact" maxlength="<%=M_Contact.SIZE_RETEL %>" value="<%=unContact.getRETEL() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>T�l�phone 2</label>
				<input type="text" name="c_tel2contact" maxlength="<%=M_Contact.SIZE_RETEL2 %>" value="<%=unContact.getRETEL2() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>Fax</label>
				<input type="text" name="c_faxcontact" maxlength="<%=M_Contact.SIZE_REFAX %>" value="<%=unContact.getREFAX() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>Email 1</label>
				<input type="text" name="c_mail1contact" maxlength="<%=M_Contact.SIZE_RENET %>" value="<%=unContact.getRENET() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div class='miniBlocs'>
				<label class='lbContact'>Email 2</label>
				<input type="text" name="c_mail2contact" maxlength="<%=M_Contact.SIZE_RENET2 %>" value="<%=unContact.getRENET2() %>" class='inputsComplementaires' <%=formulaire.getState4OGfxbject() %>/>
			</div>

		<%} %>
		</form>
		
		<div id='navFlottanteCRM'>
			<%-- <a class='lienNavFlottanteCRM' href='<%=urlRetour %>' id='urlRetour'><img src='images/urlretour.png'/></a> --%>
		<% if( formulaire.isCreation() ){ %>
			<!-- <a class='lienNavFlottanteCRM' href='Contact?action2do=10' id='urlRetour'><img src='images/urlretour.png'/></a> -->
			<a class='lienNavFlottanteCRM' href='<%=urlRetour %>' id='urlRetour'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' a href='#' onClick='soumettreUnFormulaire("formContact")'  id='validerAction'><img src='images/validerAction.png'/></a>
		<%} else if( formulaire.isModification()){ %>
			<%-- <a class='lienNavFlottanteCRM' href='Contact?action2do=0&idcontact=<%=unContact.getRENUM() %>' id='urlretour3'><img src='images/urlretour.png'/></a> --%>
			<a class='lienNavFlottanteCRM' href='<%=urlRetour %>' id='urlretour3'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' href='#' onClick='soumettreUnFormulaire("formContact")' id='validerAction3'><img src='images/validerAction.png'/></a>
			<a class='lienNavFlottanteCRM' href='#' onClick = "confirmerUnLien('Contact?idcontact=<%=unContact.getRENUM() %>&action2do=<%=GestionFormulaire.SUPPRESSION %>','Voulez vous supprimer ce contact ?');" id='supprimerAction'><img src='images/supprimerAction.png'/></a>
		<%} else { %>
				<!-- <a class='lienNavFlottanteCRM' href='Contact?action2do=10' id='urlretour3'><img src='images/urlretour.png'/></a> -->
				<a class='lienNavFlottanteCRM' href='<%=urlRetour %>' id='urlretour3'><img src='images/urlretour.png'/></a>
				<a  class='lienNavFlottanteCRM' href='Contact?idcontact=<%=unContact.getRENUM() %>&action2do=<%=GestionFormulaire.MODIFICATION %>' id='modifierAction'><img src='images/modifierAction.png'/></a>
				<a class='lienNavFlottanteCRM' href='#' onClick = "confirmerUnLien('Contact?idcontact=<%=unContact.getRENUM() %>&action2do=<%=GestionFormulaire.SUPPRESSION %>','Voulez vous supprimer ce contact ?');" id='supprimerAction'><img src='images/supprimerAction.png'/></a>
		<%}%>
		</div>
		
</section>


<jsp:include page='footer.jsp' />




    		