<%@page import="java.util.ArrayList"%>
<%@page import="ri.seriem.libas400.dao.gvm.programs.client.M_Client"%>
<% 
	M_Client[] listeClients = (M_Client[])request.getAttribute("listeClients");
%>

<jsp:include page="head.jsp" />

<section class='secContenu' id='listeMetier'>
	<h1 class='tetiereListes'>
		<span class='titreTetiere'>Liste clients</span><span class='apportListe'><%=listeClients.length %> r�sultats</span>
		<div class='triTetieres'>
			<a href='ActionsCom?tri=CLNOM' class='teteListe' id='triClePrincip'>Nom ou raison sociale</a>
			<a href='ActionsCom?tri=CLVIL' class='teteListe' id='trivaleurPrincip'>Ville</a>
			<!-- Autres ent�tes de champs � r�cup�rer et afficher ici -->
		</div>
	</h1>
		<% if( listeClients.length > 0 ) {%>
			<% for(M_Client elt : listeClients ) {%>	
			<div class='listesClassiques'>
				<a href='ActionsCom?client=<%=elt.getCLCLI() %>&suffixe=<%=elt.getCLLIV() %>' class='detailsListe'>
					<div class="clePrincipCRM"><%=elt.getCLNOM() %></div>
					<div class="valeurPrincipCRM"><%=elt.getCLVIL() %></div>
					<!-- Autres champs � r�cup�rer et afficher ici -->
				</a>
			</div>
			<%} %>
		<%} else {%>
			<p class="messageListes">Pas de clients pour ces crit�res</p>
		<%} %>
	
</section>

<jsp:include page="footer.jsp" />