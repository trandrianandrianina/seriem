opaciteGlob = 0;
mode = 4;
timer = null;
timer2 = null;
timer3 = null;
timer4 = null;

window.onload=function()
{
	timer = setInterval("set_opacity(\"intro\",1)", 55);
};

function set_opacity(id, phase)
{
	if(phase==1)
	{
		if(opaciteGlob>=150)
			mode = -8;
		else if(opaciteGlob < 0)
		{
			clearInterval(timer);
			afficherConnexion();
		}
	}
	else
	{
		mode = 10;
		if(opaciteGlob>=100)
		{
			clearInterval(timer);
			clearInterval(timer2);
			clearInterval(timer3);
			clearInterval(timer4);
			document.getElementById('login').focus();
		}
	}
		
	opaciteGlob += mode;
			
    el = document.getElementById(id);
    /*el.style["filter"] = "alpha(opacity="+opaciteGlob+")";*/
    el.style["-moz-opacity"] = opaciteGlob/100;
    el.style["-khtml-opacity"] = opaciteGlob/100;
    el.style["opacity"] = opaciteGlob/100;
    return true;
}

function afficherConnexion()
{
	opaciteGlob = 0;
	document.getElementById("intro").style.display = "none";
	document.getElementById("connexion").style.display = "block";
	timer = setInterval("set_opacity(\"connexion\",2)",80);
	timer2 = setInterval("set_opacity(\"logoRi\",2)",80);
	timer3 = setInterval("set_opacity(\"mobile\",2)",80);
	timer4 = setInterval("set_opacity(\"LibelleServeur\",2)",80);
}


