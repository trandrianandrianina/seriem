/*
 * Permet de remplacer toutes les occurences d'une chaine dans une autre 
 */
function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//fonction qui s�lectionne et d�selectionne une ligne de liste
function switchSelection(element, classe, id)
{
	//ENVOYER OU RETIRER DU COOKIES DES SELECTIONS
	var cook = readCookie(classe);
	
	//alert(cook);
	
	if(cook)
	{
		//on veut d�selectionner la ligne
		if(element.className== classe + "F")
		{
			element.className = classe;
			cook = cook.replace(id+'_',"");
			cook = cook.replace('_' + id,"");
			cook = cook.replace(id,"");
		}
		//on veut s�lectionner la ligne	
		else
		{
			element.className = classe + "F"; 
			cook = cook + "_" + id;
		}
		cook = replaceAll('"', '', cook);
		//cr�er ou modifier le cookie si n�cessaire
		createCookie(classe, cook, 0);
		//cacher ou montrer le bouton de s�lection
		if(cook=="")
			document.getElementById('rechSelection').className = "lienBtLinksC";
		else
			document.getElementById('rechSelection').className = "lienBtLinks";
	}
	else
	{
		if(element.className== classe + "F")
			element.className = classe; 
		else
			element.className = classe + "F";
		//cr�er le cookie pour la premi�re fois
		createCookie(classe,id,0);
		document.getElementById('rechSelection').className = "lienBtLinks";
	} 
	
}

//gestion des cookies
function createCookie(name,value,days) 
{
	var expires = "";
	if (days) 
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	//else 
	//var expires = "";
	//document.cookie = name+"="+ value + expires + "; path=/";
	document.cookie = name+"="+ value + expires;
}

function readCookie(name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) 
{
	createCookie(name,"",-1);
}

function switchOptionsListe()
{
	if(document.getElementById('fenOptions').style.display == "block")
	{
		document.getElementById('fenOptions').style.display = "none";
		document.getElementById('filtreTotal').style.display = "none";
	}
	else
	{
		document.getElementById('fenOptions').style.display = "block";
		document.getElementById('filtreTotal').style.display = "block";
	}
}

function switchCheckbox(element, idCheckbox)
{
	if(element.className== "decorCheckbox")
	{
		element.className = "decorCheckboxB"; 
		document.getElementById(idCheckbox).checked=true;
	}
	else
	{ 
		element.className = "decorCheckbox";
		document.getElementById(idCheckbox).checked=false;
	}
}

function soumettreUnFormulaire(nomForm)
{
    //document.forms[nomForm].submit();
	//alert(nomForm);
	 var formulaire = document.forms[nomForm];
	 if(formulaire != null)
         formulaire.submit();
}

function controlerSaisieRecherche(form, element)
{
	if(document.getElementById(element).value.length < 3)
	{
		alert("3 caract�res minimum");
		return false;
	}	
	else form.submit();
}

function majTailleFiche(id)
{
	var hauteur = 0;
	var elementActuel;
	var tableauDivs = ["uneBoite","deuxBoites1","deuxBoites2","deuxBoites3"];
	var boiteModule = "module";
	
	//si le module existe pas
	
		//comparer les boites 
		for(var i=0; i<tableauDivs.length; i++)
		{
			if(document.getElementById(tableauDivs[i])!=null)
			{
				elementActuel = document.getElementById(tableauDivs[i]);
			
				if(elementActuel.offsetHeight)          {if(elementActuel.offsetHeight>hauteur) hauteur=elementActuel.offsetHeight;}
				else if(elementActuel.style.pixelHeight){if(elementActuel.style.pixelHeight>hauteur) hauteur=elementActuel.style.pixelHeight;}
			}	
		}
		
		if(document.getElementById(boiteModule)!=null)
		{
			elementActuel = document.getElementById(boiteModule);
			
			if(elementActuel.offsetHeight)          {hauteur= hauteur + elementActuel.offsetHeight;}
			else if(elementActuel.style.pixelHeight){hauteur= hauteur + elementActuel.style.pixelHeight;}
		}

	
		hauteur += 250;
		document.getElementById(id).style.minHeight = hauteur + "px";
}

/**
 * Cette m�thode permet de fixer le view port des pages dans le cas de IOS dont le viewport ne change pas en paysage ou en portrait
 * Si on enl�ve cette m�thode, il y a un gros probl�me lors de la prise de focus des zones de saisie avec le clavier IOS
 * */
function fixerViewPort()
{
	if (navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i))
	{
		var isIPad = navigator.userAgent.match(/iPad/i);
		var orientation = window.orientation;
		var hauteur = 768;
	      switch(orientation) {
	        case 0:
	        	if(isIPad) 
	        		hauteur = 1024;
	        	else hauteur = 480;
	            break; 

	        case 90:
	        	if(isIPad) 
	        		hauteur = 768;
	        	else hauteur = 320;
	            break;

	        case -90: 
	        	if(isIPad) 
	        		hauteur = 768;
	        	else hauteur = 320;
	            break;
	            
	        case 180:
	        	if(isIPad) 
	        		hauteur = 1024;
	        	else hauteur = 480;
	            break; 
	      }
		var viewportmeta = document.querySelector('meta[name="viewport"]');
	  	
	  	viewportmeta.setAttribute('content', 'width=device-width, height=' + hauteur + ', initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi');
	  	
	}
}

function gererFocus(element)
{
	element.focus();
}

/*
 * Sauve les donn�es du formulaire de saisie d'une action
 */
function saveActionForm(lien)
{
	cleanActionFromSessionStorage();
	var arr = new Array();
    arr = document.forms['formActionCom'];
    for(var i = 0; i < arr.length; i++)
    {
        var obj = arr.elements[i];
        if( obj.name.indexOf("ac_") == 0 ){
        	//alert(obj.name + " =  " + obj.value);
        	sessionStorage.setItem(obj.name, obj.value);
    	}
    }
	window.location=lien;
}

/*
 * Restaure les donn�es du formulaire de saisie d'une action
 */
function loadActionForm()
{
	var size = sessionStorage.length;
	for(var i=0; i <size; i++)
	{
		if( sessionStorage.key(i).indexOf("ac_") == 0 ){
			console.log(sessionStorage.key(i) );
			document.forms['formActionCom'].elements[ sessionStorage.key(i) ].value = sessionStorage.getItem( sessionStorage.key(i) );
		}
	}
	cleanActionFromSessionStorage();
}

/*
 * Supprime les donn�es stock�es provisoirement pour une action
 */
function cleanActionFromSessionStorage()
{
	var size = sessionStorage.length;
	var i=0;
	while( i < size )
	{
		if( sessionStorage.key(i).indexOf("ac_") == 0 ){
			console.log(sessionStorage.key(i));
			sessionStorage.removeItem( sessionStorage.key(i) );
		} else {
			i++;
		}
	}
}

/*
 * Lit le cookie qui contient l'URL de retour (bouton Back)
 */
function readUrlRetour()
{
	var lien = readCookie("urlRetour");
	document.getElementById('urlretour').setAttribute('href', lien);
}


function confirmerUnLien(lien,message)
{
	if(confirm(message))
		window.location.href=lien;
}
/**
 * Envoyer les donn�es en POST plut�t qu'en GET (caract�res sp�ciaux)
 * */
function envoyerDonneesEnPOST(page,nameData1,data1,nameData2,data2)
{
	traitementEnCours(null);
    var form = document.createElement('form');
    form.setAttribute('action', page);
    form.setAttribute('method', 'post');
    form.setAttribute('accept-charset', 'utf-8');
    var inputvar1 = document.createElement('input');
    inputvar1.setAttribute('type', 'hidden');
    inputvar1.setAttribute('name', nameData1);
    inputvar1.setAttribute('value', data1);
    form.appendChild(inputvar1);
    var inputvar2 = document.createElement('input');
    inputvar2.setAttribute('type', 'hidden');
    inputvar2.setAttribute('name', nameData2);
    inputvar2.setAttribute('value', data2);
    form.appendChild(inputvar2);
    
    document.body.appendChild(form);
    form.submit();
}


/*
 * Met � jour l'URL retour � partir de l'URL courante et cr�� un cookie avec l'URL courante de la nouvelle page
 *
function createUrlCourante()
{
	var lien = readCookie("urlCourante");
	if( lien != null ){
		create("urlRetour", lien, 0);
	}
	createCookie("urlCourante", document.location.href, 0);
}*/