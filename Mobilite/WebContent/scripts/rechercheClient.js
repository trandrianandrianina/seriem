function rechercheCli(id)
{
	// On teste la longueur de la chaine afin de commencer la requ�te � partir de  2 caract�res (afin d'�liminer le nombre d'enregistrements)
	var nomcli = document.getElementById("rechCli").value;
	if (nomcli.length < 2)
	{
		document.getElementById("listeClient").style.display = "none";
		return;
	}

	// On aligne la fen�tre sous le Input ainsi que le bouton Reset
	var top = document.getElementById("rechCli").offsetTop + document.getElementById("rechCli").offsetHeight;
	document.getElementById("listeClient").style.top = top + 'px';
	document.getElementById("listeClient").style.left = document.getElementById("rechCli").offsetLeft + 'px';

	// On affiche le bloc
	document.getElementById("listeClient").style.display = "block";

	//se connecter � la servlet pour g�rer les acc�s base de donn�es et la liste des vues
	var url = "articles?rechclient=" + nomcli;
	if (window.XMLHttpRequest) 
	{
		requete = new XMLHttpRequest();
		if (requete) 
		{
			requete.open("POST", url, true);
			requete.onreadystatechange = majIHM;
			requete.send();
		}
	}
	else if (window.ActiveXObject) 
	{
		requete = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete) 
		{
			requete.open("POST", url, true);
			requete.onreadystatechange = majIHM;
			requete.send(); 
		}
		else alert('Une erreur est survenue lors de construction objet: ActiveXObject');  
	} 
	else 
	{
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}
}

/**
 * Mise � jour de l'interface graphique au niveau des vues de l'utilisateur
 * */
function majIHM() 
{
	if (requete.readyState == 4)
	{
		if (requete.status == 200 || requete.status == 0)
		{
			document.getElementById("listeClient").innerHTML = requete.responseText;
		} 
		else
		{
			alert('Une erreur est survenue lors de la mise � jour de la page.'+'\n\nCode retour = '+requete.statusText);   
		}
	}
}

