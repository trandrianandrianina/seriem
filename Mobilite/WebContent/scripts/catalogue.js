function controlerVues(id)
{
		//se connecter � la servlet pour g�rer les acc�s base de donn�es et la liste des vues
		//var url = "catalogue?metier=1&changement=" + id;
		var url = "catalogue?changement=" + id;
		if (window.XMLHttpRequest) 
		{
			requete = new XMLHttpRequest();
			if (requete) 
			{
				requete.open("POST", url, true);
				requete.onreadystatechange = majIHM;
				requete.send();
				//requete.send();
			}
		}
		else if (window.ActiveXObject) 
		{
			requete = new ActiveXObject("Microsoft.XMLHTTP");
			if (requete) 
			{
				requete.open("POST", url, true);
				requete.onreadystatechange = majIHM;
				requete.send(); 
			}
			else alert('Une erreur est survenue lors de construction objet: ActiveXObject');  
		} 
		else 
		{
			alert("Le navigateur ne supporte pas la technologie Ajax");
		}
		
		//g�rer l'affichage du bouton 
		if(document.getElementById(id).className == "selectionVue")
			document.getElementById(id).className = "selectionVueF";
		else document.getElementById(id).className = "selectionVue";
}

/**
 * Mise � jour de l'interface graphique au niveau des vues de l'utilisateur
 * */
function majIHM() 
{
	if (requete.readyState == 4)
	{
		if (requete.status == 200 || requete.status == 0)
		{
			document.getElementById("options").innerHTML = requete.responseText;
		} 
		else
		{
			alert('Une erreur est survenue lors de la mise � jour de la page.'+'\n\nCode retour = '+requete.statusText);   
		}
	}
}