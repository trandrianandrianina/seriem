package ri.seriem.mobilite.outils;

public class Log 
{
	private String messageLog = null;
	private String dateLog = null;
	private String typeLog = null;
	private String loginLog = null;
	private String classeLog = null;
	
	public Log(String date,String type,String login,String classe,String message)
	{
		dateLog = date;
		typeLog = type;
		loginLog = login;
		classeLog = classe;
		messageLog = message;
	}

	public String getMessageLog() {
		return messageLog;
	}

	public void setMessageLog(String messageLog) {
		this.messageLog = messageLog;
	}

	public String getDateLog() {
		return dateLog;
	}

	public void setDateLog(String dateLog) {
		this.dateLog = dateLog;
	}

	public String getTypeLog() {
		return typeLog;
	}

	public void setTypeLog(String typeLog) {
		this.typeLog = typeLog;
	}

	public String getLoginLog() {
		return loginLog;
	}

	public void setLoginLog(String loginLog) {
		this.loginLog = loginLog;
	}

	public String getClasseLog() {
		return classeLog;
	}

	public void setClasseLog(String classeLog) {
		this.classeLog = classeLog;
	}
}
