
package ri.seriem.mobilite.outils;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Cette classe permet d'ouvrir le mailer par d�faut de l'OS et de pr�remplir son contenu via mailTo: souci la taille maximum du contenu
 * emp�che de faire tout ce qu'on veut
 */
public class MailTo {
  
  public void envoyerMailTo(String sujet, String body) {
    String mailTo = "";
    URI uriMailTo = null;
    
    // Assembler l'url
    // mailTo = "user@gmail.com";
    mailTo += "?subject=" + sujet;
    mailTo += "&body=" + nettoyerContenu(body);
    // Ouvrir le logiciel de messagerie par d�faut
    if (Desktop.isDesktopSupported()) {
      if (Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
        try {
          uriMailTo = new URI("mailto", mailTo, null);
          Desktop.getDesktop().mail(uriMailTo);
        }
        catch (IOException ex) {
          ex.getStackTrace();
        }
        catch (URISyntaxException e) {
          // TODO Bloc catch g�n�r� automatiquement
          e.printStackTrace();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  private String nettoyerContenu(String contenu) {
    System.out.println("-MailTo-> " + contenu);
    return contenu;
  }
  
}
