
package ri.seriem.mobilite.outils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;

public class Logs {
  private Utilisateur utilisateur = null;
  private SAXBuilder sxb = null;
  private SimpleDateFormat formater = null;
  // private String formatAffichage = "dd/MM/yy H:mm:ss";
  private String formatAffichage = "H:mm:ss";
  private String formatFichier = "yyMMdd";
  private Document logCourant = null;
  private Element elementRacine = null;
  File fichierJour = null;
  String cheminFichierDuJour = null;
  Element log = null;
  Element messageLog = null;
  Element dateLog = null;
  Element typeLog = null;
  Element loginLog = null;
  Element classeLog = null;
  
  /**
   * Constructeur par d�faut
   */
  public Logs(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
    sxb = new SAXBuilder();
  }
  
  /**
   * V�rifier si le fichier date du jour existe d�j�
   */
  private boolean fichierExisteDeja() {
    cheminFichierDuJour = retournerFichierDateDuJour(formatFichier) + ".xml";
    // cheminFichierDuJour = "131007" + ".xml";
    if (cheminFichierDuJour == null)
      return false;
    
    fichierJour = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.DOSSIER_LOGS + cheminFichierDuJour);
    // fichierJour = new File(MarbreEnvironnement.DOSSIER_LOGS + cheminFichierDuJour);
    return fichierJour.isFile();
  }
  
  /**
   * Retourner la liste compl�te des fichiers de logs
   */
  public String[] getTousLogs() {
    File dossierLogs = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.DOSSIER_LOGS);
    // File dossierLogs = new File(MarbreEnvironnement.DOSSIER_LOGS);
    if (dossierLogs.isDirectory()) {
      String[] liste = dossierLogs.list();
      
      return liste;
    }
    else
      return null;
  }
  
  /**
   * Retourner la liste des logs d'une date demand�e
   */
  public ArrayList<Log> getUneDate(String dateDemandee) {
    if (dateDemandee == null)
      return null;
    ArrayList<Log> liste = new ArrayList<Log>();
    
    File fichierXML = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.DOSSIER_LOGS + dateDemandee + ".xml");
    if (fichierXML.isFile()) {
      try {
        Document document = sxb.build(fichierXML);
        if (document != null && document.hasRootElement()) {
          Element racine = document.getRootElement();
          Element elementEncours = null;
          List<Element> listeElements = racine.getChildren();
          if (listeElements.size() > 0) {
            Iterator i = listeElements.iterator();
            while (i.hasNext()) {
              elementEncours = (Element) i.next();
              liste.add(new Log(elementEncours.getChildText("date"), elementEncours.getChildText("type"),
                  elementEncours.getChildText("login"), elementEncours.getChildText("classe"), elementEncours.getChildText("message")));
            }
          }
        }
        else {
          System.out.println("Probl�me avec le type de document !!");
        }
      }
      catch (JDOMException e) {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    
    return liste;
  }
  
  /**
   * Mettre � jour le fichier des logs avec un nouvel �v�nement
   */
  public void setLog(String messag, String theClass, String type) {
    try {
      if (fichierExisteDeja()) {
        if (logCourant == null)
          logCourant = sxb.build(fichierJour);
        if (logCourant != null)
          elementRacine = logCourant.getRootElement();
      }
      else
        gererUnNouveauDocument();
      
      log = new Element("log");
      
      dateLog = new Element("date");
      dateLog.setText(retournerFichierDateDuJour(formatAffichage));
      log.addContent(dateLog);
      
      typeLog = new Element("type");
      typeLog.setText(type);
      log.addContent(typeLog);
      
      loginLog = new Element("login");
      if (utilisateur != null)
        loginLog.setText(utilisateur.getLogin());
      else
        loginLog.setText(" ");
      log.addContent(loginLog);
      
      messageLog = new Element("message");
      messageLog.setText(messag);
      log.addContent(messageLog);
      
      classeLog = new Element("classe");
      classeLog.setText(theClass);
      log.addContent(classeLog);
      
      elementRacine.addContent(log);
      
      majLeFichier();
    }
    catch (JDOMException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println(dateLog.getText() + " - " + loginLog.getText() + " [" + classeLog.getText() + "] " + messageLog.getText());
  }
  
  /**
   * Permet de retourner le nom du fichier de log du jour courant
   */
  public String retournerFichierDateDuJour(String theFormat) {
    formater = new SimpleDateFormat(theFormat);
    
    return formater.format(new Date());
  }
  
  /**
   * Permet de mettre � jour le document courant ainsi que sa racine
   */
  private void gererUnNouveauDocument() {
    logCourant = new Document(new Element("logs"));
    if (logCourant != null)
      elementRacine = logCourant.getRootElement();
  }
  
  /**
   * Ecrire les nouvelles donn�es dans le fichier XML physique
   */
  private void majLeFichier() {
    try {
      // Affichage classique avec getPrettyFormat()
      XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
      // Cr�er une instance de FileOutputStream
      sortie.output(logCourant, new FileWriter(fichierJour));
    }
    catch (java.io.IOException e) {
      e.printStackTrace();
    }
  }
}
