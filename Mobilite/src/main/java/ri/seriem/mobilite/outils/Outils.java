
package ri.seriem.mobilite.outils;

import java.text.SimpleDateFormat;
import java.util.Date;

import ri.seriem.mobilite.Environnement.Utilisateur;

/**
 * Cette classe est une classe d'outils g�n�riques � toutes les classes
 */
public class Outils {
  String separateurDate = "/";
  private Utilisateur utilisateur = null;
  // A filer ult�rieurement � la classe responsable de ces valeurs
  private char separateurMiliers;
  private int nombreDecimales;
  
  /**
   * Constructeur recevant l'utilisateur
   */
  public Outils(Utilisateur gudule) {
    utilisateur = gudule;
    // En attendant qu'on r�cup�re les param�tres suivants via l'utilisateur
    separateurMiliers = ' ';
    nombreDecimales = 2;
  }
  
  /**
   * Met en forme une date pourrie (num�rique) en date humainement lisible
   */
  public String TransformerEnDateHumaine(String datePourrie) {
    String retour = "";
    if (datePourrie == null)
      return retour;
    
    // date pourrave du si�cle dernier
    if (datePourrie.length() == 6) {
      // r�cup�ration du jour
      retour = datePourrie.substring(4, 6) + separateurDate;
      
      // r�cup�ration du mois
      retour += datePourrie.substring(2, 4) + separateurDate;
      
      retour += "19" + datePourrie.substring(0, 2);
    }
    // mise � jour avec le si�cle
    else if (datePourrie.length() == 7) {
      // r�cup�ration du jour
      retour = datePourrie.substring(5, 7) + separateurDate;
      
      // r�cup�ration du mois
      retour += datePourrie.substring(3, 5) + separateurDate;
      
      // si�cle si 1 = 2000 si 0 = 1900
      String chaine = datePourrie.substring(0, 1);
      if (chaine.equals("1"))
        chaine = "20";
      else
        chaine = "19";
      
      retour += chaine;
      
      // ann�e
      retour += datePourrie.substring(1, 3);
    }
    
    return retour;
  }
  
  /**
   * gestion du nem�ro client
   */
  public String affichageNumeroClient(String clcli) {
    String retour = "";
    if (clcli == null)
      return retour;
    
    if (clcli.length() == 6)
      clcli = clcli + "000";
    else if (clcli.length() == 5)
      clcli = "0" + clcli + "000";
    else if (clcli.length() == 4)
      clcli = "00" + clcli + "000";
    else if (clcli.length() == 3)
      clcli = "000" + clcli + "000";
    else if (clcli.length() == 2)
      clcli = "0000" + clcli + "000";
    else
      clcli = "00000" + clcli + "000";
    
    return clcli;
    
  }
  
  /**
   * Afficher une valeur d�cimale (stock, conditionnement) correctement HORS PRIX
   */
  public String afficherValeurCorrectement(String donnee) {
    if (donnee == null || donnee.trim().equals("")) {
      return null;
    }
    String retour = donnee;
    
    // System.out.println("affichage du stock, val donnee " + donnee) ;
    if (donnee.endsWith(".000000")) {
      donnee = donnee.replaceAll(".000000", "");
    }
    else if (donnee.endsWith(".00000")) {
      donnee = donnee.replaceAll(".00000", "");
    }
    else if (donnee.endsWith(".0000")) {
      donnee = donnee.replaceAll(".0000", "");
    }
    else if (donnee.endsWith(".000")) {
      donnee = donnee.replaceAll(".000", "");
    }
    else if (donnee.endsWith(".00")) {
      donnee = donnee.replaceAll(".00", "");
    }
    else if (donnee.endsWith(".0")) {
      donnee = donnee.replaceAll(".0", "");
    }
    retour = donnee;
    
    return retour;
  }
  
  /**
   * Pour tout champs, renvoit un string format� avec miliers s�par� par le symbole en param�tre 1, et nombre de d�cimales fix� par le
   * param�tre 2
   * il faudra prendre ces deux informations dans le param�trage de l'application quand il existera.
   */
  public String gererAffichageNumerique(Object monField, char separateurMiliers, int nombreDecimales) {
    AffichageNumerique an = new AffichageNumerique(monField);
    return an.putMiseEnForme(separateurMiliers, nombreDecimales);
  }
  
  public String gererAffichageNumerique(Object monField, int nombreDecimales) {
    AffichageNumerique an = new AffichageNumerique(monField);
    return an.putMiseEnForme(separateurMiliers, nombreDecimales);
  }
  
  /**
   * en attendant de variabiliser on utilise la redefinition ci-dessous
   */
  public String gererAffichageNumerique(Object monField) {
    AffichageNumerique an = new AffichageNumerique(monField);
    return an.putMiseEnForme(separateurMiliers, nombreDecimales);
  }
  
  /**
   * Retourner la date du jour si�cle/ann�e/mois/jour
   */
  public String getDateDuJour() {
    SimpleDateFormat formater = new SimpleDateFormat("1" + "yyMMdd");
    Date aujourdhui = new Date();
    
    return formater.format(aujourdhui);
  }
  
  /**
   * Traduire de mani�re lisible le num�ro de version
   */
  public String traduireVersion(int version) {
    String chaine = String.valueOf(version);
    
    if (chaine.length() == 3)
      chaine = "v." + chaine.substring(0, 1) + "." + chaine.substring(1, 3);
    else if (chaine.length() == 4)
      chaine = "v." + chaine.substring(0, 2) + "." + chaine.substring(2, 4);
    else
      chaine = "v." + chaine;
    
    return chaine;
  }
  
  // ++++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++
  
  public char getSeparateurMiliers() {
    return separateurMiliers;
  }
  
  public void setSeparateurMiliers(char separateurMiliers) {
    this.separateurMiliers = separateurMiliers;
  }
  
  public int getNombreDecimales() {
    return nombreDecimales;
  }
  
  public void setNombreDecimales(int nombreDecimales) {
    this.nombreDecimales = nombreDecimales;
  }
  
  public void setSeparateurDate(String separateurDate) {
    this.separateurDate = separateurDate;
  }
  
}
