package ri.seriem.mobilite.outils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;



public class MonFTP 
{
	
	private FTPClient connexionFTP=null;
	
	public MonFTP()
	{
		connexionFTP = new FTPClient(); 
	}
	
	public boolean seConnecter(String serveur, String user, String password)
	{
		try
		{
			connexionFTP.connect(serveur);
			connexionFTP.login(user, password);
			
			if(connexionFTP.isConnected())
			{
				System.out.println("Connexion FTP OK");
				//miseAJour.getMaFenetre().getConsole().ajouterUneInfo("Connexion FTP au serveur " + serveur);
				return true;
			}
			else
			{
				System.out.println("Connexion FTP MOISIE");
				//miseAJour.getMaFenetre().getConsole().ajouterUneInfo("Echec Connexion FTP au serveur " + serveur);
				return false;
			}
		}
		catch(Exception e)
		{
			//miseAJour.getMaFenetre().getConsole().ajouterUneInfo(e.getMessage());
			return false;
		}
	}
	
	public void seDeconnecter()
	{
		try
		{
			connexionFTP.logout();
			connexionFTP.disconnect();
			System.out.println("Deconnexion FTP OK");
		}
		catch(Exception e)
		{
			System.out.println("Deconnexion FTP MOISIE");
		}
	}
	
	public FTPClient getMaConnexionFTP()
	{
		return connexionFTP;
	}
	
	public boolean downloadFile(String remoteFile, String localFile)
	{
		boolean ret=false;
		try
		{
			OutputStream output = new FileOutputStream(localFile);
	        ret = connexionFTP.retrieveFile(remoteFile, output);
	        output.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	public boolean uploadFile(String localFile, String remoteFile)
	{
		boolean ret=false;
		try
		{
			InputStream input = new FileInputStream(localFile);
			ret = connexionFTP.storeFile(remoteFile, input);
			input.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}

	public boolean exists(String remoteFile)
	{
		try
		{
			FTPFile lock = connexionFTP.mlistFile(remoteFile);
			return lock != null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return false;
	}
}
