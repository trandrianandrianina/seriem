package ri.seriem.mobilite.outils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


public class AffichageNumerique {
	
	
	@SuppressWarnings({ "serial" })
	private Number zone = new Number() {
		
		@Override
		public long longValue() {
			// TODO Stub de la m�thode g�n�r� automatiquement
			return 0;
		}
		
		@Override
		public int intValue() {
			// TODO Stub de la m�thode g�n�r� automatiquement
			return 0;
		}
		
		@Override
		public float floatValue() {
			// TODO Stub de la m�thode g�n�r� automatiquement
			return 0;
		}
		
		@Override
		public double doubleValue() {
			// TODO Stub de la m�thode g�n�r� automatiquement
			return 0;
		}
	};
	
	
	/**
	 * constructeur 
	 */
	public AffichageNumerique(Object zoneEnEntree)
	{
		if(zoneEnEntree instanceof String && zoneEnEntree != null)
			zone = (Number) Integer.parseInt((String) zoneEnEntree);
		
		else zone = (Number) zoneEnEntree;
	}
	
	
	
	public String putMiseEnForme(char separateur, int decimales){
		
		DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.FRANCE);
		dfs.setGroupingSeparator(separateur);
		DecimalFormat df = new DecimalFormat("#,###.##", dfs);
		df.setDecimalFormatSymbols(dfs);
		df.setMaximumFractionDigits(decimales); 
		df.setMinimumFractionDigits(decimales); 
		
		return df.format(zone);
		
	}
	
	

	
	

}
