package ri.seriem.mobilite.constantes;


//A remplacer par des fichiers ou une base de configuration
public class MarbreEnvironnement 
{
	public static final String SEPARATEUR = System.getProperty("file.separator");
	
	public static final String FICHIER_BDD = "bdd_mobilite";
	public static final String BDD_SQLITE = "bdd" + SEPARATEUR + FICHIER_BDD;
	public static final String DOSSIER_LOGS = "logs" +SEPARATEUR;
	public static final String DOSSIER_VERSIONS_PGS = "WEB-INF" + SEPARATEUR  + "versions" + SEPARATEUR;
	public static final String FICHIER_VERSION_PGS = DOSSIER_VERSIONS_PGS +  "versionActuelle.txt";
	public static final String FICHIER_MAJBDD = "miseAjourBDD.sql";
	
	public static final String SERVEUR_FTP = "31.193.129.158";
	public static final String CLIENT_FTP = "seriemclient";
	public static final String MP_FTP = "seriemad";
	public static final String DOSSIER_VERSIONS_FTP = "httpdocs/VERSION_DEV/mobilite/versions";
	
	public static final String BIBLI_ENV = "M_GPL";
	public static final int ACCES_MOBILITE = 3; 		//droits de base d'un utilisateur
	public static final int DECO_AUTO = 600;  			//10mn
	public static int NB_SESSIONS_FLOT = 0;
	public static int NB_SESSIONS_FIX = 0;
	
	public static String AS_UTILISE = null;
	
	public static int VERSION_DEV = 0;
	public final static String VERSION_JS = "19";
	
	public static final int ACCES_RI = 1;
	public static final int ACCES_ADMIN = 2;
	public static final int ACCES_UTIL = 3;
	public static final int ACCES_NON = 4;
	
	public static String SQLITE_ISOK = "$$OKrequeteBDD$$";

	// Ces variables peuvent �tre personnalis�es dans le fichier context.xml de votre serveur Tomcat
	// Ici un exemple pour un fichier d'execution normale sur l'AS400, m�me si IS_ECLIPSE, DOSSIER_RACINE_MOBILITE, DOSSIER_RACINE_TOMCAT sont facultatifs dans
	// cet exemple car ils contiennent les valeurs par d�faut. 
	// <Parameter name="IS_ECLIPSE" value="false" override="false"/>
	// <Parameter name="DOSSIER_RACINE_MOBILITE" value="/xmobilite/tomcat/webapps/" override="false"/>
	// <Parameter name="DOSSIER_RACINE_TOMCAT" value="/xmobilite/tomcat/" override="false"/>
	// <Parameter name="LIBELLE_SERVEUR" value="Version demo" override="false"/>
	public static boolean isEclipse=false;
	public static boolean afficheTousLesStocks = false;
	public static boolean stockDG = false;
	public static boolean stockUtilisateur = false;
	
	public static String DOSSIER_RACINE_MOBILITE = SEPARATEUR + "xweb" + SEPARATEUR + "tomcat" + SEPARATEUR + "webapps" + SEPARATEUR;
	public static String DOSSIER_RACINE_TOMCAT = SEPARATEUR + "xweb" + SEPARATEUR + "tomcat" + SEPARATEUR;

	//DOSSIER MOBILITE
	public static String DOSSIER_MOBILITE = DOSSIER_RACINE_MOBILITE + "Mobilite" + SEPARATEUR + (isEclipse?"WebContent"+SEPARATEUR:"");
	public static String DOSSIER_TRAVAIL = DOSSIER_RACINE_TOMCAT + "work" + SEPARATEUR; // + "bdd" + SEPARATEUR ;
	public static String DOSSIER_SAUVE =  DOSSIER_RACINE_TOMCAT + "work" + SEPARATEUR + "sauvegardes" + SEPARATEUR ;
	//NB DE SESSIONS 
	
	//Dossier de d�ploiement du WAR sur le serveur tomcat
	public static final String DOSSIER_DEPLOIE = DOSSIER_RACINE_MOBILITE;
	// DOSSIER Version sur AS
	//public static final String DOSSIER_VERSION_AS = "testFTP" + SEPARATEUR;
	public static final String DOSSIER_VERSION_AS = "";
	
	public static String Libelle_Serveur=null;

	
	/**
	 * Met � jour les variables si besoin
	 */
	public static void refreshValue()
	{
		DOSSIER_MOBILITE = DOSSIER_RACINE_MOBILITE + "Mobilite" + SEPARATEUR + (isEclipse?"WebContent"+SEPARATEUR:"");
		DOSSIER_TRAVAIL  = DOSSIER_RACINE_TOMCAT   + "work" + SEPARATEUR; // + "bdd" + SEPARATEUR;
		DOSSIER_SAUVE    = DOSSIER_RACINE_TOMCAT   + "work" + SEPARATEUR + "sauvegardes" + SEPARATEUR;
	}
 }
