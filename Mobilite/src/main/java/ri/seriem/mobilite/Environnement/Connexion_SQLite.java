
package ri.seriem.mobilite.Environnement;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.mobilite.backoffice.Parametre;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;
import ri.seriem.mobilite.metier.Catalogue;
import ri.seriem.mobilite.metier.Fichier;
import ri.seriem.mobilite.metier.Metier;
import ri.seriem.mobilite.metier.Vue;
import ri.seriem.mobilite.metier.Zone;

import ri.seriem.libas400.database.record.GenericRecord;

public class Connexion_SQLite {
  private Connection maConnexion = null;
  private String cheminDataBase = null;
  private Utilisateur utilisateur = null;
  public static final String SEPARATEUR_IDS = "@-@";
  
  /**
   * Constructeur de la connexion � la base SQLITE
   */
  public Connexion_SQLite(String database, Utilisateur utilisateur) {
    // System.out.println("chemin sqlite: " + database);
    cheminDataBase = database;
    maConnexion = createConnection();
    try {
      if (maConnexion != null && !maConnexion.isClosed()) {
        // stat = maConnexion.createStatement();
        if (utilisateur != null)
          this.utilisateur = utilisateur;
      }
      else {
        if (utilisateur != null)
          utilisateur.getLogs().setLog("ECHEC connexion SQLITE de l'utilisateur 1", "SQLITE", "E");
      }
    }
    catch (SQLException e) {
      if (utilisateur != null)
        utilisateur.getLogs().setLog("ECHEC connexion SQLITE de l'utilisateur 2", "SQLITE", "E");
      e.printStackTrace();
    }
  }
  
  /**
   * V�rifier si la connexion SQLITE est OP
   */
  private boolean isConnecteCorrectement() {
    try {
      return ((maConnexion != null) && !maConnexion.isClosed());
    }
    catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * V�rifier si le statement est OP
   *
   * private boolean statementIsOp()
   * {
   * try
   * {
   * //return (stat!=null && !stat.isClosed());
   * return (stat!=null);
   * 
   * } catch (Exception e) {e.printStackTrace();return false;}
   * 
   * }
   */
  
  /**
   * Recr�er la connexion et le statement
   */
  private void majDesConnexionsSQLITE() {
    // LA CONNEXION EST MOISIE
    if (!isConnecteCorrectement()) {
      System.out.println("MAJ de la connexion SQLITE");
      if (stopConnection())
        maConnexion = createConnection();
    }
    /*
    //LE STATEMENT EST MOISI
    else if(!statementIsOp())
    {
    	System.out.println("MAJ du statement SQLITE");
    	try 
    	{
    		if(stopConnection())
    			stat = maConnexion.createStatement();
    	} catch (SQLException e) {e.printStackTrace();}
    }
    else 
    	try 
    	{
    		stat = maConnexion.createStatement();
    	} catch (SQLException e) {e.printStackTrace();}
    */
    // FERMER SYSTEMATIQUEMENT LE RESULTSET
    /*try 
    {
    	if(resultSet!=null && !resultSet.isClosed())
    		resultSet.close();
    } catch (SQLException e) {e.printStackTrace();}*/
  }
  
  /**
   * Permet de se connecter via un JDBC � la BDD SQLITE
   */
  public Connection createConnection() {
    try {
      Class.forName("org.sqlite.JDBC");
      return DriverManager.getConnection("jdbc:sqlite:" + cheminDataBase);
    }
    catch (Exception e) {
      if (utilisateur != null)
        utilisateur.getLogs().setLog("ECHEC createConnection()", "SQLITE", "E");
      e.printStackTrace();
      return null;
    }
  }
  
  /**
   * Fermer la connection � la BDD SQLITE
   */
  public boolean stopConnection() {
    try {
      // resultSet.close();
      // if(stat!=null) stat.close();
      if (maConnexion != null)
        maConnexion.close();
      /*if(utilisateur!=null)
      	utilisateur.getLogs().setLog("OK stopConnection()", "SQLITE", "M");
      else System.out.println("OK POUR STOP CONNEXION");*/
      return true;
    }
    catch (SQLException e) {
      if (utilisateur != null)
        utilisateur.getLogs().setLog("ECHEC stopConnection()", "SQLITE", "E");
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * V�rifie la pr�sence de l'utilisateur dans la BDD SQLITE et r�cup�re les donn�es inh�rentes � celui-ci
   */
  public void gererUtilisateur(Utilisateur utilisateur) {
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      
      // on v�rifie que l'utilisateur existe sinon...
      // ResultSet resultSet = stat.executeQuery("SELECT util_id, util_acces, util_super, util_repres FROM utilisateur WHERE util_login =
      // '" + utilisateur.getLogin() + "';");
      ResultSet resultSet = stat
          .executeQuery("SELECT util_id, util_acces, util_super FROM utilisateur WHERE util_login = '" + utilisateur.getLogin() + "';");
      // si un utilisateur existe
      if (resultSet.next()) {
        // si il existe on lui attribue son ID, ses vues
        utilisateur.setIdUtil(resultSet.getInt(1));
        // r�cup�rer le niveau d'acc�s aux param�tres de la mobilit�
        utilisateur.setNiveauAcces(resultSet.getInt(2));
        // A r�gler avec la base de donn�es et le BO -> session flottantes et sessions fixes
        utilisateur.setUnSuperUtilisateur(resultSet.getInt(3) == 1);
        // recup�rer le code repr�sentant le cas �ch�ant
        // utilisateur.setCodeRepresentant(resultSet.getString(4));
        
        // A r�gler avec la base de donn�e et le BO : 600secondes par d�faut
        utilisateur.setMaDecoAutomatique(MarbreEnvironnement.DECO_AUTO);
        
        resultSet.close();
        
        try {
          resultSet = stat.executeQuery("SELECT util_repres FROM utilisateur WHERE util_login = '" + utilisateur.getLogin() + "';");
          
          if (resultSet.next()) {
            utilisateur.setCodeRepresentant(resultSet.getString(1));
            resultSet.close();
          }
          
        }
        catch (SQLException e) {
          utilisateur.getLogs().setLog("UTIL_REPRES n'existe pas encore", "SQLITE", "E");
          e.printStackTrace();
        }
      }
      else {
        resultSet.close();
        if (insererNouvelUtilisateur(utilisateur, false)) {
          if (insererCataloguesPreRequis(utilisateur))
            insererVuesPreRequis(utilisateur);
        }
      }
      
      utilisateur.setMesVues(retournerMesVues(utilisateur));
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL gererUtilisateur()", "SQLITE", "E");
      e.printStackTrace();
    }
  }
  
  /**
   * Retourner les vues de l'utilisateur
   */
  public ArrayList<Vue> retournerMesVues(Utilisateur utilisateur) {
    if (utilisateur == null)
      return null;
    
    ArrayList<Vue> liste = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT * FROM vue v, vue_util u WHERE v.vue_id = u.vue_id AND u.util_id = '"
          + utilisateur.getIdUtil() + "' ORDER BY v.metier_id;");
      
      liste = new ArrayList<Vue>();
      // traitement des vues : rajouter le filtre utilisateur
      while (resultSet.next())
        liste.add(new Vue(resultSet.getInt(5), resultSet.getInt(7), resultSet.getInt(1), resultSet.getString(4), resultSet.getString(3),
            resultSet.getString(2)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL retournerMesVues()", "SQLITE", "E");
      e.printStackTrace();
    }
    
    return liste;
  }
  
  /**
   * Retourner une vue en connaissant son ID
   */
  public Vue retournerUneVue(int id) {
    Vue vue = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT * FROM vue WHERE vue_id = '" + id + "';");
      
      // traitement des vues : rajouter le filtre utilisateur
      while (resultSet.next())
        vue = new Vue(resultSet.getInt(5), resultSet.getInt(7), resultSet.getInt(1), resultSet.getString(4), resultSet.getString(3),
            resultSet.getString(2));
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL retournerUneVue()", "SQLITE", "E");
      e.printStackTrace();
    }
    
    return vue;
  }
  
  /**
   * Rajouter une vue � la liste de l'utilisateur
   */
  public boolean seRajouterUneVue(Utilisateur utilisateur, String id) {
    if (id == null)
      return false;
    boolean requeteOK = false;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      // Rentrer un nouvelle vue pour l'utilisateur dans la BDD
      if (stat.executeUpdate("INSERT INTO vue_util VALUES ('" + utilisateur.getIdUtil() + "','" + Integer.parseInt(id) + "')") == 1)
        requeteOK = true;
      else
        utilisateur.getLogs().setLog("ECHEC INSERT seRajouterUneVue()", "SQLITE", "E");
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL seRajouterUneVue()", "SQLITE", "E");
      e.printStackTrace();
      return requeteOK;
    }
    
    return requeteOK;
  }
  
  /**
   * Enlever une vue de la liste de l'utilisateur
   */
  public boolean sEnleverUneVue(Utilisateur utilisateur, String id) {
    if (id == null)
      return false;
    boolean requeteOK = false;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      
      // Rentrer un nouvelle vue pour l'utilisateur dans la BDD
      if (stat.executeUpdate(
          "DELETE FROM vue_util WHERE util_id='" + utilisateur.getIdUtil() + "' AND vue_id='" + Integer.parseInt(id) + "'") == 1)
        requeteOK = true;
      else
        utilisateur.getLogs().setLog("ECHEC DELETE sEnleverUneVue()", "SQLITE", "E");
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL sEnleverUneVue()", "SQLITE", "E");
      e.printStackTrace();
      return requeteOK;
    }
    
    return requeteOK;
  }
  
  /**
   * Ins�rer un nouvel utilisateur dans la BDD SQLITE
   */
  public boolean insererNouvelUtilisateur(Utilisateur utilisateur, boolean aUnRepr) {
    boolean isOk = false;
    
    try {
      
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("select MAX(util_id) from utilisateur;");
      int nouvelId = resultSet.getInt(1) + 1;
      String requete = "INSERT INTO utilisateur VALUES ('0','" + MarbreEnvironnement.ACCES_MOBILITE + "', '" + nouvelId + "', '"
          + utilisateur.getLogin() + "')";
      if (aUnRepr)
        requete = "INSERT INTO utilisateur VALUES ('0','" + MarbreEnvironnement.ACCES_MOBILITE + "', '" + nouvelId + "', '"
            + utilisateur.getLogin() + "','')";
      // Rentrer un nouvel utilisateur dans la BDD
      // if(stat.executeUpdate("INSERT INTO utilisateur VALUES ('0','" + MarbreEnvironnement.ACCES_MOBILITE + "', '" + nouvelId + "', '" +
      // utilisateur.getLogin() + "','')")==1)
      if (stat.executeUpdate(requete) == 1) {
        utilisateur.setIdUtil(nouvelId);
        utilisateur.setNiveauAcces(MarbreEnvironnement.ACCES_MOBILITE);
        isOk = true;
      }
      else
        utilisateur.getLogs().setLog("ECHEC INSERT insererNouvelUtilisateur()", "SQLITE", "E");
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      if (!aUnRepr)
        return insererNouvelUtilisateur(utilisateur, true);
      else {
        utilisateur.getLogs().setLog("ECHEC SQL insererNouvelUtilisateur()", "SQLITE", "E");
        e.printStackTrace();
        return false;
      }
    }
    
    return isOk;
  }
  
  /**
   * Ins�rer un nouvelle bibli dans la BDD SQLITE
   */
  public boolean gererBibli(Utilisateur utilisateur) {
    if (utilisateur == null || utilisateur.getBibli() == null)
      return false;
    
    boolean isOk = false;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT bib_id FROM bibli WHERE bib_libelle = '" + utilisateur.getBibli() + "' ;");
      // si la bibli existe
      if (resultSet.next())
        isOk = true;
      // sinon
      else {
        int nouvelId = 0;
        resultSet = stat.executeQuery("select MAX(bib_id) from bibli;");
        if (resultSet.next())
          nouvelId = resultSet.getInt(1) + 1;
        // Rentrer un nouvel utilisateur dans la BDD
        if (stat.executeUpdate("INSERT INTO bibli VALUES ( '" + nouvelId + "', '" + utilisateur.getBibli() + "')") == 1)
          isOk = true;
        else
          utilisateur.getLogs().setLog("ECHEC INSERT gererBibli()", "SQLITE", "E");
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL gererBibli()", "SQLITE", "E");
      e.printStackTrace();
      return false;
    }
    
    return isOk;
  }
  
  /**
   * Installer une liste de vues Pr� requises pour l'utilisateur
   */
  private boolean insererVuesPreRequis(Utilisateur utilisateur) {
    boolean isOk = true;
    ArrayList<Vue> listeVuesPreRequis = retournerVuesPreRequises();
    
    // si la liste est vide
    if (listeVuesPreRequis == null)
      return false;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      
      int i = 0;
      while (i < listeVuesPreRequis.size() && isOk) {
        if (stat.executeUpdate(
            "INSERT INTO vue_util VALUES ( " + utilisateur.getIdUtil() + ", " + listeVuesPreRequis.get(i).getIdVue() + ")") == 1)
          i++;
        else {
          utilisateur.getLogs().setLog("ECHEC INSERT insererVuesPreRequis() indice " + i, "SQLITE", "E");
          isOk = false;
          break;
        }
      }
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL insererVuesPreRequis()", "SQLITE", "E");
      e.printStackTrace();
      return false;
    }
    
    return isOk;
  }
  
  /**
   * Installer une liste de catalogues Pr� requis pour l'utilisateur
   */
  private boolean insererCataloguesPreRequis(Utilisateur utilisateur) {
    boolean isOk = true;
    ArrayList<Catalogue> listeCataloguesPreRequis = retournerCataloguesPreRequis();
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      
      int i = 0;
      while (i < listeCataloguesPreRequis.size() && isOk) {
        if (stat.executeUpdate("INSERT INTO cata_util VALUES ( '" + utilisateur.getIdUtil() + "', "
            + listeCataloguesPreRequis.get(i).getIdCatalogue() + ")") == 1)
          i++;
        else {
          utilisateur.getLogs().setLog("ECHEC INSERT insererCataloguesPreRequis() indice " + i, "SQLITE", "E");
          isOk = false;
          break;
        }
      }
      stat.close();
    }
    catch (SQLException e) {
      utilisateur.getLogs().setLog("ECHEC SQL insererCataloguesPreRequis()", "SQLITE", "E");
      e.printStackTrace();
      return false;
    }
    
    return isOk;
  }
  
  /**
   * retourner la liste des catalogues disponibles
   */
  public ArrayList<Catalogue> retournerListeCatalogues() {
    ArrayList<Catalogue> liste = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT c.cata_id, c.cata_label, c.cata_icone FROM catalogue c, cata_util u WHERE c.cata_id = u.cata_id AND u.util_id = '"
              + utilisateur.getIdUtil() + "' ;");
      
      liste = new ArrayList<Catalogue>();
      // traitement des vues : rajouter le filtre utilisateur
      while (resultSet.next())
        liste.add(new Catalogue(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerListeCatalogues()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * retourne la liste des m�tiers propos�s par le catalogue s�lectionn�
   */
  public ArrayList<Metier> retournerListeMetiers(String catalogue) {
    ArrayList<Metier> liste = null;
    try {
      majDesConnexionsSQLITE();
      
      int cata = 0;
      if (catalogue == null)
        return null;
      
      cata = Integer.parseInt(catalogue);
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT DISTINCT(m.metier_id), m.metier_icone, metier_label FROM metier m, vue v, vue_catalogue c WHERE m.metier_id = v.metier_id AND v.vue_id = c.vue_id AND metier_actif = 1 AND c.cata_id = '"
              + cata + "';");
      
      liste = new ArrayList<Metier>();
      
      while (resultSet.next())
        liste.add(new Metier(resultSet.getString(2), resultSet.getInt(1), resultSet.getString(3)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerListeMetiers()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner la liste des vues pour un catalogue donn�
   */
  public ArrayList<Vue> retournerListeVues(String catalogue, String metier) {
    ArrayList<Vue> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT v.vue_id, v.metier_id, v.vue_code, v.vue_name, v.vue_link, v.vue_icone FROM vue v, vue_catalogue c WHERE v.vue_id = c.vue_id AND c.cata_id = '"
              + catalogue + "' AND metier_id = '" + metier + "' ORDER BY vue_code");
      
      liste = new ArrayList<Vue>();
      
      while (resultSet.next())
        liste.add(new Vue(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getString(6)));
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerListeVues()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner un catalogue en fonction de son id
   */
  public Catalogue retournerUnCatalogue(String cataString) {
    Catalogue catalogue = null;
    try {
      majDesConnexionsSQLITE();
      
      int cata = 0;
      if (cataString == null)
        return null;
      
      cata = Integer.parseInt(cataString);
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT * FROM catalogue WHERE cata_id = '" + cata + "';");
      
      while (resultSet.next())
        catalogue = new Catalogue(resultSet.getInt(2), resultSet.getString(3), resultSet.getString(1));
      
      resultSet.close();
      stat.close();
      return catalogue;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerUnCatalogue()", "SQLITE", "E");
      return null;
    }
  }
  
  /**
   * Retourner un m�tier en fonction de son id
   */
  public Metier retournerUnMetier(String metierString) {
    Metier metier = null;
    try {
      majDesConnexionsSQLITE();
      
      int met = 0;
      if (metierString == null)
        return null;
      met = Integer.parseInt(metierString);
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet =
          stat.executeQuery("SELECT metier_icone, metier_id, metier_label  FROM metier WHERE metier_id = '" + met + "';");
      
      while (resultSet.next())
        metier = new Metier(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3));
      
      resultSet.close();
      stat.close();
      return metier;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerUnMetier()", "SQLITE", "E");
      return null;
    }
  }
  
  /**
   * retourner si l'article est un favori ou non suivant si on le modifie ou non
   * @param utilisateur
   * @param onInverse
   * @return
   */
  public boolean gererUnArticleFavori(Utilisateur utilisateur, boolean onInverse) {
    // On va tester la pr�sence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT article_id FROM favoris_articles WHERE util_id = '" + utilisateur.getIdUtil()
          + "' AND bib_id = '" + utilisateur.getBibli() + "' AND article_id = '"
          + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim() + SEPARATEUR_IDS
          + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        stat.close();
        // on veut le supprimer des favoris
        if (onInverse) {
          if (stat.executeUpdate(
              "DELETE FROM favoris_articles WHERE util_id = '" + utilisateur.getIdUtil() + "' AND bib_id = '" + utilisateur.getBibli()
                  + "' AND article_id = '" + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim()
                  + SEPARATEUR_IDS + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "'; ") == 1)
            return false;
          else {
            utilisateur.getLogs().setLog("ECHEC DELETE gererUnArticleFavori()", "SQLITE", "E");
            return true;
          }
        }
        // sinon on retourne sa pr�sence
        else
          return true;
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        stat.close();
        // on veut le rajouter aux favoris
        if (onInverse) {
          if (stat.executeUpdate("INSERT INTO favoris_articles VALUES ('"
              + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim() + SEPARATEUR_IDS
              + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "', '" + utilisateur.getIdUtil()
              + "', '" + utilisateur.getBibli() + "'); ") == 1)
            return true;
          else {
            utilisateur.getLogs().setLog("ECHEC INSERT gererUnArticleFavori()", "SQLITE", "E");
            return false;
          }
        }
        // sinon on retourne sa non pr�sence
        else
          return false;
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL gererUnArticleFavori()", "SQLITE", "E");
      return false;
    }
  }
  
  /**
   * Retourner si l'article est un favori
   */
  public boolean isUnArticleFavori(Utilisateur utilisateur, GenericRecord record) {
    // On va tester la pr�sence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT article_id FROM favoris_articles WHERE util_id = '" + utilisateur.getIdUtil()
          + "' AND bib_id = '" + utilisateur.getBibli() + "' AND article_id = '" + record.getField("A1ETB").toString().trim()
          + SEPARATEUR_IDS + record.getField("A1ART").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        stat.close();
        return true;
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        stat.close();
        return false;
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL isUnArticleFavori()", "SQLITE", "E");
      return false;
    }
  }
  
  /**
   * Permet de r�cup�rer tous les articles favoris de l'utilisateur dans sa bibli active
   * @param utilisateur
   */
  public ArrayList<String> recupererLesArticlesFavoris(Utilisateur utilisateur, String tri) {
    if (utilisateur == null)
      return null;
    ArrayList<String> liste = new ArrayList<String>();
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT article_id FROM favoris_articles WHERE util_id = '" + utilisateur.getIdUtil()
          + "' AND bib_id = '" + utilisateur.getBibli() + "' ORDER BY article_id " + tri + ";");
      while (resultSet.next())
        liste.add(resultSet.getString(1));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererLesArticlesFavoris()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * retourner si le client est un favori ou non suivant si on le modifie ou non
   * @param utilisateur
   * @param onInverse
   * @return
   */
  public boolean gererUnClientFavori(Utilisateur utilisateur, boolean onInverse) {
    // On va tester la pr�sence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT client_id FROM favoris_clients WHERE util_id = '" + utilisateur.getIdUtil()
          + "' AND bib_id = '" + utilisateur.getBibli() + "' AND client_id = '"
          + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim() + SEPARATEUR_IDS
          + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim() + SEPARATEUR_IDS
          + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        stat.close();
        // on veut le supprimer des favoris
        if (onInverse) {
          if (stat.executeUpdate(
              "DELETE FROM favoris_clients WHERE util_id = '" + utilisateur.getIdUtil() + "' AND bib_id = '" + utilisateur.getBibli()
                  + "' AND client_id = '" + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim()
                  + SEPARATEUR_IDS + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim()
                  + SEPARATEUR_IDS + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "'; ") == 1)
            return false;
          else {
            utilisateur.getLogs().setLog("ECHEC DELETE gererUnClientFavori()", "SQLITE", "E");
            return true;
          }
        }
        // sinon on retourne sa pr�sence
        else
          return true;
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        stat.close();
        // on veut le rajouter aux favoris
        if (onInverse) {
          if (stat.executeUpdate("INSERT INTO favoris_clients VALUES ('"
              + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim() + SEPARATEUR_IDS
              + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim() + SEPARATEUR_IDS
              + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "', '" + utilisateur.getIdUtil()
              + "', '" + utilisateur.getBibli() + "'); ") == 1)
            return true;
          else {
            utilisateur.getLogs().setLog("ECHEC INSERT gererUnClientFavori()", "SQLITE", "E");
            return false;
          }
        }
        // sinon on retourne sa non pr�sence
        else
          return false;
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL gererUnClientFavori()", "SQLITE", "E");
      return false;
    }
  }
  
  /**
   * Retourner si le client est un favori
   */
  public boolean isUnClientFavori(Utilisateur utilisateur, GenericRecord record) {
    // On va tester la pr�sence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet =
          stat.executeQuery("SELECT client_id FROM favoris_clients WHERE util_id = '" + utilisateur.getIdUtil() + "' AND bib_id = '"
              + utilisateur.getBibli() + "' AND client_id = '" + record.getField("CLETB").toString().trim() + SEPARATEUR_IDS
              + record.getField("CLCLI").toString().trim() + SEPARATEUR_IDS + record.getField("CLLIV").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        stat.close();
        return true;
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        stat.close();
        return false;
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL isUnClientFavori()", "SQLITE", "E");
      return false;
    }
  }
  
  /**
   * Permet de r�cup�rer tous les clients favoris de l'utilisateur dans sa bibli active
   * @param utilisateur
   */
  public ArrayList<String> recupererLesClientsFavoris(Utilisateur utilisateur, String tri) {
    if (utilisateur == null)
      return null;
    ArrayList<String> liste = new ArrayList<String>();
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT client_id FROM favoris_clients WHERE util_id = '" + utilisateur.getIdUtil()
          + "' AND bib_id = '" + utilisateur.getBibli() + "' ORDER BY client_id " + tri + ";");
      while (resultSet.next())
        liste.add(resultSet.getString(1));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererLesClientsFavoris()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Liste les param�tres du back-office
   */
  public ArrayList<Parametre> recupererLesParametres(Utilisateur utilisateur) {
    if (utilisateur == null)
      return null;
    ArrayList<Parametre> liste = new ArrayList<Parametre>();
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT para_id,para_libelle,para_acces,para_lien FROM parametre WHERE para_acces >= '" + utilisateur.getNiveauAcces() + "';");
      while (resultSet.next())
        liste.add(new Parametre(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererLesParametres()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner un param�tre en fonction de son ID
   */
  public Parametre retournerUnParametre(int para) {
    Parametre parametre = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet =
          stat.executeQuery("SELECT para_id,para_libelle,para_acces,para_lien FROM parametre WHERE para_id = '" + para + "';");
      while (resultSet.next()) {
        parametre = new Parametre(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4));
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerUnParametre()", "SQLITE", "E");
      return null;
    }
    
    return parametre;
  }
  
  /**
   * Recup�rer la liste de tous les utilisateurs
   */
  public ArrayList<Utilisateur> recupererLesUtilisateurs() {
    ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet =
          stat.executeQuery("SELECT util_id, util_login, util_acces, util_super, util_repres FROM utilisateur ORDER BY util_login;");
      while (resultSet.next())
        liste.add(new Utilisateur(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4),
            resultSet.getString(5)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererLesUtilisateurs()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * R�cup�rer un utilisateur par son Id
   */
  public Utilisateur recupererUnUtilisateur(int id) {
    Utilisateur utilisateur = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat
          .executeQuery("SELECT util_id,util_login, util_acces, util_super, util_repres FROM utilisateur WHERE util_id = '" + id + "';");
      while (resultSet.next()) {
        utilisateur = new Utilisateur(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4),
            resultSet.getString(5));
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererUnUtilisateur()", "SQLITE", "E");
      return null;
    }
    
    return utilisateur;
  }
  
  /**
   * Modifier les propri�t�s d'un utilisateur
   */
  public boolean modifierUnUtilisateur(HttpServletRequest request) {
    
    try {
      majDesConnexionsSQLITE();
      
      String util_super = "0";
      if (request.getParameter("util_super") != null)
        util_super = "1";
      
      String majRepresentant = "";
      if (request.getParameter("util_repres") != null)
        majRepresentant = ", util_repres = '" + request.getParameter("util_repres").trim() + "' ";
      
      Statement stat = maConnexion.createStatement();
      boolean ret = (stat.executeUpdate("UPDATE utilisateur SET util_acces = '" + request.getParameter("util_acces") + "', util_super = '"
          + util_super + "' " + majRepresentant + " WHERE util_id='" + request.getParameter("utilId") + "'") == 1);
      stat.close();
      return ret;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL modifierUnUtilisateur()", "SQLITE", "E");
      return false;
    }
  }
  
  /**
   * Choper les champs de base du m�tier demand�
   */
  public String getChampsBase(int metier) {
    String retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT f.fichier_raccou, z.zone_code FROM fichier f, metier m, zone z WHERE f.fichier_id = z.fichier_id AND f.fichier_id = m.fichier_id AND m.metier_id = '"
              + metier + "' AND z.zone_base > 0 ");
      while (resultSet.next()) {
        if (retour != null)
          retour += "," + resultSet.getString(1) + "." + resultSet.getString(2);
        else
          retour = resultSet.getString(1) + "." + resultSet.getString(2);
      }
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL getChampsBase()", "SQLITE", "E");
      return null;
    }
    return retour;
  }
  
  /**
   * Choper le fichier AS400 ma�tre du m�tier correspondant: article, client, fournisseur...
   */
  public Fichier getFichierParent(int metier) {
    Fichier retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT f.fichier_id, f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where FROM fichier f, metier m WHERE f.fichier_id = m.fichier_id AND m.metier_id = '"
              + metier + "'");
      while (resultSet.next())
        retour = new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(1));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL getFichierParent()", "SQLITE", "E");
      return null;
    }
    return retour;
  }
  
  public Fichier getFichierParentModule(int module) {
    Fichier retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT f.fichier_id, f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where FROM fichier f, metier m, vue v WHERE f.fichier_id = m.fichier_id AND m.metier_id = v.metier_id AND v.vue_id = "
              + module);
      while (resultSet.next())
        retour = new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(1));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL getFichierParent()", "SQLITE", "E");
      return null;
    }
    return retour;
  }
  
  /**
   * Choper le fichier AS400 ma�tre du m�tier correspondant: article, client, fournisseur...
   */
  public Fichier getUnFichier(int id) {
    Fichier retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT f.fichier_id, f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where FROM fichier f WHERE f.fichier_id = '"
              + id + "'");
      while (resultSet.next())
        retour = new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(1));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL getUnFichier()", "SQLITE", "E");
      return null;
    }
    return retour;
  }
  
  /**
   * Choper les champs � s�lectionner dans la liste de la vue correspondante
   */
  public ArrayList<Zone> getChampsListe(int metier, int vue) {
    
    ArrayList<Zone> matrice = null;
    try {
      majDesConnexionsSQLITE();
      
      // System.out.println("Je rentre dans getChampsListe: " + metier + " - " + vue);
      matrice = new ArrayList<Zone>();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM fichier f, zone z, zones_vues v WHERE f.fichier_id = z.fichier_id AND z.zone_id = v.zone_id AND v.vue_id= '"
              + vue
              + "' AND (v.liste_zones > 0 OR (z.fichier_id = (SELECT f.fichier_id FROM fichier f, metier m WHERE f.fichier_id = m.fichier_id AND m.metier_id = '"
              + metier + "' LIMIT 1)  AND z.zone_base > 0)) ORDER BY v.liste_zones ");
      
      while (resultSet.next()) {
        // System.out.println("Zone: " + resultSet.getString(4)+ resultSet.getString(5)+ resultSet.getInt(6)+ resultSet.getInt(7)+
        // resultSet.getString(8));
        matrice.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9),
            resultSet.getString(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14),
            resultSet.getInt(15), resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL getChampsListe()", "SQLITE", "E");
      return null;
    }
    
    return matrice;
  }
  
  /**
   * Choper les champs � s�lectionner dans la fiche de la vue correspondante
   */
  public ArrayList<Zone> getChampsFiche(int metier, int vue) {
    ArrayList<Zone> matrice = null;
    try {
      majDesConnexionsSQLITE();
      
      matrice = new ArrayList<Zone>();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM fichier f, zone z, zones_vues v WHERE f.fichier_id = z.fichier_id AND z.zone_id = v.zone_id AND v.vue_id= '"
              + vue + "' ORDER BY v.bloc_zones, v.ordre_zones ");
      
      while (resultSet.next()) {
        System.out.println("zone matrice fiche: " + resultSet.getInt(1) + " - " + resultSet.getString(4));
        matrice.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9),
            resultSet.getString(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14),
            resultSet.getInt(15), resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL getChampsFiche()", "SQLITE", "E");
      return null;
    }
    
    return matrice;
  }
  
  /**
   * Retourner la liste des fichiers AS 400 attach�s � un fichier principal
   */
  public ArrayList<Fichier> retournerListeFichiers(boolean fiche, Fichier fichierParent, int vue) {
    ArrayList<Fichier> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      
      liste = new ArrayList<Fichier>();
      String triZonesListes = null;
      if (fiche)
        triZonesListes = "";
      else
        triZonesListes = " AND (zv.liste_zones > 0 OR z.zone_base > 0) ";
      
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT DISTINCT(f.fichier_id), f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where, zv.fic_lien FROM fichier f, zones_vues zv, zone z WHERE zv.zone_id = z.zone_id AND z.fichier_id = f.fichier_id AND f.fichier_id <> '"
              + fichierParent.getFichier_id() + "' AND zv.fic_lien = " + fichierParent.getFichier_id() + " AND zv.vue_id = '" + vue + "' "
              + triZonesListes);
      // System.out.println("On r�cup�re les fichiers: " + " SELECT DISTINCT(f.fichier_id), f.fichier_libelle, f.fichier_raccou,
      // f.fichier_descri, f.fichier_from, f.fichier_where, zv.fic_lien FROM fichier f, zones_vues zv, zone z WHERE zv.zone_id = z.zone_id
      // AND z.fichier_id = f.fichier_id AND f.fichier_id <> '" + fichierParent.getFichier_id() + "' AND zv.fic_lien = " +
      // fichierParent.getFichier_id() + " AND zv.vue_id = '" + vue + "' " + triZonesListes);
      
      while (resultSet.next()) {
        // System.out.println("Recup des fichiers de jointure: " + resultSet.getInt(1) + " - " + resultSet.getString(2));
        liste.add(new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(7)));
      }
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerListeFichiers()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * retourner le morceau de SQL qui fait la jointure entre deux fichiers
   */
  public String retournerJointureFichiers(Fichier fichierParent, Fichier fichierLie, int vue) {
    if (fichierParent == null || fichierLie == null)
      return null;
    String retour = null;
    
    try {
      majDesConnexionsSQLITE();
      
      // Checker les jointures dans le fichier de liaison
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT CASE WHEN (l.calcul_zone = '') THEN z.zone_code ELSE l.calcul_zone END AS caclcul1, CASE WHEN (l.calcul_fic_zone = '') THEN z2.zone_code ELSE l.calcul_fic_zone END AS caclcul2 FROM zone z,zone z2,  liaison_fichier l WHERE l.zone_id = z.zone_id AND l.fic_zone_id = z2.zone_id AND l.fichier_id = '"
              + fichierParent.getFichier_id() + "' AND l.fic_fichier_id = '" + fichierLie.getFichier_id() + "' ");
      
      while (resultSet.next()) {
        if (retour != null)
          retour += " AND ";
        else
          retour = "";
        
        retour += resultSet.getString(1) + " = " + resultSet.getString(2);
      }
      
      resultSet.close();
      
      // rajouter les sp�cificit�s de la vue
      resultSet = stat
          .executeQuery("SELECT fic_from FROM vues_fichier WHERE vue_id = " + vue + " AND fichier_id = " + fichierLie.getFichier_id());
      
      while (resultSet.next())
        retour += " AND " + resultSet.getString(1);
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerJointureFichiers()", "SQLITE", "E");
      return null;
    }
    
    return retour;
  }
  
  /**
   * R�cup�rer les zones ID du m�tier s�lectionn�
   */
  public ArrayList<Zone> recupererIdsMetier(Fichier fichier, int vue) {
    ArrayList<Zone> matrice = null;
    try {
      matrice = new ArrayList<Zone>();
      
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT DISTINCT(z.zone_id), z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM fichier f, zone z, zones_vues v WHERE z.zone_id = v.zone_id AND z.fichier_id = f.fichier_id  AND z.zone_isId >0 AND f.fichier_id = "
              + fichier.getFichier_id() + " AND v.vue_id = " + vue + " ORDER BY zone_isId ");
      
      while (resultSet.next())
        matrice.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9),
            resultSet.getString(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14),
            resultSet.getInt(15), resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererIdsMetier()", "SQLITE", "E");
      return null;
    }
    
    return matrice;
  }
  
  /**
   * R�cup�rer les zones concern�es par la recherche par mot cl�
   */
  public ArrayList<Zone> recupererChampsMotsCle(int metier) {
    ArrayList<Zone> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      
      liste = new ArrayList<Zone>();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT DISTINCT(z.zone_id), z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou FROM fichier f, zone z, metier m WHERE f.fichier_id = m.fichier_id AND f.fichier_id = z.fichier_id AND m.metier_id = "
              + metier + " AND z.zone_rech > 0 ");
      
      while (resultSet.next()) {
        // System.out.println("Zone recherche: " + resultSet.getString(13) + "." + resultSet.getString(4));
        liste.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10),
            resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), 0, 0, 0, 0, 0));
      }
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererChampsMotsCle()", "SQLITE", "E");
      return null;
    }
    
    return liste;
  }
  
  /**
   * recup�rer la valeur d'une constante via sa cl�
   */
  public String recupererConstante(String cle) {
    String valeur = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT c.const_valeur FROM constante c WHERE c.const_cle = '" + cle + "' LIMIT 1");
      
      while (resultSet.next())
        valeur = resultSet.getString(1);
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      if (utilisateur != null)
        utilisateur.getLogs().setLog("ECHEC SQL recupererConstante()", "SQLITE", "E");
      return null;
    }
    
    return valeur;
  }
  
  /**
   * Ajouter l'adresse IP de l'as400 dans les constantes. Seulement si c'est l'admin principal qui la saisit.
   */
  public boolean ajouterAdresseIpMachine(String ip, String profil) {
    boolean retour = false;
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT util_id FROM utilisateur WHERE util_login = '" + profil.trim() + "' LIMIT 1");
      
      while (resultSet.next()) {
        if (resultSet.getInt(1) == 1)
          retour = (stat.executeUpdate("UPDATE constante SET const_valeur = '" + ip + "' WHERE const_cle='IP_AS400'") == 1);
      }
      resultSet.close();
      stat.close();
      
      return retour;
    }
    catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * Retourner la liste des zones d'un module
   */
  public ArrayList<Zone> retournerZonesListeModule(int vue) {
    ArrayList<Zone> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM zone z, fichier f, zones_vues v WHERE z.fichier_id = f.fichier_id AND z.zone_id = v.zone_id AND v.vue_id = (SELECT module FROM vue WHERE vue_id ="
              + vue + " LIMIT 1) ORDER BY v.liste_zones ");
      
      while (resultSet.next()) {
        if (liste == null)
          liste = new ArrayList<Zone>();
        
        liste.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10),
            resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14), resultSet.getInt(15),
            resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
        // System.out.println("Zone module: " + resultSet.getInt(1) + " - " + resultSet.getString(4) + " ");
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner la liste des vues pr�requises � la cr�ation d'un utilisateur
   */
  private ArrayList<Vue> retournerVuesPreRequises() {
    ArrayList<Vue> vues = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT v.vue_id, v.metier_id, v.vue_code, v.vue_name, v.vue_link, v.vue_icone FROM vue v WHERE vue_requis > 0 AND vue_code > 0 ORDER BY vue_id");
      
      vues = new ArrayList<Vue>();
      
      while (resultSet.next())
        vues.add(new Vue(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getString(6)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerVuesPreRequises()", "SQLITE", "E");
      return null;
    }
    
    return vues;
  }
  
  /**
   * Retourner la liste des catalogues pr�requis � la cr�ation d'un utilisateur
   */
  private ArrayList<Catalogue> retournerCataloguesPreRequis() {
    ArrayList<Catalogue> catalogues = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT c.cata_id, c.cata_label, c.cata_icone FROM catalogue c WHERE c.cata_acces > 0 ;");
      
      catalogues = new ArrayList<Catalogue>();
      
      while (resultSet.next())
        catalogues.add(new Catalogue(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
      
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerCataloguesPreRequis()", "SQLITE", "E");
      return null;
    }
    
    return catalogues;
  }
  
  /**
   * recuperer le num�ro de version
   */
  public int recupererVersionBDD() {
    try {
      majDesConnexionsSQLITE();
      
      int version = 0;
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT const_valeur FROM constante WHERE const_cle = 'VERSION';");
      
      if (resultSet.next()) {
        try {
          version = Integer.parseInt(resultSet.getString(1));
        }
        catch (SQLException e) {
          e.printStackTrace();
          return 0;
        }
      }
      
      resultSet.close();
      stat.close();
      return version;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererVersionBDD()", "SQLITE", "E");
      return 0;
    }
    
  }
  
  /**
   * R�cup�rer la lettre d'environnement
   */
  public boolean recupererLettreEnvironnement() {
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT const_valeur FROM constante WHERE const_cle = 'LETTRE_ENV';");
      if (resultSet.next()) {
        try {
          utilisateur.setLettre_env(resultSet.getString(1));
        }
        catch (SQLException e) {
          e.printStackTrace();
          return false;
        }
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL recupererLettreEnvironnement()", "SQLITE", "E");
      return false;
    }
    return true;
  }
  
  public ArrayList<ArrayList<String>> envoyerRequete(String requete) {
    ArrayList<ArrayList<String>> listeFinale = null;
    
    if (requete == null)
      return null;
    
    if (requete.trim().toUpperCase().startsWith("SELECT")) {
      try {
        majDesConnexionsSQLITE();
        Statement stat = maConnexion.createStatement();
        ResultSet resultSet = stat.executeQuery(requete);
        ResultSetMetaData metadata = resultSet.getMetaData();
        int nombreColonnes = metadata.getColumnCount();
        
        listeFinale = new ArrayList<ArrayList<String>>();
        ArrayList<String> listeResultat = new ArrayList<String>();
        
        for (int i = 0; i < nombreColonnes; i++) {
          listeResultat.add(metadata.getColumnName(i + 1));
        }
        listeFinale.add(listeResultat);
        
        while (resultSet.next()) {
          listeResultat = new ArrayList<String>();
          for (int i = 0; i < nombreColonnes; i++)
            listeResultat.add(resultSet.getString(i + 1));
          listeFinale.add(listeResultat);
        }
        
        resultSet.close();
        stat.close();
      }
      catch (SQLException e) {
        e.printStackTrace();
        utilisateur.getLogs().setLog("ECHEC SQL envoyerRequete()", "SQLITE", "E");
        return null;
      }
    }
    else {
      if (envoyerRequeteModif(requete)) {
        ArrayList<String> listeResultat = new ArrayList<String>();
        listeResultat.add(MarbreEnvironnement.SQLITE_ISOK);
        listeFinale = new ArrayList<ArrayList<String>>();
        listeFinale.add(listeResultat);
      }
    }
    
    return listeFinale;
  }
  
  /**
   * Envoyer une requete SQLITE en UPDATE OU EN INSERT -> retourne le succ�s ou l'�chec de la proc�dure sous forme de boolean
   */
  public boolean envoyerRequeteModif(String requete) {
    try {
      majDesConnexionsSQLITE();
      Statement stat = maConnexion.createStatement();
      int retour = stat.executeUpdate(requete);
      stat.close();
      
      System.out.println("retour SQL" + requete + ": -> " + retour);
      if (requete.trim().toUpperCase().startsWith("UPDATE") || requete.trim().toUpperCase().startsWith("INSERT")
          || requete.trim().toUpperCase().startsWith("DELETE"))
        return (retour != 0);
      else if (requete.trim().toUpperCase().startsWith("CREATE ") || requete.trim().toUpperCase().startsWith("DROP TABLE")
          || requete.trim().toUpperCase().startsWith("ALTER TABLE"))
        return (retour != 0);
      else
        return false;
    }
    catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * retourne la zone qui se charge de l'�tat de la fiche m�tier
   */
  public Zone chargerZoneEtatDunMetier(int metier) {
    Zone zoneEtat = null;
    majDesConnexionsSQLITE();
    try {
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery(
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou FROM fichier f, zone z WHERE z.fichier_id = f.fichier_id AND f.fichier_id = "
              + metier + " AND z.zone_base = 2 ");
      
      while (resultSet.next()) {
        zoneEtat = new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10),
            resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), 0, 0, 0, 0, 0);
      }
      resultSet.close();
      stat.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL chargerZoneEtatDunMetier", "SQLITE", "E");
      return null;
    }
    
    return zoneEtat;
  }
  
  /**
   * Si le module poss�de un sous-mod�le, retourne l'ID du sous module sinon 0
   */
  public int retournerIdSousModule(int vue) {
    int sousModule = 0;
    
    try {
      majDesConnexionsSQLITE();
      
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT module FROM vue WHERE vue_id = " + vue + " ;");
      
      while (resultSet.next())
        sousModule = resultSet.getInt(1);
      
      resultSet.close();
      stat.close();
      return sousModule;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerIdSousModule()", "SQLITE", "E");
      return 0;
    }
  }
  
  /**
   * retourne le titre de la vue s�lectionn�e
   */
  public String retournerTitreVue(int vue) {
    String retour = "";
    
    try {
      majDesConnexionsSQLITE();
      
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT vue_name FROM vue WHERE vue_id = " + vue + " ;");
      
      while (resultSet.next())
        retour = resultSet.getString(1);
      
      resultSet.close();
      stat.close();
      return retour;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerTitreVue()", "SQLITE", "E");
      return "";
    }
  }
  
  public String retournerLabelVue(int vue) {
    String retour = "";
    
    try {
      majDesConnexionsSQLITE();
      
      Statement stat = maConnexion.createStatement();
      ResultSet resultSet = stat.executeQuery("SELECT vue_label FROM vue WHERE vue_id = " + vue + " ;");
      
      while (resultSet.next())
        retour = resultSet.getString(1);
      
      resultSet.close();
      stat.close();
      return retour;
    }
    catch (SQLException e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("ECHEC SQL retournerLabelVue()", "SQLITE", "E");
      return "";
    }
  }
  
  // +++++++++++++++++++++++++++++++++++++++++++++ GETTERS et SETTERS +++++++++++++++++++++++++++++++++++++++++++++++++
  
  /**
   * Choper L'utilisateur principal
   */
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  /**
   * Attribuer l'utilisateur principal
   */
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
