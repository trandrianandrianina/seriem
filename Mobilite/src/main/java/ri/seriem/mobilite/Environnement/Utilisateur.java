
package ri.seriem.mobilite.Environnement;

import ri.seriem.libas400.dao.gvx.database.PgvmparmManager;
import ri.seriem.libas400.dao.gvx.database.PgvmsecmManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import ri.seriem.libas400.parameters.Societe;
import ri.seriem.libas400.system.SystemManager;

import ri.seriem.mobilite.backoffice.GestionBObdd;
import ri.seriem.mobilite.backoffice.GestionBOlogs;
import ri.seriem.mobilite.backoffice.GestionBOutilisateurs;
import ri.seriem.mobilite.backoffice.GestionBOversion;
import ri.seriem.mobilite.backoffice.GestionParametres;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;
import ri.seriem.mobilite.crm.GestionFormulaire;
import ri.seriem.mobilite.metier.GestionCatalogue;
import ri.seriem.mobilite.metier.InfosClient;
import ri.seriem.mobilite.metier.Magasin;
import ri.seriem.mobilite.metier.Vue;
import ri.seriem.mobilite.metier.article.GestionArticles;
import ri.seriem.mobilite.metier.client.GestionClients;
import ri.seriem.mobilite.metier.contact.GestionContacts;
import ri.seriem.mobilite.metier.crm.LAF_ActionCommerciale;
import ri.seriem.mobilite.outils.Logs;
import ri.seriem.mobilite.outils.Outils;

import ri.seriem.libas400.database.record.ExtendRecord;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.database.record.GenericRecordManager;
import ri.seriem.libas400.dao.exp.database.PsemrtemManager;
import ri.seriem.libas400.dao.exp.database.PsemussmManager;
import ri.seriem.libas400.dao.exp.programs.contact.M_Contact;

public class Utilisateur {
  // Variables
  private int idUtil = 0;
  private String login = null;
  private String mdp = null;
  private String machine = null;
  private String bibli = null;
  private String lettre_env = null;
  private String etb = null;
  private Magasin magasin = null;
  private String codeRepresentant = null;
  private GestionClients gestionClients = null;
  private GestionArticles gestionArticles = null;
  private GestionCatalogue gestionCatalogue = null;
  private GestionParametres gestionParametres = null;
  private GestionContacts gestionContacts = null;
  private GestionBOutilisateurs gestionBOutilisateur = null;
  private GestionBOversion gestionBOversion = null;
  private GestionBOlogs gestionBOlogs = null;
  private GestionBObdd gestionBObdd = null;
  private SystemManager systeme = null;
  private Connection maconnection = null;
  private Connexion_SQLite baseSQLITE = null;
  private ArrayList<Vue> mesVues = null;
  private GenericRecordManager manager = null;
  private PgvmparmManager fparametre = null;
  private Outils outils = null;
  // session fixe ou flottante (fixe: le mec peut se connecter quelque soit le nb de sessions d�j� connect�es)
  private boolean isUnSuperUtilisateur = false;
  private int maDecoAutomatique = 0;
  private Societe societe = new Societe();
  private int niveauAcces = 0;
  private String msgErreur = "";
  private Logs logs = null;
  private int nbLignesListes = 0;
  private InfosClient infosClient = null;
  private M_Contact contact = null;
  private ArrayList<GenericRecord> listeClientsCRM = null;
  private LAF_ActionCommerciale gestionAC = new LAF_ActionCommerciale();
  private GestionFormulaire formulaireCourant = new GestionFormulaire();
  private PgvmsecmManager pgvmsecm = null;
  private PgvmparmManager pgvmparm = null;
  
  /**
   * Constructeur avec les param�tres de connexion de l'utilisateur
   * @param log
   * @param passe
   * @param mach
   * @param bib
   */
  public Utilisateur(String alogin, SystemManager sys) {
    logs = new Logs(this);
    login = alogin.toUpperCase();
    machine = MarbreEnvironnement.AS_UTILISE;
    systeme = sys;
    infosClient = new InfosClient(this);
    // niveau d'acc�s par d�faut
    niveauAcces = 3;
    nbLignesListes = 50;
    
    // Gestion de la connection � DB2 de l'AS
    try {
      if ((systeme != null) && (systeme.getSystem() != null)) {
        maconnection = systeme.getdatabase().getConnection();
      }
      
      if (maconnection != null)
        manager = new GenericRecordManager(maconnection);
      
      else
        logs.setLog("ECHEC Connexion DB2 de l'utilisateur 1", "Utilisateur", "E");
    }
    catch (Exception e) {
      e.printStackTrace();
      logs.setLog("ECHEC Connexion DB2 de l'utilisateur 2", "Utilisateur", "E");
    }
    
    // Connexion � la SQLITE du serveur WEB
    if (maconnection != null) {
      // System.out.println("connection DB2 effectu�e");
      baseSQLITE = new Connexion_SQLite(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE, this);
      if (baseSQLITE != null) {
        baseSQLITE.gererUtilisateur(this);
        // R�cup�ration de la biblioth�que (curlib) de l'utilisateur
        bibli = getBibliotheque(login);
        baseSQLITE.gererBibli(this);
        // R�cup�ration de la lettre d'environnement
        if (!baseSQLITE.recupererLettreEnvironnement()) {
          setLettre_env("X");
          logs.setLog("Il y a eu un soucis lors de la lecture de LETTRE_ENV, donc on la force � X", "Utilisateur", "E");
        }
        else
          System.out.println("Lettre environnement: " + lettre_env);
      }
    }
    
    fparametre = new PgvmparmManager(maconnection);
    pgvmsecm = new PgvmsecmManager(systeme.getdatabase().getConnection());
    pgvmsecm.setLibrary(bibli);
    pgvmparm = new PgvmparmManager(systeme.getdatabase().getConnection());
    pgvmparm.setLibrary(bibli);
    
    // Recherche des informations principales sur l'utilisateur
    if (recuperationInfosDGSociete(bibli, societe)) {
      // �tablissement en cours
      etb = societe.getCodeEtablissementPilote();
      // magasin par d�faut
      // magasin = new Magasin(societe.getEtablissementPilote().getMagasinGeneral());
      try {
        ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB(login, etb);
        // De l'�tablissement si le user n'en a pas
        String codeMagasin = null;
        String stockMobilite = null;
        if (record == null) {
          
          magasin = new Magasin(societe.getEtablissementPilote().getMagasinGeneral());
          // System.out.println("code magasin par d�faut : " + magasin.getLibelleMagasin());
          getInfosMagasin(etb, magasin);
        }
        else {
          stockMobilite = record.getField("SET176").toString().trim();
          
          // on va lire le param�tre pour afficher le stock (set176, si blanc ou 0 = affiche magasin dans la DG,
          // si 1 affiche le magasin de l'utilisateur.
          
          if ((stockMobilite.equals("")) || (stockMobilite.equals("0"))) {
            magasin = new Magasin(societe.getEtablissementPilote().getMagasinGeneral());
            magasin.setCodeMagasin(societe.getEtablissementPilote().getMagasinGeneral());
            getInfosMagasin(etb, magasin);
            MarbreEnvironnement.stockDG = true;
            MarbreEnvironnement.afficheTousLesStocks = false;
            
          }
          else if (stockMobilite.equals("1")) {
            magasin = new Magasin();
            codeMagasin = (String) record.getField("SEMAUS").toString().trim();
            magasin.setCodeMagasin(codeMagasin);
            MarbreEnvironnement.stockUtilisateur = true;
            MarbreEnvironnement.afficheTousLesStocks = false;
            getInfosMagasin(etb, magasin);
          }
          else {
            magasin = new Magasin();
            codeMagasin = (String) record.getField("SEMAUS").toString().trim();
            magasin.setCodeMagasin(codeMagasin);
            MarbreEnvironnement.afficheTousLesStocks = true;
          }
        }
      }
      catch (Exception e) {
        getLogs().setLog("ECHEC recup&eagrave;ration du magasin", "UTILISATEUR", "E");
        e.printStackTrace();
      }
    }
    else {
      etb = societe.getCodeEtablissementPilote();
      if (etb == null) {
        etb = "";
      }
      magasin = new Magasin();
    }
    // Cr�ation de la boite � outils de l'utilisateur
    outils = new Outils(this);
    
    // Recherche des informations Contact(psemrtem) de l'utilisateur (TODO voir le pb d'ETB RIS ou blanc)
    recuperationInfoContact("");
  }
  
  public void deconnecterAS400() {
    if (systeme != null) {
      
      if (maconnection != null)
        try {
          maconnection.close();
          maconnection = null;
        }
        catch (SQLException e) {
          e.printStackTrace();
        }
      
      systeme.disconnect();
      
      /*systeme.getSystem().disconnectAllServices();
      systeme.getSystem().resetAllServices();
      systeme.getdatabase().disconnect();*/
      
      systeme = null;
      
      System.out.println("Jai bien tout p�t�");
    }
  }
  
  /**
   * Constructeur pour utilsisateur lambda
   */
  public Utilisateur(int id, String login, int niveau, int superU, String repres) {
    idUtil = id;
    this.login = login;
    this.niveauAcces = niveau;
    this.setUnSuperUtilisateur(superU == 1);
    this.codeRepresentant = repres;
  }
  
  /**
   * R�cup�ration de la biblioth�que utilisateur
   * @return
   */
  private String getBibliotheque(String alogin) {
    final PsemussmManager psemussmManager = new PsemussmManager(maconnection);
    String bib = psemussmManager.getBibliotheque(MarbreEnvironnement.BIBLI_ENV, alogin);
    if (bib == null)
      logs.setLog("Bibliotheque NULL", "Utilisateur", "M");
    
    return bib;
  }
  
  public void changerMag(String mag) {
    ArrayList<GenericRecord> listeMag = this.getMagasin().retournerListeDesMagasins(this);
    for (int i = 0; i < listeMag.size(); i++) {
      if (mag.equals(listeMag.get(i).getField("PARIND").toString().trim())) {
        magasin.setLibelleMagasin(listeMag.get(i).getField("LIBETB").toString().trim());
      }
    }
    magasin.setCodeMagasin(mag);
    
  }
  
  /**
   * Recup�re les informations de la DG de la soci�t�
   * @param abib
   * @return
   */
  private boolean recuperationInfosDGSociete(String abib, Societe asociete) {
    if (fparametre == null)
      return false;
    fparametre.setLibrary(abib);
    
    if (fparametre.getInfosSociete(asociete))
      return fparametre.getInfosEtablissement(asociete.getEtablissementPilote());
    else
      return false;
  }
  
  /**
   * Initialise la classe Contact � partir des infos du PSEMRTEM pour cet utilisateur
   */
  private void recuperationInfoContact(String aetb) {
    final PsemrtemManager rtem = new PsemrtemManager(maconnection);
    contact = rtem.getContact(bibli, aetb, login.toUpperCase());
    if (contact == null)
      getLogs().setLog("ECHEC recup&eagrave;ration des infos du contact", "UTILISATEUR", "E");
  }
  
  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Lecture des infos pour un magasin
   * @param etb
   * @param codemag
   * @return
   */
  public boolean getInfosMagasin(String etb, Magasin amagasin) {
    if (amagasin == null) {
      return false;
    }
    
    // GenericRecord record = new GenericRecord();
    String libMag = "";
    ArrayList<GenericRecord> liste = this.getManager()
        .select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, SUBSTR(PARZ2, 1, 24) AS LIBETB, PARZ3, PARZ4 from " + this.getBibli()
            + ".PGVMPARM where PARETB = '" + etb + "' and PARTYP = 'MA' and PARIND = '" + amagasin.getCodeMagasin() + "'");
    
    // On alimente les infos si on trouve l'enregistrement
    if (liste.size() == 1) {
      // record.setField("PARZ2", liste.get(0).toString().trim());
      libMag = liste.get(0).getField("LIBETB").toString().trim();
      amagasin.setLibelleMagasin(libMag);
      // System.out.println("libell� du magasin :" + amagasin.getLibelleMagasin().toString().trim());
      return true;
    }
    
    return false;
  }
  // +++++++++++++++++++++++++++++++++++++++++++++ GETTERS et SETTERS +++++++++++++++++++++++++++++++++++++++++++++++++
  
  public Connection getMaconnection() {
    return maconnection;
  }
  
  public void setMaconnection(Connection maconnection) {
    this.maconnection = maconnection;
  }
  
  public String getLogin() {
    return login;
  }
  
  public void setLogin(String login) {
    this.login = login;
  }
  
  public String getMachine() {
    return machine;
  }
  
  public void setMachine(String machine) {
    this.machine = machine;
  }
  
  public String getBibli() {
    if (bibli == null)
      bibli = getBibliotheque(getLogin());
    return bibli;
  }
  
  public void setBibli(String bibli) {
    this.bibli = bibli;
  }
  
  /**
   * @return le lettre_env
   */
  public String getLettre_env() {
    return lettre_env;
  }
  
  /**
   * @param lettre_env le lettre_env � d�finir
   */
  public void setLettre_env(String lettre_env) {
    this.lettre_env = lettre_env;
  }
  
  public GestionClients getGestionClients() {
    return gestionClients;
  }
  
  public GestionArticles getGestionArticles() {
    return gestionArticles;
  }
  
  public void setGestionArticles(GestionArticles gestionArticles) {
    this.gestionArticles = gestionArticles;
  }
  
  public void setGestionClients(GestionClients gestionClients) {
    this.gestionClients = gestionClients;
  }
  
  public Connexion_SQLite getBaseSQLITE() {
    return baseSQLITE;
  }
  
  public void setBaseSQLITE(Connexion_SQLite baseSQLITE) {
    this.baseSQLITE = baseSQLITE;
  }
  
  public ArrayList<Vue> getMesVues() {
    return mesVues;
  }
  
  public void setMesVues(ArrayList<Vue> mesVues) {
    this.mesVues = mesVues;
  }
  
  public int getIdUtil() {
    return idUtil;
  }
  
  public void setIdUtil(int idUtil) {
    this.idUtil = idUtil;
  }
  
  public GestionCatalogue getGestionCatalogue() {
    return gestionCatalogue;
  }
  
  public void setGestionCatalogue(GestionCatalogue gestionCatalogue) {
    this.gestionCatalogue = gestionCatalogue;
  }
  
  public GenericRecordManager getManager() {
    return manager;
  }
  
  public void setManager(GenericRecordManager manager) {
    this.manager = manager;
  }
  
  public String getEtb() {
    return etb;
  }
  
  public void setEtb(String etb) {
    this.etb = etb;
  }
  
  public Magasin getMagasin() {
    return magasin;
  }
  
  public void setMagasin(Magasin magasin) {
    this.magasin = magasin;
  }
  
  public Outils getOutils() {
    return outils;
  }
  
  public void setOutils(Outils outils) {
    this.outils = outils;
  }
  
  public boolean isUnSuperUtilisateur() {
    return isUnSuperUtilisateur;
  }
  
  public void setUnSuperUtilisateur(boolean isUnSuperUtilisateur) {
    this.isUnSuperUtilisateur = isUnSuperUtilisateur;
  }
  
  public int getMaDecoAutomatique() {
    return maDecoAutomatique;
  }
  
  public void setMaDecoAutomatique(int maDecoAutomatique) {
    this.maDecoAutomatique = maDecoAutomatique;
  }
  
  public int getNiveauAcces() {
    return niveauAcces;
  }
  
  public void setNiveauAcces(int niveauAcces) {
    this.niveauAcces = niveauAcces;
  }
  
  public GestionParametres getGestionParametres() {
    return gestionParametres;
  }
  
  public void setGestionParametres(GestionParametres gestionParametres) {
    this.gestionParametres = gestionParametres;
  }
  
  public GestionBOutilisateurs getGestionBOutilisateur() {
    return gestionBOutilisateur;
  }
  
  public void setGestionBOutilisateur(GestionBOutilisateurs gestionBOutilisateur) {
    this.gestionBOutilisateur = gestionBOutilisateur;
  }
  
  public GestionBOlogs getGestionBOlogs() {
    return gestionBOlogs;
  }
  
  public void setGestionBOlogs(GestionBOlogs gestionBOlogs) {
    this.gestionBOlogs = gestionBOlogs;
  }
  
  public Logs getLogs() {
    return logs;
  }
  
  public void setLogs(Logs logs) {
    this.logs = logs;
  }
  
  public int getNbLignesListes() {
    // On en rajoute une ligne pour tester si le nb de lignes r�elles d�passe le nb de lignes affich�es
    return nbLignesListes;
  }
  
  public void setNbLignesListes(int nbLignesListes) {
    this.nbLignesListes = nbLignesListes;
  }
  
  public PgvmparmManager getFparametre() {
    return fparametre;
  }
  
  public void setFparametre(PgvmparmManager fparametre) {
    this.fparametre = fparametre;
  }
  
  public GestionBOversion getGestionBOversion() {
    return gestionBOversion;
  }
  
  public void setGestionBOversion(GestionBOversion gestionBOversion) {
    this.gestionBOversion = gestionBOversion;
  }
  
  public SystemManager getSysteme() {
    return systeme;
  }
  
  public void setSysteme(SystemManager systeme) {
    this.systeme = systeme;
  }
  
  public GestionBObdd getGestionBObdd() {
    return gestionBObdd;
  }
  
  public void setGestionBObdd(GestionBObdd gestionBObdd) {
    this.gestionBObdd = gestionBObdd;
  }
  
  public String getCodeRepresentant() {
    return codeRepresentant;
  }
  
  public void setCodeRepresentant(String codeRepresentant) {
    this.codeRepresentant = codeRepresentant;
  }
  
  public Societe getSociete() {
    return societe;
  }
  
  public void setSociete(Societe societe) {
    this.societe = societe;
  }
  
  /**
   * @return le infosClient
   */
  public InfosClient getInfosClient() {
    return infosClient;
  }
  
  /**
   * @param infosClient le infosClient � d�finir
   */
  public void setInfosClient(InfosClient infosClient) {
    this.infosClient = infosClient;
  }
  
  public M_Contact getContact() {
    return contact;
  }
  
  public void setContact(M_Contact contact) {
    this.contact = contact;
  }
  
  public ArrayList<GenericRecord> getListeClientsCRM() {
    return listeClientsCRM;
  }
  
  public void setListeClientsCRM(ArrayList<GenericRecord> listeClientsCRM) {
    this.listeClientsCRM = listeClientsCRM;
  }
  
  /**
   * @return le gestionAC
   */
  public LAF_ActionCommerciale getGestionAC() {
    return gestionAC;
  }
  
  /**
   * @param gestionAC le gestionAC � d�finir
   */
  public void setGestionAC(LAF_ActionCommerciale gestionAC) {
    this.gestionAC = gestionAC;
  }
  
  /**
   * @return le formulaireCourant
   */
  public GestionFormulaire getFormulaireCourant() {
    return formulaireCourant;
  }
  
  /**
   * @param formulaireCourant le formulaireCourant � d�finir
   */
  public void setFormulaireCourant(GestionFormulaire formulaireCourant) {
    this.formulaireCourant = formulaireCourant;
  }
  
  public String getMdp() {
    return mdp;
  }
  
  public void setMdp(String mdp) {
    this.mdp = mdp;
  }
  
  public GestionContacts getGestionContacts() {
    return gestionContacts;
  }
  
  public void setGestionContacts(GestionContacts gestionContacts) {
    this.gestionContacts = gestionContacts;
  }
  
}
