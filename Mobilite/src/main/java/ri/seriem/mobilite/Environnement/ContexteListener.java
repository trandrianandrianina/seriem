package ri.seriem.mobilite.Environnement;


import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
 
public class ContexteListener implements ServletContextListener {
 
    public void contextInitialized(ServletContextEvent sce)
    {
        System.out.println("Debut application Mobilit�");
    }
 
    public void contextDestroyed(ServletContextEvent sce)
    {
        System.out.println("Fin application Mobilit�");
        
        Enumeration<Driver> drivers = null;
        try
        {
	        drivers = DriverManager.getDrivers();
	        while(drivers.hasMoreElements()) 
	        {
	            DriverManager.deregisterDriver(drivers.nextElement());
	            System.out.println("++++++++ MOB driver desinscrit: " + drivers.nextElement().toString());
	        }
	    } catch(Exception e) 
	    {
	    	if( (drivers != null) && drivers.hasMoreElements() )
	    		System.out.println("MOB PB pour desinscrire ce driver: " + drivers.nextElement().toString() );
	    	else
	    		System.out.println("MOB PB pour desinscrire un driver");
	    }
    }
}
