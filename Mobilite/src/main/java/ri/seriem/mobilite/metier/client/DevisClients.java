
package ri.seriem.mobilite.metier.client;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.metier.MethodesSpecifiques;
import ri.seriem.mobilite.metier.Specifiques;

public class DevisClients extends Specifiques implements MethodesSpecifiques {
  
  public DevisClients() {
    vue = 6;
  }
  
  public void construireRequeteListeRecords() {
    // maj des fichiers concern�s par cette vue
    mettreAjourFichiersListe(utilisateur.getGestionClients().getFichierParent());
    // maj de la matrice des zones qui sont des ids
    if (matriceZonesIds == null)
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    // maj de la matrice des zones de la liste
    if (matriceZonesListe == null)
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionClients().getMetier(), vue);
    
    // maj des zones de cl� et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    // maj des champs de bases n�cessaires � cette vue
    utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesListe));
    // maj des fichiers attaqu�s en SQL et leurs jointures
    // utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersListe));
    utilisateur.getGestionClients()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(), vue, false, true));
    // maj des crit�res de tri et de s�lection
    // utilisateur.getGestionClients().setCriteresBase(" cl.CLTNS <> 9 " );
    // utilisateur.getGestionClients().setCriteresBase(recupererWhere(utilisateur.getGestionClients().getFichierParent(),vue));
    utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersListe));
  }
  
  public void construireRequeteUnRecord() {
    mettreAjourFichiersFiche(utilisateur.getGestionClients().getFichierParent());
    
    if (matriceZonesIds == null)
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    
    if (matriceZonesFiche == null)
      matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionClients().getMetier(), vue);
    
    utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesFiche));
    
    // utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersFiche));
    System.out.println("Je passe ici");
    utilisateur.getGestionClients()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(), vue, true, true));
    
    utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersFiche));
    
  }
  
  // construction de ma liste de contact par client
  public void constructionListeContact() {
    // maj des fichiers concern�s par cette vue
    
    mettreAjourFichiersListe(utilisateur.getGestionClients().getFichierParent());
    // maj de la matrice des zones qui sont des ids
    if (matriceZonesIds == null)
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    // maj de la matrice des zones de la liste
    if (matriceZonesListe == null)
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionClients().getMetier(), vue);
    
    // maj des zones de cl� et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    // maj des champs de bases n�cessaires � cette vue
    utilisateur.getGestionClients().setChampsBase("RLCOD,RLETB,RLIND,RENUM,REPAC,XICOD,XIETB,XINUM,XISUF ");// ,renum,repac
                                                                                                            // ,xicod,xietb,xinum,xisuf");
    // maj des fichiers attaqu�s en SQL et leurs jointures
    utilisateur.getGestionClients()
        .setFromBase("FROM PSEMRTLM left join psemrtem on rlnumt=renum left join pgvmxlim on xilib=reetb!!digits(renum)"
            + " and xicod='D'and xityp=72 ");
    // maj des crit�res de tri et de s�lection
    utilisateur.getGestionClients().setCriteresBase("rletb='" + utilisateur.getEtb() + "' AND rlind='"
        + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI") + "' and rlcod='C'");
    
  }
  
  public String afficherListeRecords(String apport, int nbLignes) {
    if (apport.equals("")) {
      if (utilisateur.getGestionClients().getListeRecords().size() < nbLignes)
        apport = utilisateur.getGestionClients().getListeRecords().size() + " r&eacute;sultats";
      else
        apport = "Plus de " + (utilisateur.getGestionClients().getListeRecords().size() - 1) + " r&eacute;sultats";
    }
    String retour = "";
    retour = "<h1 class='tetiereListes'><span class='titreTetiere'>Devis clients</span><span class='apportListe'>" + apport
        + "</span><div class='triTetieres'><a href='clients?tri=" + clePrincipale.getZone_code()
        + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle() + "</a><a href='clients?tri="
        + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle()
        + "</a></div><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    
    // Si la liste est vide afficher un message
    if (utilisateur.getGestionClients().getListeRecords().size() == 0)
      retour += "<p class='messageListes'>Pas de client pour ces crit�res</p>";
    
    boolean isFavori = false;
    String etatFiche = null;
    // si la liste n'est pas vide
    for (int i = 0; i < utilisateur.getGestionClients().getListeRecords().size(); i++) {
      isFavori = utilisateur.getGestionClients().isUnFavori(utilisateur.getGestionClients().getListeRecords().get(i));
      etatFiche = gestionEtatFiche(utilisateur.getGestionClients(), utilisateur.getGestionClients().getListeRecords().get(i));
      
      if (i % 2 == 0)
        retour += "<div class='listesClassiques'>";
      else
        retour += "<div class='listesClassiquesF'>";
      // si la ligne est inf�rieure au nombre maximum de lignes
      if (i < nbLignes - 1) {
        retour += "<a id='client" + i + "' href='"
            + retournerLienVersDetail("clients?", utilisateur.getGestionClients().getListeRecords().get(i), matriceZonesIds)
            + "' class='detailsListe'>"
            + retournerStructureAffichageListe(utilisateur.getGestionClients().getListeRecords().get(i), isFavori, etatFiche) + "</a>";
        retour += "<a href='#' class='"
            + utilisateur.getGestionClients().retournerClasseSelection(
                utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-"
                    + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-"
                    + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim(),
                "selectionClient", "selectionClientF")
            + "' onClick=\"switchSelection(this,'selectionClient','"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim() + "');\"></a>";
      }
      // si il existe plus d'enregistrements que le max affichable
      else
        retour += "<a id='plusResultats' href='clients?ancrage=client" + i + "&tri=PLUSLIGNES&plusDeLignes=" + nbLignes
            + "'>Afficher plus de r�sultats</a>";
      
      retour += "</div>";
    }
    return retour;
  }
  
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif)
  
  {
    String retour = "";
    retour += "<h1><span class='titreH1'><span class='superflus'>Devis client: </span> "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLNOM")
        + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    retour += "<form action='clients' id='formFiche' method='post'>";
    
    // gestion de l'�tat de la fiche et du message d'alerte
    String etatFiche = gestionEtatFiche(utilisateur.getGestionClients(), utilisateur.getGestionClients().getRecordActuel());
    if (etatFiche != null)
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    
    boolean chgtBoite = false;
    boolean gestionDevis = utilisateur.getGestionClients().getRecordActuel().isPresentField("DEVISACTIFS")
        && !utilisateur.getGestionClients().getRecordActuel().getField("DEVISACTIFS").toString().equals("0");
    boolean affPasDeDevis = false;
    
    for (int i = 0; i < matriceZonesFiche.size(); i++) {
      // gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        retour += "<h2>Dernier devis</h2>";
      }
      
      if (matriceZonesFiche.get(i).getBloc_zones() == 2 && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        if (isUnFavori)
          retour += "<h2>D�tails client<span id='isUnFavori'></span></h2>";
        else
          retour += "<h2>D�tails client<span id='isPasUnFavori'></span></h2>";
      }
      
      // gestion du contenu ++++++++++++++++++++
      if (matriceZonesFiche.get(i).getBloc_zones() == 1 && !gestionDevis) {
        if (!affPasDeDevis) {
          retour += "<p>Aucune commande pour ce client</p>";
          affPasDeDevis = true;
        }
      }
      else
        // Sp�cifique
        retour += utilisateur.getGestionClients().traitementSpecMetier(this, matriceZonesFiche.get(i), isEnModif);
      
      if (i == matriceZonesFiche.size() - 1)
        retour += "</div>";
    }
    retour += "</form>";
    ;
    retour += afficherListeContactParClient(utilisateur, utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString());
    
    // affiche les contact pour un client
    /*	ArrayList<GenericRecord>listeC = getContactPourUnClient(utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString());
    	String contact="";*/
    // retour+=afficherListeContactParClient(utilisateur,utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString(),request);
    // affiche le module devis
    // for (int i=0; i<listeC.size();i++
    
    // contact = listeC.get(i).getField("RENUM").toString();*/
    retour += afficherLeModule("clients?module=7&", utilisateur.getGestionClients().getRecordActuel(), "derniers devis", "FROM "
        + utilisateur.getBibli() + ".PGVMCLIM cl LEFT OUTER JOIN  " + utilisateur.getBibli()
        + ".PGVMEBCM cv ON cl.CLCLI = cv.E1CLLP AND cl.CLLIV = cv.E1CLLS WHERE cv.E1COD = 'D' AND cv.E1TOP = 0 AND cv.E1ETA <= 2 AND cl.CLCLI = "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI") + " AND cl.CLLIV = "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV") + " AND cl.CLETB='" + utilisateur.getEtb()
        + "' ORDER BY cv.E1NUM DESC, cv.E1SUF DESC FETCH FIRST " + MarbreAffichage.NB_LIGNES_MODULES + " ROWS ONLY");
    
    return retour;
  }
  
  // liste les contacts pour un client
  public ArrayList<GenericRecord> getContactPourUnClient(String clcli) {
    if (clcli == null)
      return null;
    clcli = utilisateur.getOutils().affichageNumeroClient(clcli);
    System.out.println("client - " + clcli);
    String requete = "SELECT RLCOD,RLETB,RLIND,RENUM,REPAC,RECL1 FROM " + utilisateur.getBibli() + ".PSEMRTLM LEFT JOIN " + // ,XICOD,XIETB,XINUM,XISUF
        utilisateur.getBibli() + ".PSEMRTEM ON RLNUMT=RENUM " + // LEFT JOIN "+utilisateur.getBibli()+".PGVMXLIM ON XILIB=REETB || DIGITS
                                                                // (RENUM) AND XICOD='D' AND XITYP='72'" +
        " WHERE rletb='" + utilisateur.getEtb() + "' AND RLIND='" + clcli + "'AND RLCOD ='C'";
    System.out.println(requete);
    return utilisateur.getManager().select(requete);
  }
  
  // liste les devis pour un contact
  public ArrayList<GenericRecord> listeDevisPourUnContact(String clcli, String contact) {
    if (clcli == null && contact == null)
      ;
    clcli = utilisateur.getOutils().affichageNumeroClient(clcli);
    System.out.println("client - " + clcli);
    ArrayList<GenericRecord> liste = utilisateur.getManager()
        .select("SELECT RLCOD,RLETB,RLIND,RENUM,REPAC,RECL1,XICOD,XIETB,XINUM,XISUF,E1THTL,E1RCC,E1CRE,E1ETB,E1SUF,E1HOM FROM "
            + utilisateur.getBibli() + ".PSEMRTLM LEFT JOIN " + utilisateur.getBibli() + ".PSEMRTEM ON RLNUMT=RENUM LEFT JOIN "
            + utilisateur.getBibli() + ".PGVMXLIM ON XILIB=REETB || DIGITS (RENUM) AND XICOD='D' AND XITYP='72' LEFT JOIN "
            + utilisateur.getBibli() + ".PGVMEBCM on" + " XICOD=E1COD AND XIETB=E1ETB AND  XINUM=E1NUM AND  XISUF=E1SUF"
            + " WHERE rletb='" + utilisateur.getEtb() + "' AND RLIND='" + clcli + "'AND RLCOD ='C' AND RLNUMT = '" + contact
            + "'  order by renum");
    return liste;
  }
  // detail du devis
  
  public ArrayList<GenericRecord> detailDuDevis(String clcli, String contact, String numDevis) {
    if (clcli == null && contact == null)
      ;
    clcli = utilisateur.getOutils().affichageNumeroClient(clcli);
    System.out.println("client - " + clcli);
    ArrayList<GenericRecord> detailDevis = utilisateur.getManager()
        .select("SELECT RLCOD,RLETB,RLIND,RENUM,REPAc,XICOD,XIETB,"
            + "XINUM,XISUF,E1NCC,E1RCC, e1thtl, recl1, L1NUM,L1ERL,L1QTE,L1MHT,L1ART,E1SUF,E1ETB FROM " + utilisateur.getBibli()
            + ".psemrtlm left join " + utilisateur.getBibli() + ".psemrtem " + "on rlnumt=renum LEFT JOIN " + utilisateur.getBibli()
            + ".pgvmxlim ON XILIB=REETB || DIGITS (RENUM) AND XICOD='D' " + "AND XITYP='72' left join " + utilisateur.getBibli()
            + ".pgvmebcm on XICOD=E1COD AND XIETB=E1ETB AND XINUM=E1NUM AND  XISUF=E1SUF " + "left join " + utilisateur.getBibli()
            + ".pgvmlbcm on e1cod=l1cod and E1ETB= L1ETB and E1NUM= L1NUM and e1suf=L1SUF " + "WHERE rletb='" + utilisateur.getEtb()
            + "' AND RLIND='" + clcli + "'AND RLCOD ='C' AND RLNUMT = '" + contact + "' and XINUM='" + numDevis + "'");
    return detailDevis;
  }
  
  // recuperation de ma liste de contact---------PAS BEAU .......SI DAVID VOIT CA !!!!
  public String afficherListeContactParClient(Utilisateur utilisateur, String numcli) {
    ArrayList<GenericRecord> listeContactPourUnClient = getContactPourUnClient(numcli);
    StringBuilder retour = new StringBuilder();
    
    retour.append("<div id='bdrechCont'>");
    retour.append("<div class='borderrechCont'>");
    
    retour.append("<div><label name='labelContact' class='labelContact'>Devis par Contact</label></div>");
    
    retour.append("<form action='Contacts' name='contactDevis'>");
    retour.append("<div><input type='hidden' id='clie' value='" + numcli + "' />");
    retour.append(
        "<select name='numberContact' id='rechCont' size='1' maxlenght='100' onChange='envoiVariable()' pattern='[\\S].{1,}[\\S]' >");// onChange='envoiVariable()'>");
    // on parcourt la liste des contacts
    if (listeContactPourUnClient != null && listeContactPourUnClient.size() > 0) {
      retour.append("<option> </option>");
      for (int i = 0; i < listeContactPourUnClient.size(); i++) {
        retour.append("<option name='contactChoisi' value='" + listeContactPourUnClient.get(i).getField("RENUM") + "'>"
            + listeContactPourUnClient.get(i).getField("REPAC") + "</option>");
      }
    }
    else {
      retour.append("<option value=''>pas de contact</option>");
      retour.append("<input class='BTN' type='hidden' value ='DEVIS' onChange='envoiVariable()'/></div>");
    }
    retour.append("</select>");// +
    retour.append("<input class='BTN' type='submit' value ='DEVIS' onChange='envoiVariable()'/></div>");
    
    retour.append("</form>");
    retour.append("</div>");// +
    retour.append("</div>");
    // passer en get le contact
    retour.append("<script>" + "function envoiVariable(){" + "var cont=document.getElementById('rechCont').value;"
        + "var clie=document.getElementById('clie').value;" +
        
        "document.forms['contactDevis'].method='get'" + "document.forms['contactDevis'].submit();"
        + "if (document.getElementById('clie').value<>0)" + "alert(merci de choisir un contact)" + "}" + "</script>");
    
    return retour.toString();
    
  }
}
