
package ri.seriem.mobilite.metier.article;

import ri.seriem.libas400.dao.gvm.programs.GestionTarif;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.constantes.MarbreMetier;
import ri.seriem.mobilite.metier.MethodesSpecifiques;
import ri.seriem.mobilite.metier.Specifiques;
import ri.seriem.mobilite.metier.Zone;

import ri.seriem.libas400.database.record.GenericRecord;

public class PrixVente extends Specifiques implements MethodesSpecifiques {
  // Constantes
  private static final BigDecimal DIVIDOR = new BigDecimal(100);
  private static final BigDecimal DIVIDOR_4_DECIMALES = new BigDecimal(10000);
  
  // Variables
  private GestionTarif gestionTarif = null;
  
  /**
   * Constructeur par d�faut
   */
  public PrixVente() {
    vue = 4;
  }
  
  /**
   * @overwrite
   */
  public void construireRequeteListeRecords() {
    // maj des fichiers concern�s par cette vue
    mettreAjourFichiersListe(utilisateur.getGestionArticles().getFichierParent());
    // maj de la matrice des zones qui sont des ids
    if (matriceZonesIds == null)
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionArticles().getFichierParent(), vue);
    
    // maj des champs de bases n�cessaires � cette vue
    if (matriceZonesListe == null)
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionArticles().getMetier(), vue);
    
    // maj des zones de cl� et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    // maj des champs de bases n�cessaires � cette vue
    // TODO A1UNV PROBLEME
    utilisateur.getGestionArticles().setChampsBase(recupererCodeZones(matriceZonesListe) + ", A1UNV ");
    // maj des fichiers attaqu�s en SQL et leurs jointures
    // utilisateur.getGestionArticles().setFromBase(recupererFichiersFrom(listeFichiersListe));
    utilisateur.getGestionArticles()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionArticles().getFichierParent(), vue, false, true));
    // maj des crit�res de tri et de s�lection
    // utilisateur.getGestionArticles().setCriteresBase("(ar.A1ETB = '" + utilisateur.getEtb() + "' and ar.A1FAM <> '' and ar.A1NPU <>
    // '1') AND (ta.ATDEV = '' OR ta.ATDEV IS NULL) AND (ta.ATDAP IN (SELECT max(atdap) FROM " + utilisateur.getBibli() + "." +
    // MarbreMetier.FICHIER_TARIFS + " WHERE ATART = ta.ATART AND (ATDEV='' OR ATDEV IS NULL) AND ATDAP <= '" +
    // utilisateur.getOutils().getDateDuJour() + "' group by ATART) OR ta.ATDAP IS NULL)");
    
    // utilisateur.getGestionArticles().setCriteresBase(recupererWhere(utilisateur.getGestionArticles().getFichierParent(),vue));
    utilisateur.getGestionArticles().setCriteresBase(recupererWhere(listeFichiersListe));
  }
  
  /**
   * @overwrite
   */
  public void construireRequeteUnRecord() {
    mettreAjourFichiersFiche(utilisateur.getGestionArticles().getFichierParent());
    
    if (matriceZonesIds == null)
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionArticles().getFichierParent(), vue);
    
    if (matriceZonesFiche == null)
      matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionArticles().getMetier(), vue);
    
    utilisateur.getGestionArticles().setChampsBase(recupererCodeZones(matriceZonesFiche) + ", A1UNV ");
    
    // utilisateur.getGestionArticles().setFromBase(recupererFichiersFrom(listeFichiersFiche));
    utilisateur.getGestionArticles()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionArticles().getFichierParent(), vue, false, true));
    
    // utilisateur.getGestionArticles().setCriteresBase("(ar.A1ETB = '" + utilisateur.getEtb() + "' and ar.A1FAM <> '' and ar.A1NPU <>
    // '1') AND (ta.ATDEV = '' OR ta.ATDEV IS NULL) AND (ta.ATDAP IN (SELECT max(atdap) FROM " + utilisateur.getBibli() + "." +
    // MarbreMetier.FICHIER_TARIFS + " WHERE ATART = ta.ATART AND (ATDEV='' OR ATDEV IS NULL) AND ATDAP <= (SELECT '1' CONCAT
    // substring(YEAR(CURRENT DATE), 3, 2) CONCAT substring(digits(month(CURRENT DATE)), 6, 2) concat substring(digits(day(CURRENT DATE)),
    // 9, 2) FROM SYSIBM.SYSDUMMY1) group by ATART) OR ta.ATDAP IS NULL)");
    // utilisateur.getGestionArticles().setCriteresBase(recupererWhere(utilisateur.getGestionArticles().getFichierParent(),vue));
    utilisateur.getGestionArticles().setCriteresBase(recupererWhere(listeFichiersFiche));
    
    // utilisateur.getGestionArticles().setOrder(" ORDER BY PRIXPUBLIC DESC FETCH FIRST 1 ROWS ONLY ");
    utilisateur.getGestionArticles().setOrder(" FETCH FIRST 1 ROWS ONLY ");
  }
  
  /**
   * @overwrite
   */
  public String afficherListeRecords(String apport, int nbLignes) {
    final ArrayList<GenericRecord> listerecord = utilisateur.getGestionArticles().getListeRecords();
    
    if (apport.equals("")) {
      if (listerecord.size() < nbLignes)
        apport = listerecord.size() + " r&eacute;sultats";
      else
        apport = "Plus de " + (listerecord.size() - 1) + " r&eacute;sultats";
    }
    
    // On force la valeur principale puisque nous n'obtenons plus le tarif par une requ�te mais par un appel direct au RPG
    valeurPrincipale = new Zone("PRIXCLIENT", getTitreTetiere(true), 0, 1, 0, 0, 1);
    valeurPrincipale.setZone_type(MarbreAffichage.TYPE_MONTANT);
    // Gymnastique de merde afin de remettre la valeur principale avant la secondaire sinon probl�me d'affichage si pas de tarif...
    cleValPriSec.remove(MarbreAffichage.structureListe[3]);
    cleValPriSec.put(MarbreAffichage.structureListe[2], valeurPrincipale);
    cleValPriSec.put(MarbreAffichage.structureListe[3], valeurSecondaire);
    
    // Construction t�ti�re
    String retour = "<h1 class='tetiereListes'>" + "<span class='titreTetiere'>Prix de vente</span>" + "<span class='apportListe'>"
        + apport + "</span>" + "<div class='triTetieres'>" + "<a href='articles?tri=" + clePrincipale.getZone_code()
        + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle() + "</a>" +
        // D�sactivation car le tri ne peut plus se faire avec la requ�te --> "<a href='articles?tri=" + valeurPrincipale.getZone_code() +
        // "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle() + "</a>" +
        "<span" + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle() + "</span>"
        + "</div>" + "<a class='optionsListe' href='#' onClick=\"switchOptionsListe();\">"
        + "<img id='imgOptionsListe' src='images/optionsListe.png'/>" + "</a>" + "</h1>" + "<div id='bdrechCli' >"
        + "<div id='borderrechCli'>"
        + "<input name='recherchecli' id='rechCli' type='search' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' placeholder='Recherche client' value='"
        + utilisateur.getInfosClient().getClnom() + "' pattern='[\\S].{1,}[\\S]' onkeyup=\"rechercheCli('rechCli');\" />" + // onFocus=\"gererFocus(this);\"
        "<a id='resetClient' href=\"articles?vue=3&clcli=0\" > </a>" + "<div id='listeClient'> </div>" + "</div>" + "</div>";
        
    // Si la liste est vide afficher un message
    if (listerecord.size() == 0)
      retour += "<p class='messageListes'>Pas d'article pour ces crit�res</p>";
    
    boolean isFavori = false;
    String etatFiche = null;
    // Si la liste n'est pas vide
    System.out.println("-PrixVente (afficherListeRecords)-> Nbr enreg:" + listerecord.size());
    for (int i = 0; i < listerecord.size(); i++) {
      // TODO A1UNV PROBLEME
      String unite = null;
      if (listerecord.get(i).isPresentField("A1UNV"))
        unite = listerecord.get(i).getField("A1UNV").toString().trim();
      
      BigDecimal valeur = recherchePrixClient(utilisateur.getInfosClient().getClcli(), utilisateur.getInfosClient().getClliv(),
          (String) listerecord.get(i).getField("A1ART"), unite);
      if (valeur != null) {
        listerecord.get(i).setField("PRIXCLIENT", valeur);
        System.out
            .println("-PrixVente (afficherListeRecords)->" + listerecord.get(i).getField("PRIXCLIENT").toString() + " valeur:" + valeur);
      }
      
      isFavori = utilisateur.getGestionArticles().isUnFavori(listerecord.get(i));
      etatFiche = gestionEtatFiche(utilisateur.getGestionArticles(), listerecord.get(i));
      
      if (i % 2 == 0)
        retour += "<div class='listesClassiques'>";
      else
        retour += "<div class='listesClassiquesF'>";
      // Si la ligne est inf�rieur au nombre maximum de lignes
      if (i < nbLignes - 1) {
        retour += "<a id='article" + i + "' href='" + retournerLienVersDetail("articles?", listerecord.get(i), matriceZonesIds)
            + "' class='detailsListe'>" + retournerStructureAffichageListe(listerecord.get(i), isFavori, etatFiche) + "</a>";
        
        retour +=
            "<a href='#' class='"
                + utilisateur.getGestionArticles()
                    .retournerClasseSelection(listerecord.get(i).getField("A1ETB").toString().trim() + "-"
                        + listerecord.get(i).getField("A1ART").toString().trim(), "selectionArticle", "selectionArticleF")
                + "' onClick=\"switchSelection(this,'selectionArticle','" + listerecord.get(i).getField("A1ETB").toString().trim() + "-"
                + listerecord.get(i).getField("A1ART").toString().trim() + "');\"></a>";
      }
      // si il existe plus d'enregistrements que le max affichable
      else
        retour += "<a id='plusResultats' href='articles?ancrage=article" + i + "&tri=PLUSLIGNES&plusDeLignes=" + nbLignes
            + "'>Afficher plus de r�sultats</a>";
      
      retour += "</div>";
    }
    return retour;
  }
  
  /**
   * @overwrite
   */
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif) {
    /*String famille = "";
    
    try 
    {
    	famille = utilisateur.getFparametre().getRecordsByType_Code(utilisateur.getEtb(), "FA", utilisateur.getGestionArticles().getRecordActuel().getField("A1FAM").toString().trim()).get(0).getField(5).toString().trim();
    } catch (Exception e) {utilisateur.getLogs().setLog("ECHEC recup&eagrave;ration unit&eacute;", "STOCKDISPO", "E"); e.printStackTrace();}*/
    
    String retour = "<h1><span class='titreH1'><span class='superflus'>Prix de vente </span>"
        + utilisateur.getGestionArticles().getRecordActuel().getField("A1LIB")
        + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    retour += "<form action='articles' method='post'>";
    
    // gestion de l'�tat de la fiche et du message d'alerte
    String etatFiche = gestionEtatFiche(utilisateur.getGestionArticles(), utilisateur.getGestionArticles().getRecordActuel());
    if (etatFiche != null)
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    
    boolean chgtBoite = false;
    boolean gestionPrix = false;
    
    // TODO A1UNV PROBLEME
    String unite = null;
    if (utilisateur.getGestionArticles().getRecordActuel().isPresentField("A1UNV"))
      unite = utilisateur.getGestionArticles().getRecordActuel().getField("A1UNV").toString().trim();
    
    // TODO A1UNV PROBLEME
    if (utilisateur.getGestionArticles().getRecordActuel().isPresentField("PRIXPUBLIC") && unite != null) {
      System.out.println("utilisateur.getGestionArticles().getRecordActuel().getField('PRIXPUBLIC'):"
          + utilisateur.getGestionArticles().getRecordActuel().getField("PRIXPUBLIC").toString());
      BigDecimal valeur = (BigDecimal) utilisateur.getGestionArticles().getRecordActuel().getField("PRIXPUBLIC");
      if (unite.endsWith("*"))
        utilisateur.getGestionArticles().getRecordActuel().setField("PRIXPUBLIC", valeur.divide(DIVIDOR));
      
      System.out.println("utilisateur.getGestionArticles().getRecordActuel().getField('PRIXPUBLIC'):"
          + utilisateur.getGestionArticles().getRecordActuel().getField("PRIXPUBLIC").toString());
    }
    
    // boolean gestionPrix = utilisateur.getGestionArticles().getRecordActuel().isPresentField("PRIXPUBLIC");
    BigDecimal valeur = recherchePrixClient(utilisateur.getInfosClient().getClcli(), utilisateur.getInfosClient().getClliv(),
        (String) utilisateur.getGestionArticles().getRecordActuel().getField("A1ART"), unite);
    
    if (valeur != null) {
      gestionPrix = true;
      utilisateur.getGestionArticles().getRecordActuel().setField("PRIXCLIENT", valeur);
      // Contr�le de l'existence de la zone dans la matrice et insertion au bon endroit sinon
      boolean trouve = false;
      int index = -1;
      for (int i = 0; i < matriceZonesFiche.size(); i++) {
        if ((index == -1) && (matriceZonesFiche.get(i).getBloc_zones() == 1))
          index = i;
        if (matriceZonesFiche.get(i).getZone_code().equals("PRIXCLIENT")) {
          trouve = true;
          break;
        }
      }
      if (!trouve) {
        matriceZonesFiche.add(index, new Zone("PRIXCLIENT", getTitreTetiere(false), 0, 1, 3, 0, 1));
        matriceZonesFiche.get(index).setZone_type(MarbreAffichage.TYPE_MONTANT);
      }
    }
    boolean affPasDePrix = false;
    
    for (int i = 0; i < matriceZonesFiche.size(); i++) {
      // System.out.println("--> " + matriceZonesFiche.get(i).getZone_libelle() + " " + matriceZonesFiche.get(i).getBloc_zones());
      
      // gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        if (gestionPrix)
          retour += "<h2>Prix de vente<span id='bonusBoite1'>" + MarbreMetier.MONNAIE + " / HT</span></h2>";
        else
          retour += "<h2>Prix de vente<span id='bonusBoite1'> HT</span></h2>";
      }
      
      if ((matriceZonesFiche.get(i).getBloc_zones() == 2) && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        if (isUnFavori)
          retour += "<h2>D�tails article<span id='isUnFavori'></span></h2>";
        else
          retour += "<h2>D�tails article<span id='isPasUnFavori'></span></h2>";
      }
      
      // gestion du contenu ++++++++++++++++++++
      if ((matriceZonesFiche.get(i).getBloc_zones() == 1) && !gestionPrix) {
        if (!affPasDePrix) {
          retour += "<p>Pas de gestion de prix pour cet article</p>";
          affPasDePrix = true;
        }
      }
      else
        // tester s'il existe un traitement sp�cifique et retourner la bonne valeur
        retour += utilisateur.getGestionArticles().traitementSpecMetier(this, matriceZonesFiche.get(i), isEnModif);
      
      if (i == matriceZonesFiche.size() - 1)
        retour += "</div>";
    }
    
    retour += "</form>";
    
    return retour;
  }
  
  /**
   * Recherche le prix d'un article pour le client en cours
   * @return
   */
  private BigDecimal recherchePrixClient(BigDecimal numcli, BigDecimal livcli, String codeart, String unite) {
    System.out
        .println("-PrixVente (recherchePrixClient)->nimcli:" + numcli + " livcli:" + livcli + " codeart:" + codeart + " unite:" + unite);
    // Instanciation le premier coup
    if (gestionTarif == null) {
      System.out.println("utilisateur.getSysteme(): " + utilisateur.getSysteme());
      System.out.println("utilisateur.getLettre_env(): " + utilisateur.getLettre_env().charAt(0));
      System.out.println("utilisateur.getBibli(): " + utilisateur.getBibli());
      gestionTarif = new GestionTarif(utilisateur.getSysteme(), utilisateur.getLettre_env().charAt(0), utilisateur.getBibli());
      gestionTarif.init();
    }
    System.out.println("-rechtarif->" + numcli.intValue() + " " + livcli.intValue());
    if (gestionTarif.execute(utilisateur.getEtb(), numcli, livcli, codeart, new BigDecimal(1))) {
      BigDecimal valeur = gestionTarif.getTarif().getValue();
      if (valeur.equals(BigDecimal.ZERO)) {
        System.out
            .println(" RETOUR A NULL DE getTARIF de nimcli:" + numcli + " livcli:" + livcli + " codeart:" + codeart + " unite:" + unite);
        return null;
      }
      
      // TODO A1UNV PROBLEME
      /*if(unite!=null && unite.endsWith("*") && numcli.intValue()>0)
      	valeur = valeur.divide(DIVIDOR_4_DECIMALES);
      else valeur = valeur.divide(DIVIDOR);*/
      
      // valeur= valeur.setScale(2, BigDecimal.ROUND_HALF_DOWN);
      
      if (unite != null && unite.endsWith("*") && numcli.intValue() > 0)
        return valeur = valeur.divide(DIVIDOR_4_DECIMALES);
      return valeur = valeur.divide(DIVIDOR);
    }
    return null;
  }
  
  /**
   * Retourne le titre de la t�ti�re concernant les tarifs
   * @param affInfoTVA
   * @return
   */
  private String getTitreTetiere(boolean affInfoTVA) {
    String titre = "Prix client";
    // System.out.println("-PrixVente (getTitreTetiere)->" + utilisateur.getInfosClient().getClnom().equals(""));
    if (utilisateur.getInfosClient().getClnom().equals(""))
      titre = "Prix public";
    if (affInfoTVA)
      return titre + " HT";
    return titre;
    
  }
}
