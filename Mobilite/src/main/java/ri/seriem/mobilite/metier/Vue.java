package ri.seriem.mobilite.metier;

public class Vue 
{
	private int idVue = 0;
	private int refMetier =0;
	private int codeVue = 0;
	private String denomination = null;
	private String lien = null;
	private String image = null;
	
	public Vue(int id,int ref, int code, String deno,String link,String icone)
	{
		idVue = id;
		refMetier = ref;
		codeVue = code;
		denomination = deno;
		lien = link;
		image = icone;
	}

	public int getIdVue() 
	{
		return idVue;
	}

	public int getRefMetier() 
	{
		return refMetier;
	}

	public String getDenomination() {
		return denomination;
	}

	public String getLien() {
		return lien;
	}

	public String getImage() {
		return image;
	}

	public int getCodeVue() {
		return codeVue;
	}

	public void setCodeVue(int codeVue) {
		this.codeVue = codeVue;
	}
	
	
}
