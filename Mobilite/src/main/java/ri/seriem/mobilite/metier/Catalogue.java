package ri.seriem.mobilite.metier;

public class Catalogue 
{
	private int idCatalogue = 0;
	private String labelCatalogue = null;
	private String iconeCatalogue = null;
	
	
	public Catalogue(int id, String label, String icone)
	{
		idCatalogue = id;
		labelCatalogue = label;
		iconeCatalogue = icone;
	}


	public int getIdCatalogue() {
		return idCatalogue;
	}


	public void setIdCatalogue(int idCatalogue) {
		this.idCatalogue = idCatalogue;
	}


	public String getLabelCatalogue() {
		return labelCatalogue;
	}


	public void setLabelCatalogue(String labelCatalogue) {
		this.labelCatalogue = labelCatalogue;
	}


	public String getIconeCatalogue() {
		return iconeCatalogue;
	}


	public void setIconeCatalogue(String iconeCatalogue) {
		this.iconeCatalogue = iconeCatalogue;
	}
	
	
	
}
