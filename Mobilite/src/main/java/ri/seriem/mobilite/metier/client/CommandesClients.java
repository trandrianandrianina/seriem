package ri.seriem.mobilite.metier.client;

import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.metier.MethodesSpecifiques;
import ri.seriem.mobilite.metier.Specifiques;

public class CommandesClients  extends Specifiques implements MethodesSpecifiques
{

	public CommandesClients()
	{
		vue = 8;
	}
	
	public void construireRequeteListeRecords() 
	{
		//maj des fichiers concern�s par cette vue
		mettreAjourFichiersListe(utilisateur.getGestionClients().getFichierParent());
		//maj de la matrice des zones qui sont des ids
		if(matriceZonesIds==null)
			matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(),vue);
		//maj de la matrice des zones de la liste
		if(matriceZonesListe==null)
			matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionClients().getMetier(), vue);
						
		//maj des zones de cl� et de valeurs de la liste
		majCleValeursListe(matriceZonesListe);
						
		// maj des champs de bases n�cessaires � cette vue
		utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesListe));
		// maj des fichiers attaqu�s en SQL et leurs jointures
		//utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersListe));
		utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(),vue,false,true));
		// maj des crit�res de tri et de s�lection
		//utilisateur.getGestionClients().setCriteresBase(" cl.CLTNS <> 9 " );
		//utilisateur.getGestionClients().setCriteresBase(recupererWhere(utilisateur.getGestionClients().getFichierParent(),vue));
		utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersListe));
	}
	
	public void construireRequeteUnRecord() 
	{
		mettreAjourFichiersFiche(utilisateur.getGestionClients().getFichierParent());
		
		if(matriceZonesIds==null)
			matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(),vue);
		
		if(matriceZonesFiche==null)
			matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionClients().getMetier(), vue);
		
		utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesFiche));
		
		//utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersFiche));
		System.out.println("Je passe ici");
		utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(),vue,true,true));
		
		utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersFiche));
		
	}

	public String afficherListeRecords(String apport, int nbLignes) 
	{
		if(apport.equals(""))
		{
			if(utilisateur.getGestionClients().getListeRecords().size() < nbLignes ) apport = utilisateur.getGestionClients().getListeRecords().size() + " r&eacute;sultats";
			else apport = "Plus de " + (utilisateur.getGestionClients().getListeRecords().size()-1)  + " r&eacute;sultats";
		}
		String retour = "";
		retour = "<h1 class='tetiereListes'><span class='titreTetiere'>Commandes clients</span><span class='apportListe'>" + apport + "</span><div class='triTetieres'><a href='clients?tri=" + clePrincipale.getZone_code() + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle() + "</a><a href='clients?tri=" + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle() + "</a></div><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
		
		//Si la liste est vide afficher un message
		if(utilisateur.getGestionClients().getListeRecords().size()==0)
			retour+= "<p class='messageListes'>Pas de client pour ces crit�res</p>";
		
		boolean isFavori = false;
		String etatFiche = null;
		//si la liste n'est pas vide
		for(int i = 0; i < utilisateur.getGestionClients().getListeRecords().size(); i++)
		{
			isFavori = utilisateur.getGestionClients().isUnFavori(utilisateur.getGestionClients().getListeRecords().get(i));
			etatFiche = gestionEtatFiche(utilisateur.getGestionClients(),utilisateur.getGestionClients().getListeRecords().get(i));
			
			if(i%2==0)
				retour+= "<div class='listesClassiques'>";
			else 
				retour+= "<div class='listesClassiquesF'>";
				//si la ligne est inf�rieur au nombre maximum de lignes
				if(i < nbLignes-1)
				{
					retour += "<a id='client" + i +  "' href='" + retournerLienVersDetail("clients?",utilisateur.getGestionClients().getListeRecords().get(i), matriceZonesIds) + "' class='detailsListe'>" + retournerStructureAffichageListe(utilisateur.getGestionClients().getListeRecords().get(i),isFavori,etatFiche) + "</a>";
					
					retour+= "<a href='#' class='"+ utilisateur.getGestionClients().retournerClasseSelection(utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-" + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-" + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim(),"selectionClient","selectionClientF") +"' onClick=\"switchSelection(this,'selectionClient','" + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-" + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-" + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim() + "');\"></a>";
				}
				//si il existe plus d'enregistrements que le max affichable
				else
					retour+="<a id='plusResultats' href='clients?ancrage=client" + i +"&tri=PLUSLIGNES&plusDeLignes=" + nbLignes + "'>Afficher plus de r�sultats</a>";
				
				retour+= "</div>";
		}
		return retour;
	}

	public String afficherLeRecord(boolean isUnFavori, boolean isEnModif) 
	{
		String retour = "";
		retour += "<h1><span class='titreH1'><span class='superflus'>Commandes client: </span> " + utilisateur.getGestionClients().getRecordActuel().getField("CLNOM") + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
			retour += "<form action='clients' id='formFiche' method='post'>";
		
		//gestion de l'�tat de la fiche et du message d'alerte
		String etatFiche = gestionEtatFiche(utilisateur.getGestionClients(),utilisateur.getGestionClients().getRecordActuel());
		if(etatFiche!=null)
			retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
			
		boolean chgtBoite = false;
		boolean gestionCommandes = (utilisateur.getGestionClients().getRecordActuel().isPresentField("CDEACTIVES") && !utilisateur.getGestionClients().getRecordActuel().getField("CDEACTIVES").toString().equals("0"));
		boolean affPasDeCommandes = false;
			
		for(int i =0; i < matriceZonesFiche.size(); i++)
		{
			//gestion des boites +++++++++++++++++++
			if(i==0)
			{
				retour += "<div class='sousBoite' id='deuxBoites1'>";
					retour += "<h2>Derni�re commande</h2>";
			}
				
			if(matriceZonesFiche.get(i).getBloc_zones()==2 && !chgtBoite)
			{
				chgtBoite = true;
				retour += "</div>";
				retour += "<div class='sousBoite' id='deuxBoites2'>";
				if(isUnFavori)
					retour += "<h2>D�tails client<span id='isUnFavori'></span></h2>";
				else
					retour += "<h2>D�tails client<span id='isPasUnFavori'></span></h2>";
			}
				
			//gestion du contenu ++++++++++++++++++++
			if(matriceZonesFiche.get(i).getBloc_zones()==1 && !gestionCommandes)
			{
				if(!affPasDeCommandes)
				{ 
					retour+="<p>Aucune commande pour ce client</p>";
					affPasDeCommandes = true;
				}
			}
			else
				//Sp�cifique
				retour += utilisateur.getGestionClients().traitementSpecMetier(this,matriceZonesFiche.get(i),isEnModif);	
					
			if(i==matriceZonesFiche.size()-1)
				retour += "</div>";
		}
				
		retour += "</form>";
			
		retour += afficherLeModule("clients?module=9&",utilisateur.getGestionClients().getRecordActuel(),"derni�res commandes", "FROM " + utilisateur.getBibli() + ".PGVMCLIM cl LEFT OUTER JOIN  " + utilisateur.getBibli() + ".PGVMEBCM cv ON cl.CLCLI = cv.E1CLLP AND cl.CLLIV = cv.E1CLLS WHERE cv.E1COD = 'E' AND cv.E1TOP = 0 AND cv.E1ETA < 4 AND cv.E1ETA > 0 AND cl.CLCLI = " +  utilisateur.getGestionClients().getRecordActuel().getField("CLCLI") + " AND cl.CLLIV = "  + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV") + " AND cl.CLETB='" + utilisateur.getEtb() + "' ORDER BY cv.E1NUM DESC, cv.E1SUF DESC FETCH FIRST " + MarbreAffichage.NB_LIGNES_MODULES + " ROWS ONLY");

		return retour;
	}

}
