package ri.seriem.mobilite.metier;

import java.math.BigDecimal;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class InfosClient
{
	// Constantes
	private final static String LIBCLI_DFT = ""; 
	
	private Utilisateur utilisateur=null;
	private String clnom = LIBCLI_DFT;			// Conservation du nom du client
	private BigDecimal clcli = BigDecimal.ZERO;	// Num�ro du client s�lectionn� en cours
	private BigDecimal clliv = BigDecimal.ZERO;

	/**
	 * Constructeur
	 * @param utilisateur
	 */
	public InfosClient(Utilisateur utilisateur)
	{
		this.utilisateur= utilisateur; 
	}
	
	/**
	 * Initialisation via les param�res de l'url dans AffichageArticle 
	 * @param clcli
	 * @param clliv
	 */
	public void setWithUrlParameters(String clcli, String clliv)
	{
		if ((clcli == null) && (clliv == null)) return;
		// On a demand� un reset des donn�es du client 
		if (clcli.equals("0"))
		{
			reset();
			return;
		}
		
		setClcli(clcli);
		setClliv(clliv);
		setClnom(utilisateur.getGestionArticles().getClientNameWithClcli(getClcli(), getClliv()));
	}

	/**
	 * Initialise les valeurs par d�faut
	 */
	public void reset()
	{
		this.clnom = LIBCLI_DFT;
		setClcli(null);
		setClliv(null);
	}
	
	/**
	 * @return le clcli
	 */
	public BigDecimal getClcli()
	{
		return clcli;
	}

	/**
	 * @param clcli le clcli � d�finir
	 */
	public void setClcli(String clcli)
	{
		if (clcli == null)
			this.clcli = BigDecimal.ZERO;
		else
			this.clcli = new BigDecimal(clcli);
	}

	/**
	 * @return le clliv
	 */
	public BigDecimal getClliv()
	{
		return clliv;
	}

	/**
	 * @param clliv le clliv � d�finir
	 */
	public void setClliv(String clliv)
	{
		if (clliv == null)
			this.clliv = BigDecimal.ZERO;
		else
			this.clliv = new BigDecimal(clliv);
	}

	/**
	 * @return le clnom
	 */
	public String getClnom()
	{
		return clnom;
	}

	/**
	 * @param clnom le clnom � d�finir
	 */
	public void setClnom(String clnom)
	{
//System.out.println("-InfosClient Nom->" + clnom);
		if (clnom == null)
			reset();
		else
			this.clnom = clnom.trim();
	}
	
}
