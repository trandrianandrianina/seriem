package ri.seriem.mobilite.metier;

public class Metier 
{
	private int idMetier = 0;
	private  String labelMetier = null;
	private String iconeMetier = null;
	
	public Metier(String icone, int id, String label)
	{
		iconeMetier = icone;
		idMetier = id;
		labelMetier = label;
	}

	public int getIdMetier() {
		return idMetier;
	}

	public void setIdMetier(int idMetier) {
		this.idMetier = idMetier;
	}

	public String getLabelMetier() {
		return labelMetier;
	}

	public void setLabelMetier(String labelMetier) {
		this.labelMetier = labelMetier;
	}

	public String getIconeMetier() {
		return iconeMetier;
	}

	public void setIconeMetier(String iconeMetier) {
		this.iconeMetier = iconeMetier;
	}
	
	
}
