
package ri.seriem.mobilite.metier.crm;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class GestionItineraire {
  /**
   * M�thode qui retourne les donn�es propre � un itin�raire
   */
  public GenericRecord retournerUnClient(Utilisateur utilisateur, String numClient, String suffixeClient) {
    if (utilisateur == null)
      return null;
    
    ArrayList<GenericRecord> clients = null;
    if (utilisateur.getManager() != null) {
      System.out.println("ETB utilisateur:" + utilisateur.getEtb());
      // Traitement propre � la r�cup�ration de la requ�te
      clients = utilisateur.getManager()
          .select("SELECT CLCLI, CLLIV, CLNOM, CLCPL, CLRUE, CLLOC, CLCDP1, CLVIL FROM " + utilisateur.getBibli()
              + ".PGVMCLIM WHERE CLETB = '" + utilisateur.getEtb() + "' AND CLCLI = '" + numClient + "' AND CLLIV = '" + suffixeClient
              + "' FETCH FIRST 1 ROWS ONLY ");
    }
    
    if (clients == null || clients.size() == 0)
      return null;
    
    return clients.get(0);
  }
  
  /**
   * Retourner la liste des clients par d�faut
   */
  public ArrayList<GenericRecord> retournerClients(Utilisateur utilisateur) {
    if (utilisateur == null)
      return null;
    
    ArrayList<GenericRecord> clients = null;
    if (utilisateur.getManager() != null) {
      // TODO Traitement propre � la r�cup�ration de la requ�te A MODIFIER
      clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM FROM " + utilisateur.getBibli()
          + ".PGVMCLIM WHERE TRIM(CLNOM) <> '' ORDER BY CLNOM FETCH FIRST 10 ROWS ONLY ");
    }
    
    return clients;
  }
  
  /**
   * Retourner la liste des clients pour un crit�re de recherche
   */
  public ArrayList<GenericRecord> retournerClients(Utilisateur utilisateur, String recherche, int nbElementsMax) {
    if (utilisateur == null || recherche == null)
      return null;
    
    if (recherche.trim().equals(""))
      return null;
    else
      recherche = recherche.trim().toUpperCase();
    
    ArrayList<GenericRecord> clients = null;
    if (utilisateur.getManager() != null) {
      // TODO Traitement propre � la r�cup�ration de la requ�te A MODIFIER CORRECTEMENT
      clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBibli()
          + ".PGVMCLIM WHERE CLNOM LIKE '" + recherche.trim() + "%' ORDER BY CLNOM FETCH FIRST " + nbElementsMax + " ROWS ONLY ");
      if (clients == null || clients.size() == 0)
        clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBibli()
            + ".PGVMCLIM WHERE CLNOM LIKE '%" + recherche.trim() + "%' ORDER BY CLNOM FETCH FIRST " + nbElementsMax + " ROWS ONLY ");
      if (clients == null || clients.size() == 0)
        clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBibli()
            + ".PGVMCLIM WHERE CLVIL LIKE '%" + recherche.trim() + "%' ORDER BY CLVIL FETCH FIRST " + nbElementsMax + " ROWS ONLY ");
    }
    
    return clients;
  }
}
