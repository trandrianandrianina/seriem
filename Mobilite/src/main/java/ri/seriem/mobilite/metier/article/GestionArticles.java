
package ri.seriem.mobilite.metier.article;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import ri.seriem.mobilite.Environnement.Connexion_SQLite;
import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.metier.Gestion;
import ri.seriem.mobilite.metier.MethodesGestion;
import ri.seriem.mobilite.metier.Specifiques;
import ri.seriem.mobilite.metier.Zone;
import ri.seriem.mobilite.outils.TriGenericRecords;

import ri.seriem.libas400.database.record.GenericRecord;

/**
 * Classe de gestion m�tier des articles. Etend la classe de Gestion globale
 * @author ritoudb
 *
 */
public class GestionArticles extends Gestion implements MethodesGestion {
  private StockDispo stockdispo = null;
  private PrixVente prixVente = null;
  
  // private Magasin mag = null;
  /**
   * Constructeur de la gestion des clients
   */
  public GestionArticles(Utilisateur utilisateur) {
    // constructeur de Gestion
    super(utilisateur);
    
    metier = 2;
    fichierParent = utilisateur.getBaseSQLITE().getFichierParent(metier);
    
    order = "ORDER BY a.A1LIB ";
    /*champsBase = "a.A1ETB, a.A1ART, a.A1LIB";
    fromBase = " FROM " + utilisateur.getBibli() + "." + MarbreMetier.FICHIER_ARTICLES + " a ";*/
    
    champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
    fromBase =
        " FROM " + utilisateur.getBibli() + "." + fichierParent.getFichier_libelle() + " " + fichierParent.getFichier_raccou() + " ";
    
    criteresBase = "(" + fichierParent.getFichier_raccou() + ".A1ETB = '" + utilisateur.getEtb() + "' and "
        + fichierParent.getFichier_raccou() + ".A1FAM <> '' and " + fichierParent.getFichier_raccou() + ".A1NPU <> '1')";
    
    // Gestion de l'�tat d'une fiche
    zoneEtatFiche = utilisateur.getBaseSQLITE().chargerZoneEtatDunMetier(metier);
    // System.out.println("Zone etat: " + zoneEtatFiche.getZone_code());
    tousEtatsFiches = new HashMap<String, String>();
    tousEtatsFiches.put("2", "Article �puis�");
    tousEtatsFiches.put("5", "Article pr�-fin de s�rie");
    tousEtatsFiches.put("6", "Article fin de s�rie");
  }
  
  /**
   * R�cup�rer une liste de records via un mot-cl�
   */
  public ArrayList<GenericRecord> gestionMotCle() {
    // requeteMotCle = "SELECT " + champsBase + fromBase + " WHERE (" + criteresBase + " AND ( ar.A1ART like '%" + motCle.toUpperCase() +
    // "%' Or ar.A1CL1 like '%" + motCle.toUpperCase() + "%' Or ar.A1CL2 like '%" + motCle.toUpperCase() + "%' Or ar.A1LIB like '%" +
    // motCle.toUpperCase() + "%' ))" + order + limit;
    requeteMotCle =
        "SELECT " + champsBase + fromBase + " WHERE " + criteresBase + recuperationDesChampsMotsCle(metier, motCle) + order + limit;
    
    // System.out.println("mot cl� 1: " + requeteMotCle);
    return utilisateur.getManager().select(requeteMotCle);
  }
  
  /**
   * r�cup�rer le d�tail d'un record
   */
  public GenericRecord recupererUnRecord(String[] ids, boolean modeDeBase) {
    if (ids == null || ids.length != 2)
      return null;
    
    ArrayList<GenericRecord> liste = null;
    
    // Si on a pas de vue particuli�re
    if (modeDeBase) {
      /*champsBase = "a.A1ETB, a.A1ART, a.A1LIB, a.A1FAM, a.A1UNS ";
      fromBase =  " FROM " + utilisateur.getBibli() + "." + MarbreMetier.FICHIER_ARTICLES + " a ";*/
      champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
      fromBase =
          " FROM " + utilisateur.getBibli() + "." + fichierParent.getFichier_libelle() + " " + fichierParent.getFichier_raccou() + " ";
      criteresBase = " " + fichierParent.getFichier_raccou() + ".A1ETB <> ''";
      order = "";
    }
    else {
      // A voir si il ne faut pas refaire une gestion des vues � ce niveau
      if (typeVue == 1) {
        // vue g�n�rique
      }
      // vue stocks disponibles
      else if (typeVue == 2) {
        if (stockdispo != null)
          stockdispo.construireRequeteUnRecord();
      }
      // vue Prix
      else if (typeVue == 3) {
        if (prixVente != null)
          prixVente.construireRequeteUnRecord();
      }
    }
    
    liste = utilisateur.getManager()
        .select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND " + fichierParent.getFichier_raccou() + ".A1ETB = '"
            + ids[0] + "' AND " + fichierParent.getFichier_raccou() + ".A1ART = '" + ids[1] + "' " + order);
    System.out
        .println("Un record: SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND " + fichierParent.getFichier_raccou()
            + ".A1ETB = '" + ids[0] + "' AND " + fichierParent.getFichier_raccou() + ".A1ART = '" + ids[1] + "' " + order);
    if (liste == null || liste.size() == 0) {
      if (!modeDeBase)
        recordActuel = recupererUnRecord(ids, true);
      else
        return null;
    }
    else
      recordActuel = liste.get(0);
    
    return recordActuel;
  }
  
  /**
   * R�cup�rer le d�tail d'un record via deux codes
   */
  public GenericRecord recupererUnRecord(String code1, String code2) {
    if (code1 == null || code2 == null)
      return null;
    
    String[] tab = { code1, code2 };
    
    return recupererUnRecord(tab, false);
  }
  
  /**
   * En fonction de la vue choisie charger les infos correspondantes
   */
  public void gestionVues(String tri) {
    gererTri(tri);
    
    // vue g�n�rique
    if (typeVue == 1) {
      champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
      fromBase =
          " FROM " + utilisateur.getBibli() + "." + fichierParent.getFichier_libelle() + " " + fichierParent.getFichier_raccou() + " ";
    }
    // vue stocks disponibles
    else if (typeVue == 2) {
      if (stockdispo != null) {
        stockdispo.construireRequeteListeRecords();
        vueCourante = stockdispo;
      }
      
      if (tri == null || (tri != null && tri.equals(""))) {
        // TODO A param�trer coorectement
        typeTri = "A1ART";
        sensTri = "ASC";
      }
    }
    else if (typeVue == 3) {
      if (prixVente != null) {
        prixVente.construireRequeteListeRecords();
        vueCourante = prixVente;
      }
      // System.out.println("--> tri " + tri);
      if (tri == null || (tri != null && tri.equals(""))) {
        // TODO A param�trer coorectement
        typeTri = "A1ART";
        sensTri = "ASC";
      }
    }
    
    order = " ORDER BY " + typeTri + " " + sensTri;
  }
  
  /**
   * r�cup�re la liste des favoris de l'utilisateur
   */
  public ArrayList<GenericRecord> gestionFavoris(String tri) {
    String[] tabIds = new String[2];
    ArrayList<GenericRecord> listeFinale = new ArrayList<GenericRecord>();
    // Si il existe un tri r�el
    if (tri != null && !tri.equals("") && !tri.equals("MEMELIGNES") && !tri.equals("PLUSLIGNES") && !tri.equals("VUE")) {
      TriGenericRecords tg = new TriGenericRecords(listeRecords);
      if (sensTri.equals("ASC") || sensTri.equals("")) {
        listeFinale = tg.descendant(tri);
        // sensTri = "DESC";
      }
      else {
        listeFinale = tg.ascendant(tri);
        // sensTri = "ASC";
      }
    }
    else {
      ArrayList<GenericRecord> liste = null;
      ArrayList<String> listeArticles = utilisateur.getBaseSQLITE().recupererLesArticlesFavoris(utilisateur, sensTri);
      if (listeArticles != null && listeArticles.size() > 0) {
        // On scanne la liste d'articles existante et on la r�g�n�re avec les nouvelles donn�es
        for (int i = 0; i < listeArticles.size(); i++) {
          utilisateur.getBaseSQLITE();
          // d�couper l'ID en AIETB et A1ART
          tabIds = listeArticles.get(i).split(Connexion_SQLite.SEPARATEUR_IDS);
          // si on bien r�cup�rer les IDS SQL sur DB2
          if (tabIds != null && tabIds.length == 2) {
            liste = utilisateur.getManager()
                .select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND " + fichierParent.getFichier_raccou()
                    + ".A1ETB = '" + tabIds[0] + "' AND " + fichierParent.getFichier_raccou() + ".A1ART = '" + tabIds[1] + "' ");
            System.out.println("Favoris " + i + ": SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND "
                + fichierParent.getFichier_raccou() + ".A1ETB = '" + tabIds[0] + "' AND " + fichierParent.getFichier_raccou()
                + ".A1ART = '" + tabIds[1] + "' ");
            // Si la requ�te contient toutes le donn�es demand�es
            if (liste != null && liste.size() > 0)
              listeFinale.add(liste.get(0));
            // sinon on charge une liste minimale pour un message sp�cialis� -> ex: pas de stock
            else {
              // TODO remplacer par les champs de base quand il y aura des champs pas de base
              liste = utilisateur.getManager()
                  .select("SELECT ar.A1ETB, ar.A1ART, ar.A1LIB FROM " + utilisateur.getBibli() + "." + fichierParent.getFichier_libelle()
                      + " " + fichierParent.getFichier_raccou() + " WHERE " + fichierParent.getFichier_raccou() + ".A1ETB = '" + tabIds[0]
                      + "' AND " + fichierParent.getFichier_raccou() + ".A1ART = '" + tabIds[1] + "' ");
              listeFinale.add(liste.get(0));
            }
          }
        }
        /*TriGenericRecords tg = new TriGenericRecords(listeFinale);
        //TODO A param�trer pour les vues (diff mani�res de trier)
        if(sensTri.equals("ASC") || sensTri.equals(""))
        {
        	listeFinale = tg.descendant(tri);
        }
        else
        {
        	listeFinale = tg.ascendant(tri);
        }*/
      }
    }
    return listeFinale;
  }
  
  /**
   * Retourne si l'article en cours est un favori ou non en fonction de si on a modifi� son �tat ou non.
   */
  public boolean isUnFavori(boolean onInverse) {
    return utilisateur.getBaseSQLITE().gererUnArticleFavori(utilisateur, onInverse);
  }
  
  /**
   * Retourne si l'article en cours est un favori ou non PAS DE MOFIF
   */
  public boolean isUnFavori(GenericRecord record) {
    return utilisateur.getBaseSQLITE().isUnArticleFavori(utilisateur, record);
  }
  
  // L��� il faut trouver une solution pour cette m�thode qui est beaucoup trop lourde et mal fichue... A changer POUAAAAAAAH !!!
  /**
   * R�cup�re la liste d'articles actuels
   */
  public ArrayList<GenericRecord> recuperationDesMemeRecords(String tri) {
    if (listeRecords == null)
      return null;
    
    ArrayList<GenericRecord> listeFinale = null; // new ArrayList<GenericRecord>();
    /*GenericRecord record = null;*/
    
    // monter la liste au lieu de la descendre si il y a un tri
    int i = 0;
    int fin = listeRecords.size();
    int increment = 1;
    if (tri != null && !tri.equals("VUE")) {
      i = listeRecords.size() - 1;
      fin = -1;
      increment = -1;
    }
    
    gestionVues(tri);
    
    // On scanne la liste d'articles existante et on la r�g�n�re avec les nouvelles donn�es
    String complementWhere = "";
    
    // adapter le order � la liste
    if (listeRecords.size() > 0) {
      order = " ORDER BY CASE A1ART ";
      
      // order = " ORDER BY CASE A1ART ";
      int j = 1;
      
      while (i != fin) {
        if ((fin == -1 && i == listeRecords.size() - 1) || (fin == listeRecords.size() && i == 0))
          complementWhere += " AND ( ";
        else
          complementWhere += " OR ";
        
        complementWhere += " (ar.A1ETB = '" + listeRecords.get(i).getField("A1ETB").toString() + "'  AND ar.A1ART = '"
            + listeRecords.get(i).getField("A1ART").toString() + "') ";
        order += "WHEN '" + listeRecords.get(i).getField("A1ART").toString() + "' THEN " + j + " ";
        
        // finir le bloc de conditions
        if ((fin == -1 && i == 0) || (fin == listeRecords.size() && i == listeRecords.size() - 1)) {
          complementWhere += " ) ";
          
          criteresBase += complementWhere;
          order += " END ";
        }
        
        i += increment;
        j++;
      }
      
      listeFinale = utilisateur.getManager().select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + order);
    }
    else {
      order = " ORDER BY A1ART FETCH FIRST 1 ROWS ONLY ";
      listeFinale = new ArrayList<GenericRecord>();
    }
    
    // System.out.println("++++++++++++ MEME RECORDS : SELECT " + champsBase + fromBase + " WHERE " + criteresBase + order);
    
    return listeFinale;
  }
  
  /**
   * En fonction de la vue charger le sp�cifique demand�
   */
  public void setVue(int vue) {
    typeVue = vue;
    switch (typeVue) {
      case 1:
        break;
      case 2:
        if (this.getStockdispo() == null)
          this.setStockdispo(new StockDispo());
        vueCourante = this.getStockdispo();
        break;
      case 3:
        if (this.getPrixVente() == null)
          this.setPrixVente(new PrixVente());
        vueCourante = this.getPrixVente();
        break;
    }
  }
  
  // TODO A remplacer par la classe d'affichage num�rique
  /**
   * G�rer la d�cimale stock
   */
  public String gererDecimaleStock(String stock) {
    stock = stock.replaceAll(".000", " ");
    
    return stock;
  }
  
  /**
   * Traitement sp�cifique aux zones articles
   */
  public String traitementSpecMetier(Specifiques spec, Zone zone, boolean isEnModif) {
    String retour = "";
    
    // date d'attendu
    /*if(zone.getZone_code().equals("DTATTEN"))
    {
    	GenericRecord record = null;
    	//tester si la zone existe
    	if(utilisateur.getGestionArticles().getRecordActuel().isPresentField(zone.getZone_code()))
    	{
    		record = utilisateur.getGestionArticles().getRecordActuel();
    		//si la valeur de la ligne de bon est � 0   alors choper celle de l'ent�te
    		if(record.getField("DTATTEN").toString().trim().equals("0") || record.getField("DTATTEN").toString().trim().equals(""))
    		{
    			if(record.isPresentField("EADLP") && !record.getField("EADLP").toString().trim().equals("0") && !record.getField("EADLP").toString().trim().equals(""))
    				record.setField("DTATTEN",new BigDecimal(record.getField("EADLP").toString().trim()));   
    		}
    	}
    }*/
    
    retour += spec.retournerTypeZone(utilisateur.getGestionArticles().getRecordActuel(), zone, isEnModif);
    
    return retour;
  }
  
  /**
   * Requ�te qui permet de rechercher une liste de client par leur nom
   * @param val
   * @return
   */
  public ArrayList<GenericRecord> traitementRechercheClient(String val) {
    if (val == null)
      return null;
    String requete = "SELECT CLNOM, CLCLI, CLLIV FROM " + utilisateur.getBibli() + ".PGVMCLIM WHERE CLETB='" + utilisateur.getEtb()
        + "' AND CLNOM like '" + val.toUpperCase() + "%' order by CLNOM, CLLIV";
    // System.out.println("-tr->" + requete);
    
    return utilisateur.getManager().select(requete);
  }
  
  /**
   * Requ�te qui permet de rechercher une liste de client par leur nom
   * @param clcli
   * @param clliv
   * @return
   */
  public String getClientNameWithClcli(BigDecimal clcli, BigDecimal clliv) {
    if ((clcli == null) || (clliv == null))
      return null;
    String requete = "SELECT CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM WHERE CLETB='" + utilisateur.getEtb() + "' AND CLCLI = "
        + clcli.intValue() + " AND CLLIV = " + clliv.intValue();
    // System.out.println("-tr->" + requete);
    
    return utilisateur.getManager().firstEnrgSelect(requete, "CLNOM");
  }
  
  /**
   * retourne le stock de plusieurs magasins
   */
  public ArrayList<GenericRecord> retourneStock(String etb, String article, ArrayList<GenericRecord> mag) {
    if (etb == null || article == null || mag == null) {
      return null;
    }
    mag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    String mag1 = "";
    String mag2 = "";
    
    if (mag.size() > 0 && mag != null) {
      mag1 = (String) mag.get(0).getField("PARIND").toString().trim();
      mag2 = (String) mag.get(1).getField("PARIND").toString().trim();
      
    }
    
    String requete =
        "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + " + "S1QEM + S1QSM + S1QDM +" + " S1QES + S1QSS + S1QDS- S1RES) AS STK, S1MAG "
            + " FROM " + utilisateur.getBibli() + ".PGVMSTKM  WHERE S1ETB = '" + utilisateur.getEtb() + "' AND S1ART = '" + article + "' "
            + " AND S1MAG IN ('" + mag1 + "','" + mag2 + "') GROUP BY S1MAG,S1ART,S1ETB ";
    utilisateur.getManager().select(requete);
    
    return utilisateur.getManager().select(requete);
  }
  
  /**
   * retourne le cumul de tous les stocks
   */
  public GenericRecord retourneCumulStock(String etb, String article, ArrayList<GenericRecord> mag) {
    if (etb == null || article == null || mag == null) {
      return null;
    }
    mag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    String mag1 = "";
    String mag2 = "";
    
    if (mag.size() > 0 && mag != null) {
      mag1 = (String) mag.get(0).getField("PARIND").toString().trim();
      mag2 = (String) mag.get(1).getField("PARIND").toString().trim();
      
    }
    String requete = "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + " + "S1QEM + S1QSM + S1QDM +" + " S1QES + S1QSS + S1QDS- S1RES) AS STK "
        + " FROM " + utilisateur.getBibli() + ".PGVMSTKM  WHERE S1ETB = '" + utilisateur.getEtb() + "' AND S1ART = '" + article + "' "
        + " AND S1MAG IN ('" + mag1 + "','" + mag2 + "')";
    ArrayList<GenericRecord> cumulStock = utilisateur.getManager().select(requete);
    
    return cumulStock.get(0);
  }
  
  /**
   * retourne le stock d'un magasin et d'un article
   */
  public GenericRecord retourneUnStock(String etb, String article, String mag) {
    
    if (etb == null || article == null || mag == null) {
      return null;
    }
    
    String requete = "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM +" + " S1QES + S1QSS + S1QDS- S1RES) AS STK, S1MAG "
        + " FROM " + utilisateur.getBibli() + ".PGVMSTKM  WHERE S1ETB = '" + utilisateur.getEtb() + "' AND S1ART = '" + article + "' "
        + " AND S1MAG = '" + mag + "' GROUP BY S1MAG,S1ART,S1ETB ";
    ArrayList<GenericRecord> unStock = utilisateur.getManager().select(requete);
    // utilisateur.getManager().selectOld(requete);
    // System.out.println(utilisateur.getManager().getMsgError());
    
    return unStock.get(0);
  }
  
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++ GETTER SETTER ++++++++++++++++++++++++++++++++++++++++++++*/
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
  public StockDispo getStockdispo() {
    return stockdispo;
  }
  
  public void setStockdispo(StockDispo stock) {
    this.stockdispo = stock;
    if (this.stockdispo != null)
      this.stockdispo.setUtilisateur(utilisateur);
  }
  
  public PrixVente getPrixVente() {
    return prixVente;
  }
  
  public void setPrixVente(PrixVente prixVente) {
    this.prixVente = prixVente;
    if (this.prixVente != null)
      this.prixVente.setUtilisateur(utilisateur);
  }
  
}
