
package ri.seriem.mobilite.metier;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;

public interface MethodesGestion {
  /**
   * Permet de r�cup�rer un record avec ses ID
   */
  public GenericRecord recupererUnRecord(String[] ids, boolean modeDeBase);
  
  /**
   * Permet de switcher sur les sp�cifiques m�tier en fonction de la vue m�tier
   */
  public void gestionVues(String tri);
  
  /**
   * Construire l'objet sp�cifique demand� en fonction de la vue
   */
  public void setVue(int vue);
  
  /**
   * Permet de charger la liste des records favoris pour un m�tier
   */
  public ArrayList<GenericRecord> gestionFavoris(String tri);
  
  /**
   * Permet de r�cuperer la m�me liste de records avec des donn�es diff�rentes
   */
  public ArrayList<GenericRecord> recuperationDesMemeRecords(String tri);
  
  /**
   * Retourne si le record en cours est un favori ou non en fonction de si on doit modifier son �tat ou non.
   */
  public boolean isUnFavori(boolean onInverse);
  
  /**
   * Avant d'afficher la zone et son contenu v�rifier les traitements sp�cifiques au m�tier
   */
  public String traitementSpecMetier(Specifiques spec, Zone zone, boolean isEnModif);
  
}
