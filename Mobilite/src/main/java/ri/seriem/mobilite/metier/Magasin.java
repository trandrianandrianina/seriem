
package ri.seriem.mobilite.metier;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class Magasin {
  private String codeMagasin = "";
  private String libelleMagasin = "";
  
  public Magasin(String code) {
    if (code != null)
      codeMagasin = code;
  }
  
  public Magasin() {
    
  }
  
  public String getCodeMagasin() {
    return codeMagasin;
  }
  
  public void setCodeMagasin(String codeMagasin) {
    this.codeMagasin = codeMagasin;
  }
  
  public String getLibelleMagasin() {
    return libelleMagasin;
  }
  
  public void setLibelleMagasin(String libelleMagasin) {
    this.libelleMagasin = libelleMagasin;
  }
  
  /**
   * Retournerla liste complete des magasins
   */
  public ArrayList<GenericRecord> retournerListeDesMagasins(Utilisateur utilisateur) {
    if (utilisateur == null) {
      return null;
    }
    String requete = ("Select PARIND, SUBSTR(PARZ2, 1, 24) AS LIBETB from " + utilisateur.getBibli() + ".PGVMPARM where PARETB = '"
        + utilisateur.getEtb() + "' and PARTYP = 'MA' ORDER BY PARIND " + "FETCH FIRST 4 ROWS ONLY ");
    
    return utilisateur.getManager().select(requete);
    
  }
  
  /**
   * retourne le stock d'un magasin choisi
   */
  public GenericRecord retourneUnMagasin(Utilisateur utilisateur, String mag) {
    if (utilisateur == null) {
      return null;
    }
    
    String requete = ("SELECT PARIND, SUBSTR(PARZ2, 1, 24) AS LIBETB from " + utilisateur.getBibli() + ".PGVMPARM WHERE PARETB = '"
        + utilisateur.getEtb() + "' and PARTYP = 'MA' AND LIBETB = '" + mag + "'");
    
    ArrayList<GenericRecord> unMag = utilisateur.getManager().select(requete);
    
    return unMag.get(0);
  }
  
}
