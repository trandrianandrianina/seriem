package ri.seriem.mobilite.metier;

public interface MethodesSpecifiques 
{
	/**
	 * Construire la requ�te pour r�cup�rer un record
	 * */
	public void construireRequeteUnRecord();
	/**
	 * Construire la requ�te pour r�cup�rer une liste de records
	 * */
	public void construireRequeteListeRecords();

	/**
	 * Afficher la liste des records r�cup�r�s
	 * */
	public String afficherListeRecords(String apport, int nbLignes);
	
	/**
	 * R�cup�rer le record r�cup�r�
	 * */
	public String afficherLeRecord(boolean isUnFavori, boolean isEnModif);
}
