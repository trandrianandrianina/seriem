
package ri.seriem.mobilite.metier.crm;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class GestionGeolocalisation {
  
  /**
   * M�thode qui retourne les clients � proximit� (dans le d�partement pass� en param�tre)
   */
  public ArrayList<GenericRecord> retournerListeClients(Utilisateur utilisateur, String departement) {
    if (utilisateur == null)
      return null;
    
    ArrayList<GenericRecord> liste = null;
    if (utilisateur.getManager() != null && departement != "") {
      // TODO Traitement propre � la r�cup�ration de la requ�te
      liste = utilisateur.getManager()
          .select("SELECT CLCLI, CLLIV, CLNOM, CLCPL, CLRUE, CLLOC, CLVIL, CLPAY FROM " + utilisateur.getBibli()
              + ".PGVMCLIM WHERE TRIM(CLNOM) <> '' AND LENGTH(TRIM(CLNOM))>1 AND CLTNS <> 9 AND CLTOP = 0 AND CLETB = '"
              + utilisateur.getEtb() + "' AND CLPAY = '' AND CLCDP1 LIKE '" + departement
              + "%' ORDER BY CLNOM FETCH FIRST 15 ROWS ONLY ");
      if (liste == null)
        System.out.println("Echec requete: " + utilisateur.getManager().getMsgError());
    }
    
    return liste;
  }
  
}
