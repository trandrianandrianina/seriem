package ri.seriem.mobilite.metier;

import java.util.ArrayList;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class GestionCatalogue 
{
	private Utilisateur utilisateur = null;
	private Metier metierEnCours = null;
	private Catalogue catalogueEnCours = null;
	
	public GestionCatalogue(Utilisateur util)
	{
		this.utilisateur = util;
	}

	/**
	 * 
	 * */
	public ArrayList<Catalogue> recupererLesCatalogues()
	{
		return utilisateur.getBaseSQLITE().retournerListeCatalogues();
	}
	
	/**
	 * R�cup�rer la liste des m�tiers dont les vues correspondent au catalogue choisi
	 *  
	 * */
	public ArrayList<Metier> recupererLesMetiers(String catalogue)
	{
		//attribuer le m�tier au m�tier en cours
		if(catalogue== null) return null;
		else catalogueEnCours = utilisateur.getBaseSQLITE().retournerUnCatalogue(catalogue);
		//appel � la base SQLITE
		return utilisateur.getBaseSQLITE().retournerListeMetiers(catalogue);
	}
	
	/**
	 * R�cuperer la liste des vues correspondants au m�tier s�lectionn�
	 * 
	 * */
	public ArrayList<Vue> recupererLesVues(String catalogue, String metier)
	{
		//attribuer le m�tier au m�tier en cours
		if(metier== null) return null;
		else metierEnCours = utilisateur.getBaseSQLITE().retournerUnMetier(metier);
		//appel � la base SQLITE
		return utilisateur.getBaseSQLITE().retournerListeVues(catalogue, metier);
	}
	
	/**
	 * permet de changer dynamiquement la classe de la vue dans la liste afin de voir s'il fait partie de la s�lection ou non
	 * */
	public String retournerClasseSelection(int id)
	{
		String selection = "selectionVue";
		//controler que la selection de clients ne soit pas vide
		if(utilisateur.getMesVues()!=null)
		{
			int i =0;
			while(i <utilisateur.getMesVues().size())
			{
				//si on trouve changer la classe du client
				if(id == utilisateur.getMesVues().get(i).getIdVue())
				{
					selection = "selectionVueF";
					i = utilisateur.getMesVues().size();
				}
				i++;
			}
		}
		
		return selection;
	}
	
	/**
	 * permet d'acc�der � la version catalogue de l'image demand�e
	 * */
	public String accederImageVersionCatalogue(String image)
	{
		if(image==null) return null;
		
		image = image.replace(".png", "_cata.png");
		
		return image; 
	}
	
	/**
	 * contr�ler la pr�sence de la vue dans la liste des vues de l'utilisateur et la virer ou la rajouter en fonction 
	 * */
	public boolean controlerPresenceVue(String id)
	{
		if(id==null) return false;  
			
		boolean isOk = false;
		//contr�ler la pr�sence de la vue dans la liste des vues de l'utilisateur
		int i = 0;
		int indexOk = -1;
		while(i < utilisateur.getMesVues().size() && indexOk < 0)
		{
			if(utilisateur.getMesVues().get(i).getIdVue()== Integer.parseInt(id))
			{
				indexOk = i;
				System.out.println("J'ai trouv� la vue au rang: " + i);
			}	
			i++;
		}
		
		//Si la vue existe dans les vues utilisateur alors la virer
		if(indexOk>=0)
		{
			if(utilisateur.getBaseSQLITE().sEnleverUneVue(utilisateur,id))
				utilisateur.getMesVues().remove(indexOk);
		}
		//sinon la rajouter au vues
		else
		{
			if(utilisateur.getBaseSQLITE().seRajouterUneVue(utilisateur,id))
				utilisateur.getMesVues().add(utilisateur.getBaseSQLITE().retournerUneVue(Integer.parseInt(id)));
		}
		
		return isOk;
	}

	
	public Metier getMetierEnCours() {
		return metierEnCours;
	}


	public void setMetierEnCours(Metier metier) {
		this.metierEnCours = metier;
	}


	public Catalogue getCatalogueEnCours() {
		return catalogueEnCours;
	}


	public void setCatalogueEnCours(Catalogue catalogue) {
		this.catalogueEnCours = catalogue;
	}
	
	
// ++++++++++++++++++++++ GETTERS ET SETTERS ++++++++++++++++++++++
	
	
	
}
