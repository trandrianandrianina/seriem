
package ri.seriem.mobilite.metier;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.constantes.MarbreMetier;

public class Module extends Specifiques {
  private GenericRecord recordActuel = null;
  private String titreModule = null;
  private String labelModule = null;
  private ArrayList<Zone> matriceZonesFicheModules = null;
  private Map<String, String[]> conditionsWhere = null;
  
  public Module(Utilisateur util) {
    utilisateur = util;
  }
  
  public void majModule(int module, Map<String, String[]> donnees) {
    vue = module;
    conditionsWhere = donnees;
    
    titreModule = utilisateur.getBaseSQLITE().retournerTitreVue(vue);
    labelModule = utilisateur.getBaseSQLITE().retournerLabelVue(vue);
    
    String requete = "SELECT ";
    
    matriceZonesFicheModules = utilisateur.getBaseSQLITE().getChampsFiche(0, module);
    // recup�ration des champs � s�lectionner
    requete += recupererCodeZones(matriceZonesFicheModules);
    
    // r�cup�ration du FROM
    Fichier fichierParent = utilisateur.getBaseSQLITE().getFichierParentModule(module);
    requete += recupererFichiersFrom(fichierParent, module, true, true);
    
    // Pr�paration du WHERE
    requete += " WHERE ";
    int i = 0;
    for (Entry<String, String[]> entry : donnees.entrySet()) {
      System.out.println("CLE:" + entry.getKey() + " - " + entry.getValue()[0]);
      if (!entry.getKey().equals("module")) {
        requete += entry.getKey() + "= '" + entry.getValue()[0] + "'";
        if (i < donnees.size() - 1)
          requete += " AND ";
      }
      i++;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getManager().select(requete);
    
    if (liste != null && liste.size() != 0)
      recordActuel = liste.get(0);
  }
  
  /**
   * Afficher le record
   */
  public String afficherLeRecord() {
    String retour = "";
    
    retour += "<h1><span class='titreH1'><span class='superflus'>D�tail " + titreModule + "</span></span></h1>";
    retour += "<form action='clients' id='formFiche' method='post'>";
    
    // gestion de l'�tat de la fiche et du message d'alerte
    String etatFiche = null;// gestionEtatFiche(utilisateur.getGestionClients(),utilisateur.getGestionClients().getRecordActuel());
    if (etatFiche != null)
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    
    boolean isUnFavori = false;
    boolean chgtBoite = false;
    boolean isGere = true;
    boolean affPasGere = false;
    
    for (int i = 0; i < matriceZonesFicheModules.size(); i++) {
      // gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        retour += "<h2>Infos " + labelModule + "</h2>";
      }
      
      if (matriceZonesFicheModules.get(i).getBloc_zones() == 2 && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        // A PARAMETRER ABSOOOOLUMENT
        if (isUnFavori)
          retour += "<h2>D�tails client<span id='isUnFavori'></span></h2>";
        else
          retour += "<h2>D�tails client<span id='isPasUnFavori'></span></h2>";
      }
      
      // gestion du contenu ++++++++++++++++++++
      if (matriceZonesFicheModules.get(i).getBloc_zones() == 1 && !isGere) {
        if (!affPasGere) {
          retour += "<p>Pas de d�tail pour cette fiche</p>";
          affPasGere = true;
        }
      }
      else {
        // Sp�cifique
        if (recordActuel != null)
          retour += retournerTypeZone(recordActuel, matriceZonesFicheModules.get(i), false);
      }
      
      if (i == matriceZonesFicheModules.size() - 1)
        retour += "</div>";
    }
    
    retour += "</form>";
    retour += afficherSousModule(vue);
    
    return retour;
  }
  
  /**
   * Afficher le module demand� pour un record de cette vue
   */
  public String afficherSousModule(int vue) {
    String retour = "";
    
    int sousModule = utilisateur.getBaseSQLITE().retournerIdSousModule(vue);
    
    // r�cup�rer les zones � afficher
    ArrayList<Zone> matriceZones = utilisateur.getBaseSQLITE().retournerZonesListeModule(vue);
    /*for(int i = 0; i < matriceZones.size(); i ++)
    	System.out.println("zones SM: " + matriceZones.get(i).getZone_code());
    }*/
    
    String requete = "SELECT ";
    
    requete += recupererCodeZones(matriceZones);
    
    requete += recupererFichiersFrom(utilisateur.getBaseSQLITE().getFichierParentModule(sousModule), sousModule, false, true);
    
    // Pr�paration du WHERE
    requete += " WHERE ";
    String spec = recupererWhere(utilisateur.getBaseSQLITE().retournerListeFichiers(false,
        utilisateur.getBaseSQLITE().getFichierParentModule(sousModule), sousModule));
    if (spec != null && !spec.trim().equals(""))
      requete += (spec + " AND ");
    
    int i = 0;
    for (Entry<String, String[]> entry : conditionsWhere.entrySet()) {
      System.out.println("CLE:" + entry.getKey() + " - " + entry.getValue()[0]);
      if (!entry.getKey().equals("module")) {
        requete += entry.getKey() + "= '" + entry.getValue()[0] + "'";
        if (i < conditionsWhere.size() - 1)
          requete += " AND ";
      }
      i++;
    }
    
    // System.out.println("REQUETE SM: " + requete);
    
    ArrayList<GenericRecord> listeModule = utilisateur.getManager().select(requete);
    
    // ++++++++++++++++++++++++ A FACTORISER CAR DOUBLON SPECIFIQUE ++++++++++++++++++++++++++++++
    // afficher les donn�es
    if (listeModule != null && listeModule.size() > 0) {
      retour = "<div id='module' class='sousBoite'>";
      retour += "<h2>" + utilisateur.getBaseSQLITE().retournerTitreVue(sousModule) + "</h2>";
      
      // Ent�te de liste de module
      retour += "<div class='enteteListeModule'>";
      for (i = 0; i < matriceZones.size(); i++) {
        if (matriceZones.get(i).getListe_zones() > 0) {
          if (matriceZones.get(i).getZone_type() == MarbreAffichage.TYPE_TEL)
            retour += "<div ></div>";
          else if (matriceZones.get(i).getZone_type() == MarbreAffichage.TYPE_PARA)
            retour += "<div class='module100'>" + matriceZones.get(i).getZone_libelle() + "</div>";
          else if (matriceZones.get(i).getZone_type() == MarbreAffichage.TYPE_MAIL)
            retour += "<div></div>";
          else if (matriceZones.get(i).getZone_type() == MarbreAffichage.TYPE_DATE)
            retour += "<div class='moduleTel'>" + matriceZones.get(i).getZone_libelle() + "</div>";
          else if (matriceZones.get(i).getZone_type() == MarbreAffichage.TYPE_MONTANT
              || matriceZones.get(i).getZone_type() == MarbreAffichage.TYPE_NUME)
            retour += "<div style=\"text-align:right;\" class='" + traitementTailleModule(matriceZones.get(i).getZone_long()) + "'>"
                + matriceZones.get(i).getZone_libelle() + "</div>";
          else
            retour += "<div class='" + traitementTailleModule(matriceZones.get(i).getZone_long()) + "'>"
                + matriceZones.get(i).getZone_libelle() + "</div>";
        }
      }
      retour += "</div>";
      
      // pour chaque enregistrement
      for (i = 0; i < listeModule.size(); i++) {
        if (i % 2 == 0)
          retour += "<div class='ligneModule'>";
        else
          retour += "<div class='ligneModuleF'>";
        // afficher les donn�es en fonction de la matrice
        for (int j = 0; j < matriceZones.size(); j++) {
          // System.out.println(" Scan zone module: " + matriceZones.get(j).getZone_code() + " - liste: " +
          // matriceZonesModule.get(j).getListe_zones());
          if (matriceZones.get(j).getListe_zones() > 0 && listeModule.get(i).isPresentField(matriceZones.get(j).getZone_code())) {
            if (matriceZones.get(j).getZone_type() == MarbreAffichage.TYPE_PARA) {
              if (!listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim().equals(""))
                retour += "<a class='module100' >" + gestionParametres(listeModule.get(i), matriceZones.get(j), false) + "</a>";
              else
                retour += "<div class='module100'></div>";
            }
            else if (matriceZones.get(j).getZone_type() == MarbreAffichage.TYPE_TEL) {
              if (!listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim().equals(""))
                retour += "<a class='modulePhone' ></a>";
              else
                retour += "<div class='modulePhoneVide'></div>";
            }
            else if (matriceZones.get(j).getZone_type() == MarbreAffichage.TYPE_MAIL) {
              if (!listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim().equals(""))
                retour += "<a class='moduleMail' ></a>";
              else
                retour += "<div class='enTetemoduleMailVide'></div>";
            }
            else if (matriceZones.get(j).getZone_type() == MarbreAffichage.TYPE_DATE)
              retour += "<a class='moduleTel'>" + utilisateur.getOutils()
                  .TransformerEnDateHumaine(listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()) + "</a>";
            else if (matriceZones.get(j).getZone_type() == MarbreAffichage.TYPE_NUME)
              retour += "<a style=\"text-align:right;\" class='" + traitementTailleModule(matriceZones.get(j).getZone_long()) + "' >"
                  + gererDecimale(
                      utilisateur.getOutils().gererAffichageNumerique(listeModule.get(i).getField(matriceZones.get(j).getZone_code())))
                  + "</a>"; // listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()
            else if (matriceZones.get(j).getZone_type() == MarbreAffichage.TYPE_MONTANT)
              retour += "<a style=\"text-align:right;\" class='" + traitementTailleModule(matriceZones.get(j).getZone_long()) + "' >"
                  + utilisateur.getOutils().gererAffichageNumerique(listeModule.get(i).getField(matriceZones.get(j).getZone_code())) + " "
                  + MarbreMetier.MONNAIE + "</a>"; // listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()
            else
              retour += "<a class='" + traitementTailleModule(matriceZones.get(j).getZone_long()) + "'  >"
                  + listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim() + "</a>";
          }
        }
        
        retour += "</div>";
      }
      
      retour += "</div>";
    }
    
    return retour;
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public ArrayList<Zone> getMatriceZonesFicheModules() {
    return matriceZonesFicheModules;
  }
  
  public void setMatriceZonesFicheModules(ArrayList<Zone> matriceZonesFicheModules) {
    this.matriceZonesFicheModules = matriceZonesFicheModules;
  }
  
  public GenericRecord getRecordActuel() {
    return recordActuel;
  }
  
  public void setRecordActuel(GenericRecord recordActuel) {
    this.recordActuel = recordActuel;
  }
  
}
