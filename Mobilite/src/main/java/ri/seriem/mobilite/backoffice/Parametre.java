package ri.seriem.mobilite.backoffice;

public class Parametre 
{
	private int para_id = 0;
	private String para_libelle = null;
	private int para_acces = 0;
	private String para_lien = null;
	
	public Parametre(int id, String libelle, int acces, String lien)
	{
		para_id = id;
		para_libelle = libelle;
		para_acces = acces;
		para_lien = lien;
	}

	public int getPara_id() {
		return para_id;
	}

	public void setPara_id(int para_id) {
		this.para_id = para_id;
	}

	public String getPara_libelle() {
		return para_libelle;
	}

	public void setPara_libelle(String para_libelle) {
		this.para_libelle = para_libelle;
	}

	public int getPara_acces() {
		return para_acces;
	}

	public void setPara_acces(int para_acces) {
		this.para_acces = para_acces;
	}

	public String getPara_lien() {
		return para_lien;
	}

	public void setPara_lien(String para_lien) {
		this.para_lien = para_lien;
	}
	
	
}
