package ri.seriem.mobilite.backoffice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.affichage.PatternErgonomie;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;

/**
 * Servlet implementation class AffichageBOversion
 */
public class AffichageBOversion extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private int niveauAcces = MarbreEnvironnement.ACCES_ADMIN;
	private PatternErgonomie pattern = null;   
	private int metier = -4;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AffichageBOversion(){
    	super();
        pattern = new PatternErgonomie();
    }

    @SuppressWarnings("unchecked")
	private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
    	try
		{
    		int niveau = 2;
			boolean modeModif = false;
			String[] liensOptions = {"#","#","#"};
			String metaSpecifiques = "";
			String titreOptions = "Options";
			
			//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				//On cr�� un handler temporaire pour la session
				Utilisateur utilisateur = (Utilisateur)request.getSession().getAttribute("utilisateur");
				if(utilisateur.getNiveauAcces()<= niveauAcces)
				{
					//ancrage dans la page
					String ancrage = request.getParameter("ancrage");
					
					if(utilisateur.getGestionBOversion()==null)
						utilisateur.setGestionBOversion(new GestionBOversion(utilisateur));
					
					//passer en mode modification le cas �ch�ant
					modeModif = (request.getParameter("modeModif")!=null && request.getParameter("modeModif").equals("1"));
					
					// charger le look correspondant
					metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
					
					ServletOutputStream out = response.getOutputStream() ;
					out.println(pattern.afficherEnTete(metaSpecifiques,ancrage));
					out.println(pattern.afficherHeader(metier,niveau,null, utilisateur));
					out.println("<article id='contenu'>");
					
					if(request.getParameter("telechVersion") != null && request.getParameter("telechVersion").equals("1") && request.getParameter("ftp") == null && request.getParameter("zip") == null )
					{
						//utilisateur.deconnecterAS400();
						//Thread.sleep(10000);
						//request.getSession().invalidate();
						if(utilisateur.getGestionBOversion().telechargerVersion())
							getServletContext().getRequestDispatcher("/affichageBOversion?ftp=1").forward(request, response);
						else getServletContext().getRequestDispatcher("/affichageBOversion?ftp=2").forward(request, response);
					}
					else 
						out.println(afficherInterfaceControle(utilisateur, request.getParameter("ftp"), request.getParameter("zip")));
					
					out.println("</article>");	
					out.println(pattern.afficherFin(titreOptions,null));
				}
				else
				{
					request.getSession().invalidate();
					getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
				}
			}
			else 
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}	
		}
    	catch (Exception e) {e.printStackTrace();}
	}
    
    
    /**
     * Afficher l'interface de contr�le des versions
     * */
    private String afficherInterfaceControle(Utilisateur utilisateur, String ftp, String zip)
    {
    	String retour = "";
    	
		try 
		{
			//Si on valide un t�l�chargement de FTP
			if(zip!=null &&zip.equals("3")) 
			{
				
					retour+="<section class='secContenu' id='controleVersion'>";
					retour += "<h1><span class='titreH1'>Installation de la derni�re version</h1>";
					retour += "<div>Version " + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().getVersionWAR()) + " install�e</div>";
					retour+= "</section>";
					
					
					
			}
			else if(ftp!=null)
			{
				String resultatTransfert = "t�l�charg�e avec succ�s";
				if(ftp.equals("2"))
					resultatTransfert = "n'a pu �tre t�l�charg�e";
					
				retour+="<section class='secContenu' id='controleVersion'>";
				retour += "<h1><span class='titreH1'>Telechargement de la derni�re version</h1>";
				retour += "<div>Version " + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().getVersionWAR()) + " " + resultatTransfert + "</div>";
				retour+= "</section>";
				
			}
			
			retour+=("<section class='secContenu' id='controleVersion'>");
			retour += "<h1><span class='titreH1'>Contr�le de version</h1>";
			retour += "<div>Votre version logicielle: " + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().verifierVersionWAR()) + "</div>";
			retour += "<div>Votre version Base de donn�es: " + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().verifierVersionBDD()) + "</div>";
			
			switch(utilisateur.getGestionBOversion().controlerVersionGLobale())
			{
				case 0 : retour += "<div>Votre version est � jour"; break;
				case 1 : retour += afficherInterfacePasAJour(utilisateur); break;
				case 2 : retour += "La BDD est d�synchronis�e par rapport � votre version"; break;
				default : retour += "";
			}
			
			retour+= "</section>";
		}
		catch (Exception e) {e.printStackTrace(); return retour;}
		
		return retour;
    }
    
    /**
     * Afficher l'interface du WAR pas � jour
     * */
    private String afficherInterfacePasAJour(Utilisateur utilisateur)
    {
    	String retour = "";
    	
		retour+= "<div>Votre version logicielle n'est pas � jour</div>";
			
		retour+= "<a href='affichageBOversion?telechVersion=1'>T�l�charger la derni�re version</a>";
		
		return retour;
    }
    
    private String afficherInterfaceTelecharger()
    {
    	String retour = "";
    	
    	retour+=("<section class='secContenu' id='controleVersion'>");
		retour += "<h1><span class='titreH1'>T�l�chargement de la derni�re version</h1>";
		retour += "<div>Telechargement de la version en cours</div>";
		retour+= "</section>";
		
    	return retour;
    }
    
    private String afficherInterfaceAppliquer(Utilisateur utilisateur)
    {
    	String retour = "";
    	
    	retour+=("<section class='secContenu' id='controleVersion'>");
		retour += "<h1><span class='titreH1'>Application de la derni�re version</h1>";
		
		retour+= "</section>";
		
    	return retour;
    }
    
   
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request,response);
	}

}
