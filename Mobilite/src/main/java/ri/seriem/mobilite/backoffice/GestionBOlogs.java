package ri.seriem.mobilite.backoffice;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class GestionBOlogs 
{
	private Utilisateur utilisateur = null;
	
	public GestionBOlogs(Utilisateur utilisateur)
	{
		this.utilisateur = utilisateur;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
}
