package ri.seriem.mobilite.backoffice;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.affichage.PatternErgonomie;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;

/**
 * Servlet implementation class AffichageBOutilisateurs
 */
public class AffichageBOutilisateurs extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private int niveauAcces = MarbreEnvironnement.ACCES_ADMIN;
	private PatternErgonomie pattern = null;   
	private int metier = -2;
	//private StringBuilder sb = new StringBuilder(16000);
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AffichageBOutilisateurs() {
        super();
        pattern = new PatternErgonomie();
    }

    private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
    	try
		{
			int niveau = 2;
			boolean modeModif = false;
			//String[] liensOptions = {"#","#","#"};
			String metaSpecifiques = "";
			String titreOptions = "Options";
			
			//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				//On cr�� un handler temporaire pour la session
				Utilisateur utilisateur = (Utilisateur)request.getSession().getAttribute("utilisateur");
				if(utilisateur.getNiveauAcces()<= niveauAcces)
				{
					//ancrage dans la page
					String ancrage = request.getParameter("ancrage");
					
					if(utilisateur.getGestionBOutilisateur()==null)
						utilisateur.setGestionBOutilisateur(new GestionBOutilisateurs(utilisateur));
					
					//passer en mode modification le cas �ch�ant
					modeModif = (request.getParameter("modeModif")!=null && request.getParameter("modeModif").equals("1"));
						
					// charger le look correspondant
					metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
					if(request.getParameter("utilId")==null)
					{
						metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
						metaSpecifiques += "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"30\">";
					}
					else
					{
						metaSpecifiques += "<link href='css/fiches.css' rel='stylesheet'/>";
						metaSpecifiques += "<link href='css/BOunUtilisateur.css' rel='stylesheet'/>";
						niveau = 3;
					}
					
					ServletOutputStream out = response.getOutputStream() ;
					out.println(pattern.afficherEnTete(metaSpecifiques,ancrage));
					out.println(pattern.afficherHeader(metier,niveau,null, utilisateur));
					out.println("<article id='contenu'>");
						
					if(request.getParameter("utilId")==null)
					{
						//out.println("<section class='secContenu' id='gestionParametres'>");
						out.println(afficherLesUtilisateurs(request,out, utilisateur));
					}
					else
					{
						if(request.getParameter("modifie")!=null)
							if(utilisateur.getGestionBOutilisateur().modifierUnUtilisateur(request))
								 out.println("<p class='messagePrincipal' id='messageConfirmation'>Utilisateur modifi&eacute; avec succ&egrave;s</p>");
							else out.println("<p class='messagePrincipal' id='messageErreur'>Echec de la modification de cet utilisateur</p>");
						
						//out.println("<section class='secContenu' id='gestionParametres'>");
						out.println(afficherUnUtilisateur(out, utilisateur, Integer.parseInt(request.getParameter("utilId")),modeModif));
					}
					out.println("</article>");	
					out.println(pattern.afficherFin(titreOptions,null));
				}
				else
				{
					request.getSession().invalidate();
					getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
				}
			}
			else 
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}
		}
		catch (Exception e) {e.printStackTrace();}
	}
    
    /**
     * Afficher la fiche d'un utilisateur 
     * */
	private String afficherUnUtilisateur(ServletOutputStream out, Utilisateur utilisateur, int id, boolean enModif) 
	{
		Utilisateur util = utilisateur.getGestionBOutilisateur().recupererUnUtilisateur(id);
		if(util == null) return null;
		
		String retour = "";
		try 
		{
			retour+=("<section class='secContenu' id='gestionParametres'>");
			if(enModif)
			{
				retour += "<h1 class='h1Modification'>";
				retour+="<span id='modeModif'>Modification - </span>";
			}
			else
				retour += "<h1>";
			retour+="<span class='titreH1'>" + util.getLogin() + "</h1>";
			retour += "<form action='affichageBOutilisateurs' method='post' name='formBOutilisateurs'>";
			retour+= "<input type='hidden' name='utilId' value='" + util.getIdUtil() + "'/>";
			retour+= "<input type='hidden' name='modifie' value='1'/>";
			retour += "<div class='sousBoite' id='uneBoite'>";
			retour += "<h2>Droits</h2>";

			// Type de license (flottante ou fixe)
			retour += "<label>Session fixe</label><input type='checkbox' name='util_super' id='util_super' value='1' class='znCheckBox' ";

			if(util.isUnSuperUtilisateur()) 
				retour += "checked ";
			if(enModif)
				retour += "/><a href='#' class='";
			else
				retour += "/><div class='";
			//look checkbox si param�tre coch�
			if(util.isUnSuperUtilisateur()) 
				retour += "decorCheckboxB";
			else
				retour += "decorCheckbox";
			if(enModif)
				retour+="' onClick=\"switchCheckbox(this,'util_super');\"></a><br/>";
			else
				retour+="' ></div><br/>";

			// Droit d'acc�s
			retour += "<label>Droits d'acc&egrave;s</label><select name='util_acces' class='znSelect' ";
			if(!enModif)
				retour += "disabled>";
			else
				retour+=">";
			if(util.getNiveauAcces() < utilisateur.getNiveauAcces() || utilisateur.getNiveauAcces()<= MarbreEnvironnement.ACCES_RI)
			{
				retour += "<option value='" + MarbreEnvironnement.ACCES_RI + "'";
				if(util.getNiveauAcces()== MarbreEnvironnement.ACCES_RI)
					retour+= " selected ";
				retour+= ">R&eacute;solution informatique</option>";
			}
			if(util.getNiveauAcces() < utilisateur.getNiveauAcces() || utilisateur.getNiveauAcces()<= MarbreEnvironnement.ACCES_ADMIN)
			{
				retour += "<option value='" + MarbreEnvironnement.ACCES_ADMIN + "'";
				if(util.getNiveauAcces()== MarbreEnvironnement.ACCES_ADMIN)
					retour+= " selected ";
				retour+= ">Administrateur</option>";
			}
			//utilisateur classique
			retour += "<option value='" + MarbreEnvironnement.ACCES_UTIL + "'";
			if(util.getNiveauAcces()== MarbreEnvironnement.ACCES_UTIL)
				retour+= " selected ";
			retour+=">Utilisateur</option>";
			//utilisateur interdit
			retour += "<option value='" + MarbreEnvironnement.ACCES_NON + "'";
			if(util.getNiveauAcces()== MarbreEnvironnement.ACCES_NON)
				retour+= " selected ";
			retour+=">Non autoris&eacute;</option>";

			retour += "</select><br/>";

			// Visiblit� du repr�sentant
			String valueRepr = "";
			if(util.getCodeRepresentant()!=null && !util.getCodeRepresentant().trim().equals(""))
				valueRepr = util.getCodeRepresentant();
			retour += "<label>Visibilit� repr�sentant</label><input type='text' name='util_repres' id='util_repres' value='" + valueRepr +"' class='entree' maxlength='2'";
			if(!enModif)
				retour += " disabled />";

			// La biblioth�que
			retour += "<br><label>Biblioth&egrave;que</label><input type='text' name='util_bib' id='util_bib' value='" + util.getBibli() +"' class='entree' maxlength='10' disabled />";
			retour+= "</div>";
			retour += "</form>";
			//Ne laisser la possibilit� de modifier qu'� un utilisateur qui a de meilleurs droits que le profil � modifier
			if(util.getNiveauAcces() >= utilisateur.getNiveauAcces() && util.getIdUtil() != 1)
			{
				retour += "<div id='navigationFiche'>";
				if(enModif)
					retour += "<a href='affichageBOutilisateurs?utilId=" + util.getIdUtil() + "' id='ficheAnnuler'></a><a href='#' id='ficheValider' onClick=\"soumettreUnFormulaire('formBOutilisateurs')\"></a>";
				else
					retour += "<a href='affichageBOutilisateurs?utilId=" + util.getIdUtil() + "&modeModif=1' id='ficheModifier'></a>";
					
				retour += "</div>";
			}

			retour+= "</section>";

			return retour;
		} 
		catch (Exception e) {e.printStackTrace(); return null;}
	}
	
	/**
	 * Afficher la liste des utilisateurs du serveur de la mobilit�
	 * */
	private String afficherLesUtilisateurs(HttpServletRequest request,ServletOutputStream out,Utilisateur utilisateur) 
	{
		ArrayList<HttpSession> sessions = ((ArrayList<HttpSession>)getServletContext().getAttribute("sessions"));
		if(getServletContext().getAttribute("sessions")!=null)
			sessions = ((ArrayList<HttpSession>)getServletContext().getAttribute("sessions"));
		
		ArrayList<Utilisateur> liste = utilisateur.getGestionBOutilisateur().recupererLesUtilisateurs();
		String retour = "";
		try 
		{
			retour+="<section class='secContenu' id='gestionParametres'>";
				retour += "<h1><span class='titreH1'>Utilisateurs: " + sessions.size();
				if(sessions.size() > 1)
					 retour += " sessions actives</h1>";
				else retour += " session active</h1>";
			
			if(liste == null || liste.size()==0)
				retour+= "Pas d'utilisateur disponible";
			else
			{
				for(int i = 0; i < liste.size(); i++)
				{
					if(i%2==0)
						retour+= "<div class='listesClassiques'>";
					else 
						retour+= "<div class='listesClassiquesF'>";
					
					retour+= "<a class='labelParametre' href=\"affichageBOutilisateurs?utilId=" + liste.get(i).getIdUtil() + "\"><span class='lblPara'>" + liste.get(i).getLogin() + "</span>";
					if(testerPresenceUtilisateur(liste.get(i), sessions))
						retour+= "<span class='utilConnecte'>Connect�</span>";
					retour+= "</a>";
					retour+= "</div>";
				}
			}
			
			retour+="</section>";
			
			return retour;
			
		} catch (Exception e) {e.printStackTrace(); return null;}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		traiterPOSTouGET(request,response);
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		traiterPOSTouGET(request,response); 
	}

	private boolean testerPresenceUtilisateur(Utilisateur utilisateur, ArrayList<HttpSession> sessions)
	{
		boolean isPresent = false;
		if(sessions==null) return false;
		
		int i = 0;
		while( i < sessions.size() && !isPresent)
		{
			if(((Utilisateur)sessions.get(i).getAttribute("utilisateur")).getIdUtil()== utilisateur.getIdUtil())
				isPresent = true;
			i++;
		}
				
		return isPresent;
	}
	
}
