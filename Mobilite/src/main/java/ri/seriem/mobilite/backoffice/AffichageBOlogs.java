package ri.seriem.mobilite.backoffice;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.affichage.PatternErgonomie;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;
import ri.seriem.mobilite.outils.Log;

/**
 * Servlet implementation class AffichageBOlogs
 */
public class AffichageBOlogs extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private int niveauAcces = MarbreEnvironnement.ACCES_ADMIN;
	private PatternErgonomie pattern = null;   
	private int metier = -3;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AffichageBOlogs() 
    {
        super();
        pattern = new PatternErgonomie();
    }
    
    /**
     * Traiter de mani�re g�n�rique le POST ou le GET
     * */
    private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
    {
    	try
		{
    		int niveau = 2;
    		String[] liensOptions = {"#","#","#"};
			String metaSpecifiques = "";
			String titreOptions = "Options";
			
    		//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				//On cr�� un handler temporaire pour la session
				Utilisateur utilisateur = (Utilisateur)request.getSession().getAttribute("utilisateur");
				if(utilisateur.getNiveauAcces()<= niveauAcces)
				{
					//ancrage dans la page
					String ancrage = request.getParameter("ancrage");
					
					if(utilisateur.getGestionBOlogs()==null)
						utilisateur.setGestionBOlogs(new GestionBOlogs(utilisateur));
					
					metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
					metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
					//s�lection de logs par date
					if(request.getParameter("idLog")!=null)
					{
						niveau = 3;
						metaSpecifiques += "<link href='css/detailLog.css' rel='stylesheet'/>";
						metaSpecifiques += "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"30\">";
					}
					//liste compl�te des logs
					else
					{
						
					}
					
					ServletOutputStream out = response.getOutputStream() ;
					out.println(pattern.afficherEnTete(metaSpecifiques,ancrage));
					out.println(pattern.afficherHeader(metier,niveau,null, utilisateur));
					out.println("<article id='contenu'>");
					
					//afficher la liste des logs
					if(niveau==2)
						out.println(afficherListeLogs(out, utilisateur));
					//ou le contenu d'un log
					else out.println(afficherUnLog(out, utilisateur, request.getParameter("idLog")));
					
					out.println("</article>");
					out.println(pattern.afficherFin(titreOptions,null));
				}
				else
				{
					request.getSession().invalidate();
					getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
				}
			}
			else 
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}	
		}
    	catch (Exception e) {e.printStackTrace();}
    }
    
    /**
     * Afficher les logs d'une date donn�e
     * */
	private String afficherUnLog(ServletOutputStream out, Utilisateur utilisateur, String idLog) 
	{
		String retour = "";
		
		if(idLog!=null && utilisateur!=null)
		{
			ArrayList<Log> listeLogs = utilisateur.getLogs().getUneDate(idLog);
			
			retour = "<section class='secContenu' id='gestionParametres'>";
				retour+="<h1><span class='titreH1'>Logs du " + traductionDateHumaine(idLog) + "</span></h1>";
			
			if(listeLogs!=null && listeLogs.size()>0)
			{
				for(int i = listeLogs.size()-1; i > -1; i--)
				{
					if(i%2==0)
					{
						if(listeLogs.get(i).getTypeLog().equals("E"))
							retour+= "<div class='listesClassiquesE'>";
						else
							retour+= "<div class='listesClassiques'>";
					}
					else 
					{
						if(listeLogs.get(i).getTypeLog().equals("E"))
							retour+= "<div class='listesClassiquesFE'>";
						else
							retour+= "<div class='listesClassiquesF'>";
					}
								retour+="<div class='dateLogs'> " + listeLogs.get(i).getDateLog() + "</div>";
								retour+="<div class='loginLogs'>" + listeLogs.get(i).getLoginLog() + "</div>";
								retour+="<div class='classeLogs'>" + listeLogs.get(i).getClasseLog() + "</div>";
								retour+="<div class='messageLogs'>" + listeLogs.get(i).getMessageLog() + "</div>";
							retour+= "</div>";
				}
			}
			else { retour += "<p>Pas de logs pour la date choisie</p>";}
		}else { retour += "<p>Erreur sur le fichier de logs</p>";}
		
		retour+= "</section>";
		
		return retour;
	}

	/**
	 * Afficher la liste compl�te des logs par date
	 * */
	private String afficherListeLogs(ServletOutputStream out, Utilisateur utilisateur) 
	{
		String retour = "";
		String[] fichiers = utilisateur.getLogs().getTousLogs();
		
		retour = "<section class='secContenu' id='gestionParametres'>";
		if(fichiers!=null)
		{
			int j=0;
			retour+="<h1><span class='titreH1'>Liste des logs</span></h1>";
			//for(int i = 0; i < fichiers.length; i++)
			for(int i=fichiers.length-1; i> -1; i--)
			{
				if(fichiers[i].endsWith(".xml"))
				{
					if(j%2==0)
						retour+= "<div class='listesClassiques'>";
					else 
						retour+= "<div class='listesClassiquesF'>";
				
							retour+= "<a class='labelParametre' href=\"affichageBOlogs?idLog=" + fichiers[i].substring(0,6) + "\"><span class='lblPara'>" + traductionDateHumaine(fichiers[i]) + "</span></a>";
						
					retour+= "</div>";
					j++;
				}
			}
			if(j==0) retour+= "<p>Pas de fichier logs dans ce dossier</p>";
		}
		else
		{
			retour+= "<p>Pas de fichier logs dans ce dossier</p>";
		}
		
		retour+= "</section>";
		
		return retour;
	}
	
	/**
	 * Traduire le nom du fichier en date humainement lisible
	 * */
	private String traductionDateHumaine(String date)
	{
		String retour = "";
		
		retour = date.substring(4,6) + "/" + date.substring(2, 4) + "/" + date.substring(0, 2);
		
		return retour;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request,response);
	}

	
}
