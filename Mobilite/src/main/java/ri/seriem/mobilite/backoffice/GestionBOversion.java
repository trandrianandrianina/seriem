package ri.seriem.mobilite.backoffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;
import ri.seriem.mobilite.outils.MonFTP;

public class GestionBOversion 
{
	private Utilisateur utilisateur = null;
	private int versionWAR = 0;
	private int versionBDD = 0;
	private MonFTP objetFTP = null;	
	private File fichierWAR = null;
	
	public GestionBOversion(Utilisateur utilisateur)
	{
		this.utilisateur = utilisateur;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	//Controler la version dans son ensemble : WAR et BDD
	protected int controlerVersionGLobale()
	{
		int pb = 0;
		//controler le WAR
		if(controlerVersionWAR()>0) pb ++;
		//controler la BDD
		else if(!controlerVersionBDD()) pb += 2;
		
		return pb;
	}
	
	//Va v�rifier la version du WAR sur le site de t�l�chargement
	protected int controlerVersionWAR()
	{
		objetFTP = new MonFTP();
		if(objetFTP.seConnecter(MarbreEnvironnement.SERVEUR_FTP, MarbreEnvironnement.CLIENT_FTP, MarbreEnvironnement.MP_FTP))
		{
			int versionDispo = 0;
			int versionMax = verifierVersionWAR();
			
			try 
			{
				if(objetFTP.getMaConnexionFTP().changeWorkingDirectory(MarbreEnvironnement.DOSSIER_VERSIONS_FTP))
					System.out.println("On change de repertoire");
				else
					System.out.println("ECHEC change de repertoire");
				
				String[] dossiers  = objetFTP.getMaConnexionFTP().listNames();
				
				for(int i=0; i<dossiers.length; i++)
				{
					System.out.println("Dossier: " + dossiers[i]);
				
					try
					{
						versionDispo =Integer.parseInt(dossiers[i]);
						if(versionDispo > versionMax)
						{
							versionMax = versionDispo;
							System.out.println("Trouve version plus r�cente:" + versionMax);
						}
					}
					catch (Exception e) {e.printStackTrace();}
				}
			} catch (IOException e) {e.printStackTrace(); return 0;}
			
			objetFTP.seDeconnecter();
			
			if(versionMax > versionWAR)
				return versionMax;
			else return 0;
			
		}
		else
			return 0;
	}
	
	//on contr�le si le WAR est d�j� t�l�charg� sur le serveur local
	/*public boolean controlerSiDejaTelechargee()
	{
		//fichierWAR = new File(MarbreEnvironnement.DOSSIER_MOBILITE + MarbreEnvironnement.DOSSIER_VERSION_AS + "SerieMobile.war");
		fichierWAR = new File(MarbreEnvironnement.DOSSIER_DEPLOIE + MarbreEnvironnement.DOSSIER_VERSION_AS + "SerieMobile.war");
		
		return (fichierWAR!=null && fichierWAR.isFile());
	} */
	
	//Va v�rifier la version du WAR sur le site de t�l�chargement
	public boolean controlerVersionBDD()
	{
		return(verifierVersionBDD()==verifierVersionWAR());
	}
	
	//Va v�rifier la version du WAR avant de l'attribuer � versionWAR
	public int verifierVersionWAR()
	{
		versionWAR = MarbreEnvironnement.VERSION_DEV;
		System.out.println("version WAR: " + versionWAR);
		return versionWAR;
	}
	
	//Va v�rifier la version du WAR avant de l'attribuer � versionWAR
	public int verifierVersionBDD()
	{
		versionBDD = utilisateur.getBaseSQLITE().recupererVersionBDD();
		System.out.println("version BDD: " + versionBDD);	
		return versionBDD;
	}

	/**
	 * T�l�charger la version du serveur de t�l�chargement vers le serveur local S�rieMobile
	 * */
	protected boolean telechargerVersion()
	{
		boolean okTransfertFTP = false;
		
		versionWAR = controlerVersionWAR();
		if(versionWAR != 0)
		{
				if(objetFTP==null) objetFTP = new MonFTP();
			
				if(objetFTP.seConnecter(MarbreEnvironnement.SERVEUR_FTP, MarbreEnvironnement.CLIENT_FTP, MarbreEnvironnement.MP_FTP))
				{
					try 
					{
						objetFTP.getMaConnexionFTP().enterLocalPassiveMode();
					
						if(!objetFTP.getMaConnexionFTP().setFileType(FTP.BINARY_FILE_TYPE))
							System.out.println("Erreur de FTP: BIN");
					
						if(!objetFTP.getMaConnexionFTP().doCommand("NAM", " 1"))
							System.out.println("Erreur de FTP: NAM 1");
						if(objetFTP.getMaConnexionFTP().changeWorkingDirectory(MarbreEnvironnement.DOSSIER_VERSIONS_FTP + "/" + versionWAR ))
						{
							if(objetFTP.downloadFile("SerieNmobilite.war", MarbreEnvironnement.DOSSIER_DEPLOIE + MarbreEnvironnement.DOSSIER_VERSION_AS  +  "SerieNmobilite.war"))
							{
								okTransfertFTP = true;
								System.out.println("download OK FTP");
							}
							else System.out.println("download moisi FTP");
						}
						else System.out.println("Changement de dossier FTP moisi:" + MarbreEnvironnement.DOSSIER_VERSIONS_FTP + "/" + versionWAR);
					}catch (IOException e) {e.printStackTrace();}
				
					objetFTP.seDeconnecter();
				}
				else System.out.println("Pas de connexion FTP");
		}			
		return okTransfertFTP;
	}

	
	/**
	 * En se basant sur le fichier de version, met � jour la constante VERSION_DEV si n�cessaire
	 * 
	 * */
	public boolean majConstanteVersion() 
	{
		boolean isOk =false;
		
			File fichier = new File(MarbreEnvironnement.DOSSIER_MOBILITE + MarbreEnvironnement.FICHIER_VERSION_PGS);
			
			if(fichier.isFile())
			{
				InputStreamReader flog	= null;
				LineNumberReader llog	= null;
				String chaine = "";
				String contenu		     = null;
				int resultat = 0;
				try
				{ 
					flog = new InputStreamReader(new FileInputStream(fichier) );
					llog = new LineNumberReader(flog);
					while ((contenu = llog.readLine()) != null) 
						chaine += contenu;
			      
					llog.close();
					flog.close();
					
					resultat = Integer.parseInt(chaine.trim());
					System.out.println("resultat Version WAR: " + resultat);
					
			     }catch (Exception e){System.err.println("erreur maj Constante : "+e.getMessage());}
				 
				MarbreEnvironnement.VERSION_DEV = resultat;
				System.out.println("La version du WAR est : " + MarbreEnvironnement.VERSION_DEV );
				isOk =  true;
			}
			
			return isOk ;
	}

	/**
	 * 
	 * */
	public boolean majBDD() 
	{
		boolean isOk = false;
		//1 Planquer la BDD originale
		if(planquerLaBDD())
		{
			//2 R�cup�rer tous les ordres du SQL de maj
			//3 Executer les ordres dans la BDD
			System.out.println("J'ai bien planqu� la BDD");
			if(envoyerLesOrdres(recupererLesOrdresSQL()))
			{
				majConstanteVersion();
				isOk = true;
			}
		}
		
		return isOk;
	}
	
	/**
	 * Envoyer les ordres � la base de donn�e SQLITE
	 * */
	private boolean envoyerLesOrdres(ArrayList<String> lesOrdresSQL) 
	{
		if(lesOrdresSQL == null) return false;
		boolean isOk = true;
		
		for(int i=0; i < lesOrdresSQL.size(); i++)
		{
			if(!utilisateur.getBaseSQLITE().envoyerRequeteModif(lesOrdresSQL.get(i)))
			{
				utilisateur.getLogs().setLog("ECHEC requete: " + lesOrdresSQL.get(i), "GestionBOversion", "E");
				isOk = false;
			}
			if(!isOk) break;
		}
		
		return isOk;
	}

	/**
	 * R�cup�rer dans les SQL s�lectionn�s la liste des ordres SQL � passer
	 * */
	private ArrayList<String> recupererLesOrdresSQL() 
	{
		System.out.println("Je suis AU DEBUT de recupererLesOrdresSQL");
		ArrayList<String> listeDossiers = recupererVersionsIntermediaires();
		if(listeDossiers==null || listeDossiers.size()==0)
			return null;
		
		System.out.println("Je suis dans recupererLesOrdresSQL");
		return retournerListeDesOrdres(listeDossiers);
	}

	/**
	 * Retourner dans les SQL s�lectionn�s la liste des ordres SQL � passer  : UN PEU REDONDANTE AVEC recupererLesOrdresSQL   TODO 
	 * */
	private ArrayList<String> retournerListeDesOrdres(ArrayList<String> listeDossiers) 
	{
		if(listeDossiers==null) return null;
		File fichierMAJ = null;
		ArrayList<String> listeOrdres = null ;
		InputStreamReader flog	= null;
		LineNumberReader llog	= null;
		String chaine = null;
		String contenu = null;
		
		for(int i =0; i < listeDossiers.size(); i++)
		{
			if(i==0)
				listeOrdres = new ArrayList<String>();
			
			fichierMAJ = new File(MarbreEnvironnement.DOSSIER_MOBILITE + MarbreEnvironnement.DOSSIER_VERSIONS_PGS + listeDossiers.get(i) + MarbreEnvironnement.SEPARATEUR + MarbreEnvironnement.FICHIER_MAJBDD);
			if(fichierMAJ.isFile())
			{
				chaine = "";
				try
				{ 
					flog = new InputStreamReader(new FileInputStream(fichierMAJ) );
					llog = new LineNumberReader(flog);
					while ((contenu = llog.readLine()) != null) 
						chaine += contenu;
					llog.close();
					flog.close();
					
					String[] tabChaine = chaine.split(";;;");
					for(int j = 0; j < tabChaine.length; j++)
					{
						System.out.println("ordre: " + tabChaine[j] + ";");
						listeOrdres.add(tabChaine[j] + ";");
					}
					
			     }catch (Exception e){System.err.println("Error : "+e.getMessage());}
			}
		}
		
		return listeOrdres;
	}

	/**
	 * R�cup�rer la liste des dossiers de versions interm�diaires entre la version actuelle et la derni�re version
	 * */
	private ArrayList<String> recupererVersionsIntermediaires() {
		File dossierVersions = new File(MarbreEnvironnement.DOSSIER_MOBILITE + MarbreEnvironnement.DOSSIER_VERSIONS_PGS);
		ArrayList<String> liste = null;
		
		if(dossierVersions!=null && dossierVersions.isDirectory())
		{
			//check de la version de BDD
			versionBDD = verifierVersionBDD();
			versionWAR = verifierVersionWAR();
			
			liste = new ArrayList<String>();
			
			File[] listeVersions = dossierVersions.listFiles();
			if(listeVersions!=null)
			{
				//scanner tous les dossiers de version
				for (int i = 0; i < listeVersions.length; i++)
				{
					if(listeVersions[i].isDirectory())
					{
						try
						{
							//r�cup�rer les versions qui sont disponibles entre l'ancienne version de BDD et la version souhait�e
							if(Integer.parseInt(listeVersions[i].getName())> versionBDD && Integer.parseInt(listeVersions[i].getName()) <=  versionWAR)
							{
								liste.add(listeVersions[i].getName());
								System.out.println(" -------Ajout d'un dossier de version interm�diaire: " + listeVersions[i].getName());
							}
						}catch(Exception e){}
					}
				}
			}
		}
		else 
		{ 
			System.out.println("probl�me dans le repertoire"); 
		}  
		
		return liste;
	}

	/**
	 * Permet de sauvegarder la BDD actuelle dans un fichier bdd_mobilite<numeroVersion>
	 * */
	private boolean planquerLaBDD() 
	{
		File fichier = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE);
		boolean resultat=false;
		
		if(fichier.isFile())
		{
	        FileInputStream filesource=null;
	        FileOutputStream fileDestination=null;
	        try
	        {
	            filesource=new FileInputStream(fichier);
	            fileDestination=new FileOutputStream(MarbreEnvironnement.DOSSIER_SAUVE + MarbreEnvironnement.FICHIER_BDD + MarbreEnvironnement.VERSION_DEV);
	            byte buffer[]=new byte[512*1024];
	            int nblecture;
	            while((nblecture=filesource.read(buffer))!=-1)
	                fileDestination.write(buffer,0,nblecture);
	            
	            resultat=true;
	            
	        }catch(FileNotFoundException nf){nf.printStackTrace();}
	         catch(IOException io){io.printStackTrace();}
	        finally
	        {
	            try
	            {
	                filesource.close();
	            }
	            catch(Exception e){e.printStackTrace();}
	            try
	            {
	                fileDestination.close();
	            }catch(Exception e){e.printStackTrace();}
	        } 
		}
		
		return resultat;
	}
	
	/*protected boolean DezipperWar()
	{
		if(controlerSiDejaTelechargee())
		{
			try
			{
				Zip.uncompressjar(fichierWAR, new File(MarbreEnvironnement.DOSSIER_MOBILITE + MarbreEnvironnement.DOSSIER_VERSION_AS));
				
				return true;
			} catch (Exception e) {e.printStackTrace(); return false;}
        }
		else return false;
	}*/
	
	
	//+++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++
	
	
	public int getVersionWAR() {
		return versionWAR;
	}

	public void setVersionWAR(int versionWAR) {
		this.versionWAR = versionWAR;
	}

	public int getVersionBDD() {
		return versionBDD;
	}

	public void setVersionBDD(int versionBDD) {
		this.versionBDD = versionBDD;
	}

	public MonFTP getObjetFTP() {
		return objetFTP;
	}

	public void setObjetFTP(MonFTP objetFTP) {
		this.objetFTP = objetFTP;
	}

	public File getFichierWAR() {
		return fichierWAR;
	}

	public void setFichierWAR(File fichierWAR) {
		this.fichierWAR = fichierWAR;
	}

	

}
