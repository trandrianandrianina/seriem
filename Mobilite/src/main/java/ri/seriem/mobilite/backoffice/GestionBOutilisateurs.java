package ri.seriem.mobilite.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class GestionBOutilisateurs 
{
	private Utilisateur utilisateur = null;
	private Utilisateur utilisateurSelectionne = null;
	
	/**
	 * Constructeur par d�faut
	 * */
	public GestionBOutilisateurs(Utilisateur utilisateur)
	{
		this.utilisateur = utilisateur;
	}
	
	/**
	 * r�cup�rer la liste des utilisateurs
	 * */
	public ArrayList<Utilisateur> recupererLesUtilisateurs()
	{
		ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
		
		liste = utilisateur.getBaseSQLITE().recupererLesUtilisateurs();
		
		return liste;
	}
	
	
	/**
	 * Modifier les propri�t�s d'un utilisateur sur le serveur
	 * */
	public boolean modifierUnUtilisateur(HttpServletRequest request) 
	{
		//il faut effectuer les contr�les ici et trouver un maoyen de les retourner dans le message principal
		if(request!=null && request.getParameter("modifie")!=null)
		{
			return utilisateur.getBaseSQLITE().modifierUnUtilisateur(request);
		}
		else return false;
		
	}
	
	public Utilisateur recupererUnUtilisateur(int id) {
		utilisateurSelectionne = utilisateur.getBaseSQLITE().recupererUnUtilisateur(id);
		initUtilisateurSelectionne();
		return utilisateurSelectionne;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Utilisateur getUtilisateurSelectionne() {
		return utilisateurSelectionne;
	}

	public void setUtilisateurSelectionne(Utilisateur utilisateurSelectionne) {
		this.utilisateurSelectionne = utilisateurSelectionne;
	}

	/**
	 * Initialise les donn�es g�n�riques de l'utilisateur s�lection� avec celles de l'utiliasteur courant
	 */
	private void initUtilisateurSelectionne()
	{
		// On teste pour si on a pas d�j� fait cet init
		if (utilisateurSelectionne.getMaconnection() != null) return;
		
		utilisateurSelectionne.setMaconnection(utilisateur.getMaconnection());
		utilisateurSelectionne.setLogs(utilisateur.getLogs());
	}
}
