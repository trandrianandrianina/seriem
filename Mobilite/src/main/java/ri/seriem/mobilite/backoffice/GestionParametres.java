package ri.seriem.mobilite.backoffice;

import java.util.ArrayList;

import ri.seriem.mobilite.Environnement.Utilisateur;

public class GestionParametres 
{
	private Utilisateur utilisateur = null;
	private Parametre parametreEnCours = null;
	
	/**
	 * Constructeur de la gestion des param�tres
	 * */
	public GestionParametres(Utilisateur utilisateur)
	{
		this.utilisateur = utilisateur;
	}
	
	/**
	 * r�cup�rer les param�tres auquels acc�de l'utilisateur
	 * */
	public ArrayList<Parametre> recupererLesParametres()
	{
		ArrayList<Parametre> liste = new ArrayList<Parametre>();
		
		liste = utilisateur.getBaseSQLITE().recupererLesParametres(utilisateur);
		
		return liste;
	}

	public Parametre getParametreEnCours() {
		return parametreEnCours;
	}

	public void setParametreEnCours(Parametre parametreEnCours) {
		this.parametreEnCours = parametreEnCours;
	}

}
