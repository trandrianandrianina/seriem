package ri.seriem.mobilite.backoffice;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.affichage.PatternErgonomie;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;

/**
 * Servlet implementation class AffichageBObdd
 */
public class AffichageBObdd extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private int niveauAcces = MarbreEnvironnement.ACCES_RI;
	private PatternErgonomie pattern = null;   
	private int metier = -4;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AffichageBObdd() 
    {
    	super();
        pattern = new PatternErgonomie();
    }
    
    private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
    	try
		{
    		int niveau = 2;
    		String metaSpecifiques = "";
			String titreOptions = "Options";
			
			//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				//On cr�� un handler temporaire pour la session
				Utilisateur utilisateur = (Utilisateur)request.getSession().getAttribute("utilisateur");
				if(utilisateur.getNiveauAcces()<= niveauAcces)
				{
					//ancrage dans la page
					String ancrage = request.getParameter("ancrage");
					String requeteBDD = request.getParameter("requeteBDD");
					
					if(utilisateur.getGestionBObdd()==null)
						utilisateur.setGestionBObdd(new GestionBObdd(utilisateur));
					
					metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
					
					ServletOutputStream out = response.getOutputStream() ;
					out.println(pattern.afficherEnTete(metaSpecifiques,ancrage));
					out.println(pattern.afficherHeader(metier,niveau,null, utilisateur));
					out.println("<article id='contenu'>");
					
					if(requeteBDD!=null)
						out.println(afficherRetourRequete(utilisateur,requeteBDD));
					else	
						out.println(afficherFormulaire(null));
					
					out.println("</article>");	
					out.println(pattern.afficherFin(titreOptions,null));
				}
				else
				{
					request.getSession().invalidate();
					getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
				}
			}
			else 
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}
		}
    	catch(Exception e){e.printStackTrace();}
	}

	private String afficherFormulaire(String requete) 
	{
		if(requete==null)
			requete = "";
		
		String chaine = "<section class='secContenu' id='blocFormulaireBDD'>";
		chaine+="<h1><span class='titreH1'>Saisie de requ�te SQLITE</span></h1>";
		chaine += "<form action='AffichageBObdd' method='POST' name='formRequeteBDD' id='formRequeteBDD'>";
			chaine += "<textarea name='requeteBDD' id='requeteBDD'>" + requete + "</textarea></br>";
			chaine += "<input type='submit' value='Envoyer' id='EnvoyerSQLITE'/>";
		chaine += "</form>";
		chaine += "</section>";
		
		return chaine;
	}
	
	private String afficherRetourRequete(Utilisateur utilisateur, String requete)
	{
		String chaine = "";
		
		chaine += afficherFormulaire(requete);
		
		chaine += "<section class='secContenu'>";
		chaine+="<h1><span class='titreH1'>R�sultat de la requ�te</span></h1>";
		
		
		ArrayList<ArrayList<String>> listeResultat = null;
		listeResultat = utilisateur.getGestionBObdd().envoyerRequeteSQLITE(requete);
		
		if(listeResultat!=null)
		{
			if(listeResultat.size()>0)
			{
				if(listeResultat.size()==1 && listeResultat.get(0).get(0).equals(MarbreEnvironnement.SQLITE_ISOK))
					chaine += "<p id='retourSQLITE'>Succ�s de la requ�te</p>";
				else
				{
					chaine+= "<table id='tabSQLITE'>";
					int j =0;
					for(int i=0; i < listeResultat.size(); i++)
					{
						if(i==0)
						{
							chaine+= "<tr class='lignesSQLITE_th'>";
							
							for(j=0; j < listeResultat.get(i).size(); j++)
								chaine += "<th>" + listeResultat.get(i).get(j) + "</th>";	
							
							chaine+= "</tr>";
						}
						else
						{
							if(i%2==0)
								chaine+= "<tr class='lignesSQLITE_pyg'>";
							else chaine += "<tr class='lignesSQLITE'>";
							for(j=0; j < listeResultat.get(i).size(); j++)
								chaine += "<td class='cellSQLITE'>" + listeResultat.get(i).get(j) + "</td>";
							chaine+= "</tr>";
						}
					}
					chaine += "</table>";
				}
			}
			else
				chaine += "<p id='retourSQLITE'>Pas de r�sultat pour cette requ�te</p>";
		}
		else
			chaine += "<p id='retourSQLITE'>Probl�me avec la requ�te</p>";
		
		chaine += "</section>";
		
		
		return chaine;
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		traiterPOSTouGET(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request,response);
	}

}
