/*
 * Gestion de la popup Option qui permet de g�n�rer des PDF, mailer et ajouter dans les favoris 
 */
package ri.seriem.mobilite.affichage;

public class Options 
{
	
	protected String retournerFenetreOptions(String titre, String[] liens)
	{
		if(liens==null) return null;
		String chaine = "";
		
		chaine = "<div id='filtreTotal'></div>";
		
		chaine += "<div id='fenOptions'>";
			chaine += "<h2>" + titre + "<a id='fermerOptions' href='#' onClick=\"switchOptionsListe();\"><img src='images/optionsListeRetour.png'/></a></h2>";
			if(liens[0]!=null)
				chaine += "<a class='possibilitesListe' id='optionPDF' href='" + liens[0] + "'><img src='images/optionsPDF.png'/></a>";
			if(liens[1]!=null)
				chaine += "<a class='possibilitesListe' id='optionMail' href='" + liens[1] + "'><img src='images/optionsMail.png'/></a>";
			if(liens[2]!=null)
				chaine += "<a class='possibilitesListe' id='optionFavori' href='" + liens[2] + "'><img src='images/optionsFavoris.png'/></a>";
		chaine += "</div>";
			
		return chaine;
	}
}
