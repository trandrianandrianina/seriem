
package ri.seriem.mobilite.affichage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.system.SystemManager;

import ri.seriem.mobilite.Environnement.Connexion_SQLite;
import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;
import ri.seriem.mobilite.outils.Logs;

/**
 * Affichage et gestion de la connexion Utilisateur
 */
public class AffichageConnexion extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private boolean okPourIntro = false;
  private Connexion_SQLite sqlite = null;
  private String patternAdresseIp =
      "\\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b";
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageConnexion() {
    super();
    // A param�trer pour supprimer l'intro dans un cookies
    okPourIntro = true;
  }
  
  /**
   * @see Servlet#init(ServletConfig)
   */
  public void init(ServletConfig config) {
    try {
      super.init(config);
      initWithContextFile();
      checkerIpAs400();
      File fichierBDD = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE);
      
      if (fichierBDD.isFile()) {
        sqlite = new Connexion_SQLite(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE, null);
        if (sqlite != null) {
          try {
            MarbreEnvironnement.NB_SESSIONS_FLOT = Integer.valueOf(sqlite.recupererConstante("SESSIONS_FLOT"));
          }
          catch (Exception e) {
            MarbreEnvironnement.NB_SESSIONS_FLOT = 0;
          }
          try {
            MarbreEnvironnement.NB_SESSIONS_FIX = Integer.valueOf(sqlite.recupererConstante("SESSIONS_FIX"));
          }
          catch (Exception e) {
            MarbreEnvironnement.NB_SESSIONS_FIX = 1;
          }
          
          sqlite.stopConnection();
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    traiterPOSTouGET(request, response);
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    traiterPOSTouGET(request, response);
  }
  
  /**
   * Traite de mani�re g�n�rique le GET et le POST de la servlet
   */
  @SuppressWarnings("unchecked")
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      response.reset();
      ServletOutputStream out = response.getOutputStream();
      // String message = "";
      // r�ception des param�tres
      String param = request.getParameter("connexion");
      String echec = request.getParameter("echec");
      String loginCo = request.getParameter("loginCo");
      String mpCo = request.getParameter("mpCo");
      String ipmachine = request.getParameter("ipmachine");
      String arborescence = request.getParameter("arborescence");
      String profilAdmin = request.getParameter("profilAdmin");
      
      if (echec != null && echec.equals("1")) {
        // message = "Merci de vous identifier au pr&eacutealable";
        param = null;
      }
      
      if (MarbreEnvironnement.AS_UTILISE == null || MarbreEnvironnement.AS_UTILISE.trim().equals("")) {
        if (ipmachine != null) {
          if (traiterIpMachine(ipmachine, profilAdmin))
            gererReaffichageConnexion(request.getSession(), out, okPourIntro, "Connexion utilisateur 1", false, false, false);
          else
            afficherSaisieMachine(out, "Erreur de saisie IP + profil");
        }
        else if (arborescence != null) {
          if (majArborescence())
            afficherSaisieMachine(out, "IP de la machine");
          else
            afficherMAJarborescence(out, "Probleme lors de la construction de l'arborescence");
        }
        else {
          if (checkerIpAs400())
            gererReaffichageConnexion(request.getSession(), out, okPourIntro, "Connexion utilisateur 2", false, false, false);
          else
            afficherMAJarborescence(out, "Cr�er l'arborescence");
        }
      }
      else if (param != null && param.equals("go")) {
        Utilisateur utilisateur = null;
        // si on est pas dans un cas d'�crasement de session doublon
        if (request.getParameter("ecraser") == null)
          utilisateur = gererLaSession(loginCo, mpCo);
        else
          utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        
        // si l'utilisateur est valide l'attribuer � la session
        if (utilisateur != null) {
          // cr�er la liste de sessions s'il existe pas dans le contexte
          if (getServletContext().getAttribute("sessions") == null)
            getServletContext().setAttribute("sessions", new ArrayList<HttpSession>());
          
          // on r�cup�re les sessions d�j� actives
          ArrayList<HttpSession> sessions = ((ArrayList<HttpSession>) getServletContext().getAttribute("sessions"));
          
          // tester si on ne d�passe pas la limite de sessions OU si l'utilisateur est un utilisateur � session fixe
          if ((sessions != null && sessions.size() <= MarbreEnvironnement.NB_SESSIONS_FLOT && MarbreEnvironnement.NB_SESSIONS_FLOT > 0)
              || (sessions != null && utilisateur.isUnSuperUtilisateur())) {
            // gestion des cookies
            nettoyageCookies(request, response);
            
            // r�cup�rer la session en cours et y attribuer tous les attributs dont l'utilisateur
            HttpSession sessionPrincipale = request.getSession();
            
            // on va contr�ler si l'utilisateur n'est pas d�j� connect� sous une autre session ou la m�me (onglets navigateurs)
            boolean sessionIdentique = false;
            boolean memeUtilisateur = false;
            if (request.getParameter("ecraser") == null) {
              for (int i = 0; i < sessions.size(); i++) {
                if (((Utilisateur) sessions.get(i).getAttribute("utilisateur")).getLogin().equals(utilisateur.getLogin())) {
                  memeUtilisateur = true;
                  if (sessions.get(i).getId().equals(sessionPrincipale.getId()))
                    sessionIdentique = true;
                }
                else {
                  if (sessions.get(i).getId().equals(sessionPrincipale.getId()))
                    sessionIdentique = true;
                }
              }
            }
            // si il l'est et qu'il choisit d'�craser la session
            else {
              for (int i = 0; i < sessions.size(); i++) {
                if (((Utilisateur) sessions.get(i).getAttribute("utilisateur")).getLogin().equals(utilisateur.getLogin())) {
                  sessions.get(i).invalidate();
                  break;
                }
              }
            }
            
            // fixer la deconnexion automatique de la session et passer l'utilisateur
            sessionPrincipale.setMaxInactiveInterval(MarbreEnvironnement.DECO_AUTO);
            sessionPrincipale.setAttribute("utilisateur", utilisateur);
            
            if (!sessionIdentique && memeUtilisateur)
              gererReaffichageConnexion(request.getSession(), out, false, "Utilisateur d&eacute;j&agrave; connect&eacute", true, false,
                  false);
            else {
              if (sessionIdentique && !memeUtilisateur)
                gererReaffichageConnexion(request.getSession(), out, false, "Session d&eacute;j&agrave; utilis&eacute;e", false, false,
                    false);
              else {
                // Ne rajouter l'utilisateur que si c'est un nouvel utilisateur
                if (!memeUtilisateur)
                  sessions.add(sessionPrincipale);
                
                utilisateur.getLogs().setLog("D&eacute;but de session " + sessionPrincipale.getId(), "AffConnexion", "M");
                // basculer vers la page d'accueil
                getServletContext().getRequestDispatcher("/accueil?menu=G").forward(request, response);
              }
            }
          }
          // on a d�pass� le nombre max de sessions
          else
            gererReaffichageConnexion(request.getSession(), out, false, "Nombre maximum de sessions atteint", false, true, false);
        }
        // Erreur d'authentification (SI JAMAIS ON s'est JAMAIS CONNECTE mettre un truc dans sqlite qui d�tecte que c'est l'adresse IP de
        // la machine qui est pourrie et pas les logins -> retourner l'interface de saisie IP)
        else
          gererReaffichageConnexion(request.getSession(), out, false, "Authentification incorrecte", false, true, true);
      }
      // deconnexion volontaire
      else if (param != null && param.equals("stop"))
        gererReaffichageConnexion(request.getSession(), out, false, "Utilisateur d&eacute;connect&eacute", false, true, false);
      // affichage classique page � vide
      else
        gererReaffichageConnexion(request.getSession(), out, okPourIntro, "Utilisateur d&eacute;connect&eacute", false, false, false);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher l'interface de mise � jour de l'arborescence
   */
  private void afficherMAJarborescence(ServletOutputStream out, String message) {
    try {
      out.println("<div id='ajust'></div>");
      out.println("<article id='intro'>");
      out.println("<p>S&eacute;rie N <span>mobilit&eacute;</span></p>");
      out.println("</article>");
      out.println("<article id='arborescence'>");
      out.println("<p id='messageConnexion'>" + message + "</p>");
      out.println("<form action='connexion' method='post' name='formArborescence'>");
      out.println("<div id='champs'>");
      out.println("<input name ='arborescence' id='arborescence' type='hidden'>");
      out.println("<input id='validation' type='submit' value='valider'>");
      out.println("</div>");
      out.println("</form>");
      out.println("</article>");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
  }
  
  /**
   * G�rer la connexion avec le syst�me Serveur et l'authentification de l'utilisateur
   * @param log
   * @param passe
   * @return
   */
  private Utilisateur gererLaSession(String log, String passe) {
    Utilisateur utilisateur = null;
    // Tous les traitements qui permettent de se connecter � l'AS400 retourne OK si on est connect� au syst�me
    SystemManager systeme = new SystemManager(MarbreEnvironnement.AS_UTILISE, log, passe, true); // Mode normal
    // SystemManager systeme = new SystemManager(MarbreEnvironnement.AS_UTILISE, log, passe, true, false, DatabaseManager.DEB_VERBOSE +
    // DatabaseManager.DEB_NOCACHE); // Debug mode
    if (systeme != null && systeme.getSystem() != null) {
      utilisateur = new Utilisateur(log, systeme);
      // v�rifier que l'utilisateur est bien connect� � DB2 avant de valider l'utilisateur
      if (utilisateur.getMaconnection() == null) {
        utilisateur = null;
        Logs logs = new Logs(null);
        logs.setLog("Utilisateur NULL", "AffConnexion", "E");
      }
      
      if (utilisateur.getNiveauAcces() == MarbreEnvironnement.ACCES_NON) {
        utilisateur = null;
        Logs logs = new Logs(null);
        logs.setLog("Utilisateur NON AUTORISE", "AffConnexion", "E");
      }
      else
        utilisateur.setMdp(passe);
    }
    
    return utilisateur;
  }
  
  /**
   * Affiche le formulaire de connexion utilisateur
   * @param out
   * @param message
   * @throws IOException
   */
  private void afficherConnexion(ServletOutputStream out, String message, boolean isConf) {
    try {
      out.println("<div id='ajust'></div>");
      out.println("<article id='intro'>");
      out.println("<p><i id='suiteH1'>Suite</i>S&eacute;rie N <span>mobilit&eacute;</span></p>");
      out.println("</article>");
      out.println("<article id='connexion'>");
      out.println("<p id='messageConnexion'>" + message + "</p>");
      if (isConf) {
        out.println("<p id='messageConf'><span id='texteMessageConf'>Supprimer l'ancienne connexion ?</span>");
        out.println(
            "<a class='liensConf' id='validerConf' href='connexion?connexion=go&ecraser=1'><img src='images/ficheValider.png'/></a>");
        out.println("<a class='liensConf' id='annulerConf' href='connexion'><img src='images/ficheAnnuler.png'/></a>");
        out.println("</p>");
      }
      else {
        out.println("<form action='connexion' method='post' name='formConnexion'>");
        out.println("<div id='champs'>");
        out.println(
            "<input name ='loginCo' class='champ' id='login' type='text' placeholder='login' required autofocus autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false'><br/>");
        out.println(
            "<input name ='mpCo' class='champ' id='motPasse' type= 'password' placeholder='mot de passe' required autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false'>");
        out.println("<input name = 'connexion' type='hidden' value='go'>");
        out.println("<input id='validation' type='submit' value='&nbsp;'>");
        out.println("</div>");
        out.println("</form>");
      }
      out.println("</article>");
      out.println("<section id='mobile'><i id='suite'>Suite</i>S&eacute;rie N <span> mobilit&eacute;</span></section>");
      out.println("<p id='LibelleServeur'>" + MarbreEnvironnement.Libelle_Serveur + "</p>");
      out.println("<img id='logoRi' src='images/ri.png' alt='logo RI'>");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher les �lements propres � la fen�tre de connexion
   * @param out
   * @throws IOException
   */
  protected void afficherEnTete(ServletOutputStream out, String enTete, boolean intro) {
    try {
      out.println("<!DOCTYPE html>");
      out.println("<html lang='fr'>");
      out.println("<head>");
      out.println(gestionModeApp());
      out.println("<meta charset='utf-8'>");
      out.println(
          "<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi' />");
      // out.println("<meta name='viewport' content='width=device-width; user-scalable=no; minimum-scale = 1; maximum-scale = 1;' />");
      out.println("<meta name='ROBOTS' content='NONE'> ");
      out.println("<meta name='GOOGLEBOT' content='NOARCHIVE'>");
      /*web APP Apple*/
      out.println("<meta name='apple-mobile-web-app-capable' content='yes'>");
      out.println("<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />");
      /* ------    */
      out.println("<title>Connexion S&eacuterie N mobilit&eacute;</title>");
      out.println("<link href='css/connexion.css' rel='stylesheet'/>");
      if (intro)
        out.println("<script src='scripts/scriptsConnexion.js'></script>");
      else
        out.println("<link href='css/connexionRapide.css' rel='stylesheet'/>");
      out.println("<link rel=\"apple-touch-icon\" href=\"touch-icon-iphone.png\">");
      out.println("<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"touch-icon-ipad.png\">");
      out.println("<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"touch-icon-iphone-retina.png\">");
      out.println("<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"touch-icon-ipad-retina.png\">");
      out.println("<link rel=\"shortcut icon\" href=\"images/favicon.png\" />");
      out.println("</head>");
      out.println("<body>");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
  }
  
  /**
   * CLot�rer l'affichage de la fen�tre principale
   * @param out
   * @throws IOException
   */
  protected void afficherFin(ServletOutputStream out) {
    try {
      out.println("</body>");
      out.println("</html>");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * R�afficher la fen�tre de connexion avec les nouveaux param�tres
   */
  protected void gererReaffichageConnexion(HttpSession session, ServletOutputStream out, boolean intro, String message, boolean isConf,
      boolean invalider, boolean loguer) {
    // invalider la session si n�cessaire
    if (invalider)
      session.invalidate();
    // g�rer les logs
    if (loguer) {
      Logs logs = new Logs(null);
      logs.setLog(message, "AffConnexion", "M");
    }
    
    // affichage
    afficherEnTete(out, "Connexion S&eacuterie Mobile", intro);
    afficherConnexion(out, message, isConf);
    afficherFin(out);
  }
  
  /**
   * Afficher l'interface de saisie de l'adresse IP de l' AS400. Seul l'admin principal peut la saisir
   */
  protected void afficherSaisieMachine(ServletOutputStream out, String message) {
    try {
      out.println("<div id='ajust'></div>");
      out.println("<article id='intro'>");
      out.println("<p>S&eacute;rie N <span>mobilit&eacute;</span></p>");
      out.println("</article>");
      out.println("<article id='connexion'>");
      out.println("<p id='messageConnexion'>" + message + "</p>");
      out.println("<form action='connexion' method='post' name='formIpMachine'>");
      out.println("<div id='champs'>");
      out.println(
          "<input name ='ipmachine' class='champ' id='ipmachine' type='text' placeholder='adresse ip' required autofocus autocomplete='off' autocorrect='off' autocapitalize='off' pattern='"
              + patternAdresseIp + "' spellcheck='false'><br/>");
      out.println(
          "<input name ='profilAdmin' class='champ' id='login' type='text' placeholder='profil admin' required autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false'>");
      out.println("<input id='validation' type='submit' value='valider'>");
      out.println("</div>");
      out.println("</form>");
      out.println("</article>");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Traiter l'adresse IP saisie pour l'ins�rer dans la bdd
   */
  boolean traiterIpMachine(String ip, String profil) {
    if (sqlite == null)
      sqlite = new Connexion_SQLite(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE, null);
    
    if (sqlite.ajouterAdresseIpMachine(ip, profil))
      return checkerIpAs400();
    else
      return false;
  }
  
  /**
   * Checker l'adresse IP de l'AS400
   */
  private boolean checkerIpAs400() {
    boolean isOk = false;
    
    File fichierBDD = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE);
    
    if (fichierBDD.isFile()) {
      sqlite = new Connexion_SQLite(MarbreEnvironnement.DOSSIER_TRAVAIL + MarbreEnvironnement.BDD_SQLITE, null);
      if (sqlite != null) {
        MarbreEnvironnement.AS_UTILISE = sqlite.recupererConstante("IP_AS400");
      }
      
      if (MarbreEnvironnement.AS_UTILISE != null && !MarbreEnvironnement.AS_UTILISE.trim().equals("")) {
        sqlite.stopConnection();
        sqlite = null;
        
        isOk = true;
      }
    }
    
    return isOk;
  }
  
  /**
   * Mettre � jour l'arborescence de dossiers et de fichiers dans
   */
  private boolean majArborescence() {
    boolean isOk = false;
    
    File fichierBDD = null;
    File dossiersLogs = null;
    File dossiersDonnees = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + "bdd");
    File dossierSauves = null;
    // L'arborescence n'existait pas
    if (!dossiersDonnees.isDirectory()) {
      System.out.println("il faut cr�er: " + MarbreEnvironnement.DOSSIER_TRAVAIL + "bdd");
      if (dossiersDonnees.mkdirs()) {
        System.out.println("cr�ation: " + MarbreEnvironnement.DOSSIER_TRAVAIL + "bdd");
        isOk = copierFichierBDD(dossiersDonnees);
        dossiersLogs = new File(MarbreEnvironnement.DOSSIER_TRAVAIL + "logs");
        if (!dossiersLogs.isDirectory()) {
          System.out.println("il faut cr�er: " + dossiersLogs);
          isOk = dossiersLogs.mkdirs();
        }
      }
      else
        System.out.println("echec cr�ation: " + dossiersDonnees);
    }
    else {
      System.out.println(dossiersDonnees + " deja cree");
      fichierBDD = new File(dossiersDonnees + MarbreEnvironnement.FICHIER_BDD);
      if (!fichierBDD.isFile())
        isOk = copierFichierBDD(dossiersDonnees);
    }
    
    dossierSauves = new File(MarbreEnvironnement.DOSSIER_SAUVE);
    if (!dossierSauves.isDirectory())
      isOk = dossierSauves.mkdirs();
    
    return isOk;
  }
  
  /**
   * Copier le fichier de BDD initial
   */
  private boolean copierFichierBDD(File dossiersDonnees) {
    File fichier = new File(MarbreEnvironnement.DOSSIER_MOBILITE + MarbreEnvironnement.DOSSIER_VERSIONS_PGS + "1"
        + MarbreEnvironnement.SEPARATEUR + MarbreEnvironnement.FICHIER_BDD);
    boolean resultat = false;
    System.out.println("Fichier � copier:" + fichier.getAbsolutePath());
    
    if (fichier.isFile()) {
      FileInputStream filesource = null;
      FileOutputStream fileDestination = null;
      try {
        filesource = new FileInputStream(fichier);
        fileDestination = new FileOutputStream(dossiersDonnees + MarbreEnvironnement.SEPARATEUR + MarbreEnvironnement.FICHIER_BDD);
        byte buffer[] = new byte[512 * 1024];
        int nblecture;
        while ((nblecture = filesource.read(buffer)) != -1)
          fileDestination.write(buffer, 0, nblecture);
        
        resultat = true;
        
      }
      catch (FileNotFoundException nf) {
        nf.printStackTrace();
      }
      catch (IOException io) {
        io.printStackTrace();
      }
      finally {
        try {
          filesource.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        try {
          fileDestination.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    
    return resultat;
  }
  
  /**
   * r�cup�re les cookies du navigateur pour en r�cup�rer les valeurs correspondantes � notre page
   */
  private void nettoyageCookies(HttpServletRequest request, HttpServletResponse response) {
    // r�cup�ration des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    if (cookies == null)
      return;
    
    // trouver notre cookie et r�cup�rer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      // cookie �ph�mere des s�lections
      if (cookies[i].getName().equals("selectionClient") || cookies[i].getName().equals("selectionArticle")) {
        cookies[i].setValue(null);
        cookies[i].setMaxAge(0);
        cookies[i].setPath("/");
        response.addCookie(cookies[i]);
      }
    }
  }
  
  /**
   * Afficher un script en d�but de meta qui force le mode Web-App
   */
  private String gestionModeApp() {
    String retour = "";
    
    retour += "<script type=\"text/javascript\">";
    // liens internes
    retour += "(function(document,navigator,standalone) {";
    retour += "if ((standalone in navigator) && navigator [standalone] ) { ";
    retour += "var curnode, location=document.location, stop=/^(a|html)$/i;";
    retour += "document.addEventListener('click', function(e) {";
    retour += "curnode=e.target;";
    retour += "while (!(stop).test(curnode.nodeName)) {";
    retour += "curnode=curnode.parentNode;}";
    retour += "if('href' in curnode && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) ) {";
    retour += "e.preventDefault();";
    retour += "location.href = curnode.href;}";
    retour += "},false);}";
    retour += "})(document,window.navigator,'standalone');";
    
    retour += "</script>";
    
    return retour;
  }
  
  /**
   * Lecture des variables dans le fichier context.xml pour initialiser certaines variables d'environnement
   */
  private void initWithContextFile() {
    boolean refresh = false;
    
    String chaine = getServletContext().getInitParameter("IS_ECLIPSE");
    if (chaine != null) {
      MarbreEnvironnement.isEclipse = chaine.trim().equalsIgnoreCase("true");
      refresh = true;
    }
    
    chaine = getServletContext().getInitParameter("DOSSIER_RACINE_TOMCAT");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_RACINE_TOMCAT = chaine;
      refresh = true;
    }
    
    chaine = getServletContext().getInitParameter("DOSSIER_RACINE_MOBILITE");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_RACINE_MOBILITE = chaine;
      refresh = true;
    }
    
    chaine = getServletContext().getInitParameter("LIBELLE_SERVEUR");
    if (chaine != null)
      MarbreEnvironnement.Libelle_Serveur = chaine;
    else
      MarbreEnvironnement.Libelle_Serveur = "";
    
    if (refresh)
      MarbreEnvironnement.refreshValue();
    
    // System.out.println("-DOSSIER_MOBILITE-> " + MarbreEnvironnement.DOSSIER_MOBILITE);
  }
  
}
