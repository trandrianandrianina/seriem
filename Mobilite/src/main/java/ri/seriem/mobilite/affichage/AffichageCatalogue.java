package ri.seriem.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.metier.Catalogue;
import ri.seriem.mobilite.metier.GestionCatalogue;
import ri.seriem.mobilite.metier.Metier;
import ri.seriem.mobilite.metier.Vue;

/**
 * Servlet implementation class Catalogue
 */
public class AffichageCatalogue extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private PatternErgonomie pattern = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AffichageCatalogue() 
    {
        super();
        pattern = new PatternErgonomie();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	{
		traiterPOSTouGET(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	{
		traiterPOSTouGET(request, response);
	}

	/**
	 * M�thode g�n�rique qui traite le GET et le POST
	 * */
	private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				int niveau = 1;
				//String[] liensOptions = {"#","#","#"};
				String metaSpecifiques = "";
				String titreOptions = "Options";
				
				//On cr�� un handler temporaire pour la session
				Utilisateur utilisateur = (Utilisateur)request.getSession().getAttribute("utilisateur");
				if(utilisateur.getGestionCatalogue()==null)
					utilisateur.setGestionCatalogue(new GestionCatalogue(utilisateur));
				
				//mot cl� de recherche -> recherche stock�e
				String catalogue = request.getParameter("catalogue");
				String metier = request.getParameter("metier");
				String changement = request.getParameter("changement");
				//ancrage dans la page
				String ancrage = request.getParameter("ancrage");
				String page = request.getParameter("page");
				if(page!=null)
				{
					if(page.equals("metier"))
						metier = String.valueOf(utilisateur.getGestionCatalogue().getMetierEnCours().getIdMetier());
					else if(page.equals("catalogue"))
						catalogue = String.valueOf(utilisateur.getGestionCatalogue().getCatalogueEnCours().getIdCatalogue());
				}
				
				if(catalogue!=null)
					niveau = 2;
				else if(metier!=null)
					niveau = 3;
				
				// Si on est mode AJAX 
				if(changement!=null)
				{
					//Changer les donn�es en BDD et sur la liste de l'utilisateur
					utilisateur.getGestionCatalogue().controlerPresenceVue(changement);
					//afficher en LIVE 
					response.getWriter().write(pattern.afficherOptions(utilisateur,0,-1,0,true));
				}
				//si on est mode normal (pas AJAX)
				else if(changement==null)
				{
					ServletOutputStream out = response.getOutputStream() ;
					metaSpecifiques += "<script src='scripts/catalogue.js'></script>";
					metaSpecifiques += "<link href='css/catalogues.css' rel='stylesheet'/>";
					metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
					
					// +++++++++++++++ AFFICHAGE +++++++++++++++++++++++++++
					try 
					{
						out.println(pattern.afficherEnTete(metaSpecifiques,ancrage));
						out.println(pattern.afficherHeader(-1,niveau,null, utilisateur));
						out.println(pattern.afficherOptions(utilisateur,0,-1,0,false));
						out.println("<article id='contenu'>");
							out.println("<section class='secContenu' id='gestionCatalogue'>");
			
						if(catalogue!=null)
							out.println(afficherUnCatalogue(catalogue,out, utilisateur));
						else if (metier!=null)
							out.println(afficherUnMetier(utilisateur.getGestionCatalogue().getCatalogueEnCours(),metier,out, utilisateur));
						else out.println(afficherLesCatalogues(out, utilisateur));
					
							out.println("</section>");
						out.println("</article>");
					
						out.println(pattern.afficherFin(titreOptions,null));
					} 
					catch (IOException e) {e.printStackTrace();}
				}
			}
			else 
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}
		}
		catch (Exception e) {e.printStackTrace();}
	}
	
	/**
	 * Permet d'afficher les m�tiers disponibles dont les vues correspondent au catalogue choisi
	 * */
	private String afficherUnCatalogue(String catalogue, ServletOutputStream out, Utilisateur utilisateur)
	{
		ArrayList<Metier> liste = utilisateur.getGestionCatalogue().recupererLesMetiers(catalogue);
		String retour = "";
		
		try 
		{
			retour = "<h1><span class='titreH1'><span class='superflus'>Fiches du </span>" + utilisateur.getGestionCatalogue().getCatalogueEnCours().getLabelCatalogue().toUpperCase() + "</span></h1>";
			
			if(liste == null || liste.size()==0)
				retour+= "Pas de vues disponibles pour ce catalogue";
			else
			{
				for(int i = 0; i < liste.size(); i++)
				{
					if(i%2==0)
						retour+= "<div class='listesClassiques'>";
					else 
						retour+= "<div class='listesClassiquesF'>";
					
							retour+= "<a class='labelCatalogue' href='catalogue?metier=" + liste.get(i).getIdMetier() + "'><span class='lblCata'><span class='superflus'>Fiche </span>" + liste.get(i).getLabelMetier().toUpperCase() + "</span></a>";
							retour+= "<a class='iconeCatalogue' href='catalogue?metier=" + liste.get(i).getIdMetier() + "'><img src='" + liste.get(i).getIconeMetier() + "' alt='" + liste.get(i).getLabelMetier() + "'/></a>";
					
						retour+= "</div>";
				}
			}
			
			return retour;
			
		} 
		catch (Exception e) {e.printStackTrace(); utilisateur.getLogs().setLog("Erreur sur afficherUnCatalogue()", "AffCatalogue", "E"); return null;}
	}
	
	/**
	 * Permet d'afficher les vues disponibles pour le m�tier s�lectionn�
	 * */
	private String afficherUnMetier(Catalogue catalogue, String metier, ServletOutputStream out, Utilisateur utilisateur)
	{
		if(catalogue == null || metier == null) return null;
		
		ArrayList<Vue> liste = utilisateur.getGestionCatalogue().recupererLesVues(String.valueOf(catalogue.getIdCatalogue()),metier);
		String retour = "";
		try 
		{
			retour = "<h1><span class='titreH1'><span class='superflus'>Vues </span>" + utilisateur.getGestionCatalogue().getMetierEnCours().getLabelMetier().toUpperCase() + "</h1>";
			
			if(liste == null || liste.size()==0)
				retour+= "Pas de vue disponible";
			else
			{
				for(int i = 0; i < liste.size(); i++)
				{
					if(i%2==0)
						retour+= "<div class='listesClassiques'>";
					else 
						retour+= "<div class='listesClassiquesF'>";
					
							retour+= "<a class='labelVue' href='#'><span class='lblCata'>" + liste.get(i).getDenomination() + "</span></a>";
							retour+= "<a class='iconeVue' href='#'><img src='" + utilisateur.getGestionCatalogue().accederImageVersionCatalogue(liste.get(i).getImage()) + "' /></a>";
							retour+= "<a class='" + utilisateur.getGestionCatalogue().retournerClasseSelection(liste.get(i).getIdVue())+ "' href='#' onClick='controlerVues(" + liste.get(i).getIdVue() + ");' id='" + liste.get(i).getIdVue() + "'></a>";
					
						retour+= "</div>";
				}
			}
			
			return retour;
			
		} catch (Exception e) {e.printStackTrace(); utilisateur.getLogs().setLog("Erreur sur afficherUnMetier()", "AffCatalogue", "E"); return null;}
	}
	
	/**
	 * Permet d'afficher la liste des catalogues disponibles pour l'utilisateur
	 * */
	private String afficherLesCatalogues(ServletOutputStream out, Utilisateur utilisateur)
	{
		ArrayList<Catalogue> liste = utilisateur.getGestionCatalogue().recupererLesCatalogues();
		String retour = "";
		try 
		{
			
			retour = "<h1><span class='titreH1'>Catalogues</h1>";
			
			if(liste == null || liste.size()==0)
				retour+= "Pas de catalogue disponible";
			else
			{
				for(int i = 0; i < liste.size(); i++)
				{
					if(i%2==0)
						retour+= "<div class='listesClassiques'>";
					else 
						retour+= "<div class='listesClassiquesF'>";
					
							retour+= "<a class='labelCatalogue' href='catalogue?catalogue=" + liste.get(i).getIdCatalogue() + "'><span class='lblCata'>" + liste.get(i).getLabelCatalogue().toUpperCase() + "</span></a>";
							retour+= "<a class='iconeCatalogue' href='catalogue?catalogue=" + liste.get(i).getIdCatalogue() + "'><img src='" + liste.get(i).getIconeCatalogue() + "' alt='" + liste.get(i).getLabelCatalogue() + "'/></a>";
					
						retour+= "</div>";
				}
			}
			
			return retour;
			
		} catch (Exception e) {e.printStackTrace(); utilisateur.getLogs().setLog("Erreur sur afficherLesCatalogue()", "AffCatalogue", "E"); return null;}
	}
}

