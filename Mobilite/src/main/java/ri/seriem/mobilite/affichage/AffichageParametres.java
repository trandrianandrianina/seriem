package ri.seriem.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.backoffice.GestionParametres;
import ri.seriem.mobilite.backoffice.Parametre;

/**
 * Servlet implementation class AffichageParametres
 */
public class AffichageParametres extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private PatternErgonomie pattern = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AffichageParametres() {
        super();
        pattern = new PatternErgonomie();
    }
    
    /**
	 * M�thode g�n�rique qui traite le GET et le POST
	 * */
	private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			int niveau = 1;
			//String[] liensOptions = {"#","#","#"};
			String metaSpecifiques = "";
			String titreOptions = "Options";
			
			//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				//On cr�� un handler temporaire pour la session
				Utilisateur utilisateur = (Utilisateur)request.getSession().getAttribute("utilisateur");
				if(utilisateur.getGestionParametres()==null)
					utilisateur.setGestionParametres(new GestionParametres(utilisateur));
				
				String page = request.getParameter("page");
				
				if(page!=null)
				{
					
				}
				
				// charger le look correspondant
				metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
				metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
				
				ServletOutputStream out = response.getOutputStream() ;
				out.println(pattern.afficherEnTete(metaSpecifiques,null));
				out.println(pattern.afficherHeader(-2,niveau,null, utilisateur));
				out.println("<article id='contenu'>");
					out.println("<section class='secContenu' id='gestionParametres'>");
					
				out.println(afficherLesParametres(out, utilisateur));
					
					out.println("</section>");
				out.println("</article>");
				out.println(pattern.afficherFin(titreOptions,null));
			}
			else 
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}
		}
		catch (Exception e) {e.printStackTrace();}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response){
		traiterPOSTouGET(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request, response);
	}
	
	/**
	 * Afficher la liste des param�tres stock�s dans la BDD SQLITE
	 * */
	private String afficherLesParametres(ServletOutputStream out, Utilisateur utilisateur)
	{
		ArrayList<Parametre> liste = utilisateur.getGestionParametres().recupererLesParametres();
		String retour = "";
		try 
		{
			
			retour = "<h1><span class='titreH1'>Param�tres</h1>";
			
			if(liste == null || liste.size()==0)
				retour+= "Pas de param�tre disponible";
			else
			{
				for(int i = 0; i < liste.size(); i++)
				{
					if(i%2==0)
						retour+= "<div class='listesClassiques'>";
					else 
						retour+= "<div class='listesClassiquesF'>";
					
							retour+= "<a class='labelParametre' href='" + liste.get(i).getPara_lien() + "'><span class='lblPara'>" + liste.get(i).getPara_libelle() + "</span></a>";
							
						retour+= "</div>";
				}
			}
			
			return retour;
			
		} catch (Exception e) {e.printStackTrace(); utilisateur.getLogs().setLog("Erreur sur afficherLesParametres()", "AffParametres", "E"); return null;}
	}

}
