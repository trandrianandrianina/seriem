
package ri.seriem.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.metier.client.GestionClients;

/**
 * Servlet implementation class AffichageContact
 */
public class AffichageContact extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private String[] infosRecherche = { "clients", "formClients", "RECHERCHE CLIENT" };
  private PatternErgonomie pattern = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageContact() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Init de l'affichage client
   */
  @Override
  public void init(ServletConfig config) {
    try {
      super.init(config);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    traiterPOSTouGET(request, response);
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    traiterPOSTouGET(request, response);
  }
  
  /**
   * m�thode g�n�rique au traitements des param�tres pass�s dans le GET ou le POST
   * @param request
   * @param response
   */
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      // String[] liensOptions = {"#","mailto:","#"};
      String metaSpecifiques = null;
      String titreOptions = "Options";
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        ServletOutputStream out = response.getOutputStream();
        // On cr�� un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        // si ce n'est pas fait, acc�der � la gestion m�tier des clients
        if (utilisateur.getGestionClients() == null)
          utilisateur.setGestionClients(new GestionClients(utilisateur));
        
        // variables
        int vue = 0;
        
        Map<String, String[]> donnees = request.getParameterMap();
        
        for (Entry<String, String[]> entry : donnees.entrySet()) {
          System.out.println("CLE:" + entry.getKey() + " - " + entry.getValue()[0]);
        }
        // ++++++++++++++++++++++++++++++++++++ param�tres pass�s ++++++++++++++++++++++++++++++++++++++++
        
        // mot cl� de recherche -> recherche stock�e
        String recherche = request.getParameter("recherche");
        String special = request.getParameter("special");
        // Donn�es d'un �tablissement
        String etb = request.getParameter("CLETB");
        String idSoc = request.getParameter("CLCLI");
        String suff = request.getParameter("CLLIV");
        String cont = request.getParameter("numberContact");
        String devis = request.getParameter("XINUM");
        // gestion de pages
        String page = request.getParameter("page");
        String module = request.getParameter("module");
        // gestion des options de fiche ou de liste
        // String options = request.getParameter("options");
        // ancrage dans la page
        String ancrage = request.getParameter("ancrage");
        // mode modification
        // boolean isEnModif = request.getParameter("isEnModif")!=null && request.getParameter("isEnModif").equals("1");
        // nombre de lignes des listes
        int nbLignes = utilisateur.getNbLignesListes() + 1;
        // tri sur les listes
        String tri = request.getParameter("tri");
        // niveau m�tier pour le fil rouge (liste, fiche...)
        if (request.getParameter("niveau") != null)
          utilisateur.getGestionClients().setNiveauMetier(Integer.parseInt(request.getParameter("niveau")));
        // passer la vue demand�e � la gestion des clients
        if (request.getParameter("vue") != null) {
          try {
            if (vue != 0)
              tri = "MEMELIGNES";
            else
              tri = "VUE";
            vue = Integer.parseInt(request.getParameter("vue"));
          }
          catch (Exception e) {
            vue = 1;
            e.printStackTrace();
          }
          // attribuer la vue � la gestion clients
          utilisateur.getGestionClients().setVue(vue);
        }
        // gestion du nombre de lignes des listes
        if ((page != null && page.equals("retour")) || (tri != null && !tri.equals("PLUSLIGNES")) || request.getParameter("vue") != null)
          nbLignes = utilisateur.getGestionClients().getNbLignesMax();
        else if (request.getParameter("plusDeLignes") != null)
          nbLignes = Integer.parseInt(request.getParameter("plusDeLignes")) + utilisateur.getNbLignesListes();
        
        // ++++++++++++++++++++++++++++++++++++ Traitements ++++++++++++++++++++++++++++++++++++++++
        
        // gestion des cookies client
        utilisateur.getGestionClients().setListeSelection(gestionCookies(request));
        
        // sinon changer le niveau m�tier vers la liste
        utilisateur.getGestionClients().setNiveauMetier(1);
        idSoc = utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim();
        
        metaSpecifiques += "<link href='css/devisClients.css' rel='stylesheet'/>";
        
        out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
        
        out.println(pattern.afficherHeader(MarbreAffichage.METIER_CLIENTS, 1, infosRecherche, utilisateur));
        out.println(pattern.afficherOptions(utilisateur, MarbreAffichage.METIER_CLIENTS, utilisateur.getGestionClients().getTypeVue(),
            utilisateur.getGestionClients().getNiveauMetier(), false));
        
        out.println(afficherDevisPourUnContact(utilisateur, idSoc, cont));
        /*if (request.getParameter("XINUM")!=null)
        	//afficher le d�tail du devis
        	out.println(afficherDetailDevis(utilisateur,idSoc, cont,devis ));*/
        System.out.println("--------------client :" + idSoc);
        System.out.println("--------------contact : " + cont);
        System.out.println("--------------devis : " + devis);
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le d�tail du client s�lectionn�
   * @param out
   * @param etb
   * @param idSoc
   * @param suff
   */
  public void afficherUnClient(ServletOutputStream out, Utilisateur utilisateur, String etb, String idSoc, String suff, String page,
      String options, boolean isEnModif) {
    try {
      boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='ficheMetier'>");
      
      utilisateur.getGestionClients().recupererUnRecord(etb, idSoc, suff);
      // traiter les favoris
      isUnFavori = utilisateur.getGestionClients().isUnFavori(options != null && options.equals("favoris"));
      // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
      
      if (utilisateur.getGestionClients().getRecordActuel() == null)
        out.println("Le client n'a pas &eacute;t&eacute; trouv&eacute;");
      else {
        // r�cup�rer la vue demand�e
        switch (utilisateur.getGestionClients().getTypeVue()) {
          case 1:
            break;
          case 2:
            out.println(utilisateur.getGestionClients().getEncours().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 3:
            out.println(utilisateur.getGestionClients().getContacts().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 4:
            out.println(utilisateur.getGestionClients().getDevis().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 5:
            out.println(utilisateur.getGestionClients().getCommandes().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 6:
            out.println(utilisateur.getGestionClients().getBonsLivr().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 7:
            out.println(utilisateur.getGestionClients().getFactures().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 8:
            out.println(utilisateur.getGestionClients().getFacturesNR().afficherLeRecord(isUnFavori, isEnModif));
            break;
        }
        // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
        out.println(majTailleFiche("ficheMetier"));
        
      }
      out.println("</section>");
      
      out.println("</article>");
    }
    catch (Exception e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("Erreur sur afficherUnClient()", "AffClients", "E");
    }
  }
  
  /**
   * Afficher le d�tail du client s�lectionn�
   * @param out
   * @param etb
   * @param idSoc
   * @param suff
   */
  public void afficherDetailModule(ServletOutputStream out, Utilisateur utilisateur, String module, String etb, String idSoc, String suff,
      String page, String options, Map<String, String[]> mapDonnees) {
    try {
      int intModule = 0;
      
      if (module != null)
        intModule = Integer.parseInt(module);
      // out.println("<a id='boutonRetourModule' href='javascript:window.history.go(-1)'>Retour</a>");
      out.println("<a id='boutonRetourModule' href='javascript:history.back()'>Retour</a>");
      
      // boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      
      out.println("<section class='secContenu' id='moduleMetier'>");
      // PARAMETRER LE MODUUUUUULE PAS BOOOOOOO !!
      utilisateur.getGestionClients().gestionModule(intModule, mapDonnees);
      
      out.println(utilisateur.getGestionClients().getModuleEnCours().afficherLeRecord());
      
      out.println(majTailleFiche("moduleMetier"));
      
      out.println("</section>");
      
      out.println("</article>");
      
    }
    catch (Exception e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("Erreur sur afficherDetailModule()", "AffClients", "E");
    }
  }
  
  /**
   * afficher une liste de clients en fonction des param�tres souhait�s
   * @param out
   * @param clients
   * @throws IOException
   */
  /*private void afficherLesClients(ServletOutputStream out, Utilisateur utilisateur, String recherche, String tri, String special, String page, int nbLignes)
  {
  	try 
  	{
  		String apport = "";
  		out.println("<article id='contenu'>");
  			out.println("<section class='secContenu' id='listeMetier'>");
  	
  		//Si on tri la liste pr�c�dente
  		if(tri!=null)
  		{
  			if(recherche==null)
  				recherche = utilisateur.getGestionClients().getMotCle();
  			if(special==null) 
  				special = utilisateur.getGestionClients().getRechercheSpeciale();
  		}
  		//si on est pas en mode tri
  		else
  		{
  			//si on passe un mot de recherche -> p�ter la recherche sp�ciale stock�e
  			if(recherche!=null)	
  				special = null;
  		}
  		
  		//calculer une nouvelle liste si on ne retourne pas sur  l'ancienne
  		if(page==null || !page.equals("retour") || tri!=null)
  		{
  			//si on r�cup�re les m�me lignes
  			if(tri!= null && (tri.equals("MEMELIGNES")||tri.equals("VUE")) && utilisateur.getGestionClients().getListeRecords()!=null)
  			{
  				//System.out.println("Je passe DANS MEME LIGNES");
  				utilisateur.getGestionClients().setListeRecords(utilisateur.getGestionClients().recuperationDesMemeRecords(tri));
  			}
  			else utilisateur.getGestionClients().chercherDesRecords(recherche,tri,special,nbLignes);
  		}
  		else utilisateur.getGestionClients().setListeRecords(utilisateur.getGestionClients().recuperationDesMemeRecords(tri));
  		
  		//traiter le texte apport
  		if(utilisateur.getGestionClients().getRechercheSpeciale()!=null)
  		{
  			if(utilisateur.getGestionClients().getRechercheSpeciale().equals("fav")) apport = "Mes favoris";
  			if(utilisateur.getGestionClients().getRechercheSpeciale().equals("sel")) apport = "Ma s&eacute;lection";
  		}
  		
  		//affichage des clients par vue demand�e
  		switch(utilisateur.getGestionClients().getTypeVue())
  		{
  			case 1:  break;
  			case 2: out.println(utilisateur.getGestionClients().getEncours().afficherListeRecords(apport,nbLignes)); break;
  			case 3: out.println(utilisateur.getGestionClients().getContacts().afficherListeRecords(apport,nbLignes)); break;
  			case 4: out.println(utilisateur.getGestionClients().getDevis().afficherListeRecords(apport,nbLignes)); break;
  			case 5: out.println(utilisateur.getGestionClients().getCommandes().afficherListeRecords(apport,nbLignes)); break;
  			case 6: out.println(utilisateur.getGestionClients().getBonsLivr().afficherListeRecords(apport,nbLignes)); break;
  			case 7: out.println(utilisateur.getGestionClients().getFactures().afficherListeRecords(apport,nbLignes)); break;
  			case 8: out.println(utilisateur.getGestionClients().getFacturesNR().afficherListeRecords(apport,nbLignes)); break;
  		}	
  			out.println("</section>");
  		
  		out.println("</article>");
  	} 
  	catch (Exception e) {e.printStackTrace(); utilisateur.getLogs().setLog("Erreur sur afficherLesClients()", "AffClients", "E");}
  	
  }
  */
  
  /**
   * r�cup�re les cookies du navigateur pour en r�cup�rer les valeurs correspondantes � notre page
   */
  private String[] gestionCookies(HttpServletRequest request) {
    String[] idClients = null;
    // r�cup�ration des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    // trouver notre cookie et r�cup�rer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      if (cookies[i].getName().equals("selectionClient")) {
        String valeurs = cookies[i].getValue();
        if (valeurs != null && !valeurs.trim().equals(""))
          idClients = valeurs.split("_");
      }
    }
    
    return idClients;
  }
  
  private String majTailleFiche(String id) {
    String retour = "<script>";
    retour += "majTailleFiche(\"" + id + "\");";
    retour += "</script>";
    
    return retour;
  }
  
  /*	private void afficherClientParContact(ServletOutputStream out, Utilisateur utilisateur, String etb, String idSoc, String suff, String page, String options, boolean isEnModif, String numCont)
  	{
  		try
  		{
  		boolean isUnFavori = false;
  		out.println("<article id='contenu'>");
  			out.println("<section class='secContenu' id='ficheMetier'>");
  			utilisateur.getGestionClients().recupererUnRecord(etb, idSoc, suff);
  			//traiter les favoris
  			isUnFavori = utilisateur.getGestionClients().isUnFavori(options!=null && options.equals("favoris"));
  			//out.println(afficherListeContactParClient(utilisateur,idSoc, request));
  			
  			if(utilisateur.getGestionClients().getRecordActuel()== null)
  				out.println("Le client n'a pas &eacute;t&eacute; trouv&eacute;");
  			else
  			{
  			//r�cup�rer la vue devis
  				//if(utilisateur.getGestionClients().getTypeVue()==4)
  				//{
  				out.println(utilisateur.getGestionClients().getDevis().afficherLeRecord(isUnFavori,isEnModif));
  				//}
  			}
  		}
  		catch(Exception e)
  		{
  			e.printStackTrace();
  			utilisateur.getLogs().setLog("Erreur sur afficherUnClient()", "AffClients", "E");
  		}
  	}*/
  
  public String afficherDevisPourUnContact(Utilisateur utilisateur, String idSoc, String numContact) {
    
    String retour = "";
    try {
      retour += ("<article id='contenu'>");
      retour += ("<section class='secContenu' id='ficheMetier'>");
      ArrayList<GenericRecord> listeDevis = utilisateur.getGestionClients().getDevis().listeDevisPourUnContact(idSoc, numContact);
      
      String nomCli = (String) listeDevis.get(0).getField("RECL1");
      
      retour += "<div id='module' class='sousBoite'>";
      // retour += "<span class='grosTitre'> Les devis</span>";
      retour += "<a id='boutonRetourModule' href='javascript:history.back()'>Retour</a>";
      // retour += "<span class= 'titreH1'>";
      retour += "<span class='grosTitre'>Devis du contact : " + nomCli + "</span>";
      
      retour +=
          "<div class='enteteListeModule'>" + "<span id='titreNumero'>Num�ro </span>" + "<span id='titreRefLongue'>R�f. longue</span>"
              + "<span id='titreValidit�'>date de validit�</span>" + "<span id='titreMontant'>Montant HT en Euro</span></div>";// "<span
                                                                                                                               // id='titreRefLongue'>R�f.
                                                                                                                               // longue</span>"
                                                                                                                               // +
      
      for (int i = 0; i < listeDevis.size(); i++) {
        if (listeDevis != null && listeDevis.get(i).getField("XINUM") != null) {
          // Ent�te de liste de module
          
          retour += "<div class='listes'>";
          // le num�ro de devis
          retour += "<a class='titreNumero' href='clients?module=7&E1COD=D&E1ETB=" + listeDevis.get(i).getField("E1ETB") + "&E1NUM="
              + listeDevis.get(i).getField("XINUM") + "&E1SUF=" + listeDevis.get(i).getField("E1SUF") + "'>";//
          // clients?module=7&E1COD=D&E1ETB=002&E1NUM=116914&E1SUF=0
          
          if (listeDevis.get(i).isPresentField("XINUM"))
            retour += listeDevis.get(i).getField("XINUM").toString().trim();
          else
            retour += ("aucune");
          retour += ("</a>");
          
          // la ref longue
          retour += "<a class='titreRefLongue' href='clients?module=7&E1COD=D&E1ETB=" + listeDevis.get(i).getField("E1ETB") + "&E1NUM="
              + listeDevis.get(i).getField("XINUM") + "&E1SUF=" + listeDevis.get(i).getField("E1SUF") + "'>";
          if (listeDevis.get(i).isPresentField("E1RCC"))
            retour += listeDevis.get(i).getField("E1RCC").toString().trim();
          else
            retour += ("aucune");
          retour += ("</a>");
          
          // la validit� du devis
          retour += "<a class='titreValidit� 'href='clients?module=7&E1COD=D&E1ETB=" + listeDevis.get(i).getField("E1ETB") + "&E1NUM="
              + listeDevis.get(i).getField("XINUM") + "&E1SUF=" + listeDevis.get(i).getField("E1SUF") + "'>";
          if (listeDevis.get(i).isPresentField("E1HOM"))
            retour += utilisateur.getOutils().TransformerEnDateHumaine(listeDevis.get(i).getField("E1HOM").toString().trim());
          else
            retour += ("aucune");
          retour += ("</a>");
          
          // le montant ht par ligne
          retour += "<a class='titreMontant' href='#'>";
          if (listeDevis.get(i).isPresentField("E1THTL"))
            retour += listeDevis.get(i).getField("E1THTL").toString().trim();
          else
            retour += ("aucune");
          retour += ("</a>");
          
        }
        else
          
          retour += "<p>Aucun devis pour ce contact</p>";
        // afficher les devis de tout le monde
        retour += "</div>";
      }
      
      retour += ("</section>");
      
      retour += ("</article>");
    }
    catch (Exception e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
      utilisateur.getLogs().setLog("Erreur sur afficherUnClient()", "AffClients", "E");
    }
    return retour;
  }
  
  public String afficherDetailDevis(Utilisateur utilisateur, String idSoc, String numContact, String devis) {
    System.out.println("je passe dans ma methode");
    String retour = "";
    try {
      retour += ("<article id='contenu'>");
      // retour+=("<section class='secContenu' id='ficheMetier'>");
      ArrayList<GenericRecord> listeDetailDevis = utilisateur.getGestionClients().getDevis().detailDuDevis(idSoc, numContact, devis);
      
      for (int i = 0; i < listeDetailDevis.size(); i++) {
        retour += "<label class='labelNumero' >Num�ro/suffixe</label>";
        retour += "<input type='text' name=nbrDevis value='" + listeDetailDevis.get(i).getField("XINUM") + "'/>";
        
      }
      retour += ("</article>");
      
    }
    catch (Exception e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
      utilisateur.getLogs().setLog("Erreur sur afficherUnClient()", "AffClients", "E");
    }
    return retour;
  }
}
