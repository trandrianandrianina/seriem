package ri.seriem.mobilite.affichage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;

/**
 * Servlet implementation class mobilitePluch
 */
public class mobilitePluch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public mobilitePluch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		traiterPOSTouGET(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			//verification de session
			if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				/*URL url = new URL("http://88.188.171.20:8080/WebShop/accueil");
				HttpURLConnection connection = (HttpURLConnection)url.openConnection();
				
				//connection.setDoOutput( true);
				
				connection.setRequestMethod("POST");
				
				
				/*OutputStreamWriter outputWriter = new OutputStreamWriter(connection.getOutputStream());
				outputWriter.write("pouet");
				outputWriter.flush();
				outputWriter.close();*/
				
				//ServletOutputStream out = response.getOutputStream() ;
				//getServletContext().getRequestDispatcher("../WebShop/accueil").forward(request, response);
				//response.encodeRedirectURL("http://88.188.171.20:8080/WebShop/accueil");
				//request.setAttribute("passUser", "gar1972"); //passUser=gar1972&loginUser=JFSINGA
				//request.setAttribute("loginUser", "JFSINGA");
				//request.getRequestDispatcher("WEB-INF/crm/unContact.jsphttp://88.188.171.20:8080/WebShop/accueil").forward(request, response);
				//System.out.println("encode: " + response.encodeRedirectURL("http://88.188.171.20:8080/WebShop/accueil"));
				//response.sendRedirect(response.encodeURL("http://88.188.171.20:8080/WebShop/accueil"));
				
				ServletOutputStream out = response.getOutputStream() ;
				response.setContentType( "text/html" );
				out.println( "<HTML>" );
				out.println( "<HEAD>");
				out.println( 	"<meta charset='windows-1252'>");
				out.println( "</HEAD>" );
				out.println( "<BODY style='background-color:#2b2a30;'>" );
					out.println("<form action='../WebShop/accueil' name='mobilitePluch' method='POST'>");
						out.println("<input type='hidden' name='loginUser' value='" + ((Utilisateur)request.getSession().getAttribute("utilisateur")).getLogin() + "'/>");	
						out.println("<input type='hidden' name='passUser' value='" + ((Utilisateur)request.getSession().getAttribute("utilisateur")).getMdp() + "'/>");
						out.println("<input type='hidden' name='mobilitePluch' value='1'/>");	
					out.println("</form>");
					out.println("<script>");
					out.println("document.forms[\"mobilitePluch\"].submit();");
					out.println("</script>");
				out.println( "</BODY>" );
				out.println( "</HTML>" );
			}
			else
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}
		}
		catch (Exception e) {e.printStackTrace();}
	}

}
