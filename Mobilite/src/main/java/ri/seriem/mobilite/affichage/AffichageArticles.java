
package ri.seriem.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;
import ri.seriem.mobilite.metier.article.GestionArticles;

import ri.seriem.libas400.database.record.GenericRecord;

/**
 * Servlet implementation class AffichageArticles
 */
public class AffichageArticles extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private String[] infosRecherche = { "articles", "formArticles", "Recherche articles" };
  private PatternErgonomie pattern = null;
  private StringBuilder divListeClient = new StringBuilder(100);
  
  /**
   * @see HttpServlet#HttpServlet()
   *      Constructeur par d�faut d'une page d'affichage articles
   */
  public AffichageArticles() {
    super();
    pattern = new PatternErgonomie();
  }
  
  @Override
  public void init(ServletConfig config) {
    try {
      super.init(config);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    traiterPOSTouGET(request, response);
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    traiterPOSTouGET(request, response);
  }
  
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      String[] liensOptions = { "#", "mailto:", "#" };
      String metaSpecifiques = null;
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On cr�� un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        // si ce n'est pas fait, acc�der � la gestion m�tier des articles
        if (utilisateur.getGestionArticles() == null)
          utilisateur.setGestionArticles(new GestionArticles(utilisateur));
        
        // int vue = MarbreAffichage.METIER_ARTICLES;
        int vue = 0;
        
        // ++++++++++++++++++++++++++++++++++++ param�tres pass�s ++++++++++++++++++++++++++++++++++++++++
        String nomclient = request.getParameter("rechclient");
        // Traitement AJAX sur la recherche client
        if (nomclient != null) {
          response.getWriter().write(rechercheclient(utilisateur, nomclient));
          return;
        }
        
        // mot cl� de recherche -> recherche stock�e
        String recherche = request.getParameter("recherche");
        String special = request.getParameter("special");
        // donn�es d'un �tablissement
        String etb = request.getParameter("A1ETB");
        String code = request.getParameter("A1ART");
        String mag = request.getParameter("chgMagasin");
        
        String options = request.getParameter("options");
        int nbLignes = utilisateur.getNbLignesListes() + 1;
        
        String ancrage = request.getParameter("ancrage");
        // gestion de pages
        String page = request.getParameter("page");
        // mode modification
        boolean isEnModif = request.getParameter("isEnModif") != null && request.getParameter("isEnModif").equals("1");
        // tri sur les listes
        String tri = request.getParameter("tri");
        // niveau m�tier pour le fil rouge (liste, fiche...)
        if (request.getParameter("niveau") != null)
          utilisateur.getGestionArticles().setNiveauMetier(Integer.parseInt(request.getParameter("niveau")));
        // passer la vue demand�e � la gestion des clients
        if (request.getParameter("vue") != null) {
          try {
            // pour garder les variables de la pr�c�dente vue
            if (vue != 0)
              tri = "MEMELIGNES";
            else
              tri = "VUE";
            vue = Integer.parseInt(request.getParameter("vue"));
            
          }
          catch (Exception e) {
            vue = 1;
            e.printStackTrace();
            utilisateur.getLogs().setLog("Echec ParseInt" + request.getParameter("vue"), "AffArticles", "E");
          }
          
          // attribuer la vue � la gestion
          utilisateur.getGestionArticles().setVue(vue);
          
        }
        // R�cup�ration du num�ro client et du num�ro livr�
        utilisateur.getInfosClient().setWithUrlParameters(request.getParameter("clcli"), request.getParameter("clliv"));
        
        // gestion du nombre de lignes des listes
        if ((page != null && page.equals("retour")) || (tri != null && !tri.equals("PLUSLIGNES")) || request.getParameter("vue") != null)
          nbLignes = utilisateur.getGestionArticles().getNbLignesMax();
        else if (request.getParameter("plusDeLignes") != null)
          nbLignes = Integer.parseInt(request.getParameter("plusDeLignes")) + utilisateur.getNbLignesListes();
        
        // ++++++++++++++++++++++++++++++++++++ Traitements ++++++++++++++++++++++++++++++++++++++++
        
        // gestion des cookies articles
        utilisateur.getGestionArticles().setListeSelection(gestionCookies(request));
        
        // si on rafraichit la page inititialiser la gestion des articles
        if (page != null && page.equals("refresh"))
          utilisateur.getGestionArticles().initDonnes();
        
        // Changer le niveau du m�tier vers la fiche article si un code article est pass� ou si on change juste de vue
        if (etb != null && code != null)
          utilisateur.getGestionArticles().setNiveauMetier(2);
        // si on change juste de vue dans l'affichage d'un article
        else if (utilisateur.getGestionArticles().getNiveauMetier() == 2 && request.getParameter("vue") != null) {
          // pas bo pas propre. Le m�tier a rien � foutre l� bordel !! UN message perso de David pour David, je pense qu'il se
          // reconnaitra.
          etb = utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim();
          code = utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim();
        }
        // sinon changer le niveau m�tier vers la liste
        else
          utilisateur.getGestionArticles().setNiveauMetier(1);
        
        // charger le CSS pour la liste de clients et les infos pour les options de la page
        if (utilisateur.getGestionArticles().getNiveauMetier() == 1) {
          metaSpecifiques = "<link href='css/listes.css' rel='stylesheet'/>";
          liensOptions = new String[] { "#", "mailto:", null };
        }
        // CSS et infos pour la fiche articles et les infos pour les options de la page
        else
          metaSpecifiques = "<link href='css/fiches.css' rel='stylesheet'/>";
        
        switch (utilisateur.getGestionArticles().getTypeVue()) {
          case 1:
            break;
          case 2:
            metaSpecifiques += "<link href='css/stockDispo.css' rel='stylesheet'/>";
            break;
          case 3:
            metaSpecifiques += "<link href='css/prixVente.css' rel='stylesheet'/><script src='scripts/rechercheClient.js?"
                + MarbreEnvironnement.VERSION_JS + "'></script>";
            break;
        }
        
        ServletOutputStream out = response.getOutputStream();
        out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
        out.println(pattern.afficherHeader(MarbreAffichage.METIER_ARTICLES, utilisateur.getGestionArticles().getNiveauMetier(),
            infosRecherche, utilisateur));
        out.println(pattern.afficherOptions(utilisateur, MarbreAffichage.METIER_ARTICLES, utilisateur.getGestionArticles().getTypeVue(),
            utilisateur.getGestionArticles().getNiveauMetier(), false));
        
        // on affiche la liste que si le code article n'est pass� en param�tre et si on ne change pas de vue
        if (utilisateur.getGestionArticles().getNiveauMetier() == 1) {
          if (request.getParameter("chgMagasin") != null) {
            mag = request.getParameter("chgMagasin");
            utilisateur.changerMag(mag);
          }
          else if (!MarbreEnvironnement.stockDG) {
            mag = utilisateur.getMagasin().getCodeMagasin();
          }
          afficherLesArticles(out, utilisateur, recherche, tri, special, page, nbLignes);
          // liensOptions[1] = "mailto:?subject=liste articles";
          liensOptions[1] = "#";
        }
        else if (utilisateur.getGestionArticles().getNiveauMetier() == 2) {
          afficherUnArticle(out, utilisateur, etb, code, page, options, isEnModif);
          liensOptions[1] = "mailto:?subject=fiche article&body=" + utilisateur.getGestionArticles().retournerInfosBrutesFiche();
          liensOptions[2] = "articles?A1ETB=" + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim()
              + "&A1ART=" + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "&options=favoris";
        }
        
        out.println(pattern.afficherFin(titreOptions, liensOptions));
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Traitement pour la recherche client
   * @throws IOException
   */
  private String rechercheclient(Utilisateur utilisateur, String nomclient) {
    divListeClient.setLength(0);
    
    // Cas particulier : on r�-initialise les infos client
    /*
    if (nomclient.equals("*reset"))
    {
    	utilisateur.getInfosClient().reset();
    	return " ";
    }*/
    
    // On recherche les clients sur les premi�res lettres
    ArrayList<GenericRecord> listeClients = utilisateur.getGestionArticles().traitementRechercheClient(nomclient);
    if ((listeClients != null) && (listeClients.size() > 0))
      for (int i = 0; i < listeClients.size(); i++)
        divListeClient.append("<a class='detailClient' href=\"articles?vue=3&clcli=").append(listeClients.get(i).getField("CLCLI"))
            .append("&clliv=").append(listeClients.get(i).getField("CLLIV")).append("\">")
            .append(((String) listeClients.get(i).getField("CLNOM")).trim()).append("</a>");
      
    return divListeClient.toString();
  }
  
  /**
   * r�cup�re les cookies du navigateur pour en r�cup�rer les valeurs correspondantes � notre page
   */
  private String[] gestionCookies(HttpServletRequest request) {
    String[] idArticles = null;
    // r�cup�ration des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    // trouver notre cookie et r�cup�rer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      if (cookies[i].getName().equals("selectionArticle")) {
        String valeurs = cookies[i].getValue();
        if (valeurs != null && !valeurs.trim().equals(""))
          idArticles = valeurs.split("_");
      }
    }
    
    return idArticles;
  }
  
  /**
   * affiche la liste des articles en fonction des vues, types de recherche et des tris pass�s
   */
  private void afficherLesArticles(ServletOutputStream out, Utilisateur utilisateur, String recherche, String tri, String special,
      String page, int nbLignes) {
    try {
      String apport = "";
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='listeMetier'>");
      
      // Si on tri la liste pr�c�dente
      if (tri != null) {
        if (recherche == null)
          recherche = utilisateur.getGestionArticles().getMotCle();
        if (special == null)
          special = utilisateur.getGestionArticles().getRechercheSpeciale();
        
        System.out.println("recherche: " + recherche);
        System.out.println("special: " + special);
      }
      // si on est pas en mode tri
      else {
        // si on passe un mot de recherche -> p�ter la recherche sp�ciale stock�e
        if (recherche != null)
          special = null;
      }
      
      if (utilisateur.getGestionArticles().getListeRecords() == null)
        apport = "Mes favoris";
      
      // calculer une nouvelle liste si on ne retourne pas sur l'ancienne
      if (page == null || !page.equals("retour") || tri != null) {
        // si on r�cup�re les m�me lignes
        if (tri != null && (tri.equals("MEMELIGNES") || tri.equals("VUE")) && utilisateur.getGestionArticles().getListeRecords() != null)
          utilisateur.getGestionArticles().setListeRecords(utilisateur.getGestionArticles().recuperationDesMemeRecords(tri));
        else
          utilisateur.getGestionArticles().chercherDesRecords(recherche, tri, special, nbLignes);
      }
      // on retourne sur l'ancienne mais avec d'�ventuelles nouvelles donn�es
      else
        utilisateur.getGestionArticles().setListeRecords(utilisateur.getGestionArticles().recuperationDesMemeRecords(tri));
      
      // traiter le texte apport
      if (utilisateur.getGestionArticles().getRechercheSpeciale() != null) {
        if (utilisateur.getGestionArticles().getRechercheSpeciale().equals("fav"))
          apport = "Mes favoris";
        if (utilisateur.getGestionArticles().getRechercheSpeciale().equals("sel"))
          apport = "Ma s&eacute;lection";
      }
      
      // affichage des clients par vue demand�e
      switch (utilisateur.getGestionArticles().getTypeVue()) {
        case 1:
          break;
        case 2:
          out.println(utilisateur.getGestionArticles().getStockdispo().afficherListeRecords(apport, nbLignes));
          break;
        case 3:
          out.println(utilisateur.getGestionArticles().getPrixVente().afficherListeRecords(apport, nbLignes));
          break;
      }
      out.println("</section>");
      out.println("</article>");
    }
    catch (Exception e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("Erreur sur afficherLesArticles()", "AffArticles", "E");
    }
  }
  
  /**
   * Affiche l'article s�lectionn� ou recherch�
   */
  private void afficherUnArticle(ServletOutputStream out, Utilisateur utilisateur, String etb, String code, String page, String options,
      boolean isEnModif) {
    try {
      boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='ficheMetier'>");
      utilisateur.getGestionArticles().recupererUnRecord(etb, code);
      
      // traiter les favoris
      isUnFavori = utilisateur.getGestionArticles().isUnFavori(options != null && options.equals("favoris"));
      
      if (utilisateur.getGestionArticles().getRecordActuel() == null)
        out.println("L'article n'a pas &eacute;t&eacute; trouv&eacute;");
      else {
        // r�cup�rer la vue demand�e
        switch (utilisateur.getGestionArticles().getTypeVue()) {
          case 1:
            break;
          case 2:
            out.println(utilisateur.getGestionArticles().getStockdispo().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 3:
            out.println(utilisateur.getGestionArticles().getPrixVente().afficherLeRecord(isUnFavori, isEnModif));
            break;
        }
        
        out.println(majTailleFiche("ficheMetier"));
      }
      out.println("</section>");
      out.println("</article>");
    }
    catch (Exception e) {
      e.printStackTrace();
      utilisateur.getLogs().setLog("Erreur sur afficherUnArticle()", "AffArticles", "E");
    }
  }
  
  /**
   * retailler la zone de fiche principale de mani�re dynamiiiiique
   */
  private String majTailleFiche(String id) {
    String retour = "<script>";
    retour += "majTailleFiche(\"" + id + "\");";
    retour += "</script>";
    
    return retour;
  }
  
}
