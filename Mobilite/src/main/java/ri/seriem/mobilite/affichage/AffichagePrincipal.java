package ri.seriem.mobilite.affichage;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.backoffice.GestionBOversion;
import ri.seriem.mobilite.constantes.MarbreAffichage;
import ri.seriem.mobilite.constantes.MarbreEnvironnement;


/**
 * Classe d'affichage de l'accueil de S�rie Mobile
 */
public class AffichagePrincipal extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	//private String[] infosRecherche = {"accueil","rechGlobale","Recherche globale"};
	private static final String URLReport = "http://88.188.171.20:8085";
	
	private String[] infosRecherche = null;
	
	private PatternErgonomie pattern = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
 	public AffichagePrincipal() {
		super();
		pattern = new PatternErgonomie();
	}
	
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config)
	{
		try
		{
			super.init(config);
		}
		catch (Exception e) {e.printStackTrace();}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,HttpServletResponse response) //throws ServletException, IOException 
	{
		traiterPOSTouGET(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	{
		traiterPOSTouGET(request, response);
	}
	
	/**
	 * M�thode g�n�rique permettant de traiter le GET et le POST 
	 * */
	private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response)
	{
		try 
		{
			ServletOutputStream out = response.getOutputStream() ;
		
			if(request.getSession().getAttribute("utilisateur")!= null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
			{
				if(checkerLesVersions((Utilisateur)request.getSession().getAttribute("utilisateur")))
					if( (request.getParameter("menu") == null) || !request.getParameter("menu").equals("G") )
						out.println( afficherAccueil( (Utilisateur)request.getSession().getAttribute("utilisateur"), null) );
					else{
						out.println( afficherAccueilGeneral( (Utilisateur)request.getSession().getAttribute("utilisateur"), null) );
					}
						
				else
					out.println( affichageMAJBDD( (Utilisateur)request.getSession().getAttribute("utilisateur") ) );
			}
			else
			{
				request.getSession().invalidate();
				getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			}
		}
		catch (Exception e) {e.printStackTrace();}
	}

	/**
	 * Afficher la page d'accueil g�n�ral
	 * */
	private String afficherAccueilGeneral(Utilisateur utilisateur, String message)
	{
		String chaine = "";
		try 
		{
			chaine+= pattern.afficherEnTete("<link href='css/accueilGeneral.css' rel='stylesheet'/>",null);
			//chaine+= pattern.afficherHeader(0,0,infosRecherche, utilisateur);
			chaine+= pattern.afficherHeader(-100,0,infosRecherche, utilisateur);
			//chaine+= pattern.afficherOptions(utilisateur,0,MarbreAffichage.METIER_ACCUEIL,0,false);
			chaine+= pattern.afficherOptions(utilisateur,-2,-1,1,false);
			
			chaine+= "<article id='contenu'>";
			if(message!=null)
				chaine += "<div id='messageAccueil'>" + message + "</div>";
			chaine+= "<section id='gestionAccueil'>";
				chaine+= "<div class='favorisAccueil'><a class='lienFavorisAccueil' href='accueil'><div class='clmob'><div class='grostit'>Mobilit�</div><div class='petittit'>Consultation</div></div></a></div>";
				chaine+= "<div class='favorisAccueil'><a  class='lienFavorisAccueil' onclick='return false' id='mobilitePlus' href='mobilitePluch'><div class='clmobplus'><div class='grostit'>Mobilit� +</div><div class='petittit'>Saisie devis/Commande</div></div></a></div>";	
				//chaine+= "<div class='favorisAccueil'><a  class='lienFavorisAccueil' href='http://88.188.171.20:8080/WebShop/accueil?passUser=gar1972&loginUser=JFSINGA'><div class='clmobplus'><div class='grostit'>Mobilit� +</div><div class='petittit'>Saisie devis/Commande</div></div></a></div>";	
				chaine+= "<div class='favorisAccueil'><a  class='lienFavorisAccueil' onclick='return false' id='mobilitePlus' href='crm'><div class='clcrm'><div class='grostit'>CRM</div><div class='petittit'>Action commerciale</div></div></a></div>";	
				chaine+= "<div class='favorisAccueil'><a  class='lienFavorisAccueil' onclick='return false' id='mobilitePlus' href='"+URLReport+"' target='_blank'><div class='clreport'><div class='grostit'>Report <span id='cacheMoi'>Mobilit�</span></div><div class='petittit'>Tableau de bord</div></div></a></div>";	

					
			chaine+= "</section>";
			//test appareil photo
			
			//fin de test cam�ra
			chaine+= "</article> "; 
			
			//chaine+= "<a href='connexion?connexion=stop' id='bt_Deco'></a>";
			// A param�trer
			
			chaine+= "</body>";
			chaine+= "</html>";
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			utilisateur.getLogs().setLog("Erreur sur afficherAccueil:" + e.getMessage(), "AffPrincipal", "E");
		}
		
		return chaine ;
	}

	/**
	 * Afficher la page d'accueil
	 * */
	private String afficherAccueil(Utilisateur utilisateur, String message)
	{
		String chaine = "";
		try 
		{
			chaine+= pattern.afficherEnTete("<link href='css/accueil.css' rel='stylesheet'/>",null);
			chaine+= pattern.afficherHeader(0,0,infosRecherche, utilisateur);
			chaine+= pattern.afficherOptions(utilisateur,-2,-1,1,false);
			//chaine+= pattern.afficherOptions(utilisateur,0,MarbreAffichage.METIER_ACCUEIL,0,false);
			
			chaine+= "<article id='contenu'>";
			if(message!=null)
				chaine += "<div id='messageAccueil'>" + message + "</div>";
			chaine+= "<section id='gestionAccueil'>";
			if(utilisateur.getMesVues()!=null)
			{
				int nbBlocs = MarbreAffichage.VUES_ACCUEIL;
				if(utilisateur.getMesVues().size()>= nbBlocs)
					nbBlocs += MarbreAffichage.VUES_LIGNES_ACCUEIL;
				
				//Afficher les boutons de vue ou les boutons vides	
				for(int i = 0; i < nbBlocs; i++)
				{
					if(i>= utilisateur.getMesVues().size())
						chaine+="<div class='favorisAccueilVide'><a href='catalogue'><span class='superf'>Ajouter une </span>nouvelle vue</a></div>";
					else chaine+= "<div class='favorisAccueil'><a href='" + utilisateur.getMesVues().get(i).getLien() + "'><img src='" + utilisateur.getMesVues().get(i).getImage() + "'><p>"+ utilisateur.getMesVues().get(i).getDenomination() +"</p></a></div>";	
				}
			}
					
			chaine+= "</section>";
			//test appareil photo
			
			//fin de test cam�ra
			chaine+= "</article> "; 
			
			//chaine+= "<a href='connexion?connexion=stop' id='bt_Deco'></a>";
			// A param�trer
			
			chaine+= "</body>";
			chaine+= "</html>";
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			utilisateur.getLogs().setLog("Erreur sur afficherAccueil:" + e.getMessage(), "AffPrincipal", "E");
		}
		
		return chaine ;
	}
	
	private String affichageMAJBDD(Utilisateur utilisateur)
	{
		String chaine = "";
		if(utilisateur.getGestionBOversion().majBDD())
			chaine+= afficherAccueil(utilisateur,"Nouvelle version " + utilisateur.getOutils().traduireVersion(MarbreEnvironnement.VERSION_DEV) + " install�e avec succ�s");
		else	
		{
			chaine+="<div>Pb de mise � jour de la BDD</div>";
		}
		
		return chaine;
	}
	
	private boolean checkerLesVersions(Utilisateur utilisateur)
	{
		if(utilisateur.getGestionBOversion()==null)
			utilisateur.setGestionBOversion(new GestionBOversion(utilisateur));
		//Je contr�le la version du War et je la mets � jour dans la constante �quivalente
		if(utilisateur.getGestionBOversion().majConstanteVersion())
			return utilisateur.getGestionBOversion().controlerVersionBDD();
		else
			return false;
	}
}



