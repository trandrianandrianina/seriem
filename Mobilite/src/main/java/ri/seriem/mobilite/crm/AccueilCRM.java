package ri.seriem.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;

/**
 * Servlet implementation class AccueilCRM
 */
public class AccueilCRM extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private PatternErgonomie pattern = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccueilCRM() {
    	super();
       // pattern = new PatternErgonomie();
    }
    
    @Override
	public void init(ServletConfig config)
	{
		try
		{
			super.init(config);
		} 
		catch (Exception e) {e.printStackTrace();}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		//Si on est connecté 
		if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
		{
			request.getSession().setAttribute("filRouge", "0");
			request.getSession().setAttribute("optionsCRM", "0");
			request.setAttribute("cssSpecifique", "css/accueil.css");
			request.setAttribute("utilisateur", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getLogin());
			request.setAttribute("bibli", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getBibli());
			// Récupération du dispatcher + forward JSP
			request.getRequestDispatcher("WEB-INF/crm/accueil.jsp").forward(request, response);
		}
		else 
		{
			request.getSession().invalidate();
			getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
