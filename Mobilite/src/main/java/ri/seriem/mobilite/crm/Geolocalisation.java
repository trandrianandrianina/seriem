
package ri.seriem.mobilite.crm;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.database.record.GenericRecord;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.metier.crm.GestionGeolocalisation;

/**
 * Servlet implementation class Geolocalisation
 */
public class Geolocalisation extends HttpServlet {
  private static final long serialVersionUID = 1L;
  // Controleur de cette classe d'affichage
  private GestionGeolocalisation gestionGeo = null;
  private String[] tabMotsPourris = { "B.P", "BP", "CEDEX" };
  private String departement = "";
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Geolocalisation() {
    super();
    gestionGeo = new GestionGeolocalisation();
  }
  
  @Override
  public void init(ServletConfig config) {
    try {
      super.init(config);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    // Si on est connect�
    if (request.getSession().getAttribute("utilisateur") != null
        && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
      departement = (String) request.getParameter("departement");
      ArrayList<GenericRecord> listeDeClients =
          gestionGeo.retournerListeClients((Utilisateur) request.getSession().getAttribute("utilisateur"), departement);
      request.setAttribute("clientsProx", listeDeClients);
      request.setAttribute("listeAdresses", controlerLesClientsPourGoogle(listeDeClients));
      request.getSession().setAttribute("filRouge", "1");
      request.getSession().setAttribute("optionsCRM", "4");
      request.setAttribute("cssSpecifique", "css/fiches.css");
      request.setAttribute("cssSpecifique2", "css/listes.css");
      request.setAttribute("utilisateur", ((Utilisateur) request.getSession().getAttribute("utilisateur")).getLogin());
      request.setAttribute("bibli", ((Utilisateur) request.getSession().getAttribute("utilisateur")).getBibli());
      // R�cup�ration du dispatcher + forward JSP
      request.getRequestDispatcher("WEB-INF/crm/geolocalisation.jsp").forward(request, response);
    }
    else {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
    }
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub
    
    // Si on est connect�
    if (request.getSession().getAttribute("utilisateur") != null
        && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
      // r�cup�ration du d�partement, pass� en POST
      departement = (String) request.getParameter("departement");
      // Appel de la requ�te (utilisateur et le d�partement dans lequel il se trouve)
      ArrayList<GenericRecord> listeDeClients =
          gestionGeo.retournerListeClients((Utilisateur) request.getSession().getAttribute("utilisateur"), departement);
      request.setAttribute("clientsProx", listeDeClients);
      request.setAttribute("listeAdresses", controlerLesClientsPourGoogle(listeDeClients));
      request.getSession().setAttribute("filRouge", "1");
      request.getSession().setAttribute("optionsCRM", "4");
      request.setAttribute("cssSpecifique", "css/fiches.css");
      request.setAttribute("cssSpecifique2", "css/listes.css");
      request.setAttribute("utilisateur", ((Utilisateur) request.getSession().getAttribute("utilisateur")).getLogin());
      request.setAttribute("bibli", ((Utilisateur) request.getSession().getAttribute("utilisateur")).getBibli());
      // R�cup�ration du dispatcher + forward JSP
      request.getRequestDispatcher("WEB-INF/crm/geolocalisation.jsp").forward(request, response);
    }
    else {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
    }
  }
  
  /**
   * Permet de retourner une String format�e et contr�ler pour la recherche d'adresses dans Google Maps
   */
  private String controlerLesClientsPourGoogle(ArrayList<GenericRecord> listeClients) {
    String retour = null;
    String rue = "";
    String ville = "";
    
    if (listeClients != null) {
      int j = 0;
      for (int i = 0; i < listeClients.size(); i++) {
        // Contr�le de pr�sence
        if (listeClients.get(i).isPresentField("CLRUE") && listeClients.get(i).isPresentField("CLLOC"))
          rue = listeClients.get(i).getField("CLRUE").toString().trim() + " " + listeClients.get(i).getField("CLLOC").toString().trim()
              + ",";
        if (listeClients.get(i).isPresentField("CLVIL"))
          ville = listeClients.get(i).getField("CLVIL").toString().trim();
        
        if (!rue.equals("") && !ville.equals("") && controlerMotsFoireux(rue, ville)) {
          if (j > 0)
            retour += ",";
          else
            retour = "";
          retour += "[\"" + rue + listeClients.get(i).getField("CLVIL").toString().trim() + ", FRANCE\", \""
              + listeClients.get(i).getField("CLNOM").toString().trim() + "\"]";
          j++;
        }
      }
    }
    
    System.out.println("liste Google: " + retour);
    
    return retour;
  }
  
  /**
   * On contr�le que les blocs adresses ne contiennent pas de mots foireux
   */
  private boolean controlerMotsFoireux(String rue, String ville) {
    
    for (int i = 0; i < tabMotsPourris.length; i++) {
      if (rue.indexOf(tabMotsPourris[i]) > -1)
        return false;
      if (ville.indexOf(tabMotsPourris[i]) > -1)
        return false;
    }
    
    return true;
  }
}
