
package ri.seriem.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.tools.convert.ConvertDate;

import ri.seriem.mobilite.Environnement.Utilisateur;

import ri.seriem.libas400.dao.exp.programs.contact.M_Contact;

/**
 * Servlet implementation class Contact
 */
public class Contact extends HttpServlet {
  
  private static final long serialVersionUID = 1L;
  
  // Constantes
  public static final int LIST = 10;
  
  // Variables
  private Utilisateur user = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Contact() {
    super();
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if (!isConnected(request, response))
      return;
    initialization(request);
    
    // Si on a choisi un client et donc on affiche la liste des actions commerciales qui le concerne
    if (request.getParameter("action2do") != null) {
      String action2do = request.getParameter("action2do");
      String idcontact = request.getParameter("idcontact");
      int id = (idcontact == null) ? 0 : Integer.parseInt(idcontact);
      action2Do(Integer.parseInt(action2do.trim()), id, request, response);
    }
    // System.out.println("-GET->contact");
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if (!isConnected(request, response))
      return;
    initialization(request);
    
    // Si on a choisi un client et donc on affiche la liste des actions commerciales qui le concerne
    if (request.getParameter("action2do") != null) {
      String action2do = request.getParameter("action2do");
      String idcontact = request.getParameter("idcontact");
      int id = (idcontact == null) ? 0 : Integer.parseInt(idcontact);
      action2Do(Integer.parseInt(action2do.trim()), id, request, response);
    }
    // System.out.println("-POST->contact");
  }
  
  /**
   * Gestion des actions en fonction de la demande
   * @param action2do
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void action2Do(int action2do, int idcontact, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    switch (action2do) {
      case GestionFormulaire.CONSULTATION:
        System.out.println("--> Consultation d'un contact");
        user.getFormulaireCourant().activeConsultation();
        request.setAttribute("unContact", user.getGestionAC().retournerUnContact(idcontact));
        request.setAttribute("formulaire", user.getFormulaireCourant());
        afficheDetailContact(request, response);
        break;
      case GestionFormulaire.CREATION:
        System.out.println("--> Cr�ation d'un contact");
        user.getFormulaireCourant().activeCreation();
        request.setAttribute("unContact", user.getGestionAC().retournerUnContact(idcontact));
        request.setAttribute("formulaire", user.getFormulaireCourant());
        afficheDetailContact(request, response);
        break;
      case GestionFormulaire.MODIFICATION:
        System.out.println("--> Modification d'un contact");
        user.getFormulaireCourant().activeModification();
        request.setAttribute("unContact", user.getGestionAC().retournerUnContact(idcontact));
        request.setAttribute("formulaire", user.getFormulaireCourant());
        afficheDetailContact(request, response);
        break;
      case GestionFormulaire.SUPPRESSION:
        System.out.println("--> Suppression d'un contact");
        user.getFormulaireCourant().activeConsultation();
        // Supprime l'action
        user.getGestionAC().supprimeContact(user.getGestionAC().getCCourant());
        // On r�affiche la liste des contacts
        listContact(request, response);
        break;
      case GestionFormulaire.ANNULATION:
        System.out.println("--> Annulation de la cr�ation d'un contact");
        user.getFormulaireCourant().activeConsultation();
        // On r�affiche la liste des contacts
        listContact(request, response);
        break;
      case GestionFormulaire.VALIDATION:
        if (isValidatedData(request, user.getGestionAC().getCCourant(idcontact))) {
          System.out.println("Validation r�ussie !!");
          if (idcontact == 0) { // Il s'agit d'un ajout
            if (!user.getGestionAC().insertContact(user.getGestionAC().getCCourant())) {
              System.out.println("Echec de l'insertion" + user.getGestionAC().getMsgError());
              // Affichage du formulaire
              afficheDetailContact(request, response);
            }
            else {
              user.getFormulaireCourant().activeConsultation();
              
              // On r�cup�re la liste des contacts
              listContact(request, response);
            }
          }
          else { // Il s'agit d'une modification
            if (!user.getGestionAC().updateContact(user.getGestionAC().getCCourant(idcontact))) {
              System.out.println("Echec de la mise � jour" + user.getGestionAC().getMsgError());
            }
            else {
              user.getFormulaireCourant().activeConsultation();
            }
            // On r�affiche le contact en mode consultation
            request.setAttribute("unContact", user.getGestionAC().getCCourant());
            request.setAttribute("formulaire", user.getFormulaireCourant());
            
            // Affichage du formulaire
            afficheDetailContact(request, response);
          }
        }
        else {
          System.out.println("Echec de la validation!!");
        }
        break;
      
      case LIST:
        System.out.println("--> Liste des contacts");
        // Liste des contacts pour un client
        listContact(request, response);
        break;
    }
  }
  
  /**
   * Affiche la page avec la liste des contacts
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void listContact(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("listeContacts", user.getGestionAC().getListeContactsClient4List(request.getParameter("idclient"),
        request.getParameter("clsuf"), receiveCookie(request, "/", "selectionContact")));
    request.setAttribute("urlRetour", receiveCookie(request, "/", "urlRetour"));
    afficheListeContacts(request, response);
  }
  
  /**
   * Contr�le la validit� des donn�es du formulaire
   * @param request
   * @param actioncom
   * @return
   */
  private boolean isValidatedData(HttpServletRequest request, M_Contact contact) {
    // Contr�le de la civilit�
    String valeur = request.getParameter("c_civcontact");
    int indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < contact.getListCodeCivility().length) {
        contact.setRECIV(contact.getListCodeCivility()[indice]);
        System.out.println("-civilit�-<>    " + contact.getRECIV());
      }
      else
        return false;
    }
    else {
      return false;
    }
    
    // Contr�le du nom
    valeur = request.getParameter("c_namecontact");
    if ((valeur != null) && (valeur.trim().length() > 0)) {
      contact.setRENOM(valeur);
      System.out.println("-nom-<>    " + contact.getRENOM());
    }
    else {
      return false;
    }
    
    // Contr�le du pr�nom
    valeur = request.getParameter("c_firstnamecontact");
    if (valeur != null) {
      contact.setREPRE(valeur);
      System.out.println("-pr�nom-<>    " + contact.getREPRE());
    }
    else {
      return false;
    }
    
    // Contr�le de la cat�gorie
    valeur = request.getParameter("c_catcontact");
    indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < contact.getListCodeClass().length) {
        contact.setRECAT(contact.getListCodeClass()[indice]);
        System.out.println("-categorie-<>    " + contact.getRECAT());
      }
      else
        return false;
    }
    else {
      return false;
    }
    
    // Contr�le du t�l�phone
    valeur = request.getParameter("c_tel1contact");
    if (valeur != null) {
      contact.setRETEL(valeur);
      System.out.println("-t�l�phone-<>    " + contact.getRETEL());
    }
    else {
      return false;
    }
    
    // Contr�le du t�l�phone 2
    valeur = request.getParameter("c_tel2contact");
    if (valeur != null) {
      contact.setRETEL2(valeur);
      System.out.println("-t�l�phone 2-<>    " + contact.getRETEL2());
    }
    else {
      return false;
    }
    
    // Contr�le du fax
    valeur = request.getParameter("c_faxcontact");
    if (valeur != null) {
      contact.setREFAX(valeur);
      System.out.println("-fax-<>    " + contact.getREFAX());
    }
    else {
      return false;
    }
    
    // Contr�le du mail 1
    valeur = request.getParameter("c_mail1contact");
    if (valeur != null) {
      contact.setRENET(valeur);
      System.out.println("-mail 1-<>    " + contact.getRENET());
    }
    else {
      return false;
    }
    
    // Contr�le du mail 2
    valeur = request.getParameter("c_mail2contact");
    if (valeur != null) {
      contact.setRENET2(valeur);
      System.out.println("-mail 2-<>    " + contact.getRENET2());
    }
    else {
      return false;
    }
    
    return true;
  }
  
  /**
   * Lecture des cookies sur le poste client
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private String receiveCookie(HttpServletRequest request, String path, String namecookie) throws ServletException, IOException {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        // System.out.println("-path->" + cookie.getPath());
        if (cookie.getName().equals(namecookie)) {// && cookie.getPath().equals( path )){
          return cookie.getValue();
        }
      }
    }
    return "";
  }
  
  /**
   * V�rifie que l'on est bien connect�
   * @param request
   * @param response
   * @return
   * @throws ServletException
   * @throws IOException
   */
  private boolean isConnected(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Object user = request.getSession().getAttribute("utilisateur");
    
    // Si on n'est pas connect�
    if ((user == null) || !(user instanceof Utilisateur)) {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      return false;
    }
    
    return true;
  }
  
  /**
   * Initialisation lors de la premi�re utilisation
   * @param request
   */
  private void initialization(HttpServletRequest request) {
    // On passe l'utilisateur � la JSP pour le look g�n�rique
    user = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    
    request.setAttribute("utilisateur", user.getLogin());
    request.setAttribute("bibli", user.getBibli());
    // Option CRM s�lectionn� par l'utilisateur
    // request.setAttribute("optionsCRM", "1");
    
    user.getGestionAC().setUtilisateur((Utilisateur) request.getSession().getAttribute("utilisateur"));
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des contacts
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheListeContacts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // L'�tat du formulaire courant
    request.setAttribute("formulaire", user.getFormulaireCourant());
    // request.setAttribute("moteurRecherche", "ActionsCom");
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "5");
    request.getSession().setAttribute("filRouge", "5");
    
    // CSS sp�cifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // R�cup�ration du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/listeContacts.jsp").forward(request, response);
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des contacts
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheDetailContact(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // L'�tat du formulaire courant
    request.setAttribute("formulaire", user.getFormulaireCourant());
    
    // l'URL de provenance
    request.setAttribute("urlRetour", receiveCookie(request, "/", "urlRetour"));
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "5");
    request.getSession().setAttribute("filRouge", "5");
    
    // CSS sp�cifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // R�cup�ration du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/unContact.jsp").forward(request, response);
  }
  
}
