
package ri.seriem.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.metier.crm.GestionItineraire;

/**
 * Servlet implementation class Itineraire
 */
public class Itineraire extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private GestionItineraire gestionIt = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Itineraire() {
        super();
        gestionIt = new GestionItineraire();
    }

    @Override
   	public void init(ServletConfig config)
   	{
   		try
   		{
   			super.init(config);
   		} 
   		catch (Exception e) {e.printStackTrace();}
   	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si on est connect� 
		if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
		{
			request.getSession().setAttribute("optionsCRM", "3");
			//On passe l'utilisateur � la JSP pour le look g�n�rique
			request.setAttribute("utilisateur", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getLogin());
			request.setAttribute("bibli", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getBibli());
			
			//Avec un client saisi
			if(request.getParameter("numClientSaisi")!=null && request.getParameter("sufClientSaisi")!=null)
			{
				//R�cup�ration des donn�es m�tier
				request.setAttribute("unClient",gestionIt.retournerUnClient(((Utilisateur)request.getSession().getAttribute("utilisateur")),request.getParameter("numClientSaisi"),request.getParameter("sufClientSaisi")));
				
				//Navigation
				request.getSession().setAttribute("filRouge", "2");
				//look
				request.setAttribute("cssSpecifique", "css/fiches.css");
				
				// R�cup�ration du dispatcher + forward JSP
				request.getRequestDispatcher("WEB-INF/crm/itineraire.jsp").forward(request, response);
			}
			//Liste de clients
			else
			{
				request.setAttribute("moteurRecherche","itineraire");
				//R�cup�ration des donn�es m�tier
				if(request.getParameter("retour")!=null && ((Utilisateur)request.getSession().getAttribute("utilisateur")).getListeClientsCRM()!=null)
					request.setAttribute("listeClients",((Utilisateur)request.getSession().getAttribute("utilisateur")).getListeClientsCRM());
				
				//request.setAttribute("listeClients",gestionIt.retournerClients(((Utilisateur)request.getSession().getAttribute("utilisateur"))));
				//Navigation
				request.getSession().setAttribute("filRouge", "1");
				//look
				request.setAttribute("cssSpecifique", "css/listes.css");
				
				// R�cup�ration du dispatcher + forward JSP
				request.getRequestDispatcher("WEB-INF/crm/listeItineraire.jsp").forward(request, response);
			}
		}
		else 
		{
			request.getSession().invalidate();
			getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("utilisateur")!=null && request.getSession().getAttribute("utilisateur") instanceof Utilisateur)
		{
			request.getSession().setAttribute("optionsCRM", "3");
			//On passe l'utilisateur � la JSP pour le look g�n�rique
			request.setAttribute("utilisateur", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getLogin());
			request.setAttribute("bibli", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getBibli());
			
			if(request.getParameter("recherche")!=null)
			{
				request.setAttribute("moteurRecherche","itineraire");
				//R�cup�ration des donn�es m�tier
				((Utilisateur)request.getSession().getAttribute("utilisateur")).setListeClientsCRM(gestionIt.retournerClients(((Utilisateur)request.getSession().getAttribute("utilisateur")),request.getParameter("recherche"),20));
				request.setAttribute("listeClients",((Utilisateur)request.getSession().getAttribute("utilisateur")).getListeClientsCRM());
				
				//Navigation
				request.getSession().setAttribute("filRouge", "1");
				//look
				request.setAttribute("cssSpecifique", "css/listes.css");
				
				// R�cup�ration du dispatcher + forward JSP
				request.getRequestDispatcher("WEB-INF/crm/listeItineraire.jsp").forward(request, response);
			}
		}
		else 
		{
			request.getSession().invalidate();
			getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
		}
		
	}

}
