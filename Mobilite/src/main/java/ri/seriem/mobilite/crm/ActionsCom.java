
package ri.seriem.mobilite.crm;

import ri.seriem.libas400.dao.exp.programs.contact.M_EvenementContact;
import ri.seriem.libas400.dao.gvm.programs.actioncommerciale.M_ActionCommerciale;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.tools.convert.ConvertDate;
import ri.seriem.libas400.tools.convert.TimeOperations;

import ri.seriem.mobilite.Environnement.Utilisateur;

/**
 * Servlet implementation class ActionsCom
 */
public class ActionsCom extends HttpServlet {
  
  private static final long serialVersionUID = 1L;
  
  // Variables
  private TimeOperations timeOperations = new TimeOperations();
  private Utilisateur user = null;
  private boolean firstTimeInModif = true;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public ActionsCom() {
    super();
  }
  
  @Override
  public void init(ServletConfig config) {
    try {
      super.init(config);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if (!isConnected(request, response))
      return;
    initialization(request);
    
    // Si on a choisi une action � effectuer sur une action en particulier
    if (request.getParameter("idaction") != null) {
      String action2do = request.getParameter("action2do");
      String idAction = request.getParameter("idaction");
      action2Do(action2do, idAction, request, response);
    }
    else
    // Si on a choisi un client et donc on affiche la liste des actions commerciales qui le concerne
    if (request.getParameter("client") != null && request.getParameter("suffixe") != null) {
      // On cr�� un cookie avec l'url pour pouvoir revenir avec le bouton Back
      sendCookie(response, "/", "urlRetour", "ActionsCom");
      
      // On met � jour le client courant
      user.getGestionAC().setClCourant(request.getParameter("client"), request.getParameter("suffixe"));
      
      // On r�cup�re la liste des actions commerciales
      request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
      request.setAttribute("mclient", user.getGestionAC().getClCourant());
      
      // Affichage du formulaire
      afficheListeActionCom(request, response);
    }
    // Sinon on affiche la liste des clients
    else {
      // On r�cup�re la liste des clients
      request.setAttribute("listeClients",
          user.getGestionAC().getListeClients(request.getParameter("tri"), request.getParameter("recherche")));
      
      // Affichage du formulaire
      afficheListeClients(request, response);
    }
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    if (!isConnected(request, response))
      return;
    initialization(request);
    
    // On effectue une recherche
    if (request.getParameter("recherche") != null) {
      request.setAttribute("moteurRecherche", "ActionsCom");
      
      // On r�cup�re la liste des clients
      request.setAttribute("listeClients",
          user.getGestionAC().getListeClients(request.getParameter("tri"), request.getParameter("recherche")));
      
      // Affichage du formulaire
      afficheListeClients(request, response);
    }
    else {
      String action2do = request.getParameter("action2do");
      String idActionCom = request.getParameter("idaction");
      // Contr�le des param�tres
      // System.out.println("--> action2Do : " + action2do + " " + request.getParameter( "action2do2" ) );
      action2Do(action2do, idActionCom, request, response);
    }
  }
  
  // -- M�thodes priv�es ----------------------------------------------------
  
  /**
   * Contr�le la validit� des donn�es du formulaire
   * @param request
   * @param actioncom
   * @return
   */
  private boolean isValidatedData(HttpServletRequest request, M_ActionCommerciale actioncom) {
    // System.out.println("-id-<> " + actioncom.getACID() + " " +actioncom.getEvenementContact().getETID() );
    // System.out.println("-id-<> " + actioncom.getListObjectAction() );
    // System.out.println("-id-<> " + actioncom.getListCodeObjectAction() );
    // System.out.println("-id-<> " + actioncom.getListLabelObjectAction() );
    
    // Contr�le de l'objet
    String valeur = request.getParameter("ac_objetaction");
    int indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < actioncom.getListCodeObjectAction().length) {
        actioncom.setACOBJ(actioncom.getListCodeObjectAction()[indice]);
        System.out.println("-objet-<>    " + actioncom.getACOBJ());
      }
      else
        return false;
    }
    else {
      return false;
    }
    
    // Contr�le de l'�tat
    valeur = request.getParameter("ac_etataction");
    indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < M_EvenementContact.getListState().length) {
        actioncom.getEvenementContact().setETETA((byte) indice);
        System.out.println("-etat-<>    " + actioncom.getEvenementContact().getETETA());
      }
      else
        return false;
    }
    else {
      return false;
    }
    
    // Contr�le de l'�tat
    valeur = request.getParameter("ac_prioriteaction");
    indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < M_EvenementContact.getListPriority().length) {
        actioncom.getEvenementContact().setETCODP((byte) indice);
        System.out.println("-priorit�-<>    " + actioncom.getEvenementContact().getETCODP());
      }
      else
        return false;
    }
    else {
      return false;
    }
    
    // Contr�le de l'observation
    valeur = request.getParameter("ac_obsaction");
    if (valeur != null) {
      actioncom.getEvenementContact().setETOBS(valeur);
      System.out.println("-obs-<>      " + actioncom.getEvenementContact().getETOBS());
      
    }
    else {
      return false;
    }
    
    // Contr�le du temps pass�
    valeur = request.getParameter("ac_tpspasseaction");
    if (timeOperations.validOperation(valeur)) {
      actioncom.getEvenementContact().setOperationETTOTP(valeur);
      System.out.println("-temps-<>    " + actioncom.getEvenementContact().getETTOTP());
    }
    else {
      return false;
    }
    
    // Contr�le de la date de cr�ation
    valeur = request.getParameter("ac_dateCreation");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETDCR(indice);
    System.out.println("-dcr-<>    " + actioncom.getEvenementContact().getETDCR());
    
    // Contr�le de l'heure de cr�ation
    valeur = request.getParameter("ac_heureCreation");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETHCR(indice);
    System.out.println("-hcr-<>    " + actioncom.getEvenementContact().getETHCR());
    
    // Contr�le de la date de cloture
    valeur = request.getParameter("ac_dateLimite");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETDCL(indice);
    System.out.println("-dcl-<>    " + actioncom.getEvenementContact().getETDCL());
    
    // Contr�le de l'heure de cloture
    valeur = request.getParameter("ac_heureLimite");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETHCL(indice);
    System.out.println("-hcl-<>    " + actioncom.getEvenementContact().getETHCL());
    
    // Contr�le de la date de r�alisation
    valeur = request.getParameter("ac_dateRealisation");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETDRL(indice);
    System.out.println("-drl-<>    " + actioncom.getEvenementContact().getETDRL());
    
    // Contr�le de l'heure de r�alisation
    valeur = request.getParameter("ac_heureRealisation");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETHRL(indice);
    System.out.println("-hrl-<>    " + actioncom.getEvenementContact().getETHRL());
    
    // Contr�le de la date de rappel
    valeur = request.getParameter("ac_dateRappel");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETDRP(indice);
    System.out.println("-drp-<>    " + actioncom.getEvenementContact().getETDRP());
    
    // Contr�le de l'heure de r�alisation
    valeur = request.getParameter("ac_heureRappel");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1)
      return false;
    actioncom.getEvenementContact().setETHRP(indice);
    System.out.println("-hrp-<>    " + actioncom.getEvenementContact().getETHRP());
    
    return true;
  }
  
  /**
   * V�rifie que l'on est bien connect�
   * @param request
   * @param response
   * @return
   * @throws ServletException
   * @throws IOException
   */
  private boolean isConnected(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Object user = request.getSession().getAttribute("utilisateur");
    
    // Si on n'est pas connect�
    if ((user == null) || !(user instanceof Utilisateur)) {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      return false;
    }
    
    return true;
  }
  
  /**
   * Initialisation lors de la premi�re utilisation
   * @param request
   */
  private void initialization(HttpServletRequest request) {
    // On passe l'utilisateur � la JSP pour le look g�n�rique
    user = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    
    request.setAttribute("utilisateur", user.getLogin());
    request.setAttribute("bibli", user.getBibli());
    
    user.getGestionAC().setUtilisateur((Utilisateur) request.getSession().getAttribute("utilisateur"));
  }
  
  /**
   * Gestion des actions � ex�cuter
   * @param action2do
   * @param idActionCom
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void action2Do(String action2do, String idActionCom, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // System.out.println("--> action2Do:" + action2do);
    M_ActionCommerciale actioncom = null;
    if (idActionCom == null)
      idActionCom = "-1";
    int idAction = Integer.parseInt(idActionCom);
    actioncom = user.getGestionAC().getAcCourante(idAction);
    // if( receiveCookie(request, "/", "selectionContact") == null)
    // if( firstTimeInModif )
    // sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
    
    // LE ZERO c'est pour corriver vite fait le urlRetour qui renvoie un z�ro dans l'action2do
    if (action2do == null || action2do.equals("0")) {
      actioncom = user.getGestionAC().retournerUneActionCom(idAction);
      
      // On d�finit l'�tat du formulaire
      if (actioncom.getACID() == 0) {
        user.getFormulaireCourant().activeCreation();
        user.getGestionAC().setContactsLies(actioncom.getListContact(), "");
        // sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
      }
      else {
        user.getFormulaireCourant().activeConsultation();
        // sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
      }
      sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
      request.setAttribute("uneAction", actioncom);
      // Affichage du formulaire
      afficheDetailActionCom(request, response);
    }
    else
    // En fonction de l'action � effectuer
    if (action2do != null) {
      request.setAttribute("action2Do", action2do);
      switch (Integer.parseInt(action2do)) {
        
        case GestionFormulaire.CREATION:
          System.out.println("--> Formulaire Action en cr�ation");
          user.getFormulaireCourant().activeCreation();
          // Gestion des contacts s�lectionn�s
          user.getGestionAC().setContactsLies(actioncom.getListContact(), receiveCookie(request, "/", "selectionContact"));
          sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
          // On r�cup�re l'action commerciale souhait�e
          request.setAttribute("uneAction", actioncom);
          // Affichage formulaire
          afficheDetailActionCom(request, response);
          break;
        
        // Supprimer une action commerciale
        case GestionFormulaire.SUPPRESSION:
          user.getFormulaireCourant().activeConsultation();
          // Supprime l'action
          user.getGestionAC().supprimeActionCom(idAction);
          System.out.println("--> Action supprim�e " + idActionCom);
          // On r�cup�re la liste des actions commerciales
          request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
          // Affichage du formulaire
          afficheListeActionCom(request, response);
          
          break;
        
        // On annule une action commerciale (ne veux pas dire supprim�e, juste on annule les modif en cours)
        case GestionFormulaire.ANNULATION:
          user.getFormulaireCourant().activeConsultation();
          // Annulation de l'action
          System.out.println("--> Action annul�e ");
          sendCookie(response, "/", "selectionContact", "");
          
          // On r�cup�re la liste des actions commerciales
          request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
          // Affichage du formulaire
          afficheListeActionCom(request, response);
          
          break;
        
        // Afficher le formulaire du d�tail d'une action commerciale en modification
        case GestionFormulaire.MODIFICATION:
          System.out.println("--> Formulaire Action en modification");
          user.getFormulaireCourant().activeModification();
          // Gestion des contacts s�lectionn�s
          if (request.getParameter("annulModifsContact") != null) {
            sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
          }
          else
            user.getGestionAC().setContactsLies(actioncom.getListContact(), receiveCookie(request, "/", "selectionContact"));
          
          // On r�cup�re l'action commerciale souhait�e
          request.setAttribute("uneAction", actioncom);
          // Affichage formulaire
          afficheDetailActionCom(request, response);
          firstTimeInModif = false;
          break;
        
        // Ajout ou modification d'une action commerciale
        case GestionFormulaire.VALIDATION:
          if (isValidatedData(request, actioncom)) {
            System.out.println("Validation r�ussie !!");
            if (idAction == 0) { // Il s'agit d'un ajout
              if (!user.getGestionAC().insertActionCommerciale(actioncom)) {
                // System.out.println("Echec de l'insertion" + user.getGestionAC().getMsgError());
                // Affichage du formulaire
                afficheDetailActionCom(request, response);
              }
              else {
                user.getFormulaireCourant().activeConsultation();
                
                // On r�cup�re la liste des actions commerciales
                request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
                
                // Affichage de la liste des actions
                afficheListeActionCom(request, response);
              }
            }
            else { // Il s'agit d'une modification
              if (!user.getGestionAC().updateActionCommerciale(actioncom)) {
                System.out.println("Echec de la mise � jour" + user.getGestionAC().getMsgError());
              }
              else {
                sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
                user.getFormulaireCourant().activeConsultation();
              }
              // On r�cup�re l'action commerciale souhait�e
              request.setAttribute("uneAction", actioncom);
              
              // Affichage du formulaire
              afficheDetailActionCom(request, response);
            }
          }
          else {
            System.out.println("Echec de la validation!!");
            
          }
          break;
      }
    }
  }
  
  /**
   * Lecture des cookies sur le poste client
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private String receiveCookie(HttpServletRequest request, String path, String namecookie) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        // if( cookie.getPath() == null ) continue;
        // System.out.println("-receive-> name:" + cookie.getName() +"-"+ namecookie);
        if (cookie.getName().equals(namecookie)) { // && cookie.getPath().equals( path )){
          System.out.println("-receive-> namecookie:" + namecookie + " value:" + cookie.getValue());
          return cookie.getValue();
        }
      }
    }
    return "";
  }
  
  /**
   * On envoi un cookie au client
   * Pour le vider il suffit d'envoyer une chaine vide
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void sendCookie(HttpServletResponse response, String path, String namecookie, String valuecookie) {
    Cookie cookie = new Cookie(namecookie, valuecookie);
    // cookie.setPath(path);
    cookie.setValue(valuecookie);
    response.addCookie(cookie);
    System.out.println("-send->namecookie:" + namecookie + " value:" + valuecookie);
  }
  
  /**
   * Supprime un cookie
   * Pour le vider il suffit d'envoyer une chaine vide
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void deleteCookie(HttpServletResponse response, String path, String namecookie) {
    Cookie cookie = new Cookie(namecookie, "");
    cookie.setPath(path);
    cookie.setMaxAge(0);
    response.addCookie(cookie);
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des clients
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheListeClients(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("moteurRecherche", "ActionsCom");
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "1");
    request.getSession().setAttribute("filRouge", "1");
    
    // CSS sp�cifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // R�cup�ration du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/listeClientsCom.jsp").forward(request, response);
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des actions commerciales
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  // private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response, String client, String suffixe) throws
  // ServletException, IOException
  private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("mclient", user.getGestionAC().getClCourant());
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "1");
    request.getSession().setAttribute("filRouge", "1");
    
    // CSS sp�cifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // R�cup�ration du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/listeActionsCom.jsp").forward(request, response);
  }
  
  /**
   * Initialisations diverses et appel du formulaire d'une action commerciale
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheDetailActionCom(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // L'�tat du formulaire courant
    request.setAttribute("formulaire", user.getFormulaireCourant());
    
    // Si le formulaire est en consultation on vide le cookie selectionContact
    if (user.getFormulaireCourant().isConsultation()) {
      // sendCookie(response, "/", "selectionContact", null);
      deleteCookie(response, "/", "selectionContact");
    }
    // On cr�� un cookie avec l'url pour pouvoir revenir
    sendCookie(response, "/", "urlRetour", "ActionsCom?idaction=" + user.getGestionAC().getAcCourante().getACID() + "&action2do="
        + user.getFormulaireCourant().getModeFormulaire());
    
    // Niveau du fil rouge dans la navigation
    String optionsCRM = (String) request.getSession().getAttribute("optionsCRM");
    // String filrouge = (String) request.getSession().getAttribute("filRouge");
    // System.out.println("-ActionsCom->optionsCRM:" + optionsCRM + " filrouge:" + filrouge);
    if (optionsCRM.equals("1"))
      request.getSession().setAttribute("filRouge", "2");
    else if (optionsCRM.equals("2")) {
      request.getSession().setAttribute("optionsCRM", "2");
      request.getSession().setAttribute("filRouge", "2");
    }
    
    // CSS sp�cifique liste
    request.setAttribute("cssSpecifique", "css/fiches.css");
    // R�cup�ration du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/uneActionCom.jsp").forward(request, response);
  }
}
