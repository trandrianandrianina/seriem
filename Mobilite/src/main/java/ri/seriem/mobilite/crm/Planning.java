package ri.seriem.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.mobilite.Environnement.Utilisateur;
import ri.seriem.mobilite.metier.crm.LAC_ActionCommerciale;

/**
 * Servlet implementation class Planning
 */
public class Planning extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private LAC_ActionCommerciale gestionAC = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Planning() {
        super();
        gestionAC = new LAC_ActionCommerciale();
    }

    @Override
	public void init(ServletConfig config)
	{
		try
		{
			super.init(config);
		} 
		catch (Exception e) {e.printStackTrace();}
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if( !isConnected(request, response) ) return;
		initialization( request );

		gestionAC.setUtilisateur((Utilisateur)request.getSession().getAttribute("utilisateur"));
		
		//On r�cup�re la liste d'�v�nements
		request.setAttribute("listeActions", gestionAC.getListeActionsCommerciales(true) );
		afficheListeActionCom(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * V�rifie que l'on est bien connect�
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	private boolean isConnected(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException
	{
		Object user = request.getSession().getAttribute("utilisateur");
		
		// Si on n'est pas connect� 
		if( ( user == null) || !(user instanceof Utilisateur) )
		{
			request.getSession().invalidate();
			getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
			return false;
		}
			
		return true;
	}

	/**
	 * Initialisation lors de la premi�re utilisation
	 * @param request
	 */
	private void initialization(HttpServletRequest request)
	{
		// On passe l'utilisateur � la JSP pour le look g�n�rique
		request.setAttribute("utilisateur", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getLogin());
		request.setAttribute("bibli", ((Utilisateur)request.getSession().getAttribute("utilisateur")).getBibli());
		//Option CRM s�lectionn� par l'utilisateur
		//request.setAttribute("optionsCRM", "2");
	}

	/**
	 * Initialisations diverses et appel du formulaire de la liste des actions commerciales
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	//private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response, String client, String suffixe)  throws ServletException, IOException
	private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException
	{
		// Niveau du fil rouge dans la navigation
		request.getSession().setAttribute("optionsCRM", "2");
		request.getSession().setAttribute("filRouge", "1");
		
		// CSS sp�cifique liste
		request.setAttribute("cssSpecifique", "css/listes.css");
		// R�cup�ration du dispatcher + forward JSP
		request.getRequestDispatcher("WEB-INF/crm/planning.jsp").forward(request, response);
	}

}
