/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package outils;

public enum EnumProtocoleServeurMail {
  NON_DEFINI(-1, "", "Non d�fini"),
  SMTP_BASIC(0, "smtp", "smtp/basic"),
  SMTP_TLS(1, "smtp", "smtp/TLS"),
  SMTP_SSL(2, "smtp", "smtp/SSL"),
  SMTP_STARTTLS(3, "smtp", "smtp/STARTTLS");
  
  private final Integer numero;
  private final String codeProtocole;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumProtocoleServeurMail(Integer pNumero, String pCodeProtocole, String pLibelle) {
    numero = pNumero;
    codeProtocole = pCodeProtocole;
    libelle = pLibelle;
  }
  
  /**
   * Le num�ro sous lequel la valeur est persist�e en base de donn�es.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le code du protocole associ� au num�ro.
   */
  public String getCodeProtocole() {
    return codeProtocole;
  }
  
  /**
   * Le libell� associ� au num�ro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le num�ro associ� dans une cha�ne de caract�re.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet �num par son num�ro.
   */
  static public EnumProtocoleServeurMail valueOfByNumero(Integer pNumero) {
    for (EnumProtocoleServeurMail value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    return null;
    // throw new MessageErreurException("Le serveur du mail est invalide : " + pNumero);
  }
  
}
