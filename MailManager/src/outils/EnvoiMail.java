
package outils;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import manager.MailManager;
import metier.ConfigEnvoiMail;
import metier.Mail;

public class EnvoiMail {
  
  private ConfigEnvoiMail configEnvoi = null;
  
  public EnvoiMail(ConfigEnvoiMail pConfig) {
    configEnvoi = pConfig;
  }
  
  /**
   * Envoyer un mail avec une adresse mail le sujet et le contenu.
   */
  public boolean envoyerUnMail(Mail pMail, boolean isDebug) {
    if (configEnvoi == null || pMail == null || !pMail.isValide()) {
      return false;
    }
    
    boolean retour = false;
    try {
      Session mailSession = getInstanceSession(configEnvoi);
      mailSession.setDebug(isDebug);
      
      Message message = new MimeMessage(mailSession);
      InternetAddress recipientTo = new InternetAddress(pMail.getMailDestinataire());
      InternetAddress recipientFrom = new InternetAddress(configEnvoi.getUser());
      message.setRecipient(Message.RecipientType.TO, recipientTo);
      message.setFrom(recipientFrom);
      message.setSubject(pMail.getSujet());
      message.setContent(pMail.getCorpsMail(), "text/html");
      
      Transport.send(message);
      retour = true;
    }
    catch (NoSuchProviderException e) {
      pMail.setMessageErreur(e.getMessage());
    }
    catch (AddressException e) {
      pMail.setMessageErreur(e.getMessage());
    }
    catch (MessagingException e) {
      pMail.setMessageErreur(e.getMessage());
    }
    catch (Exception e) {
      pMail.setMessageErreur(e.getMessage());
    }
    
    return retour;
  }
  
  /**
   * Permet de tester la configuration d'envoi de mail pass�e en param�tre
   */
  public boolean testerLaConfiguration() {
    if (configEnvoi == null) {
      return false;
    }
    boolean retour = false;
    Transport transport = null;
    
    try {
      Session mailSession = getInstanceSession(configEnvoi);
      mailSession.setDebug(MailManager.ISDEBUG);
      
      transport = mailSession.getTransport();
      MimeMessage message = new MimeMessage(mailSession);
      InternetAddress recipientTo = new InternetAddress(MailManager.ADRESSE_TEST);
      InternetAddress recipientFrom = new InternetAddress(configEnvoi.getUser());
      message.setRecipient(Message.RecipientType.TO, recipientTo);
      message.setFrom(recipientFrom);
      message.setSubject("Test configuration d'envoi de mails " + configEnvoi.getEtb());
      message.setContent("La configuration d'envoi de mails du MailManager de " + configEnvoi.getBib() + " pour l'�tablissement "
          + configEnvoi.getEtb() + " est correcte.", "text/html");
      
      transport.connect();
      transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
      
      retour = true;
      System.out.println("[EnvoiMail] bib " + configEnvoi.getBib() + ": config de l'etb " + configEnvoi.getEtb() + " est ACTIVE ");
    }
    catch (NoSuchProviderException e) {
      System.out.println(
          "[EnvoiMail] bib " + configEnvoi.getBib() + ": config de l'etb " + configEnvoi.getEtb() + " en ECHEC " + e.getMessage());
    }
    catch (AddressException e) {
      System.out.println(
          "[EnvoiMail] bib " + configEnvoi.getBib() + ": config de l'etb " + configEnvoi.getEtb() + " en ECHEC " + e.getMessage());
    }
    catch (MessagingException e) {
      System.out.println(
          "[EnvoiMail] bib " + configEnvoi.getBib() + ": config de l'etb " + configEnvoi.getEtb() + " en ECHEC " + e.getMessage());
    }
    catch (Exception e) {
      System.out.println(
          "[EnvoiMail] bib " + configEnvoi.getBib() + ": config de l'etb " + configEnvoi.getEtb() + " en ECHEC " + e.getMessage());
    }
    
    if (transport != null) {
      try {
        transport.close();
      }
      catch (MessagingException e) {
        e.printStackTrace();
      }
    }
    
    configEnvoi.setActive(retour);
    return retour;
  }
  
  // -- M�thodes priv�es
  
  /**
   * Retourne une instance d'une session.
   */
  private Session getInstanceSession(final ConfigEnvoiMail pConfigEnvoi) {
    Properties props = new Properties();
    props.put("mail.transport.protocol", pConfigEnvoi.getProtocole().getCodeProtocole());
    props.put("mail.smtp.host", pConfigEnvoi.getHost());
    props.put("mail.smtp.port", pConfigEnvoi.getPort());
    props.put("mail.smtp.auth", "true");
    
    switch (pConfigEnvoi.getProtocole()) {
      // Le port est 25
      case SMTP_BASIC:
        props.put("mail.smtp.starttls.enable", "false");
        break;
      // Le port est 587 ou 25
      case SMTP_TLS:
        props.put("mail.smtp.starttls.enable", "true");
        break;
      // Le port est 465
      case SMTP_SSL:
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.socketFactory.port", pConfigEnvoi.getPort());
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        break;
      // Le port est 587 (Note: pour Office365, il faut utiliser comme smtp-mail.outlook.com)
      case SMTP_STARTTLS:
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        break;
      
      default:
        // Trace.alerte("Le protocole " + pConfigEnvoi.getProtocole().getLibelle() + " n'est pas trait�.");
        System.out.println("Le protocole " + pConfigEnvoi.getProtocole().getLibelle() + " n'est pas trait�.");
        break;
    }
    
    Authenticator authenticator = new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(pConfigEnvoi.getUser(), pConfigEnvoi.getPassword());
      }
    };
    
    return Session.getInstance(props, authenticator);
  }
}
