
package metier;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.net.ntp.TimeStamp;

import database.record.GenericRecord;

public class Mail {
  
  private int id = -1;
  private String etablissement = null;
  private TimeStamp dateCreation = null;
  private TimeStamp dateTraitement = null;
  private int type = -1;
  private String idDestinataire = null;
  private String nomDestinataire = null;
  private String mailDestinataire = null;
  private String idDocument = null;
  private int statut = -1;
  private String sujet = null;
  private String corpsMail = null;
  private HashMap<String, String> listeVariables = null;
  private String messageErreur = "";
  
  // Statiques
  // Statut � traiter
  public static int STT_A_ENVOYER = 0;
  public static int STT_ENVOYE = 1;
  public static int STT_ER_TRAIT = 2;
  public static int STT_ER_ENVOI = 3;
  public static int STT_ER_BDD = 4;
  public static int STT_ATTENTE = 5;
  
  public Mail(GenericRecord pRecord) {
    if (pRecord == null) {
      return;
    }
    
    initVariables();
    
    if (pRecord.isPresentField("MAID")) {
      try {
        id = Integer.parseInt(pRecord.getField("MAID").toString().trim());
      }
      catch (Exception e) {
      }
    }
    
    if (pRecord.isPresentField("MAETB")) {
      etablissement = pRecord.getField("MAETB").toString().trim();
    }
    
    if (pRecord.isPresentField("MATYPE")) {
      try {
        type = Integer.parseInt(pRecord.getField("MATYPE").toString().trim());
      }
      catch (Exception e) {
        System.out.println("Erreur lors de l'initialisation du type du mail : " + e);
      }
    }
    
    if (pRecord.isPresentField("MACDST")) {
      idDestinataire = pRecord.getField("MACDST").toString().trim();
    }
    
    if (pRecord.isPresentField("MANDST")) {
      nomDestinataire = pRecord.getField("MANDST").toString().trim();
    }
    
    if (pRecord.isPresentField("MAMDST")) {
      mailDestinataire = pRecord.getField("MAMDST").toString().trim();
    }
    
    if (pRecord.isPresentField("MADOCU")) {
      idDocument = pRecord.getField("MADOCU").toString().trim();
      listeVariables.put("$$numeroDocument$$", idDocument);
    }
    
    if (pRecord.isPresentField("MASTAT")) {
      try {
        statut = Integer.parseInt(pRecord.getField("MASTAT").toString().trim());
      }
      catch (Exception e) {
      }
    }
    
    if (pRecord.isPresentField("MASUJT")) {
      sujet = pRecord.getField("MASUJT").toString().trim();
      listeVariables.put("$$sujet$$", sujet);
    }
    
    if (pRecord.isPresentField("MACRPS")) {
      corpsMail = pRecord.getField("MACRPS").toString().trim();
      listeVariables.put("$$texte$$", corpsMail);
    }
    
    if (pRecord.isPresentField("MAMSG")) {
      messageErreur = pRecord.getField("MAMSG").toString().trim();
    }
  }
  
  /**
   * On initialise la liste totale des variables � transmettre dans le mail
   */
  private void initVariables() {
    listeVariables = new HashMap<String, String>();
  }
  
  /**
   * On met � jour le sujet du mail sur la base du sujet de son type de mail
   * apr�s avoir effectu� un traitement sur les variables qui lui sont
   * propres.
   */
  public void majDuSujet(String pPattern) {
    sujet = traitementVariablesContenu(pPattern);
  }
  
  /**
   * On met � jour le corps du mail sur la base du corps de son type de mail
   * apr�s avoir effectu� un traitement sur les variables qui lui sont
   * propres.
   */
  public void majDuContenu(String pPattern) {
    corpsMail = traitementVariablesContenu(pPattern);
  }
  
  /**
   * On remplace les balises variables par les bonnes valeurs dans le contenu d'un mail.
   */
  private String traitementVariablesContenu(String pTexteBrut) {
    if (pTexteBrut == null || listeVariables == null) {
      return null;
    }
    
    for (Map.Entry<String, String> variable : listeVariables.entrySet()) {
      if (variable.getValue() != null) {
        pTexteBrut = pTexteBrut.replace(variable.getKey(), variable.getValue());
      }
    }
    
    return pTexteBrut;
  }
  
  /**
   * On v�rifie que le mail contienne les donn�es essentielles � son envoi
   */
  public boolean isValide() {
    if (etablissement == null) {
      return false;
    }
    
    if (mailDestinataire == null || mailDestinataire.trim().equals("")) {
      return false;
    }
    
    if (sujet == null || sujet.trim().equals("")) {
      return false;
    }
    
    if (corpsMail == null || corpsMail.trim().equals("")) {
      return false;
    }
    
    return true;
  }
  
  // ++++++++++++++++ ACCESSEURS +++++++++++++++++ //
  
  public int getId() {
    return id;
  }
  
  public void setId(int id) {
    this.id = id;
  }
  
  public String getEtablissement() {
    return etablissement;
  }
  
  public void setEtablissement(String etablissement) {
    this.etablissement = etablissement;
  }
  
  public TimeStamp getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(TimeStamp dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public TimeStamp getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(TimeStamp dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public String getIdDestinataire() {
    return idDestinataire;
  }
  
  public void setIdDestinataire(String idDestinataire) {
    this.idDestinataire = idDestinataire;
  }
  
  public String getNomDestinataire() {
    return nomDestinataire;
  }
  
  public void setNomDestinataire(String nomDestinataire) {
    this.nomDestinataire = nomDestinataire;
  }
  
  public String getMailDestinataire() {
    return mailDestinataire;
  }
  
  public void setMailDestinataire(String mailDestinataire) {
    this.mailDestinataire = mailDestinataire;
  }
  
  public String getIdDocument() {
    return idDocument;
  }
  
  public void setIdDocument(String idDocument) {
    this.idDocument = idDocument;
  }
  
  public int getStatut() {
    return statut;
  }
  
  public void setStatut(int statut) {
    this.statut = statut;
  }
  
  public String getCorpsMail() {
    return corpsMail;
  }
  
  public void setCorpsMail(String corpsMail) {
    this.corpsMail = corpsMail;
  }
  
  public String getSujet() {
    return sujet;
  }
  
  public int getType() {
    return type;
  }
  
  public String getMessageErreur() {
    return messageErreur;
  }
  
  public void setMessageErreur(String messageErreur) {
    if (messageErreur == null) {
      return;
    }
    System.out.println(messageErreur);
    this.messageErreur += messageErreur;
  }
  
}
