package metier;

import database.record.GenericRecord;

/**
 * Type de mail � envoyer � un destinataire avec son sujet et son contenu
 */
public class TypeMail {
	public static final int TYPE_CDES_WEB_DISPOS = 1;
	public static final int TYPE_CDES_WEB_EXPE = 2;
	public static final int TYPE_MAIL_TEXTE = 11;

	private int id = -1;
	private String etablissement = null;
	private String libelle = null;
	private boolean isActif = false;
	private String sujet = null;
	private String corpsMessage = null;

	/**
	 * Constructeur.
	 * 
	 * @param pRecord
	 */
	public TypeMail(GenericRecord pRecord) {
		if (pRecord == null) {
			return;
		}

		if (pRecord.isPresentField("TYID")) {
			try {
				id = Integer.parseInt(pRecord.getField("TYID").toString().trim());
			} catch (Exception e) {
			}
		}

		if (pRecord.isPresentField("TYETB")) {
			etablissement = pRecord.getField("TYETB").toString().trim();
		}

		if (pRecord.isPresentField("TYLIBL")) {
			libelle = pRecord.getField("TYLIBL").toString().trim();
		}

		if (pRecord.isPresentField("TYACTI")) {
			isActif = pRecord.getField("TYACTI").toString().trim().equals("1");
		}

		if (pRecord.isPresentField("TYSUJT")) {
			sujet = pRecord.getField("TYSUJT").toString().trim();
		}

		if (pRecord.isPresentField("TYCORP")) {
			corpsMessage = pRecord.getField("TYCORP").toString().trim();
		}
	}

	// -- Accesseurs

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(String etablissement) {
		this.etablissement = etablissement;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public boolean isActif() {
		return isActif;
	}

	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public String getCorpsMessage() {
		return corpsMessage;
	}

	public void setCorpsMessage(String corpsMessage) {
		this.corpsMessage = corpsMessage;
	}
}
