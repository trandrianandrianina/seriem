
package metier;

import database.record.GenericRecord;
import outils.EnumProtocoleServeurMail;

/**
 * Classe de configuration d'envoi de mails
 */
public class ConfigEnvoiMail {
  private int id = -1;
  private String bib = null;
  private String etb = null;
  private String description = null;
  private EnumProtocoleServeurMail protocole = null;
  private String host = null;
  private String port = null;
  private String user = null;
  private String password = null;
  private boolean isActive = false;
  
  /**
   * Constructeur.
   * @param pBib
   * @param pRecord
   */
  public ConfigEnvoiMail(String pBib, GenericRecord pRecord) {
    if (pRecord == null || pBib == null) {
      return;
    }
    
    bib = pBib;
    
    if (pRecord.isPresentField("SMID")) {
      try {
        id = Integer.parseInt(pRecord.getField("SMID").toString().trim());
      }
      catch (Exception e) {
      }
    }
    
    if (pRecord.isPresentField("SMETB")) {
      etb = pRecord.getField("SMETB").toString().trim();
    }
    
    if (pRecord.isPresentField("SMDESC")) {
      description = pRecord.getField("SMDESC").toString().trim();
    }
    
    if (pRecord.isPresentField("SMPROT")) {
      Integer numeroProtocole = Integer.parseInt(pRecord.getField("SMPROT").toString().trim());
      protocole = EnumProtocoleServeurMail.valueOfByNumero(numeroProtocole.intValue());
    }
    
    if (pRecord.isPresentField("SMHOST")) {
      host = pRecord.getField("SMHOST").toString().trim();
    }
    
    if (pRecord.isPresentField("SMPORT")) {
      port = pRecord.getField("SMPORT").toString().trim();
    }
    
    if (pRecord.isPresentField("SMUSER")) {
      user = pRecord.getField("SMUSER").toString().trim();
    }
    
    if (pRecord.isPresentField("SMPASS")) {
      password = pRecord.getField("SMPASS").toString().trim();
    }
  }
  
  // -- Accesseurs
  
  public int getId() {
    return id;
  }
  
  public String getEtb() {
    return etb;
  }
  
  public String getDescription() {
    return description;
  }
  
  public EnumProtocoleServeurMail getProtocole() {
    return protocole;
  }
  
  public String getHost() {
    return host;
  }
  
  public String getPort() {
    return port;
  }
  
  public String getUser() {
    return user;
  }
  
  public String getPassword() {
    return password;
  }
  
  public boolean isActive() {
    return isActive;
  }
  
  public void setActive(boolean act) {
    isActive = act;
  }
  
  public String getBib() {
    return bib;
  }
  
}
