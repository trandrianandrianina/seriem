
package manager;

import java.util.ArrayList;
import java.util.HashMap;

import database.record.GenericRecord;
import metier.ConfigEnvoiMail;
import metier.Mail;
import metier.TypeMail;
import outils.EnvoiMail;

/**
 * Chaque instance de cette classe scanne les ordres de mails de la table de
 * mails de la biblioth�que concern�e
 */
public class BibliMailManager extends Thread {
  public static final String TABLE_MANAGER_MAIL = "PSEMMAILMM";
  public static final String TABLE_TYPE_MAIL = "PSEMTYPMAM";
  public static final String TABLE_CONFIG_ENVOI_MAIL = "PSEMSMM";
  public static final String TABLE_STATUTS_MAIL = "PSEMSTTMAM";
  
  private String bibli = null;
  private boolean isActif = true;
  private HashMap<String, ConfigEnvoiMail> listeConfigsEnvoi = null;
  
  /**
   * Constructeur.
   * @param pBib
   */
  public BibliMailManager(String pBib) {
    bibli = pBib;
    
    setName("BibliMailManager env :" + MailManager.LETTREENV + " bibli : " + pBib);
    
    isActif = initConfigsMail();
    if (isActif) {
      initialisationMailsEnAttente();
    }
  }
  
  /**
   * On remet tous les mails en attente en statut � traiter.
   */
  private void initialisationMailsEnAttente() {
    int retour = MailManager.accesDb2.requete("UPDATE " + bibli + "." + TABLE_MANAGER_MAIL + " SET MASTAT = '" + Mail.STT_A_ENVOYER
        + "', MAMSG = '' WHERE MASTAT = '" + Mail.STT_ATTENTE + "' ");
    if (retour < 0) {
      System.out.println("[BibliMailManager] " + bibli + ": Erreur de mise � jour en BDD des mails en attente ");
    }
  }
  
  /**
   * Traiter tous les mails � envoyer.
   */
  private void traiterTousLesMails() {
    // On scanne les mails concern�s
    ArrayList<Mail> listeMails = recupererMailsAtraiter();
    if (listeMails != null) {
      for (Mail mail : listeMails) {
        System.out.println("[BibliMailManager] " + bibli + "/" + mail.getEtablissement() + " On traite le mail N� " + mail.getId());
        // On met � jour les informations n�cessaires dans le mail
        if (recupererLesInfosParTypedeMail(mail)) {
          // On envoie les mails
          if (envoyerUnMail(mail)) {
            mail.setStatut(Mail.STT_ENVOYE);
            mail.setMessageErreur(bibli + "/" + mail.getEtablissement() + " Mail N� " + mail.getId() + " envoy� avec succ�s");
          }
          else {
            if (mail.getStatut() != Mail.STT_ATTENTE) {
              mail.setStatut(Mail.STT_ER_ENVOI);
              mail.setMessageErreur(bibli + " Erreur d'envoi du mail N� " + mail.getId());
            }
          }
        }
        else {
          mail.setStatut(Mail.STT_ER_TRAIT);
          mail.setMessageErreur(bibli + " Erreur de traitement mail N� " + mail.getId());
        }
        // Mise � jour la table de mail avec le retour d'envoi
        majMailBdd(mail);
      }
    }
  }
  
  /**
   * On r�cup�re les mails � envoyer dans la table correspondante.
   */
  private ArrayList<Mail> recupererMailsAtraiter() {
    ArrayList<Mail> listeMails = null;
    // On scanne
    ArrayList<GenericRecord> listeRecords =
        MailManager.accesDb2.select("SELECT MAID, MAETB, MATYPE, MACDST, MANDST, MAMDST, MADOCU, MASTAT, MASUJT, MACRPS FROM " + bibli
            + "." + TABLE_MANAGER_MAIL + " WHERE MASTAT = '" + Mail.STT_A_ENVOYER + "' ORDER BY MAETB, MADTCR");
    
    // Contr�le erreur
    if (listeRecords == null) {
      System.out.println("[BibliMailManager] " + bibli + ": Erreur de r�cup�ration des mails � traiter ");
      return null;
    }
    
    // Si des mails � envoyer ont �t� trouv�
    if (!listeRecords.isEmpty()) {
      listeMails = new ArrayList<Mail>();
      for (GenericRecord record : listeRecords) {
        Mail mail = new Mail(record);
        listeMails.add(mail);
      }
    }
    
    return listeMails;
  }
  
  /**
   * R�cup�rer les informations du mail sur la base du type de mail attendu.
   */
  private boolean recupererLesInfosParTypedeMail(Mail pMail) {
    if (pMail == null || pMail.getType() <= 0) {
      pMail.setMessageErreur("recupererLesInfosParTypedeMail() objets � Null");
      return false;
    }
    System.out.println("[BibliMailManager] " + bibli + ": RecupererLesInfosParTypedeMail() du mail: " + pMail.getId());
    
    // On v�rifie le num�ro du document et on r�cup�re le destinataire correspondant
    if (!majDestinataire(pMail)) {
      System.out.println("[BibliMailManager] " + bibli + ": Pas de destinataire recup�r� pour le mail " + pMail.getId());
      return false;
    }
    
    // On r�cup�re le type de mail attendu et on incruste le corps du mail
    String requete = "SELECT * FROM " + bibli + "." + TABLE_TYPE_MAIL + " WHERE TYETB = '" + pMail.getEtablissement() + "' AND TYID = '"
        + pMail.getType() + "'  AND TYACTI = '1' ";
    System.out.println(requete);
    ArrayList<GenericRecord> listeRecords = MailManager.accesDb2.select(requete);
    if (listeRecords != null && listeRecords.size() == 1) {
      TypeMail typeMail = new TypeMail(listeRecords.get(0));
      // Est ce un mail pour le WebShop ?
      boolean webshop = false;
      if (typeMail.getId() == TypeMail.TYPE_CDES_WEB_DISPOS || typeMail.getId() == TypeMail.TYPE_CDES_WEB_EXPE) {
        webshop = true;
      }
      
      // Construit le sujet
      if (typeMail.getSujet() != null) {
        pMail.majDuSujet(typeMail.getSujet());
      }
      // Construit le corps du message
      if (typeMail.getCorpsMessage() != null) {
        String corps = typeMail.getCorpsMessage();
        // Pour le WebShop triatement sp�cial pour le HTML
        if (webshop) {
          corps = PatternMailHTML.retournerPatternMailHTML(corps);
        }
        pMail.majDuContenu(corps);
      }
      
      // On remplace les variables de type sp�cifique
      majInfosSpecifiques(pMail);
      
      return true;
    }
    
    // D�finit le type d'erreur
    if (listeRecords == null) {
      pMail.setMessageErreur("Erreur de r�cup�ration du type de mail.");
    }
    else {
      pMail.setMessageErreur("Le mail ne sera pas envoy� car le type de mail est d�sactiv�.");
    }
    return false;
  }
  
  /**
   * Mettre � jour les destinataires d'un mail.
   */
  private boolean majDestinataire(Mail pMail) {
    if (pMail == null || pMail.getType() <= 0 || pMail.getEtablissement() == null) {
      pMail.setMessageErreur("majDestinataire() objets � Null");
      return false;
    }
    
    boolean retour = false;
    
    // Contr�le si le mode test du mail manager est activ�
    if (MailManager.ISMODETEST) {
      if (MailManager.ADRESSE_TEST == null) {
        pMail.setMessageErreur("L'adresse mail de test est invalide.");
        return false;
      }
      pMail.setIdDestinataire("001");
      pMail.setNomDestinataire("Compte test");
      pMail.setMailDestinataire(MailManager.ADRESSE_TEST);
      return true;
    }
    
    // Si le type mail n'est pas pour le WebShop alors le destinataire du mail doit �tre stock� dans le champ MAMDST de la table
    // PSEMMAILMM
    System.out.println("Mail " + pMail.getId() + " de type " + pMail.getType());
    if (pMail.getType() != TypeMail.TYPE_CDES_WEB_DISPOS && pMail.getType() != TypeMail.TYPE_CDES_WEB_EXPE
        && pMail.getMailDestinataire() != null && !pMail.getMailDestinataire().isEmpty()) {
      return true;
    }
    
    // Pour les types de mail web
    if (pMail.getIdDocument() == null) {
      pMail.setMessageErreur("L'identifiant du document de ventes est invalide.");
      return false;
    }
    String numeroCde = null;
    String suffixeCde = null;
    String[] tabCde = decouperIdCommande(pMail.getIdDocument().trim());
    if (tabCde != null && tabCde.length == 2) {
      numeroCde = tabCde[0];
      suffixeCde = tabCde[1];
    }
    
    if (numeroCde == null || suffixeCde == null) {
      pMail.setMessageErreur(
          "La commande ne correspond pas � la structure attendue: numeroCde:" + numeroCde + " / suffixeCde:" + suffixeCde);
      return retour;
    }
    
    // Construction de la requ�te afin de r�cup�rer le destinataire du mail
    String requete = null;
    switch (pMail.getType()) {
      case TypeMail.TYPE_CDES_WEB_DISPOS:
        requete = "SELECT RENUM AS IDDEST, RECIV, REPAC AS NOMDEST, RENET AS MAILDEST FROM " + bibli + ".PGVMEBCM LEFT JOIN " + bibli
            + ".PGVMXLIM ON XIETB = E1ETB AND XICOD = E1COD AND XINUM = E1NUM AND XISUF = E1SUF LEFT JOIN " + bibli
            + ".PSEMRTEM ON RENUM = XILIB WHERE E1COD = 'E' AND E1IN18 = 'W' AND XITYP = '72' AND E1ETB = '" + pMail.getEtablissement()
            + "' AND E1NUM = '" + numeroCde + "' AND E1SUF = '" + suffixeCde + "' ";
        break;
      case TypeMail.TYPE_CDES_WEB_EXPE:
        requete = "SELECT RENUM AS IDDEST, RECIV, REPAC AS NOMDEST, RENET AS MAILDEST FROM " + bibli + ".PGVMEBCM LEFT JOIN " + bibli
            + ".PGVMXLIM ON XIETB = E1ETB AND XICOD = E1COD AND XINUM = E1NUM AND XISUF = E1SUF LEFT JOIN " + bibli
            + ".PSEMRTEM ON RENUM = XILIB WHERE E1COD = 'E' AND E1IN18 = 'W' AND XITYP = '72' AND E1ETB = '" + pMail.getEtablissement()
            + "' AND E1NUM = '" + numeroCde + "' AND E1SUF = '" + suffixeCde + "' ";
        break;
      
      default:
        requete = null;
        pMail.setMessageErreur("Ce type de mail n'existe pas:" + pMail.getType());
        return retour;
    }
    
    // Pour les commandes web, r�cup�ration du destinataire du mail
    ArrayList<GenericRecord> listeRecords = MailManager.accesDb2.select(requete);
    if (listeRecords != null && listeRecords.size() == 1) {
      if (listeRecords.get(0).isPresentField("IDDEST")) {
        pMail.setIdDestinataire(listeRecords.get(0).getField("IDDEST").toString().trim());
      }
      if (listeRecords.get(0).isPresentField("NOMDEST")) {
        pMail.setNomDestinataire(listeRecords.get(0).getField("NOMDEST").toString().trim());
      }
      if (listeRecords.get(0).isPresentField("MAILDEST") && !listeRecords.get(0).getField("MAILDEST").toString().trim().equals("")) {
        pMail.setMailDestinataire(listeRecords.get(0).getField("MAILDEST").toString().trim());
      }
      else {
        pMail.setMessageErreur("Erreur de r�cup�ration de l'adresse mail du destinataire pour la commande " + pMail.getIdDocument());
        return false;
      }
      
      retour = true;
    }
    else {
      pMail.setMessageErreur("Erreur de r�cup�ration du contact de la commande " + pMail.getIdDocument());
      return false;
    }
    
    return retour;
  }
  
  /**
   * Mise � jour des champs variabilis�s en fonction du type du mail et si n�cessaire.
   * @param pMail
   */
  private void majInfosSpecifiques(Mail pMail) {
    if (pMail == null || pMail.getType() <= 0 || pMail.getEtablissement() == null) {
      pMail.setMessageErreur("majInfosSpecifiques() pMail corrompu");
      return;
    }
    // Pour les mails de types "Commande web"
    if (pMail.getType() == TypeMail.TYPE_CDES_WEB_DISPOS || pMail.getType() == TypeMail.TYPE_CDES_WEB_EXPE) {
      if (pMail.getIdDocument() == null || pMail.getCorpsMail() == null) {
        pMail.setMessageErreur("majInfosSpecifiques() pMail corrompu car l'identifiant du document de ventes est invalide.");
      }
      TypeMailCommandeWeb.majContenu(pMail, bibli);
    }
  }
  
  /**
   * D�couper le num�ro de document du mail en commande et suffixe de commande.
   */
  private String[] decouperIdCommande(String pCde) {
    if (pCde == null || pCde.length() < 2 || pCde.length() > 7) {
      return null;
    }
    
    String[] tabRetour = new String[2];
    tabRetour[0] = pCde.substring(0, pCde.length() - 1);
    tabRetour[1] = pCde.substring(pCde.length() - 1, pCde.length());
    
    return tabRetour;
  }
  
  /**
   * Initialiser les liste des configuration de la biblioth�que du Manager
   */
  private boolean initConfigsMail() {
    if (bibli == null) {
      System.out.println("[BibliMailManager] La biblioth�que du Manager est nulle " + bibli + " initConfigsMail()");
      return false;
    }
    
    boolean retour = false;
    
    // On r�cup�re la liste d'�tablissement
    ArrayList<GenericRecord> listeRecords = MailManager.accesDb2.select(" SELECT * FROM " + bibli + "." + TABLE_CONFIG_ENVOI_MAIL);
    if (listeRecords != null && listeRecords.size() > 0) {
      listeConfigsEnvoi = new HashMap<String, ConfigEnvoiMail>();
      
      for (GenericRecord record : listeRecords) {
        if (record.isPresentField("SMETB")) {
          ConfigEnvoiMail config = new ConfigEnvoiMail(bibli, record);
          testerUneConfigEnvoi(config);
          
          listeConfigsEnvoi.put(record.getField("SMETB").toString().trim(), config);
          System.out.println("[BibliMailManager] " + bibli + ": On rajoute une configuration d'envoi de mails pour l'etb: "
              + record.getField("SMETB").toString().trim());
        }
      }
      retour = true;
    }
    else {
      System.out.println("[BibliMailManager] " + bibli + ": Aucune configuration d'envoi de mail n'est d�clar�e");
    }
    
    return retour;
  }
  
  /**
   * Envoyer le mail pass� en param�tre avec la configuration attribu�e � son
   * �tablissement
   */
  private boolean envoyerUnMail(Mail pMail) {
    boolean isOk = false;
    if (pMail == null || pMail.getEtablissement() == null) {
      pMail.setMessageErreur("envoyerUnMail() Objets � Null");
      return false;
    }
    
    ConfigEnvoiMail config = listeConfigsEnvoi.get(pMail.getEtablissement());
    if (config != null) {
      if (config.isActive()) {
        EnvoiMail envoiMail = new EnvoiMail(config);
        isOk = envoiMail.envoyerUnMail(pMail, MailManager.ISDEBUG);
      }
      else {
        pMail.setStatut(Mail.STT_ATTENTE);
        pMail.setMessageErreur("La configuration d'envoi de mail de l' �tablissement " + pMail.getEtablissement()
            + " n'est pas active : On met en attente le mail.");
      }
    }
    else {
      pMail.setMessageErreur("Aucune configuration d'envoi de mail n'est associ�e � cet �tablissement: " + pMail.getEtablissement());
    }
    
    return isOk;
  }
  
  /**
   * On met � jour la base de donn�es apr�s le traitement et l'envoi d'un mail
   */
  private void majMailBdd(Mail pMail) {
    int retour = MailManager.accesDb2.requete(" UPDATE " + bibli + "." + TABLE_MANAGER_MAIL + " SET MADTMD = CURRENT TIMESTAMP,"
        + " MACDST = '" + pMail.getIdDestinataire() + "'," + " MANDST = '" + traiterCaracteresSpeciauxSQL(pMail.getNomDestinataire())
        + "'," + " MAMDST = '" + pMail.getMailDestinataire() + "'," + " MASTAT = '" + pMail.getStatut() + "'," + " MASUJT = '"
        + traiterCaracteresSpeciauxSQL(pMail.getSujet()) + "'," + " MACRPS = '" + traiterCaracteresSpeciauxSQL(pMail.getCorpsMail())
        + "'," + " MAMSG = '" + traiterCaracteresSpeciauxSQL(pMail.getMessageErreur()) + "' " + " WHERE MAID = '" + pMail.getId() + "' ");
    if (retour != 1) {
      pMail.setStatut(Mail.STT_ER_BDD);
      pMail.setMessageErreur("[BibliMailManager] " + bibli + ": Erreur de mise � jour en BDD du mail N� " + pMail.getId());
      retour = MailManager.accesDb2.requete(" UPDATE " + bibli + "." + TABLE_MANAGER_MAIL + " SET MASTAT = '" + pMail.getStatut() + "' "
          + " WHERE MAID = '" + pMail.getId() + "' ");
      if (retour != 1) {
        System.out.println("[BibliMailManager] " + bibli + ": Erreur Niveau 2 de mise � jour en BDD du mail N� " + pMail.getId());
      }
    }
  }
  
  /**
   * Echapper les caract�res sp�ciaux
   */
  public static String traiterCaracteresSpeciauxSQL(String chaine) {
    if (chaine == null)
      return null;
    
    return chaine.replace("'", "''");
  }
  
  /**
   * Tester si une configuration d'envoi est active en envoyant un mail test
   */
  private void testerUneConfigEnvoi(ConfigEnvoiMail pConfig) {
    EnvoiMail envoiMail = new EnvoiMail(pConfig);
    envoiMail.testerLaConfiguration();
  }
  
  @Override
  public void run() {
    super.run();
    
    while (isActif) {
      try {
        Thread.sleep(MailManager.FREQUENCE_SCAN_BIBS);
        traiterTousLesMails();
      }
      catch (InterruptedException ex) {
      }
    }
    
    if (!isActif && !isInterrupted()) {
      interrupt();
    }
  }
  
  @Override
  public void start() {
    System.out.println("La surveillance de la bibli " + bibli + " d�marre");
    super.start();
  }
  
  @Override
  public void interrupt() {
    System.out.println("La surveillance de la bibli " + bibli + " s'arr�te");
    isActif = false;
    super.interrupt();
  }
  
  public boolean isActif() {
    return isActif;
  }
}
