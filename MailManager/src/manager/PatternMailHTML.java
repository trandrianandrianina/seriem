
package manager;

import java.text.DateFormat;
import java.util.Date;

public class PatternMailHTML {
  
  private static DateFormat mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
  private static String contenuFichierCss = null;
  
  public static String retournerPatternMailHTML(String texteContenu) {
    StringBuilder texteClasse = new StringBuilder(20000);
    String imageMail = "";
    if (MailManager.URL_PHOTO_MAILS != null && !MailManager.URL_PHOTO_MAILS.trim().isEmpty()) {
      imageMail = "<img src='" + MailManager.URL_PHOTO_MAILS + "'/>";
    }
    
    texteClasse.append("<!DOCTYPE html>" + "<html lang='fr'>" + "<head>" + retournerCSSMail() + "</head>" + "<body>"
        + "<table id='presMail'>" + "<tr>" + "<td class='paddingHorizontal'></td>" + "<td id='serveurEmetteur'>"
        + MailManager.NOM_COMMERCIAL_SERVEUR + "</td>" + "<td id='imageDecoration'>" + imageMail + "</td>"
        + "<td id='WebShop'><span id='webMail'>Web</span><br/><span id='shopMail'>SHOP</span></td>"
        + "<td class='paddingHorizontal'></td>" + "</tr>" + "<tr >" + "<td  colspan='5' id='paddingTete'></td>" + "</tr>" + "</table>"
        + texteContenu + "<table>" + "<tr><td  colspan='5' class='paddingVertical'></td></tr>"
        + "<tr><td  colspan='5' class='paddingVertical'></td></tr>" + "</table>" + "<table id='piedDeMail'>"
        + "<tr ><td  colspan='5' class='paddingVertical'></td></tr>"
        + "<tr ><td class='paddingHorizontal'></td><td  colspan='3' class='separationMail'></td><td class='paddingHorizontal'></td></tr>"
        + "<tr ><td  colspan='5' class='paddingVertical'></td></tr>" + "<tr >" + "<td class='paddingHorizontal'></td>"
        + "<td  colspan='3' class='messagePiedMail' id='messNoReturn' >Merci de ne pas r�pondre � ce mail : il est g�n�r� automatiquement par notre site.</td>"
        + "<td class='paddingHorizontal'></td>" + "</tr>" + "<tr >" + "<td class='paddingHorizontal'></td>"
        + "<td  colspan='3' class='messagePiedMail' id='dateMail' >Email envoy� le " + mediumDateFormat.format(new Date()) + "</td>"
        + "<td class='paddingHorizontal'></td>" + "</tr>" + "<tr >" + "<td class='paddingHorizontal'></td>" + "</tr>" + "<tr >"
        + "<td  colspan='5' class='paddingVertical'></td>" + "</tr>" + "</table>" + "</body>" + "</html>");
    
    return texteClasse.toString();
  }
  
  /**
   * On r�cup�re le css des mails.
   */
  private static String retournerCSSMail() {
    if (contenuFichierCss == null) {
      contenuFichierCss = "" + "*{margin:0; padding:0;}"
          + "body{border:0;color:#7C7C7C;font-size:13px;font-family: sans-serif;background:#f1ede9;padding-top:30px;width:100%;text-align:center;}"
          + "table{width:80%;background-color:white;border-left:1px solid #7C7C7C;border-right:1px solid #7C7C7C;margin:auto;text-align:left;border-collapse: collapse;}"
          + "#presMail{border-top:1px solid #7C7C7C;}" + "#piedDeMail{border-bottom:1px solid #7C7C7C;}"
          + "#serveurEmetteur{width:31%;height:70px;font-size:22px;vertical-align: bottom;}" + "#imageDecoration{width:48%;height:70px;}"
          + "#WebShop{width:17%;height:70px;text-align:right;vertical-align: bottom;}"
          + "#webMail{font-size:29px;color:#3581BA;font-weight:bold;}" + "#shopMail{font-size:34px;color:#7C7C7C;font-weight:bold;}"
          + "td{font-size:12px;}" + ".messagePiedMail{width:96%;height:20px;max-height:20px;}"
          + ".separationMail{height:8px;max-height:8px;background-color:#7C7C7C;}" + ".paddingHorizontal{width:2%;min-width:2%;}"
          + ".paddingVertical{height:20px;max-height:20px;}" + "#paddingTete{height:6px;max-height:6px;}"
          + "#messNoReturn{font-weight:bold;}"
          + ".h2Mail{background-color:#3581BA;height:28px;color:white;font-size:16px;font-weight:bold;text-align:center;}"
          + ".messageDeConf{font-size:15px;font-weight:bold;height:36px;line-height:36px;}"
          + "#enteteliste {font-size: 14px;color: white;text-align: center;background-color: #7C7C7C;height: 26px;font-weight: bold;}"
          + ".refArticleMail {font-size: 13px;height: 26px; width: 18%;}" + ".libArticleMail {font-size: 13px;height: 26px;width: 33%;}"
          + ".qteArticleMail {font-size: 13px;height: 26px;text-align: right;width: 8%;}"
          + ".multipleMail {font-size: 13px;height: 26px;width: 5%;text-align: right;}"
          + ".tarifArticleMail, .totalArticleMail {font-size: 13px; height: 26px;width: 16%;text-align: right;}"
          + "#totalCommandeMail {font-size: 13px;height: 26px;text-align: right;font-weight: bold;border-top: 1px solid #7C7C7C;}";
    }
    
    String css = "<style type=\"text/css\">" + contenuFichierCss + "</style>";
    
    return css;
  }
}
