
package manager;

import java.util.ArrayList;

import database.record.GenericRecord;
import metier.Mail;
import metier.TypeMail;

/**
 * Permet d'executer les traitements sp�cifiques des types de mail commandes Web
 * correspondant aux types de mails CDES_WEB_DISPOS et CDES_WEB_EXPE
 */
public class TypeMailCommandeWeb {
  private static final String TABLE_ENTETE_CDES = "PGVMEBCM";
  private static final String TABLE_LIGNES_CDES = "PGVMLBCM";
  private static final String TABLE_CLIENTS = "PGVMCLIM";
  private static final String TABLE_ARTICLE = "PGVMARTM";
  private static final String TABLE_ADRESSES_CDES = "PGVMADVM";
  private static final String TABLE_PARAMETRES = "PGVMPARM";
  private static final String CODE_RESUME_COMMANDE = "$$RESUMECOMMANDE$$";
  private static final String CODE_DETAIS_COMMANDE = "$$DETAILCOMMANDE$$";
  private static final String CODEFRANCSPACIFIQUES = "XPF";
  
  public static void majContenu(Mail pMail, String pBib) {
    if (pMail == null || pBib == null) {
      return;
    }
    if (pMail.getCorpsMail() == null || pMail.getCorpsMail().trim().length() < 1) {
      return;
    }
    
    String contenuPartiel = pMail.getCorpsMail();
    if (pMail.getCorpsMail().indexOf(CODE_RESUME_COMMANDE) >= 0) {
      contenuPartiel = contenuPartiel.replace(CODE_RESUME_COMMANDE,
          retournerResumeCommande(pMail.getType(), pMail.getEtablissement(), pMail.getIdDocument(), pBib));
      pMail.setCorpsMail(contenuPartiel);
    }
    
    if (pMail.getCorpsMail().indexOf(CODE_DETAIS_COMMANDE) >= 0) {
      contenuPartiel =
          contenuPartiel.replace(CODE_DETAIS_COMMANDE, retournerDetailsCommande(pBib, pMail.getEtablissement(), pMail.getIdDocument()));
      pMail.setCorpsMail(contenuPartiel);
    }
  }
  
  /**
   * Retourne le contenu HTML de l'ent�te d'une commande Web
   */
  private static String retournerResumeCommande(int pType, String petb, String pNumCde, String pBib) {
    
    String retour = "";
    if (pNumCde == null || pBib == null || petb == null) {
      return retour;
    }
    
    System.out.println("[retournerResumeCommande] " + pBib + ":infos pNum : " + pNumCde + " - pBib : " + pBib);
    String[] idsCde = retournerIdsCommande(pNumCde);
    if (idsCde == null || idsCde.length != 2) {
      return retour;
    }
    
    ArrayList<GenericRecord> listeRecords =
        MailManager.accesDb2.select("SELECT E1COD, E1NUM, E1SUF, E1MAG, SUBSTR(PARZ2, 1, 24) AS LIBMAG, "
            + " CASE WHEN AVNOM IS NOT NULL THEN AVNOM ELSE CLNOM END AS CLINOM, "
            + " CASE WHEN AVCPL IS NOT NULL THEN AVCPL ELSE CLCPL END AS CLICPL, "
            + " CASE WHEN AVRUE IS NOT NULL THEN AVRUE ELSE CLRUE END AS CLIRUE, "
            + " CASE WHEN AVLOC IS NOT NULL THEN AVLOC ELSE CLLOC END AS CLILOC, "
            + " CASE WHEN AVCDP IS NOT NULL THEN AVCDP ELSE SUBSTR(CLVIL, 1, 5) END AS CLICDP, "
            + " CASE WHEN AVVIL IS NOT NULL THEN AVVIL ELSE SUBSTR(CLVIL, 6, 24)  END AS CLIVIL " + " FROM " + pBib + "."
            + TABLE_ENTETE_CDES + " LEFT JOIN " + pBib + "." + TABLE_CLIENTS + " " + " ON E1ETB=CLETB AND E1CLFP=CLCLI AND E1CLFS=CLLIV "
            + " LEFT JOIN " + pBib + "." + TABLE_ADRESSES_CDES + " ON E1ETB=AVETB AND E1NUM=AVNUM AND E1SUF=AVSUF AND AVCOD = 'L' "
            + " LEFT JOIN " + pBib + "." + TABLE_PARAMETRES + " ON PARTYP = 'MA' AND PARETB = E1ETB AND E1MAG = PARIND "
            + " WHERE E1ETB = '" + petb + "' AND E1COD='E' AND E1NUM = '" + idsCde[0] + "' AND E1SUF='" + idsCde[1] + "' ");
    
    if (listeRecords != null) {
      if (listeRecords.size() == 1) {
        switch (pType) {
          case TypeMail.TYPE_CDES_WEB_DISPOS:
            retour = retournerPatternCommandeDispo(listeRecords.get(0));
            break;
          case TypeMail.TYPE_CDES_WEB_EXPE:
            retour = retournerPatternCommandeLivree(listeRecords.get(0));
            break;
          default:
            break;
        }
      }
      else {
        System.out.println("[TypeMailCommandeWeb] resume " + pBib + ": commande introuvable ");
      }
    }
    else {
      System.out.println("[TypeMailCommandeWeb] resume " + pBib + ": Erreur SQL de r�cup�ration de la commande ");
    }
    
    return retour;
  }
  
  /**
   * Retourne le tableau des identifiants de commande S�rie N numero et suffixe
   */
  private static String[] retournerIdsCommande(String pCdeBrute) {
    if (pCdeBrute == null || pCdeBrute.length() != 7) {
      return null;
    }
    
    String premier = pCdeBrute.substring(0, 6);
    String second = pCdeBrute.substring(6);
    String[] tab = { premier, second };
    
    System.out.println("[retournerIdsCommande()] premier " + premier);
    System.out.println("[retournerIdsCommande()] second : " + second);
    
    return tab;
  }
  
  /**
   * Retourne le pattern HTML du message de confirmation d'une commande Web exp�di�e
   */
  private static String retournerPatternCommandeLivree(GenericRecord pRecord) {
    String retour = "";
    if (pRecord == null) {
      return retour;
    }
    
    String numero = "";
    if (pRecord.isPresentField("E1NUM")) {
      numero = pRecord.getField("E1NUM").toString().trim();
    }
    String suffixe = "";
    if (pRecord.isPresentField("E1SUF")) {
      suffixe = pRecord.getField("E1SUF").toString().trim();
    }
    String nom = null;
    if (pRecord.isPresentField("CLINOM")) {
      nom = pRecord.getField("CLINOM").toString().trim();
    }
    String complement = null;
    if (pRecord.isPresentField("CLICPL")) {
      complement = pRecord.getField("CLICPL").toString().trim();
    }
    String rue = null;
    if (pRecord.isPresentField("CLIRUE")) {
      rue = pRecord.getField("CLIRUE").toString().trim();
    }
    String localite = null;
    if (pRecord.isPresentField("CLILOC")) {
      localite = pRecord.getField("CLILOC").toString().trim();
    }
    String codePostal = null;
    if (pRecord.isPresentField("CLICDP")) {
      codePostal = pRecord.getField("CLICDP").toString().trim();
    }
    String ville = null;
    if (pRecord.isPresentField("CLIVIL")) {
      ville = pRecord.getField("CLIVIL").toString().trim();
    }
    
    retour = "Nous avons le plaisir de vous informer que votre commande N�" + numero + "/" + suffixe
        + " a �t� exp�di�e � l'adresse de livraison suivante :<br/>";
    
    if (nom != null) {
      retour += nom;
    }
    if (complement != null) {
      retour += " - " + complement;
    }
    if (nom != null || complement != null) {
      retour += "<br/>";
    }
    
    if (rue != null) {
      retour += rue;
    }
    if (localite != null) {
      retour += " - " + localite;
    }
    if (rue != null || localite != null) {
      retour += "<br/>";
    }
    
    if (codePostal != null) {
      retour += codePostal;
    }
    if (ville != null) {
      retour += " - " + ville;
    }
    
    return retour;
  }
  
  /**
   * Retourne le pattern HTML du message de confirmation d'une commande Web mise � disposition en magasin
   */
  private static String retournerPatternCommandeDispo(GenericRecord pRecord) {
    String retour = "";
    if (pRecord == null) {
      return retour;
    }
    
    String numero = "";
    if (pRecord.isPresentField("E1NUM")) {
      numero = pRecord.getField("E1NUM").toString().trim();
    }
    String suffixe = "";
    if (pRecord.isPresentField("E1SUF")) {
      suffixe = pRecord.getField("E1SUF").toString().trim();
    }
    String magasin = "";
    if (pRecord.isPresentField("LIBMAG")) {
      magasin = pRecord.getField("LIBMAG").toString().trim();
    }
    
    retour = "Nous avons le plaisir de vous informer que votre commande N�" + numero + "/" + suffixe
        + " est disponible dans votre magasin " + magasin + ".";
    
    return retour;
  }
  
  /**
   * Retourne le contenu HTML du d�tail d'une commande � int�grer dans un mail
   * Ent�te + lignes
   */
  private static String retournerDetailsCommande(String pBib, String pEtb, String pNumCde) {
    String retour = "";
    if (pNumCde == null || pBib == null || pEtb == null) {
      return retour;
    }
    
    String[] idsCde = retournerIdsCommande(pNumCde);
    if (idsCde == null || idsCde.length != 2) {
      return retour;
    }
    
    String multiplicateur = "1";
    String devise = " &euro;";
    // Franc pacifique
    if (montantsSontEnCFA(pBib, pEtb)) {
      devise = " F";
      multiplicateur = "100";
    }
    
    ArrayList<GenericRecord> listeRecords = MailManager.accesDb2.select(" SELECT L1ETB, L1ART, A1LIB, L1UNV, L1QTE*L1KSV as L1QTE, "
        + " (L1PVC*" + multiplicateur + ") AS L1PVC, (L1MHT*" + multiplicateur + ") AS L1MHT," + " L1ERL, L1NLI, E1ETA, (E1THTL*"
        + multiplicateur + ") AS E1THTL, E1RCC, E1NCC " + " FROM " + pBib + "." + TABLE_LIGNES_CDES + " LEFT JOIN " + pBib + "."
        + TABLE_ARTICLE + " ON L1ETB=A1ETB AND L1ART=A1ART " + " LEFT JOIN " + pBib + "." + TABLE_ENTETE_CDES + " "
        + " ON L1ETB=E1ETB AND L1NUM=E1NUM AND L1SUF=E1SUF AND L1COD=E1COD " + " WHERE L1ETB = '" + pEtb + "' AND L1COD='E' AND L1NUM = '"
        + idsCde[0] + "' AND L1SUF='" + idsCde[1] + "' " + " AND L1ERL IN ('C' , 'S') ORDER BY L1NLI ");
    
    if (listeRecords != null) {
      if (listeRecords.size() > 0) {
        retour = retournerPatternDetailCommande(listeRecords, devise);
      }
      else {
        System.out.println("[TypeMailCommandeWeb] retournerDetailsCommande " + pBib + ": commande introuvable ");
      }
    }
    
    else {
      System.out.println("[TypeMailCommandeWeb] retournerDetailsCommande" + pBib + ": Erreur SQL de r�cup�ration de la commande ");
    }
    
    return retour;
  }
  
  /**
   * Retourne le pattern HTML du d�tail d'une commande Web En t�te et lignes
   */
  private static String retournerPatternDetailCommande(ArrayList<GenericRecord> pListe, String pDevise) {
    StringBuilder retour = new StringBuilder();
    
    if (pListe != null && pListe.size() > 0) {
      
      retour.append("<table id='listeDarticles'>");
      retour.append(
          "<tr><td class='paddingHorizontal'></td><td colspan='6' id='enteteliste'>Les articles</td><td class='paddingHorizontal'></td></tr>");
      
      for (int i = 0; i < pListe.size(); i++) {
        retour.append("<tr class='ligneArticleMail'>");
        
        retour.append("<td class='paddingHorizontal'></td>");
        
        retour.append("<td class='refArticleMail'>");
        // r�f�rence ou code article
        if (pListe.get(i).isPresentField("L1ART"))
          retour.append(pListe.get(i).getField("L1ART").toString().trim());
        retour.append("</td>");
        
        if (!pListe.get(i).isPresentField("A1LIB") && pListe.get(i).isPresentField("L1ART"))
          pListe.get(i).setField("A1LIB", pListe.get(i).getField("L1ART").toString().trim());
        
        retour.append("<td class='libArticleMail'>" + pListe.get(i).getField("A1LIB").toString() + "</td>");
        
        if (pListe.get(i).isPresentField("L1QTE"))
          retour.append(
              "<td class='qteArticleMail'>" + afficherValeurCorrectement(pListe.get(i).getField("L1QTE").toString().trim()) + "</td>");
        retour.append("<td class='multipleMail'> x </td>");
        if (pListe.get(i).isPresentField("L1PVC") && pListe.get(i).isPresentField("L1UNV"))
          retour.append("<td class='tarifArticleMail'>" + pListe.get(i).getField("L1PVC") + pDevise + "</td>");
        if (pListe.get(i).isPresentField("L1MHT") && pListe.get(i).isPresentField("L1UNV"))
          retour.append("<td class='totalArticleMail'>" + afficherValeurCorrectement(pListe.get(i).getField("L1MHT").toString().trim())
              + pDevise + "</td>");
        
        retour.append("<td class='paddingHorizontal'></td>");
        retour.append("</tr>");
      }
      
      if (pListe.get(0).isPresentField("E1THTL"))
        retour.append("<tr><td class='paddingHorizontal'></td><td  colspan='6' id='totalCommandeMail'>Total HT : "
            + afficherValeurCorrectement(pListe.get(0).getField("E1THTL").toString().trim()) + pDevise
            + " </td><td class='paddingHorizontal'></td></tr>");
      
      retour.append("</table>");
    }
    
    return retour.toString();
  }
  
  /**
   * Afficher une valeur d�cimale (stock, conditionnement) correctement HORS PRIX
   */
  public static String afficherValeurCorrectement(String donnee) {
    if (donnee == null || donnee.trim().equals(""))
      return null;
    String retour = donnee;
    
    if (donnee.endsWith(".000000"))
      donnee = donnee.replaceAll(".000000", "");
    else if (donnee.endsWith(".00000"))
      donnee = donnee.replaceAll(".00000", "");
    else if (donnee.endsWith(".0000"))
      donnee = donnee.replaceAll(".0000", "");
    else if (donnee.endsWith(".000"))
      donnee = donnee.replaceAll(".000", "");
    else if (donnee.endsWith(".00"))
      donnee = donnee.replaceAll(".00", "");
    else if (donnee.endsWith(".0"))
      donnee = donnee.replaceAll(".0", "");
    retour = donnee;
    
    return retour;
  }
  
  /**
   * Permet de savoir si les montants sont en CFA
   */
  public static boolean montantsSontEnCFA(String pBib, String pEtb) {
    boolean retour = false;
    ArrayList<GenericRecord> listeRecords =
        MailManager.accesDb2.select(" SELECT SUBSTR(PARZ2, 121, 3) AS DEVISE, SUBSTR(PARZ2, 134, 1) AS DECIMALES " + " FROM " + pBib + "."
            + TABLE_PARAMETRES + " WHERE PARTYP = 'DG' AND PARETB ='" + pEtb + "' ");
    
    if (listeRecords != null) {
      if (listeRecords.size() == 1) {
        if (listeRecords.get(0).isPresentField("DEVISE") && listeRecords.get(0).isPresentField("DECIMALES")) {
          retour = (listeRecords.get(0).getField("DEVISE").toString().trim().equals(CODEFRANCSPACIFIQUES)
              && listeRecords.get(0).getField("DECIMALES").toString().trim().equals("1"));
        }
      }
    }
    
    return retour;
  }
}
