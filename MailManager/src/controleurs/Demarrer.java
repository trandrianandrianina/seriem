package controleurs;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.MailManager;

/**
 * Servlet implementation class MailManager
 */
public class Demarrer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static MailManager mailManager = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Demarrer() {
		super();
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		if (mailManager == null) {
			boolean okParametres = true;

			if (config.getServletContext().getInitParameter("ADRESSE_AS400") == null) {
				System.out.println("MailManager: Param�tre ADRESSE_AS400 manquant dans le web.xml");
				okParametres = false;
			}
			
			if (config.getServletContext().getInitParameter("LETTRE_ENV") == null) {
				System.out.println("MailManager: Param�tre LETTRE_ENV manquant dans le web.xml");
				okParametres = false;
			}

			if (config.getServletContext().getInitParameter("MAIL_PROFIL_AS") == null) {
				System.out.println("MailManager: Param�tre MAIL_PROFIL_AS manquant dans le web.xml");
				okParametres = false;
			}
			if (config.getServletContext().getInitParameter("MAIL_MP_AS") == null) {
				System.out.println("MailManager: Param�tre MAIL_MP_AS manquant dans le web.xml");
				okParametres = false;
			}
			if (config.getServletContext().getInitParameter("LIBELLE_SERVEUR") == null) {
				System.out.println("MailManager: Param�tre LIBELLE_SERVEUR manquant dans le web.xml");
				okParametres = false;
			}
			if (config.getServletContext().getInitParameter("MAIL_FREQUENCE_SCAN") == null) {
				System.out.println("MailManager: Param�tre MAIL_FREQUENCE_SCAN manquant dans le web.xml");
				okParametres = false;
			}

			boolean debug = (config.getServletContext().getInitParameter("MAIL_DEBUG") != null
					&& config.getServletContext().getInitParameter("MAIL_DEBUG").trim().equals("1"));

			boolean isModeTest = (config.getServletContext().getInitParameter("MAIL_MODE_TEST") != null
					&& config.getServletContext().getInitParameter("MAIL_MODE_TEST").trim().equals("1"));
			if (config.getServletContext().getInitParameter("MAIL_MODE_TEST") == null) {
				System.out.println("MailManager: Param�tre MAIL_MODE_TEST manquant dans le web.xml");
				okParametres = false;
			}
			if (config.getServletContext().getInitParameter("MAIL_ADRESSE_TEST") == null) {
				System.out.println("MailManager: Param�tre MAIL_ADRESSE_TEST manquant dans le web.xml");
				okParametres = false;
			}

			if (okParametres) {
				mailManager = new MailManager(config.getServletContext().getInitParameter("ADRESSE_AS400"),
						config.getServletContext().getInitParameter("LETTRE_ENV"),
						config.getServletContext().getInitParameter("MAIL_PROFIL_AS"),
						config.getServletContext().getInitParameter("MAIL_MP_AS"),
						config.getServletContext().getInitParameter("LIBELLE_SERVEUR"),
						config.getServletContext().getInitParameter("MAIL_FREQUENCE_SCAN"), debug,
						config.getServletContext().getInitParameter("MAIL_URL_PHOTO"), isModeTest,
						config.getServletContext().getInitParameter("MAIL_ADRESSE_TEST"));

				mailManager.start();
			} else {
				System.out.println("MailManager: Il manque des param�tres dans le context.xml de Tomcat pour lancer le MailManager.");
			}
		} else {
			System.out.println("MailManager: MailManager d�j� cr��.");
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	@Override
	public void destroy() {
		if (mailManager != null) {
			mailManager.interrupt();
		}
		super.destroy();
	}
}
