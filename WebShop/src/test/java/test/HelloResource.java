/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/bonjour")
public class HelloResource {
  
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String direBonjour() {
    return "{\"name\":\"greeting\", \"message\":\"Bonjour tout le monde!\"}";
  }
}
