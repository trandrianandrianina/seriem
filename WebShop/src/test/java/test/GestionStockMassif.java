//=================================================================================================
//==>                                                                       03/02/2015 - 04/01/2015
//==> Permet la r�cup�ration du stock pour un article  
//==> A faire:
//==> Exemple: voir test11
//=================================================================================================
package test;


import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.util.ArrayList;

import ri.seriem.libas400.database.field.Field;
import ri.seriem.libas400.database.field.FieldAlpha;
import ri.seriem.libas400.database.field.FieldDecimal;
import ri.seriem.libas400.system.CallProgram;
import ri.seriem.libas400.system.SystemManager;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.Job;
import com.ibm.as400.access.ProgramParameter;

public class GestionStockMassif
{
		// Constantes
	
		// SGVX13WEBM
		private static final int LG_NBR_M=4;
		private static final int LG_LEN_M=4;
		private static final int LG_PARAME_M=32767;
	
		// SGVX13WEB
		private static final int LG_DATASTRUCT=55;
		private static final int LG_ETB=3;
		private static final int LG_CODEART=20;
		private static final int LG_MAG1=2;
		private static final int LG_MAG2=2;
		private static final int LG_STOCK1=9;
		private static final int LG_STOCK1_DEC=3;
		private static final int LG_STOCK2=9;
		private static final int LG_STOCK2_DEC=3;
		
		// Variables sp�cifiques du programme RPG SGVX13WEB
		private char[] indicateurs={'0', '0', '0', '0', '0', '0','0', '0', ' ', ' '};
		private FieldAlpha etablissement=new FieldAlpha(LG_ETB);
		private FieldAlpha codeArticle=new FieldAlpha(LG_CODEART);
		private FieldAlpha codeMag1=new FieldAlpha(LG_MAG1);
		private FieldAlpha codeMag2=new FieldAlpha(LG_MAG2);
		private FieldDecimal stock1=new FieldDecimal(new BigDecimal(1), LG_STOCK1, LG_STOCK1_DEC);
		private FieldDecimal stock2=new FieldDecimal(new BigDecimal(1), LG_STOCK2, LG_STOCK2_DEC);
		
		// Varibles
		private boolean init=false;
		private SystemManager system = null;
		private ProgramParameter[] parameterList = new ProgramParameter[3];
		private AS400Text nbrx = new AS400Text(LG_NBR_M);
		private AS400Text lenx = new AS400Text(LG_LEN_M);
		private AS400Text parame = new AS400Text(LG_PARAME_M);
		private char lettre = 'W';
		private String curlib = null;
		private CallProgram cl = null;
		private int nbrLignesMax = 0;
		private int nbrlignes = 0;
		private StringBuilder parameters = null;
		private ArrayList<String>listeParameters = null;

		/**
		 * Constructeur
		 * @param sys
		 * @param let
		 * @param curlib
		 */
		public GestionStockMassif(SystemManager sys, char let, String curlib)
		{
			system = sys;
			setLettre(let);
			setCurlib(curlib);
		}
		
		/**
		 * Initialise l'environnment d'execution du programme CL
		 * @return
		 */
		public boolean init(int nbrlignes)
		{
			if (init) return true;
			
			nbrLignesMax = nbrlignes;
			parameters = new StringBuilder(nbrLignesMax * LG_DATASTRUCT);
			listeParameters = new ArrayList<String>(nbrLignesMax);
			
			parameterList[0] = new ProgramParameter(LG_NBR_M);
			parameterList[1] = new ProgramParameter(LG_LEN_M);
			parameterList[2] = new ProgramParameter(LG_PARAME_M);
	        cl = new CallProgram(system.getSystem(), "/QSYS.LIB/"+lettre+"GVMX.LIB/SGVX13WEBM.PGM", null);
	        init = cl.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('"+lettre+"')");
	        if (!init)
	        	return false;
	        cl.addLibrary(curlib, true);
	        cl.addLibrary("M_GPL", false);
	        cl.addLibrary(lettre + "EXPAS", false);
	        cl.addLibrary(lettre + "GVMAS", false);
	        init = cl.addLibrary(lettre + "GVMX", false);
	        return init;
		}

		/**
		 * Vide la liste des param�tres
		 */
		public void clearParameters()
		{
			parameters.setLength(0);
			nbrlignes = 0;
			listeParameters.clear();
		}
		
		/**
		 * Effectue la recherche du stock
		 * @param valEtb
		 * @param valArt
		 * @param valMag1
		 * @param valMag2
		 * @param valStock1
		 * @param valStock2
		 * @return
		 */
		public void addParameters(String valEtb, String valArt, String valMag1, String valMag2, BigDecimal valStock1, BigDecimal valStock2)
		{
			setValueEtablissement(valEtb);
			setValueCodeArticle(valArt);
			setValueMagasin1(valMag1);
			setValueMagasin2(valMag2);
			setValueStock1(valStock1);
			setValueStock2(valStock2);
			
			// Ajout d'un param�tre AS400 PPAR1
			parameters.append(getIndicateurs())
			  .append(etablissement.toFormattedString())
			  .append(codeArticle.toFormattedString())
			  .append(codeMag1.toFormattedString())
			  .append(codeMag2.toFormattedString())
			  .append(stock1.toFormattedString())
			  .append(stock2.toFormattedString());
			nbrlignes++;
		}
		
		/**
		 * Effectue la recherche du stock
		 * @return
		 */
		public boolean execute()
		{
			if (cl == null)
				return false;
			
			
			try
			{
/*System.out.println("1 - " +parameters.toString().length()+ " Valeur PARAME : " + parameters.toString());				
System.out.println("1 - Nbr de lignes:" +nbrlignes);*/
				// Construction des param�tres AS400 pour le SGVX13WEBM
				parameterList[0].setInputData(nbrx.toBytes(String.format("%0"+LG_NBR_M+"d", nbrlignes)));
				parameterList[1].setInputData(lenx.toBytes(String.format("%0"+LG_LEN_M+"d", LG_DATASTRUCT)));
				parameterList[2].setInputData(parame.toBytes(String.format("%-"+LG_PARAME_M+"s", parameters.toString())));
				cl.setParameterList(parameterList);
				// Appel du programme CL
				if (cl.execute())
				{
					// R�cup�ration de la variable PARAME 
					String valeur = (String) parame.toObject(parameterList[2].getOutputData());
//System.out.println("2 - "+valeur.length()+" Valeur PARAME : " + valeur);
					if (valeur != null)
					{
						// On d�compose les param�tres pour chaque ligne dans une liste
						for( int i=0; i<nbrlignes; i++)
						{
							int pos = i*LG_DATASTRUCT;
							listeParameters.add( String.format("%-"+(LG_DATASTRUCT+1)+"s", valeur.substring( pos, pos+LG_DATASTRUCT )) );
						}
					}
					return true;
				}
				//else System.out.println("probleme � l'exec du cl.execute() "  + cl.getMsgError());
			}
			catch (PropertyVetoException e)
			{
				e.printStackTrace();
			}
			return false;
		}
		
		/**
		 * Retourne le stock1 d'une ligne pr�cise
		 * @param indice
		 * @return
		 */
		public Field<BigDecimal> getStock1(int indice)
		{
			try
			{
				//TODO Attention des fois il y a un le symbole "�"  � traiter dans ce substring -> valeur n�gative C'est tout nouveau �a vient de sortir
				stock1.setValue(new BigDecimal(listeParameters.get(indice).substring(38, 46)));
			}
			catch (Exception e)
			{
				stock1.setValue(new BigDecimal(0));
				e.printStackTrace();
			}
			return getStock1();
		}
		
		/**
		 * Retourne le stock2 d'une ligne pr�cise
		 * @param indice
		 * @return
		 */
		public Field<BigDecimal> getStock2(int indice)
		{
			try
			{
				//TODO Attention des fois il y a un le symbole "�"  � traiter dans ce substring -> valeur n�gative C'est tout nouveau �a vient de sortir
				stock2.setValue(new BigDecimal(listeParameters.get(indice).substring(47, 55)));
			}
			catch (Exception e)
			{
				stock2.setValue(new BigDecimal(0));
				e.printStackTrace();
			}
			return getStock2();
		}

		/**
		 * Calcul et formatage du stock dispo pour une ligne
		 * @param indice
		 * @return
		 */
		public String getStockDispo( int indice)
		{
			String dispo = null;
			BigDecimal stockdispo = getStock1(indice).getValue().add( getStock2(indice).getValue() ); 
			if(stockdispo.toString().length() > 1)
				dispo = stockdispo.toString().substring(0, stockdispo.toString().length() - 3) + "." + stockdispo.toString().substring(stockdispo.toString().length() - 3);
			else
				dispo = stockdispo.toString();
			
			return dispo;
		}
		
		// -- M�thodes priv�es ----------------------------------------------------
		
		/**
		 * Formate une variable de type String (avec cadrage � droite, notamment pour l'�tablissement)
		 * @param valeur
		 * @param lg
		 * @return
		 */
		private String formateVar(String valeur, int lg)
		{
			if (valeur == null)
				return String.format("%"+lg+"."+lg+"s", "");
			else
				return String.format("%"+lg+"."+lg+"s", valeur);
		}

		
		// -- Accesseurs ----------------------------------------------------------
		
		/**
		 * @return le lettre
		 */
		public char getLettre()
		{
			return lettre;
		}


		/**
		 * @param lettre le lettre � d�finir
		 */
		public void setLettre(char lettre)
		{
			if (lettre != ' ')
				this.lettre = lettre;
		}


		/**
		 * @return le curlib
		 */
		public String getCurlib()
		{
			return curlib;
		}


		/**
		 * @param curlib le curlib � d�finir
		 */
		public void setCurlib(String curlib)
		{
			this.curlib = curlib;
		}


		/**
		 * @return le indicateurs
		 */
		private char[] getIndicateurs()
		{
			return indicateurs;
		}
	
		
		/**
		 * Initialise la valeur de l'�tablissement
		 * @param etablissement le etablissement � d�finir
		 */
		private void setValueEtablissement(String etablissement)
		{
			this.etablissement.setValue(formateVar(etablissement, LG_ETB));
		}
		
		
		
		/**
		 * Initialise la valeur du code Article
		 * @param codMag1 le codMag1 � d�finir
		 */
		private void setValueCodeArticle(String codeArticle)
		{
			this.codeArticle.setValue(codeArticle);
		}
		
		
		
		/**
		 * Initialise la valeur du magasin 2
		 * @param codMag2 le codMag2 � d�finir
		 */
		private void setValueMagasin1(String codeMag1)
		{
			this.codeMag1.setValue(codeMag1);
		}
		
		
		
		/**
		 * Initialise la valeur du code Article
		 * @param codeArticle le codeArticle � d�finir
		 */
		private void setValueMagasin2(String codeMag2)
		{
			this.codeMag2.setValue(codeMag2);
		}
		
		/**
		 *  Retourne le champ valStock1
		 * @return le stock 1
		 */
		private Field<BigDecimal> getStock1()
		{
			return stock1;
		}
		
		/**
		 * Initialise la valeur de la quantit�
		 * @param stock1 le stock1 � d�finir
		 */
		private void setValueStock1(BigDecimal stock1)
		{
			this.stock1.setValue(stock1);
		}
		
		/**
		 *  Retourne le champ valStock1
		 * @return le stock 2
		 */
		private Field<BigDecimal> getStock2()
		{
			return stock2;
		}
		
		/**
		 * Initialise la valeur de la quantit�
		 * @param stock2 le stock2 � d�finir
		 */
		private void setValueStock2(BigDecimal stock2)
		{
			this.stock2.setValue(stock2);
		}
		
		public Job getJob()
		{
			if (cl != null)
				return cl.getJob();
			return null;
		}
	

}
