/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package test;

import javax.xml.bind.annotation.XmlElement;

public class RequestBody {
  @XmlElement
  String nom1;
  @XmlElement
  String nom2;
}
