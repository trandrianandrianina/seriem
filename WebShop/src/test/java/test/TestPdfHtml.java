
package test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class TestPdfHtml {
  public static void main(String[] args) {
    String pdfFile = ("C:\\Users\\ritoudb\\Desktop\\Desktop\\FilePDF.pdf");
    // String CSS = "tr { text-align: center; } th { background-color: lightgreen; padding: 3px; } td {background-color:
    // lightblue; padding: 3px; }";
    
    Document document = new Document(PageSize.A4);
    document.setMargins(20, 20, 30, 30);
    
    try {
      PdfWriter writer;
      // ByteArrayOutputStream FilePDF = new ByteArrayOutputStream();
      writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
      
      writer.close();
      
      document.open();
      // document.newPage();
      
      StringBuilder sb = new StringBuilder();
      
      sb.append("<table id='presentation'>");
      sb.append("<tr>");
      sb.append("<td class='tdPres' id='nomServeur'>");
      sb.append("Nom du serveur test");
      sb.append("</td>");
      sb.append("<td class='tdPres' id='image_serveur'>");
      sb.append("<img id='imageDeco' src='http://resolution-informatique.com:8022/webshop/css/image_decoration.jpg'/>");
      sb.append("</td>");
      sb.append("<td class='tdPres' id='webShop'>");
      sb.append("<span id='spanWeb'>Web</span><br/><span id='spanShop'>SHOP</span>");
      // sb.append("WebShop");
      sb.append("</td>");
      sb.append("</tr>");
      sb.append("<tr>");
      sb.append("<td id='titrePage' colspan='3'>Titre de la page</td>");
      sb.append("</tr>");
      sb.append("</table>");
      
      sb.append("<table id='commandes'>");
      sb.append("<tr>");
      sb.append("<th>Sr. No.</th>");
      sb.append("<th>Text Data</th>");
      sb.append("<th>Number Data</th>");
      sb.append("</tr>");
      for (int i = 0; i < 60;) {
        i++;
        sb.append("<tr>");
        sb.append("<td>");
        sb.append(i);
        sb.append("</td>");
        sb.append("<td>This is text data ");
        sb.append(i);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(i);
        sb.append("</td>");
        sb.append("</tr>");
      }
      sb.append("</table>");
      
      // CssFile cssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream(CSS.getBytes()));
      // XMLWorkerHelper helper = XMLWorkerHelper.getInstance();
      CssFile cssFile = XMLWorkerHelper.getCSS(new FileInputStream("C:\\Users\\ritoudb\\Desktop\\Desktop\\test.css"));
      
      CSSResolver cssResolver = new StyleAttrCSSResolver();
      cssResolver.addCss(cssFile);
      
      // HTML
      HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
      htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
      // Pipelines
      PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
      HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
      CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
      
      // XML Worker
      XMLWorker worker = new XMLWorker(css, true);
      XMLParser p = new XMLParser(worker);
      p.parse(new ByteArrayInputStream(sb.toString().getBytes()));
    }
    catch (Exception e) {
    }
    
    if (document != null)
      document.close();
  }
}
