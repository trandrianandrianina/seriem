/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package test;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/Auteur")
public class AuteurService {
  @POST
  @Path("/listeAuteurs")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces(MediaType.APPLICATION_JSON)
  public List<Personne> getListeAuteur(RequestBody pRequestBody) {
    List<Personne> listePersonne = new ArrayList<Personne>();
    Personne p1 = new Personne();
    p1.setId(1);
    p1.setName(pRequestBody.nom1);
    
    Personne p2 = new Personne();
    p2.setId(2);
    p2.setName(pRequestBody.nom2);
    
    listePersonne.add(p1);
    listePersonne.add(p2);
    return listePersonne;
  }
  
}
