/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package test;

import java.io.Serializable;

public class Personne implements Serializable {
  private int id;
  private String name;
  
  public int getId() {
    return id;
  }
  
  public void setId(int id) {
    this.id = id;
  }
  
  public String getName() {
    return name.toLowerCase();
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public Personne(int id, String name) {
    super();
    this.id = id;
    this.name = name;
  }
  
  public Personne() {
    super();
  }
}
