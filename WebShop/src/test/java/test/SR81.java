package test;

import java.io.IOException;

public class SR81
{
	private static String WCAR = " " ;
	private static int WNIV ;
	private static String WNG = "";
	private static int W1 ;
	
	private static int indic01 = 0;
	private static int indic02 = 1;
	private static int indic03 = 0;
	
	private static void sr81()
	{
		System.out.println("Je passe dans sr81(): " );
		WNIV = 0;
		W1 = 0;
		
		setOff(indic02);
		
		sr8102();
		return;
	}
	
	private static void sr8102()
	{
		W1++;
		System.out.println("Je passe dans sr8102(): " + W1);
		
		if(W1>6) setOn(indic01);
		
		if(isLeve(indic01))
		{
			sr8110();
			return;
		}
		
		//Si les positions commencent � 1 sinon changer le -1  - > +1 
		if(WNG.length()>W1-1 && W1 > 0)
			WCAR = WNG.substring(W1-1, W1);
		
		if(WCAR.equals(" ")) 
			setOn(indic01);
		
		if(!isLeve(indic01))
		{
			sr8104();
			return;
		}
		
		setOn(indic02);
		
		if(isLeve(indic03))
		{
			if(WNG.length()>W1-1 && W1 > 0)
				WNG = WNG.substring(0, W1-1) + "9" + WNG.substring(W1);
		}
			
		if(!isLeve(indic03))
		{
			if(WNG.length()>W1-1 && W1 > 0)
				WNG = WNG.substring(0, W1-1) + "0" + WNG.substring(W1);
		}
		
		if(W1<100) //Enlever ce test de boucle continue
			sr8102();
	}
	
	private static void sr8104()
	{
		System.out.println("Je passe dans sr8104(): " );
		if(isLeve(indic02))
		{
			sr8190();
			return;
		}
		
		if(W1>1)
			setOn(indic01);																																					
		
		if(isLeve(indic01))
		{
			sr8106();
			return;
		}
		
		if(Integer.parseInt(WCAR)< 1)
			setOn(indic01);	
		
		if(Integer.parseInt(WCAR)> 8)
			setOn(indic01);	
		
		if(isLeve(indic01))
		{
			sr8190();
			return;
		}
		
		sr8108();
		return;
	}
	
	private static void sr8106()
	{
		System.out.println("Je passe dans sr8106(): " );
		try
		{
			if(Integer.parseInt(WCAR)>= 0)
				setOn(indic01);
		}
		catch (NumberFormatException e)
		{
			// TODO Bloc catch g�n�r� automatiquement
			e.printStackTrace();
		}
		
		if(!isLeve(indic01))
			if(Integer.parseInt(WCAR)> 9)
				setOn(indic01);	
		
		if(isLeve(indic01))
			sr8190();
	}
	
	private static void sr8108()
	{
		System.out.println("Je passe dans sr8108(): " );
		WNIV++;
		sr8102();
		return;
	}
	
	private static void sr8110()
	{
		System.out.println("Je passe dans sr8110(): " );
		setOff(indic01);
		sr8199();
		return;
	}
	
	private static void sr8190()
	{
		System.out.println("Je passe dans sr8190(): " );
		setOn(indic01);
	}
	
	private static void sr8199()
	{
		System.out.println("Je passe dans sr8199(): " );
		
	}
	
	private static void setOff(int indic)
	{
		indic = 0;
	}
	
	private static void setOn(int indic)
	{
		indic = 1;
	}
	
	private static boolean isLeve(int indic)
	{
		if(indic>0)
			return true;
		else return false;
	}
	
	public static void main(String[] args) throws IOException 
	{
		sr81();
		
		System.out.println("+++++++++++++ RESULTAT SR81(): ");
		System.out.println("+++++++ WNIV: " + WNIV);
		System.out.println("+++++++ W1: " + W1);
		System.out.println("+++++++ indic01: " + indic01);
		System.out.println("+++++++ indic02: " + indic02);
	}
}
