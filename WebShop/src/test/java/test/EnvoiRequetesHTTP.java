
package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class EnvoiRequetesHTTP {
  static URL oracle = null;
  static URLConnection yc = null;
  static BufferedReader in = null;
  
  public static String get(String url) throws IOException {
    
    String source = "";
    
    try {
      yc = oracle.openConnection();
      in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
      String inputLine;
      
      while ((inputLine = in.readLine()) != null)
        source += inputLine;
      in.close();
    }
    catch (Exception e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
    }
    
    return source;
  }
  
  public static void main(String[] args) throws IOException {
    // int compteur = 110;
    int compteur = 1;
    String retour = "";
    // String url = "http://88.188.171.20:8080/WebShop/accueil?id=";
    // String url = "http://172.31.1.249:8020/WebShop/accueil";
    // String url = "http://88.188.171.20:8080/WebShop/accueil?loginUser=sarlsatp@orange.fr&passUser=gar1972";
    // String url = "http://88.188.171.20:8080/WebShop/catalogue";
    // String url = "http://88.188.171.20:8080/WebShop/accueil?loginUser=sarlsatp@orange.fr&passUser=test' OR '1'= '1 ";
    String url = "http://resolution-informatique.com:8080/WebShop/accueil";
    oracle = new URL(url);
    
    for (int i = 0; i < compteur; i++) {
      retour = EnvoiRequetesHTTP.get(url);
      System.out.println("requete " + i + " -> " + retour);
    }
    
    // SessionListener.afficherListeDesSessions();
  }
}
