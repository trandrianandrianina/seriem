
package ri.seriem.webshop.vues;

import java.text.DateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.GestionConnexion;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;

/**
 * Cette classe est le pattern du look global du Web Shop. La structure de chaque page repose sur ce mod�le
 */
public class PatternErgonomie {
  
  private DateFormat mediumDateFormat = null;
  
  /**
   * Constructeur par d�faut
   */
  public PatternErgonomie() {
    mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
  }
  
  /**
   * Afficher l'ent�te de la page Web
   */
  public String majDuHead(HttpServletRequest request, String metaSpecifiques, String ancrage) {
    String chaine = "";
    if ((Utilisateur) request.getSession().getAttribute("utilisateur") == null)
      return "";
    chaine += "<!DOCTYPE html>";
    chaine += "<html lang='fr'>";
    chaine += "<head>";
    chaine += "<meta charset='windows-1252'>";
    chaine +=
        "<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi' />";
    chaine +=
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "webShop.css?" + ConstantesEnvironnement.versionWAR + "' rel='stylesheet'/>";
    chaine += "<link rel=\"shortcut icon\" href=\"" + ConstantesEnvironnement.DOSSIER_IMAGES_DECO + "favicon.png\" />";
    chaine += "<script src='scripts/general.js?" + ConstantesEnvironnement.versionWAR + "'></script>";
    
    if (metaSpecifiques != null)
      chaine += metaSpecifiques;
    
    // Relooker en mode REPRESENTANT
    if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS)
      chaine += "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "representant.css?" + ConstantesEnvironnement.versionWAR
          + "' rel='stylesheet'/>";
    
    chaine += "<link href='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/css/CSSspecifique.css?"
        + ConstantesEnvironnement.versionWAR + "' rel='stylesheet'/>";
    
    // Google analytique
    if (ConstantesEnvironnement.GOOGLE_ANALYTICS != null)
      chaine += ConstantesEnvironnement.GOOGLE_ANALYTICS;
    else {
      chaine += "<meta name='ROBOTS' content='NONE'> ";
      chaine += "<meta name='GOOGLEBOT' content='NOARCHIVE'>";
    }
    chaine += "</head>";
    
    if (ancrage == null)
      ancrage = "";
    chaine += "<body " + ancrage + ">";
    return chaine;
  }
  
  /**
   * Afficher le panneau de pr�sentation
   */
  public String afficherPresentation(String page, HttpServletRequest request, String connexion) {
    if ((Utilisateur) request.getSession().getAttribute("utilisateur") == null)
      return "";
    
    Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
    
    StringBuilder chaine = new StringBuilder(2000);
    chaine.append("<div id='presentationHeader'>");
    
    chaine.append("<header>");
    if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
      chaine.append(
          "<div id='alerteClient'><div class='puceAlerte'>&nbsp;</div><div id='contenuAlerteClient'>Votre compte n'est pas autoris� � commander sur le Web Shop</div></div>");
    }
    chaine.append("<h1 id='h1Web'>Web</h1>");
    chaine.append("<h1 id='h1Shop'>SHOP</h1>");
    chaine.append("<h1 id='h1Client'>Nom du client</h1>");
    if (connexion != null)
      chaine.append(connexion);
    chaine.append(afficherNavigation(page, request.getSession()));
    
    if (utilisateur.getETB_EN_COURS() != null && utilisateur.getETB_EN_COURS().getAdresse_site_public() != null
        && (!utilisateur.getETB_EN_COURS().getAdresse_site_public().equals(""))) {
      chaine.append("<a href='" + utilisateur.getETB_EN_COURS().getAdresse_site_public() + "' target='_blank' id='lienAccueil'>");
      chaine.append("<img id='logoClient' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/logoSociete.png' alt=''>");
      chaine.append("</a>");
    }
    else {
      chaine.append("<a href='accueil' id='lienAccueil'>");
      chaine
          .append("<img id ='logoClient'  src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/logoSociete.png' alt=''>");
      chaine.append("</a>");
    }
    
    chaine.append("<img id='image_deco' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/image_decoration.jpg' alt='Web Shop'>");
    if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS)
      chaine.append("<div id='titreRepresentant'>Mobilit� <span id='plusRepresentant'>+</span></div>");
    chaine.append("</header>");
    
    chaine.append("</div>");
    
    return chaine.toString();
  }
  
  /**
   * ---------------------------GESTION DU PANNEAU DE NAVIGATION-----------
   */
  
  /**
   * Afficher le panneau de navigation
   */
  public String afficherNavigation(String page, HttpSession session) {
    if (page == null || session == null)
      return "";
    
    StringBuilder chaine = new StringBuilder(2000);
    
    chaine.append("<nav id='navPersistante'>");
    
    Utilisateur utilisateur = null;
    if ((Utilisateur) session.getAttribute("utilisateur") == null) {
      return "";
    }
    else {
      utilisateur = (Utilisateur) session.getAttribute("utilisateur");
    }
    
    if (utilisateur.getPanierIntuitif() == null) {
      // Aller chercher la liste des menus si n�cessaire
      if (utilisateur.getMesMenus() == null) {
        utilisateur.recupererMenus();
        
        if (utilisateur.getMesMenus() == null)
          return "";
      }
      
      String classe = "";
      for (GenericRecord menu : utilisateur.getMesMenus()) {
        if (page.equals(menu.getField("MN_LINK").toString().trim()))
          classe = "class='courantNav'";
        else
          classe = "class='lienNav'";
        chaine.append("<a " + classe + " href='" + menu.getField("MN_LINK").toString().trim() + "'>" + "<span class='navModeTexte'>"
            + utilisateur.getTraduction().traduire(menu.getField("MN_NAME").toString().trim()) + " </span>" + "<img src='"
            + menu.getField("MN_IMG").toString().trim() + "' alt='" + menu.getField("MN_DES").toString().trim() + "' 'title='"
            + menu.getField("MN_DES").toString().trim() + "' class='navModeGraphique'/>" + "</a>");
      }
    }
    // Moteur de recherche
    if (utilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_CLIENT) {
      chaine.append(
          "<form onSubmit='traitementEnCours(null);' action='catalogue' method='POST' id='moteurRecherche' name='moteurRecherche' accept-charset=\"utf-8\">"
              + "<input type='search' name='champRecherche' id='champRecherche' placeholder='"
              + utilisateur.getTraduction().traduire("$$Rechercher") + "' required/>"
              + "<a id='validMoteurRecherche' href='#' onClick=\"traitementEnCoursForm(document.moteurRecherche);\">r</a></form>");
    }
    
    chaine.append("</nav>");
    
    return chaine.toString();
  }
  
  /**
   * Afficher la panneau d'�tat de connexion
   */
  public String afficherConnexion(String page, HttpServletRequest request) {
    if (request == null || request.getSession() == null)
      return "";
    
    StringBuilder chaine = new StringBuilder(500);
    
    // Si on a pass� une tentative de connexion
    if (request.getParameter("loginUser") != null && request.getParameter("passUser") != null) {
      request.getSession().invalidate();
      GestionConnexion connexion = new GestionConnexion();
      connexion.majConnexion(request.getSession(), request.getParameter("loginUser"), request.getParameter("passUser"),
          request.getParameter("passUserCtrl"), request.getParameter("numeroClient"), request.getParameter("mobilitePluch"),
          request.isSecure());
    }
    // si on passe une tentative de d�connexion
    else if (request.getParameter("deconnecter") != null && request.getParameter("deconnecter").equals("1"))
      request.getSession().invalidate();
    
    // toujours passer un utilisateur � la session
    if (request.getSession().getAttribute("utilisateur") == null)
      request.getSession().setAttribute("utilisateur", new Utilisateur(request.getSession().getId()));
    
    chaine.append("<aside id='etatConnexion'>");
    
    // affichage si on est connect� ou pas
    if (request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
      Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
      // on change la langue de l'utilisateur
      if (utilisateur != null && request.getParameter("chgtLangue") != null) {
        utilisateur.setLanguage(request.getParameter("chgtLangue"));
        utilisateur.getTraduction().chargerListeEquivalence(request.getParameter("chgtLangue"));
        utilisateur.majFooters(request.getParameter("chgtLangue"));
      }
      
      // On est en mode connect�
      if (utilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_CLIENT) {
        
        chaine.append("<div id='connecte'>");
        chaine.append("<span id='monLogin'>" + afficherPortionLogin(utilisateur.getUS_LOGIN()) + "</span>");
        chaine.append(afficherModeConnexion(utilisateur));
        if (!page.equals("monPanier")) {
          // Si on est connect� via la Mobilit� Pluch
          if (utilisateur.isMobilitePlus()) {
            chaine.append("<a id='deconnecter' href='../SerieNmobilite/affichagePrincipal?menu=G'>Suite S�rie N mobilit�</a>");
            chaine.append("<a id='imgDeconnexion' href='../SerieNmobilite/affichagePrincipal?menu=G'><img src='"
                + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/deconnexion.png' id='monImgDeconnexion'/></a>");
          }
          else {
            chaine.append(
                "<a id='deconnecter' href='" + page + "?deconnecter=1'>" + utilisateur.getTraduction().traduire("Deconnexion") + "</a>");
            chaine.append("<a id='imgDeconnexion' href='" + page + "?deconnecter=1'><img src='"
                + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/deconnexion.png' id='monImgDeconnexion'/></a>");
          }
        }
        chaine.append("</div>");
        chaine.append("</aside>");
        if (utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_CONSULTATION) {
          if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
            chaine.append("");
          }
          else {
            chaine.append(afficherPanier(utilisateur, false));
          }
        }
      }
      // On est en mode pas connect�
      else {
        chaine.append("<form id='pasConnecte' method='POST' name ='connexionUser' action='" + page + "' accept-charset=\"utf-8\">");
        chaine.append("<input name='loginUser' id='loginUser' type='text' placeholder='"
            + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required />");
        chaine.append("<input name='passUser' id='passUser' type='password' placeholder='"
            + utilisateur.getTraduction().traduire("$$motDePasse") + "' required />");
        chaine.append("<input type ='submit' id='validerConnexionPattern' />");
        chaine.append("<a id='validConnexion' href='#' onClick=\"document.getElementById('validerConnexionPattern').click();\"><img src='"
            + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fleche.png' id='imgValidConnexion'/></a>");
        
        chaine.append("</form>");
        chaine.append("</aside>");
      }
    }
    
    return chaine.toString();
  }
  
  /**
   * Afficher une portion r�duite du login de l'utilisateur
   */
  private String afficherPortionLogin(String login) {
    String retour = "";
    if (login == null)
      return "";
    else
      retour = login.trim();
    
    if (login.trim().length() > 25 && login.split("@").length == 2)
      retour = login.trim().split("@")[0];
    
    if (retour.length() > 25)
      retour = retour.substring(0, 24) + "..";
    
    return retour;
  }
  
  /**
   * Afficher le mode de connexion : Nom du client ou Gestionnaire ou Administrateur....etc
   */
  private String afficherModeConnexion(Utilisateur utilisateur) {
    String retour = "";
    if (utilisateur == null)
      return retour;
    
    if (utilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_CLIENT
        || utilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_CONSULTATION) {
      retour += "<span id='modeConnexion'>(";
      if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT
          || utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CONSULTATION && utilisateur.getClient().getNomClient() != null) {
        if (utilisateur.getClient().getNomClient().trim().length() > 30)
          retour += utilisateur.getClient().getNomClient().trim().substring(0, 29) + "..";
        else
          retour += utilisateur.getClient().getNomClient().trim();
      }
      else if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS)
        retour += "Repr�sentant";
      else if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_RESPON_WS)
        retour += "Gestionnaire";
      else if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_ADMIN_WS)
        retour += "Administrateur";
      
      retour += ")</span>";
    }
    
    return retour;
  }
  
  /**
   * permet d'afficher le r�sum� du panier de l'utilisateur sur toutes les
   * pages
   */
  protected String afficherPanier(Utilisateur utilisateur, boolean isAjax) {
    String chaine = "";
    if (utilisateur == null)
      return chaine;
    
    if (!isAjax)
      chaine += "<div id='monPanier'>";
    
    if (utilisateur.getMonPanier() != null) {
      chaine += "<a id='lienPanier' href='monPanier'><img src='images/decoration/panier2.png'/></a>";
      chaine += "<a id='quantitePanier' href='monPanier'>(" + utilisateur.getMonPanier().getNombreArticle() + ")</a>";
      chaine += "<a id='totalMonPanier' href='monPanier'>"
          + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalHT()) + "</a>";
    }
    else if (utilisateur.getPanierIntuitif() != null) {
      chaine += "<a id='lienPanier' href='PanierIntuitifBO'><img src='images/decoration/panier2.png'/></a>";
      chaine += "<a id='quantitePanier' href='PanierIntuitifBO'>(" + utilisateur.getPanierIntuitif().getNombreArticle() + ")</a>";
      chaine += "<a id='totalMonPanier' href='PanierIntuitifBO'>&nbsp;</a>";
    }
    
    if (!isAjax)
      chaine += "</div>";
    
    return chaine;
  }
  
  /**
   * Afficher le pied de page
   */
  public String afficherPiedPage(String pop, HttpSession session, String page) {
    if (session == null)
      return "";
    
    Utilisateur utilisateur = ((Utilisateur) session.getAttribute("utilisateur"));
    
    if (utilisateur.getPanierIntuitif() != null) {
      return "";
    }
    
    // Infos sessions
    String infos = "- Serveur " + ConstantesEnvironnement.LIBELLE_SERVEUR + "\\n- Web Shop " + ConstantesEnvironnement.versionWAR
        + " \\n- Biblioth�que " + ConstantesEnvironnement.BIBLI_CLIENTS;
    if (utilisateur.getETB_EN_COURS() != null)
      infos += "\\n- Etb " + utilisateur.getETB_EN_COURS().getLibelleETB() + " (" + utilisateur.getETB_EN_COURS().getCodeETB() + ")";
    
    StringBuilder chaine = new StringBuilder(1000);
    if (utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_REPRES_WS) {
      chaine.append("<footer>");
      chaine.append("<a class='liensFooter' href='conditionsGenerales'>" + utilisateur.getTraduction().traduire("$$CGV") + "</a>");
      chaine.append("<a  class='liensFooter' href='magasins'>" + utilisateur.getTraduction().traduire("$$votreMagasin") + "</a>");
      
      chaine.append(afficherFooterDynamique(utilisateur));
      
      chaine.append("<a  class='liensFooter' href='contact'>Contact</a>");
      chaine.append("<a  class='liensFooter' href='#' onClick='alert(\"" + infos + "\");'>?</a>");
      // lien Facebook
      if (utilisateur.getETB_EN_COURS() != null && utilisateur.getETB_EN_COURS().getAdresse_site_facebook() != null
          && !utilisateur.getETB_EN_COURS().getAdresse_site_facebook().trim().equals("")) {
        chaine.append("<a class='liensFooter' id='lienFB' href='" + utilisateur.getETB_EN_COURS().getAdresse_site_facebook()
            + "' target='_blank'><span class='DrapeauInvisible'>FB</span></a>");
      }
      chaine.append("<span  class='liensFooter' id='copyright'>&copy 2018 R�solution Informatique</span>");
      if (page == null || (!page.equals("accueil") && !page.equals("informations") && !page.equals("conditionsGenerales")
          && !page.equals("magasins") && !page.equals("contact"))) {
        page = "accueil";
      }
      if (ConstantesEnvironnement.IS_MODE_LANGUAGES) {
        chaine.append("<a  class='liensFooter' id='lienFR' href='" + page + "?chgtLangue=fr'><span class='DrapeauInvisible'>fr</span></a>");
        chaine
            .append("<a  class='liensFooter' id='lienEN'  href='" + page + "?chgtLangue=en'><span class='DrapeauInvisible'>en</span></a>");
      }
      chaine.append("</footer>");
    }
    chaine.append("</article>");
    chaine.append(afficherPopUp(utilisateur, pop));
    chaine.append(afficherFocus("champRecherche"));
    chaine.append("</body>");
    chaine.append("</html>");
    return chaine.toString();
  }
  
  /**
   * Afficher des liens dynamiques dans le footer
   */
  private String afficherFooterDynamique(Utilisateur utilisateur) {
    if (utilisateur == null)
      return null;
    
    StringBuilder chaine = new StringBuilder(1000);
    
    if (utilisateur.getMesFooters() == null)
      utilisateur.majFooters(utilisateur.getLanguage());
    
    String target = "";
    if (utilisateur.getMesFooters() != null && utilisateur.getMesFooters().size() > 0)
      for (int i = 0; i < utilisateur.getMesFooters().size(); i++) {
        if (utilisateur.getMesFooters().get(i).getField("FT_TARG").toString().trim().equals("BL"))
          target = "Target='_blank'";
        else
          target = "";
        chaine.append("<a  class='liensFooter' href='" + utilisateur.getMesFooters().get(i).getField("FT_LIEN").toString().trim() + "' "
            + target + " >" + utilisateur.getMesFooters().get(i).getField("FT_LIB").toString().trim() + "</a>");
      }
    
    return chaine.toString();
  }
  
  /**
   * Donner le focus � un objet sinon au formulaire de connexion
   */
  private String afficherFocus(String idObjet) {
    if (idObjet == null)
      return "";
    
    String chaine = "";
    chaine = "<script type=\"text/javascript\">" + "if(document.getElementById('" + idObjet.trim() + "')!= null) "
        + " document.getElementById('" + idObjet.trim() + "').focus(); " + "else if (document.getElementById('loginUser')!= null) "
        + " document.getElementById('loginUser').focus(); " + "</script>";
    
    return chaine;
  }
  
  /**
   * 
   * ORIGINAL
   * Permet de mettre en forme les mails du client
   * 
   * string : texte du corps du mail avant mise en forme
   */
  public String afficherMaquetteMail(String texteContenu) {
    StringBuilder texteClasse = new StringBuilder(20000);
    
    texteClasse.append("<!DOCTYPE html>" + "<html lang='fr'>" + "<head>"
        + "<link href='http://resolution-informatique.com:8022/webshop/css/mails.css?" + Math.random() + "' rel='stylesheet'/>" +
        // "<link href='"+MarbreEnvironnement.
        "</head>" + "<body>" + "<table id='presMail'>" + "<tr>" + "<td class='paddingHorizontal'></td>" + "<td id='serveurEmetteur'>"
        + ConstantesEnvironnement.LIBELLE_SERVEUR + "</td>"
        + "<td id='imageDecoration'><img src='http://resolution-informatique.com:8022/webshop/css/image_deco.jpg'/></td>"
        + "<td id='WebShop'><span id='webMail'>Web</span><br/><span id='shopMail'>SHOP</span></td>" + "<td class='paddingHorizontal'></td>"
        + "</tr>" + "<tr ><td  colspan='5' id='paddingTete'></td></tr>" + "</table>" + texteContenu + "<table id='piedDeMail'>"
        + "<tr ><td  colspan='5' class='paddingVertical'></td></tr>"
        + "<tr ><td class='paddingHorizontal'></td><td  colspan='3' class='separationMail'></td><td class='paddingHorizontal'></td></tr>"
        + "<tr ><td  colspan='5' class='paddingVertical'></td></tr>"
        + "<tr ><td class='paddingHorizontal'></td><td  colspan='3' class='messagePiedMail' id='messNoReturn' >Merci de ne pas r�pondre � ce mail : il est g�n�r� automatiquement par notre site.</td><td class='paddingHorizontal'></td></tr>"
        + "<tr ><td class='paddingHorizontal'></td><td  colspan='3' class='messagePiedMail' id='dateMail' >Email envoy� le "
        + mediumDateFormat.format(new Date()) + "</td><td class='paddingHorizontal'></td></tr>"
        + "<tr ><td class='paddingHorizontal'></td><td  colspan='3' class='messagePiedMail' id='webMaster'>Pour toute demande, contactez le <a href='mailto:"
        + ConstantesEnvironnement.MAIL_WEBMASTER + "'>Webmaster</a></td><td class='paddingHorizontal'></td></tr>"
        + "<tr ><td  colspan='5' class='paddingVertical'></td></tr>" + "</table>" + "</body>" + "</html>");
        
    return texteClasse.toString();
  }
  
  /**
   * Afficher Pop up g�n�rale
   */
  private String afficherPopUp(Utilisateur utilisateur, String pop) {
    String retour = "";
    retour = "<div id='filtreTotal'></div>";
    
    retour += "<div id='popUpGenerale'>";
    retour += pop;
    retour += "</div>";
    
    retour += "<div id='traitementEnCours'>" + utilisateur.getTraduction().traduire("$$traitementCours") + "</div>";
    
    return retour;
  }
  
  public String razDuPanier(Utilisateur utilisateur) {
    String retour = "";
    
    retour += "<script type=\"text/javascript\">" + "razPanier(\""
        + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalHT()) + "\");" + "</script>";
    
    return retour;
  }
  
}
