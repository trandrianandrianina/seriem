
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionAccueilBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet AccueilBO
 */
public class AccueilBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AccueilBO() {
    super(new GestionAccueilBO(), "accueilBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "accueilBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficheAccesBO(request.getSession(), "<h2><span id='titrePage'>Gestion du Web Shop</span></h2>"),
          "accueilBO", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private String afficheAccesBO(HttpSession session, String titre) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    ArrayList<GenericRecord> contenuAccesBO =
        (((GestionAccueilBO) maGestion).retourneAccesBO((Utilisateur) session.getAttribute("utilisateur")));
    Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
    
    // int res=-1;
    
    if (utilisateur != null && utilisateur instanceof Utilisateur) {
      retour.append(afficherMessagesAlerteParametrage());
      
      if (contenuAccesBO.size() == 0)
      // le contenu de la table est null, je crée un bouton
      {
        retour.append("<a class = 'liensBO' href = 'accesBO'><span class='titreLienGraphique'>Droits du BO</span></a>");
      }
      else {
        // ArrayList<GenericRecord> listMenu = ((GestionAccueilBO) maGestion).retourneAccueilBO(utilisateur);
        ArrayList<GenericRecord> listMenu = utilisateur.getListeAccesBackOffice();
        if (listMenu != null) {
          for (int i = 0; i < listMenu.size(); i++) {
            if (utilisateur.getUS_ACCES() == (ConstantesEnvironnement.ACCES_RESPON_WS))
              retour.append("<a class='liensBO' href='" + listMenu.get(i).getField("BTN_LINK").toString().trim() + " '>"
                  + listMenu.get(i).getField("BTN_NAME").toString().trim() + "</a>");
            else
              retour.append("<a class='liensBO' href='" + listMenu.get(i).getField("BTN_LINK").toString().trim() + " '>"
                  + listMenu.get(i).getField("BTN_NAME").toString().trim() + "</a>");
          }
          
        }
      }
      
    }
    return retour.toString();
  }
  
  private String afficherMessagesAlerteParametrage() {
    StringBuilder retour = null;
    
    // String etablissementsCoupables = null;
    if (ConstantesEnvironnement.LISTE_ETBS != null && ConstantesEnvironnement.LISTE_ETBS.size() > 0) {
      for (int i = 0; i < ConstantesEnvironnement.LISTE_ETBS.size(); i++) {
        if (!ConstantesEnvironnement.LISTE_ETBS.get(i).isParametresSontDefinis()) {
          if (retour == null) {
            retour = new StringBuilder();
            retour.append("<p class='messageAlerte' >ATTENTION !! Les établissements suivants nécessitent un paramétrage: ");
          }
          retour.append("<a class='etbPourris' href='metierBO?choixEtb=" + ConstantesEnvironnement.LISTE_ETBS.get(i).getCodeETB() + "'>- "
              + ConstantesEnvironnement.LISTE_ETBS.get(i).getLibelleETB() + " (" + ConstantesEnvironnement.LISTE_ETBS.get(i).getCodeETB()
              + ")</a>");
        }
      }
      if (retour != null)
        retour.append("</p>");
    }
    else
      return "";
    
    if (retour == null)
      return "";
    else
      return retour.toString();
  }
  
}

/** =========================fin programme=================================== **/
