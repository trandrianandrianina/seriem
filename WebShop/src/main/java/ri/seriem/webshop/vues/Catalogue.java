
package ri.seriem.webshop.vues;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionCatalogue;
import ri.seriem.webshop.controleurs.GestionMonPanier;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Magasin;
import ri.seriem.webshop.outils.Outils;

/**
 * Servlet implementation class Commandes
 */
public class Catalogue extends MouleVues {
  private static final long serialVersionUID = 1L;
  private ArrayList<Magasin> listeDesMags = null;
  private MonPanier monPanier = null;
  private final int NB_PROMOS_MAX_NAVIGATION = 2;
  
  /**
   * Constructeur par d�faut du catalogue
   */
  public Catalogue() {
    super(new GestionCatalogue(), "catalogue",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "monPanier.css?" + ConstantesEnvironnement.versionWAR
            + "' rel='stylesheet'/><link href='" + ConstantesEnvironnement.DOSSIER_CSS + "catalogue.css?" + ConstantesEnvironnement.versionWAR
            + "' rel='stylesheet'/><script src='scripts/catalogue.js?" + ConstantesEnvironnement.versionWAR + "'></script>",
        ConstantesEnvironnement.ACCES_CLIENT);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("catalogue", "catalogue", "Catalogue", "Catalog"));
    listeFilRouge.add(new FilRouge("listeArticles", "catalogue?retour=1", "Liste d'articles", "Items list"));
    listeFilRouge.add(new FilRouge("unArticle", "catalogue?unArticle=1", "Un article", "Item"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      
      Utilisateur utilisateur = null;
      if ((Utilisateur) request.getSession().getAttribute("utilisateur") != null) {
        utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
      }
      
      // mettre � jour le mode de r�cup�ration de l'utilisateur
      if (request.getParameter("modeRecup") != null) {
        GestionMonPanier panier = new GestionMonPanier();
        if (panier != null)
          panier.majModeRecuperation(utilisateur, request.getParameter("modeRecup"));
      }
      
      // MODE AJAX pour rajouter un article au panier
      if (request.getParameter("panierArticle") != null && request.getParameter("tarifArticle") != null
          && request.getParameter("panierQuantite") != null) {
        if (utilisateur != null) {
          utilisateur.getMonPanier().ajouteArticle(request.getParameter("etbArticle"), request.getParameter("panierArticle"),
              request.getParameter("refFournisseur"), new BigDecimal(request.getParameter("panierQuantite")),
              new BigDecimal(request.getParameter("tarifArticle")), request.getParameter("conditionnement"),
              request.getParameter("uniteVente"), null, true);
          // afficher en LIVE
          response.getWriter().write(getPattern().afficherPanier(utilisateur, true));
        }
      }
      else if (request.getParameter("intuitif") != null && request.getParameter("codeArticle") != null) {
        if (utilisateur != null && utilisateur.getPanierIntuitif() != null) {
          utilisateur.getPanierIntuitif().ajouterUnArticle(utilisateur, request.getParameter("codeArticle"));
          // afficher en LIVE
          response.getWriter().write(getPattern().afficherPanier(utilisateur, true));
        }
      }
      // Si on veut mettre � jour un article dans les favoris
      else if (request.getParameter("favoriETB") != null && request.getParameter("favoriArticle") != null) {
        int retour = 0;
        if (utilisateur != null)
          retour = ((GestionCatalogue) maGestion).majFavoriArticle(utilisateur, request.getParameter("favoriETB"),
              request.getParameter("favoriArticle"));
        if (retour == 1)
          response.getWriter().write("<em id='artPasFavori'>Cet article ne fait pas partie de vos favoris</em>");
        else if (retour == 2)
          response.getWriter().write("<em id='artFavori'>Cet article fait partie de vos favoris</em>");
        else
          response.getWriter().write("<em id='artPasFavori'>pb</em>");
      }
      else {
        ServletOutputStream out = response.getOutputStream();
        
        String connexion = pattern.afficherConnexion(nomPage, request);
        redirectionSecurite(request, response);
        
        out.println(pattern.majDuHead(request, cssSpecifique, " onLoad=\"majTailleNavig();\""));
        
        out.println(pattern.afficherPresentation(nomPage, request, connexion));
        // Mes articles : favoris , consult�s ou achats...etc
        if (request.getParameter("favoris") != null || request.getParameter("historique") != null
            || request.getParameter("consulte") != null || request.getParameter("unPanierIntuitif") != null)
          out.println(afficherContenu(
              afficherUneListeArticles(request, "<h2><span id='titrePage'>Mes articles</span></h2>", request.getParameter("indiceDebut")),
              "listeArticles", null, afficherPopUpRecup(utilisateur, false), request.getSession()));
        // Moteur de recherche ou pagination
        else if (request.getParameter("champRecherche") != null || request.getParameter("groupeArticles") != null
            || request.getParameter("familleArticles") != null || request.getParameter("indiceDebut") != null)
          out.println(afficherContenu(
              afficherUneListeArticles(request,
                  "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$rechercheArticles") + "</span></h2>",
                  request.getParameter("indiceDebut")),
              "listeArticles", null, afficherPopUpRecup(utilisateur, false), request.getSession()));
        // r�cup�ration de la derni�re liste avec ou sans mode de r�cup
        else if ((request.getParameter("retour") != null && utilisateur.getDerniereListeArticle() != null)
            || (request.getParameter("modeRecup") != null && request.getParameter("A1ETB") == null))
          out.println(afficherContenu(afficherUneListeArticles(request, null, null), "listeArticles", null,
              afficherPopUpRecup(utilisateur, false), request.getSession()));
        // r�cup�ration d'un fiche article
        else if (request.getParameter("A1ETB") != null && request.getParameter("A1ART") != null)
          out.println(
              afficherContenu(
                  afficherUnArticle(request,
                      "<h2 id='h2Catalogue'><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$ficheArticle")
                          + "</span></h2>",
                      request.getParameter("A1ETB"), request.getParameter("A1ART"), "catalogue?retour=1"),
                  "unArticle", null, afficherPopUpRecup(utilisateur, request.getParameter("A1ETB"), request.getParameter("A1ART"), false),
                  request.getSession()));
        else if (request.getParameter("intuitif") != null) {
          out.println(afficherContenu(
              afficherLeCatalogue(request.getSession(), "<h2 id='h2Catalogue'><span id='titrePage'>Panier intuitif</span></h2>"), null,
              null, null, request.getSession()));
        }
        // Sinon l'accueil du catalogue
        else if (request.getParameter("panierIntuitif") != null) {
          out.println(afficherContenu(
              afficherLeCatalogueIntuitif(request, "<h2 id='h2Catalogue'><span id='titrePage'>Paniers intuitifs</span></h2>"), null, null,
              null, request.getSession()));
        }
        // Sinon l'accueil du catalogue
        else
          out.println(afficherContenu(
              afficherLeCatalogue(request.getSession(),
                  "<h2 id='h2Catalogue'><span id='titrePage'>" + utilisateur.getTraduction().traduire("Catalogue") + "</span></h2>"),
              null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le d�tail d'un article
   */
  private String afficherUnArticle(HttpServletRequest request, String titre, String etb, String article, String lienRetour) {
    if (request == null || article == null || etb == null)
      return "";
    Utilisateur utilisateur = null;
    StringBuilder retour = new StringBuilder();
    
    retour.append(titre);
    
    if (request.getSession().getAttribute("utilisateur") != null
        && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
      utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
      // Pop up verte de succ�s d'ajout au panier
      retour.append(afficherPopUpAjout());
      
      retour.append("<div class='blocContenu' id='detailArticle' >");
      
      if (ConstantesEnvironnement.MODE_DEBUG)
        System.out.println("[Catalogue] afficherUnArticle: |" + etb + "| -> |" + article + "|");
      
      GenericRecord rcdArt = null;
      rcdArt = ((GestionCatalogue) getMaGestion()).retourneDetailArticle(utilisateur, etb, article);
      GenericRecord tarifArt = null;
      GenericRecord substitution = null;
      ArrayList<GenericRecord> listeVariantes = null;
      
      String CND = "1";
      String CAREF = "";
      String UNITE = "";
      String REF_AFFICHEE = "";
      
      if (rcdArt != null) {
        ((GestionCatalogue) getMaGestion()).ajouterAuxConsultes(utilisateur, etb, article);
        
        if (utilisateur.getETB_EN_COURS().voirSubstitutions()) {
          substitution = ((GestionCatalogue) getMaGestion()).retourneArticleSubstitution(utilisateur, etb, article);
          
          if (utilisateur.getETB_EN_COURS().voir_Subst_Variante()) {
            listeVariantes = ((GestionCatalogue) getMaGestion()).retournerVarianteDunArticle(utilisateur, etb, article);
          }
        }
        
        // attribution de variables
        if (rcdArt.isPresentField("CAREF") && !rcdArt.getField("CAREF").toString().trim().equals(""))
          CAREF = rcdArt.getField("CAREF").toString().trim();
        // Affichage de la r�f�rence soit article soit fournisseur
        if (utilisateur.getETB_EN_COURS().getZone_reference_article() != null) {
          
          if (utilisateur.getETB_EN_COURS().getZone_reference_article().equals("CAREF") && !CAREF.equals(""))
            REF_AFFICHEE = CAREF;
          else if (article != null)
            REF_AFFICHEE = article;
        }
        
        if (rcdArt.isPresentField("A1UNV"))
          UNITE = rcdArt.getField("A1UNV").toString().trim();
        
        // N'afficher le tarif public que si il est adapt� (profils) et param�tr�
        if ((utilisateur.getETB_EN_COURS().isVoir_prix_public() || utilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_REPRES_WS)
            && utilisateur.isAutoriseVuePrix()) {
          tarifArt = ((GestionCatalogue) getMaGestion()).retourneTarifGeneralArticle(utilisateur, etb, article, UNITE);
          if (tarifArt != null && tarifArt.isPresentField("ATP01") && rcdArt.isPresentField("A1TVA")) {
            if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_BOUTIQUE) {
              BigDecimal montantTTC =
                  ((GestionCatalogue) getMaGestion()).retournerTTC(utilisateur, tarifArt.getField("ATP01"), rcdArt.getField("A1TVA"));
              tarifArt.setField("ATP01", montantTTC);
            }
          }
        }
        
        boolean isVendable = false;
        
        if (rcdArt.isPresentField("PROMO")) {
          if (rcdArt.isPresentField("A1LIB")) {
            retour.append("<div id='estEnPromo'><span id='libPromo1'>Article en promotion</span><span id='libPromo2'>"
                + rcdArt.getField("A1LIB").toString().trim() + "</span></div>");
          }
        }
        else {
          retour.append("<h3><span class='puceH3'>&nbsp;</span>");
          if (rcdArt.isPresentField("A1LIB"))
            retour.append(rcdArt.getField("A1LIB").toString().trim());
          retour.append("</h3>");
        }
        
        // Article de substitution
        if (substitution != null) {
          if (substitution.isPresentField("LIBSUB") && substitution.getField("LIBSUB") != null) {
            retour.append("<p id='messagePrincipal'>" + substitution.getField("LIBSUB").toString().trim()
                + "<br/><a class='lienSub' href='catalogue?A1ETB=" + etb + "&A1ART=" + substitution.getField("A1ART").toString().trim()
                + "'>" + substitution.getField("A1ART").toString().trim() + " / " + substitution.getField("A1LIB").toString().trim()
                + "</a>" + "</p>");
          }
        }
        
        retour.append("<div id='blocFiche'>");
        retour.append("<div class='unBloc' id='blocGauche'>");
        
        // <<<<<<<<<< Bloc detail article >>>>>>>>>>
        retour.append("<div class='sousBlocs' id='fi_details'>");
        retour.append("<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$detailsArticle") + "</h5>");
        // code article
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$codeArticle") + "</p>");
        if (rcdArt.isPresentField("A1ART") && !rcdArt.getField("A1ART").toString().trim().equals(""))
          retour.append("<p class='zoneDonnee' id='fi_A1ART'><span class='paddingInterne'>" + rcdArt.getField("A1ART").toString().trim()
              + "</span></p><br/>");
        else
          retour.append("<p class='placeholderSpan' id='fi_A1ART'><span class='paddingInterne'>"
              + utilisateur.getTraduction().traduire("$$reference") + "</span></p><br/>");
        // libelle article
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$libelle") + "</p>");
        if (rcdArt.isPresentField("A1LIB") && !rcdArt.getField("A1LIB").toString().trim().equals(""))
          retour.append("<p class='zoneDonnee' id='fi_A1LIB'><span class='paddingInterne'>" + rcdArt.getField("A1LIB").toString().trim()
              + "</span></p><br/>");
        else
          retour.append("<p class='placeholderSpan' id='fi_A1LIB'><span class='paddingInterne'>"
              + utilisateur.getTraduction().traduire("$$libelle") + "</span></p><br/>");
        
        // Gestion des familles et sous familles
        if (utilisateur.getETB_EN_COURS().isVoir_familles_fiche()) {
          // famille article
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$famille") + "</p>");
          if (rcdArt.isPresentField("A1FAM") && !rcdArt.getField("A1FAM").toString().trim().equals(""))
            retour.append("<p class='zoneDonnee' id='fi_A1FAM'><span class='paddingInterne'>"
                + ((GestionCatalogue) getMaGestion()).retourneInfoFamille(utilisateur, rcdArt.getField("A1FAM").toString().trim())
                + "</span></p><br/>");
          else
            retour.append("<p class='placeholderSpan' id='fi_A1FAM'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$famille") + "</span></p><br/>");
          
          // sous famille article
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$sousFamille") + "</p>");
          if (rcdArt.isPresentField("A1SFA") && !rcdArt.getField("A1SFA").toString().trim().equals(""))
            retour.append("<p class='zoneDonnee' id='fi_A1SFA'><span class='paddingInterne'>"
                + ((GestionCatalogue) getMaGestion()).retourneInfoSousFamille(utilisateur, rcdArt.getField("A1SFA").toString().trim())
                + "</span></p><br/>");
          else
            retour.append("<p class='placeholderSpan' id='fi_A1SFA'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$sousFamille") + "</span></p><br/>");
        }
        
        String taxes = "HT";
        if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_BOUTIQUE)
          taxes = "TTC";
        
        // tarif de base
        if (tarifArt != null && tarifArt.isPresentField("ATP01") && !tarifArt.getField("ATP01").toString().trim().equals("")) {
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$tarifPublic") + " " + taxes + "</p>");
          retour.append("<p class='zoneDonnee' id='fi_ATP01'><span class='paddingInterne'>"
              + Outils.formaterUnTarifArrondi(utilisateur, tarifArt.getField("ATP01")) + "</span></p><br/>");
        }
        
        // CONDITIONNER LE TARIF CLIENT SUR L'ACCES UNIQUEMENT CLIENT
        if (utilisateur.getClient() != null && utilisateur.isAutoriseVuePrix()) {
          // tarif du client
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$votreTarif") + " " + taxes + "</p>");
          if (rcdArt.isPresentField("TARIF") && !rcdArt.getField("TARIF").toString().trim().equals("")) {
            isVendable = ((GestionCatalogue) getMaGestion()).verifierSiVendable((BigDecimal) rcdArt.getField("TARIF"),
                utilisateur.getETB_EN_COURS().isVend_articles_a_zero());
            retour.append("<p class='zoneDonnee'  id='fi_TARIF'><span class='paddingInterne'>"
                + Outils.formaterUnTarifArrondi(utilisateur, rcdArt.getField("TARIF")) + "</span></p><br/>");
          }
          else
            retour.append("<p class='placeholderSpan'><span class='paddingInterne'>" + utilisateur.getTraduction().traduire("$$votreTarif")
                + "</span></p><br/>");
        }
        retour.append(afficherLienFicheFournisseur(utilisateur, etb, article));
        
        retour.append("</div>");
        
        // <<<<<<<<<< Bloc detail fournisseur >>>>>>>>>>
        if (utilisateur.getETB_EN_COURS().isVoir_bloc_fournis_fiche()) {
          retour.append("<div class='sousBlocs'  id='fi_fourniss'>");
          retour.append("<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$fournisseur") + "</h5>");
          // nom du fournisseur
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$fournisseur") + "</p>");
          if (rcdArt.isPresentField("FRNOM") && !rcdArt.getField("FRNOM").toString().trim().equals(""))
            retour.append("<p class='zoneDonnee' id='fi_FRNOM'><span class='paddingInterne'>" + rcdArt.getField("FRNOM").toString().trim()
                + "</span></p><br/>");
          else
            retour.append("<p class='placeholderSpan'  id='fi_FRNOM'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$fournisseur") + "</span></p>");
          
          // reference fournisseur
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$reference") + "</p>");
          if (rcdArt.isPresentField("CAREF") && !rcdArt.getField("CAREF").toString().trim().equals(""))
            retour.append("<p class='zoneDonnee' id='fi_CAREF'><span class='paddingInterne'>" + rcdArt.getField("CAREF").toString().trim()
                + "</span></p><br/>");
          else
            retour.append("<p class='placeholderSpan' id='fi_CAREF'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$reference") + "</span></p>");
          
          retour.append("</div>");
        }
        
        // <<<<<<<<<< Bloc stock et unit� >>>>>>>>>>
        retour.append("<div class='sousBlocs' id= 'fi_stock'>");
        retour.append("<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$disponibilite") + "</h5>");
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$unite") + "</p>");
        // unit� de vente
        if (rcdArt.isPresentField("LIBUNV") && !rcdArt.getField("LIBUNV").toString().trim().equals(""))
          retour.append("<p class='zoneDonnee' id='fi_LIBUNV'><span class='paddingInterne'>" + rcdArt.getField("LIBUNV").toString().trim()
              + "</span></p><br/>");
        else
          retour.append("<p class='placeholderSpan' id='fi_LIBUNV'><span class='paddingInterne'>"
              + utilisateur.getTraduction().traduire("$$unite") + "</span></p>");
        // conditionnement
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$conditionnement") + "</p>");
        if (rcdArt.isPresentField(ConstantesEnvironnement.ZONE_CONDITIONNEMENT)
            && !rcdArt.getField(ConstantesEnvironnement.ZONE_CONDITIONNEMENT).toString().trim().equals("")) {
          CND = afficherValeurCorrectement(utilisateur, rcdArt.getField(ConstantesEnvironnement.ZONE_CONDITIONNEMENT).toString().trim());
          // Si le conditionnement est � 0 afficher 1
          if (CND != null && CND.equals("0"))
            CND = "1";
          retour.append("<p class='zoneDonnee' id='fi_A1CND'><span class='paddingInterne'>" + CND + "</span></p><br/>");
        }
        else
          retour.append("<p class='placeholderSpan' id='fi_A1CND'><span class='paddingInterne'>Condit.</span></p>");
        
        if (utilisateur.isAutoriseVueStockDispo()) {
          // ++++ On a un mode de r�cup�ration dans le magasin
          if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() > ConstantesEnvironnement.MODE_NON_CHOISI
              || utilisateur.getETB_EN_COURS().is_filtre_stock()) {
            // +++ On veut voir le stock de tous les magasins
            if (utilisateur.getETB_EN_COURS().isVoir_stocks_ts_mags()) {
              if (listeDesMags == null)
                listeDesMags = ((GestionCatalogue) maGestion).retournerTousLesMagasins(utilisateur);
              
              String stock = null;
              String livraison = null;
              String dateReassort = null;
              String dateLivraison = null;
              retour.append("<div id='titreMultiStocks'>Stocks disponibles</div>");
              if (listeDesMags != null && listeDesMags.size() > 0)
                for (int i = 0; i < listeDesMags.size(); i++) {
                  // livraison = null;
                  dateReassort = null;
                  stock = ((GestionCatalogue) maGestion)
                      .retourneDispoArticleSQLunMagasin(utilisateur, etb, article, listeDesMags.get(i).getCodeMagasin()).trim();
                  retour.append("<p class='labelDonnee'>- " + listeDesMags.get(i).getLibelleMagasin() + "</p>");
                  // on affiche la valeur du stock ou une icone
                  // dispo/pas dispo
                  if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_AV_VALEUR) {
                    String attendu = null;
                    if (utilisateur.getETB_EN_COURS().isVoir_stock_attendu()) {
                      attendu = ((GestionCatalogue) maGestion).retournerStockAttendu(utilisateur, etb, article,
                          listeDesMags.get(i).getCodeMagasin());
                    }
                    
                    if (attendu == null || attendu.trim().equals("")) {
                      attendu = "";
                    }
                    else {
                      attendu = "/" + afficherStockCorrectement(utilisateur, attendu);
                    }
                    
                    if (listeDesMags.get(i).getCodeMagasin().trim().equals(utilisateur.getMagasinSiege().getCodeMagasin()))
                      livraison = "<p class='zoneStock'><span class='paddingInterne'>" + afficherStockCorrectement(utilisateur, stock)
                          + attendu + "</span></p>";
                    
                    retour.append("<p class='zoneStock'><span class='paddingInterne'>" + afficherStockCorrectement(utilisateur, stock)
                        + attendu + "</span></p>");
                  }
                  // Si on affiche l'image et non le stock
                  else {
                    // Affichage mono magasin ou magasin du
                    // panier uniquement
                    if (utilisateur.getETB_EN_COURS().is_mono_magasin() || !utilisateur.getETB_EN_COURS().isVoir_stocks_ts_mags())
                      retour.append("<p id='lienStock'>" + afficherStockFiche(utilisateur, stock, "") + "</p>");
                    // Affichage multistocks
                    else {
                      if (listeDesMags.get(i).getCodeMagasin().trim().equals(utilisateur.getMagasinSiege().getCodeMagasin()))
                        livraison = "<p class='liensMultiStocks'>" + afficherStockFiche(utilisateur, stock, "mini") + "</p>";
                      retour.append("<p class='liensMultiStocks'>" + afficherStockFiche(utilisateur, stock, "mini") + "</p>");
                    }
                  }
                  
                  // On r�cup�re la date de r�assort pour les
                  // ruptures
                  if (stock != null && Float.parseFloat(stock) <= 0 && utilisateur.getETB_EN_COURS().isVoir_date_reassort()) {
                    retour.append("<p class='reassort' onMouseOver=\"afficherInfoBulle('infoB" + i
                        + "');\" onMouseOut=\"fermerInfoBulle('infoB" + i + "');\">");
                    dateReassort = ((GestionCatalogue) maGestion).retournerDateAttenduMagasin(utilisateur, etb, article,
                        listeDesMags.get(i).getCodeMagasin());
                    // Stocker la donn�e pour la livraison �
                    // partir du magasin si�ge
                    if (listeDesMags.get(i).getCodeMagasin().trim().equals(utilisateur.getMagasinSiege().getCodeMagasin())) {
                      livraison +=
                          "<p class='reassort' onMouseOver=\"afficherInfoBulle('infoX');\" onMouseOut=\"fermerInfoBulle('infoX');\">"
                              + dateReassort + "</p>";
                      dateLivraison = dateReassort;
                    }
                    retour.append(dateReassort);
                    
                    retour.append("</p>");
                    retour.append("<p class='lienReassort' onMouseOver=\"afficherInfoBulle('infoB" + i
                        + "');\" onMouseOut=\"fermerInfoBulle('infoB" + i + "');\">&nbsp;</p>");
                    retour.append("<div class='infoBulle' id='infoB" + i + "'>Date estim�e de la disponibilit� de cet article: "
                        + dateReassort + "</div>");
                  }
                  
                  retour.append("</br>");
                }
              // ++ Cas de la livraison (magasin si�ge)
              if (livraison != null) {
                retour.append("<p class='labelDonnee'><i>* Pour livraison</i></p>");
                retour.append(livraison);
                // Si on a demand� une date de r�assort on affiche
                // ce qu'il faut
                if (dateLivraison != null) {
                  retour.append(
                      "<p class='lienReassort' onMouseOver=\"afficherInfoBulle('infoX');\" onMouseOut=\"fermerInfoBulle('infoX');\">&nbsp;</p>");
                  retour.append(
                      "<div class='infoBulle' id='infoX'>Date estim�e de la disponibilit� de cet article: " + dateLivraison + "</div>");
                }
              }
            }
            // +++ On ne veut voir que le stock du magasin choisi dans
            // le panier
            else {
              // on affiche la valeur du stock
              if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_AV_VALEUR) {
                retour.append("<p class='labelDonnee'>Stock disponible</p>");
                if (rcdArt.isPresentField("STOCK") && !rcdArt.getField("STOCK").toString().trim().equals(""))
                  retour.append("<p class='zoneDonnee' id='fi_STOCK'><span class='paddingInterne'>"
                      + afficherStockCorrectement(utilisateur, rcdArt.getField("STOCK").toString().trim()) + "</span></p>");
                else
                  retour.append("<p class='placeholderSpan' id='fi_STOCK'><span class='paddingInterne'>Stock</span></p>");
              }
              // on affiche une image
              else {
                if (rcdArt.isPresentField("STOCK") && !rcdArt.getField("STOCK").toString().trim().equals(""))
                  retour.append(
                      "<p id='lienStock'>" + afficherStockFiche(utilisateur, rcdArt.getField("STOCK").toString().trim(), "") + "</p>");
              }
              
              // On r�cup�re la date de r�assort pour les ruptures
              if (rcdArt.isPresentField("STOCK") && Float.parseFloat(rcdArt.getField("STOCK").toString().trim()) <= 0
                  && utilisateur.getETB_EN_COURS().isVoir_date_reassort()) {
                String dateReassort = "NR";
                // Si on affiche la valeur du stock
                if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_SANS_VALEUR)
                  retour.append("<p class='labelDonnee'>Date disponibilit�</p>");
                if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_SANS_VALEUR)
                  retour.append(
                      "<p class='zoneDonnee' id='fi_dateReassort' onMouseOver=\"afficherInfoBulle('infoB1');\" onMouseOut=\"fermerInfoBulle('infoB1');\"><span class='paddingInterne'>");
                else
                  retour.append(
                      "<p class='reassort' onMouseOver=\"afficherInfoBulle('infoB1');\" onMouseOut=\"fermerInfoBulle('infoB1');\">");
                
                dateReassort = ((GestionCatalogue) maGestion).retournerDateAttenduMagasin(utilisateur, etb, article,
                    utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
                retour.append(dateReassort);
                
                if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_SANS_VALEUR)
                  retour.append("</span>");
                retour.append("</p>");
                
                retour.append(
                    "<p class='lienReassort' onMouseOver=\"afficherInfoBulle('infoB1');\" onMouseOut=\"fermerInfoBulle('infoB1');\">&nbsp;</p>");
                retour.append(
                    "<div class='infoBulle' id='infoB1'>Date estim�e de la disponibilit� de cet article: " + dateReassort + "</div>");
                
              }
              
              retour.append("<br/>");
              
              String recup = " <b>la r�cup�ration</b> de ";
              String lieurecup = " au magasin - <b>" + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin() + "</b> -";
              
              if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
                recup = " <b>la livraison</b> de ";
                lieurecup = "";
              }
              
              retour.append(
                  "<p id='texteStock'><i id='paddTexteStock'><i id='alertTexteStock'>&nbsp;</i>Cette disponibilit� n'est valable que pour "
                      + recup + " votre commande " + lieurecup + "</i></p>");
            }
          }
          // ++++ On a pas de mode de r�cup
          else {
            if (utilisateur.getUS_ACCES() < ConstantesEnvironnement.ACCES_RESPON_WS && utilisateur.getClient() != null) {
              retour.append("<a id='lienStock' href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\" ><img src='"
                  + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_recup.png'/><span>"
                  + utilisateur.getTraduction().traduire("$$stocks") + "</span></a>");
              retour.append("<p id='texteStock'><i id='paddTexteStock'><i id='alertMajStock'>&nbsp;</i>"
                  + utilisateur.getTraduction().traduire("$$choisirModeRecuperation") + "</i></p>");
            }
          }
        }
        
        retour.append("</div>");
        
        retour.append("</div>");
        
        retour.append("<div class='unBloc' id='blocDroit'>");
        
        // <<<<<<<<<< Bloc photo >>>>>>>>>>
        retour.append("<div class='sousBlocs' id='fi_photo'>" + "<p class='decoH5'>&nbsp;</p><h5>"
            + utilisateur.getTraduction().traduire("$$photo") + "</h5>");
        
        retour.append("</div>");
        
        // <<<<<<<<<< Ajouter au panier >>>>>>>>>>
        retour.append("<div class='sousBlocs'  id='fi_actions'>");
        retour.append("<p class='decoH5'>&nbsp;</p><h5>Actions</h5>");
        if (utilisateur.getMonPanier() != null) {
          String texteSituation = "";
          if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON)
            texteSituation =
                "<b class='spanTextePourPanier'>Votre panier est actuellement configur� pour <i id='spanGras'>une livraison</i>. <a href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\">Changer pour un retrait en magasin</a></b>";
          else if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT)
            texteSituation = "<b class='spanTextePourPanier'>Votre panier est actuellement attribu� au magasin : <i id='spanGras'>"
                + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin()
                + "</i>. <a href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\">Changer pour une livraison ou changer de magasin</a></b>";
          else if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_NON_CHOISI)
            texteSituation =
                "<b class='spanTextePourPanier'>Votre panier n'est pas encore attribu� � un magasin ou une livraison. <a href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\">Attribuer</a></b>";
          retour.append("<p id='textePourPanier'>" + texteSituation + "</p>");
        }
        // Mettre au panier
        if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
          retour.append("<p class='btActions' ><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/><span>" + utilisateur.getTraduction().traduire("$$panier") + "</span></p>");
        }
        else if (utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_CONSULTATION
            && utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_BOUTIQUE && rcdArt.isPresentField("A1ART")
            && !rcdArt.getField("A1ART").toString().trim().equals("") && rcdArt.isPresentField("TARIF")
            && !rcdArt.getField("TARIF").toString().trim().equals("") && isVendable) {
          String lib = rcdArt.getField("A1LIB").toString().trim();
          lib = lib.replace("'", "\\'");
          lib = lib.replace('"', ' ');
          retour.append("<a class='btActions' readonly href=\"javascript:void(0)\"  onClick=\"ouvrirPopPanierComplet('"
              + afficherValeurCorrectement(utilisateur, CND) + "','" + REF_AFFICHEE + "','" + lib + "','"
              + Outils.formaterUnTarifArrondi(utilisateur, rcdArt.getField("TARIF")) + "','" + UNITE + "','"
              + rcdArt.getField("LIBUNV").toString().trim() + "');\"><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/><span>" + utilisateur.getTraduction().traduire("$$panier") + "</span></a>");
          retour.append("<a id='onMetAuPanierInvisible' href=\"javascript:void(0)\"  onClick=\"ajoutPanier('" + REF_AFFICHEE + "','" + etb
              + "','" + article + "','" + rcdArt.getField("TARIF").toString().trim() + "','miniqteSaisie','" + CAREF + "','" + CND + "','"
              + UNITE + "',true);\"><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/><span>" + utilisateur.getTraduction().traduire("$$panier") + "</span></a>");
        }
        else if (utilisateur.getPanierIntuitif() != null) {
          retour.append("<a class='btActions' href=\"javascript:void(0)\" onClick=\"ajouterPanierIntuitif('"
              + rcdArt.getField("A1ART").toString().trim() + "');\" >" + "<img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/>" + "<span>Panier</span></a>");
        }
        else {
          retour.append("<p class='btActions' ><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/><span>" + utilisateur.getTraduction().traduire("$$panier") + "</span></p>");
        }
        // Favoris
        if (utilisateur.getETB_EN_COURS().isVoir_favoris())
          retour.append("<a class='btActions' href=\"javascript:void(0)\" onClick=\"majFavori('" + etb + "','" + article
              + "');\" ><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_favori.png'/><span>"
              + utilisateur.getTraduction().traduire("$$favori") + "</span></a>");
        else
          retour.append("<p class='btActions' id='favoriOpaque'><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_favori.png'/><span>" + utilisateur.getTraduction().traduire("$$favori") + "</span></p>");
        
        retour.append("<a class='btActions' href=\"" + lienRetour + "\"><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/decoration/fiche_retour.png'/><span>" + utilisateur.getTraduction().traduire("$$retour") + "</span></a>");
        
        retour.append("<div id='etatFavori'>" + afficherFavoriArticle(utilisateur, etb, article) + "</div>");
        
        // afficher la fiche fournisseur
        retour.append("</div>");
        retour.append("</div>");
        retour.append("</div>");
        if (listeVariantes != null) {
          retour.append(afficherVariantes(utilisateur, listeVariantes));
        }
        
        // Chargement de l'image apr�s le chargement de la page
        // On r�cup�re le code RUBIS s'il existe
        String retourImage = "<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$photo") + "</h5>";
        
        retourImage += afficherPhoto(rcdArt, etb, article, utilisateur);
        
        retour.append("<script type=\"text/javascript\">");
        retour.append("window.onload= document.getElementById(\"fi_photo\").innerHTML= \"" + retourImage + "\";");
        retour.append("</script> ");
        
      }
      else
        retour.append("Pas de d�tail article");
      
      retour.append("</div>");
      
      retour.append(afficherMajTaillesDiv());
    }
    
    return retour.toString();
  }
  
  /**
   * Afficher le bloc propre au statut Favori de l'article
   */
  private String afficherFavoriArticle(Utilisateur utilisateur, String etb, String article) {
    String retour = "<em id='artPasFavori'>Cet article ne fait pas partie de vos favoris</em>";
    
    if (utilisateur == null || etb == null || article == null)
      return retour;
    
    if (((GestionCatalogue) maGestion).isEnFavori(utilisateur, etb, article) == 2)
      retour = "<em id='artFavori'>Cet article fait partie de vos favoris</em>";
    
    return retour;
  }
  
  /**
   * Afficher la pop up sp�cifique � la mise au panier avec quantit� et
   * conditionnement
   */
  private String afficherPopUpPanier(Utilisateur utilisateur) {
    String retour = "</div>" + "<div id='popUpPanier'>"
        + "<div id='titreMiniPanier'><span id='presentReference'>Ajout d'un article au panier: </span><span id='miniReference'></span>"
        + "<span id='miniLibelle'></span></div>"
        + "<div id='textMiniCondition'>Cet article est conditionn� par <span id='miniCondition'></span> <span id='miniUnite'></span></div>"
        + "<div id='ligneMiniPanier'>"
        + "<a id='miniMoins' href=\"javascript:void(0)\" onClick= \"majQuantiteGlobal('miniqteSaisie','-1');\">-</a>"
        + "<div id='divMiniSaisie'><input id='miniqteSaisie' type='text' name='fuckingQuantite' value='1' maxlength = '5' onKeyPress='only_numeric(event)' onChange=\"majCndGlobal('miniqteSaisie');\" /></div>"
        + "<a id='miniPlus' href=\"javascript:void(0)\" onClick= \"majQuantiteGlobal('miniqteSaisie','1');\">+</a>"
        + "<span id='miniMultipl'>x</span>" + "<div id='miniTarif'></div>"
        + "<a id='miniBtPanier' href=\"javascript:void(0)\" onClick='mettreAuPanierFiche();' ><img id='miniImgPanier' src='"
        + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_panier.png' alt='Mise au panier'/></a>"
        + "<a id='miniBtRetour' href=\"javascript:void(0)\" onClick ='fermerPopPanierComplet();'><img id='miniImgRetour' src='"
        + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_retour.png' alt='Retour'/></a>" + "</div>";
    
    return retour;
  }
  
  /**
   * *Afficher le lien fiche article du fournisseur s'il existe
   * 
   * @param refFournisseur
   * @return
   */
  private String afficherLienFicheFournisseur(Utilisateur utilisateur, String etb, String article) {
    // TODO Stub de la m�thode g�n�r� automatiquement
    String lienFiche = ((GestionCatalogue) maGestion).recupererFicheTechniqueURL(utilisateur, etb, article);
    if (lienFiche != null)
      return "   <a class='btActions' id='ficheTechnique' href='" + lienFiche + "' target = '_BLANK'>" + "<img src='"
          + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_fourniss.png'/>" + "<span>"
          + utilisateur.getTraduction().traduire("$$fiche") + "</span>" + "</a>";
    else
      return "<p class='btActions' id='ficheTechnique' ><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/fiche_fourniss.png'/><span>" + utilisateur.getTraduction().traduire("$$fiche") + "</span></p>";
  }
  
  /**
   * *Afficher la photo de l'article
   * 
   * @param refFournisseur
   * @return
   */
  private String afficherPhoto(GenericRecord record, String etb, String article, Utilisateur utilisateur) {
    String retour = "";
    String nomImage = null;
    String srcImage = null;
    if (record == null)
      return retour;
    
    if (utilisateur.getETB_EN_COURS().getZone_affichage_photos().equals("CAREF") && record.isPresentField("CARFC")
        && !record.getField("CARFC").toString().trim().equals(""))
      nomImage = record.getField("CARFC").toString().trim();
    else if (utilisateur.getETB_EN_COURS().getZone_affichage_photos().equals("CAREF") && record.isPresentField("CAREF")
        && !record.getField("CAREF").toString().trim().equals(""))
      nomImage = record.getField("CAREF").toString().trim();
    else if (utilisateur.getETB_EN_COURS().getZone_affichage_photos().equals("A1ART"))
      nomImage = article;
    else
      return retour;
    
    retour += "<img id='imageArticle' ";
    // r�cup�rer par une m�thode ou une autre la photo de l'article
    // URL
    srcImage = ((GestionCatalogue) maGestion).recupererURLphoto(nomImage, etb, article, utilisateur);
    // IFS
    if (srcImage == null) {
      if (ConstantesEnvironnement.MODE_DEBUG)
        System.out.println("recupererURLphoto() -> NULL");
      srcImage = ((GestionCatalogue) maGestion).recupererIFSphoto(utilisateur, nomImage);
    }
    // Si on a pas r�cup�r� d'URL
    if (srcImage == null) {
      if (ConstantesEnvironnement.MODE_DEBUG)
        System.out.println("recupererIFSphoto() -> NULL");
      srcImage = "src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/imageVide.png'";
    }
    
    retour = "<img id='imageArticle' " + srcImage + " alt='ref�rence " + nomImage + "' title='ref�rence " + nomImage + "' />";
    
    return retour;
  }
  
  /**
   * pop up g�n�rale de la liste factoriser plus tard
   */
  private String afficherPopUpRecup(Utilisateur utilisateur, boolean isRepresentant) {
    String retour = "";
    
    if (monPanier == null)
      monPanier = new MonPanier();
    
    retour += "<div>";
    retour += monPanier.afficherModeDeRecuperation(utilisateur, "Catalogue");
    retour += "</div>";
    
    retour += afficherPopUpPanier(utilisateur);
    
    return retour;
  }
  
  /**
   * Pop up g�n�rale de la fiche article factoriser plus tard
   */
  private String afficherPopUpRecup(Utilisateur utilisateur, String etbArticle, String codeArticle, boolean isRepresentant) {
    String retour = "";
    
    if (monPanier == null)
      monPanier = new MonPanier();
    
    retour += "<div>";
    retour += monPanier.afficherModeDeRecuperation(utilisateur,
        "catalogue?A1ETB=" + etbArticle.toString().trim() + "&A1ART=" + codeArticle.toString().trim());
    retour += "</div>";
    
    retour += afficherPopUpPanier(utilisateur);
    
    return retour;
  }
  
  /**
   * Affiche un morceau de la liste d'articles trouv�e
   */
  private String afficherUneListeArticles(HttpServletRequest request, String titre, String indiceDebut) {
    if (request == null)
      return "";
    Utilisateur utilisateur = null;
    StringBuilder retour = null;
    float nbArt = -1;
    int idDeb = 0;
    String url = null;
    
    // Gestion de session
    HttpSession sess = request.getSession();
    if (sess == null)
      return "";
    
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof Utilisateur) {
      retour = new StringBuilder(15000);
      
      utilisateur = (Utilisateur) sess.getAttribute("utilisateur");
      
      // Gestion du filtre sur le stock positif
      if (request.getParameter("stockPositif") != null) {
        utilisateur.getFiltres().setStock_positif(request.getParameter("stockPositif").equals("1"));
      }
      
      // Mettre � jour le filtre sur les fournisseurs si n�cessaire
      if (request.getParameter("filtreFrs") != null && !request.getParameter("filtreFrs").trim().equals("")) {
        utilisateur.setDernierFiltreFournisseur(request.getParameter("filtreFrs"));
      }
      
      // passer le param�tre de visualisation des stocks si n�cessaire
      if (!utilisateur.getETB_EN_COURS().isVisu_auto_des_stocks())
        utilisateur.setVisuStock(request.getParameter("visuStock") != null);
      
      // Si on a pass� un changement de magasin de retrait
      if (request.getParameter("chgMagasin") != null && utilisateur.getMonPanier() != null)
        utilisateur.getMonPanier().modifierLeMagasinRetrait(request.getParameter("chgMagasin"));
        
      // maj des infos de recherche si on est pas dans le rechargement
      // d'une liste existante
      if (titre != null) {
        utilisateur.setInfosRechercheArticles(request.getParameter("champRecherche"), request.getParameter("groupeArticles"),
            request.getParameter("familleArticles"), request.getParameter("filtreFrs"), request.getParameter("favoris"),
            request.getParameter("historique"), request.getParameter("consulte"), request.getParameter("unPanierIntuitif"), indiceDebut);
        retour.append(titre);
      }
      else
        retour.append("<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$rechercheArticles") + "</span></h2>");
      
      retour.append(afficherPopUpAjout());
      
      // aficher le bloc de navigation du catalogue
      retour.append(afficherNavigationCatalogue(utilisateur));
      
      retour.append("<div class='blocContenu' id='listeArticles' >");
      ArrayList<GenericRecord> liste = null;
      
      // CHERCHER LES ARTICLES A AFFICHER
      // ++++++++++++++++++++++++++++++++++++++++++++++++++
      // Si il s'agit d'une nouvelle requ�te
      if (indiceDebut == null) {
        // Favoris
        if (request.getParameter("favoris") != null)
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesFavoris(utilisateur);
        // historique
        else if (request.getParameter("historique") != null)
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesAchats(utilisateur);
        // historique
        else if (request.getParameter("consulte") != null)
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesConsultes(utilisateur);
        else if (request.getParameter("unPanierIntuitif") != null)
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesPanierIntuitif(utilisateur,
              request.getParameter("unPanierIntuitif"));
        // MOTEUR DE RECHERCHE
        else if (request.getParameter("champRecherche") != null)
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesMoteur(utilisateur, request.getParameter("champRecherche"));
        // Recherche par famille ou sous famille
        else if (request.getParameter("familleArticles") != null)
          liste = ((GestionCatalogue) getMaGestion()).retourneListeArticlesFamille(utilisateur, request.getParameter("familleArticles"));
        // Recherche par groupe
        else if (request.getParameter("groupeArticles") != null)
          liste = ((GestionCatalogue) getMaGestion()).retourneListeArticlesGroupe(utilisateur, request.getParameter("groupeArticles"));
        // rechargement de la derni�re liste
        else if (titre == null) {
          liste = utilisateur.getDerniereListeArticle();
          indiceDebut = utilisateur.getIndiceDebut();
        }
      }
      
      // si on pagine
      utilisateur.setIndiceDebut(indiceDebut);
      // d�terminer l'indice de d�but
      try {
        if (indiceDebut == null)
          idDeb = 0;
        else
          idDeb = Integer.parseInt(indiceDebut);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      
      ((GestionCatalogue) getMaGestion()).filtrerUneListeArticles(utilisateur);
      
      // AFFICHER LES ARTICLES SELECTIONNES
      // ++++++++++++++++++++++++++++++++++++++++++++++++++
      retour.append(afficherTitreListe(request, utilisateur.getDerniereListeArticle()));
      
      // Bilan sur le nombre d'articles total
      if (utilisateur.getDerniereListeArticle() != null)
        nbArt = utilisateur.getDerniereListeArticle().size();
      
      // Si la liste a un souci ou ne poss�de pas d'�l�ments
      if (nbArt <= 0) {
        if (nbArt == -1)
          retour
              .append("<div class='listesCommentaires'><span class='txtCommentaires'>Vos crit�res de saisie sont incorrects</span></div>");
        else if (nbArt == 0) {
          retour.append(retournerBandeauFiltres(utilisateur, url));
          retour.append("<div class='listesCommentaires'><span class='txtCommentaires'>"
              + utilisateur.getTraduction().traduire("$$pasDarticles") + "</span></div>");
        }
      }
      // Si la liste ne comporte qu'un seul article et que nous ne sommes pas en mode filtre...
      else if (nbArt == 1 && request.getParameter("filtreFrs") == null) {
        retour = new StringBuilder(15000);
        retour.append(afficherUnArticle(request, "<h2 id='h2Catalogue'><span id='titrePage'>"
            + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$ficheArticle") + "</span></h2>",
            utilisateur.getDerniereListeArticle().get(0).getField("A1ETB").toString().trim(),
            utilisateur.getDerniereListeArticle().get(0).getField("A1ART").toString().trim(), "catalogue"));
        return retour.toString();
      }
      else {
        retour.append(retournerBandeauFiltres(utilisateur, url));
      }
      
      // Extraire un morceau de la liste afin d'en afficher des pages
      liste = ((GestionCatalogue) getMaGestion()).retourneMorceauListe(utilisateur, idDeb);
      
      // Afficher la liste
      if (liste != null && nbArt > 0)
        retour.append(afficherLignesArticles(liste, utilisateur, nbArt, idDeb));
      
      retour.append("</div>");
      
      retour.append(afficherMajTaillesDiv());
    }
    
    return retour.toString();
  }
  
  /**
   * afficher le bandeau des filtres de la liste des articles s�lectionn�s
   */
  private String retournerBandeauFiltres(Utilisateur pUtil, String url) {
    StringBuilder retour = new StringBuilder();
    HashMap<String, String[]> listeFrs = null;
    
    // Construction de l'url en fonction des derniers crit�res de s�lection
    if (pUtil.getDerniersFavoris() != null)
      url = "Catalogue?favoris=" + pUtil.getDerniersFavoris();
    else if (pUtil.getDernierHistorique() != null)
      url = "Catalogue?historique=" + pUtil.getDernierHistorique();
    else if (pUtil.getDerniersConsultes() != null)
      url = "Catalogue?consulte=" + pUtil.getDerniersConsultes();
    else if (pUtil.getDernierPaniersIntuitifs() != null)
      url = "Catalogue?unPanierIntuitif=" + pUtil.getDernierPaniersIntuitifs();
    else if (pUtil.getDerniereExpression() != null)
      url = "Catalogue?champRecherche=" + pUtil.getDerniereExpression();
    else if (pUtil.getDernierGroupeArticles() != null)
      url = "Catalogue?groupeArticles=" + pUtil.getDernierGroupeArticles();
    
    if (pUtil.getDerniereFamilleArticles() != null)
      url += "&familleArticles=" + pUtil.getDerniereFamilleArticles();
    
    String checked = "";
    if (pUtil.getFiltres().isStock_positif()) {
      checked = "checked";
    }
    
    String selected = "";
    
    retour.append("<div id='blocDesFiltres'>");
    
    retour.append("<input type='checkbox' name='stockPositif' value = '1' id='stockPositif' " + checked
        + " onChange = \"rajouterUnFiltreURL('" + url + "');\" /><label id='lb_stockPositif'>Articles en stock</label>");
    
    retour.append("<select name = 'filtreFrs' id='choixFournisseur' onChange = \"rajouterUnFiltreURL('" + url + "');\" >");
    
    if (pUtil.getDernierFiltreFournisseur() == null) {
      selected = "selected='selected'";
    }
    
    retour.append("<option value = '' " + selected + ">Tous les fournisseurs</option>");
    listeFrs = ((GestionCatalogue) getMaGestion()).retourneListeFournisseur(pUtil);
    if (listeFrs != null && listeFrs.size() > 0) {
      for (Entry<String, String[]> entry : listeFrs.entrySet()) {
        selected = "";
        if (pUtil.getDernierFiltreFournisseur() != null) {
          if (pUtil.getDernierFiltreFournisseur().equals(entry.getValue()[1] + entry.getValue()[2])) {
            selected = "selected='selected'";
          }
        }
        retour.append(
            "<option value = '" + entry.getValue()[1] + entry.getValue()[2] + "' " + selected + ">" + entry.getValue()[0] + "</option>");
      }
    }
    
    retour.append("</select>");
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Affiche le d�tail de chaque ligne article de la liste r�cup�r�e
   */
  private String afficherLignesArticles(ArrayList<GenericRecord> liste, Utilisateur utilisateur, float nbArt, int idDeb) {
    if (liste == null || utilisateur == null)
      return "";
    StringBuilder retour = new StringBuilder(10000);
    boolean vendable = false;
    // si on est en mode admin ou responsable on voit les stocks
    
    String lienFicheArticle = "#";
    String A1ETB = null;
    String A1ART = null;
    String TARIF = null;
    String CAREF = "";
    String REF_AFFICHEE = "Aucune";
    String CND = "1";
    String UNITE = "1";
    String enTetesClients = "";
    
    if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
      enTetesClients = "";
    }
    else if (utilisateur.getMonPanier() != null && utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_CONSULTATION)
      enTetesClients = "<span id='titreActions'>Quantit�</span><a href='#' onClick = \"ajouterToutAuPanier(" + liste.size()
          + ");\" id='panierGlobal' alt='Ajouter tous les articles s�lectionn�s au panier' title='Ajouter tous les articles s�lectionn�s au panier'></a>";
    
    // Ent�te de liste
    retour.append("<div class='listes' id='enTeteListes'>" + "<span id='titreCAREF'>" + utilisateur.getTraduction().traduire("$$reference")
        + "</span>" + "<span id='titreA1LIB'>" + utilisateur.getTraduction().traduire("$$libelle") + "</span>" + "<span id='titreSTOCK'>"
        + utilisateur.getTraduction().traduire("$$dispo") + "</span>" + "<span id='titreTARIF'>");
    if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_BOUTIQUE)
      retour.append("Tarif TTC");
    else if (utilisateur.getClient() != null)
      retour.append("Tarif HT");
    else
      retour.append("Tarif HT");
    retour.append(
        "</span>" + "<span id='titreLIBCND'>" + utilisateur.getTraduction().traduire("$$unite") + "</span>" + enTetesClients + "</div>");
    
    // Affichage des autres magasins pour affiner ses stocks
    if (utilisateur.getETB_EN_COURS().isVoir_stocks_ts_mags()
        && ((utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT) || (utilisateur.getMonPanier() != null
            && utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI && utilisateur.isVisuStock()))) {
      // On est un client
      if (utilisateur.getMonPanier() != null) {
        if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT
            && utilisateur.getMonPanier().getMagasinPanier() != null)
          retour.append(
              "<a href=\"javascript:void(0)\" id='gestionDesStocks'  onClick=\"gererOuvertureStocks();\"><span id='spanEtatStock'>Stocks pour un retrait � <b>"
                  + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin()
                  + "</b></span><span id='spanAutresStocks'>Voir les autres stocks</span></a>");
        else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON && utilisateur.getMagasinSiege() != null)
          retour.append(
              "<a href=\"javascript:void(0)\" id='gestionDesStocks'  onClick=\"gererOuvertureStocks();\"><span id='spanEtatStock'>Stocks disponibles � la livraison</span><span id='spanAutresStocks'>Voir les autres stocks</span></a>");
      }
      else
        retour.append(
            "<a href=\"javascript:void(0)\" id='gestionDesStocks' onClick=\"gererOuvertureStocks();\" ><span>Voir les stocks</span></a>");
      
      retour.append("" + "<div id='magasinsPourStocks' >" + afficherListeMagasinsStock(utilisateur) + "</div>");
    }
    
    String separateur = "";
    // scanner la liste d'articles
    for (int i = 0; i < liste.size(); i++) {
      // les init
      lienFicheArticle = "#";
      A1ETB = null;
      A1ART = null;
      TARIF = null;
      CAREF = "";
      CND = "1";
      UNITE = "UN";
      
      vendable = false;
      
      // liens fiche article et lien mise au panier
      if (liste.get(i).isPresentField("A1ART") && !liste.get(i).getField("A1ART").toString().trim().equals("")) {
        A1ART = liste.get(i).getField("A1ART").toString().trim();
        A1ETB = liste.get(i).getField("A1ETB").toString().trim();
        lienFicheArticle = "catalogue?A1ETB=" + A1ETB + "&amp;A1ART=" + liste.get(i).getField("A1ART").toString().trim();
      }
      // gestion tarif
      if (liste.get(i).isPresentField("TARIF"))
        TARIF = liste.get(i).getField("TARIF").toString().trim();
      // gestion unite
      if (liste.get(i).isPresentField("TARIF"))
        UNITE = liste.get(i).getField("A1UNV").toString().trim();
      
      // Lignes d'articles
      retour.append("<div class='listes'>");
      
      // Affiche si l'article est en promotion ou non
      if (liste.get(i).isPresentField("PROMO")) {
        retour.append("<div class='pastillePromo'>Promotion</div>");
      }
      
      // ++++++++++++++++ REF fournisseur ou article
      retour.append(
          "<a class='CAREF' title='R�f�rence article' alt='R�f�rence article' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      
      if (liste.get(i).isPresentField("CAREF") && !liste.get(i).getField("CAREF").toString().trim().equals(""))
        CAREF = liste.get(i).getField("CAREF").toString().trim();
      
      // Affichage de la r�f�rence soit article soit fournisseur
      if (utilisateur.getETB_EN_COURS().getZone_reference_article() != null) {
        if (utilisateur.getETB_EN_COURS().getZone_reference_article().equals("CAREF") && !CAREF.equals(""))
          REF_AFFICHEE = CAREF;
        else if (A1ART != null)
          REF_AFFICHEE = A1ART;
        else
          REF_AFFICHEE = "Aucune";
      }
      
      retour.append(surlignerExpressionSaisie(utilisateur, REF_AFFICHEE));
      
      retour.append("</a>");
      
      // ++++++++++++++++ libell� article
      retour.append(
          "<a class='A1LIB' title='Libell� article' alt='Libell� article'  href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (liste.get(i).isPresentField("A1LIB") && !liste.get(i).getField("A1LIB").toString().trim().equals(""))
        retour.append(surlignerExpressionSaisie(utilisateur, liste.get(i).getField("A1LIB").toString().trim()));
      else
        retour.append("aucun");
      retour.append("</a>");
      
      // ++++++++++++++++ Stock articles
      // Si on doit int�grer la notion de stock
      if (utilisateur.getMonPanier() != null && utilisateur.isAutoriseVueStockDispo()) {
        if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_NON_CHOISI
            && !utilisateur.getETB_EN_COURS().voir_stock_total()) {
          retour.append(
              "<a class='STOCK' href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\"  title='Affichage du stock de cet article' alt='Affichage du stock de cet article'>");
          retour.append("<span class='pasMagPanier'>&nbsp;</span>");
        }
        else {
          if (utilisateur.isVisuStock() || utilisateur.getETB_EN_COURS().is_filtre_stock()) {
            retour.append("<a class='STOCK' href='" + lienFicheArticle + "'>");
            if (liste.get(i).isPresentField("STOCK") && !liste.get(i).getField("STOCK").toString().trim().equals("")) {
              retour.append(afficherStockCorrectement(utilisateur, liste.get(i).getField("STOCK").toString().trim()));
              
              if (liste.get(i).isPresentField("ATTEN") && !liste.get(i).getField("ATTEN").toString().trim().equals("")
                  && utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI) {
                retour.append("/" + afficherStockCorrectement(utilisateur, liste.get(i).getField("ATTEN").toString().trim()));
              }
            }
            else
              retour.append("<span>NC</span>");
          }
          else {
            retour.append(
                "<a class='STOCK' href='javascript:void(0)' onClick=\"traitementEnCours('catalogue?retour=1&visuStock=1');\" title='visulaliser les stocks' alt='visulaliser les stocks'>");
            retour.append("<span class='pasMagPanier'>&nbsp;</span>");
          }
          retour.append("</a>");
        }
      }
      else
        retour.append("<a class='STOCK' href=\"javascript:void(0)\" >&nbsp;</a>");
      
      separateur = "";
      // ++++++++++++++++ Tarif article
      retour.append("<a class='TARIF' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\"" + A1ETB
          + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (liste.get(i).isPresentField("TARIF") && !liste.get(i).getField("TARIF").toString().trim().equals("")) {
        vendable = ((GestionCatalogue) getMaGestion()).verifierSiVendable((BigDecimal) liste.get(i).getField("TARIF"),
            utilisateur.getETB_EN_COURS().isVend_articles_a_zero());
        retour.append(Outils.formaterUnTarifArrondi(utilisateur, liste.get(i).getField("TARIF")));
        separateur = "/ ";
      }
      else
        retour.append("<span class='aucunStock'>&nbsp;</span>");
      retour.append("</a>");
      
      // L'unit� de vente
      retour.append("<a class='LIBCND' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\"" + A1ETB
          + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (liste.get(i).isPresentField("LIBCND") && !liste.get(i).getField("LIBCND").toString().trim().equals(""))
        retour.append(separateur + liste.get(i).getField("LIBCND").toString().trim().toLowerCase());
      else
        retour.append("&nbsp;");
      retour.append("</a>");
      
      // ++++++++++++++++ Actions de mise au panier
      
      // On conditionne l'option mettre au panier que si: un magasin est associ� au panier,
      // on est un client autoris� et que l'article est vendable
      if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
        
      }
      else if (utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_CONSULTATION
          && utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_BOUTIQUE && utilisateur.getMonPanier() != null && vendable) {
        if (liste.get(i).getField("CND") != null && !liste.get(i).getField("CND").toString().trim().equals("")
            && !liste.get(i).getField("CND").toString().trim().equals("0"))
          CND = liste.get(i).getField("CND").toString().trim();
        
        retour.append("<a class='moinsArticle' href=\"javascript:void(0)\" onClick= \"majQuantiteZone('saisieQ" + i + "','-1','"
            + afficherValeurCorrectement(utilisateur, CND) + "');\">&nbsp;</a>");
        retour.append("<span class='quantiteArticle'><input id='saisieQ" + i
            + "' type ='text' class='saisieQuantite' maxlength = '5' onKeyPress='only_numeric(event)' " + " onChange=\"majCnd('saisieQ" + i
            + "','" + afficherValeurCorrectement(utilisateur, CND) + "');\" tabindex=" + (i + 1) + " /></span>");
        retour.append("<a class='plusArticle' href=\"javascript:void(0)\" onClick= \"majQuantiteZone('saisieQ" + i + "','1','"
            + afficherValeurCorrectement(utilisateur, CND) + "');\">&nbsp;</a>");
        retour.append(
            "<a class='ajoutPanier' title=\"Ajouter l'article au panier\" alt=\"Ajouter l'article au panier\" href=\"javascript:void(0)\" onClick=\"ajoutPanier('"
                + REF_AFFICHEE + "','" + A1ETB + "','" + A1ART + "','" + TARIF + "','saisieQ" + i + "','" + CAREF + "','" + CND + "','"
                + UNITE + "',true);\">&nbsp;</a>");
        retour.append("<textarea class='donneesCachees' id='" + i + "'>" + A1ETB + "$$$" + A1ART + "$$$" + TARIF + "$$$" + CAREF + "$$$"
            + CND + "$$$" + UNITE + "$$$saisieQ" + i + "</textarea>");
      }
      else if (utilisateur.getPanierIntuitif() != null) {
        retour.append(
            "<a class='ajoutPanier' title=\"Ajouter l'article au panier\" alt=\"Ajouter l'article au panier intuitif\" href=\"javascript:void(0)\""
                + " onClick=\"ajouterPanierIntuitif('" + A1ART + "');\">&nbsp;</a>");
      }
      
      // Afficher le nom du fournisseur
      if (utilisateur.getETB_EN_COURS().isFourni_liste_articles()) {
        if (liste.get(i).isPresentField("FRNOM") && !liste.get(i).getField("FRNOM").toString().trim().equals(""))
          retour.append("<div class='libDuFournisseur'>" + liste.get(i).getField("FRNOM").toString().trim() + "</div>");
      }
      // Afficher le conditionnement si il est diff�rent de 1
      if (liste.get(i).getField("CND") != null && liste.get(i).getField("LIBCND") != null)
        retour.append(afficherConditionnement(utilisateur, CND, liste.get(i).getField("LIBCND").toString().trim()));
      
      retour.append("</div>");
    }
    
    utilisateur.setVisuStock(utilisateur.getETB_EN_COURS().isVisu_auto_des_stocks());
    // Pagination
    retour.append(afficherPaginationArticles(utilisateur, nbArt, idDeb, null));
    
    return retour.toString();
  }
  
  /**
   * Afficher la ligne de conditionnement si n�cessaire
   */
  private String afficherConditionnement(Utilisateur utilisateur, String conditionnement, String unite) {
    String retour = "";
    try {
      if (Double.parseDouble(conditionnement) > 1)
        retour += "<div class='conditionnement'>Vendu par " + unite + " et conditionn� par "
            + afficherValeurCorrectement(utilisateur, conditionnement) + "</div>";
    }
    catch (Exception e) {
    }
    
    return retour;
  }
  
  /**
   * Contenu de la pop up d'ajout au panier
   */
  private String afficherPopUpAjout() {
    StringBuilder builder = new StringBuilder();
    
    builder.append("<div id='popUpAjout'>");
    
    // builder.append("<div id='decoAjout'>&nbsp;</div>");
    builder.append("Article ajout� au panier");
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Afficher le script de mise � jour des tailles de Div catalogue/listes
   * articles
   */
  private String afficherMajTaillesDiv() {
    String retour = "";
    
    retour += "<script type='text/javascript'>" + "majTailleNavig();" + "</script>";
    
    return retour;
  }
  
  /**
   * Affiche le catalogue
   */
  private String afficherLeCatalogue(HttpSession sess, String titre) {
    
    if (titre == null || sess == null)
      return "";
    StringBuilder builder = new StringBuilder(5000);
    String lien = null;
    String groupe = null;
    String id = null;
    String classe = null;
    String plusDeFamilles = "";
    String image = null;
    String puce = null;
    int compteurFamilles = 0;
    int compteurMax = 6;
    
    builder.append(titre);
    builder.append("<div class='blocContenu' id='blocCatalogue'>");
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof Utilisateur) {
      ArrayList<GenericRecord> liste =
          ((GestionCatalogue) getMaGestion()).recupereGroupesFamilles((Utilisateur) sess.getAttribute("utilisateur"));
      if (liste != null) {
        
        if ((((Utilisateur) sess.getAttribute("utilisateur")).getETB_EN_COURS().isVoir_favoris()
            || ((Utilisateur) sess.getAttribute("utilisateur")).getETB_EN_COURS().isVoir_art_historique()
            || ((Utilisateur) sess.getAttribute("utilisateur")).getETB_EN_COURS().isVoir_art_consulte())
            && ((Utilisateur) sess.getAttribute("utilisateur")).getUS_ACCES() != ConstantesEnvironnement.ACCES_BOUTIQUE) {
          builder.append("<div class='blocGroupe'>");
          
          builder.append(
              "<a class='groupeCatalogue' id='groupeMesArticles' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?favoris=1');\" >Mes articles</a>"
                  + "<a class='imagegroupeCatalogue' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?favoris=1');\"><img class='imageGroupe' src='"
                  + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/groupeMesImages.png' alt=''/></a>");
          if (((Utilisateur) sess.getAttribute("utilisateur")).getETB_EN_COURS().isVoir_favoris()) {
            builder.append(
                "<a class='familleCatalogue' id='lienMesFavoris' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?favoris=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Mes favoris</a>");
          }
          if (((Utilisateur) sess.getAttribute("utilisateur")).getETB_EN_COURS().isVoir_art_historique()
              && (((Utilisateur) sess.getAttribute("utilisateur")).getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT
                  || ((Utilisateur) sess.getAttribute("utilisateur")).getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS)) {
            builder.append(
                "<a class='familleCatalogue' id='lienMonHistorique' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?historique=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Mes achats</a>");
          }
          if (((Utilisateur) sess.getAttribute("utilisateur")).getETB_EN_COURS().isVoir_art_consulte()) {
            builder.append(
                "<a class='familleCatalogue' id='lienMesConsultes' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?consulte=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>R�cemment consult�s</a>");
          }
          
          if (((Utilisateur) sess.getAttribute("utilisateur")).getPaniersIntuitifsActifs() != null) {
            builder.append(
                "<a class='familleCatalogue' id='lienPanierIntuitif' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?panierIntuitif=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Paniers intuitifs</a>");
          }
          builder.append("</div>");
        }
        for (int i = 0; i < liste.size(); i++) {
          // on sort de la boucle si souci de fields
          if (!liste.get(i).isPresentField("TYPE") || !liste.get(i).isPresentField("LIB") || !liste.get(i).isPresentField("IND")
              || liste.get(i).getField("IND").toString().trim().length() < 1)
            break;
          // Type le lien en fonction de groupe ou famille
          if (liste.get(i).getField("TYPE").toString().trim().equals("GR")) {
            // Si c'est pas le premier groupe de la liste on cloture son affichage
            if (i > 0) {
              // si plus de familles que l'affichage le peut
              if (compteurFamilles > compteurMax)
                builder.append(plusDeFamilles);
              
              builder.append("</div>");
            }
            
            // nouveau groupe
            builder.append("<div class='blocGroupe'>");
            plusDeFamilles = "";
            compteurFamilles = 0;
            
            lien = "groupeArticles=" + liste.get(i).getField("IND").toString().substring(0, 1);
            groupe = lien;
            classe = "groupeCatalogue";
            id = "GR" + liste.get(i).getField("IND").toString().substring(0, 1);
            image = "<img class='imageGroupe' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/" + id + ".png' alt=''/>";
            puce = "";
          }
          else {
            compteurFamilles++;
            
            lien = groupe + "&amp;familleArticles=" + liste.get(i).getField("TYPE") + liste.get(i).getField("IND");
            classe = "familleCatalogue";
            id = "FAM" + liste.get(i).getField("IND").toString().trim();
            image = "";
            puce = "<span class='puceFamilles'>&nbsp;</span>";
          }
          // Si on peut afficher la famille
          if (compteurFamilles <= compteurMax)
            builder.append("<a class='" + classe + "' id='" + id + "' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?"
                + lien + "');\" >" + puce + liste.get(i).getField("LIB").toString().trim() + "</a>");
          
          // s'il s'agit d'un groupe afficher et stocker ce qu'il faut
          if (!image.trim().equals("")) {
            builder.append("<a class='image" + classe + "' id='image" + id
                + "' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?" + lien + "');\" >" + image + "</a>");
            plusDeFamilles = "<a class='familleCatalogue'  href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?" + lien
                + "');\" >[+] " + ((Utilisateur) sess.getAttribute("utilisateur")).getTraduction().traduire("$$autreFamille") + "</a>";
          }
          // fin de liste cloturer l'affichage
          if (i == liste.size() - 1) {
            if (compteurFamilles > compteurMax)
              builder.append(plusDeFamilles);
            builder.append("</div>");
          }
        }
      }
      else
        builder.append("<p>Pas de catalogue � afficher</p>");
    }
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Affiche le catalogue des paniers intuitifs
   */
  private String afficherLeCatalogueIntuitif(HttpServletRequest pRequest, String pTitre) {
    Utilisateur pUtil = null;
    StringBuilder retour = new StringBuilder(1000);
    
    pUtil = (Utilisateur) pRequest.getSession().getAttribute("utilisateur");
    
    retour.append(pTitre);
    
    retour.append("<div class='blocContenu' id='blocCatalogue'>");
    
    if (pUtil.getPaniersIntuitifsActifs() != null) {
      for (GenericRecord record : pUtil.getPaniersIntuitifsActifs()) {
        retour.append("<div class='blocGroupePI'>");
        retour.append("<a class='groupeCatalogue' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?unPanierIntuitif="
            + record.getField("IN_ID").toString().trim() + "');\" >" + record.getField("IN_LIBELLE").toString().trim() + "</a>"
            + "<a class='imageCatalogueIntuitif' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?unPanierIntuitif="
            + record.getField("IN_ID").toString().trim() + "');\">" + "<img class='imagePanierIntuitif' src='"
            + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/panierIntuitif_" + record.getField("IN_ID").toString().trim()
            + ".jpg' alt=''/></a>");
        retour.append("</div>");
      }
    }
    else {
      retour.append("<p>Pas de catalogue � afficher</p>");
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Affiche le titre de la liste *
   */
  private String afficherTitreListe(HttpServletRequest request, ArrayList<GenericRecord> liste) {
    Utilisateur utilisateur = null;
    String retour = "";
    String nbArticles = "";
    
    utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
    
    if (utilisateur.getDernierHistorique() != null)
      retour = "Mes achats r�cents";
    else if (utilisateur.getDerniersFavoris() != null)
      retour = "Mes articles favoris";
    else if (utilisateur.getDernierPaniersIntuitifs() != null)
      retour = "Mon panier intuitif";
    else if (utilisateur.getDerniersConsultes() != null)
      retour = "R�cemment consult�s";
    // si type de recherche � partir du moteur on affiche l'expression dans
    // le titre
    else if (utilisateur.getDerniereExpression() != null)
      retour = utilisateur.getTraduction().traduire("$$motsCles") + " [ "
          + getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getDerniereExpression()) + " ]";
    // si type de recherche � partir d'un groupe on r�cupere le libelle du
    // groupe
    else if (utilisateur.getDerniereFamilleArticles() != null) {
      String grp = null;
      // cas famille
      if (utilisateur.getDerniereFamilleArticles().startsWith("FA")) {
        grp = ((GestionCatalogue) getMaGestion()).retourneInfoFamille(utilisateur, utilisateur.getDerniereFamilleArticles());
        retour = utilisateur.getTraduction().traduire("$$famille") + " ";
      }
      // cas sousfamille
      else {
        grp = ((GestionCatalogue) getMaGestion()).retourneInfoSousFamille(utilisateur, utilisateur.getDerniereFamilleArticles());
        retour = utilisateur.getTraduction().traduire("$$sousFamille") + " ";
      }
      
      if (grp != null)
        retour += "[ " + grp + " ]";
      else
        retour = "";
      
    }
    // si le type de recherche est bas� sur le groupe
    else if (utilisateur.getDernierGroupeArticles() != null) {
      ArrayList<GenericRecord> grp =
          ((GestionCatalogue) getMaGestion()).retourneInfoGroupe(utilisateur, utilisateur.getDernierGroupeArticles());
      if (grp != null && grp.size() > 0) {
        retour = utilisateur.getTraduction().traduire("$$groupe") + " [ " + grp.get(0).getField("LIB") + " ]";
      }
    }
    
    if (liste != null) {
      String pluriel = "";
      if (liste.size() > 1)
        pluriel = "s";
      
      String plafond = "";
      if (liste.size() == utilisateur.getETB_EN_COURS().getLimit_requete_article())
        plafond = utilisateur.getTraduction().traduire("$$plafond");
      
      nbArticles = "<span id='nbArticlesListe'>" + plafond + liste.size() + " " + utilisateur.getTraduction().traduire("$$article")
          + pluriel + "</span>";
    }
    
    retour = "<h3><span class='puceH3'>&nbsp;</span>" + retour + "<span class='aCacher''>" + nbArticles + "</span></h3>";
    
    return retour;
  }
  
  /**
   * Affiche une liste de familles/sous familles correspondant � un groupe
   * s�lectionn�
   */
  private String afficherNavigationCatalogue(Utilisateur utilisateur) {
    StringBuilder retour = new StringBuilder(1000);
    
    retour.append("<div class='blocContenu' id='navigationGroupe'>");
    retour.append("<h2 class='navigCatalogue'>" + utilisateur.getTraduction().traduire("Catalogue") + "</h2>");
    if (utilisateur != null && utilisateur instanceof Utilisateur) {
      // Afficher la liste des groupes si le groupe est null
      if (utilisateur.getDernierGroupeArticles() == null) {
        ArrayList<GenericRecord> lstGrp = ((GestionCatalogue) getMaGestion()).retourneListeGroupes((Utilisateur) utilisateur);
        if (lstGrp != null) {
          for (int i = 0; i < lstGrp.size(); i++)
            retour.append("<a class='h4Groupes' href=\"javascript:void(0)\" onClick='traitementEnCours(\"catalogue?groupeArticles="
                + lstGrp.get(i).getField("IND").toString().substring(0, 1)
                + "\");'><span class='decoGroupes'>+</span><span class='libGroupe'>" + lstGrp.get(i).getField("LIB") + "</span></a>");
        }
      }
      else {
        // Retourne et affiche le libelle du groupe
        ArrayList<GenericRecord> grp =
            ((GestionCatalogue) getMaGestion()).retourneInfoGroupe((Utilisateur) utilisateur, utilisateur.getDernierGroupeArticles());
        if (grp != null && grp.size() > 0) {
          retour.append("<div id='imgDuGroupe'><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/GR"
              + utilisateur.getDernierGroupeArticles() + ".png' alt=''/></div>");
          retour.append(
              "<h4><a class='decoGroupes' href='catalogue'>^</a><span class='libGroupe'>" + grp.get(0).getField("LIB") + "</span></h4>");
        }
        
        ArrayList<GenericRecord> liste = ((GestionCatalogue) getMaGestion()).retournerFamillesPourUnGroupe((Utilisateur) utilisateur,
            utilisateur.getDernierGroupeArticles());
        if (liste != null) {
          String retourFamille = "";
          String retourPartiel = "";
          String classe = "";
          String idFamille = "";
          // String affichage = "";
          String rappel = "";
          int nbSF = 0;
          
          for (int i = 0; i < liste.size(); i++) {
            // Si l'�lement est une famille
            if (liste.get(i).getField("PARTYP").toString().trim().equals("FA")) {
              // affichage de la derni�re famille et ses sous
              // familles
              if (i > 0) {
                if (nbSF > 0)
                  retourFamille = "<a class='accordeonFamille' href=\"javascript:void(0)\" onClick='ouvrirSousFamilles(\"" + idFamille
                      + "\",null,true);'>+</a>" + retourFamille;
                else
                  retourFamille = "<span class='accordeonFamille'>&nbsp;</span>" + retourFamille;
                
                retour.append(retourFamille + retourPartiel);
              }
              
              retourPartiel = "";
              nbSF = 0;
              
              classe = "familleArticle";
              idFamille = "Famille" + liste.get(i).getField("PARIND").toString().trim();
              rappel = idFamille;
              
              retourFamille =
                  "<a class='" + classe + "' id='" + idFamille + "' href='#' onClick='traitementEnCours(\"catalogue?groupeArticles="
                      + utilisateur.getDernierGroupeArticles() + "&amp;familleArticles=" + liste.get(i).getField("PARTYP").toString().trim()
                      + liste.get(i).getField("PARIND").toString().trim()
                      + "\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>"
                      + liste.get(i).getField("LIBELLEPAR").toString().trim() + "</a>";
            }
            // Sous famille
            else if (liste.get(i).getField("PARTYP").toString().trim().equals("SF")) {
              nbSF++;
              classe = "classeSousFamille";
              retourPartiel += "<span class='accordeonSousFamille' id='" + "accordeon" + liste.get(i).getField("PARIND").toString().trim()
                  + "rappel" + rappel + "' >&nbsp;</span>" + "			<a class='" + classe + "' id='" + "sf"
                  + liste.get(i).getField("PARIND").toString().trim() + "rappel" + rappel
                  + "' href='#' onClick='traitementEnCours(\"Catalogue?groupeArticles=" + utilisateur.getDernierGroupeArticles()
                  + "&amp;familleArticles=" + liste.get(i).getField("PARTYP").toString().trim()
                  + liste.get(i).getField("PARIND").toString().trim() + "\");'>" + liste.get(i).getField("LIBELLEPAR").toString().trim()
                  + "</a>";
            }
            else {
              classe = "";
              idFamille = "";
              retourFamille = "";
              retourPartiel = "";
              nbSF = 0;
            }
            
            if (i == liste.size() - 1) {
              if (nbSF > 0)
                retourFamille = "<a class='accordeonFamille' href=\"javascript:void(0)\" onClick='ouvrirSousFamilles(\"" + idFamille
                    + "\",null,true);'>+</a>" + retourFamille;
              else
                retourFamille = "<span class='accordeonFamille'>&nbsp;</span>" + retourFamille;
              
              retour.append(retourFamille + retourPartiel);
            }
          }
        }
        else
          retour.append("<p>Pas de famille pour le groupe</p>");
      }
    }
    
    // Afficher les favoris si param�tr�s
    if ((utilisateur.getETB_EN_COURS().isVoir_favoris() || utilisateur.getETB_EN_COURS().isVoir_art_historique()
        || utilisateur.getETB_EN_COURS().isVoir_art_consulte()) && utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_BOUTIQUE)
      retour.append(afficherNavigationMesArticles(utilisateur));
    
    retour.append(afficherPromotionsNavigation(utilisateur));
    
    retour.append("</div>");
    
    retour.append(afficherMajOuvertureFamilles(utilisateur));
    
    return retour.toString();
  }
  
  /**
   * Afficher le script de mise � jour des tailles de Div catalogue/listes
   * articles
   */
  private String afficherMajOuvertureFamilles(Utilisateur utilisateur) {
    String retour = "";
    
    retour += "<script type='text/javascript'>" + "majOuvertureFamilles(\"" + utilisateur.getDernierGroupeArticles() + "\",\""
        + utilisateur.getDerniereFamilleArticles() + "\");" + "</script>";
    
    return retour;
  }
  
  /**
   * Permet d'afficher les articles li�s � l'utilisateur soit par favoris, ses
   * pr�c�dents paniers ou d'autres crit�res
   */
  private String afficherNavigationMesArticles(Utilisateur utilisateur) {
    String retour = "";
    
    retour += "<h2 class='navigCatalogue' id='navigMesArticles'>Mes articles</h2>";
    // Acc�der � ses favoris
    if (utilisateur.getETB_EN_COURS().isVoir_favoris()) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='mesFavoris' href='#' onClick='traitementEnCours(\"catalogue?favoris=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Mes articles favoris</a>";
    }
    // Acc�der � son historique d'articles
    if (utilisateur.getETB_EN_COURS().isVoir_art_historique() && (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT
        || utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS)) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='monHistorique' href='#' onClick='traitementEnCours(\"catalogue?historique=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Mes achats r�cents</a>";
    }
    // Acc�der � ses r�cemment consutl�s
    if (utilisateur.getETB_EN_COURS().isVoir_art_consulte()) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='mesConsultes' href='#' onClick='traitementEnCours(\"catalogue?consulte=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>R�cemment consult�s</a>";
    }
    // Paniers intuitifs
    if (utilisateur.getPaniersIntuitifsActifs() != null) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='paniersIntuitifs' href='#' onClick='traitementEnCours(\"catalogue?panierIntuitif=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Paniers intuitifs</a>";
    }
    
    return retour;
  }
  
  /**
   * Afficher la liste compl�te des magasins pour changement de visiblit� du
   * stock (mags de retrait ou livraison)
   */
  private String afficherListeMagasinsStock(Utilisateur utilisateur) {
    String retour = "";
    // Si cette liste n'est pas initialis�e
    if (listeDesMags == null)
      listeDesMags = ((GestionCatalogue) maGestion).retournerTousLesMagasins(utilisateur);
    
    retour += "<div id='divInterneMags'>"
        + "<a href=\"javascript:void(0)\" class='changeMags' onClick=\"gererOuvertureStocks();\" ><span class='libMagChoix'>Pas de changement</span><span class='choisirNvMag' id='annulerChangement'>&nbsp;</span></a>";
    if (listeDesMags != null && listeDesMags.size() > 0)
      for (int i = 0; i < listeDesMags.size(); i++) {
        // Si il ne s'agit pas du magasin courant dans le panier
        if (utilisateur.getMonPanier() != null
            && (!utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin().equals(listeDesMags.get(i).getCodeMagasin())
                || utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON))
          retour += "<a onClick=\"traitementEnCours('catalogue?visuStock=1&modeRecup=" + ConstantesEnvironnement.MODE_RETRAIT + "&chgMagasin="
              + listeDesMags.get(i).getCodeMagasin() + "');\" href='javascript:void(0)' class='changeMags'>"
              + "<span class='libMagChoix'>Stocks pour un retrait � " + listeDesMags.get(i).getLibelleMagasin()
              + "</span><span class='choisirNvMag'>&nbsp;</span>" + "</a>";
      }
    if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_LIVRAISON)
      retour += "<a onClick=\"traitementEnCours('catalogue?visuStock=1&modeRecup=" + ConstantesEnvironnement.MODE_LIVRAISON
          + "');\" href='javascript:void(0)' class='changeMags'><span class='libMagChoix'>Voir les stocks pour une livraison</span><span class='choisirNvMag'>&nbsp;</span></a>";
    if (utilisateur.getETB_EN_COURS().voir_stock_total()) {
      retour += "<a onClick=\"traitementEnCours('catalogue?visuStock=1&modeRecup=" + ConstantesEnvironnement.MODE_RETRAIT + "&chgMagasin="
          + Magasin.TOUS_MAGS + "');\" href='javascript:void(0)' class='changeMags'>"
          + "<span class='libMagChoix'>Stocks tous magasins</span><span class='choisirNvMag'>&nbsp;</span>" + "</a>";
    }
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * On affiche les promotions li�es � un groupe ou une famille s�lectionn�e
   */
  private String afficherPromotionsNavigation(Utilisateur pUtil) {
    if (pUtil == null || pUtil.getListePromotionsActives() == null) {
      return "";
    }
    
    StringBuilder builder = new StringBuilder();
    
    ArrayList<GenericRecord> listePromos = null;
    
    // On recherche par Famille ou sous famille
    if (pUtil.getDerniereFamilleArticles() != null || pUtil.getDernierGroupeArticles() != null) {
      listePromos = GestionCatalogue.recupererPromotionsParCatalogue(pUtil, NB_PROMOS_MAX_NAVIGATION);
    }
    
    if (listePromos != null) {
      builder.append("<div id='promosNavigation'>");
      
      for (GenericRecord record : listePromos) {
        String lienArticle = "#";
        if (record.isPresentField("PR_A1ETB") && record.isPresentField("PR_A1ART")) {
          lienArticle = "catalogue?A1ETB=" + record.getField("PR_A1ETB").toString().trim() + "&A1ART="
              + record.getField("PR_A1ART").toString().trim();
        }
        
        builder.append("<div class='unePromo'>");
        
        if (record.isPresentField("PR_PRIX")) {
          if (((BigDecimal) record.getField("PR_PRIX")).compareTo(BigDecimal.ZERO) > 0) {
            builder.append("<div class='prixPromo'>");
            builder.append(Outils.formaterUnTarifArrondi(pUtil, record.getField("PR_PRIX")));
            builder.append("</div>");
          }
        }
        builder.append("<a class='imagePromotion' href='" + lienArticle + "'>");
        builder.append("<img class='imagePromo' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/promotions/promotion_"
            + record.getField("PR_ID") + ".jpg'/>");
        builder.append("</a>");
        
        builder.append("<div class='detailsPromotion'>");
        if (record.isPresentField("PR_A1LIB") && !record.getField("PR_A1LIB").toString().trim().equals("")) {
          builder.append("<span class='detailLibPromo'>" + record.getField("PR_A1LIB").toString().trim() + "</span>");
        }
        builder.append("<span class='textePromo'>" + record.getField("PR_TEXTE").toString().trim() + "</span>");
        builder.append("<span class='validPromo'>Jusqu'au "
            + Outils.transformerDateSeriemEnHumaine(record.getField("PR_DATEF").toString().trim()) + "</span>");
        builder.append("</div>");
        
        builder.append("</div>");
      }
      
      builder.append("</div>");
    }
    else
      return "";
    
    return builder.toString();
  }
  
  /**
   * On affiche la liste des articles en variantes
   */
  private String afficherVariantes(Utilisateur pUtil, ArrayList<GenericRecord> pVariantes) {
    if (pVariantes == null || pVariantes.size() == 0) {
      return "";
    }
    
    StringBuilder builder = new StringBuilder();
    String A1ETB = null;
    String A1ART = null;
    String CAREF = "";
    String REF_AFFICHEE = "Aucune";
    
    builder.append("<div class='sousBlocs'  id='fi_variantes'>");
    builder.append("<p class='decoH5'>&nbsp;</p><h5>Articles associ�s</h5>");
    
    for (GenericRecord article : pVariantes) {
      if (article.isPresentField("A1ETB") && !article.getField("A1ETB").toString().trim().equals("")) {
        A1ETB = article.getField("A1ETB").toString().trim();
      }
      if (article.isPresentField("A1ART") && !article.getField("A1ART").toString().trim().equals("")) {
        A1ART = article.getField("A1ART").toString().trim();
      }
      
      builder.append("<div class='listes'>");
      // ++++++++++++++++ REF fournisseur ou article
      builder.append(
          "<a class='CAREF' title='R�f�rence article' alt='R�f�rence article' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      
      if (article.isPresentField("CAREF") && !article.getField("CAREF").toString().trim().equals(""))
        CAREF = article.getField("CAREF").toString().trim();
      
      // Affichage de la r�f�rence soit article soit fournisseur
      if (pUtil.getETB_EN_COURS().getZone_reference_article() != null) {
        if (pUtil.getETB_EN_COURS().getZone_reference_article().equals("CAREF") && !CAREF.equals(""))
          REF_AFFICHEE = CAREF;
        else if (A1ART != null)
          REF_AFFICHEE = A1ART;
        else
          REF_AFFICHEE = "Aucune";
      }
      
      builder.append(surlignerExpressionSaisie(pUtil, REF_AFFICHEE));
      
      builder.append("</a>");
      
      // ++++++++++++++++ libell� article
      builder.append(
          "<a class='A1LIB' title='Libell� article' alt='Libell� article'  href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (article.isPresentField("A1LIB") && !article.getField("A1LIB").toString().trim().equals(""))
        builder.append(surlignerExpressionSaisie(pUtil, article.getField("A1LIB").toString().trim()));
      else
        builder.append("aucun");
      builder.append("</a>");
      builder.append(
          "<a class='FRNOM' title='Fournisseur' alt='Fournisseur'  href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (article.isPresentField("FRNOM") && !article.getField("FRNOM").toString().trim().equals(""))
        builder.append(surlignerExpressionSaisie(pUtil, article.getField("FRNOM").toString().trim()));
      else
        builder.append("");
      builder.append("</a>");
      builder.append("</div>");
    }
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Surligner l'expression saisie dans le moteur dans la chaine pass�e en
   * param�tre
   */
  private String surlignerExpressionSaisie(Utilisateur utilisateur, String libelle) {
    if (utilisateur == null || libelle == null)
      return libelle;
    
    if (utilisateur.getDerniereExpression() != null && !utilisateur.getDerniereExpression().trim().equals("")) {
      String expression = "\\b" + utilisateur.getDerniereExpression().trim() + "\\b";
      Pattern patternRecherche = Pattern.compile(expression);
      Matcher matcher = patternRecherche.matcher(libelle);
      libelle = matcher.replaceAll("<u class='surligne'>" + utilisateur.getDerniereExpression() + "</u>");
    }
    
    return libelle;
  }
  
}
