
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionDebug;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class Debug
 */
public class Debug extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public Debug() {
    super(new GestionDebug(), "debug", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "debug.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficherDebug((Utilisateur) request.getSession().getAttribute("utilisateur"),
          "<h2><span id='titrePage'>Param�tres DEBUG</span></h2>", request), null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des VARIABLES DE DEBUG table[DEBUG] et possibilit� de modifier leur valeurs
   */
  private String afficherDebug(Utilisateur utilisateur, String titre, HttpServletRequest request) {
    if (utilisateur == null || titre == null)
      return "";
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    // En cas de traitement des donn�es modifi�es
    if (request.getParameter("paramDebugs") != null) {
      if (((GestionDebug) maGestion).majVariablesDebug(utilisateur, request) > 0)
        retour.append("<div id='messagePrincipal'>Donn�es valid�es</div>");
      else
        retour.append("<div id='messageAlerte'>Probl�me de validation</div>");
    }
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Les variables de DEBUG &nbsp;&nbsp;</h3>");
    
    utilisateur.setListeDeTravail(((GestionDebug) maGestion).recupererVariablesDebug(utilisateur));
    
    if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0) {
      
      String isFalseChecked = "checked";
      String isTrueChecked = "checked";
      
      retour.append("<form action='debug' method ='POST'>");
      
      // Titre tableau
      retour.append("<div id='lignesFormTitre'>" + "<label id='labelparamTitre'>VARIABLE</label>"
          + "<span class='spanRadiosTitre'>INACTIF</span>" + "<span class='spanRadiosTitre'>ACTIF</span>" + "</div>");
      
      // on liste tous les param�tres de debug
      for (int i = 0; i < utilisateur.getListeDeTravail().size(); i++) {
        if (utilisateur.getListeDeTravail().get(i).isPresentField("DEB_CLE")
            && utilisateur.getListeDeTravail().get(i).isPresentField("DEB_VALEUR")) {
          // on checke les valeurs en fonction de leur �tat dans la base
          if (utilisateur.getListeDeTravail().get(i).getField("DEB_VALEUR").toString().trim().equals("0")) {
            isTrueChecked = "";
            isFalseChecked = "checked";
          }
          else {
            isTrueChecked = "checked";
            isFalseChecked = "";
          }
          
          retour.append(
              "<div class='lignesFormulaires'>" + "<label class='labelparam'>" + utilisateur.getListeDeTravail().get(i).getField("DEB_CLE")
                  + "</label>" + "<span class='spanRadios'><input type='radio' class='btnRadio' name='"
                  + utilisateur.getListeDeTravail().get(i).getField("DEB_CLE") + "' value='0' " + isFalseChecked + "/></span>"
                  + "<span class='spanRadios'><input type='radio' class='btnRadio' name='"
                  + utilisateur.getListeDeTravail().get(i).getField("DEB_CLE") + "' value='1' " + isTrueChecked + " /></span>" + "</div>");
        }
      }
      retour.append("<input type = 'hidden' name='paramDebugs' value='1'/>");
      retour.append("<input type='submit' value='Modifier' id='btValDebug'/>");
      retour.append("</form>");
    }
    
    return retour.toString();
  }
  
}
