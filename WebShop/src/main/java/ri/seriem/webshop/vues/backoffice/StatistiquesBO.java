
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionStatistiquesBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class StatistiquesBO
 */
public class StatistiquesBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public StatistiquesBO() {
    super(
        new GestionStatistiquesBO(), "statistiquesBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS
            + "statistiquesBO.css' rel='stylesheet'/><link href='" + ConstantesEnvironnement.DOSSIER_CSS + "c3.min.css' rel='stylesheet' />",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if (request.getParameter("ca") != null)
        out.println(afficherContenu(afficherCAWeb(request.getSession(), "<h2><span id='titrePage'>Statistiques du Web Shop</span></h2>",
            request.getParameter("ca"), request), null, null, null, request.getSession()));
      else if (request.getParameter("cnx") != null)
        out.println(
            afficherContenu(afficherConnexionsClients(request.getSession(), "<h2><span id='titrePage'>Statistiques du Web Shop</span></h2>",
                request.getParameter("cnx"), request), null, null, null, request.getSession()));
      else
        out.println(
            afficherContenu(afficherAccueilStats(request.getSession(), "<h2><span id='titrePage'>Statistiques du Web Shop</span></h2>"),
                null, null, null, request.getSession()));
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * affiche l'accueil statistique
   * 
   */
  private String afficherAccueilStats(HttpSession session, String titre) {
    String retour = titre;
    
    retour += "<div class='blocContenu'>";
    
    retour += afficherBoutonStats();
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * affiche les 2 boutons statistiques
   * @return
   */
  private String afficherBoutonStats() {
    String chaine = "";
    
    chaine += "<a class='statistiquesBO' href='StatistiquesBO?ca=1'>Statistiques CA</a>";
    chaine += "<a class='statistiquesBO' href='StatistiquesBO?cnx=1'>Statistiques Connexions Clients</a>";
    
    return chaine;
  }
  
  /**
   * affiche les statistiques par ann�e du CA et son graphe.
   */
  private String afficherCAWeb(HttpSession session, String titre, String an, HttpServletRequest request) {
    Utilisateur utilisateur = null;
    String page = "";
    StringBuilder builder = new StringBuilder();
    builder.append(titre);
    builder.append("<div class='blocContenu'>");
    
    builder.append(afficherBoutonStats());
    
    // Affichage des bontons statistiques
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof Utilisateur) {
      utilisateur = (Utilisateur) session.getAttribute("utilisateur");
      
      ArrayList<GenericRecord> liste = ((GestionStatistiquesBO) getMaGestion()).retournerCAWeb(utilisateur, an);
      
      // affiche le titre
      builder.append("<h3><span class='puceH3'>&nbsp;</span>CA commande Web  </h3>");
      // affiche le selecteur de date
      page = "ca";
      builder.append(afficherSelectionAn(page, an));
      if (liste != null) {
        
        // ent�tes de colonnes
        builder.append(
            "<div class='listes' id='enTeteListes'>" + "<span id='titreMOIS'>Mois</span>" + "<span id='titreCA'>CA</span>" + "</div>");
        
        for (int i = 0; i < liste.size(); i++) {
          builder.append("<a class='listes'>");
          
          builder.append("<span id='MOIS'>");
          if (liste.get(i).isPresentField("MOIS"))
            builder.append(Outils.retourneMois(liste.get(i).getField("MOIS").toString().trim()));
          else
            builder.append("&nbsp;");
          builder.append("</span>");
          
          builder.append("<span id='CA'>");
          if (liste.get(i).isPresentField("CA"))
            builder.append(Outils.formaterUnTarifArrondi(utilisateur, liste.get(i).getField("CA")));
          else
            builder.append("&nbsp;");
          builder.append("</span>");
          
          builder.append("</a>");
          
        }
      }
    }
    
    /** GRAPHE **/
    // builder.append("<div id='chart'></div>");
    builder.append("<script type ='text/javascript>" + // "jQuery.ready(function()" +
        
        "$(function { " + "var myChart = new Highcharts.chart('container', {" + "  chart: {" + "  type: 'bar'" + "  }," + " title: {"
        + "  text: 'Fruit Consumption'" + "  }," + " xAxis: {" + "   categories: ['Apples', 'Bananas', 'Oranges']" + "  }," + "  yAxis: {"
        + "  title: {" + "   text: 'Fruit eaten'" + "}" + "}," + " series: [{" + " name: 'Jane'," + " data: [1, 0, 4]" + " }, {"
        + "  name: 'John'," + " data: [5, 7, 3]" + "}]" + "});" + "});" + builder.append("</script>"));
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * affiche les statistiques du nombre de connexion par ann�e
   */
  private String afficherConnexionsClients(HttpSession session, String titre, String an, HttpServletRequest request) {
    Utilisateur utilisateur = null;
    String page = "";
    // String retour = titre;
    StringBuilder builder = new StringBuilder();
    builder.append(titre);
    builder.append("<div class='blocContenu'>");
    
    builder.append(afficherBoutonStats());
    
    // Affichage des bontons statistiques
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof Utilisateur) {
      utilisateur = (Utilisateur) session.getAttribute("utilisateur");
      
      ArrayList<GenericRecord> liste = ((GestionStatistiquesBO) getMaGestion()).retournerConnexionsclients(utilisateur, an);
      
      // affiche le titre
      builder.append("<h3><span class='puceH3'>&nbsp;</span>Nombre de connexions clients </h3>");
      // System.out.println(liste.size());
      // affiche le selecteur de date
      page = "cnx";
      builder.append(afficherSelectionAn(page, an));
      
      if (liste != null) {
        
        // ent�tes de colonnes
        builder.append("<div class='listes' id='enTeteListes'>" + "<span id='titreMOIS'>Mois</span>"
            + "<span id='titreCNX'>Connexions clients</span>" + "</div>");
        for (int i = 0; i < liste.size(); i++) {
          
          builder.append("<a class='listes'>");
          
          builder.append("<span id='MOIS'>");
          if (liste.get(i).isPresentField("MOIS"))
            builder.append(Outils.retourneMois(liste.get(i).getField("MOIS").toString().trim()));
          else
            builder.append("&nbsp;");
          builder.append("</span>");
          
          builder.append("<span id='CNX'>");
          if (liste.get(i).isPresentField("CNX"))
            builder.append(liste.get(i).getField("CNX").toString().trim());
          else
            builder.append("&nbsp;");
          builder.append("</span>");
          
          builder.append("</a>");
        }
        builder.append("</div>");
      }
    }
    builder.append("</div>");
    return builder.toString();
  }
  
  /**
   * Affiche la combobox pour choisir son ann�e
   */
  private String afficherSelectionAn(String page, String an) {
    String chaine = "";
    String sel = "";
    
    int a = Calendar.getInstance().get(Calendar.YEAR);
    ;
    
    chaine += "<select name='choixan' id='choixan' size='1' onChange='location = this.options[this.selectedIndex].value;'>";
    chaine += ("<option value=''>choisir une ann�e</option>");
    for (int i = 1; i < 6; i++) {
      if (an.equals(Integer.toString(a)))
        sel = "selected=\"selected\"";
      else
        sel = "";
      
      chaine += "<option " + sel + "value='StatistiquesBO?" + page + "=" + a + "'>" + a + "</option>";// onClick=\"dataGraph()\"
      a = a - 1;
      
    }
    chaine += "</select>";
    
    return chaine;
  }
}
