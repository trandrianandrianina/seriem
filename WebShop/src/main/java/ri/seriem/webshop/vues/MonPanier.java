
package ri.seriem.webshop.vues;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionMesDonnees;
import ri.seriem.webshop.controleurs.GestionMonPanier;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.PanierArticle;
import ri.seriem.webshop.metier.PanierArticleNormal;
import ri.seriem.webshop.outils.EnvoiMail;
import ri.seriem.webshop.outils.Outils;

/**
 * Servlet MonPanier c'est l'affichage du panier de l'utilisateur
 */
public class MonPanier extends MouleVues {
  private static final long serialVersionUID = 1L;
  private EnvoiMail mails = null;
  
  /**
   * Constructeur du panier
   */
  public MonPanier() {
    super(new GestionMonPanier(), "monPanier",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "monPanier.css?" + ConstantesEnvironnement.versionWAR
            + "' rel='stylesheet'/>" + "<link rel='stylesheet' href='jquery/jquery-ui.css'>"
            + "<script src='jquery/external/jquery/jquery.js'></script>" + "<script src='jquery/jquery-ui.js'></script>"
            + "<script src='jquery/datepicker-fr.js'></script>" + "<script src='scripts/monPanier.js?" + ConstantesEnvironnement.versionWAR
            + "'></script>",
        ConstantesEnvironnement.ACCES_CLIENT);
    
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("catalogue", "catalogue?retour=1", "Catalogue", "Catalog"));
    listeFilRouge.add(new FilRouge("monPanier", "catalogue", "Mon panier", "My basket"));
    
    if (logs != null)
      logs.setIS_EN_DEBUG_SPEC(ConstantesDebug.DEBUG_PANIER);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
      // Mode AJAX pour mettre � jour les infos de livraison/retrait dans le panier de l'utilisateur
      if (request.getParameter("modificationBlocAdresse") != null) {
        String zone = null;
        
        if (request.getParameter("idCLNOM") != null)
          zone = "idCLNOM";
        else if (request.getParameter("idCLCPL") != null)
          zone = "idCLCPL";
        else if (request.getParameter("idCLRUE") != null)
          zone = "idCLRUE";
        else if (request.getParameter("idCLLOC") != null)
          zone = "idCLLOC";
        else if (request.getParameter("idCLVIL") != null)
          zone = "idCLVIL";
        else if (request.getParameter("idCLPOS") != null)
          zone = "idCLPOS";
        else if (request.getParameter("idDetailsRetrait") != null)
          zone = "idDetailsRetrait";
        else if (request.getParameter("idDateRetrait") != null)
          zone = "idDateRetrait";
        else if (request.getParameter("idReferenceLongue") != null)
          zone = "idReferenceLongue";
        
        // Mettre � jour les variables en fonction du mode dans lequel on est REPRESENTANT OU CLIENT
        ((GestionMonPanier) maGestion).majVariablesPanier(utilisateur, zone, request.getParameter(zone));
      }
      else {
        // Supprimer un article du panier avant d'afficher le panier dans la navigation persistante
        if (request.getParameter("SupArticle") != null)
          utilisateur.getMonPanier().supprimeArticle(request.getParameter("SupEtbArt"), request.getParameter("SupArticle"));
        
        // mettre � jour le mode de r�cup�ration de l'utilisateur
        if (request.getParameter("modeRecup") != null)
          ((GestionMonPanier) maGestion).majModeRecuperation(utilisateur, request.getParameter("modeRecup"));
        
        // Modifier la quantit� du panier
        if (request.getParameter("panierArticle") != null && request.getParameter("panierQuantite") != null)
          utilisateur.getMonPanier().modificationArticle(request.getParameter("panierArticle"),
              new BigDecimal(request.getParameter("panierQuantite")));
        
        if (request.getParameter("nvMagasin") != null)
          utilisateur.getMonPanier().modifierLeMagasinRetrait(request.getParameter("nvMagasin"));
        
        ServletOutputStream out = response.getOutputStream();
        String connexion = pattern.afficherConnexion(nomPage, request);
        
        redirectionSecurite(request, response);
        
        // Affichage d'en-t�te
        out.println(pattern.majDuHead(request, cssSpecifique, null));
        out.println(pattern.afficherPresentation(nomPage, request, connexion));
        // traitement de la validation du panier en commande ou devis
        if (request.getParameter("validation") != null) {
          if (request.getParameter("validation").equals("1") || request.getParameter("validation").equals("2"))
            out.println(afficherContenu(afficherValidationPanier(utilisateur, request), "monPanier", null, null, request.getSession()));
          else if (request.getParameter("validation").equals("0"))
            traitementAnnulationPanier(utilisateur, request, response);
        }
        // Affichage simple du panier
        else {
          if (utilisateur != null) {
            String titre = "";
            if (utilisateur.getMonPanier() != null) {
              if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT
                  || utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
                titre = "Ma commande";
              }
              else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_DEVIS) {
                titre = "Mon devis";
              }
              else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_NON_CHOISI) {
                titre = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$monPanier");
              }
            }
            out.println(afficherContenu(afficherPanier(((Utilisateur) request.getSession().getAttribute("utilisateur")),
                "<h2><span id='titrePage'>" + titre + "</span></h2>"), "monPanier", null, null, request.getSession()));
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * L'utilisateur annule panier courant. Il faut donc tout supprimer et
   * afficher l'ancienne liste d'articles
   */
  private void traitementAnnulationPanier(Utilisateur utilisateur, HttpServletRequest request, HttpServletResponse reponse) {
    if (utilisateur == null || request == null)
      return;
    
    if (utilisateur.razPanier())
      try {
        getServletContext().getRequestDispatcher("/catalogue?retour=1").forward(request, reponse);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
  }
  
  /**
   * retour du passage du panier � la commande dans S�rie M
   */
  private String afficherValidationPanier(Utilisateur utilisateur, HttpServletRequest request) {
    String retour = "";
    if (request == null)
      return "";
    
    retour += "<div class='blocContenu'>";
    
    if (utilisateur != null) {
      LinkedHashMap<String, String> rapport = new LinkedHashMap<String, String>();
      
      String typeBon = "ATT";
      String typeDemande = "commande";
      
      if (request.getParameter("validation") != null)
        if (request.getParameter("validation").equals("1")) {
          // DEA devis en attente DEV devis valid�
          typeBon = "DEA";
          typeDemande = "demande de devis";
        }
      
      // Si le panier contient des articles
      if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getListeArticles() != null
          && utilisateur.getMonPanier().getListeArticles().size() > 0) {
        // On valide la commande et on checke le r�sultat
        if (utilisateur.getMonPanier().valideCommande(typeBon, null, rapport)) {
          // si on bien inject� le bon
          if (rapport.containsKey("TYPE") && rapport.containsKey("ETB") && rapport.containsKey("NUMERO")
              && rapport.containsKey("SUFFIXE")) {
            if (typeBon.equals("ATT"))
              typeBon = "E";
            else if (typeBon.equals("DEA"))
              typeBon = "D";
            else
              typeBon = "M";
            
            retour += "<h3><span class='puceH3'>&nbsp;</span>Validation de votre " + typeDemande + "</h3>";
            retour += "<p class='messageRubrique'>Votre " + typeDemande + " N� " + rapport.get("NUMERO") + rapport.get("SUFFIXE")
                + " a bien �t� transmise � nos services.<br/>Vous recevrez dans les plus brefs d�lais, la confirmation de disponibilit� de la part de notre agent Web Shop</p>";
            
            String numero = rapport.get("TYPE") + rapport.get("NUMERO") + rapport.get("SUFFIXE");
            
            if (mesDonnees == null)
              mesDonnees = new GestionMesDonnees();
            
            GenericRecord enTeteCommande = mesDonnees.recupererEnteteCommande(utilisateur, numero);
            String prix = "";
            if (enTeteCommande != null && enTeteCommande.isPresentField("E1THTL"))
              prix = Outils.formaterUnTarifArrondi(utilisateur, enTeteCommande.getField("E1THTL"));
            
            // tarif
            String[] nouveauTarif = prix.split(" &euro;");
            // Magasin de la commande
            String magasin = null;
            if (enTeteCommande != null && enTeteCommande.isPresentField("E1MAG"))
              magasin = enTeteCommande.getField("E1MAG").toString().trim();
            // Passage au mails
            if (utilisateur.razPanier()) {
              retour += pattern.razDuPanier(utilisateur);
              // Mettre dans un thread
              
              // On g�re l'envoi par mail de la confirmation
              if (utilisateur.getETB_EN_COURS().isOk_envois_mails()) {
                if (mails == null)
                  mails = new EnvoiMail(utilisateur);
                // Mail Client
                if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT)
                  mails.envoyerUnMail(utilisateur.getUS_LOGIN(), "Confirmation de " + typeDemande,
                      retournerMessageValidation(utilisateur, typeDemande, rapport, MAIL_CLIENT));
                
                // Mail gestionnaire
                String sujet =
                    typeDemande + " N� " + rapport.get("NUMERO") + rapport.get("SUFFIXE") + " d'un montant de " + nouveauTarif[0];
                if (!utilisateur.getUS_DEV().equals("XPF")) {
                  sujet += " euros";
                }
                else {
                  sujet += " F";
                }
                
                mails.envoyerUnMail(retournerAdresseMailConfirmation(magasin, typeBon, utilisateur), sujet,
                    retournerMessageValidation(utilisateur, typeDemande, rapport, MAIL_GESTIONNAIRE));
              }
              
              utilisateur.getAccesDB2().ajoutUnLog(utilisateur.getUS_ID(), request.getSession().getId(), typeBon,
                  rapport.get("NUMERO") + rapport.get("SUFFIXE"), "MonPanier");
            }
          }
          // si on a pas bien inject� le bouzin
          else {
            retour += "<p class='messageAlerte'>";
            retour += "Injection de votre " + typeDemande + " en erreur 2. Veuillez contacter votre agence.<br/>";
            for (Entry<String, String> entry : rapport.entrySet())
              if (entry.getKey().trim().contains("_LIBELLE_ERREUR")) {
                retour += (String.format("%-20s", entry.getValue())) + "<br/>";
              }
            retour += "</p>";
          }
        }
        else {
          retour += "<p class='messageAlerte'>";
          retour += "Injection de votre " + typeDemande + " en erreur. Veuillez contacter votre agence.<br/>";
          for (Entry<String, String> entry : rapport.entrySet())
            if (entry.getKey().trim().contains("_LIBELLE_ERREUR")) {
              retour += (String.format("%-20s", entry.getValue())) + "<br/>";
            }
          retour += "</p>";
        }
      }
      else
        retour += "<p class='messageAlerte'>Votre panier est vide.</p>";
    }
    
    retour += "</div>";
    return retour;
  }
  
  /**
   * Cette m�thode permet d'afficher le panier de l'utilisateur dans son
   * ensemble
   */
  private String afficherPanier(Utilisateur utilisateur, String titre) {
    String retour = titre;
    retour += "<div class='blocContenu'>";
    
    if (utilisateur != null) {
      // gestion du mode r�cup�ration
      retour += afficherModeDeRecuperation(utilisateur, "MonPanier");
      
      // afficher les articles avec les bons stocks et dans les bonnes
      // rubriques
      retour += afficherArticles(utilisateur);
      
      retour += afficherFinDeBon(utilisateur);
    }
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Cette m�thode permet de g�rer le mode r�cup�ration de la commande
   * (indispensable � la gestion des stocks)
   */
  public String afficherModeDeRecuperation(Utilisateur utilisateur, String page) {
    boolean modePanier = true;
    String retour = "";
    String parametres = "?modeRecup=";
    String ajoutBtRetour = "";
    String lienRetrait = "";
    String lienLivraison = "";
    String lienDevis = "";
    String debutSpec = "";
    String finSpec = "";
    String titreModeRecup = "$$disponibilite";
    
    // liste article
    if (page.equals("Catalogue")) {
      modePanier = false;
      parametres = "?visuStock=1&modeRecup=";
      ajoutBtRetour =
          "<div class='divDeBoutons'  id='blocDeRetour'><a class='btRecuperation' href='catalogue?retour=1'><span class='deco_bt' id='deco_bt_retour'>&nbsp;</span><span class='texte_bt'>"
              + utilisateur.getTraduction().traduire("$$retour") + "</span></a></div>";
      if (utilisateur.isAutoriseCommande()) {
        lienRetrait =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\"  onClick='traitementStockDispo(\""
                + page + parametres + ConstantesEnvironnement.MODE_RETRAIT
                + "\");'><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$retraitMagasin") + "</span></a></div>";
        lienLivraison =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\"  onClick='traitementStockDispo(\""
                + page + parametres + ConstantesEnvironnement.MODE_LIVRAISON
                + "\");'><span class='deco_bt' id='deco_bt_livraison'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$livraison") + "</span></a></div>";
      }
      if (utilisateur.isAutoriseDevis()) {
        lienDevis =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\"  onClick='traitementStockDispo(\""
                + page + parametres + ConstantesEnvironnement.MODE_DEVIS
                + "\");'><span class='deco_bt' id='deco_bt_devis'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$simpleDevis") + "</span></a></div>";
      }
    }
    // fiche article
    else if (page.startsWith("catalogue?A1ETB=")) {
      modePanier = false;
      parametres = "&modeRecup=";
      ajoutBtRetour = "<div class='divDeBoutons'  id='blocDeRetour'><a class='btRecuperation' href='" + page
          + "'><span class='deco_bt' id='deco_bt_retour'>&nbsp;</span><span class='texte_bt'>"
          + utilisateur.getTraduction().traduire("$$retour") + "</span></a></div>";
      if (utilisateur.isAutoriseCommande()) {
        lienRetrait =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\" onClick='traitementStockDispo(\""
                + page + parametres + ConstantesEnvironnement.MODE_RETRAIT
                + "\");'><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$retraitMagasin") + "</span></a></div>";
        lienLivraison =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\" onClick='traitementStockDispo(\""
                + page + parametres + ConstantesEnvironnement.MODE_LIVRAISON
                + "\");'><span class='deco_bt' id='deco_bt_livraison'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$livraison") + "</span></a></div>";
      }
      if (utilisateur.isAutoriseDevis()) {
        lienDevis =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\" onClick='traitementStockDispo(\""
                + page + parametres + ConstantesEnvironnement.MODE_DEVIS
                + "\");'><span class='deco_bt' id='deco_bt_devis'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$simpleDevis") + "</span></a></div>";
      }
    }
    // panier
    else {
      debutSpec =
          "<div class='accordeon2' id='changerMode'><a class='ouvrirAccordeonG' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChangerMode');\">&nbsp;</a><a class='titreAccordeon' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChangerMode');\">Changer votre mode de r�cup�ration"
              + "</a> <a class='ouvrirAccordeonD' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChangerMode');\">&nbsp;</a></div>";
      debutSpec += "<div class='contenuAccordeon2' id='contenuChangerMode'>";
      if (utilisateur.isAutoriseCommande()) {
        lienRetrait = "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href='" + page + parametres
            + ConstantesEnvironnement.MODE_RETRAIT + "'><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>"
            + utilisateur.getTraduction().traduire("$$retraitMagasin") + "</span></a></div>";
        lienLivraison = "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href='" + page + parametres
            + ConstantesEnvironnement.MODE_LIVRAISON + "'><span class='deco_bt' id='deco_bt_livraison'>&nbsp;</span><span class='texte_bt'>"
            + utilisateur.getTraduction().traduire("$$livraison") + "</span></a></div>";
      }
      if (utilisateur.isAutoriseDevis()) {
        lienDevis = "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href='" + page + parametres
            + ConstantesEnvironnement.MODE_DEVIS + "'><span class='deco_bt' id='deco_bt_devis'>&nbsp;</span><span class='texte_bt'>"
            + utilisateur.getTraduction().traduire("$$simpleDevis") + "</span></a></div>";
      }
      finSpec = "</div>";
      titreModeRecup = "Mode de r�cup�ration";
    }
    
    if (utilisateur == null || utilisateur.getMonPanier() == null)
      return retour;
    
    // liens ci-dessous
    retour += "<div class='rubriquePanier' id='modeRecup'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>" + utilisateur.getTraduction().traduire(titreModeRecup) + "</h3>";
    retour += "<p class='messageRubrique'>";
    // Le mode r�cup�ration n'est pas choisi
    if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_NON_CHOISI) {
      // retour +=
      // "Vous n'avez pas choisi de mode de r�cup�ration pour votre panier.<br/>L'affichage des articles de votre panier
      // d�pend directement de cette information";
      retour += utilisateur.getTraduction().traduire("$$choisirModeRecuperation");
      retour += "</p>";
      if (utilisateur.isAutoriseCommande()) {
        retour += lienRetrait;
        retour += lienLivraison;
      }
      if (utilisateur.isAutoriseDevis()) {
        retour += lienDevis;
      }
    }
    // Le mode de r�cup�ration est le "retrait en magasin"
    else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT) {
      if (utilisateur.getMonPanier().getMagasinPanier() == null)
        retour += "Vous n'avez pas encore choisi votre magasin de retrait";
      else
        // if(MarbreEnvironnement.IS_MONO_MAGASIN == 0)
        retour += "Vous avez choisi de retirer votre commande au magasin - "
            + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin() + " -";
      retour += "</p>";
      retour += afficherChoixMagasins(utilisateur);
      if (modePanier)
        retour += afficherInfosRecup(utilisateur);
      
      // Si multi magasin, on affiche le bouton de changement du magasin
      if (!utilisateur.getETB_EN_COURS().is_mono_magasin())
        retour +=
            "<div class='divDeBoutons' id='blocDeRecuperation'><a class='btRecuperation' id='chgMonMag' href='javascript:void(0)' onClick=\"montrerAccordeon('choixMagasin','contenuChoixMagasin'); cacherUnElement('chgMonMag');\" ><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>Changer de magasin</span></a></div>";
      retour += debutSpec;
      if (utilisateur.isAutoriseCommande()) {
        retour += lienLivraison;
      }
      if (utilisateur.isAutoriseDevis()) {
        retour += lienDevis;
      }
      retour += finSpec;
      
    }
    // le mode de r�cup�ration est la livraison
    else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
      retour +=
          "Vous avez choisi de vous faire livrer votre marchandise.<br/>Veuillez remplir ci-dessous les renseignements n�cessaires � la livraison.";
      retour += "</p>";
      if (modePanier)
        retour += afficherInfosRecup(utilisateur);
      
      retour += debutSpec;
      if (utilisateur.isAutoriseCommande()) {
        retour += lienRetrait;
      }
      if (utilisateur.isAutoriseDevis()) {
        retour += lienDevis;
      }
      retour += finSpec;
    }
    // simple devis
    else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_DEVIS) {
      retour += "Vous avez choisi une demande de devis";
      retour += "</p>";
      if (modePanier)
        retour += afficherInfosRecup(utilisateur);
      
      retour += debutSpec;
      if (utilisateur.isAutoriseCommande()) {
        retour += lienRetrait;
        retour += lienLivraison;
      }
      retour += finSpec;
    }
    
    retour += ajoutBtRetour;
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher les informations propres au retrait de la marchandise en magasin ou en livraison
   */
  private String afficherInfosRecup(Utilisateur utilisateur) {
    StringBuilder retour = new StringBuilder(5000);
    
    if (utilisateur != null) {
      // TRAITEMENT
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // mettre � jour le bloc adresse du client en cas de livraison
      if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
        if (utilisateur.getMonPanier().getCLNOM() == null && utilisateur.getMonPanier().getCLCPL() == null
            && utilisateur.getMonPanier().getCLRUE() == null && utilisateur.getMonPanier().getCLLOC() == null)
          ((GestionMonPanier) maGestion).majVariablesPanier(utilisateur);
      }
      
      // Mise � jour du bloc commentaires du panier
      if (utilisateur.getMonPanier().getDetailsRecuperation() == null)
        utilisateur.getMonPanier().setDetailsRecuperation("");
      
      // GESTION DE LA DATE DE RECUPERATION DU PANIER
      if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT) {
        if (Outils.recupererDateCouranteInt() > utilisateur.getMonPanier().getDateRecuperation())
          utilisateur.getMonPanier().setDateRecuperation(Outils.recupererDateCouranteInt());
      }
      else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
        if (Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
            utilisateur.getETB_EN_COURS().getDelai_mini_livraison())) > utilisateur.getMonPanier().getDateRecuperation()) {
          utilisateur.getMonPanier().setDateRecuperation(Outils.transformerDateHumaineEnSeriem(
              Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(), utilisateur.getETB_EN_COURS().getDelai_mini_livraison())));
        }
      }
      
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // AFFICHAGE
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
      // Affichage du bloc d'informations suppl�mentaires
      String typeRetrait = "retrait";
      if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_DEVIS)
        typeRetrait = "devis";
      
      if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON)
        typeRetrait = "livraison";
      
      retour.append("<div class='accordeon2' id='infosRetrait'>"
          + "<a class='ouvrirAccordeonG' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuInfosRetrait');\">&nbsp;</a>"
          + "<a class='titreAccordeon' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuInfosRetrait');\">Informations de "
          + typeRetrait + "</a>"
          + "<a class='ouvrirAccordeonD' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuInfosRetrait');\">&nbsp;</a>"
          + "</div>");
      retour.append("<div class='contenuAccordeon2' id='contenuInfosRetrait'>");
      retour.append(
          "<script type=\"text/javascript\">if(document.getElementById('contenuInfosRetrait').style.display=='') document.getElementById('contenuInfosRetrait').style.display='block';</script>"
              + "<form name='formInfosRetrait' id='formInfosRetrait' action ='MonPanier' method ='POST' >");
      if (!(utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_DEVIS))
      // Date de retrait/livraison souhait�e
      {
        retour.append("<label class='labelsModeRecup' for='idDateRetrait' id='lbDateRetrait'>Date de " + typeRetrait + " souhait�e</label>"
            + "<input type='text' name='dateRetrait' id='idDateRetrait' readonly='readonly' value = '"
            + Outils.transformerDateSeriemEnHumaine(utilisateur.getMonPanier().getDateRecuperation())
            + "' onClick=\"openCalendarRetrait()\" onChange='majInfosPanier(\"idDateRetrait\");' /><br/>");
      }
      // R�f�rence longue avec libell� param�trable
      retour
          .append("<label  class='labelsModeRecup' for='idCLNOM' id='lbCLNOM' >" + utilisateur.getTraduction().traduire("$$referenceLongue")
              + "</label>" + "<input  class='zonesBlocAdresse' type='text' name='REFLONGUE' id='idReferenceLongue' maxlength ='25' value=\""
              + utilisateur.getMonPanier().getE1RCC().trim() + "\" onBlur='majInfosPanier(\"idReferenceLongue\");'/><br/>");
      if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
        typeRetrait = "livraison";
        retour.append("<br/><br/>" + "<label  class='labelsModeRecup' for='idCLNOM' id='lbCLNOM' >Nom ou raison sociale</label>"
            + "<input class='zonesBlocAdresse' type='text' name='CLNOM' id='idCLNOM' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLNOM() + "\" onBlur='majInfosPanier(\"idCLNOM\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLCPL' id='lbCLCPL'>Nom du responsable</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLCPL' id='idCLCPL' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLCPL() + "\" onBlur='majInfosPanier(\"idCLCPL\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLRUE' id='lbCLRUE'>Adresse - rue</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLRUE' id='idCLRUE' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLRUE() + "\" onBlur='majInfosPanier(\"idCLRUE\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLLOC' id='lbDateRetrait'>Adresse - localit�</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLLOC' id='idCLLOC' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLLOC() + "\" onBlur='majInfosPanier(\"idCLLOC\");'/><br/>" +
            // Code postal
            "<label  class='labelsModeRecup' for='idCLVIL' id='lbDateRetrait'>Ville</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLPOS' id='idCLPOS' maxlength ='5' minlength ='5' onKeyPress='only_numeric(event)' value=\""
            + utilisateur.getMonPanier().getCLPOS() + "\" onBlur='majInfosPanier(\"idCLPOS\")'/>&nbsp;"
            + "<input  class='zonesBlocAdresse' type='text' name='CLVIL' id='idCLVIL' maxlength ='24' value=\""
            + utilisateur.getMonPanier().getCLVIL() + "\" onBlur='majInfosPanier(\"idCLVIL\");'/><br/>");
      }
      // commentaires
      retour.append("<label  class='labelsModeRecup' for='idDetailsRetrait' id='lbDateRetrait'>Commentaires</label>"
          + "<textarea id='idDetailsRetrait' maxlength ='360' name='detailsRetrait'  " + "onBlur='majInfosPanier(\"idDetailsRetrait\");'>"
          + utilisateur.getMonPanier().getDetailsRecuperation() + "</textarea>" + "</form>");
      // // "</form>";
      retour.append("</div>");
    }
    else
      return "";
    
    return retour.toString();
  }
  
  /**
   * Afficher la fen�tre qui permet de choisir/changer le magasin de retrait
   */
  private String afficherChoixMagasins(Utilisateur utilisateur) {
    String retour = "";
    
    if (utilisateur != null) {
      ArrayList<GenericRecord> liste = ((GestionMonPanier) maGestion).recupererListeMagasins(utilisateur);
      if (liste != null) {
        retour +=
            "<div class='accordeon2' id='choixMagasin'><a class='ouvrirAccordeonG' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChoixMagasin');\">&nbsp;</a><a class='titreAccordeon' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChoixMagasin');\">Changement du magasin de retrait</a> <a class='ouvrirAccordeonD' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChoixMagasin');\">&nbsp;</a></div>";
        retour += "<div class='contenuAccordeon2' id='contenuChoixMagasin'>";
        
        for (int i = 0; i < liste.size(); i++)
          retour += "<a class='ligneAccordeon' href='MonPanier?nvMagasin=" + liste.get(i).getField("MG_COD").toString()
              + "'><span class='libMagasinPanier'>Magasin - <span class='magImportant'>" + liste.get(i).getField("MG_NAME")
              + "</span> -</span><span class='btMagasinPanier'>&nbsp;</span></a>";
        
        retour += "</div>";
      }
    }
    else
      return "";
    
    return retour;
  }
  
  /**
   * afficher les articles avec les bons stocks dans les bonnes rubriques de
   * disponibilit�
   */
  private String afficherArticles(Utilisateur utilisateur) {
    String retour = "";
    // String magasinRetrait = null;
    Float stockDispo = null;
    Float stockRetrait = null;
    Float stockSiege = null;
    
    if (utilisateur == null || utilisateur.getMonPanier() == null)
      return retour;
    
    if (utilisateur.getMonPanier().getModeRecup() > ConstantesEnvironnement.MODE_NON_CHOISI
        && utilisateur.getMonPanier().getNombreArticle() > 0) {
      retour += "<div class='rubriquePanier'>";
      
      String typeDemande = "";
      if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT
          || utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
        typeDemande = "commande";
      }
      else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_DEVIS) {
        typeDemande = "devis";
      }
      
      retour += "<h3 id='ancreArticles'><span class='puceH3'>&nbsp;</span>Les articles de votre " + typeDemande + "</h3>";
      
      // Si cette liste n'est pas vide
      if (!utilisateur.getMonPanier().getListeArticles().isEmpty()) {
        // ent�tes de colonnes
        retour += "<div class='listesPanier' id='enTeteListes'>" + "<span id='titreCAREF2'>R�f�rence</span>"
            + "<span id='titreA1LIB2'>Libell� article</span>" + "<span id='titreSTOCK2'>Dispo.</span>" + "<span id='titreTARIF2'>Tarif("
            + utilisateur.getUS_DEV() + ")</span>"
            + "<span id='titreActionsP'>Quantit�</span><span id='titreTOTALLIGNE'>Total H.T</span></div>";
        
        ArrayList<PanierArticle> listeDispo = null;
        ArrayList<PanierArticle> listePartiel = null;
        ArrayList<PanierArticle> listeIndispo = null;
        
        /************************ On attribue les bonnes cat�gories ***********************************************/
        /* DLC */
        for (Entry<String, PanierArticle> entry : utilisateur.getMonPanier().getListeArticles().entrySet()) {
          if (entry.getValue() instanceof PanierArticleNormal && ((PanierArticleNormal) entry.getValue()).getTypeArticle().equals("C")) {
            PanierArticleNormal panierarticle = (PanierArticleNormal) entry.getValue();
            stockDispo = 0.0f;
            stockRetrait = 0.0f;
            stockSiege = 0.0f;
            
            // ON CALCULE LES STOCKS QUAND NECESSAIRE
            // le stock de retrait est mont� syst�matiquement quand on est en mode retrait
            if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT)
              stockRetrait = ((GestionMonPanier) maGestion).retourneStockPourUnArticle(utilisateur, panierarticle.getA1etb(),
                  panierarticle.getA1art(), utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
            // Le stock du magasin si�ge est mont� si MODE LIVRAISON ou (MODE RETRAIT et PAS MONO MAG et MAG <> SIEGE)
            if (utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_RETRAIT
                || ((utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT)
                    && (!utilisateur.getETB_EN_COURS().is_mono_magasin()) && (!utilisateur.getMonPanier().getMagasinPanier()
                        .getCodeMagasin().toString().equals(utilisateur.getMagasinSiege().getCodeMagasin()))))
              stockSiege = ((GestionMonPanier) maGestion).retourneStockPourUnArticle(utilisateur, panierarticle.getA1etb(),
                  panierarticle.getA1art(), utilisateur.getMagasinSiege().getCodeMagasin());
              
            // ON EN DEDUIT LE DISPONIBLE EN FONCTION DES CAS
            // Retrait
            if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT) {
              // si le retrait est (MAG + SIEGE)
              if (utilisateur.getETB_EN_COURS().isRetrait_is_siege_et_mag())
                stockDispo = stockRetrait + stockSiege;
              else
                stockDispo = stockRetrait;
            }
            // livraison ou devis
            else
              stockDispo = stockSiege;
            
            logs.loggerSiDEBUG("+++++++++++++++++++++ Panier ");
            logs.loggerSiDEBUG("++ magasinRetrait: " + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin());
            logs.loggerSiDEBUG("++ stockRetrait: " + stockRetrait.toString());
            logs.loggerSiDEBUG("++ magasinSiege: " + utilisateur.getMagasinSiege().getLibelleMagasin());
            logs.loggerSiDEBUG("++ stockSiege: " + stockSiege.toString());
            logs.loggerSiDEBUG("++ stockDispo: " + stockDispo.toString());
            
            if (stockDispo < 0)
              stockDispo = Float.valueOf("0");
            panierarticle.setStockDispo(stockDispo);
            
            ((GestionMonPanier) maGestion).majInfosArticle(utilisateur, panierarticle);
            
            if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT) {
              // Articles directement disponibles
              if (Float.parseFloat(panierarticle.getQuantite().toString()) <= stockRetrait) {
                if (listeDispo == null)
                  listeDispo = new ArrayList<PanierArticle>();
                // On rajoute cet article dans cette cat�gorie
                listeDispo.add(entry.getValue());
              }
              // Articles insuffisemment disponibles en magasin de retrait
              else if ((Float.parseFloat(panierarticle.getQuantite().toString()) > stockRetrait)
                  && (Float.parseFloat(panierarticle.getQuantite().toString()) <= (stockRetrait + stockSiege))) {
                if (listePartiel == null)
                  listePartiel = new ArrayList<PanierArticle>();
                
                listePartiel.add(entry.getValue());
              }
              // Articles indisponibles
              else if (stockDispo == 0 || Float.parseFloat(panierarticle.getQuantite().toString()) > (stockRetrait + stockSiege)) {
                if (listeIndispo == null)
                  listeIndispo = new ArrayList<PanierArticle>();
                
                listeIndispo.add(entry.getValue());
              }
            }
            else {
              // Articles directement disponibles
              if (Float.parseFloat(panierarticle.getQuantite().toString()) <= stockSiege) {
                if (listeDispo == null)
                  listeDispo = new ArrayList<PanierArticle>();
                // On rajoute cet article dans cette cat�gorie
                listeDispo.add(entry.getValue());
              }
              // Articles indisponibles
              else if (stockSiege == 0 || Float.parseFloat(panierarticle.getQuantite().toString()) > stockSiege) {
                if (listeIndispo == null)
                  listeIndispo = new ArrayList<PanierArticle>();
                
                listeIndispo.add(entry.getValue());
              }
            }
          }
        }
        
        /************************
         * On affiche les articles et leurs commentaires
         ******************************************/
        if (listeDispo != null) {
          if (utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_LIVRAISON)
            retour +=
                "<div class='listesCommentaires'><span class='txtCommentaires'>Ces articles sont directement disponibles dans votre magasin</span></div>";
          else
            retour +=
                "<div class='listesCommentaires'><span class='txtCommentaires'>Ces articles vous seront livr�s directement</span></div>";
          // }
          retour += afficherUneLigneDarticle(listeDispo, utilisateur);
        }
        if (listePartiel != null) {
          if (utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_LIVRAISON) {
            if (ConstantesEnvironnement.VOIR_TABLE_DELAIS && !utilisateur.getETB_EN_COURS().is_mono_magasin()) {
            }
            else
              retour +=
                  "<div class='listesCommentaires'><span class='txtCommentaires'  id='spanRouge'>La quantit� demand�e pour ces articles n'est que partiellement disponible</span></div>";
          }
          else
            retour +=
                "<div class='listesCommentaires'><span class='txtCommentaires'  id='spanRouge'>La quantit� demand�e pour ces articles n'est que partiellement disponible</span></div>";
          
          // }
          retour += afficherUneLigneDarticle(listePartiel, utilisateur);
        }
        if (listeIndispo != null) {
          retour +=
              "<div class='listesCommentaires'><span class='txtCommentaires'  id='spanRouge'>La quantit� demand�e pour ces articles est indisponible</span></div>";
          retour += afficherUneLigneDarticle(listeIndispo, utilisateur);
        }
      }
      retour += "</div>";
    }
    return retour;
  }
  
  /**
   * Affiche le d�tail d'une ligne � partir d'une liste d'articles
   */
  private String afficherUneLigneDarticle(ArrayList<PanierArticle> liste, Utilisateur utilisateur) {
    StringBuilder retour = new StringBuilder(2000);
    PanierArticleNormal panierarticle = null;
    String blocEcoTaxe = null;
    boolean isModifiable = utilisateur.getMonPanier().isModifiable();
    
    for (int i = 0; i < liste.size(); i++) {
      if (liste.get(i) instanceof PanierArticleNormal) {
        panierarticle = (PanierArticleNormal) liste.get(i);
        blocEcoTaxe = null;
        // Liste articles
        retour.append("<div class='listesPanier'>");
        if (utilisateur.getETB_EN_COURS().getZone_reference_article().equals("CAREF") && panierarticle.getRefFournisseur() != null
            && !panierarticle.getRefFournisseur().trim().equals(""))
          retour.append("<a class='CAREF2' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + liste.get(i).getA1etb().trim() + "\",\"A1ART\",\"" + liste.get(i).getA1art() + "\");'>" + panierarticle.getRefFournisseur()
              + "</a>");
        else
          retour.append("<a class='CAREF2' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + liste.get(i).getA1etb().trim() + "\",\"A1ART\",\"" + liste.get(i).getA1art() + "\");'>" + panierarticle.getA1art()
              + "</a>");
        
        retour.append("<a class='A1LIB2' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
            + liste.get(i).getA1etb().trim() + "\",\"A1ART\",\"" + liste.get(i).getA1art() + "\");'>" + panierarticle.getLibelleArt()
            + "</a>");
        
        if (utilisateur.isAutoriseVueStockDispo()) {
          retour.append(
              "<span class='STOCK2'>" + afficherStockCorrectement(utilisateur, panierarticle.getStockDispo().toString()) + "</span>");
        }
        
        retour.append(
            "<span class='TARIF2'>" + Outils.formaterUnTarifArrondi(utilisateur, panierarticle.getTarifHTUnitaireCalcul()) + "</span>");
        retour.append("<span class='quantiteArticleP'><input id='saisieQ" + panierarticle.getA1art());
        if (isModifiable) {
          retour.append(
              "' type ='text' class='saisieQuantiteP' maxlength = '5'  onKeyPress='only_numeric(event)' " + "onChange=\"modifQuantite('");
        }
        else {
          retour.append("' type ='text' class='saisieQuantiteP' maxlength = '5'  disabled='disabled'   onKeyPress='only_numeric(event)' "
              + "onChange=\"modifQuantite('");
        }
        retour.append(panierarticle.getA1art() + "','saisieQ" + panierarticle.getA1art() + "','" + panierarticle.getConditionnM()
            + "');\"  tabindex=" + (i + 1));
        retour.append(" value=" + panierarticle.getQuantite() + "  />");
        retour.append("</span>");
        retour.append(
            "<span class='TOTALLIGNE'>" + Outils.formaterUnTarifArrondi(utilisateur, panierarticle.getTotalPourQuantite()) + "</span>");
        
        if (isModifiable) {
          retour.append("<a class='suppPanier'  href='MonPanier?SupArticle=" + panierarticle.getA1art() + "&SupEtbArt="
              + panierarticle.getA1etb() + "#ancreArticles'>&nbsp;</a>");
        }
        
        // on pr�pare le bloc ECOTAXE
        if (utilisateur.getETB_EN_COURS().isGestion_ecotaxe() && panierarticle.getTotalEcotaxe().compareTo(BigDecimal.ZERO) == 1)
          blocEcoTaxe = "<div class='ecotaxeP'>dont �cotaxe: " + Outils.formaterUnTarifArrondi(utilisateur, panierarticle.getTotalEcotaxe())
              + "</div>";
        // CONDITIONNEMENT
        if (panierarticle.getConditionnM() > 1 || blocEcoTaxe != null)
          retour.append(afficherConditionnement(utilisateur, String.valueOf(panierarticle.getConditionnM())));
        // ON affiche l'ECOTAXE APRES le conditionnement
        if (blocEcoTaxe != null)
          retour.append(blocEcoTaxe);
        
        retour.append("</div>");
      }
    }
    
    return retour.toString();
  }
  
  /**
   * afficher les informations fins de bon
   */
  private String afficherFinDeBon(Utilisateur utilisateur) {
    String retour = "";
    String typeDemande = "panier";
    String texteBoutonValidation = "";
    String texteBoutonAnnulation = "";
    int typeValidation = 2;
    if (utilisateur == null || utilisateur.getMonPanier() == null)
      return retour;
    
    // Si le mode de r�cup�ration est choisi
    if (utilisateur.getMonPanier().getModeRecup() > ConstantesEnvironnement.MODE_NON_CHOISI) {
      if (utilisateur.getMonPanier().getIdDevisAtransformer() != null) {
        texteBoutonValidation = utilisateur.getTraduction().traduire("$$validerCommande");
        texteBoutonAnnulation = utilisateur.getTraduction().traduire("$$annulerCommande");
        typeValidation = 3;
      }
      else if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_DEVIS) {
        typeDemande = "devis";
        texteBoutonValidation = utilisateur.getTraduction().traduire("$$validerDevis");
        texteBoutonAnnulation = utilisateur.getTraduction().traduire("$$annulerDevis");
        typeValidation = 1;
      }
      else {
        typeDemande = "commande";
        texteBoutonValidation = utilisateur.getTraduction().traduire("$$validerCommande");
        texteBoutonAnnulation = utilisateur.getTraduction().traduire("$$annulerCommande");
      }
      
      retour += "<h3><span class='puceH3'>&nbsp;</span>Fin de " + typeDemande + "</h3>";
      retour += "<div class='rubriquePanier' id='finDeBon'>";
      
      if (utilisateur.getMonPanier().getNombreArticle() > 0) {
        retour += "<p class='messageRubrique'>Bilan de votre " + typeDemande + "</p>";
        retour += "<div class='listesPanier' id='bilanPanier'>";
        retour += "<span id='totalHTPanier'>Total HT : "
            + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalHT()) + " </span>";
        if (utilisateur.getETB_EN_COURS().isGestion_ecotaxe()
            && utilisateur.getMonPanier().getTotalEcotaxe().compareTo(BigDecimal.ZERO) == 1)
          retour += "<span id='totalECOTAX'>Dont �cotaxe : "
              + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalEcotaxe()) + " </span>";
        retour += "</div>";
        retour += "<div class='divDeBoutons'>";
        
        if (typeValidation == 3) {
          retour +=
              "<a class='btRecuperation' id='validerbon' href='javascript:void(0)' onClick=\"traitementEnCours('MesDonnees?validTransformation=1');\" >"
                  + "<span class='deco_bt' id='deco_bt_valider'>&nbsp;</span><span class='texte_bt'>" + texteBoutonValidation + "</span>"
                  + "</a>" + "</div>";
        }
        else {
          retour += "<a class='btRecuperation' id='validerbon' href='javascript:void(0)' onClick=\"traitementEnCours('MonPanier?validation="
              + typeValidation + "');\" >" + "<span class='deco_bt' id='deco_bt_valider'>&nbsp;</span><span class='texte_bt'>"
              + texteBoutonValidation + "</span>" + "</a>" + "</div>";
        }
      }
      else
        retour += "<p class='messageRubrique'>Votre " + typeDemande + " ne contient aucun article</p>";
      if (utilisateur.getMonPanier().isModifiable()) {
        retour +=
            "<div class='divDeBoutons'><a class='btRecuperation' id='plusArticles' href='catalogue?retour=1'><span class='deco_bt' id='deco_bt_plusArt'>&nbsp;</span><span class='texte_bt'>Ajouter des articles</span></a></div>";
      }
      retour +=
          "<div class='divDeBoutons'><a class='btRecuperation' id='annulerBon' href='MonPanier?validation=0'><span class='deco_bt' id='deco_bt_annuler'>&nbsp;</span><span class='texte_bt'>"
              + texteBoutonAnnulation + "</span></a></div>";
      retour += "</div>";
    }
    
    return retour;
    
  }
  
  /**
   * Afficher la ligne de conditionnement si n�cessaire
   */
  private String afficherConditionnement(Utilisateur utilisateur, String conditionnement) {
    String retour = "";
    retour += "<div class='conditionnement'>";
    try {
      if (Double.parseDouble(String.valueOf(conditionnement)) > 1)
        retour += "Vendu par conditionnement de " + afficherValeurCorrectement(utilisateur, conditionnement);
      else
        retour += "&nbsp;";
    }
    catch (Exception e) {
      retour += "&nbsp;";
    }
    retour += "</div>";
    
    return retour;
  }
  
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
  
  public EnvoiMail getMails() {
    return mails;
  }
  
  public void setMails(EnvoiMail mails) {
    this.mails = mails;
  }
  
}
