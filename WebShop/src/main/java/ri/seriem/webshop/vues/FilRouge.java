
package ri.seriem.webshop.vues;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;

public class FilRouge {
  private String cleFil = null;
  private String valeurFil = null;
  private String parametreLien = null;
  private String valeurFilFR = null;
  private String valeurFilEN = null;
  private String lienFil = null;
  
  /*public FilRouge(String cle, String valeur)
  {
  	cleFil = cle;
  	valeurFil = valeur;
  }*/
  
  public FilRouge(String cle, String lien, String valeurFR, String valeurEN) {
    cleFil = cle;
    lienFil = lien;
    valeurFilFR = valeurFR;
    valeurFilEN = valeurEN;
    // valeurFil = valeurFR;
  }
  
  /**
   * 
   * */
  public void majValeurLangue(String codeLangue) {
    if (codeLangue == null)
      valeurFil = valeurFilFR;
    
    if (codeLangue.equals("fr"))
      valeurFil = valeurFilFR;
    else if (codeLangue.equals("en"))
      valeurFil = valeurFilEN;
    else
      valeurFil = valeurFilFR;
  }
  
  public String getCleFil() {
    return cleFil;
  }
  
  public void setCleFil(String cleFil) {
    this.cleFil = cleFil;
  }
  
  public String getLienFil() {
    return lienFil;
  }
  
  public void setLienFil(String lienFil) {
    this.lienFil = lienFil;
  }
  
  public String getValeurFil() {
    return valeurFil;
  }
  
  public void setValeurFil(String valeurFil) {
    this.valeurFil = valeurFil;
  }
  
  public String getParametreLien() {
    return parametreLien;
  }
  
  public void setParametreLien(String parametre) {
    this.parametreLien = parametre;
    
    if (parametreLien != null)
      valeurFil = valeurFil.replaceFirst(ConstantesEnvironnement.PARAM_FILROUGE, parametre);
  }
  
  public String getValeurFilFR() {
    return valeurFilFR;
  }
  
  public void setValeurFilFR(String valeurFilFR) {
    this.valeurFilFR = valeurFilFR;
  }
  
  public String getValeurFilEN() {
    return valeurFilEN;
  }
  
  public void setValeurFilEN(String valeurFilEN) {
    this.valeurFilEN = valeurFilEN;
  }
  
}
