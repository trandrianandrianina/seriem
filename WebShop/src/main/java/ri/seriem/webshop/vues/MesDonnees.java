
package ri.seriem.webshop.vues;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionMesDonnees;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Magasin;
import ri.seriem.webshop.outils.EnvoiMail;
import ri.seriem.webshop.outils.Outils;

/**
 * Servlet MesDonnees
 */
public class MesDonnees extends MouleVues {
  private static final long serialVersionUID = 1L;
  private final static String DEVIS = "Devis";
  private final static String COMMANDE = "Commande";
  private EnvoiMail mails = null;
  
  public MesDonnees() {
    super(new GestionMesDonnees(), "mesDonnees",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "mesDonnees.css?" + ConstantesEnvironnement.versionWAR
            + "' rel='stylesheet'/><script src='scripts/MesDonnes.js?" + ConstantesEnvironnement.versionWAR + "'></script>",
        ConstantesEnvironnement.ACCES_CLIENT);
    
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("mesDonn�es", "MesDonnees?retour=1", "Mes donn�es", "My data"));
    listeFilRouge.add(new FilRouge("uneCde", "MesDonnees?cde", "D�tails", "Detail"));
    listeFilRouge.add(new FilRouge("modifierCde", "MesDonnees?reprise", "Modifier ma commande", "Modify my order"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      // D�tail d'une commande
      if (request.getParameter("cde") != null) {
        gererDetailCommande(request, out);
      }
      // On modifie le contenu d'une commande
      else if (request.getParameter("reprise") != null) {
        gererModificationCommande(request, response, out);
      }
      // On annule une commande
      else if (request.getParameter("annulation") != null) {
        gererAnnulationCommande(request, out);
      }
      // On affiche la d'un devis avant sa transformation en commande
      else if (request.getParameter("transform") != null) {
        afficherModificationDevisAvantCommande(request, response);
      }
      // On transforme un devis en commande
      else if (request.getParameter("validTransformation") != null) {
        gererTransformationDevisCommande(request, response, out);
      }
      else if (request.getParameter("retour") != null) {
        out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
            "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
            ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTypeDonneesEncours(), request.getParameter("indiceDebut")),
            null, null, null, request.getSession()));
      }
      // On affiche les donn�es et les commandes du client
      else
        out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
            "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>", request.getParameter("type"),
            request.getParameter("indiceDebut")), null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * G�rer la r�cup�ration des informations � modifier avant de transformer le devis en commande
   */
  private void afficherModificationDevisAvantCommande(HttpServletRequest request, HttpServletResponse response) {
    Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    String numeroDevis = request.getParameter("transform");
    // On checke d'abord qu'elle soit bien valide
    if (((GestionMesDonnees) getMaGestion()).isDevisValide(utilisateur, numeroDevis)) {
      if (((GestionMesDonnees) getMaGestion()).repriseCommande(utilisateur, numeroDevis)) {
        if (utilisateur.getMonPanier() != null) {
          utilisateur.getMonPanier().setIdDevisAtransformer(numeroDevis);
        }
      }
      
      utilisateur.getMonPanier().setModifiable(false);
      try {
        getServletContext().getRequestDispatcher("/monPanier").forward(request, response);
      }
      catch (ServletException e) {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * G�rer la transformation d'un devis en commande
   */
  private void gererTransformationDevisCommande(HttpServletRequest request, HttpServletResponse response, ServletOutputStream out) {
    String commandeRetour = null;
    Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    GestionMesDonnees gestionDonnees = (GestionMesDonnees) getMaGestion();
    // Etape 1: On transforme le devis initial en commande
    if (!gestionDonnees.isDevisValide(utilisateur, utilisateur.getMonPanier().getIdDevisAtransformer())) {
      // TODO Gestion d'erreurs
    }
    utilisateur.getMonPanier().deverouilleCommande(utilisateur.getMonPanier().getIdDevisAtransformer());
    commandeRetour = gestionDonnees.transformerEnCommande(utilisateur, utilisateur.getMonPanier().getIdDevisAtransformer());
    
    if (commandeRetour == null || commandeRetour.length() != 7) {
      // TODO Gestion d'erreurs
    }
    commandeRetour = "E" + commandeRetour;
    
    // Etape 2: on modifie la commande cr��e avec les donn�es stock�es dans le panier
    gestionDonnees.verouilleCommande(utilisateur, commandeRetour);
    utilisateur.getMonPanier().setNumCommandeSerieM(commandeRetour);
    // On valide la commande du panier
    LinkedHashMap<String, String> rapport = new LinkedHashMap<String, String>();
    if (!utilisateur.getMonPanier().valideCommande("ATT", null, rapport)) {
      // TODO Gestion d'erreurs
    }
    
    // Gestion des mails
    if (utilisateur.getETB_EN_COURS().isOk_envois_mails()) {
      if (mails == null)
        mails = new EnvoiMail(utilisateur);
      mails.envoyerUnMail(utilisateur.getUS_LOGIN(), "Confirmation de commande",
          retournerMessageValidation(utilisateur, "commande", rapport, MAIL_CLIENT));
      
      // Mail gestionnaire
      String magasin = "";
      if (utilisateur.getMonPanier().getMagasinPanier() != null) {
        magasin = utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
      }
      String sujet = "Commande N� " + rapport.get("NUMERO") + rapport.get("SUFFIXE");
      mails.envoyerUnMail(retournerAdresseMailConfirmation(magasin, "E", utilisateur), sujet,
          retournerMessageValidation(utilisateur, "commande", rapport, MAIL_GESTIONNAIRE));
    }
    
    // On d�gage le contenu du panier
    utilisateur.razPanier();
    
    // Affichage retour
    try {
      out.println(afficherContenu(
          afficherMesDonnees(utilisateur, request.getParameter("annulation"), commandeRetour,
              "<h2><span id='titrePage'>" + (utilisateur).getTraduction().traduire("Historique") + "</span></h2>",
              request.getParameter("type"), request.getParameter("indiceDebut")) + pattern.razDuPanier(utilisateur),
          null, null, null, request.getSession()));
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * G�rer l'annulation d'une commande
   */
  private void gererAnnulationCommande(HttpServletRequest request, ServletOutputStream out) {
    Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    
    if (((GestionMesDonnees) getMaGestion()).isCommandeEnAttente(utilisateur, request.getParameter("annulation"))) {
      ((GestionMesDonnees) getMaGestion()).annulationCommande(utilisateur, request.getParameter("annulation"));
    }
    try {
      out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
          "<h2><span id='titrePage'>" + (utilisateur).getTraduction().traduire("Historique") + "</span></h2>", request.getParameter("type"),
          request.getParameter("indiceDebut")), null, null, null, request.getSession()));
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * G�rer l'affichage du d�tail d'une commande
   */
  private void gererDetailCommande(HttpServletRequest request, ServletOutputStream out) {
    Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    try {
      out.println(afficherContenu(
          afficherDetailCommande(utilisateur, "<h2><span id='titrePage'>D�tail commande</span></h2>", request.getParameter("cde")),
          "uneCde", request.getParameter("cde"), null, request.getSession()));
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * G�rer la modification du contenu d'une commande
   */
  private void gererModificationCommande(HttpServletRequest request, HttpServletResponse response, ServletOutputStream out) {
    Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    // On checke d'abord qu'elle soit bien en attente
    if (((GestionMesDonnees) getMaGestion()).isCommandeEnAttente(utilisateur, request.getParameter("reprise"))) {
      ((GestionMesDonnees) getMaGestion()).repriseCommande(utilisateur, request.getParameter("reprise"));
      try {
        getServletContext().getRequestDispatcher("/monPanier").forward(request, response);
      }
      catch (ServletException e) {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    // Sinon retour � la case d�part
    else
      try {
        out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
            "<h2><span id='titrePage'>" + (utilisateur).getTraduction().traduire("Historique") + "</span></h2>",
            request.getParameter("type"), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
      }
      catch (IOException e) {
        e.printStackTrace();
      }
  }
  
  /**
   * Afficher d�tails du contact et client
   */
  private String afficherMesDonnees(Utilisateur utilisateur, String annulation, String transformation, String titre, String typeCde,
      String indiceDebut) {
    if (utilisateur == null)
      return "";
    // Utilisateur utilisateur = null;
    StringBuilder retour = new StringBuilder(10000);
    
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (annulation != null) {
      String det = "la";
      String typeDoc = "commande";
      if (annulation.startsWith("D")) {
        det = "le";
        typeDoc = "devis";
      }
      retour.append("<div id='messagePrincipal'>Vous avez annul� avec succ�s " + det + " " + typeDoc + " N� " + annulation + "</div>");
    }
    if (transformation != null)
      retour.append("<div id='messagePrincipal'>Vous avez cr�� avec succ�s la commande N� " + transformation + "</div>");
    
    // R�cup des infos du clients
    retour.append("<h3><span class='puceH3'>&nbsp;</span>" + utilisateur.getTraduction().traduire("$$monCompte") + "</h3>");
    
    if ((utilisateur.getClient() != null) && (utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_CONSULTATION)) {
      retour.append("<div class='sousBlocs'>");
      retour.append("<p class='decoH5'>&nbsp;</p>");
      retour.append("<h5>" + utilisateur.getTraduction().traduire("$$contact") + "</h5>");
      
      retour.append("<span id='RECIV' placeholder='cv'>");
      
      if (utilisateur.getCiviliteContact() != null && !utilisateur.getCiviliteContact().equals(""))
        retour.append(utilisateur.getCiviliteContact());
      else
        retour.append("<p class='placeholderSpan' id='fi_RECIV'><p class='paddingInterne'>Civ.</p></p>");
      retour.append("</span>");
      retour.append("<span id='REPAC' placeholder='" + utilisateur.getTraduction().traduire("$$nom") + "'>");
      
      if (utilisateur.getNomContact() != null && !utilisateur.getNomContact().trim().equals(""))
        retour.append(utilisateur.getNomContact().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_REPAC'><p class='paddingInterne'>" + utilisateur.getTraduction().traduire("$$nom")
            + "</p></p>");
      retour.append("</span>");
      
      retour.append("<span id='RETEL' placeholder='t�l�phone'>tel: ");
      
      if (utilisateur.getTelContact() != null && !utilisateur.getTelContact().trim().equals(""))
        retour.append(utilisateur.getTelContact().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_RETEL'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$telephone") + "</p></p>");
      retour.append("</span>");
      retour.append("</div>");
      
      retour.append("<div class='sousBlocs'>");
      retour.append("<p class='decoH5'>&nbsp;</p>");
      retour.append("<h5>" + utilisateur.getTraduction().traduire("$$societe") + "</h5>");
      retour.append("<span id='CLCLI'>N� ");
      if (utilisateur.getClient().getNumeroClient() != null && (!utilisateur.getClient().getNumeroClient().trim().equalsIgnoreCase("")))
        retour.append(utilisateur.getClient().getNumeroClient().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_CLCLI'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$numero") + "</p></p>");
      retour.append("</span>");
      
      retour.append("<span id='CLNOM' placeholder='" + utilisateur.getTraduction().traduire("$$societe") + "'>");
      if (utilisateur.getClient().getNomClient() != null && (!utilisateur.getClient().getNomClient().trim().equalsIgnoreCase("")))
        retour.append(utilisateur.getClient().getNomClient().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_CLNOM'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$societe") + "</p></p>");
      retour.append("</span>");
      
      retour.append("<span id='CLCPL' placeholder='" + utilisateur.getTraduction().traduire("$$complement") + "'>");
      if (utilisateur.getClient().getComplementClient() != null
          && (!utilisateur.getClient().getComplementClient().trim().equalsIgnoreCase("")))
        retour.append(utilisateur.getClient().getComplementClient().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_CLCPL'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$complement") + "</p></p>");
      retour.append("</span>");
      
      retour.append("<span id='CLRUE' placeholder='" + utilisateur.getTraduction().traduire("$$adresse") + " 1'>");
      if (utilisateur.getClient().getRueClient() != null && !utilisateur.getClient().getRueClient().trim().equalsIgnoreCase(""))
        retour.append(utilisateur.getClient().getRueClient().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_CLRUE'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$adresse") + " 1</p></p>");
      retour.append("</span>");
      
      retour.append("<span id='CLLOC'  placeholder='" + utilisateur.getTraduction().traduire("$$adresse") + " 2'>");
      if (utilisateur.getClient().getLocaliteClient() != null && (!utilisateur.getClient().getLocaliteClient().trim().equalsIgnoreCase("")))
        retour.append(utilisateur.getClient().getLocaliteClient().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_CLLOC'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$adresse") + " 2</p></p>");
      retour.append("</span>");
      retour.append("<span id='CLVIL' placeholder='" + utilisateur.getTraduction().traduire("$$ville") + "'>");
      if (utilisateur.getClient().getVilleClient() != null && (!utilisateur.getClient().getVilleClient().trim().equalsIgnoreCase("")))
        retour.append(utilisateur.getClient().getVilleClient().trim());
      else
        retour.append("<p class='placeholderSpan' id='fi_CLVIL'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$ville") + "</p></p>");
      retour.append("</span>");
      retour.append("</div>");
    }
    else
      retour.append("<div id='messagePrincipal'>vous n'�tes pas autoris� � modifier des commandes</div>");
    retour.append("</div>");
    
    // Affiche les commandes/devis du client
    // si type de commande est null, alors on le force � "E" pour extraire les commandes en priorit�
    if (typeCde == null)
      typeCde = "E";
    // si on pagine pas
    if ((utilisateur.getClient() != null) && (utilisateur.getUS_ACCES() != ConstantesEnvironnement.ACCES_CONSULTATION)) {
      
      if (indiceDebut == null)
        GestionMesDonnees.initDerniereListePagination(utilisateur, ((GestionMesDonnees) getMaGestion()).recupererCommandesClient(
            utilisateur, utilisateur.getClient().getNumeroClient(), utilisateur.getClient().getSuffixeClient(), typeCde));
      // si on pagine
      else
        GestionMesDonnees.majListePartiellePagination(utilisateur, indiceDebut);
      
      String texteTitre = utilisateur.getTraduction().traduire("$$vosCommandes");
      if (utilisateur.getDerniereListePartiellePagination() == null || utilisateur.getDernierTypeBon() == null)
        retour.append("<p>Aucune donn�e</p>");
      else {
        
        if (utilisateur.getDernierTypeBon().equals("D"))
          texteTitre = utilisateur.getTraduction().traduire("$$vosDevis");
        
        retour.append("<div class='blocContenu' id='listeCommandes'>");
        retour.append("<h3><span class='puceH3'>&nbsp;</span>" + texteTitre + "</h3>");
        
        if (utilisateur.getDernierTypeBon().equals("D"))
          retour.append(
              "<a href='MesDonnees?type=E' class='mesBoutons'><< " + utilisateur.getTraduction().traduire("$$vosCommandes") + " >></a>");
        else
          retour.append(
              "<a href='MesDonnees?type=D' class='mesBoutons'><< " + utilisateur.getTraduction().traduire("$$vosDevis") + " >></a>");
        
        if (utilisateur.getDerniereListePartiellePagination().size() > 0) {
          // ent�tes de colonnes
          retour.append("<div class='listes' id='enTeteListes'>");
          
          retour.append("<span id='titreE1NUM'>" + utilisateur.getTraduction().traduire("$$numero") + "</span>");
          retour.append("<span id='titreE1RCC'>" + utilisateur.getTraduction().traduire("$$referenceLongue") + "</span>");
          
          retour.append("" + "<span id='titreE1CRE'>" + utilisateur.getTraduction().traduire("$$date") + "</span>"
              + "<span id='titreE1DLP'>" + utilisateur.getTraduction().traduire("$$prevue") + "</span>" + "<span id='titreE1THTL'>"
              + utilisateur.getTraduction().traduire("$$montant") + "</span>" + "<span id='titreSTATUT'>"
              + utilisateur.getTraduction().traduire("$$etat") + "</span>" + "<span id='titreTYPCDE'>"
              + utilisateur.getTraduction().traduire("$$type") + "</span>" + "</div>");
          
          // Affiche de la liste
          String refCde = null;
          boolean isDateValide = false;
          for (int i = 0; i < utilisateur.getDerniereListePartiellePagination().size(); i++) {
            String look = "";
            String verCde = "";
            isDateValide = false;
            // commande qui n'est pas en attente: on ne peut pas la modifier
            if (utilisateur.getDerniereListePartiellePagination().get(i).isPresentField("E1ETA")
                && !utilisateur.getDerniereListePartiellePagination().get(i).getField("E1ETA").toString().trim().equals("0")
                && typeCde.equals("E")) {
              look = "op";
            }
            if (typeCde.equals("D")) {
              isDateValide = ((GestionMesDonnees) getMaGestion()).verifierDateValiditeDevis(utilisateur,
                  utilisateur.getDerniereListePartiellePagination().get(i).getField("E1DAT2").toString().trim());
              int etatDevis = -1;
              try {
                etatDevis = Integer.parseInt(utilisateur.getDerniereListePartiellePagination().get(i).getField("E1TDV").toString().trim());
              }
              catch (Exception e) {
                // TODO: handle exception
              }
              
              if (etatDevis > 2 || !isDateValide) {
                look = "op";
              }
            }
            
            // Si top different de 0, la commande est verouill�
            if (!utilisateur.getDerniereListePartiellePagination().get(i).getField("E1TOP").toString().trim().equals("0")) {
              look = "op";
              verCde = "(v)";
            }
            else
              verCde = "";
            // Si commande verouill�, pas de lien
            if (!verCde.equals("(v)"))
              retour.append(
                  "<a class='listes' href='MesDonnees?cde=" + utilisateur.getDerniereListePartiellePagination().get(i).getField("E1COD")
                      + utilisateur.getDerniereListePartiellePagination().get(i).getField("E1NUM")
                      + utilisateur.getDerniereListePartiellePagination().get(i).getField("E1SUF") + "'>");
            else
              retour.append("<a class='listes' >");
            
            retour.append("<span class='E1NUM" + look + "'>"
                + utilisateur.getDerniereListePartiellePagination().get(i).getField("E1NUM").toString().trim()
                + utilisateur.getDerniereListePartiellePagination().get(i).getField("E1SUF").toString().trim() + "</span>");
            refCde = null;
            if (!utilisateur.getDerniereListePartiellePagination().get(i).getField("E1NCC").toString().trim().equals(""))
              refCde = utilisateur.getDerniereListePartiellePagination().get(i).getField("E1NCC").toString().trim();
            if (!utilisateur.getDerniereListePartiellePagination().get(i).getField("E1RCC").toString().trim().equals("")) {
              if (refCde == null)
                refCde = utilisateur.getDerniereListePartiellePagination().get(i).getField("E1RCC").toString().trim();
              else
                refCde += "/" + utilisateur.getDerniereListePartiellePagination().get(i).getField("E1RCC").toString().trim();
            }
            if (refCde == null)
              refCde = "";
            retour.append("<span class='E1RCC" + look + "'>" + refCde + "</span>");
            retour.append("" + "<span class='E1CRE" + look + "'>"
                + Outils
                    .transformerDateSeriemEnHumaine(utilisateur.getDerniereListePartiellePagination().get(i).getField("E1CRE").toString())
                + "</span>" + "<span class='E1DLP" + look + "'>"
                + Outils
                    .transformerDateSeriemEnHumaine(utilisateur.getDerniereListePartiellePagination().get(i).getField("E1DLP").toString())
                + "</span>" + "<span class='E1THTL" + look + "'>"
                + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getDerniereListePartiellePagination().get(i).getField("E1THTL"))
                + "</span>" + "<span class='STATUT" + look + "'>"
                + afficherEtatBon(utilisateur.getDerniereListePartiellePagination().get(i), isDateValide) + verCde + "</span>");
            retour.append("<span class='TYPCDE" + look + "'>"
                + afficherTypeCde(utilisateur.getDerniereListePartiellePagination().get(i).getField("E1IN18").toString().trim())
                + "</span>");
            
            retour.append("</a>");
          }
        }
      }
    }
    
    // Pagination
    retour.append(afficherPagination(utilisateur, indiceDebut, null));
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Afficher le d�tail d'une seule commande
   */
  private String afficherDetailCommande(Utilisateur utilisateur, String titre, String numCde) {
    if (utilisateur == null)
      return "";
    
    StringBuilder retour = new StringBuilder(10000);
    String typeBon = null;
    String documentLie = null;
    String typeDocLie = null;
    String code = null;
    
    // affiche le titre en fonction du type de commande (commande ou devis)
    code = numCde.substring(0, 1);
    // On attribue � l'utilisateur son dernier type de consultation
    utilisateur.setTypeDonneesEncours(code);
    if (code.equals("E")) {
      typeBon = COMMANDE;
    }
    else {
      typeBon = DEVIS;
      titre = "<h2><span id='titrePage'>D�tail devis</span></h2>";
    }
    
    retour.append(titre);
    retour.append("<div class='blocContenu' id='detailCommande'>");
    
    utilisateur.setListeDeTravail(((GestionMesDonnees) getMaGestion()).recupererDetailCommande(utilisateur, numCde, false));
    
    if (utilisateur.getListeDeTravail() != null) {
      retour.append("<h3><span class='puceH3'>&nbsp;</span><span class='zwwwiip'>" + typeBon + " </span>N� " + numCde.substring(1, 7) + "/"
          + numCde.substring(7, 8) + "</h3>");
      
      // Affiche les donn�es de l'ent�te de la commande
      utilisateur.setRecordTravail(((GestionMesDonnees) getMaGestion()).recupererEnteteCommande(utilisateur, numCde));
      boolean isDateValide = false;
      Magasin mag = null;
      
      if (utilisateur.getRecordTravail() != null) {
        documentLie = ((GestionMesDonnees) getMaGestion()).retournerDocumentLie(utilisateur, numCde);
        
        if (typeBon.equals(DEVIS)) {
          isDateValide = ((GestionMesDonnees) getMaGestion()).verifierDateValiditeDevis(utilisateur,
              utilisateur.getRecordTravail().getField("E1DAT2").toString().trim());
          typeDocLie = COMMANDE;
        }
        else if (typeBon.equals(COMMANDE)) {
          typeDocLie = DEVIS;
        }
        
        retour.append("<div class='sousBlocs' id='enTeteCde'>");
        
        retour.append("<p class='decoH5'>&nbsp;</p>");
        retour.append("<h5>Ent�te</h5>");
        
        if (documentLie != null) {
          retour.append("<label class='labelsZones'>" + typeDocLie + "</label><span id='DOCLIE'><a class='docLie' href='mesDonnees?cde="
              + documentLie.substring(0, 1) + documentLie.substring(4, 10) + documentLie.substring(10, 11) + "'>"
              + documentLie.substring(4, 10) + "/" + documentLie.substring(10, 11) + "</a></span>");
        }
        
        retour.append("<label class='labelsZones'>Statut</label><span id='E1ETA'>"
            + afficherEtatBon(utilisateur.getRecordTravail(), isDateValide) + "</span>");
        
        if (utilisateur.getRecordTravail().isPresentField("E1NCC")
            && !utilisateur.getRecordTravail().getField("E1NCC").toString().trim().equals(""))
          retour.append("<label class='labelsZones'>" + utilisateur.getTraduction().traduire("$$referenceCourte")
              + "</label><span id='E1NCC'>" + utilisateur.getRecordTravail().getField("E1NCC").toString().trim() + "</span>");
        
        if (utilisateur.getRecordTravail().isPresentField("E1RCC")
            && !utilisateur.getRecordTravail().getField("E1RCC").toString().trim().equals(""))
          retour.append("<label class='labelsZones'>" + utilisateur.getTraduction().traduire("$$referenceLongue")
              + "</label><span id='E1RCC'>" + utilisateur.getRecordTravail().getField("E1RCC").toString().trim() + "</span>");
        
        if (utilisateur.getRecordTravail().isPresentField("E1CRE"))
          retour.append("<label class='labelsZones'>Date de cr�ation</label><span id='E1CREd'>"
              + Outils.transformerDateSeriemEnHumaine(utilisateur.getRecordTravail().getField("E1CRE").toString()) + "</span>");
        
        if (utilisateur.getRecordTravail().isPresentField("E1MEX")) {
          if (utilisateur.getRecordTravail().getField("E1MEX").toString().trim().equals("WE")) {
            retour.append("<label class='labelsZones'>Mode de r�cup�ration</label><span id='E1MEX'>Retrait en magasin</span>");
            // ajouter le libell� du magasin de retrait du coup
            if (utilisateur.getRecordTravail().isPresentField("E1MAG")
                && !utilisateur.getRecordTravail().getField("E1MAG").toString().trim().equals("")) {
              mag = new Magasin(utilisateur, utilisateur.getRecordTravail().getField("E1MAG").toString().trim());
              if (mag != null)
                retour
                    .append("<label class='labelsZones'>Magasin de retrait</label><span id='E1MAG'>" + mag.getLibelleMagasin() + "</span>");
            }
          }
          else if (utilisateur.getRecordTravail().getField("E1MEX").toString().trim().equals("WL"))
            retour.append("<label class='labelsZones'>Mode de r�cup�ration</label><span id='E1MEX'>Livraison</span> ");
        }
        
        if (typeBon.equals(COMMANDE)) {
          if (utilisateur.getRecordTravail().isPresentField("E1DLS"))
            retour.append("<label class='labelsZones'>Date souhait�e</label><span id='E1DLS'>"
                + Outils.transformerDateSeriemEnHumaine(utilisateur.getRecordTravail().getField("E1DLS").toString()) + "</span>");
          if (utilisateur.getRecordTravail().isPresentField("E1DLP"))
            retour.append("<label class='labelsZones'>Date pr�vue</label><span id='E1DLP'>"
                + Outils.transformerDateSeriemEnHumaine(utilisateur.getRecordTravail().getField("E1DLP").toString()) + "</span>");
        }
        else if (typeBon.equals(DEVIS)) {
          if (utilisateur.getRecordTravail().isPresentField("E1DAT2"))
            retour.append("<label class='labelsZones'>Date de validit�</label><span id='E1DLS'>"
                + Outils.transformerDateSeriemEnHumaine(utilisateur.getRecordTravail().getField("E1DAT2").toString()) + "</span>");
        }
        
        retour.append("</div>");
        
        // livraison
        if (utilisateur.getRecordTravail().isPresentField("E1MEX")
            && utilisateur.getRecordTravail().getField("E1MEX").toString().trim().equals("WL")) {
          retour.append("<div class='sousBlocs'>");
          retour.append("<p class='decoH5'>&nbsp;</p>");
          retour.append("<h5>Adresse de livraison</h5>");
          
          retour.append("<span id='CLINOM'>");
          if (utilisateur.getRecordTravail().isPresentField("CLINOM")
              && !utilisateur.getRecordTravail().getField("CLINOM").toString().trim().equals(""))
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getRecordTravail().getField("CLINOM").toString().trim()));
          else
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Nom ou raison sociale</p></p>");
          retour.append("</span>");
          
          retour.append("<span id='CLICPL'>");
          if (utilisateur.getRecordTravail().isPresentField("CLICPL")
              && !utilisateur.getRecordTravail().getField("CLICPL").toString().trim().equals(""))
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getRecordTravail().getField("CLICPL").toString().trim()));
          else
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Nom du responsable</p></p>");
          retour.append("</span>");
          
          retour.append("<span id='CLIRUE'>");
          if (utilisateur.getRecordTravail().isPresentField("CLIRUE")
              && !utilisateur.getRecordTravail().getField("CLIRUE").toString().trim().equals(""))
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getRecordTravail().getField("CLIRUE").toString().trim()));
          else
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Adresse - rue</p></p>");
          retour.append("</span>");
          
          retour.append("<span id='CLILOC'>");
          if (utilisateur.getRecordTravail().isPresentField("CLILOC")
              && !utilisateur.getRecordTravail().getField("CLILOC").toString().trim().equals(""))
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getRecordTravail().getField("CLILOC").toString().trim()));
          else
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Adresse - localit�</p></p>");
          retour.append("</span>");
          
          retour.append("<span id='CLIVIL'>");
          if (utilisateur.getRecordTravail().isPresentField("CLICDP")
              && !utilisateur.getRecordTravail().getField("CLICDP").toString().trim().equals(""))
            retour.append(getMaGestion()
                .traiterCaracteresSpeciauxAffichage(utilisateur.getRecordTravail().getField("CLICDP").toString().trim() + " "));
          if (utilisateur.getRecordTravail().isPresentField("CLIVIL")
              && !utilisateur.getRecordTravail().getField("CLIVIL").toString().trim().equals(""))
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getRecordTravail().getField("CLIVIL").toString().trim()));
          else
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Ville</p></p>");
          retour.append("</span>");
          
          retour.append("</div>");
        }
      }
      // On r�cupere les commentaires de la commande
      ArrayList<GenericRecord> listeCom = ((GestionMesDonnees) getMaGestion()).recupererCommentaireCommande(utilisateur, numCde);
      if (listeCom != null && listeCom.size() > 0 && listeCom.get(0).isPresentField("XILIB")
          && !listeCom.get(0).getField("XILIB").toString().trim().equals("")) {
        retour.append("<div class='sousBlocs' id='blocCommentaires'>");
        retour.append("<p class='decoH5'>&nbsp;</p>");
        retour.append("<h5>Commentaires</h5>");
        retour.append("<div id='XILIB'>");
        for (int i = 0; i < listeCom.size(); i++) {
          if (listeCom.get(i).isPresentField("XILIB"))
            retour.append(getMaGestion().traiterCaracteresSpeciauxAffichage(listeCom.get(i).getField("XILIB").toString().trim()));
        }
        
        retour.append("</div>");
        retour.append("</div>");
      }
      // Le bon est-il un bon d'origine Web?
      boolean isBonWeb = utilisateur.getRecordTravail().getField("E1IN18").toString().equals("W");
      // Bons dont la date est non d�pass�e
      if (utilisateur.getClient() != null && utilisateur.getClient().isAutoriseActions()) {
        if (typeBon.equals(COMMANDE) && isBonWeb) {
          // Commandes en attente sans document li�
          if (utilisateur.getRecordTravail().getField("E1ETA").toString().equals("0") && documentLie == null
              && utilisateur.isAutoriseCommande()) {
            retour.append("<div id='optionsCommande'>");
            retour.append("<a id='modifCommande' onClick = \"confirmerUnLien('MesDonnees?reprise=" + numCde
                + "','Voulez vous vraiment modifier cette commande ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Modifier >></a>");
            retour.append("<a id='annulCommande' onClick = \"confirmerUnLien('MesDonnees?annulation=" + numCde
                + "','Voulez vous vraiment annuler cette commande ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Annuler >></a>");
            retour.append("</div>");
          }
        }
        else if (typeBon.equals(DEVIS) && isDateValide) {
          // Devis en attente
          if (utilisateur.getRecordTravail().getField("E1TDV").toString().equals("0") && utilisateur.isAutoriseDevis() && isBonWeb) {
            retour.append("<div id='optionsCommande'>");
            retour.append("<a id='modifCommande' onClick = \"confirmerUnLien('MesDonnees?reprise=" + numCde
                + "','Voulez vous vraiment modifier ce devis ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Modifier >></a>");
            retour.append("<a id='annulCommande' onClick = \"confirmerUnLien('MesDonnees?annulation=" + numCde
                + "','Voulez vous vraiment annuler ce devis ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Annuler >></a>");
            retour.append("</div>");
          }
          // Devis valid�s ou envoy�s (comptoir ou web on ne teste pas isBonWeb
          else if ((utilisateur.getRecordTravail().getField("E1TDV").toString().equals("1")
              || utilisateur.getRecordTravail().getField("E1TDV").toString().equals("2"))) {
            mag = new Magasin(utilisateur, utilisateur.getRecordTravail().getField("E1MAG").toString().trim());
            if (utilisateur.isAutoriseCommande()) {
              retour.append("<div id='optionsCommande'>");
              retour.append("<a class='mesBoutons' id='annulCommande' href='MesDonnees?transform=" + numCde + "&magasin="
                  + mag.getCodeMagasin() + "'> Commander</a>");
              retour.append("</div>");
            }
          }
        }
      }
      
      // ent�tes de colonnes
      retour.append("<div class='listes' id='enTeteListes'>" + "<span id='titreA1ART'>Article</span>"
          + "<span id='titreA1LIB'>D�signation</span>" + "<span id='titreL1PVC'>Tarif H.T</span>" + "<span id='titreL1QTE'>Quantit�</span>"
          + "<span id='titreL1MHT'>Total H.T</span>" + "</div>");
      
      // On affiche la liste d'articles de la commande
      String referenceAffichee = "Aucune";
      String totalEcotaxe = null;
      for (int i = 0; i < utilisateur.getListeDeTravail().size(); i++) {
        // gestion de l'affichage de la r�f�rence fournisseur ou le code article S�rie M
        if (utilisateur.getETB_EN_COURS().getZone_reference_article().equals("CAREF")
            && utilisateur.getListeDeTravail().get(i).isPresentField("CAREF")
            && !utilisateur.getListeDeTravail().get(i).getField("CAREF").toString().trim().equals(""))
          referenceAffichee = utilisateur.getListeDeTravail().get(i).getField("CAREF").toString().trim();
        else if (utilisateur.getListeDeTravail().get(i).isPresentField("L1ART"))
          referenceAffichee = utilisateur.getListeDeTravail().get(i).getField("L1ART").toString().trim();
        else
          referenceAffichee = "Aucune";
        // On v�rifie les non null dans les articles de la ligne
        if (!utilisateur.getListeDeTravail().get(i).isPresentField("A1LIB")) {
          // On attribue un libell� "ecotaxe" au libell� de l'article en question //TODO a retravailler pour les autres
          // cas 'frais de port' etc... gerer les numeros de lignes peut �tre (ex: L1NLI = 9770 ecotaxe)
          if (utilisateur.getListeDeTravail().get(i).isPresentField("L1ERL")
              && utilisateur.getListeDeTravail().get(i).getField("L1ERL").toString().trim().equals("S")
              && utilisateur.getETB_EN_COURS().isGestion_ecotaxe() && utilisateur.getListeDeTravail().get(i).isPresentField("L1NLI")
              && utilisateur.getListeDeTravail().get(i).getField("L1NLI").toString().trim().equals("9770")) {
            utilisateur.getListeDeTravail().get(i).setField("A1LIB", "Ecotaxe");
            totalEcotaxe = Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getListeDeTravail().get(i).getField("L1MHT"));
          }
          else
            utilisateur.getListeDeTravail().get(i).setField("A1LIB", referenceAffichee);
        }
        
        retour.append("<a class='listes' href='#'>" + "<span class='A1ART'>" + referenceAffichee + "</span>" + "<span class='A1LIB'>"
            + utilisateur.getListeDeTravail().get(i).getField("A1LIB") + "</span>" + "<span class='L1PVC'>"
            + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getListeDeTravail().get(i).getField("L1PVC")) + "</span>"
            + "<span class='L1QTE'>"
            + afficherValeurCorrectement(utilisateur, utilisateur.getListeDeTravail().get(i).getField("L1QTE").toString().trim())
            + "</span>" + "<span class='L1MHT'>"
            + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getListeDeTravail().get(i).getField("L1MHT")) + "</span>" + "</a>");
      }
      
      if (utilisateur.getListeDeTravail().size() > 0 && utilisateur.getListeDeTravail().get(0).isPresentField("E1THTL")) {
        retour.append("<p class='messageRubrique'>Total de votre commande</p>");
        retour.append("<div id='piedBon'>");
        
        retour.append("<span class='TOTALHT'>Total HT : "
            + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getListeDeTravail().get(0).getField("E1THTL")) + " </span>");
        if (totalEcotaxe != null)
          retour.append("<span class='TOTALECO'>Dont �cotaxe : " + totalEcotaxe + " </span>");
        
        retour.append("</div>");
        
      }
      else
        retour.append("<p>Probl�me dans la commande, veuillez contacter votre agence.</p>");
      
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * affiche l'info de la commande Web
   */
  private String afficherTypeCde(String donnee) {
    if (donnee == null || donnee.trim().equals(""))
      donnee = "<p class='Nvu'>Comptoir</p><p class='Ncache'>Com</p>";
    // Mobilit� +
    else if (donnee.equals("M"))
      donnee = "<p class='Wvu'>Repr�sentant</p><p class='Wcache'>Rep</p>";
    else if (donnee.equals("W"))
      donnee = "<p class='Wvu'>Web <strong>Shop</strong></p><p class='Wcache'>Web</p>";
    
    return donnee;
  }
  
  /**
   * Permet de formater l'�tat du bon en fonction de son �tat et de sa validit�
   */
  private String afficherEtatBon(GenericRecord pRecord, boolean pIsDateValide) {
    String retour = "";
    
    if (pRecord == null || !pRecord.isPresentField("E1ETA") || !pRecord.isPresentField("E1TDV") || !pRecord.isPresentField("E1COD")) {
      return retour;
    }
    
    String typeBon = pRecord.getField("E1COD").toString().trim();
    String etatBon = null;
    if (typeBon.equals("E")) {
      etatBon = pRecord.getField("E1ETA").toString().trim();
    }
    else if (typeBon.equals("D")) {
      if (!pIsDateValide) {
        return "Validit� d�pass�e";
      }
      etatBon = pRecord.getField("E1TDV").toString().trim();
    }
    
    if (typeBon.equals("E")) {
      if (etatBon.equals("0"))
        retour = "En attente";
      else if (etatBon.equals("1"))
        retour = "Valid�e";
      else if (etatBon.equals("3"))
        retour = "Valid�e";
      // retour = "Reserv�e"; ON REMPLACE RESERVEE PAR VALIDEE
      else if (etatBon.equals("4"))
        retour = "Exp�di�e";
      else if (etatBon.equals("6") || etatBon.equals("7"))
        retour = "Factur�e";
    }
    else if (typeBon.equals("D")) {
      if (etatBon.equals("0"))
        retour = "En attente";
      else if (etatBon.equals("1"))
        retour = "Valid�";
      else if (etatBon.equals("2"))
        retour = "Envoy�";
      else if (etatBon.equals("3"))
        retour = "Sign�";
      else if (etatBon.equals("4"))
        retour = "Validit� d�pass�e";
      else if (etatBon.equals("5"))
        retour = "Perdu";
      else if (etatBon.equals("6"))
        retour = "Cl�tur�";
    }
    
    return retour;
  }
  
}
