
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionMetierBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class MetierBO
 */
public class MetierBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public MetierBO() {
    super(new GestionMetierBO(), "metierBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "clientBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("Maj") != null)
        out.println(afficherContenu(traiterMiseajour((Utilisateur) request.getSession().getAttribute("utilisateur"), request,
            "<h2><span id='titrePage'>Param�tres m�tier</span></h2>"), null, null, null, request.getSession()));
      else
        out.println(afficherContenu(
            afficherFormEtb((Utilisateur) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Param�tres m�tier</span></h2>", request.getParameter("choixEtb")),
            null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des param�tres du client (XWEBSHOP/ENVIRONM)
   */
  private String afficherFormEtb(Utilisateur utilisateur, String titre, String etb) {
    if (utilisateur == null)
      return "";
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    // String retour = titre;
    retour.append("<div class='blocContenu'>");
    
    if (etb == null && utilisateur.getETB_EN_COURS() != null)
      etb = utilisateur.getETB_EN_COURS().getCodeETB();
    
    if (ConstantesEnvironnement.LISTE_ETBS != null && etb != null) {
      String selected = "";
      retour.append("<form action='MetierBO' method='POST' name='choixEtbs' id='choixEtbs'>");
      retour
          .append("<select id='choixETBParams' name='choixEtb' onChange=\"document.location.href = 'MetierBO?choixEtb=' + this.value ;\">");
      for (int i = 0; i < ConstantesEnvironnement.LISTE_ETBS.size(); i++) {
        if (ConstantesEnvironnement.LISTE_ETBS.get(i).getCodeETB().equals(etb))
          selected = " selected ";
        else
          selected = "";
        retour.append("<option value='" + ConstantesEnvironnement.LISTE_ETBS.get(i).getCodeETB() + "' " + selected + ">"
            + ConstantesEnvironnement.LISTE_ETBS.get(i).getLibelleETB() + "</option>");
      }
      retour.append("</select>");
      retour.append("</form>");
      
      ArrayList<GenericRecord> liste = null;
      ArrayList<GenericRecord> listeMonETB = null;
      HashMap<String, String> listeValeurs = null;
      
      // ON travaille le pattern puis les valeurs d�j� saisies pour les comparer
      liste = ((GestionMetierBO) maGestion).recupererPatternParametresEtb(utilisateur);
      listeMonETB = ((GestionMetierBO) maGestion).recupererParametresEtb((Utilisateur) utilisateur, etb);
      if (listeMonETB != null) {
        listeValeurs = new HashMap<String, String>();
        
        for (int i = 0; i < listeMonETB.size(); i++) {
          if (listeMonETB.get(i).isPresentField("EN_CLE") && listeMonETB.get(i).isPresentField("EN_VAL")) {
            listeValeurs.put(listeMonETB.get(i).getField("EN_CLE").toString().trim(),
                listeMonETB.get(i).getField("EN_VAL").toString().trim());
          }
        }
      }
      
      if (liste != null && listeMonETB != null) {
        String typeDonnee = null;
        String valeur = "";
        
        retour.append(
            "<form action='MetierBO' method='POST' name='paramETB'>" + "<input type='hidden' name='etbCours' value = '" + etb + "'/>");
        for (int i = 0; i < liste.size(); i++) {
          // Conditionnement du champs type selon valeur de la zone EN_TYP pour conditionner le input
          if (liste.get(i).isPresentField("EN_TYP") && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))
            // typeDonnee = "text" ;
            typeDonnee = (liste.get(i).getField("EN_TYP").toString().trim());
          else
            typeDonnee = "text";
          
          if (liste.get(i).isPresentField("EN_CLE")) {
            if (listeValeurs.containsKey(liste.get(i).getField("EN_CLE").toString().trim())) {
              if (liste.get(i).isPresentField("EN_TYP") && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))
                valeur =
                    Base64Coder.decodeString(Base64Coder.decodeString(listeValeurs.get(liste.get(i).getField("EN_CLE").toString().trim())));
              else
                valeur = listeValeurs.get(liste.get(i).getField("EN_CLE").toString().trim());
            }
            else
              valeur = "";
            
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + typeDonnee + "' class='valparam' name='" + liste.get(i).getField("EN_CLE").toString().trim()
                + "' value='" + valeur + "'/></div>");
          }
        }
      }
      retour.append("<input type='hidden'  id='Maj' name='Maj' value='1'>");
      retour.append(
          "<input class='bouton' type='submit' value='Mettre � jour' id='MajP' href='javascript:void(0)' onClick=\"traitementEnCours('MetierBO');\"/>");
      retour.append("</form>");
    }
    
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * Traiter la mise � jour des param�tres du client
   */
  private String traiterMiseajour(Utilisateur utilisateur, HttpServletRequest request, String titre) {
    StringBuilder retour = new StringBuilder();
    
    if (titre != null)
      retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    if (((GestionMetierBO) maGestion).miseajourParametresEtb(utilisateur, request))
      retour.append("<div id='messagePrincipal'>Mise � jour effectu�e avec succ�s</div>");
    else
      retour.append("<div id='messagePrincipal'>Erreur dans la mise � jour des param�tres</div>");
    // System.out.println("etbCours: " + request.getParameter("etbCours"));
    retour.append(afficherFormEtb(utilisateur, "<h2><span id='titrePage'>Param�tres m�tier</span></h2>", request.getParameter("etbCours")));
    retour.append("</div>");
    
    return retour.toString();
  }
}
