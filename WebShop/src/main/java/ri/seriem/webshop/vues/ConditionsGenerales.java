
package ri.seriem.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionConditionsGenerales;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Servlet ConditionsGenerales
 */
public class ConditionsGenerales extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public ConditionsGenerales() {
    super(new GestionConditionsGenerales(), "conditionsGenerales", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS
        + "conditionsGenerales.css?" + ConstantesEnvironnement.versionWAR + "' rel='stylesheet'/>", ConstantesEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("conditionsGenerales", "ConditionsGenerales", "Conditions générales", "Terms and conditions"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(
          afficherConditions((Utilisateur) request.getSession().getAttribute("utilisateur"),
              "<h2><span id='titrePage'>"
                  + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$CGV") + "</span></h2>"),
          "conditionsGenerales", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le formulaire des CGV
   */
  private String afficherConditions(Utilisateur utilisateur, String titre) {
    if (utilisateur == null || titre == null)
      return "";
    
    String retour = titre;
    retour += "<div class='blocContenu'>";
    
    utilisateur.recupererUnRecordTravail(((GestionConditionsGenerales) maGestion).RecupererCgv(utilisateur));
    
    if (utilisateur.getRecordTravail() != null && utilisateur.getRecordTravail().isPresentField("CGV_NAME"))
      retour += "<div class='CGV_MESS'>" + utilisateur.getRecordTravail().getField("CGV_NAME").toString().trim() + "</div>";
    else
      retour += "<div class='CGV_MESS'>Pas de conditions générales</div>";
    
    retour += "</div>";
    
    return retour;
  }
}
