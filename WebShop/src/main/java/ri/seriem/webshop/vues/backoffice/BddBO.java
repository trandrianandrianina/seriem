
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionBddBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class BddBO
 */
public class BddBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public BddBO() {
    super(new GestionBddBO(), "BddBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "bddBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("BddBO", "BddBO", "Saisie Requ�te", "Enter a query"));
    listeFilRouge.add(new FilRouge("requeteBDD",
        "<a href='BddBO?fichier=" + ConstantesEnvironnement.PARAM_FILROUGE + "' class='filRougeText'>Un fichier</a>", "R�sultat", "Result"));
    // listeFilRouge.add(new FilRouge("logId", "<a href='logsBO?logId=" + MarbreEnvironnement.PARAM_FILROUGE + "'
    // class='filRougeText'>D�tail log</a>"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("requeteBDD") != null)
        out.println(afficherContenu(afficherRetourRequete(request.getSession(), request.getParameter("requeteBDD"), request), "requeteBDD",
            null, null, request.getSession()));
      else
        out.println(afficherContenu(afficherFormulaire(null, request, request.getSession()), null, null, null, request.getSession()));
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Formulaire de saisie de la requete SQL
   */
  
  private String afficherFormulaire(String requete, HttpServletRequest request, HttpSession sess) {
    StringBuilder retour = new StringBuilder();
    
    if (requete == null)
      requete = "";
    
    retour.append("<h2><span id='titrePage'>Saisie de requ�te</span></h2>");
    
    retour.append("<div class='blocContenu'>");// id='blocContenu'
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Requ�te SQL sur " + ConstantesEnvironnement.BIBLI_WS + "</h3>");
    retour.append("<form action='BddBO' method='POST' name='formRequeteBDD' id='formRequeteBDD'>");
    retour.append("<textarea name='requeteBDD' rows='5' class='zone'>" + requete + "</textarea>");
    retour.append("<input type='submit' class='BTNenvoyer' value='Traiter' id='EnvoyerSQLITE'/>");
    retour.append("</form>");
    retour.append("</div>");
    // affiche la liste des tables pr�sent dans xwebshop
    if (request.getParameter("requeteBDD") != null) {
      retour.append(afficherRetourRequete(sess, request.getParameter("requeteBDD"), request));
    }
    
    retour.append(afficherListeFichier(sess, "", request));
    if (request.getParameter("fichier") != null) {
      retour.append(afficherDetailFichier(sess, "", request.getParameter("fichier"), request));
    }
    return retour.toString();
  }
  
  /**
   * Afficher la liste des fichiers de la biblioth�que WebShop
   */
  private String afficherListeFichier(HttpSession sess, String titre, HttpServletRequest request) {
    String retour = titre;
    
    retour += "<div class='blocContenu'>";
    
    retour += ("<h3><span class='puceH3'>&nbsp;</span>Liste des tables </h3>");
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof Utilisateur) {
      ArrayList<GenericRecord> listeFichiers =
          ((GestionBddBO) maGestion).retournerListeFichier((Utilisateur) sess.getAttribute("utilisateur"));
      if (listeFichiers != null) {
        for (int i = 0; i < listeFichiers.size(); i++)
          retour += "<a class='listeTables' href='bddBO?fichier=" + listeFichiers.get(i).getField("TABLE_NAME") + "'>"
              + listeFichiers.get(i).getField("TABLE_NAME");
      }
      else
        retour += "<p>Pas de description � afficher</p>";
    }
    retour += "</div>";
    return retour;
  }
  
  /**
   * Afficher la description d'un fichier
   */
  private String afficherDetailFichier(HttpSession sess, String titre, String fichier, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof Utilisateur) {
      ArrayList<GenericRecord> listeFields =
          ((GestionBddBO) maGestion).retournerDetailFichier((Utilisateur) sess.getAttribute("utilisateur"), fichier);
      if (listeFields != null) {
        // ent�tes de colonnes
        retour.append("<div  class='listes' id='enTeteListes'>" + "<span id='titreNom'>Nom du champ </span>"
            + "<span id='titreChamp'>type du champ</span></div>");
        for (int i = 0; i < listeFields.size(); i++) {
          retour.append("<div class='listes'>");
          retour.append("<a class='COLUMN_NAME'// href='#'>");
          if (listeFields.get(i).isPresentField("COLUMN_NAME")) {
            retour.append(listeFields.get(i).getField("COLUMN_NAME").toString().trim());
          }
          else {
            retour.append("aucune");
            retour.append("</a>");
          }
          retour.append("<a class='DATA_TYPE'// href='#'>");
          if (listeFields.get(i).isPresentField("DATA_TYPE")) {
            retour.append(listeFields.get(i).getField("DATA_TYPE").toString().trim());
          }
          else {
            retour.append("aucune");
            retour.append("</a>");
          }
          retour.append("</div>");
        }
        
      }
      else {
        retour.append("<p>Pas de champs disponibles</p>");
      }
    }
    retour.append("</div>");
    return retour.toString();
  }
  
  private String afficherRetourRequete(HttpSession sess, String requete, HttpServletRequest request) {
    
    StringBuilder chaine = new StringBuilder();
    int nFields = 0;
    
    // chaine.append(afficherFormulaire(requete, request, sess));
    chaine.append("<div class='blocContenu'>");
    
    chaine.append("<h3><span class='puceH3'>&nbsp;</span>r�sultat de la requ�te </h3>");
    ArrayList<GenericRecord> listeResultat = null;
    listeResultat = ((GestionBddBO) maGestion).retournerRequete((Utilisateur) sess.getAttribute("utilisateur"), requete);
    
    if (listeResultat != null && listeResultat.size() > 0) {
      // recupere le champs
      nFields = listeResultat.get(0).getNumberOfFields();
      chaine.append("<table  class='table' >");// construction de la table pour le r�sultat de la requ�te
      for (int i = 0; i < listeResultat.size(); i++) {
        chaine.append("<TR>");
        for (int j = 0; j < nFields; j++) {
          chaine.append("<TD class='cellule'>" + listeResultat.get(i).getField(j) + "</TD>");
        }
        
        chaine.append("</TR>");
      }
    }
    else
      chaine.append("<p id='retourSQLITE'>pas d'enregistrement</p>");
    
    chaine.append("</table>");
    chaine.append("</div>");
    
    return chaine.toString();
  }
  
}
