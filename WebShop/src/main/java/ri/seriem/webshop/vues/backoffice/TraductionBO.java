
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionTraductionBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class TraductionBO
 */
public class TraductionBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public TraductionBO() {
    super(new GestionTraductionBO(), "traductionBO",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "traductionBo.css' rel='stylesheet'/>", ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("TraductionBO", "TraductionBO", "Tous les mots cl�s", "All mots cles"));
    listeFilRouge.add(new FilRouge("TRCle", "TraductionBO?TRcle=" + ConstantesEnvironnement.PARAM_FILROUGE, "Un mot cl�", "mot Cl�"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if (request.getParameter("moteurRechercheCle") != null)
        out.println(afficherContenu(afficherResultatMoteur(request.getSession(), "<h2><span id='titrePage'>TRADUCTION</span></h2>", request,
            request.getParameter("moteurRechercheCle")), "TRCle", null, null, request.getSession()));
      else if (request.getParameter("TRcle") != null || (request.getParameter("trad") != null))
        out.println(afficherContenu(modifierUnMotCl�(request.getSession(), "<h2><span id='titrePage'>Modification d'un mot cl�</span></h2>",
            request, request.getParameter("TRcle"), "", null), "TRCle", null, null, request.getSession()));
      else if (request.getParameter("majMoCle") != null)
        out.println(afficherContenu(traiterMiseajour(request.getSession(), request, request.getParameter("motCle")), null, null, null,
            request.getSession()));
      else
        out.println(afficherContenu(
            afficherlisteTraduction((Utilisateur) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Traduction</span></h2>", request, request.getParameter("indiceDebut")),
            null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * On affiche la liste des traductions
   */
  private String afficherlisteTraduction(Utilisateur utilisateur, String titre, HttpServletRequest request, String indiceDebut) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    // TRAITEMENT + PAGINATION
    // si on pagine pas
    if (request.getParameter("indiceDebut") == null) {
      GestionTraductionBO.initDerniereListePagination(utilisateur,
          ((GestionTraductionBO) maGestion).retournerListeTraductions(utilisateur));
    }
    // si on pagine
    else
      GestionTraductionBO.majListePartiellePagination((Utilisateur) request.getSession().getAttribute("utilisateur"),
          request.getParameter("indiceDebut"));
    
    retour.append("<div class='blocContenu'>");
    
    // bouton de creation
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Ins�rer une traduction</h3>");
    
    retour.append("<a class='traduire' href='TraductionBO?trad=1' >Nouvelle traduction</a>");
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Rechercher une traduction &nbsp;&nbsp;</h3>");
    
    // moteur de recherche
    retour.append(
        "<form onSubmit='traitementEnCours(null);' action='traductionBO' method='POST' name='moteurRechercheCle' accept-charset=\"utf-8\" >");
    retour.append("<div class='recherche' id='rechTraduK'>");
    retour.append("<label id='lbRechTraduK'>Rechercher une traduction</label>");
    retour.append("<input type='search' name='moteurRechercheCle' id='rechercheInput' placeholder='"
        + utilisateur.getTraduction().traduire("$$Rechercher") + "' required/>");
    retour.append("<a id='validRecherche' href='#' onClick=\"traitementEnCoursForm(document.moteurRechercheCle);\">r</a>");
    retour.append("</div>");
    retour.append("</form>");
    
    if (utilisateur.getDerniereListePartiellePagination() != null && utilisateur.getDerniereListePartiellePagination().size() > 0) {
      // ent�te de colonne
      retour.append("<div class='listes' id='enTeteListes'>" + "<span id='labelMotsCles'>Mots cl�s</span>");
      
      retour.append("</div>");
      
      String lien = null;
      String cle = null;
      String valeur = null;
      
      for (int j = 0; j < utilisateur.getDerniereListePartiellePagination().size(); j++) {
        cle = null;
        valeur = null;
        if (utilisateur.getDerniereListePartiellePagination().get(j).isPresentField("TR_CLE")) {
          cle = utilisateur.getDerniereListePartiellePagination().get(j).getField("TR_CLE").toString().trim();
          lien = "TraductionBO?TRcle=" + cle;
        }
        
        if (utilisateur.getDerniereListePartiellePagination().get(j).isPresentField("TR_VALEUR")) {
          valeur = StringEscapeUtils
              .escapeHtml4(utilisateur.getDerniereListePartiellePagination().get(j).getField("TR_VALEUR").toString().trim());
          lien = "TraductionBO?TRcle=" + cle;
        }
        
        if (cle != null) {
          retour.append("<div class='listes'>");
          
          // d�but de la ligne
          retour.append("<a class='cle' href='" + lien + "'>");
          retour.append(cle);
          retour.append("</a>");
          
          retour.append("<a class='valeurLangue' href='" + lien + "'>");
          retour.append(valeur);
          retour.append("</a>");
          
          retour.append("</div>");
        }
        
      }
      
      // Pagination
      retour.append(afficherPagination(utilisateur, request.getParameter("indiceDebut"), null));
      retour.append("</div>");
    }
    
    return retour.toString();
  }
  
  /**
   * Modifier un mot cl�
   */
  private String modifierUnMotCl�(HttpSession session, String titre, HttpServletRequest request, String motCle, String langue,
      String page) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    page = "modifCle";
    
    retour.append("<div class='blocContenu'>");
    
    if ((Utilisateur) session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof Utilisateur) {
      
      retour.append("<form action='traductionBO' method ='post'>");
      retour.append("<h2><span class='titrePage'>&nbsp;</span>TRADUCTION</h2>");
      if (motCle == null) {
        retour.append("<h3><span class='puceH3'>&nbsp;</span>Ins�rer une nouvelle traduction</h3>");
      }
      else {
        retour.append("<h3><span class='puceH3'>&nbsp;</span>Modifier une traduction</h3>");
      }
      retour.append("<label for='motCle' class='labelTraduction'>Mot Cl� :</label>");
      if (motCle == null) {
        retour.append("<input type ='text' name='motCle' class='motCle' value='' >");
      }
      else {
        retour.append("<input type ='text' name='motCle' class='motCle' value=" + motCle + " readonly>");
      }
      
      // Codes langues
      ((Utilisateur) session.getAttribute("utilisateur"))
          .setListeDeTravail(maGestion.retournerLanguesWS((Utilisateur) session.getAttribute("utilisateur")));
      
      String contenu = null;
      
      if (((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail() != null
          && ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().size() > 0) {
        for (int i = 0; i < ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().size(); i++) {
          // Un enregistrement
          ((Utilisateur) session.getAttribute("utilisateur")).recupererUnRecordTravail(
              ((GestionTraductionBO) maGestion).retourUnMotCleParLangue(((Utilisateur) session.getAttribute("utilisateur")), motCle,
                  ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()));
          if (((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail() != null) {
            contenu = ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail().getField("TR_VALEUR").toString().trim();
            retour.append("<label for='motCle' class='labelTraduction'>"
                + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB").toString()
                + "</label>");
            retour.append("<textarea name='motCle_"
                + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
                + "' class='valeur' >" + contenu + "</textarea>");
          }
          else {
            contenu = "";
            retour.append("<label for='motCle' class='labelTraduction'>"
                + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB").toString()
                + "</label>");
            retour.append("<textarea name='motCle_"
                + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
                + "' class='valeur' >" + contenu + "</textarea>");
          }
        }
        
        retour.append("<input type='hidden'  id='majMoCle' name='majMoCle' value='1'>");
        
        retour.append("<input type='submit' class='btnMAJ' value='MAJ' id='majMoCle'/>");
      }
      else
        retour.append("<div>Codes langues manquants (table LANGUES)</div>");
      
      retour.append("</div>");
    }
    return retour.toString();
  }
  
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String motCle) {
    String chaine = "";
    String TRcle = null;
    if (motCle != null)
      TRcle = motCle;
    
    if (TRcle != null)
      
      chaine += "<div class='blocContenu'>";
    chaine += "<h2><span id='titrePage'>Validation de la traduction</span></h2>";
    
    if (((GestionTraductionBO) maGestion).miseAjourMotCle((Utilisateur) session.getAttribute("utilisateur"), request, TRcle) != -1) {
      chaine += "<div id='messagePrincipal'>Mot cl� mis � jour avec succ�s.</div>";
      chaine +=
          afficherlisteTraduction(((Utilisateur) session.getAttribute("utilisateur")), "", request, request.getParameter("indiceDebut"));
    }
    else {
      chaine += "<div id='messagePrincipal'>Il y a eu une erreur pendant la mise � jour de vos donn�es.</div>";
      chaine += modifierUnMotCl�(session, "", request, request.getParameter("valeurMotCle"), request.getParameter("motCleCode"), null);
    }
    
    chaine += "<div/>";
    
    return chaine;
  }
  
  private String afficherResultatMoteur(HttpSession session, String titre, HttpServletRequest request, String resultat) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    ArrayList<GenericRecord> listeResultat =
        (((GestionTraductionBO) maGestion).retourneMotCleduMoteur((Utilisateur) session.getAttribute("utilisateur"), resultat));
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>R�sultat du moteur &nbsp;&nbsp;</h3>");
    String lien = null;
    if (listeResultat != null && listeResultat.size() > 0) {
      for (int i = 0; i < listeResultat.size(); i++) {
        lien = "TraductionBO?TRcle=" + listeResultat.get(i).getField("TR_CLE");
        
        retour.append("<div class='listes'>");
        retour.append("<a class='cle' href='" + lien + "'>");
        if (listeResultat.get(i).isPresentField("TR_CLE"))
          retour.append(listeResultat.get(i).getField("TR_CLE").toString().trim());
        retour.append("</a>");
        retour.append("</div>");
      }
      
    }
    else
      retour.append("mauvais crit�re de recherche");
    
    retour.append("</div>");
    return retour.toString();
  }
}
