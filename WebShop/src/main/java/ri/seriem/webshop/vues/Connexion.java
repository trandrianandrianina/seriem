
package ri.seriem.webshop.vues;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.GestionConnexion;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Servlet Connexion
 */
public class Connexion extends MouleVues {
  
  private static final long serialVersionUID = 1L;
  
  public Connexion() {
    super(new GestionConnexion(), "connexion",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "connexion.css?" + ConstantesEnvironnement.versionWAR
            + "' rel='stylesheet'/><script src='scripts/connexion.js?" + ConstantesEnvironnement.versionWAR + "'></script>",
        ConstantesEnvironnement.ACCES_NON_DEPLOYE);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      
      Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
      super.traiterPOSTouGET(request, response);
      // par d�faut afficher un acc�s public
      int etatConnexion = ConstantesEnvironnement.ACCES_PUBLIC;
      String retourMessage = "";
      
      ServletOutputStream out = response.getOutputStream();
      
      // Si on passe un message d'�tat de connexion
      if (utilisateur.getUS_ACCES() < ConstantesEnvironnement.ACCES_CLIENT) {
        if (utilisateur.getTentativesConnexions() > ConstantesEnvironnement.MAX_TENT_CONNEX) {
          // Bloquer l'adresse IP que si on est dans une nouvelle session
          if (utilisateur.getTentativesConnexions() == ConstantesEnvironnement.MAX_TENT_CONNEX + 1) {
            System.out.println("Session " + request.getSession().getId() + " Adresse IP " + request.getRemoteAddr()
                + " bloquee temporairement via le navigateur !!");
            // NE pas repasser dans cette boucle avant une nouvelle session
            utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
          }
          etatConnexion = ConstantesEnvironnement.BLOC_SAISIE_MAX;
        }
        else if (request.getParameter("UtilnvMP") != null)
          etatConnexion = ((GestionConnexion) maGestion).validerChgtMP(request);
        // autorisation par mail de changer de mot de passe
        else if (request.getParameter("saisieNVMP") != null)
          etatConnexion = ConstantesEnvironnement.ACCES_ATT_NV_MP;
        // Si on veut changer le mot de passe on v�rifie le profil au pr�alable
        else if (request.getParameter("verifProfil") != null)
          etatConnexion =
              ((GestionConnexion) maGestion).changementMotPasseProfil(utilisateur, request.getParameter("verifProfil"), request.isSecure());
        // message d'erreur ou d'alerte
        else if (request.getParameter("echec") != null)
          etatConnexion = Integer.parseInt(request.getParameter("echec"));
        // validation de profil
        else if (request.getParameter("validation") != null)
          etatConnexion = ((GestionConnexion) maGestion).validationProfil(utilisateur, request.getParameter("validation"));
        else if (request.getParameter("deploiement") != null)
          etatConnexion = ConstantesEnvironnement.ACCES_NON_DEPLOYE;
        else if (request.getParameter("passUser") != null && request.getParameter("loginUser") != null) {
          request.getSession().invalidate();
          etatConnexion = ((GestionConnexion) maGestion).majConnexion(request.getSession(), request.getParameter("loginUser"),
              request.getParameter("passUser"), request.getParameter("passUserCtrl"), request.getParameter("numeroClient"),
              request.getParameter("mobilitePluch"), request.isSecure());
        }
        else
          etatConnexion = ConstantesEnvironnement.ACCES_PUBLIC;
        
        // En fonction de l'�tat d'acc�s de l'utilisateur afficher l'interface adapt�e
        switch (etatConnexion) {
          case ConstantesEnvironnement.ACCES_PAS_DB2:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, "Web Shop inaccessible", "Pas d'acc�s DB2. Veuillez contacter votre agence");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_NON_DEPLOYE:
            retourMessage = afficher_NON_DEPLOYE(request, "Processus de d�ploiement", request.getParameter("deploiement"),
                (Utilisateur) request.getSession().getAttribute("utilisateur"));
            break;
          case ConstantesEnvironnement.ACCES_MAINTENANCE:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, "Web Shop inaccessible",
                "Le Web Shop est actuellement en maintenance. Merci de tenter ult�rieurement.<br/>Pour plus d'informations, contactez votre <a href='contact'>magasin</a>.");
            break;
          // DEMANDE DE CHANGEMENT DE MESSAGE SECURITE SONEPAR
          case ConstantesEnvironnement.ACCES_INEXISTANT:
            retourMessage = afficher_ACCES_ER_MDP(request, "Ce profil n'est pas autoris� � utiliser le Web Shop. Acc�s inexistant");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_AT_MDP:
            retourMessage = afficher_ACCES_AT_MDP(request, null);
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_ER_CONF_MDP:
            retourMessage = afficher_ACCES_AT_MDP(request, "Erreur confirmation mot de passe");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_DEJA_EXISTANT:
            retourMessage =
                afficher_ACCES_ER_MDP(request, "Votre profil existe d�j� dans la base de donn�es.<br/>Merci de saisir vos identifiants");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_AT_VALID:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, "Confirmation de votre inscription",
                "Vous devez valider votre profil sur votre boite mails avant d'acc�der � votre espace client");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_ER_SAISIE:
            retourMessage = afficher_ACCES_ER_MDP(request, "Erreur de saisie");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_ER_MDP:
            retourMessage = afficher_ACCES_ER_MDP(request, null);
            razProfil(request);
            break;
          case ConstantesEnvironnement.BLOC_SAISIE_MAX:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, utilisateur.getTraduction().traduire("$$ACCES_BLOQUE"),
                "Vous avez d�pass� le nombre maximum de tentatives");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_REFUSE:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, utilisateur.getTraduction().traduire("$$ACCES_BLOQUE"),
                "Votre profil n'est plus actif sur le Web Shop. Veuillez vous adresser � votre <a href='contact'>magasin</a>.");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_PUBLIC:
            retourMessage = afficher_pagePrincipale(utilisateur, utilisateur.getTraduction().traduire("$$connexionH3"),
                utilisateur.getTraduction().traduire("$$connexionH3_2"));
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_VAL_PROFIL:
            retourMessage = afficher_pagePrincipale(utilisateur,
                "<span class='superflus'>Vous avez valid� votre profil: </span>veuillez saisir vos identifiants",
                "<span class='superflus'>Vous n'avez pas encore de compte Web Shop: </span>inscrivez vous");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_MULTIPLE_CONNEX:
            retourMessage = afficherSaisieNumeroClient(request);
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_MULTIPLE_INSCRI:
            retourMessage = afficherSaisieNumeroClient(request);
            razProfil(request);
            break;
          
          // Les cas de demande de nouveau mot de passe
          case ConstantesEnvironnement.ACCES_DE_NV_MP:
            retourMessage = afficherConf_NV_MP("Changement de mot de passe",
                "Afin de finaliser votre demande concernant le changement de votre mot passe, veuillez consulter votre boite mail et suivre la proc�dure indiqu�e.");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_NV_MP_ADMIN:
            retourMessage = afficherConf_NV_MP("Changement de mot de passe",
                "Votre mot de passe ne peut �tre chang� sur le Web Shop. Merci de consulter votre administrateur.");
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_ATT_NV_MP:
            retourMessage = afficherSaisieNVMP(utilisateur, "Changement de mot de passe",
                "Veuillez saisir votre identifiant ainsi que vos nouveaux mots de passes", request.getParameter("saisieNVMP"));
            razProfil(request);
            break;
          case ConstantesEnvironnement.ACCES_VAL_NV_MP:
            retourMessage = afficher_ACCES_ER_MDP(request,
                "Votre mot de passe a �t� modifi� avec succ�s. Vous pouvez vous connecter avec vos nouveaux identifiants.");
            razProfil(request);
            break;
          // utilisateurs "connect�s"
          case ConstantesEnvironnement.ACCES_CLIENT:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case ConstantesEnvironnement.ACCES_CONSULTATION:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case ConstantesEnvironnement.ACCES_BOUTIQUE:
            getServletContext().getRequestDispatcher("/catalogue").forward(request, response);
            break;
          case ConstantesEnvironnement.ACCES_REPRES_WS:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case ConstantesEnvironnement.ACCES_RESPON_WS:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case ConstantesEnvironnement.ACCES_ADMIN_WS:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          
          default:
            break;
        }
      }
      else if (request.getParameter("choixETB") != null) {
        retourMessage = afficherChoixEtb(request);
      }
      else if (request.getParameter("etbChoisi") != null) {
        Gestion.attribuerUnETB(utilisateur, request.getParameter("etbChoisi"));
        if (utilisateur.getETB_EN_COURS() != null)
          getServletContext().getRequestDispatcher("/accueil").forward(request, response);
        else
          getServletContext().getRequestDispatcher("/connexion").forward(request, response);
      }
      else
        getServletContext().getRequestDispatcher("/accueil").forward(request, response);
      
      // Affichage
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, null));
      out.println(afficherContenu("<h2><span id='titrePage'>"
          + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$connexionH2") + "</span></h2>"
          + retourMessage, "connexion", null, null, request.getSession()));/**/
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Saisir un nouveau mot de passe
   */
  private String afficherSaisieNVMP(Utilisateur utilisateur, String titre, String message, String mp) {
    String retour = "";
    if (utilisateur == null)
      return retour;
    
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span><span class='superflus'>" + titre + "</h3>";
    if (message != null)
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    
    retour += "<form name ='nvMP' id='nvMP1' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='' type='text' placeholder='"
        + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='nvPassUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$confirmMotDePasse")
        + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='mot de passe' required /></div>";
    retour += "<input type='hidden' name='UtilnvMP' value='" + mp + "' />";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerInscription' value='"
        + utilisateur.getTraduction().traduire("$$valider") + "'/></div>";
    retour += "</form>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher en cas de tentative de changement de mot de passe
   */
  private String afficherConf_NV_MP(String titre, String contenu) {
    String retour = "";
    retour += "<div class='blocContenu' id='blocConnexion'>";
    if (titre != null)
      retour += "<h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>";
    if (contenu != null)
      retour += "<p id='messagePrincipal'>" + contenu + "</p>";
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher la page principale de connexion
   */
  private String afficher_pagePrincipale(Utilisateur utilisateur, String titre1, String titre2) {
    StringBuilder builder = new StringBuilder();
    String loginSaisi = "";
    
    // R�cup�rer le login saisi par l'utilisateur si c'est le cas
    if (utilisateur != null && utilisateur.getUS_LOGIN() != null)
      loginSaisi = utilisateur.getUS_LOGIN();
    
    builder.append("<div class='blocContenu' id='blocConnexion'>");
    if (titre1 != null)
      builder.append("<h3><span class='puceH3'>&nbsp;</span>" + titre1 + "</h3>");
    
    builder.append("<form name ='seConnecter' id='seConnecter' action='connexion' method='POST' accept-charset=\"utf-8\">");
    builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi + "' type='text' placeholder='"
        + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>");
    builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='passUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>");
    builder.append("<div class='lignesFormulaires'><input type ='submit' class='mesBoutons' id='validerConnexion' value='"
        + utilisateur.getTraduction().traduire("Connexion") + "'/></div>");
    builder.append("<div class='lignesFormulaires'><a href='#' id='mpOublie' onClick=\"verifierProfilSaisi('loginUser')\" >"
        + utilisateur.getTraduction().traduire("$$motPasseOublie") + "</a></div>");
    builder.append("</form>");
    builder.append(
        "<img id='imageConnexion' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/imageConnexion.png'/>");
    builder.append("</div>");
    
    if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_PUBLIC) {
      builder.append("<div class='blocContenu' id='blocSinscrire'>");
      if (titre2 != null)
        builder.append("<h3><span class='puceH3'>&nbsp;</span>" + titre2 + "</h3>");
      builder.append("<p id='messagePrincipal'>" + utilisateur.getTraduction().traduire("$$alerteInscription1") + " "
          + ConstantesEnvironnement.LIBELLE_SERVEUR + " " + utilisateur.getTraduction().traduire("$$alerteInscription2") + "</p>");
      builder.append("<form name ='sInscrire' id='sInscrire' action='connexion' method='POST' accept-charset=\"utf-8\">");
      builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>"
          + utilisateur.getTraduction().traduire("$$adresseEmail") + "</label><input name='loginUser' id='nvLoginUser' value ='"
          + loginSaisi + "' type='text' placeholder='" + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>");
      builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
          + "</label><input name='passUser' id='nvPassUser' type='password' placeholder='"
          + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>");
      builder.append(
          "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$confirmMotDePasse")
              + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='"
              + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>");
      builder.append("<div class='lignesFormulaires'><input type ='submit'  class='mesBoutons' id='validerInscription' value=\""
          + utilisateur.getTraduction().traduire("$$sinscrire") + "\"/></div>");
      builder.append("</form>");
      builder.append("<img id='imageInscription' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/imageInscription.png'/>");
      builder.append("</div>");
    }
    
    return builder.toString();
  }
  
  /**
   * Virer toutes les propri�t�s de l'utilisateur -> r�initialiser la totale
   */
  private void razProfil(HttpServletRequest request) {
    if (request.getSession().getAttribute("utilisateur") != null
        && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
      ((Utilisateur) request.getSession().getAttribute("utilisateur")).setUS_LOGIN(null);
      ((Utilisateur) request.getSession().getAttribute("utilisateur")).setUS_ACCES(ConstantesEnvironnement.ACCES_PUBLIC);
    }
  }
  
  /**
   * Interface de d�ploiement du Web Shop. C'est ici qu'on saisit toutes les variables propres au d�ploiement de
   * l'application
   */
  private String afficher_NON_DEPLOYE(HttpServletRequest request, String titre, String deploiement, Utilisateur utilisateur) {
    String retour = "";
    retour += "<div class='blocContenu'>";
    if (titre != null)
      retour += "<h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>";
    
    if (deploiement == null) {
      // Affichage du formulaire de d�ploiement
      retour += "<form name='traitementDeploiement' action='connexion?deploiement=1' method='POST' accept-charset=\"utf-8\">";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>Profil administrateur</label><input id='PROFIL_ADMIN' name='PROFIL_ADMIN' value='' type='text' maxlength='10' required/></div>";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>MP administrateur</label><input id='MP_ADMIN' name='MP_ADMIN' value='' type='password' maxlength='20' required/></div>";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>Adresse publique AS400</label><input id='ADRESSE_PUBLIC_SERVEUR' name='ADRESSE_PUBLIC_SERVEUR' value='' type='text' maxlength='50' required/></div>";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>Port publique AS400</label><input id='PORT_SERVEUR' name='PORT_SERVEUR' value=' ' type='text' maxlength='4' /></div>";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>Environnement AS400</label><input id='ENVIRONNEMENT' name='ENVIRONNEMENT' value='' type='text' maxlength='10' required/></div>";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>Lettre environnement</label><input id='LETTRE_ENV' name='LETTRE_ENV' value='' type='text' maxlength='1' required/></div>";
      retour +=
          "<div class='lignesFormulaires'><label class='labelDeploi'>Biblioth�que clients</label><input id='BIBLI_CLIENTS' name='BIBLI_CLIENTS' value='' type='text' maxlength='10' required/></div>";
      retour += "<div class='lignesFormulaires'><input type ='submit' id='validerParametres' value='Valider les donn�es'/></div>";
      retour += "</form>";
    }
    else {
      if (((GestionConnexion) maGestion).traiterLeDeploiement(request, utilisateur)) {
        retour += "<p>Je suis d�ploy� correctement</p>";
        request.getSession().invalidate();
      }
      else
        retour += "<p>Probl�me de d�ploiement</p>";
    }
    
    retour += "</div>";
    return retour;
  }
  
  /**
   * Formulaire d'inscription
   */
  private String afficher_ACCES_AT_MDP(HttpServletRequest request, String message) {
    String retour = "";
    String loginSaisi = "";
    
    // R�cup�rer le login saisi par l'utilisateur si c'est le cas
    if ((Utilisateur) request.getSession().getAttribute("utilisateur") != null
        && ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_LOGIN() != null)
      loginSaisi = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_LOGIN();
    
    retour += "<div class='blocContenu'>";
    retour +=
        "<h3><span class='puceH3'>&nbsp;</span><span class='superflus'>Vous n'avez pas encore de compte client: </span>inscrivez vous</h3>";
    if (message != null)
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    
    retour += "<form name ='sInscrire' id='sInscrire2' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi
        + "' type='text' placeholder='adresse mail' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='nvPassUser' type='password' placeholder='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$confirmMotDePasse")
        + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerInscription' value=\""
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$sinscrire") + "\"/></div>";
    retour += "</form>";
    retour += "<img id='imageInscription' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/decoration/imageInscription.png'/>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Interface d'inscription client au Web Shop. C'est ici qu'on saisit toutes les variables propres au d�ploiement de
   * l'application
   */
  private String afficher_ACCES_ER_MDP(HttpServletRequest request, String message) {
    String retour = "";
    String loginSaisi = "";
    
    // R�cup�rer le login saisi par l'utilisateur si c'est le cas
    if ((Utilisateur) request.getSession().getAttribute("utilisateur") != null
        && ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_LOGIN() != null)
      loginSaisi = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_LOGIN();
    
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>Vos informations <span class='superflus'>de connexion</span> sont incorrectes</h3>";
    
    if (message != null)
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    retour += "<form name ='seConnecter' id='seConnecter' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi + "' type='text' placeholder='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$adresseEmail")
        + "' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='passUser' type='password' placeholder='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerConnexion' value='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("Connexion") + "'/></div>";
    retour += "<div class='lignesFormulaires'><a href='#' id='mpOublie' onClick=\"verifierProfilSaisi('loginUser')\" >"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motPasseOublie") + "</a></div>";
    retour += "</form>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher le message de bloquage de la sasie utilisateurs
   */
  private String afficher_BLOQUAGE_MESSAGE(HttpServletRequest request, String titre, String message) {
    String retour = "";
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>";
    
    if (message != null)
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher l'interface qui permet de pr�ciser le num�ro du client
   */
  private String afficherSaisieNumeroClient(HttpServletRequest request) {
    String retour = "";
    String loginSaisi = "";
    boolean isInscription = false;
    
    // R�cup�rer le login saisi par l'utilisateur si c'est le cas
    if ((Utilisateur) request.getSession().getAttribute("utilisateur") != null
        && ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_LOGIN() != null) {
      loginSaisi = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_LOGIN();
      isInscription =
          ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_ACCES() == ConstantesEnvironnement.ACCES_MULTIPLE_INSCRI;
    }
    
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>";
    if (isInscription)
      retour += "Formulaire d'inscription";
    else
      retour += "Connexion utilisateur";
    retour += "</h3>";
    
    retour += "<p id='messagePrincipal'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$txtPlusieursClients") + "</p>";
    
    retour += "<form name ='seConnecter' id='seConnecter' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi + "' type='text' placeholder='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$adresseEmail")
        + "' required /></div>";
    retour +=
        "<div class='lignesFormulaires'><label class='labelConnexion'>Num�ro client</label><input name='numeroClient' id='numeroClient' maxlength= '6' type='text' placeholder='client' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='passUser' type='password' placeholder='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse") + "' required /></div>";
    if (isInscription)
      retour += "<div class='lignesFormulaires'><label class='labelConnexion'>"
          + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$confirmMotDePasse")
          + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='"
          + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$motDePasse")
          + "' required /></div>";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerConnexion' value='"
        + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("Connexion") + "'/></div>";
    retour += "</form>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * afficher l'interface qui permet de choisir l'�tablissement
   */
  private String afficherChoixEtb(HttpServletRequest request) {
    StringBuilder builder = new StringBuilder(1000);
    
    builder.append("<div class='blocContenu'>");
    
    builder.append("<h3><span class='puceH3'>&nbsp;</span>Choix de l'�tablissement de travail</h3>");
    builder.append(
        "<p id='messagePrincipal'>Afin d'acc�der aux diff�rents modules, vous devez choisir un �tablissement de travail pour votre session</p>");
    
    if (ConstantesEnvironnement.LISTE_ETBS != null && ConstantesEnvironnement.LISTE_ETBS.size() > 0) {
      for (int i = 0; i < ConstantesEnvironnement.LISTE_ETBS.size(); i++)
        builder.append("<a class = 'liensBoutons' href='connexion?etbChoisi=" + ConstantesEnvironnement.LISTE_ETBS.get(i).getCodeETB()
            + "'>" + ConstantesEnvironnement.LISTE_ETBS.get(i).getLibelleETB() + "</a>");
    }
    builder.append("<a class = 'liensBoutons' href='accueil?deconnecter=1'>Annuler</a>");
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
}
