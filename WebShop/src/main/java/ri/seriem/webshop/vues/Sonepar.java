
package ri.seriem.webshop.vues;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionSonepar;

/**
 * Servlet implementation class Sonepar
 */
public class Sonepar extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Sonepar() {
    super(new GestionSonepar(), "sonepar", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "sonepar.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_PUBLIC);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      out.println(afficherContenu(afficherSonepar(request, "<h2><span id='titrePage'>Groupe Sonepar</span></h2>"), "sonepar", null, null,
          request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affiche l'ensemble des �lements de la page Sonepar
   */
  private String afficherSonepar(HttpServletRequest request, String titre) {
    String retour = titre;
    retour += "<div class='blocContenu'>";
    
    retour += "</div>";
    
    return retour;
  }
  
}
