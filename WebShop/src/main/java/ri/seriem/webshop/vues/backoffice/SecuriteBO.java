
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionSecuriteBO;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class SecuriteBO
 */
public class SecuriteBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public SecuriteBO() {
    super(new GestionSecuriteBO(), "securiteBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "securiteBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficherAccueilSecurite("<h2><span id='titrePage'>Gestion de la s�curit�</span></h2>", request), null,
          null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private String afficherAccueilSecurite(String titre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    retour.append("je suis dans le contenu afficherAccueilSecurite");
    
    retour.append("</div>");
    
    return retour.toString();
  }
}
