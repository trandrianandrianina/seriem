
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionLogsBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class LogsBO
 */
public class LogsBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public LogsBO() {
    super(new GestionLogsBO(), "logsBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "logsBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("logsBO", "logsBO", "Tous les logs", "All logs"));
    listeFilRouge.add(new FilRouge("dateLog", "logsBO?dateLog=" + ConstantesEnvironnement.PARAM_FILROUGE, "Une date", "A date"));
    // listeFilRouge.add(new FilRouge("logId", "logsBO?logId=" + MarbreEnvironnement.PARAM_FILROUGE,"Un log","A log"));
    listeFilRouge.add(new FilRouge("debug", "logsBO", "Debug", "Debug"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    // if(!isEnModeEdition(request,response))
    // {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if (request.getParameter("debug") != null)
        out.println(afficherContenu(traiterVariableDebug(request.getSession(), request), null, null, null, request.getSession()));// }
      else if (request.getParameter("dateLog") != null || request.getParameter("logFichier") != null)
        out.println(afficherContenu(afficherLogsDuneDate(request, (Utilisateur) request.getSession().getAttribute("utilisateur"),
            "<h2><span id='titrePage'>Gestion des logs</span></h2>", request.getParameter("indiceDebut"),
            request.getParameter("LogFichier")), "dateLog", request.getParameter("dateLog"), null, request.getSession()));
      else
        out.println(afficherContenu(afficherDatesLogs(request.getSession(), "<h2><span id='titrePage'>Gestion des logs</span></h2>",
            request.getParameter("indiceDebut"), request), null, null, null, request.getSession()));
    }
    catch (Exception e) {
      logs.forcerLogErreur("ERREUR " + e.getMessage());
    }
    // }
  }
  
  /**
   * Afficher une liste de dates o� des logs sont disponibles
   */
  private String afficherDatesLogs(HttpSession sess, String titre, String indiceDebut, HttpServletRequest request) {
    String retour = titre;
    
    retour += "<div class='blocContenu'>";
    
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof Utilisateur) {
      retour += "<h3><span class='puceH3'>&nbsp;</span>Tous les logs </h3>";
      retour += afficherModeDebug();
      ArrayList<String> listeLogDebug = ((GestionLogsBO) maGestion).listeFileLogDebug();
      
      if (listeLogDebug != null) {
        for (int i = 0; i < listeLogDebug.size(); i++) {
          retour += ("<a  class='datesLogs' href='logsBO?logFichier=" + listeLogDebug.get(i) + "'>" + listeLogDebug.get(i));
        }
      }
    }
    retour += ("</div>");
    return retour;
    
  }
  
  /**
   * Bouton qui affiche le mode debug
   * @return
   */
  private String afficherModeDebug() {
    StringBuilder builder = new StringBuilder();
    builder.append("<form action='LogsBO' method='post'>");
    builder.append("<input type='hidden'  id='debug' name='debug' value='1'>");
    
    if (ConstantesEnvironnement.MODE_DEBUG)
      builder.append("<input type='submit' class='btnMAJ' name='debug' value='D�sactiver le mode Debug' />");
    
    else
      builder.append("<input type='submit' class='btnMAJ' name='debug' value='Activer le mode Debug' />");
    builder.append("</form>");
    
    return builder.toString();
  }
  
  /**
   * Afficher une liste de logs pour une date donn�e
   */
  private String afficherLogsDuneDate(HttpServletRequest request, Utilisateur user, String titre, String indiceDeb, String file) {
    String retour = titre;
    file = request.getParameter("logFichier");
    
    retour += "<div class='blocContenu'>";
    ArrayList<String> liste = ((GestionLogsBO) getMaGestion()).detailFichierLog(file);
    
    if (file.length() > 10)
      retour += "<h3><span class='puceH3'>&nbsp;</span>Logs du " + file + "</h3>";
    
    else
      retour += "<h3><span class='puceH3'>&nbsp;</span>Logs du " + file.substring(12, 22) + "</h3>";
    
    if (liste != null) {
      for (int i = 0; i < liste.size(); i++)
        
        retour += "<a class='listeLogs' >" + (liste.get(i)) + "</a>";
      
    }
    
    retour += "</div>";
    return retour;
  }
  
  private String traiterVariableDebug(HttpSession sess, HttpServletRequest request) {
    String chaine = "";
    boolean etatDebug = ((GestionLogsBO) maGestion).activeLogsDebug((Utilisateur) sess.getAttribute("utilisateur"));
    
    chaine += "<div class='blocContenu'>";
    if (!etatDebug) {
      chaine += ("<div id='messagePrincipal'>Variable debug desactiv�e</div>");
      ConstantesEnvironnement.MODE_DEBUG = false;
    }
    else if (etatDebug) {
      chaine += ("<div id='messagePrincipal'>Variable debug activ�e</div>");
      ConstantesEnvironnement.MODE_DEBUG = true;
    }
    
    chaine += afficherDatesLogs(sess, "", request.getParameter("indiceDebut"), request);
    
    chaine += "</div>";
    
    return chaine;
  }
  
}
