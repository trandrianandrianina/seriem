
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionUtilisateursBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class UtilisateursBO
 */
public class UtilisateursBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public UtilisateursBO() {
    super(new GestionUtilisateursBO(), "utilisateursBO",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS
            + "utilisateursBO.css' rel='stylesheet'/><script src='scripts/utilisateursBO.js'></script>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("supprimerUtil") != null)
        out.println(
            afficherContenu(suppressionResponsable(request.getSession(), "<h2><span id='titrePage'>Gestion des utilisateurs</span></h2>",
                request.getParameter("supprimerUtil")), null, null, null, request.getSession()));
      else if (request.getParameter("creaRespons") != null)
        out.println(afficherContenu(
            ajoutProfil((Utilisateur) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Gestion des utilisateurs</span></h2>", request, ConstantesEnvironnement.ACCES_RESPON_WS),
            null, null, null, request.getSession()));
      else if (request.getParameter("creaBoutique") != null)
        out.println(afficherContenu(
            ajoutProfil((Utilisateur) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Gestion des utilisateurs</span></h2>", request, ConstantesEnvironnement.ACCES_BOUTIQUE),
            null, null, null, request.getSession()));
      else
        out.println(afficherContenu(afficherListeUtilisateurs((Utilisateur) request.getSession().getAttribute("utilisateur"),
            "<h2><span id='titrePage'>Gestion des utilisateurs</span></h2>"), null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des utilisateurs
   */
  private String afficherListeUtilisateurs(Utilisateur utilisateur, String titre) {
    if (utilisateur == null)
      return "";
    StringBuilder builder = new StringBuilder();
    builder.append(titre);
    
    builder.append("<div class='blocContenu'>");
    
    builder.append("<h3><span class='puceH3'>&nbsp;</span>Tous les Utilisateurs</h3>");
    
    utilisateur.setListeDeTravail(((GestionUtilisateursBO) maGestion).retournerListeUtilis(utilisateur));
    
    // Filtre Administrateur
    builder.append(
        "<div class='accordeon' id='filtreAdmin'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresAdmin','filtreAdmin');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresAdmin','filtreAdmin');\">Administrateur</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresAdmin','filtreAdmin');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltresAdmin'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_ADMIN_WS, ConstantesEnvironnement.ACCES_ADMIN_WS));
    builder.append("</div>");
    
    // Filtre Responsable
    builder.append(
        "<div class='accordeon' id='filtreResp'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresResp','filtreResp');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresResp','filtreResp');\">Responsable</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresResp','filtreResp');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltresResp'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_RESPON_WS, ConstantesEnvironnement.ACCES_RESPON_WS));
    builder.append(afficherAjoutProfilAS400(utilisateur, "", "creaRespons"));
    builder.append("</div>");
    
    // Filtre repr�sentant
    builder.append(
        "<div class='accordeon' id='filtreRepresentant'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltreRepresentant','filtreRepresentant');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltreRepresentant','filtreRepresentant');\">Repr�sentant</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltreRepresentant','filtreRepresentant');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltreRepresentant'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_REPRES_WS, ConstantesEnvironnement.ACCES_REPRES_WS));
    builder.append("</div>");
    
    // Filtre BOUTIQUE
    builder.append(
        "<div class='accordeon' id='filtreBoutique'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresBout','filtreBoutique');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresBout','filtreBoutique');\">Profils Boutique</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresBout','filtreBoutique');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltresBout'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_BOUTIQUE, ConstantesEnvironnement.ACCES_BOUTIQUE));
    builder.append(afficherAjoutProfilAS400(utilisateur, "", "creaBoutique"));
    builder.append("</div>");
    
    // Filtre Client en consultation
    builder.append(
        "<div class='accordeon' id='filtreClientConsult'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltreClientConsult','filtreClientConsult');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltreClientConsult','filtreClientConsult');\">Clients en consultation</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltreClientConsult','filtreClientConsult');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltreClientConsult'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_CONSULTATION, ConstantesEnvironnement.ACCES_CONSULTATION));
    builder.append("</div>");
    
    // Filtre Client
    builder.append(
        "<div class='accordeon' id='filtreClient'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresClient','filtreClient');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresClient','filtreClient');\">Client</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresClient','filtreClient');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltresClient'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_AT_VALID, ConstantesEnvironnement.ACCES_CLIENT));
    builder.append("</div>");
    
    // Filtre Public
    builder.append(
        "<div class='accordeon' id='filtrePublic'><a class='ouvrirAccordeonG' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresPublic','filtrePublic');\">&nbsp;</a><a class='titreAccordeon' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresPublic','filtrePublic');\">Public</a><a class='ouvrirAccordeonD' href='#' onClick=\"gererOuvertureFiltres('contenuFiltresPublic','filtrePublic');\">&nbsp;</a></div>");
    builder.append("<div class='contenuAccordeon' id='contenuFiltresPublic'>");
    builder.append(afficherListeUtilisateurs(utilisateur, ConstantesEnvironnement.ACCES_PUBLIC, ConstantesEnvironnement.ACCES_PUBLIC));
    builder.append("</div>");
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * On affiche la liste des clients dans l'interface utilisateur
   */
  private String afficherListeUtilisateurs(Utilisateur pUtil, int pDebut, int pFin) {
    if (pUtil == null || pUtil.getListeDeTravail() == null) {
      return "";
    }
    
    boolean onVeutUnClient = false;
    
    StringBuilder builder = null;
    String autorise = "Non autoris�";
    int acces = -3;
    
    // Si on est un profil de type client
    onVeutUnClient = (pDebut >= ConstantesEnvironnement.ACCES_AT_VALID && pFin < ConstantesEnvironnement.ACCES_BOUTIQUE);
    
    if (builder == null) {
      builder = new StringBuilder();
      builder.append("" + "<div class='EnTeteListeUtil'>" + "<div class='etbUtil'>Etb</div>" + "<div class='loginUtil'>Login</div>"
          + "<div class='statutUtil'>Statut</div>");
      if (onVeutUnClient) {
        builder.append("<div class='autoriUtil'>Autorisation</div>");
      }
      builder.append("<div class='dateCreUtil'>Cr�ation</div>" + "</div>");
      if (acces == ConstantesEnvironnement.ACCES_BOUTIQUE || acces == ConstantesEnvironnement.ACCES_RESPON_WS) {
        builder.append("<div class='suppUtil'>Supprimer</div>" + "</div>");
      }
    }
    
    boolean corrompu = false;
    for (GenericRecord record : pUtil.getListeDeTravail()) {
      corrompu = false;
      try {
        acces = Integer.parseInt(record.getField("US_ACCES").toString().trim());
      }
      catch (NumberFormatException e) {
        return "";
      }
      
      // Si le profil doit �tre affich�
      if (acces >= pDebut && acces <= pFin) {
        autorise = "Non autoris�";
        
        builder.append("<div class='liste'>" + "<div class='etbUtil'>" + record.getField("US_ETB") + "</div>" + "<div class='loginUtil'>"
            + record.getField("US_LOGIN") + "</div>");
        if (onVeutUnClient && record.isPresentField("RENET") && record.getField("RENET").toString().trim().toUpperCase()
            .equals(record.getField("US_LOGIN").toString().trim().toUpperCase())) {
          builder.append("<div class='statutUtil'>" + interpreterStatutWebShop(acces) + "</div>");
        }
        else if (!onVeutUnClient) {
          builder.append("<div class='statutUtil'>" + interpreterStatutWebShop(acces) + "</div>");
        }
        else {
          corrompu = true;
          builder.append("<div class='statutUtil'>Corrompu</div>");
        }
        if (onVeutUnClient) {
          if (record.isPresentField("RWACC") && record.getField("RWACC").toString().trim().equals("W") && !corrompu) {
            autorise = "Autoris�";
          }
          // Le contact est supprim� de la base de l'ERP
          else if (!record.isPresentField("RENUM") && !corrompu) {
            autorise = "Supprim�";
          }
          else if (corrompu) {
            autorise = "Corrompu";
          }
          
          builder.append("<div class='autoriUtil'>" + autorise + "</div>");
        }
        
        builder
            .append("<div class='dateCreUtil'>" + Outils.transformerDateSeriemEnHumaine(record.getField("US_DATC").toString()) + "</div>");
        
        if (acces == ConstantesEnvironnement.ACCES_BOUTIQUE || acces == ConstantesEnvironnement.ACCES_RESPON_WS) {
          builder.append("<a class='suppUtil' href='#' onClick=\"supprimerResponsable(" + record.getField("US_ID") + " );\"> "
              + "<img id ='imageSuppr' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/suppression.png'/>"
              + "</a>");
        }
        
        if (record.isPresentField("CLNOM")) {
          builder.append("<div class='clientUtil'>" + record.getField("CLNOM") + "</div>");
        }
        
        builder.append("</div>");
      }
    }
    
    return builder.toString();
  }
  
  /**
   * Traduire le code d'acc�s au WebShop et retourner le libell� correspondant
   */
  private String interpreterStatutWebShop(int pAcces) {
    String retour = "";
    
    switch (pAcces) {
      case ConstantesEnvironnement.ACCES_PUBLIC:
        retour = "Acc�s public";
        break;
      case ConstantesEnvironnement.ACCES_AT_VALID:
        retour = "Validation Mail";
        break;
      case ConstantesEnvironnement.ACCES_ATT_NV_MP:
        retour = "Attente nouveau password";
        break;
      case ConstantesEnvironnement.ACCES_NV_MP_ADMIN:
        retour = "Demande nouveau password";
        break;
      case ConstantesEnvironnement.ACCES_DE_NV_MP:
        retour = "Demande nouveau password";
        break;
      case ConstantesEnvironnement.ACCES_VAL_NV_MP:
        retour = "Validation password";
        break;
      case ConstantesEnvironnement.ACCES_VAL_PROFIL:
        retour = "Validation mail";
        break;
      case ConstantesEnvironnement.ACCES_MULTIPLE_CONNEX:
        retour = "PLusieurs clients";
        break;
      case ConstantesEnvironnement.ACCES_MULTIPLE_INSCRI:
        retour = "Plusieurs inscriptions";
        break;
      case ConstantesEnvironnement.ACCES_CLIENT:
        retour = "Contact actif";
        break;
      case ConstantesEnvironnement.ACCES_CONSULTATION:
        retour = "Consultation";
        break;
      case ConstantesEnvironnement.ACCES_BOUTIQUE:
        retour = "Boutique";
        break;
      case ConstantesEnvironnement.ACCES_REPRES_WS:
        retour = "Repr�sentant";
        break;
      case ConstantesEnvironnement.ACCES_RESPON_WS:
        retour = "Gestionnaire";
        break;
      case ConstantesEnvironnement.ACCES_ADMIN_WS:
        retour = "Administrateur";
        break;
      default:
        break;
    }
    
    return retour;
  }
  
  /**
   * formulaire de cr�ation d'un responsable
   */
  private String afficherAjoutProfilAS400(Utilisateur utilisateur, String titre, String typeUtil) {
    StringBuilder builder = new StringBuilder();
    String nvLogin = "";
    String etb = " ";
    int acces = 30;
    builder.append("<form name='formAjoutProfil' id='nvLoginUser' action='UtilisateursBO' method='post'>");
    builder.append("<label class='creation'>Login </label>");
    builder.append(
        "<input name='loginUser' id='loginUser' value ='" + nvLogin.toUpperCase() + "' type='text' placeholder='profil AS400' required />");
    builder.append("<input  type='hidden'name='etb' value='" + etb + "'>");
    builder.append("<input  type='hidden' name='accesUser' value='" + acces + "'/>");
    builder.append("<input type='hidden' name='" + typeUtil + "' value='1'/>");
    builder.append("<input type ='submit' class='Boutons' value=' '/>");
    builder.append("</form>");
    
    return builder.toString();
  }
  
  /**
   * traitement de l'ajout d'un responsable dans la table userw
   */
  private String ajoutProfil(Utilisateur utilisateur, String titre, HttpServletRequest request, int pAcces) {
    String chaine = titre;
    chaine += "<div class='blocContenu'>";
    int retour = ((GestionUtilisateursBO) maGestion).ajoutProfilAs400(utilisateur, request, pAcces);
    if (retour == -2) {
      chaine += ("<div id='messagePrincipal'>Le Profil n'a pas les droits S�rie M</div>");
    }
    else if (retour == -1) {
      chaine += ("<div id='messagePrincipal'>Probl�me SQL</div>");
    }
    else if (retour == 0) {
      chaine += ("<div id='messagePrincipal'>Probl�me de mise � jour des donn�es</div>");
    }
    else if (retour == 1) {
      chaine += ("<div id='messagePrincipal'>Mise � jour du profil " + request.getParameter("loginUser").toString().trim()
          + " effectu�e avec succ�s</div>");
    }
    else if (retour > 1) {
      chaine +=
          ("<div id='messagePrincipal'Il exsite d�j� plusieurs profils " + request.getParameter("loginUser").toString().trim() + " </div>");
    }
    
    chaine += afficherListeUtilisateurs(utilisateur, "");
    chaine += "</div>";
    return chaine;
  }
  
  /**
   * formulaire pour suppression d'un responsable
   */
  private String suppressionResponsable(HttpSession session, String titre, String idUser) {
    
    int userId = 0;
    if ((idUser) != null) {
      try {
        userId = Integer.parseInt(idUser);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    String chaine = titre;
    chaine += "<div class='blocContenu'>";
    int retour = ((GestionUtilisateursBO) maGestion).suppressionResponsable((Utilisateur) session.getAttribute("utilisateur"), userId);
    if (retour < 1) {
      chaine += ("<div id='messagePrincipal'>Erreur de suppression de profil</div>");
    }
    else if (retour == 1) {
      chaine += ("<div id='messagePrincipal'>Succ�s de la suppression du profil</div>");
    }
    
    chaine += afficherListeUtilisateurs((Utilisateur) session.getAttribute("utilisateur"), "");
    chaine += "</div>";
    return chaine;
  }
}
