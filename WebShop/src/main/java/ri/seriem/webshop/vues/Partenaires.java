
package ri.seriem.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionPartenaires;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Servlet implementation class Partenaires
 */
public class Partenaires extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Partenaires() {
    super(new GestionPartenaires(), "partenaires",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "partenaires.css?" + ConstantesEnvironnement.versionWAR + "' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("partenaires", "partenaires", "Partenaires", "Partners"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      out.println(afficherContenu(afficherAccueil((Utilisateur) request.getSession().getAttribute("utilisateur"),
          "<h2><span id='titrePage'>Partenaires</span></h2>"), "partenaires", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * affiche l'accueil de la page partenaires
   */
  private String afficherAccueil(Utilisateur utilisateur, String titre) {
    StringBuilder retour = null;
    
    if (utilisateur == null)
      return "";
    
    retour = new StringBuilder(15000);
    
    retour.append(titre);
    
    retour.append("<div class='blocContenu' >");
    
    utilisateur.setListeDeTravail(((GestionPartenaires) maGestion).retournerListePartenaires(utilisateur));
    
    if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0) {
      for (int i = 0; i < utilisateur.getListeDeTravail().size(); i++) {
        retour.append(
            "<a class='blocsPartenaires' href=\"" + utilisateur.getListeDeTravail().get(i).getField("PAR_WEB") + "\" target=\"_blank\">");
        retour.append("<img class='imgBlocsPartenaires' src='" + utilisateur.getListeDeTravail().get(i).getField("PAR_IMAGE") + "' title ='"
            + utilisateur.getListeDeTravail().get(i).getField("PAR_NOM") + "' alt='"
            + utilisateur.getListeDeTravail().get(i).getField("PAR_NOM") + "'/>");
        retour.append("</a>");
      }
    }
    else
      retour.append("<div id='messagePrincipal'>Pas de partenaire � afficher</div>");
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
}
