
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionCGVBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class CGVBO
 */
public class CGVBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public CGVBO() {
    super(new GestionCGVBO(), "cgvBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "cgvBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      // * si mes param�tres sont vides j'affiche le formulaire sinon je traite les mises � jours
      if (request.getParameter("conditions") != null) {
        out.println(
            afficherContenu(traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Conditions g�n�rales</span></h2>"),
                null, null, null, request.getSession()));
      }
      else {
        out.println(
            afficherContenu(afficherFormulaire(request.getSession(), "<h2><span id='titrePage'>Conditions g�n�rales</span></h2>", request),
                null, null, null, request.getSession()));
        
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le formulaire de saisir des CGV
   *
   */
  private String afficherFormulaire(HttpSession session, String titre, HttpServletRequest request) {
    // String retour = titre;
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof Utilisateur) {
      // On affiche les codes langues utilis�s pour ce serveur
      ((Utilisateur) session.getAttribute("utilisateur"))
          .setListeDeTravail(maGestion.retournerLanguesWS((Utilisateur) session.getAttribute("utilisateur")));
      if (((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail() != null
          && ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().size() > 0) {
        String langue = "fr";
        if (request.getParameter("codeLangue") != null)
          langue = request.getParameter("codeLangue");
        
        retour.append("<form action='CGVBO' method='post' name='nvChoixLangue' id='formChoixLangue'>");
        retour.append("<select name='codeLangue' onChange='document.nvChoixLangue.submit();'>"); // onChange='document.choixLangue.submit();
        String selectedOpt = "";
        for (int i = 0; i < ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().size(); i++) {
          if (((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString().equals(langue))
            selectedOpt = "selected='selected'";
          else
            selectedOpt = "";
          retour.append("<option value='" + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID")
              + "' " + selectedOpt + ">" + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB")
              + "</option>");
        }
        retour.append("</select>");
        retour.append("</form>");
        
        ((Utilisateur) session.getAttribute("utilisateur"))
            .recupererUnRecordTravail(((GestionCGVBO) maGestion).recupererCgv((Utilisateur) session.getAttribute("utilisateur"), langue));
        retour.append("<form action='CGVBO' method='post'>");
        String contenu = null;
        if (((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail() != null
            && ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail().isPresentField("CGV_NAME"))
          contenu = ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail().getField("CGV_NAME").toString().trim();
        
        retour.append("<label class='textLabel'>Modifier vos conditions G�n�rales " + langue + "  </label>");
        retour.append("<textarea name='conditions' class='zone' rows='30' > " + contenu + "</textarea>");
        retour.append("<input type='hidden' name='langueInfo' value='" + langue + "' />");
        retour.append(
            "<input class='btnMAJ' type='submit' value='Mettre � jour' id='conditions' href='javascript:void(0)' onClick=\"traitementEnCours('CGVBO');\"/>");
        retour.append("</form>");
      }
      retour.append("</div>");
    }
    return retour.toString();
  }
  
  /**
   * Traitement de la mise � jour des donn�es
   * 
   */
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    String chaine = "";
    if (titre != null)
      chaine += titre;
    
    chaine += "<div class='blocContenu'>";
    
    if (((GestionCGVBO) maGestion).miseAjourCgv((Utilisateur) session.getAttribute("utilisateur"), request) > 0)
      chaine += ("<div id='messagePrincipal'>CG de vente mises � jour avec succ�s.</div>");
    else
      chaine += ("<div id='messagePrincipal'>Erreur dans la mise � jour des Conditions g�n�rales</div>");
    
    chaine += (afficherFormulaire(session, "", request));
    
    chaine += "</div>";
    
    return chaine;
    
  }
  
}
