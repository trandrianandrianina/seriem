
package ri.seriem.webshop.vues.backoffice;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.net.ntp.TimeStamp;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionMarketingBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class MarketingBO
 */
public class MarketingBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  // private String page;
  
  public MarketingBO() {
    super(new GestionMarketingBO(), "marketingBO",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "marketingBO.css' rel='stylesheet'/>"
            + "<link rel='stylesheet' href='jquery/jquery-ui.css'>" + "<script src='jquery/external/jquery/jquery.js'></script>"
            + "<script src='jquery/jquery-ui.js'></script>" + "<script src='jquery/datepicker-fr.js'></script>"
            + "<script src='scripts/marketingBO.js'></script>" + "<META HTTP-EQUIV='Pragma' CONTENT='no-cache'>"
            + "<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>" + "<META HTTP-EQUIV='Expires' CONTENT='-1'>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("MarketingBO", "MarketingBO", "les promotions", "All promotions"));
    listeFilRouge.add(new FilRouge("unePromo", "MarketingBO?prId=" + ConstantesEnvironnement.PARAM_FILROUGE, "Une promotion", "Promotion"));
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      // R�ceptionner un formulaire d'images
      if (ServletFileUpload.isMultipartContent(request)) {
        out.println(afficherContenu(afficherResultatPhoto(request.getSession(), request, "<h2><span id='titrePage'>Promotions</span></h2>"),
            "unePromo", null, null, request.getSession()));
        
      }
      else {
        // Cr�ation ou modification de promotion
        if (request.getParameter("idPromo") != null || request.getParameter("ajoutPromo") != null) {
          out.println(afficherContenu(afficherUnePromo("<h2><span id='titrePage'>Promotions</span></h2>", request, 0), "unePromo",
              request.getParameter("idPromo"), null, request.getSession()));
        }
        // On veut traiter un formulaire de cr�ation ou de modification
        else if (request.getParameter("idAmodifier") != null || request.getParameter("insertPromo") != null) {
          out.println(afficherContenu(afficherResultatMajPromo(request, "<h2><span id='titrePage'>Promotions</span></h2>"), "unePromo",
              null, null, request.getSession()));
        }
        // Liste de promotions
        else {
          out.println(afficherContenu(afficherListePromotions(request, "<h2><span id='titrePage'>Promotions</span></h2>"), null,
              request.getParameter("prId"), null, request.getSession()));
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /********************************************************** PROMOTIONS **********************************************************/
  
  /** afficher liste des promotions */
  private String afficherListePromotions(HttpServletRequest request, String titre) {
    if (request == null || request.getSession() == null || request.getSession().getAttribute("utilisateur") == null) {
      return "";
    }
    Utilisateur utilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    String filtreStt = null;
    String parametresPagination = null;
    if (request.getParameter("filtreStt") != null) {
      filtreStt = request.getParameter("filtreStt");
      parametresPagination = "filtreStt=" + filtreStt;
    }
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (request.getParameter("indiceDebut") != null) {
      GestionMarketingBO.majListePartiellePagination(utilisateur, request.getParameter("indiceDebut"));
    }
    else {
      GestionMarketingBO.initDerniereListePagination(utilisateur, GestionMarketingBO.recupererListePromotions(utilisateur, filtreStt));
    }
    
    retour.append("<a id='ajouterPromotion' href='MarketingBO?ajoutPromo=1' >Ajouter une promotion</a>");
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Les promotions</h3>");
    if (utilisateur.getDerniereListePartiellePagination() != null) {
      String selected = "selected=\"selected\"";
      retour.append("<div class='accordeon2'>Filtres</div>");
      retour.append("<div class='contenuAccordeon2' >");
      retour.append("<select name='filtreStatut' size='1' id='selectStt' onChange='location = this.options[this.selectedIndex].value;'>");
      if (filtreStt == null) {
        selected = "selected=\"selected\"";
      }
      else {
        selected = " ";
      }
      retour.append("<option value='marketingBO' " + selected + ">Tous les statuts</option>");
      if (filtreStt != null && filtreStt.equals("0")) {
        selected = "selected=\"selected\"";
      }
      else {
        selected = " ";
      }
      retour.append("<option value='marketingBO?filtreStt=0' " + selected + ">En attente</option>");
      if (filtreStt != null && filtreStt.equals("1")) {
        selected = "selected=\"selected\"";
      }
      else {
        selected = " ";
      }
      retour.append("<option value='marketingBO?filtreStt=1' " + selected + ">En cours</option>");
      if (filtreStt != null && filtreStt.equals("2")) {
        selected = "selected=\"selected\"";
      }
      else {
        selected = " ";
      }
      retour.append("<option value='marketingBO?filtreStt=2' " + selected + ">Termin�</option>");
      retour.append("</select>");
      retour.append("</div>");
      if (utilisateur.getDerniereListePartiellePagination().size() > 0) {
        // Ent�te de colonne
        retour.append("" + "<div class='listes' id='enTeteListes'>" + "<span class='codeArticle'>Code article</span>"
            + "<span class='libelle'>Libell� article</span>" + "<span class='texte'>Promotion</span>" + "<span class='prix'>Prix</span>"
            + "<span class='dateDeb'>Date d�but</span>" + "<span class='dateFin'>Date fin</span>" + "<span class='statut'>Statut</span>"
            + "</div>");
        
        for (GenericRecord promo : utilisateur.getDerniereListePartiellePagination()) {
          String lienpatchs = "MarketingBO?idPromo=" + promo.getField("PR_ID");
          retour.append("<div class='listes'>");
          retour.append("<a class='codeArticle' href='" + lienpatchs + "'>");
          if (promo.isPresentField("PR_A1ART")) {
            retour.append(promo.getField("PR_A1ART").toString().trim());
          }
          else
            retour.append("");
          retour.append("</a>");
          retour.append("<a class='libelle' href='" + lienpatchs + "'>");
          if (promo.isPresentField("PR_A1LIB")) {
            retour.append(promo.getField("PR_A1LIB").toString().trim());
          }
          else
            retour.append("");
          retour.append("</a>");
          retour.append("<a class='texte' href='" + lienpatchs + "'>");
          if (promo.isPresentField("PR_TEXTE")) {
            retour.append(promo.getField("PR_TEXTE").toString().trim());
          }
          else
            retour.append("");
          retour.append("</a>");
          retour.append("<a class='prix' href='" + lienpatchs + "'>");
          if (promo.isPresentField("PR_PRIX")) {
            String valeur;
            if (((BigDecimal) promo.getField("PR_PRIX")).compareTo(BigDecimal.ZERO) == 0) {
              valeur = "";
            }
            else {
              valeur = Outils.formaterUnTarifArrondi(utilisateur, promo.getField("PR_PRIX"));
            }
            retour.append(valeur);
          }
          else {
            retour.append("");
          }
          retour.append("</a>");
          retour.append("<a class='dateDeb' href='" + lienpatchs + "'>");
          if (promo.isPresentField("PR_DATED")) {
            retour.append(Outils.transformerDateSeriemEnHumaine(promo.getField("PR_DATED").toString().trim()));
          }
          else
            retour.append("aucune");
          retour.append("</a>");
          retour.append("<a class='dateFin' href='" + lienpatchs + "'>");
          if (promo.isPresentField("PR_DATEF")) {
            retour.append(Outils.transformerDateSeriemEnHumaine(promo.getField("PR_DATEF").toString().trim()));
          }
          else
            retour.append("aucune");
          retour.append("</a>");
          retour.append("<a class='statut' href='" + lienpatchs + "'>");
          retour.append(afficherStatutPromo(GestionMarketingBO.retournerStatutPromo(utilisateur,
              promo.getField("PR_DATED").toString().trim(), promo.getField("PR_DATEF").toString().trim())));
          retour.append("</a>");
          retour.append("</div>");
        }
        // Pagination
        retour.append(afficherPagination(utilisateur, request.getParameter("indiceDebut"), parametresPagination));
      }
      else {
        retour.append("<div id='messagePrincipal'>Pas de promotion pour ces crit�res</div>");
      }
    }
    else {
      retour.append("<div id='messagePrincipal'>Erreur de r�cup�ration des promotions</div>");
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * On affiche le libell� d'un statut de promotion en fonction des dates de la promotion
   * 0 date de d�but non atteinte "en attente"
   * 1 date de d�but atteinte et date de fin non d�pass�e "en cours"
   * 2 date de fin d�pass�e "termin�e"
   */
  private String afficherStatutPromo(int statut) {
    String retour = "";
    
    switch (statut) {
      case -1:
        retour = "Erreur";
        break;
      case 0:
        retour = "En attente";
        break;
      case 1:
        retour = "En cours";
        break;
      case 2:
        retour = "Termin�e";
        break;
      default:
        retour = String.valueOf(statut);
        break;
    }
    
    return retour;
  }
  
  /**
   * Afficher le formulaire de creation et ou de modification d'une promotion
   **/
  private String afficherUnePromo(String titre, HttpServletRequest request, int pIdPromo) {
    if (request.getSession() == null || request.getSession().getAttribute("utilisateur") == null) {
      return "";
    }
    
    Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
    String idPro = null;
    String etbArticle = null;
    String codeArticle = null;
    String libelleArt = null;
    String libPromotion = null;
    String dateDebut = null;
    String dateFin = "";
    String prixArticle = null;
    
    // id transmis � partir d'une liste il faut r�cup�rer les donn�es d'une promo
    if (request.getParameter("idPromo") != null || pIdPromo > 0) {
      if (pIdPromo > 0) {
        idPro = String.valueOf(pIdPromo);
      }
      else {
        idPro = request.getParameter("idPromo");
      }
      utilisateur.recupererUnRecordTravail(GestionMarketingBO.recupererUnepromotion(utilisateur, idPro));
      if (utilisateur.getRecordTravail() != null) {
        if (utilisateur.getRecordTravail().isPresentField("PR_A1ETB")) {
          etbArticle = utilisateur.getRecordTravail().getField("PR_A1ETB").toString().trim();
        }
        if (utilisateur.getRecordTravail().isPresentField("PR_A1ART")) {
          codeArticle = (utilisateur.getRecordTravail().getField("PR_A1ART").toString().trim());
        }
        if (utilisateur.getRecordTravail().isPresentField("PR_A1LIB")) {
          libelleArt = (utilisateur.getRecordTravail().getField("PR_A1LIB").toString().trim());
        }
        if (utilisateur.getRecordTravail().isPresentField("PR_TEXTE")) {
          libPromotion = utilisateur.getRecordTravail().getField("PR_TEXTE").toString().trim();
        }
        if (utilisateur.getRecordTravail().isPresentField("PR_DATED")) {
          dateDebut = Outils.transformerDateSeriemEnHumaine(utilisateur.getRecordTravail().getField("PR_DATED").toString().trim());
        }
        if (utilisateur.getRecordTravail().isPresentField("PR_DATEF")) {
          dateFin = Outils.transformerDateSeriemEnHumaine(utilisateur.getRecordTravail().getField("PR_DATEF").toString().trim());
        }
        if (utilisateur.getRecordTravail().isPresentField("PR_PRIX")) {
          if (((BigDecimal) utilisateur.getRecordTravail().getField("PR_PRIX")).compareTo(BigDecimal.ZERO) == 0) {
            prixArticle = "";
          }
          else {
            prixArticle = utilisateur.getRecordTravail().getField("PR_PRIX").toString().trim();
          }
        }
      }
    }
    // Cr�ation
    else if (request.getParameter("ajoutPromo") != null) {
      if (utilisateur.getETB_EN_COURS() != null) {
        etbArticle = utilisateur.getETB_EN_COURS().getCodeETB();
      }
      else {
        etbArticle = "";
      }
      codeArticle = "";
      libelleArt = "";
      libPromotion = "";
      dateDebut = "";
      dateFin = "";
      prixArticle = "";
    }
    // En r�cup du pr�c�dent envoi de formulaire
    else if (request.getParameter("idAmodifier") != null || request.getParameter("insertPromo") != null) {
      // uniquement en cas de modif
      if (request.getParameter("idAmodifier") != null) {
        idPro = request.getParameter("idAmodifier");
      }
      etbArticle = request.getParameter("etbArticle");
      codeArticle = (request.getParameter("codeArticle"));
      libelleArt = (request.getParameter("libelleArt"));
      libPromotion = request.getParameter("libPromotion");
      dateDebut = request.getParameter("dateDebut");
      dateFin = request.getParameter("dateFin");
      prixArticle = request.getParameter("prixArticle");
      
    }
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    retour.append("<form action='MarketingBO' method='POST' name ='promoBOm'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span> Une promotion</h3>");
    retour.append("<label class='textLabel'>Etablissement&nbsp;:  </label>");
    retour.append(
        "<input type='text' class='zoneSaisie' name = 'etbArticle' id='etbArticle' maxlength='3' required readonly='readonly' value='"
            + etbArticle + "'><br/>");
    retour.append("<label class='textLabel'>Code article&nbsp;:  </label>");
    retour.append("<input type='text' class='zoneSaisie' name='codeArticle' id='codeArticle'  maxlength='30' value=\""
        + traiterCaracteresSpeciauxAffichage(codeArticle) + "\" ><br/>");
    retour.append("<label  class='textLabel'>Libell� article (*30)&nbsp;:  </label>");
    retour.append("<input type='text' class='zoneSaisie'  maxlength='30' name='libelleArt' id='libelleArt' value=\""
        + traiterCaracteresSpeciauxAffichage(libelleArt) + "\" ><br/>");
    retour.append("<label  class='textLabel'>Libell� promotion (*60)&nbsp;:  </label>");
    retour.append("<input type='text' name='libPromotion' id='libPromotion' class='zoneSaisie' required maxlength='60' value = \""
        + traiterCaracteresSpeciauxAffichage(libPromotion) + "\" ><br/>");
    retour.append("<label class='textLabel'>Date de d�but&nbsp;:  </label>");
    retour.append("<input type='text' class='zoneSaisie' name='dateDebut' id='idDateDebut' readonly='readonly' required value ='"
        + dateDebut + "' onClick=\"openCalendarDebut()\" ><br/>");
    retour.append("<label class='textLabel'>Date de fin&nbsp;:  </label>");
    retour.append("<input type='text' class='zoneSaisie' name='dateFin' id='idDateFin' readonly='readonly' required value ='" + dateFin
        + "' onClick=\"openCalendarDebut()\" ><br/>");
    retour.append("<label  class='textLabel'>Tarif  article&nbsp;:  </label>");
    retour.append(
        "<input type='text' class='zoneSaisie'  maxlength='10' name='prixArticle' id='prixArticle' value='" + prixArticle + "'><br/>");
    
    if (idPro == null) {
      retour.append("<input type='hidden'  name='insertPromo' value='1'>");
      retour.append(
          "<div><input class='btnMAJ' type='submit' value='Cr�er la promotion' id='insertPromo' href='javascript:void(0)' onClick=\"verifCreationPromotion();\"/></div>");
    }
    else {
      retour.append("<input type='hidden'  id='idPro' name='idAmodifier' value='" + idPro + "'>");
      retour.append(
          "<div><input class='btnMAJ' type='submit' value='Modifier la promotion' id='insertPromo' href='javascript:void(0)' onClick=\"verifCreationPromotion();\"/></div>");
      
    }
    retour.append("</form>");
    if (idPro != null) {
      retour.append(formulaireUpload("<h2><span id='titrePage'>Promotion </span></h2>", request, idPro));
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /** traite les promotions **/
  private String afficherResultatMajPromo(HttpServletRequest request, String titre) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    // On contr�le les donn�es
    int[] erreurs = GestionMarketingBO.gestionDesErreurs(request);
    int res = 0;
    // Si les contr�les sont bons on met � jour la bdd
    if (erreurs == null) {
      res = GestionMarketingBO.miseaJourUnePromotion((Utilisateur) request.getSession().getAttribute("utilisateur"), request);
      if (res > 0) {
        retour.append("<div id='messagePrincipal'>Promotion " + res + " mise � jour avec succ�s</div>");
      }
      else {
        retour.append("<div id='messagePrincipal'>Erreur pendant le traitement de votre promotion.</div>");
      }
    }
    else {
      retour.append("<div id='messagePrincipal'>");
      for (int i = 0; i < erreurs.length; i++) {
        switch (erreurs[i]) {
          case -1:
            retour.append("Le code article saisi n'est pas valide<br/>");
            break;
          case -2:
            retour.append("Le prix saisi est invalide<br/>");
            break;
          case -3:
            retour.append("Le libell� de la promotion n'est pas valide<br/>");
            break;
          case -4:
            retour.append("Les dates saisies ne sont pas valides<br/>");
            break;
          case -5:
            retour.append("L'�tablissement saisi n'est pas valide<br/>");
            break;
          
          default:
            break;
        }
      }
      retour.append("</div>");
    }
    retour.append("</div>");
    
    retour.append(afficherUnePromo("", request, res));
    
    return retour.toString();
  }
  
  /**
   * Formulaire d'upload de l'image li�e � la promotion
   **/
  private String formulaireUpload(String titre, HttpServletRequest request, String idPromo) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String filename = "";
    if (request.getParameter("fileUploadPromo") != null)
      filename = request.getParameter("fileUploadPromo");
    
    retour.append("<div class='blocContenu' id='blocUpLoad'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Photo associ�e � la promotion</h3>");
    retour.append("<form action='MarketingBO' method='post' enctype='multipart/form-data'>");
    retour.append("<input type='hidden' name='idPromo' value='" + idPromo + "'/>");
    retour.append("<label class='textLabel'>S�lectionner une photo :</label>");
    retour.append("<input type='file' name='fileUploadPromo' class='fileUpload' value='" + filename + "'/><br/>");
    retour.append("<input type='submit' value='Modifier' class='btnMAJ'/>");
    retour.append("</form>");
    retour.append("<div id='precoImage'>"
        + "Votre photo doit �tre au format <b>.jpg</b> ou <b>.png</b>. Elle sera automatiquement interpr�t�e en image .jpg lors de son import dans le WebShop (pas de gestion de transparence).<br/><br/>"
        + "La hauteur de votre image ne doit pas d�passer 400px et la largeur ne doit pas d�passer 400px.<br/>"
        + "Une image aux ratios identiques aura un meilleur rendu en page de promotions :<br/>"
        + "<b>La pr�conisation pour les dimensions d'une image de promotion est 300px / 300px</b>." + "</div>");
    retour.append("<div id='imagePromotion'>" + "<img id='imagePromo' " + " src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/promotions/promotion_" + idPromo + ".jpg?t=" + TimeStamp.getCurrentTime() + "'/>" + "</div>");
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * traitement de l'upload d'image
   *
   */
  private String afficherResultatPhoto(HttpSession session, HttpServletRequest request, String titre) {
    String chaine = "";
    int idRetour = -1;
    
    idRetour = GestionMarketingBO.uploadPromotion((Utilisateur) session.getAttribute("utilisateur"), request);
    
    if (idRetour == -1) {
      chaine += "<div class='blocContenu'>";
      chaine += ("<div id='messagePrincipal'>Upload du fichier en erreur</div>");
      chaine += "</div>";
      
    }
    else {
      chaine += "<div class='blocContenu'>";
      chaine += ("<div id='messagePrincipal'>Upload du fichier effectu� avec succ�s</div>");
      chaine += "</div>";
      chaine += afficherUnePromo(titre, request, idRetour);
    }
    
    return chaine;
  }
  
  /**
   * Echapper les caract�res sp�ciaux
   */
  private String traiterCaracteresSpeciauxAffichage(String chaine) {
    if (chaine == null)
      return null;
    
    return StringEscapeUtils.escapeHtml4(chaine);
  }
  
}
