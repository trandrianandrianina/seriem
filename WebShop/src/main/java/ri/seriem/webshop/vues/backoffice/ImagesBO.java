
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionImagesBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class ImagesBO
 */
public class ImagesBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public ImagesBO() {
    super(new GestionImagesBO(), "imagesBO", "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "imagesBO.css' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (!ServletFileUpload.isMultipartContent(request)) {
        if (request.getParameter("maj") != null)
          out.println(afficherContenu(traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Accueil </span></h2>"),
              null, null, null, request.getSession()));
        
        else
          out.println(afficherContenu(
              afficherInfoAccueil(request.getSession(), "<h2><span id='titrePage'>Modifier l'accueil</span></h2>", "accBO", request), null,
              request.getParameter("accBO"), null, request.getSession()));
        
      }
      else {
        out.println(afficherContenu(traiterUpload(request.getSession(), request, "<h2><span id='titrePage'>Accueil BO</span></h2>",
            request.getParameter("fileUpload")), null, null, null, request.getSession()));
      }
      
    }
    
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * afficher le formulaire du texte de l'accueil
   */
  private String afficherInfoAccueil(HttpSession session, String titre, String langue, HttpServletRequest request) {
    // String retour = titre;
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof Utilisateur) {
      // Codes langues du WS sur ce serveur
      ((Utilisateur) session.getAttribute("utilisateur"))
          .setListeDeTravail(maGestion.retournerLanguesWS((Utilisateur) session.getAttribute("utilisateur")));
      
      /*affiche le titre de la page*/
      retour.append("<h3><span class='puceH3'>&nbsp;</span> Accueil </h3>");
      
      if (((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail() != null
          && ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().size() > 0) {
        retour.append("<form action='ImagesBO' method='post' name ='formTextesAccueil'>");
        String texteAmodifier = "";
        // GenericRecord texteLangue = null;
        // On scanne chaque langue du serveur
        for (int i = 0; i < ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().size(); i++) {
          // On r�cup�re le texte existant en BDD si c'est la cas et on remplit la textarea avec son contenu
          ((Utilisateur) session.getAttribute("utilisateur")).recupererUnRecordTravail(
              ((GestionImagesBO) maGestion).recupererTexteAccueil((Utilisateur) session.getAttribute("utilisateur"),
                  ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()));
          if (((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail() != null
              && ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail().isPresentField("ACC_NAME"))
            texteAmodifier = ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail().getField("ACC_NAME").toString().trim();
          else
            texteAmodifier = "";
          retour.append("<label for = 't1' class='textLabel'>Modifier texte D'accueil :  "
              + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB").toString().trim()
              + "</label>");
          retour.append("<textarea name='infoAccueil_"
              + ((Utilisateur) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
              + "' class='zone' rows='10' >" + texteAmodifier + "</textarea>");
        }
        retour.append("<input type='hidden'  id='maj' name='maj' value='1'>");
        retour.append(
            "<input class='btnMAJ' type='submit' value='Mettre � jour' id='majp' href='javascript:void(0)' onClick=\"traitementEnCours('ImagesBO');\"/>");
        retour.append("</form>");
        retour.append(formulaireUpload("<h2><span id='titrePage'>Modification de l'accueil</span></h2>", request));
      }
      else
        retour.append("<div>Codes langues manquants (table LANGUES)</div>");
      
      retour.append("</div>");
    }
    return retour.toString();
  }
  
  /** traitement de le mise � jour du formulaire accueilBO **/
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    String chaine = "";
    if (titre != null)
      chaine += titre;
    
    chaine += "<div class='blocContenu'>";
    if (((GestionImagesBO) maGestion).miseAjourAccueil((Utilisateur) session.getAttribute("utilisateur"), request) > 0)
      chaine += ("<div id='messagePrincipal'>Le texte de l'accueil mis � jour correctement. </div>");
    else
      chaine += ("<div id='messagePrincipal'>Erreur lors de la mise � jour de vos donn�es.</div>");
    
    chaine += afficherInfoAccueil(request.getSession(), "", "accBO", request);
    chaine += "</div>";
    
    return chaine;
  }
  
  /** Afficher le formulaire d'upload */
  private String formulaireUpload(String titre, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String filename = "";
    if (request.getParameter("fileUpload") != null)
      filename = request.getParameter("fileUpload");
    
    /*formulaire d'upload*/
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Images � Uploader</h3>");
    // envoie des donn�es binaires
    retour.append("<form action='ImagesBO' method='post' enctype='multipart/form-data'>"); // enctype='multipart/form-data'>");
    retour.append("<label for = 't1' class='textLabelImagel'> Modifier une image</label>");
    retour.append("<label for = 't1' class='textLabel'>Image de l'accueil � uploader :  </label>");
    retour.append("<input type='file' name='fileUpload' class='fileUpload' value='" + filename + "'/>");
    retour.append("<input type='submit' value='Uploader' class='btnMAJ'/>");
    
    retour.append("</form>");
    retour.append("</div>");
    
    return retour.toString();
    
  }
  
  /** traiter l'upload des images de l'accueil */
  private String traiterUpload(HttpSession session, HttpServletRequest request, String titre, String name) {
    String chaine = "";
    
    chaine += "<div class='blocContenu'>";
    
    if (((GestionImagesBO) maGestion).uploadI((Utilisateur) session.getAttribute("utilisateur"), request, name) == -1)
      
      chaine += ("<div id='messagePrincipal'>Upload du fichier en erreur</div>");
    else
      chaine += ("<div id='messagePrincipal'>Upload du fichier effectu� avec succ�s</div>");
    
    chaine += "</div>";
    
    return chaine;
    
  }
}
