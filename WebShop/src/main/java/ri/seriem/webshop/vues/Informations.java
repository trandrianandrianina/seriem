
package ri.seriem.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionInformations;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Servlet Informations
 */
public class Informations extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public Informations() {
    super(new GestionInformations(), "informations",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "informations.css?" + ConstantesEnvironnement.versionWAR + "' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("informations", "informations", "Informations", "About us"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(
          afficherInformations((Utilisateur) request.getSession().getAttribute("utilisateur"),
              "<h2><span id='titrePage'>"
                  + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("Infos") + "</span></h2>"),
          "informations", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affiche de mani�re informelle les informations issues de la table d'informations de XWEBSHOP.INFOSW pour
   * information
   */
  private String afficherInformations(Utilisateur utilisateur, String titre) {
    if (utilisateur == null)
      return "";
    String retour = titre;
    
    retour += "<div class='blocContenu'>";
    
    utilisateur.setListeDeTravail(((GestionInformations) maGestion).retournerInformations(utilisateur));
    
    if (utilisateur.getListeDeTravail() != null) {
      for (int i = 0; i < utilisateur.getListeDeTravail().size(); i++) {
        if (utilisateur.getListeDeTravail().get(i).isPresentField("INF_TXT"))
          retour += "<h3><span class='puceH3'>&nbsp;</span>" + utilisateur.getListeDeTravail().get(i).getField("INF_TXT").toString().trim()
              + "</h3>";
        retour += "<div class='imagesInfos' id='INF_IMG'><img class='imgInfo' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/information1.jpg'/></div>";
        if (utilisateur.getListeDeTravail().get(i).isPresentField("INF_DES"))
          retour += "<div class='texteInfos' id='INF_DES'>" + utilisateur.getListeDeTravail().get(i).getField("INF_DES").toString().trim()
              + "</div>";
        if (utilisateur.getListeDeTravail().get(i).isPresentField("INF_TXT2"))
          retour += "<h3><span class='puceH3'>&nbsp;</span>" + utilisateur.getListeDeTravail().get(i).getField("INF_TXT2").toString().trim()
              + "</h3>";
        retour += "<div  class='imagesInfos' id='INF_IMG2'><img class='imgInfo' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/information2.jpg'/></div>";
        if (utilisateur.getListeDeTravail().get(i).isPresentField("INF_DES2"))
          retour += "<div  class='texteInfos' id='INF_DES2'>"
              + utilisateur.getListeDeTravail().get(i).getField("INF_DES2").toString().trim() + "</div>";
      }
      retour += "</div>";
    }
    
    return retour;
  }
  
}
