
package ri.seriem.webshop.vues.backoffice;

import java.io.File;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionPatchLogsBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class PatchLogsBO
 */
public class PatchLogsBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public PatchLogsBO() {
    super(new GestionPatchLogsBO(), "patchLogsBO",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "patchLogsBO.css' rel='stylesheet'/>"
            + "<link rel='stylesheet' href='jquery/jquery-ui.css'>" + "<script src='jquery/external/jquery/jquery.js'></script>"
            + "<script src='jquery/jquery-ui.js'></script>" + "<script src='jquery/datepicker-fr.js'></script>" +
            
            "<script src='scripts/patchlogsBO.js'></script>",
        ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("patchLogsBO", "patchLogsBO", "Tous les patchs", "All patchs"));
    listeFilRouge.add(new FilRouge("patchId", "patchLogsBO?patchId=" + ConstantesEnvironnement.PARAM_FILROUGE, "Un patch", "Patch"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("majPatch") != null || request.getParameter("creerPatch") != null) {
        out.println(afficherContenu(
            traiterMiseAjourPatch(((Utilisateur) request.getSession().getAttribute("utilisateur")),
                "<h2><span id='titrePage'>Modification d'un patch</span></h2>", request, request.getParameter("idPatch")),
            null, null, null, request.getSession()));
      }
      else if (request.getParameter("patchId") != null || request.getParameter("Ajout") != null)
        out.println(afficherContenu(
            unPatch(((Utilisateur) request.getSession().getAttribute("utilisateur")),
                "<h2><span id='titrePage'>Ajouter/Modifier un Patch</span></h2>", request, request.getParameter("patchId")),
            "patchId", null, null, request.getSession()));
      else
        out.println(afficherContenu(
            listerPatchs(request, ((Utilisateur) request.getSession().getAttribute("utilisateur")),
                "<h2><span id='titrePage'>Les patchs</span></h2>", request.getParameter("indiceDebut")),
            null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affichage de tous les pachsw cr�es
   * 
   * @param request
   * @param session
   * @param titre
   * @param indiceDebut
   * @return
   */
  private String listerPatchs(HttpServletRequest request, Utilisateur user, String titre, String indiceDebut) {
    if (request == null || user == null || ((Utilisateur) request.getSession().getAttribute("utilisateur")) == null)
      return "";
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    // TRAITEMENT + PAGINATION
    // si on pagine pas
    if (request.getParameter("indiceDebut") == null)
      GestionPatchLogsBO.initDerniereListePagination((Utilisateur) request.getSession().getAttribute("utilisateur"),
          ((GestionPatchLogsBO) maGestion).recuperePatchs(user));
    // si on pagine
    else
      GestionPatchLogsBO.majListePartiellePagination((Utilisateur) request.getSession().getAttribute("utilisateur"),
          request.getParameter("indiceDebut"));
    
    retour.append("<div class='blocContenu'>");
    
    /* affiche le titre de la page */
    retour.append("<h3><span class='puceH3'>&nbsp;</span> Lister les patchs</h3>");
    if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination() != null
        && ((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().size() > 0) {
      // bouton d'ajout de patch
      
      retour.append("<a class='ajouter' href='PatchLogsBO?Ajout=1' >Ajouter un Patch</a>");
      
      // ent�tes de colonnes
      retour.append(
          "<div  class='listes' id='enTeteListes'>" + "<span id='titreDate'>Date </span>" + "<span id='titreClasse'>Nom de la classe</span>"
              + "<span id='titreDetail'>D�tail</span>" + "<span id='titreVersion'>Version</span></div>");
      for (int i = 0; i < ((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination()
          .size(); i++) {
        String lienpatchs = "PatchLogsBO?patchId=" + ((Utilisateur) request.getSession().getAttribute("utilisateur"))
            .getDerniereListePartiellePagination().get(i).getField("PA_ID");
        
        retour.append("<div class='listes'>");
        retour.append("<a class='PA_DATE' href='" + lienpatchs + "'>");// href='"+lienpatchs+"'"
        if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
            .isPresentField("PA_DATE"))
          retour.append(Outils.transformerDateSeriemEnHumaine(((Utilisateur) request.getSession().getAttribute("utilisateur"))
              .getDerniereListePartiellePagination().get(i).getField("PA_DATE").toString().trim()));
        else
          retour.append("aucune");
        retour.append("</a>");
        
        retour.append("<a class='PA_CLAS' href='" + lienpatchs + "'>");//
        if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
            .isPresentField("PA_CLAS"))
          retour.append(((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
              .getField("PA_CLAS").toString().trim());
        else
          retour.append("aucune");
        retour.append("</a>");
        
        retour.append("<a class='PA_DETA'  href='" + lienpatchs + "'>");//
        if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
            .isPresentField("PA_DETA"))
          retour.append(((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
              .getField("PA_DETA").toString().trim());
        else
          retour.append("aucune");
        retour.append("</a>");
        
        retour.append("<a class='PA_VERSION' href='" + lienpatchs + "'>");//
        if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
            .isPresentField("PA_ETAT"))
          retour.append(((Utilisateur) request.getSession().getAttribute("utilisateur")).getDerniereListePartiellePagination().get(i)
              .getField("PA_ETAT").toString().trim());
        else
          retour.append("aucune");
        retour.append("</a>");
        
        retour.append("</div>");
      }
    }
    else
      retour.append("<p>pas de patch</p>");
    // Pagination
    retour.append(afficherPagination(user, request.getParameter("indiceDebut"), null));
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Affiche un patch en creation, modifi
   * @param user
   * @param titre
   * @param request
   * @param idPatch
   * @return
   */
  private String unPatch(Utilisateur user, String titre, HttpServletRequest request, String idPatch) {
    if (user == null)
      return "";
    
    int patchId = 0;
    if (idPatch != null) {
      try {
        patchId = Integer.parseInt(idPatch);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    else
      patchId = 0;
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    int date = Outils.recupererDateCouranteInt();
    String datePatch = "";
    
    user.recupererUnRecordTravail(((GestionPatchLogsBO) maGestion).recupereUnPatch(user, patchId));
    
    String selectedOpt = "";
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Un patch &nbsp;&nbsp;</h3>");
    retour.append("<form action='PatchLogsBO' method='post' name = 'ajout'>");
    boolean isEnModification = false;
    if (user.getRecordTravail() != null && user.getRecordTravail().isPresentField("PA_ID"))
      isEnModification = true;
    else
      isEnModification = false;
    
    datePatch = Outils.transformerDateSeriemEnHumaine(date);
    String nomClasse = retournerTexteModifOuCrea(isEnModification, user.getRecordTravail(), "PA_CLAS");
    
    // NOM DU PATCH
    retour.append("<label for='t1' class='textLabel'>Nom de la classe :</label>");
    if (request.getParameter("classeJava") != null)
      retour.append(formulaireClasseJava(request, user, "", request.getParameter("classeJava")));
    else
      retour.append(formulaireClasseJava(request, user, "", nomClasse));
    
    // VERSION DU PATCH
    retour.append("<label for='t1' class='textLabel'>Num�ro de la version :</label>");
    if (request.getParameter("etat") != null)
      retour.append("<textarea name='etat' class='zoneVersion' readonly>" + request.getParameter("etat") + "</textarea>");
    else
      retour.append("<textarea name='etat' class='zoneVersion' readonly>"
          + retournerTexteModifOuCrea(isEnModification, user.getRecordTravail(), "PA_ETAT") + "</textarea>");
    
    // DATE DE CREATION
    retour.append("<label for = 't1' class='textLabel'>Date de cr�ation du patch :  </label>");
    if (request.getParameter("date") != null)
      retour.append("<input type='text' name='date' class='datePatch' id='datePatch' value='" + request.getParameter("date") + "'>");
    else
      retour.append("<input type='text' name='date' class='datePatch' id='datePatch' value='" + datePatch + "'>");
    if (patchId == 0) {
      retour.append("<label for = 't1' class='textLabel'>Application :  </label>");
      // retour.append("</select>");
      retour.append("<select name='application' class='visibilite' id ='appli' >");
      retour.append("<option value='1' >Globale</option>");
      retour.append("<option value='2'>Specifique</option>");
      retour.append("</select>");
      
      retour.append("<label for = 't1' class='textLabel'>Visibilit� du patch :  </label>");
      retour.append("<select name='visibilite' class='visibilite'>");
      retour.append("<option value='0'>Invisible</option>");
      retour.append("<option value='1'>Interne</option>");
      retour.append("<option value='2'>Public</option>");
      retour.append("</select>");
      
      retour.append("<label for = 't1' class='textLabel'>Nom du patcheur:  </label>");
      retour.append("<select name='dev' class='visibilite'>");
      retour.append("<option value='0'>St�phane</option>");
      retour.append("<option value='1'>Herv�</option>");
      retour.append("<option value='2'>David</option>");
      retour.append("<option value='3'>D�borah</option>");
      
      retour.append("</select>");
    }
    else {
      // CHAMP APPLICATION
      retour.append("<label for = 't1' class='textLabel'>Application :  </label>");
      retour.append("<select name='application' class='visibilite' id ='appli' >");
      
      if (user.getRecordTravail().getField("PA_APP").equals("1")) {
        retour.append("<option " + selectedOpt + "value='1'>Globale</option>");
      }
      else if (user.getRecordTravail().getField("PA_APP").equals("2")) {
        retour.append("<option " + selectedOpt + " value='2'>Sp�cifique</option>");
        retour.append("<option value='1'>Globale</option>");
      }
      retour.append("</select>");
      
      // CHAMP VISIBILITE
      retour.append("<label for = 't1' class='textLabel'>Visibilit� du patch :  </label>");
      
      retour.append("<select name='visibilite' class='visibilite' id='visib'>");
      if (user.getRecordTravail().getField("PA_VISI").equals("0")) {
        retour.append("<option " + selectedOpt + " value='0'>Invisible</option>");
        retour.append("<option value='1'>Interne</option>");
        retour.append("<option value='2'>Public</option>");
      }
      else if (user.getRecordTravail().getField("PA_VISI").equals("1")) {
        retour.append("<option " + selectedOpt + "value='1'>Interne</option>");
        retour.append("<option value='0'>Invisible</option>");
        retour.append("<option value='2'>Public</option>");
      }
      else if (user.getRecordTravail().getField("PA_VISI").equals("2")) {
        retour.append("<option" + selectedOpt + " value='2'>Public</option>");
        retour.append("<option value='0'>Invisible</option>");
        retour.append("<option value='1'>Interne</option>");
      }
      retour.append("</select>");
      
      // CHAMP DEVELOPPEUR
      retour.append("<label for = 't1' class='textLabel'>Nom du patcheur:  </label>");
      retour.append("<select name='dev' class='visibilite' >");
      if (user.getRecordTravail().getField("PA_DEV").equals("0"))//
      {
        retour.append("<option " + selectedOpt + "value='0'>St�phane</option>");
        retour.append("<option value='1'>Herv�</option>");
        retour.append("<option value='2'>David</option>");
        retour.append("<option value='3'>D�borah</option>");
      }
      else if (user.getRecordTravail().getField("PA_DEV").equals("1")) {
        retour.append("<option " + selectedOpt + "value='1'>Herv�</option>");
        retour.append("<option value='0'>St�phane</option>");
        retour.append("<option value='2'>David</option>");
        retour.append("<option value='3'>D�borah</option>");
      }
      else if (user.getRecordTravail().getField("PA_DEV").equals("2")) {
        retour.append("<option " + selectedOpt + "value='2'>David</option>");
        retour.append("<option value='0'>St�phane</option>");
        retour.append("<option value='1'>Herv�</option>");
        retour.append("<option value='3'>D�borah</option>");
      }
      else if (user.getRecordTravail().getField("PA_DEV").equals("3")) {
        retour.append("<option " + selectedOpt + "value='3'>D�borah</option>");
        retour.append("<option value='0'>St�phane</option>");
        retour.append("<option value='1'>Herv�</option>");
        retour.append("<option value='2'>David</option>");
      }
      retour.append("</select>");
    }
    // CHAMP DESCRIPTION
    retour.append("<label for='t1' class='textLabel'>Description du Patch :</label>");
    if (request.getParameter("detail") != null)
      retour.append("<textarea name='detail' class='zone' rows='10' >" + request.getParameter("detail") + "</textarea>");
    else
      retour.append("<textarea name='detail' class='zone' rows='10' >"
          + retournerTexteModifOuCrea(isEnModification, user.getRecordTravail(), ("PA_DETA")) + "</textarea>");
    
    if (patchId == 0) {
      retour.append("<input type='hidden'  id='majPatch' name='majPatch' value='1'>");
      retour.append("<input type='submit' class='btnMAJ' value='CREATION' id='majPatch'/>");
    }
    else {
      retour.append("<input type='hidden'  id='idPatch' name='idPatch' value='" + idPatch + "'>");
      retour.append("<input type='hidden'  id='majPatch' name='majPatch' value='1'>");
      retour.append("<input type='submit' class='btnMAJ' value='MAJ' id='majPatch'/>");
    }
    retour.append("</form>");
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * R�cup�ration de toutes les classes JAVA du projet
   * 
   * @param request
   * @param session
   * @param titre
   * @param classeName
   * @return
   */
  private String formulaireClasseJava(HttpServletRequest request, Utilisateur user, String titre, String classeName) {
    StringBuilder builder = new StringBuilder();
    builder.append(titre);
    ArrayList<String> allFiles = new ArrayList<String>();
    File pathS = new File(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT + "webapps" + File.separatorChar + ConstantesEnvironnement.DOSSIER_WS
        + File.separatorChar + "WEB-INF" + File.separatorChar + "classes" + File.separatorChar);
    ConstantesEnvironnement.isEclipse = false;
    
    if (!pathS.exists()) {
      pathS = new File(ConstantesEnvironnement.DOSSIER_DEV);
      ConstantesEnvironnement.isEclipse = true;
    }
    else {
      pathS = new File(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT + "webapps" + File.separatorChar + ConstantesEnvironnement.DOSSIER_WS
          + File.separatorChar + "WEB-INF" + File.separatorChar + "classes" + File.separatorChar);// +File.separator);
      ConstantesEnvironnement.isEclipse = false;
    }
    
    if (user != null && user instanceof Utilisateur) {
      ArrayList<String> liste = ((GestionPatchLogsBO) maGestion).listerClassesJava(pathS, (allFiles), request, user);
      if (liste != null && liste.size() > 0) {
        if (request.getParameter("classeJava") != null)
          
          classeName = request.getParameter("classeJava");
        
        builder.append("<select name='classeJava' size='1' class='visibilite' maxlenght='100' >");
        
        String selectedOpt = "";
        // on parcourt la liste des classes
        
        builder.append("<option>  </option>");
        for (int i = 0; i < liste.size(); i++) {
          if (liste.get(i).equals(classeName))
            selectedOpt = "selected='selected'";
          else
            selectedOpt = "";
          builder.append("<option value='" + liste.get(i) + "' " + selectedOpt + ">" + liste.get(i) + "</option>");// "
                                                                                                                   // +
                                                                                                                   // classeName
                                                                                                                   // +
                                                                                                                   // "
          
        }
      }
      
      builder.append("</select>");
      
    }
    return builder.toString();
  }
  
  /**
   * Mise � jour table patchsw
   * 
   * @param session
   * @param titre
   * 
   * @param request
   * @param patchId
   * @return
   */
  private String traiterMiseAjourPatch(Utilisateur user, String titre, HttpServletRequest request, String idPatch) {
    int patch = 0;
    
    // int indiceD = 0;
    if (idPatch != null) {
      try {
        patch = Integer.parseInt(idPatch);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    StringBuilder builder = new StringBuilder();
    builder.append("<div class='blocContenu'>");
    
    if (((GestionPatchLogsBO) maGestion).miseajourPatch(user, request, patch) == -1)
      builder.append("<div id='messagePrincipal'>Erreur dans la mise � jour du patch</div>");
    
    else
      builder.append("<div id='messagePrincipal'>Patch mis � jour avec succ�s </div>");
    
    builder.append(listerPatchs(request, user, "", request.getParameter("indiceDebut")));
    builder.append("</div>");
    return builder.toString();
  }
  
  // factorisation de l'affichage
  private String retournerTexteModifOuCrea(boolean isModif, GenericRecord record, String zone) {
    
    if (isModif && record.isPresentField(zone)) {
      return record.getField(zone).toString().trim();
    }
    else
      return "";
    
  }
  
}
