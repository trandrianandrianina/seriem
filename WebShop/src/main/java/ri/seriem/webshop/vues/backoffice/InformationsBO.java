
package ri.seriem.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.backoffice.GestionInformationsBO;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.FilRouge;
import ri.seriem.webshop.vues.MouleVues;

/**
 * Servlet implementation class InformationsBO
 */
public class InformationsBO extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  public InformationsBO() {
    super(new GestionInformationsBO(), "informationsBO",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "informationsBO.css' rel='stylesheet'/>", ConstantesEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (!ServletFileUpload.isMultipartContent(request)) {
        if (request.getParameter("texte1") != null)
          out.println(afficherContenu(traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Informations</span></h2>"),
              null, null, null, request.getSession()));
        
        else
          out.println(afficherContenu(
              afficherFormulaireModification(request.getSession(), "<h2><span id='titrePage'>Informations</span></h2>", request), null,
              null, null, request.getSession()));
      }
      else {
        
        out.println(afficherContenu(traiterUpload(request.getSession(), request, "<h2><span id='titrePage'>Information BO</span></h2>",
            request.getParameter("fileUpload")), null, null, null, request.getSession()));
        
      }
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher les formulaires de saisie
   */
  private String afficherFormulaireModification(HttpSession session, String titre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof Utilisateur) {
      // On affiche les codes langues utilis�s pour ce serveur
      ArrayList<GenericRecord> langues = maGestion.retournerLanguesWS((Utilisateur) session.getAttribute("utilisateur"));
      if (langues != null && langues.size() > 0) {
        String langue = "fr";
        if (request.getParameter("codeLangue") != null)
          langue = request.getParameter("codeLangue");
        
        retour.append("<form action='InformationsBO' method='post' name='choixLangue' id='formChoixLangue'>");
        retour.append("<select name='codeLangue' onChange='document.choixLangue.submit();'>"); // onChange='document.choixLangue.submit();
        String selectedOpt = "";
        for (int i = 0; i < langues.size(); i++) {
          if (langues.get(i).getField("LA_ID").toString().equals(langue))
            selectedOpt = "selected='selected'";
          else
            selectedOpt = "";
          retour.append("<option value='" + langues.get(i).getField("LA_ID") + "' " + selectedOpt + ">" + langues.get(i).getField("LA_LIB")
              + "</option>");
        }
        retour.append("</select>");
        retour.append("</form>");
        
        // On choisit l'information du code langue souhait�
        ((Utilisateur) session.getAttribute("utilisateur")).recupererUnRecordTravail(
            ((GestionInformationsBO) maGestion).recupererUneInformation((Utilisateur) session.getAttribute("utilisateur"), langue));
        retour.append("<form action='InformationsBO' method='post'name='formModifUneInfo' >");
        // Modification ou cr�ation pour cette info
        boolean isEnModification = false;
        if (((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail() != null
            && ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail().isPresentField("INF_ID"))
          isEnModification = true;
        
        retour.append("<h3><span class='puceH3'>&nbsp;</span>Texte 1</h3>");
        retour.append("<label class='textLabel'>Titre texte 1 :  </label>");
        retour.append("<input name='texte1' class='zoneTitre' value='"
            + retournerTexteModifOuCrea(isEnModification, ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail(), "INF_TXT")
            + "' />");
        retour.append("<label class='textLabel'>Texte 1 :  </label>");
        retour.append("<textarea name='des1' class='zone'>"
            + retournerTexteModifOuCrea(isEnModification, ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail(), "INF_DES")
            + "</textarea>");
        retour.append("<label class='textLabel'>Image texte 1 :  </label>");
        retour.append("<input name='img1' class='zoneTitre' value='"
            + retournerTexteModifOuCrea(isEnModification, ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail(), "INF_IMG")
            + "'/>");
        
        retour.append("<h3><span class='puceH3'>&nbsp;</span>Texte 2</h3>");
        retour.append("<label class='textLabel'>Titre texte 2 :  </label>");
        retour.append("<input name='texte2' class='zoneTitre' value='" + retournerTexteModifOuCrea(isEnModification,
            ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail(), "INF_TXT2") + "' />");
        retour.append("<label class='textLabel'>Texte 2 :  </label>");
        retour.append("<textarea name='des2' class='zone'>" + retournerTexteModifOuCrea(isEnModification,
            ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail(), "INF_DES2") + "</textarea>");
        retour.append("<label class='textLabel'>Image texte 2:  </label>");
        retour.append("<input name='img2' class='zoneTitre' value='" + retournerTexteModifOuCrea(isEnModification,
            ((Utilisateur) session.getAttribute("utilisateur")).getRecordTravail(), "INF_IMG2") + "' />");
        retour.append("<input type='hidden' name='langueInfo' value='" + langue + "' />");
        
        retour.append(
            "<div><input class='btnMAJ' type='submit' value='Mettre � jour' id='texte1' href='javascript:void(0)' onClick=\"traitementEnCours('informations');\"/></div>");
        retour.append("</form>");
        retour.append(formulaireUpload("<h2><span id='titrePage'>Informations</span></h2>", request));
      }
    }
    
    retour.append("</div>");
    return retour.toString();
  }
  
  private String retournerTexteModifOuCrea(boolean isModif, GenericRecord record, String zone) {
    if (isModif && record.isPresentField(zone))
      return record.getField(zone).toString().trim();
    else
      return "";
  }
  
  /**
   * formulaire d'upload
   */
  private String formulaireUpload(String titre, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String filename = "";
    if (request.getParameter("fileUpload") != null)
      filename = request.getParameter("fileUpload");
    
    /*formulaire d'upload*/
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Images � Uploader</h3>");
    // envoie des donn�es binaires
    retour.append("<form action='InformationsBO' method='post' enctype='multipart/form-data'>"); // enctype='multipart/form-data'>");
    // retour.append("<label for = 't1' class='textLabelImage'> Modifier une image Information</label>");
    retour.append("<label for = 't1' class='textLabel'>Image � uploader :  </label>");
    retour.append("<input type='file' name='fileUpload' class='fileUpload' value='" + filename + "'/>");
    retour.append("<input type='submit' value='Uploader' class='btnMAJ'/>");
    
    retour.append("</form>");
    return retour.toString();
  }
  
  /*	*//**
         * Afficher les informations dans le formulaire *
         */
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    String chaine = "";
    if (titre != null)
      chaine += titre;
    
    // on contr�le si le formulaire est mis � jour ou pas
    chaine += "<div class='blocContenu'>";
    
    if (((GestionInformationsBO) maGestion).miseajourInformation((Utilisateur) session.getAttribute("utilisateur"), request) > 0)
      chaine += ("<div id='messagePrincipal'>Informations mises � jour avec succ�s.</div>");
    else
      chaine += ("<div id='messagePrincipal'>Il y a eu une erreur pendant la mise � jour de vos donn�es.</div>");
    chaine += afficherFormulaireModification(request.getSession(), "", request);
    chaine += "</div>";
    return chaine;
    
  }
  
  /** traitement de l'upload */
  private String traiterUpload(HttpSession session, HttpServletRequest request, String titre, String name) {
    String chaine = "";
    
    chaine += "<div class='blocContenu'>";
    
    if (((GestionInformationsBO) maGestion).upload((Utilisateur) session.getAttribute("utilisateur"), request, name) == -1) {
      chaine += ("<div id='messagePrincipal'>Upload du fichier en erreur</div>");
    }
    else
      chaine += ("<div id='messagePrincipal'>Upload du fichier effectu� avec succ�s.</div>");
    
    chaine += "</div>";
    return chaine;
    
  }
  
}
/*private String selectionnerImages (String titre, HttpServletRequest request,HttpSession session)
{
	String retour = titre;
	String selectedOpt = "selected=\"selected\"";

	retour += "<div class='blocContenu'>";
	ArrayList liste =  ((GestionInformationsBO) maGestion).ListePicture((Utilisateur) session.getAttribute("utilisateur"));
	System.out.println(liste);
	retour += "<h3><span class='puceH3'>&nbsp;</span>liste des images</h3>";
	retour += "<select name='image' size='1' class='nomImage' onChange='location = this.options[this.selectedIndex].value;'><br>";

	
	if(liste != null)
	{
		//retour+= "<option value ='1'>Pas d'image</option>";
		for(int i = 0; i < liste.size(); i++)
		{
		
			retour += "<option "+ selectedOpt + "value='" +liste.get(i).toString()+"'>"+liste.get(i).toString()+"</option>";	
		}	
		retour+= "</select>";
		retour += "</div>";
	}
	//retour+= formulaireUpload("<h2><span id='titrePage'>Information </span></h2>",request, request.getParameter("image"));
	System.out.println("request.getParameter:"+request.getParameter("image") );
	return retour;
	
}*/
