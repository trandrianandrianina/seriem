
package ri.seriem.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionContact;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Etablissement;
import ri.seriem.webshop.outils.EnvoiMail;

/**
 * Servle Contact
 */
public class Contact extends MouleVues {
  private static final long serialVersionUID = 1L;
  
  private EnvoiMail envoiMail = null;
  
  public Contact() {
    super(new GestionContact(), "contact",
        "<link href='" + ConstantesEnvironnement.DOSSIER_CSS + "contact.css?" + ConstantesEnvironnement.versionWAR + "' rel='stylesheet'/>",
        ConstantesEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("contact", "contact", "Contact", "Contact"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      out.println(afficherContenu(afficherContact("<h2><span id='titrePage'>Contact</span></h2>", request.getParameter("mail"),
          request.getParameter("sujet"), request.getParameter("message"), request.getParameter("prenom"), request.getParameter("nom"),
          request.getParameter("societe"), request.getParameter("phone"), request.getParameter("etb_contact"),
          (Utilisateur) request.getSession().getAttribute("utilisateur")), "contact", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affichage du formulaire d'envoi de mail � l'adresse de contact de l'�tablissement
   */
  private String afficherContact(String titre, String adresseMail, String sujetMail, String messMail, String prenomMail, String nomMail,
      String societe, String phoneMail, String etb, Utilisateur utilisateur) {
    if (utilisateur == null)
      return "";
    
    // String retour = "";
    StringBuilder builder = new StringBuilder(2000);
    builder.append(titre);
    
    String prenom = "";
    String nom = "";
    String societeC = "";
    String telephone = "";
    String email = "";
    
    builder.append("<div class='blocContenu'>");
    // On g�re l'envoi par mail de la confirmation
    if (adresseMail != null && !adresseMail.trim().equals("") && messMail != null && !messMail.trim().equals("")) {
      if (envoiMail == null)
        envoiMail = new EnvoiMail(utilisateur);
      String sujet = "[WEBSHOP] " + sujetMail;
      String message = "<table id='envoiContact'>"
          + "<tr><td class='paddingHorizontal'></td><td class='h2Mail'>Page de contact du Web Shop</td><td class='paddingHorizontal'></td></tr>"
          + "<tr><td class='paddingVertical' colspan='3'></td></tr>"
          + "<tr><td class='paddingHorizontal'></td><td class='lignesContenu' id='introContact'><b>" + prenomMail + " " + nomMail
          + " </b>de la societ� <b>" + societe
          + " </b>vous a adress� le message suivant via le WebShop : </td><td class='paddingHorizontal'></td></tr>"
          + "<tr><td class='paddingHorizontal'></td><td class='lignesContenu' id='mailContact'><b>Adresse mail: </b>" + adresseMail
          + "</td><td class='paddingHorizontal'></td></tr>"
          + "<tr><td class='paddingHorizontal'></td><td class='lignesContenu' id='telContact'><b>T�l�phone : </b>" + phoneMail
          + "</td><td class='paddingHorizontal'></td></tr>" + "<tr><td class='paddingVertical' colspan='3'></td></tr>"
          + "<tr><td class='paddingVertical' colspan='3'></td></tr>"
          + "<tr><td class='paddingHorizontal'></td><td class='lignesContenu' id='messageContact'>\"" + messMail
          + "\"</td><td class='paddingHorizontal'></td></tr>" + "<tr><td class='paddingVertical' colspan='3'></td></tr>" + "</table>";
      
      String mailETB_EN_COURS = null;
      if (utilisateur.getETB_EN_COURS() != null)
        mailETB_EN_COURS = utilisateur.getETB_EN_COURS().getMail_contact().trim();
      else if (etb != null) {
        Etablissement etbTemp = new Etablissement(utilisateur, etb);
        if (etbTemp.getMail_contact() != null)
          mailETB_EN_COURS = etbTemp.getMail_contact().trim();
      }
      
      if (mailETB_EN_COURS != null)
        envoiMail.envoyerUnMail(mailETB_EN_COURS, sujet, message);
      
      builder.append("<div id='messagePrincipal'>" + utilisateur.getTraduction().traduire("$$confMailContact") + "</div>");
    }
    
    builder.append("<div id='boiteFormulaire'>");
    builder.append("<h3><span class='puceH3'>&nbsp</span>" + utilisateur.getTraduction().traduire("$$formulaireContact") + "</h3>");
    // Les champs adresse mail et messages doivent �tre obligatoirement saisis
    builder.append("<form name='Contact' action='contact' method='POST' id='formContact'>");
    
    if (utilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT && utilisateur.getClient() != null) {
      if (utilisateur.getNomContact() != null) {
        prenom = utilisateur.getNomContact();
        nom = utilisateur.getNomContact();
      }
      
      if (utilisateur.getClient().getNomClient() != null)
        societeC = utilisateur.getClient().getNomClient();
      
      if (utilisateur.getTelContact() != null)
        telephone = utilisateur.getTelContact();
      
      email = utilisateur.getUS_LOGIN();
    }
    
    if (utilisateur.getETB_EN_COURS() == null) {
      // utilisateur.setListeDeTravail(Etablissement.retournerListeDesEtablissementsValides(utilisateur));
      builder.append("<div class='ligneContact'>");
      builder.append("<label id='etb_label'>" + utilisateur.getTraduction().traduire("$$etablissement") + "</label>"
          + "<select name='etb_contact' id='contact_etb'>");
      if (ConstantesEnvironnement.LISTE_ETBS != null && ConstantesEnvironnement.LISTE_ETBS.size() > 0)
        for (int i = 0; i < ConstantesEnvironnement.LISTE_ETBS.size(); i++)
          builder.append("<option value='" + ConstantesEnvironnement.LISTE_ETBS.get(i).getCodeETB() + "'>"
              + ConstantesEnvironnement.LISTE_ETBS.get(i).getLibelleETB() + "</option>");
      builder.append("</select>");
      builder.append("</div>");
    }
    else
      builder.append("<input name='etb_contact' type='hidden' value='" + utilisateur.getETB_EN_COURS() + "'/>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_prenom_label'>" + utilisateur.getTraduction().traduire("$$prenom") + "</label>"
        + "<input name='prenom' id='contact_prenom' type='text' required maxlength='100' value='" + prenom + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_nom_label'>" + utilisateur.getTraduction().traduire("$$nomFamille") + "</label>"
        + "<input name='nom' id='contact_nom' type='text' required maxlength='100' value='" + nom + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_socie_label'>" + utilisateur.getTraduction().traduire("$$societe") + "</label>"
        + "<input name='societe' id='contact_societe' type='text' required  maxlength='100' value='" + societeC + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_phone_label'>T&eacute;l&eacute;phone</label>"
        + "<input name='phone' id='contact_phone' type='tel' maxlength='100' value='" + telephone + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_mail_label'>" + utilisateur.getTraduction().traduire("$$adresseEmail") + "</label>"
        + "<input name='mail' id='contact_mail' type='email' required='Champs obligatoire' maxlength='240' value='" + email + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_sujet_label'>" + utilisateur.getTraduction().traduire("$$sujet") + "</label>"
        + "<input name='sujet' id='contact_sujet' required type='text' maxlength='240' />");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact' id='divMessage'>");
    builder.append(" <label id='contact_message_label'>Message</label>"
        + "<textarea name='message' id='contact_message' required='Champs obligatoire'></textarea>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(
        "<input name='Envoyer' id='contact_valider' type='submit' value = '" + utilisateur.getTraduction().traduire("$$envoyer") + "' >");
    builder.append("</div>");
    
    builder.append("</form>");
    
    // Afficher un look graphique � la page
    builder.append("<div id='decoContact'>");
    builder.append(
        "<img id='imgDecoContact' src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/decoContact.png' />");
    builder.append("</div>");
    
    builder.append("</div>");
    
    builder.append("</div>");
    
    return builder.toString();
  }
}
