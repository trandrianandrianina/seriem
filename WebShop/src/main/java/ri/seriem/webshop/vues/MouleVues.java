
package ri.seriem.webshop.vues;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.controleurs.GestionMesDonnees;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.GestionRequetes;
import ri.seriem.webshop.environnement.SessionListener;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;

/**
 * Servlet implementation class MouleVues
 */
public abstract class MouleVues extends HttpServlet {
  private static final long serialVersionUID = 1L;
  protected PatternErgonomie pattern = null;
  protected String nomPage = null;
  protected Gestion maGestion = null;
  protected String cssSpecifique = null;
  protected int accesPage;
  protected ArrayList<FilRouge> listeFilRouge = null;
  protected FilRouge filAccueil = null;
  protected MesLogs logs = null;
  protected GestionMesDonnees mesDonnees = null;
  
  public final int MAIL_CLIENT = 1;
  public final int MAIL_GESTIONNAIRE = 2;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public MouleVues(Gestion gestionSpec, String nomP, String css, int accesMini) {
    super();
    pattern = new PatternErgonomie();
    maGestion = gestionSpec;
    nomPage = nomP;
    cssSpecifique = css;
    accesPage = accesMini;
    logs = new MesLogs();
    logs.setClasseQuiLogge(nomPage);
    
    filAccueil = new FilRouge("accueil", "accueil", "Accueil", "Home");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  public MouleVues() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Afficher un contenu au sein m�me de la page sp�cifique
   */
  protected String afficherContenu(String contenu, String page, String parametre, String pop, HttpSession session) {
    if (session == null)
      return "";
    
    String chaine = "<article id='contenu'>";
    
    chaine += retournerBlocRepresentant(session);
    
    chaine += traitementFilRouge(page, parametre, session);
    
    chaine += contenu;
    
    chaine += pattern.afficherPiedPage(pop, session, page); // session);
    
    return chaine;
  }
  
  /**
   * Permet de traiter l'affichage du fil rouge ne fonction de la page pass�e
   */
  public String traitementFilRouge(String page, String parametre, HttpSession session) {
    String retour = "";
    String lienGraph = null;
    if (session == null || session.getAttribute("utilisateur") == null)
      return retour;
    
    if (((Utilisateur) session.getAttribute("utilisateur")).getPanierIntuitif() != null) {
      return retour;
    }
    
    retour = "<div id='filRouge'>";
    
    if (page != null) {
      String partiel = "";
      int i = 0;
      boolean trouve = false;
      while (i < listeFilRouge.size() && !trouve) {
        listeFilRouge.get(i).majValeurLangue(((Utilisateur) session.getAttribute("utilisateur")).getLanguage());
        trouve = listeFilRouge.get(i).getCleFil().equals(page);
        // si on a trouv� le fil on change ses param�tres
        if (trouve)
          listeFilRouge.get(i).setParametreLien(parametre);
        // sinon on liste les �tapes de chaque fil
        else {
          if (i != 0)
            partiel += " <span class='flecheFilRouge'><</span> ";
          // partiel += listeFilRouge.get(i).getValeurFil();
          partiel +=
              "<a href='" + listeFilRouge.get(i).getLienFil() + "' class='filRougeText'>" + listeFilRouge.get(i).getValeurFil() + "</a>";
          lienGraph = listeFilRouge.get(i).getLienFil();
        }
        
        i++;
      }
      if (!trouve || (trouve && i == 1))
        partiel = "";
      
      retour += partiel;
    }
    else {
      filAccueil.majValeurLangue(((Utilisateur) session.getAttribute("utilisateur")).getLanguage());
      retour += "<a href='" + filAccueil.getLienFil() + "' class='filRougeText'>" + filAccueil.getValeurFil() + "</a>";
    }
    
    // On passe au lien graphique en mode Smartphone
    if (page != null && lienGraph != null)
      retour += "<a href='" + lienGraph + "' class='filGraphique'><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/flecheGauche.png' alt='retour' /></a>";
    else
      retour += "<a href='" + filAccueil.getLienFil() + "' class='filGraphique'><img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/flecheGauche.png' alt='retour' /></a>";
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Permet d'afficher le mode repr�sentant sur chaque interface
   */
  private String retournerBlocRepresentant(HttpSession session) {
    String retour = "";
    
    if (session.getAttribute("utilisateur") == null)
      return retour;
    
    if (((Utilisateur) session.getAttribute("utilisateur")).getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS) {
      if (((Utilisateur) session.getAttribute("utilisateur")).getClient() != null) {
        retour += "<div id='surBlocClient'>";
        retour += "<div id='blocrepresentant'><span class='superflusClient'>Vous avez associ� le client </span><span id='clientEncours'>"
            + ((Utilisateur) session.getAttribute("utilisateur")).getClient().getNomClient() + " ("
            + ((Utilisateur) session.getAttribute("utilisateur")).getClient().getNumeroClient() + "/"
            + ((Utilisateur) session.getAttribute("utilisateur")).getClient().getSuffixeClient()
            + ")</span><span class='superflusClient'> � votre session</span></div>";
        if (((Utilisateur) session.getAttribute("utilisateur")).getMonPanier() != null
            && ((Utilisateur) session.getAttribute("utilisateur")).getMonPanier().getListeArticles().size() == 0) {
          retour += "<a id='blocModifClient' href='clients'><img class='imagesClients' src='images/decoration/modifClient.png'/></a>";
          retour +=
              "<a id='blocSupprClient' href='clients?reinit=1'><img class='imagesClients' src='images/decoration/supprClient.png'/></a>";
        }
        retour += "</div>";
      }
      else {
        retour += "<div id='surBlocClient'>";
        retour +=
            "<div id='blocrepresentant'><span class='superflusClient'>Vous n'avez associ� </span><span id='clientEncours'>aucun client � votre session</span></div>";
        retour += "<a id='blocAjoutCLient' href='clients'><img class='imagesClients' src='images/decoration/ajoutClient.png'/></a>";
        retour += "</div>";
      }
    }
    
    return retour;
  }
  
  /**
   * M�thode g�n�rique de contr�le d'acc�s � la page en question
   */
  protected void redirectionSecurite(HttpServletRequest request, HttpServletResponse response) {
    try {
      int accesUtilisateur = ConstantesEnvironnement.ACCES_REFUSE;
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        
        // Cas ou la session d�passe le nombre maximum de tentatives de connexion � un compte
        if (((Utilisateur) request.getSession().getAttribute("utilisateur"))
            .getTentativesConnexions() > ConstantesEnvironnement.MAX_TENT_CONNEX) {
          accesUtilisateur = ConstantesEnvironnement.BLOC_SAISIE_MAX;
          if (((Utilisateur) request.getSession().getAttribute("utilisateur"))
              .getTentativesConnexions() == ConstantesEnvironnement.MAX_TENT_CONNEX + 1) {
            System.out.println("Session " + request.getSession().getId() + " Adresse IP " + request.getRemoteAddr() + " bloquee !!");
            // Ne pas repasser dans cette boucle avant une nouvelle session
            ((Utilisateur) request.getSession().getAttribute("utilisateur"))
                .setTentativesConnexions(((Utilisateur) request.getSession().getAttribute("utilisateur")).getTentativesConnexions() + 1);
          }
        }
        else
          accesUtilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getUS_ACCES();
      }
      
      // Si on est pas autoris� ou on doit saisir un num�ro client avant d'acc�der � la page
      if (accesUtilisateur < accesPage || accesUtilisateur == ConstantesEnvironnement.ACCES_MULTIPLE_CONNEX
          || accesUtilisateur == ConstantesEnvironnement.ACCES_MULTIPLE_INSCRI) {
        
        if (ConstantesEnvironnement.MODE_DEBUG) {
          System.out.println("[redirectionSecurite] " + request.getSession().getId() + " Adresse IP " + request.getRemoteAddr() + " -> ");
          System.out.println("++++++ accesUtilisateur: " + accesUtilisateur);
          System.out.println("++++++ accesPage: " + accesPage);
        }
        getServletContext().getRequestDispatcher("/connexion?echec=" + accesUtilisateur).forward(request, response);
      }
      
      if (accesUtilisateur > ConstantesEnvironnement.ACCES_REPRES_WS
          && ((Utilisateur) request.getSession().getAttribute("utilisateur")).getETB_EN_COURS() == null) {
        System.out.println("acc�s utilisateur " + accesUtilisateur);
        if (request.getParameter("choixETB") == null)
          getServletContext().getRequestDispatcher("/connexion?choixETB=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public void init(ServletConfig config) {
    try {
      super.init(config);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * M�thode qui regroupe le traitement par GET ou par POST
   */
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    // request.getSession().setAttribute("utilisateur", null); TESTER LES OBJETS A NULL
    if (request != null && request.getSession() != null && request.getSession().getAttribute("utilisateur") != null) {
      // maj de la page en cours de l'utilisateur
      ((Utilisateur) request.getSession().getAttribute("utilisateur")).majPageEnCours(nomPage, request);
      // nettoyage des listes temporaires
      Gestion.initListesTemporaires((Utilisateur) request.getSession().getAttribute("utilisateur"));
      // S�curiser le cookie de SESSION en sortie
      response.setHeader("Set-Cookie", "JSESSIONID=" + request.getSession().getId() + "; Path=/WebShop; HttpOnly");
    }
    else
      try {
        logs.loggerSiDEBUG("********** Session ou utilisateur � NULL");
        getServletContext().getRequestDispatcher("/connexion").forward(request, response);
      }
      catch (ServletException e) {
        // TODO Bloc catch g�n�r� automatiquement
        e.printStackTrace();
      }
      catch (IOException e) {
        // TODO Bloc catch g�n�r� automatiquement
        e.printStackTrace();
      }
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    try {
      gererLesCasMoisis(request, response);
    }
    catch (ServletException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    try {
      gererLesCasMoisis(request, response);
    }
    catch (ServletException e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
    }
    catch (IOException e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
    }
  }
  
  /**
   * g�rer les cas moisis de requ�tes moisies
   * @throws IOException
   * @throws ServletException
   */
  private void gererLesCasMoisis(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    if (ConstantesEnvironnement.IS_MODE_SSL) {
      if (!request.isSecure()) {
        System.out.println("controlerRequetesHTTP a rejet� la requ�te -> INVALIDATE SESSION");
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        // request.getSession().invalidate();
        return;
      }
    }
    
    if (controlerRequetesHTTP(request, response))
      traiterPOSTouGET(request, response);
    else {
      // Renouvellement de session obsol�te
      if (request == null || request.getSession() == null || ((Utilisateur) request.getSession().getAttribute("utilisateur") == null)) {
        // L� on a vraiment un souci.....
        if (request == null) {
          System.out.println("REQUETE EST NULL -> SC_FORBIDDEN");
          response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
        // renouveller la session
        else if (request.getSession() == null) {
          request.getSession(true);
          System.out.println("ON RECREE UNE SESSION: " + request.getSession().getId());
          request.getSession().setAttribute("utilisateur", new Utilisateur(request.getSession().getId()));
          getServletContext().getRequestDispatcher("/connexion").forward(request, response);
        }
        // Cr�er un utilisateur si il n'existe pas dans la session
        else if (request.getSession().getAttribute("utilisateur") == null) {
          // System.out.println("pb d'utilisateur � null: " + request.getSession().getId());
          request.getSession().setAttribute("utilisateur", new Utilisateur(request.getSession().getId()));
          getServletContext().getRequestDispatcher("/connexion").forward(request, response);
        }
      }
      // requ�te moisie
      else {
        System.out.println("controlerRequetesHTTP a rejet� la requ�te -> INVALIDATE SESSION");
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        request.getSession().invalidate();
      }
    }
  }
  
  /**
   * affiche l'info stock en fonction du param�tre filiale ou des d�cimales
   * choisies
   */
  public String afficherStockCorrectement(Utilisateur utilisateur, String donnee) {
    if (donnee == null || donnee.trim().equals("") || utilisateur == null || utilisateur.getETB_EN_COURS() == null)
      return "";
    String retour = donnee;
    
    // Si on est en mode affichage simple
    if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_SANS_VALEUR) {
      float stock = 0;
      try {
        stock = Float.parseFloat(donnee);
        
        if (stock > 0 && utilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI)
          retour = "<span class='dispoStock'>&nbsp;</span>";
        else
          retour = "<span class='pasDispoStock'>&nbsp;</span>";
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
        retour = "&nbsp;";
      }
    }
    // On formate l'affichage du vrai stock
    else {
      // Si on est en n�gatif on cache le n�gatif
      try {
        if (Float.parseFloat(donnee) < 0)
          return "0";
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
        return "0";
      }
      // Sinon on affiche sans d�cimale le stock
      String[] str = donnee.split("\\.");
      if (str.length == 2) {
        donnee = str[0];
      }
      
      retour = donnee;
      
    }
    
    return retour;
  }
  
  /**
   * On �chappe les simples quotes
   */
  public String traiterCaracteresINPUT(String brut) {
    if (brut == null) {
      return null;
    }
    
    return StringEscapeUtils.escapeHtml4(brut);
  }
  
  /**
   * Afficher un icone repr�sentatif du stock saisi
   */
  public String afficherStockFiche(Utilisateur utilisateur, String donnee, String taille) {
    if (donnee == null || donnee.trim().equals(""))
      return null;
    String retour = donnee;
    
    if (utilisateur.getETB_EN_COURS().getMode_stocks() == ConstantesEnvironnement.STOCK_SANS_VALEUR) {
      float stock = 0;
      try {
        stock = Float.parseFloat(donnee);
        
        if (stock > 0)
          retour = "<img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_stockOK" + taille + ".png'/>";
        else
          retour = "<img src='" + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_stockNUL" + taille + ".png'/>";
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
        retour = "&nbsp;";
      }
    }
    
    return retour;
  }
  
  /**
   * Afficher une valeur d�cimale (STOCK ET CONDITIONNEMENTS) correctement HORS PRIX
   */
  public String afficherValeurCorrectement(Utilisateur utilisateur, String donnee) {
    if (donnee == null || donnee.trim().equals(""))
      return null;
    String retour = donnee;
    
    // System.out.println("afficherValeurCorrectement(): VALEUR DONNEE AVANT " + donnee) ;
    
    if (donnee.endsWith(".000000"))
      donnee = donnee.replaceAll("\\.000000", "");
    else if (donnee.endsWith(".00000"))
      donnee = donnee.replaceAll("\\.00000", "");
    else if (donnee.endsWith(".0000"))
      donnee = donnee.replaceAll("\\.0000", "");
    else if (donnee.endsWith(".000"))
      donnee = donnee.replaceAll("\\.000", "");
    else if (donnee.endsWith(".00"))
      donnee = donnee.replaceAll("\\.00", "");
    else if (donnee.endsWith(".0"))
      donnee = donnee.replaceAll("\\.0", "");
    retour = donnee;
    
    // System.out.println("afficherValeurCorrectement(): VALEUR DONNEE AP " + donnee) ;
    
    return retour;
  }
  
  /**
   * retourne le contenu d'un message mail de validation de commande et de devis pour un client
   */
  protected String retournerMessageValidation(Utilisateur utilisateur, String type, LinkedHashMap<String, String> rapport, int typeMail) {
    String L1COD = null;
    String numero = null;
    String suffixe = null;
    String contenu = "";
    
    if (rapport != null) {
      L1COD = rapport.get("TYPE");
      numero = rapport.get("NUMERO");
      suffixe = rapport.get("SUFFIXE");
      
    }
    
    if (numero != null && suffixe != null) {
      contenu += "<table id='confCommande'>";
      if (typeMail == MAIL_CLIENT) {
        contenu += "<tr><td class='paddingHorizontal'></td><td class='h2Mail'>Confirmation de " + type
            + "</td><td class='paddingHorizontal'></td></tr>";
      }
      else if (typeMail == MAIL_GESTIONNAIRE) {
        contenu += "<tr><td class='paddingHorizontal'></td><td class='h2Mail'>Rapport de " + type
            + "</td><td class='paddingHorizontal'></td></tr>";
      }
      contenu += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
      
      if (typeMail == MAIL_CLIENT) {
        contenu += "<tr><td class='paddingHorizontal'></td><td class='messageDeConf'>Votre " + type + " <span class='importantMail'>N� "
            + numero + "/" + suffixe + "</span> pour la soci�t� " + utilisateur.getClient().getNomClient()
            + " a bien �t� transmise � nos services.</td><td class='paddingHorizontal'></td></tr>";
        contenu +=
            "<tr><td class='paddingHorizontal'></td><td class='messageDeConf'>Vous recevrez dans les plus brefs d�lais, la confirmation de disponibilit� de la part de notre agent Web Shop</td><td class='paddingHorizontal'></td></tr>";
      }
      else if (typeMail == MAIL_GESTIONNAIRE) {
        contenu += "<tr><td class='paddingHorizontal'></td><td class='messageDeConf'>" + type + " N� " + numero + suffixe
            + " de la part de " + utilisateur.getUS_LOGIN() + " pour le client <b>" + utilisateur.getClient().getNomClient() + "("
            + utilisateur.getClient().getNumeroClient() + ")</b></td><td class='paddingHorizontal'></td></tr>";
      }
      
      contenu += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
      contenu += "</table>";
      
      contenu += afficherListeArticlesMail(utilisateur, type, L1COD, numero, suffixe, typeMail);
    }
    
    return contenu;
  }
  
  /**
   * retourne la liste format�e des articles qui doivent s'afficher dans le mail de conf
   */
  public String afficherListeArticlesMail(Utilisateur utilisateur, String demande, String code, String numero, String suffixe,
      int typeMail) {
    StringBuilder retour = new StringBuilder(5000);
    if (utilisateur == null || numero == null || suffixe == null)
      return retour.toString();
    
    if (mesDonnees == null)
      mesDonnees = new GestionMesDonnees();
    
    if (mesDonnees != null) {
      numero = code.trim() + numero.trim() + suffixe.trim();
      GenericRecord enTeteCommande = mesDonnees.recupererEnteteCommande(utilisateur, numero);
      ArrayList<GenericRecord> listeArticles = mesDonnees.recupererDetailCommande(utilisateur, numero, false);
      
      if (enTeteCommande != null) {
        if (enTeteCommande.isPresentField("E1MEX")) {
          retour.append("<table>");
          // Si le mode d'exp�dition est le mode de retrait
          if (enTeteCommande.getField("E1MEX").toString().trim().equals("WE")) {
            if (typeMail == MAIL_CLIENT) {
              retour.append(
                  "<tr><td class='paddingHorizontal'></td><td colspan='2' id='modeExpe'>Vous avez choisi de retirer votre commande en magasin</td><td class='paddingHorizontal'></td></tr>");
            }
            else if (typeMail == MAIL_GESTIONNAIRE) {
              retour.append(
                  "<tr><td class='paddingHorizontal'></td><td colspan='2' id='modeExpe'>Le client a choisi de retirer la commande en magasin</td><td class='paddingHorizontal'></td></tr>");
            }
          }
          // Si le mode d'exp�dition est la livraison
          else if (enTeteCommande.getField("E1MEX").toString().trim().equals("WL")) {
            if (typeMail == MAIL_CLIENT) {
              retour.append(
                  "<tr><td class='paddingHorizontal'></td><td colspan='2' id='modeExpe'>Vous avez choisi de vous faire livrer votre marchandise aux coordonn�es ci-dessous:</td><td class='paddingHorizontal'></td></tr>");
            }
            else if (typeMail == MAIL_GESTIONNAIRE) {
              retour.append(
                  "<tr><td class='paddingHorizontal'></td><td colspan='2' id='modeExpe'>Le client a choisi de se faire livrer la marchandise aux coordonn�es ci-dessous:</td><td class='paddingHorizontal'></td></tr>");
              
            }
            
            if (enTeteCommande.isPresentField("CLINOM") && !enTeteCommande.getField("CLINOM").toString().trim().equals(""))
              retour.append("<tr><td class='paddingHorizontal'></td><td colspan='2' class='infosLivr'>"
                  + enTeteCommande.getField("CLINOM").toString().trim() + "</td><td class='paddingHorizontal'></td></tr>");
            if (enTeteCommande.isPresentField("CLICPL") && !enTeteCommande.getField("CLICPL").toString().trim().equals(""))
              retour.append("<tr><td class='paddingHorizontal'></td><td colspan='2' class='infosLivr'>"
                  + enTeteCommande.getField("CLICPL").toString().trim() + "</td><td class='paddingHorizontal'></td></tr>");
            if (enTeteCommande.isPresentField("CLIRUE") && !enTeteCommande.getField("CLIRUE").toString().trim().equals(""))
              retour.append("<tr><td class='paddingHorizontal'></td><td colspan='2' class='infosLivr'>"
                  + enTeteCommande.getField("CLIRUE").toString().trim() + "</td><td class='paddingHorizontal'></td></tr>");
            if (enTeteCommande.isPresentField("CLILOC") && !enTeteCommande.getField("CLILOC").toString().trim().equals(""))
              retour.append("<tr><td class='paddingHorizontal'></td><td colspan='2' class='infosLivr'>"
                  + enTeteCommande.getField("CLILOC").toString().trim() + "</td><td class='paddingHorizontal'></td></tr>");
            retour.append("<tr><td class='paddingHorizontal'></td>");
            if (enTeteCommande.isPresentField("CLICDP"))
              retour.append("<td id='cdpMail'>" + enTeteCommande.getField("CLICDP").toString().trim() + "</td>");
            if (enTeteCommande.isPresentField("CLIVIL"))
              retour.append("<td id='villeMail'>" + enTeteCommande.getField("CLIVIL").toString().trim() + "</td>");
            retour.append("<td class='paddingHorizontal'></td></tr>");
          }
          
          if (enTeteCommande.isPresentField("E1RCC") && !enTeteCommande.getField("E1RCC").toString().trim().equals("")) {
            retour.append("<tr>" + "<td class='paddingHorizontal'></td>");
            retour.append("<td id='lbRefCde'>Ref.</td>");
            retour.append("<td id='refCde'>" + enTeteCommande.getField("E1RCC").toString().trim() + "</td>");
            retour.append("<td class='paddingHorizontal'></td>" + "</tr>");
          }
          
          retour.append("<tr ><td  colspan='4' class='paddingVertical'></td></tr>");
          
          retour.append("</table>");
        }
      }
      
      if (listeArticles != null && listeArticles.size() > 0) {
        retour.append("<table id='listeDarticles'>");
        
        retour.append(
            "<tr><td class='paddingHorizontal'></td><td colspan='6' id='enteteliste'>Les articles</td><td class='paddingHorizontal'></td></tr>");
        
        for (int i = 0; i < listeArticles.size(); i++) {
          retour.append("<tr class='ligneArticleMail'>");
          
          retour.append("<td class='paddingHorizontal'></td>");
          
          retour.append("<td class='refArticleMail'>");
          // r�f�rence ou code article
          if (listeArticles.get(i).isPresentField(utilisateur.getETB_EN_COURS().getZone_reference_article())
              && !listeArticles.get(i).getField(utilisateur.getETB_EN_COURS().getZone_reference_article()).toString().trim().equals(""))
            retour.append(listeArticles.get(i).getField(utilisateur.getETB_EN_COURS().getZone_reference_article()));
          else if (listeArticles.get(i).isPresentField("L1ART"))
            retour.append(listeArticles.get(i).getField("L1ART").toString().trim());
          retour.append("</td>");
          
          if (!listeArticles.get(i).isPresentField("A1LIB") && listeArticles.get(i).isPresentField("L1ART"))
            listeArticles.get(i).setField("A1LIB", listeArticles.get(i).getField("L1ART").toString().trim());
          retour.append("<td class='libArticleMail'>" + listeArticles.get(i).getField("A1LIB").toString() + "</td>");
          
          if (listeArticles.get(i).isPresentField("L1QTE"))
            retour.append("<td class='qteArticleMail'>"
                + afficherValeurCorrectement(utilisateur, listeArticles.get(i).getField("L1QTE").toString().trim()) + "</td>");
          retour.append("<td class='multipleMail'> x </td>");
          if (listeArticles.get(i).isPresentField("L1PVC") && listeArticles.get(i).isPresentField("L1UNV"))
            retour.append("<td class='tarifArticleMail'>"
                + Outils.formaterUnTarifArrondi(utilisateur, listeArticles.get(i).getField("L1PVC")) + "</td>");
          if (listeArticles.get(i).isPresentField("L1MHT") && listeArticles.get(i).isPresentField("L1UNV"))
            retour.append("<td class='totalArticleMail'>"
                + Outils.formaterUnTarifArrondi(utilisateur, listeArticles.get(i).getField("L1MHT")) + "</td>");
          
          retour.append("<td class='paddingHorizontal'></td>");
          retour.append("</tr>");
        }
        
        if (enTeteCommande != null && enTeteCommande.isPresentField("E1THTL"))
          retour.append("<tr><td class='paddingHorizontal'></td><td  colspan='6' id='totalCommandeMail'>Total H.T : "
              + Outils.formaterUnTarifArrondi(utilisateur, enTeteCommande.getField("E1THTL"))
              + " </td><td class='paddingHorizontal'></td></tr>");
        retour.append("</table>");
      }
    }
    
    return retour.toString();
  }
  
  /**
   * Contr�ler la requ�te saisi par le client sur un
   */
  public boolean controlerRequetesHTTP(HttpServletRequest request, HttpServletResponse response) {
    if (request == null || request.getSession() == null || ((Utilisateur) request.getSession().getAttribute("utilisateur") == null)) {
      if (ConstantesEnvironnement.MODE_DEBUG)
        System.out.println("[controlerRequetesHTTP] la requete / la session HTTP ou l'utilisateur sont nulles !!");
      return false;
    }
    // Si on a affaire � une adresse d�j� rentr�e en bDD pas la peine d'aller plus loin
    if (SessionListener.listeIpBloquees != null && SessionListener.listeIpBloquees.containsKey(request.getRemoteAddr())
        && SessionListener.listeIpBloquees.get(request.getRemoteAddr()) >= ConstantesEnvironnement.NB_SESSIONS_MAX)
      return false;
    // On contr�le les requ�tes suspectes sur l'ID de session qui envoie la requ�te
    else if (!request.isRequestedSessionIdValid() && request.getRequestedSessionId() == null) {
      if (ConstantesEnvironnement.MODE_DEBUG) {
        System.out.println(System.getProperty("line.separator"));
        System.out.println("+++++++++++++++++++++++++ Requete " + request.getQueryString() + "/" + request.getSession().getId()
            + " suspecte. on va checker " + request.getRemoteAddr());
      }
      // On teste l'adresse IP de cette requ�te ainsi que sa fr�quence de demandes de requ�tes pour �viter les robots
      return GestionRequetes.testerRequete(request);
    }
    else {
      if (ConstantesEnvironnement.MODE_DEBUG) {
        System.out.println(System.getProperty("line.separator"));
        System.out.println("+++++++++++++++++++++++++ Requete " + request.getQueryString() + "/" + request.getSession().getId() + " valide "
            + request.getRemoteAddr() + " de la session " + request.getSession().getId());
        SessionListener.afficherListeDesSessions();
      }
      return true;
    }
  }
  
  /**
   * Afficher la pagination d'une liste
   */
  protected String afficherPaginationArticles(Utilisateur utilisateur, float total, int indice, String parametres) {
    int nbPage = -1;
    int numPage = -1;
    int debItem = 0;
    
    if (parametres == null)
      parametres = "";
    else
      parametres += "&";
    
    StringBuilder retour = new StringBuilder(400);
    nbPage = (int) Math.ceil(total / utilisateur.getETB_EN_COURS().getLimit_liste());
    
    if (nbPage > 1) {
      retour.append("<div class='blocPagination'>");
      for (int p = 0; p < nbPage; p++) {
        numPage = p + 1;
        if (indice >= debItem && indice < debItem + utilisateur.getETB_EN_COURS().getLimit_liste()) {
          if (indice > 0)
            retour.append("<span class='spanPagination'> < </span>");
          
          retour.append("<span id='spanPageEnCours'>" + utilisateur.getTraduction().traduire("$$page") + " " + numPage + "</span>");
        }
        else {
          if (indice > debItem && debItem > 0)
            retour.append("<span class='spanPagination'> < </span>");
          else if (debItem != 0)
            retour.append("<span> > </span>");
          
          retour.append("<a class='aPagination' href='" + nomPage + "?" + parametres + "indiceDebut=" + debItem + "'>" + numPage + "</a>");
        }
        
        debItem += utilisateur.getETB_EN_COURS().getLimit_liste();
      }
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * afficher la pagination propre � la liste de pagination de l'utilisateur
   */
  protected String afficherPagination(Utilisateur utilisateur, String indiceString, String parametres) {
    if (utilisateur == null || utilisateur.getDerniereListePagination() == null)
      return "";
    
    float total = utilisateur.getDerniereListePagination().size();
    
    int nbPage = -1;
    int numPage = -1;
    int debItem = 0;
    int indice = 0;
    
    try {
      if (indiceString != null)
        indice = Integer.parseInt(indiceString);
    }
    catch (Exception e) {
      indice = 0;
      logs.forcerLogErreur("afficherPagination() " + e.getMessage());
    }
    
    if (parametres == null)
      parametres = "";
    else
      parametres += "&";
    
    StringBuilder retour = new StringBuilder(400);
    nbPage = (int) Math.ceil(total / utilisateur.getETB_EN_COURS().getLimit_liste());
    
    if (nbPage > 1) {
      retour.append("<div class='blocPagination'>");
      for (int p = 0; p < nbPage; p++) {
        numPage = p + 1;
        if (indice >= debItem && indice < debItem + utilisateur.getETB_EN_COURS().getLimit_liste()) {
          if (indice > 0)
            retour.append("<span class='spanPagination'> < </span>");
          
          retour.append("<span id='spanPageEnCours'>" + utilisateur.getTraduction().traduire("$$page") + " " + numPage + "</span>");
        }
        else {
          if (indice > debItem && debItem > 0)
            retour.append("<span class='spanPagination'> < </span>");
          else if (debItem != 0)
            retour.append("<span> > </span>");
          
          retour.append("<a class='aPagination' href='" + nomPage + "?" + parametres + "indiceDebut=" + debItem + "'>" + numPage + "</a>");
        }
        
        debItem += utilisateur.getETB_EN_COURS().getLimit_liste();
      }
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Retourner la bonne adresse Mail de confirmation de commande N�gociant
   * Si une adresse est d�clar�e sur le magasin de retrait la prendre sinon prendre l'adresse de l'�tablissement en
   * cours
   */
  protected String retournerAdresseMailConfirmation(String pMag, String pTypeBon, Utilisateur pUtilisateur) {
    String retour = "";
    
    // Si on a pas de magasin associ�
    if (pTypeBon == null || pUtilisateur == null)
      return null;
    
    if (pMag != null) {
      retour = retournerAdresseMailMagasin(pUtilisateur, pTypeBon, pMag);
    }
    
    // Si il n'y a aucune adresse mail magasin
    if (retour == null) {
      if (pTypeBon.equals("E")) {
        retour = pUtilisateur.getETB_EN_COURS().getMail_commandes();
      }
      else if (pTypeBon.equals("D")) {
        retour = pUtilisateur.getETB_EN_COURS().getMail_devis();
      }
    }
    
    return retour;
  }
  
  /**
   * Retourner l'adresse mail d'une commande ou d'un devis d�clar�e pour un magasin
   */
  private String retournerAdresseMailMagasin(Utilisateur pUtilisateur, String pTypeBon, String pMag) {
    if (pMag == null || pUtilisateur == null || pTypeBon == null) {
      return null;
    }
    
    String retour = null;
    String zoneMail = "MG_MAIL_CD";
    
    if (pTypeBon.equals("E")) {
      zoneMail = "MG_MAIL_CD";
    }
    else if (pTypeBon.equals("D")) {
      zoneMail = "MG_MAIL_DV";
    }
    
    ArrayList<GenericRecord> liste = null;
    liste = pUtilisateur.getAccesDB2()
        .select("SELECT " + zoneMail + " FROM " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB = '"
            + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND MG_COD ='" + pMag + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).getField(zoneMail) != null && !liste.get(0).getField(zoneMail).toString().trim().equals("")) {
        retour = liste.get(0).getField(zoneMail).toString().trim();
      }
    }
    
    return retour;
  }
  
  /*++++++++++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
  public PatternErgonomie getPattern() {
    return pattern;
  }
  
  public void setPattern(PatternErgonomie pattern) {
    this.pattern = pattern;
  }
  
  public String getNomPage() {
    return nomPage;
  }
  
  public void setNomPage(String nomPage) {
    this.nomPage = nomPage;
  }
  
  public Gestion getMaGestion() {
    return maGestion;
  }
  
  public void setMaGestion(Gestion maGestion) {
    this.maGestion = maGestion;
  }
  
  public String getCssSpecifique() {
    return cssSpecifique;
  }
  
  public void setCssSpecifique(String cssSpecifique) {
    this.cssSpecifique = cssSpecifique;
  }
  
  public int getAccesPage() {
    return accesPage;
  }
  
  public void setAccesPage(int accesPage) {
    this.accesPage = accesPage;
  }
  
}
