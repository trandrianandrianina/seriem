
package ri.seriem.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;

/**
 * Permet de g�rer l'ensemble des sessions cr��es sur le web shop
 */
public class SessionListener implements HttpSessionListener {
  public static ArrayList<HttpSession> listeSessions = null;
  
  // liste des IP bloquees
  public static HashMap<String, Integer> listeIpBloquees = null;
  
  // permet de g�rer la s�curit� des requ�tes rentrantes
  public static GestionRequetes gestionDesRequetes = null;
  
  /**
   * lanc�e � la cr�ation d'une session
   */
  public void sessionCreated(HttpSessionEvent moteur) {
    // Cr�er un utilisateur si il n'existe pas
    if (moteur.getSession().getAttribute("utilisateur") == null)
      moteur.getSession().setAttribute("utilisateur", new Utilisateur(moteur.getSession().getId()));
    
    if ((Utilisateur) moteur.getSession().getAttribute("utilisateur") != null) {
      // Attribuer un temps max d'inactivit� � la session
      moteur.getSession().setMaxInactiveInterval(ConstantesEnvironnement.TEMPS_SESSION_INACTIVE);
      // on cr�e la liste de sessions si elle est nulle
      if (listeSessions == null)
        listeSessions = new ArrayList<HttpSession>();
      listeSessions.add(moteur.getSession());
      
      // On logge la session
      if (((Utilisateur) moteur.getSession().getAttribute("utilisateur")).getAccesDB2() != null) {
        
      }
      else
        System.out.println("Acc�s DB2 inexistant");
    }
    else {
      moteur.getSession().invalidate();
      System.out.println("utilisateur inexistant");
    }
    
    if (ConstantesEnvironnement.MODE_DEBUG || ConstantesDebug.DEBUG_CONNEXIONS)
      afficherListeDesSessions();
  }
  
  /**
   * lanc�e � la destruction d'une session
   */
  public void sessionDestroyed(HttpSessionEvent moteur) {
    // si il s'agit d'une session utilisateur la logger sinon .... fuck
    if ((Utilisateur) moteur.getSession().getAttribute("utilisateur") != null) {
      if (((Utilisateur) moteur.getSession().getAttribute("utilisateur")).getUS_ACCES() > ConstantesEnvironnement.ACCES_PUBLIC)
        ((Utilisateur) moteur.getSession().getAttribute("utilisateur")).getAccesDB2().ajoutUnLog(
            ((Utilisateur) moteur.getSession().getAttribute("utilisateur")).getUS_ID(), moteur.getSession().getId(), "F",
            "Fin de connexion " + ((Utilisateur) moteur.getSession().getAttribute("utilisateur")).getUS_LOGIN(), "SessionListener");
      ((Utilisateur) moteur.getSession().getAttribute("utilisateur")).deconnecterAS400();
    }
    
    // Virer la session de la liste
    if (listeSessions != null && listeSessions.size() > 0) {
      int i = 0;
      boolean trouve = false;
      
      while (i < listeSessions.size() && !trouve) {
        if (listeSessions.get(i).getId().equals(moteur.getSession().getId())) {
          listeSessions.remove(i);
          trouve = true;
        }
        i++;
      }
    }
    
    if (ConstantesEnvironnement.MODE_DEBUG || ConstantesDebug.DEBUG_CONNEXIONS)
      afficherListeDesSessions();
  }
  
  /**
   * affiche la liste des sessions connect�es au Web Shop
   */
  public static void afficherListeDesSessions() {
    System.out.println(System.getProperty("line.separator"));
    System.out.println("+++++++++++++++++++++++++++++++++++ GESTION SESSIONS +++++++++++++++++++++++++++++++++++");
    System.out.println(System.getProperty("line.separator"));
    
    if (listeSessions != null && listeSessions.size() > 0) {
      System.out.println("Sessions actives: " + listeSessions.size());
      for (int i = 0; i < listeSessions.size(); i++)
        System.out.println("Session " + i + " -> " + listeSessions.get(i).getId());
    }
    else
      System.out.println("Pas de session active");
    
    System.out.println(System.getProperty("line.separator"));
    System.out.println("+++++++++++++++++++++++++++++++++++ FIN SESSIONS +++++++++++++++++++++++++++++++++++");
    System.out.println(System.getProperty("line.separator"));
  }
  
  /**
   * Affiche la liste des adresses IP bloqu�es dans le contexte
   */
  public static void afficherListeIpBloquees() {
    
    System.out.println(System.getProperty("line.separator"));
    System.out.println("--- liste des adresses IP bloquees: ");
    
    if (listeIpBloquees == null || listeIpBloquees.size() == 0)
      System.out.println("--- Cette liste d'adresse bloqu�e est vide");
    else
      for (Map.Entry<String, Integer> indice : listeIpBloquees.entrySet())
        System.out.println("--------- Adresse " + indice.getKey() + " -> " + indice.getValue().toString());
  }
}
