
package ri.seriem.webshop.environnement;

import java.io.File;
import java.sql.Driver;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import ri.seriem.libcommun.outils.FileNG;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.outils.Installation;
import ri.seriem.webshop.outils.VersionWar;

public class ContexteListener implements ServletContextListener {
  
  // Variables
  private ServletContext context = null;
  
  MesLogs mesLogs = null;
  // private static final String NAME_LOG4J = "log4j.properties";
  
  public void contextInitialized(ServletContextEvent sce) {
    
    System.out.println("== D�marrage de l'application Web Shop ==\n");
    
    // liste les user connect�s
    // ArrayList<Utilisateur> list = new ArrayList<Utilisateur>();
    // ServletContext context = event.getServletContext();
    // System.out.println(list.size());
    
    // Chargement des variables du context.xml au d�marrage
    context = sce.getServletContext();
    initWithContextFile();
    // context.setAttribute("MesUtilisateursConnectes",list);
    // Gestion du d�ploiement
    deploy();
    
    // maj de la base Db2 si n�cessaire
    majDB2();
    
    // supprime les vieux logs
    supprVieuxLogs();
  }
  
  public void contextDestroyed(ServletContextEvent sce) {
    Utilisateur utilisateur = new Utilisateur();
    
    if (utilisateur.getAccesDB2() != null) {
      MajDB2 majDB2 = new MajDB2(utilisateur);
      if (!majDB2.supprimerIndexs())
        System.out.println("++++++ ATTENTION ++++++ Probl�me de supprimerIndexs()");
    }
    else
      System.out.println("++++++ ATTENTION ++++++ Probl�me de majDB2()");
    
    System.out.println("== Arr�t de l'application Web Shop ==");
    
    Enumeration<Driver> drivers = null;
    try {
      drivers = DriverManager.getDrivers();
      while (drivers.hasMoreElements()) {
        DriverManager.deregisterDriver(drivers.nextElement());
        System.out.println("++++++++ Web Shop driver desinscrit: " + drivers.nextElement().toString());
      }
    }
    catch (Exception e) {
      if (drivers != null && drivers.hasMoreElements())
        System.out.println("Web Shop PB pour desinscrire ce driver: " + drivers.nextElement().toString());
      else
        System.out.println("PB pour desinscrire un driver");
    }
  }
  
  /**
   * Lecture des variables dans le fichier context.xml pour initialiser certaines variables d'environnement
   */
  private void initWithContextFile() {
    boolean refresh = false;
    
    String chaine = context.getInitParameter("ADRESSE_AS400");
    if (chaine != null) {
      ConstantesEnvironnement.ADRESSE_AS400 = chaine;
      refresh = true;
    }
    chaine = context.getInitParameter("PROFIL_AS_ANONYME");
    if (chaine != null) {
      ConstantesEnvironnement.PROFIL_AS_ANONYME = chaine;
      refresh = true;
    }
    chaine = context.getInitParameter("MP_AS_ANONYME");
    if (chaine != null) {
      ConstantesEnvironnement.MP_AS_ANONYME = chaine;
      refresh = true;
    }
    chaine = context.getInitParameter("DOSSIER_RACINE_TOMCAT");
    if (chaine != null) {
      ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT = chaine;
      refresh = true;
    }
    chaine = context.getInitParameter("DOSSIER_RACINE_PUBLIC");
    if (chaine != null) {
      ConstantesEnvironnement.DOSSIER_RACINE_PUBLIC = chaine;
      refresh = true;
    }
    chaine = context.getInitParameter("DOSSIER_RACINE_PRIVATE");
    if (chaine != null) {
      ConstantesEnvironnement.DOSSIER_RACINE_PRIVATE = chaine;
      refresh = true;
    }
    chaine = context.getInitParameter("DOSSIER_RACINE_XWEBSHOP");
    if (chaine != null) {
      ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP = chaine;
      if (ConstantesEnvironnement.versionWAR == null && ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP != null)
        ConstantesEnvironnement.versionWAR = VersionWar.recupereVersion();
      refresh = true;
    }
    chaine = context.getInitParameter("DOSSIER_SRC");
    if (chaine != null) {
      ConstantesEnvironnement.DOSSIER_SRC = chaine;
      refresh = true;
    }
    
    chaine = context.getInitParameter("LIBELLE_SERVEUR");
    if (chaine != null)
      ConstantesEnvironnement.LIBELLE_SERVEUR = chaine;
    else
      ConstantesEnvironnement.LIBELLE_SERVEUR = "";
    
    chaine = context.getInitParameter("IS_ECLIPSE");
    if (chaine != null)
      ConstantesEnvironnement.isEclipse = chaine.trim().equalsIgnoreCase("true");
    
    chaine = context.getInitParameter("LIB_RACINE");
    if (chaine != null)
      ConstantesEnvironnement.BIBLI_WS = chaine;
    
    if (refresh)
      ConstantesEnvironnement.refreshValue();
  }
  
  /**
   * Analyse et d�ploie si n�cessaire
   */
  private void deploy() {
    Installation deploy = new Installation();
    switch (deploy.check4Installation()) {
      case Installation.INSTALL:
        deploy.installation();
        break;
      case Installation.UPDATE:
        deploy.update();
        break;
    }
  }
  
  /**
   * Cette m�thode permet de mettre � jour certaines tables de DB2 dans XWEB
   */
  private void majDB2() {
    Utilisateur utilisateur = new Utilisateur();
    
    if (utilisateur.getAccesDB2() != null) {
      MajDB2 majDB2 = new MajDB2(utilisateur);
      if (!majDB2.mettreAJourDB2())
        System.out.println("++++++ ATTENTION ++++++ Probl�me de mettreAJourDB2()");
      
    }
    else
      System.out.println("++++++ ATTENTION ++++++ Probl�me de majDB2()");
  }
  
  private void supprVieuxLogs() {
    String monFichier = null;
    
    FileNG cheminDesLogs = new FileNG(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT + File.separator + "logs" + File.separator);
    
    // liste de mes fichiers
    File[] listF = cheminDesLogs.listFiles();
    
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat formater = null;
    formater = new SimpleDateFormat("yyyy-MM-dd");
    calendar.add(Calendar.DATE, -(ConstantesEnvironnement.NB_JOURS_MAX_LOGS));
    String pouet = formater.format(calendar.getTime());
    // System.out.println("Suppression des anciens fichiers logs : "+pouet);
    
    String monf = null;
    FileNG fileAsupprimer = null;
    if (cheminDesLogs.exists()) {
      for (int i = 0; i < listF.length; i++) {
        monFichier = listF[i].getName();
        if (monFichier.length() > 12) {
          monFichier = listF[i].getName();
          monf = monFichier.substring(12, 22);
          if (monf.compareTo(pouet) < 0) {
            fileAsupprimer = new FileNG(cheminDesLogs + File.separator + monFichier);
            fileAsupprimer.remove();
          }
          
        }
      }
    }
    else if (ConstantesEnvironnement.MODE_DEBUG) {
      System.out.println("-----Le repertoire des logs est manquant---- Probl�me avec LOG4J");
      
    }
  }
  
}
