
package ri.seriem.webshop.environnement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.ControleurZone;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.metier.Etablissement;
import ri.seriem.webshop.metier.Panier;
import ri.seriem.webshop.outils.EnvoiMail;
import ri.seriem.webshop.outils.Outils;

public class GestionConnexion extends Gestion {
  private EnvoiMail mails = null;
  
  public GestionConnexion() {
    // super();
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_CONNEXIONS;
  }
  
  /**
   * Mise � jour de la connexion Utilisateur si on lui passe un profil et un mot de passe
   */
  public int majConnexion(HttpSession session, String login, String mdp, String mdpctrl, String client, String provenance,
      boolean isHttps) {
    forcerLogMessage("majConnexion isHttps: " + isHttps);
    
    if (controlerLaSaisie(login, mdp, mdpctrl)) {
      return controlerProfil(session, login, mdp, mdpctrl, client, provenance, isHttps);
    }
    else {
      forcerLogMessage("Probl�me de login et / ou mot de passe : " + login);
      ((Utilisateur) session.getAttribute("utilisateur"))
          .setTentativesConnexions(((Utilisateur) session.getAttribute("utilisateur")).getTentativesConnexions() + 1);
      return ConstantesEnvironnement.ACCES_ER_SAISIE;
    }
  }
  
  /**
   * Contr�ler la saisie des donn�es pass�es
   */
  private boolean controlerLaSaisie(String login, String mdp, String mpdCtrl) {
    int longueurSecurite = 3;
    
    return (login != null && login.trim().length() >= longueurSecurite && (mdp != null && mdp.trim().length() >= longueurSecurite));
  }
  
  /**
   * Contr�ler si le profil existe dans la BDD
   */
  private int controlerProfil(HttpSession session, String login, String mdp, String mdpctrl, String client, String provenance,
      boolean isHttps) {
    if (session.getAttribute("utilisateur") == null)
      session.setAttribute("utilisateur", new Utilisateur(session.getId()));
    
    Utilisateur utilisateur = ((Utilisateur) session.getAttribute("utilisateur"));
    
    if (utilisateur.getAccesDB2() == null)
      utilisateur.majConnexionDB2(ConstantesEnvironnement.PROFIL_AS_ANONYME,
          Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_ANONYME)));
    
    // Si l'utilisateur a bien l'acc�s � DB2
    if (utilisateur.getAccesDB2() != null) {
      utilisateur.setListeDeTravail(
          selectSecurite(utilisateur, "SELECT US_ID,US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB FROM " + ConstantesEnvironnement.BIBLI_WS
              + ".USERW WHERE UPPER(US_LOGIN) = ? " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, login.toUpperCase()));
      
      // Si le profil existe dans la base de donn�es
      if ((utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() == 1)
          || (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 1 && client != null)) {
        if (mdpctrl != null)
          return ConstantesEnvironnement.ACCES_DEJA_EXISTANT;
        int accesWS = ConstantesEnvironnement.ACCES_REFUSE;
        GenericRecord record = utilisateur.getListeDeTravail().get(0);
        // r�cup de l'acc�s de l'utilisateur
        try {
          accesWS = Integer.parseInt(record.getField("US_ACCES").toString());
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        
        // Si son acc�s est celui d'un client ou d'un client en mode consultation
        if (accesWS == ConstantesEnvironnement.ACCES_CLIENT || accesWS == ConstantesEnvironnement.ACCES_CONSULTATION) {
          // On contr�le quand m�me si l'utilisateur est encore existant dans la base S�rie M et autoris� en un seul
          // exemplaire
          int autorisation = controlerClientAutorise(utilisateur, login, record.getField("US_ETB").toString().trim());
          // autoris� et un seul exemplaire
          if (autorisation == 1 || (autorisation > 1 && client != null)) {
            if (client != null) {
              if (client.trim().length() < 6) {
                int boucle = 6 - client.trim().length();
                String ajout = "";
                for (int i = 0; i < boucle; i++)
                  ajout += "0";
                
                client = ajout + client.trim();
              }
              
              utilisateur.setListeDeTravail(selectSecurite(utilisateur,
                  " SELECT US_ID,US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 " + " FROM "
                      + ConstantesEnvironnement.BIBLI_WS + ".USERW " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM "
                      + " ON CAST(RLNUMT AS CHAR(15)) = US_SERIEM " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTEM ON RLNUMT = RENUM " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTWM ON RLNUMT = RWNUM " + " WHERE RLCOD ='C' AND RWACC = 'W' AND UPPER(RENET) = ? "
                      + " AND SUBSTR(RLIND, 1, 6) = '" + client + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
                  login.toUpperCase()));
            }
            // Pas de client associ�
            else {
              utilisateur.setListeDeTravail(selectSecurite(utilisateur,
                  " SELECT US_ID,US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 " + " FROM "
                      + ConstantesEnvironnement.BIBLI_WS + ".USERW " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM "
                      + " ON CAST(RLNUMT AS CHAR(15)) = US_SERIEM " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTEM ON RLNUMT = RENUM " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTWM ON RLNUMT = RWNUM " + " WHERE RLCOD ='C' AND RWACC = 'W' AND UPPER(RENET) = ? "
                      + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
                  login.toUpperCase()));
            }
            
            // on re contr�le la liste au cas ou on l'ait remplie � nouveau
            if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0) {
              // TODO c'est ici qu'on va contr�ler qu'il y ait un seul contact S�rie M associ� dans ce cas il faut
              // supprimer
              if (utilisateur.getListeDeTravail().size() > 1) {
                String codeContact = utilisateur.getListeDeTravail().get(0).getField("US_SERIEM").toString().trim();
                String requete = "SELECT US_ID,US_LOGIN,US_SERIEM FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW WHERE US_SERIEM = '"
                    + codeContact + "' AND UPPER(US_LOGIN) <> '" + login.toUpperCase() + "' ";
                ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(requete);
                if (liste != null) {
                  for (GenericRecord record2 : liste) {
                    String requeteDelete = "DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW WHERE US_ID = '"
                        + record2.getField("US_ID").toString().trim() + "' ";
                    int retour = utilisateur.getAccesDB2().requete(requeteDelete);
                    if (retour < 1) {
                      forcerLogErreur("Probleme pour supprimer l'utilisateur  : " + record2.getField("US_ID").toString().trim());
                    }
                    else {
                      System.out.println("Suppression US_ID : " + record2.getField("US_ID").toString().trim());
                    }
                  }
                }
              }
              
              record = utilisateur.getListeDeTravail().get(0);
              // Contr�le Mot de passe
              if (record.getField("US_PASSW").toString().trim().equals(Base64Coder.encodeString(Base64Coder.encodeString(mdp)).trim())) {
                Etablissement etb = null;
                if (record.isPresentField("US_ETB") && !record.getField("US_ETB").toString().trim().equals(""))
                  etb = new Etablissement(utilisateur, record.getField("US_ETB").toString().trim());
                utilisateur.setInfos(record, etb);
                utilisateur.getAccesDB2().ajoutUnLog(utilisateur.getUS_ID(), session.getId(), "C", "Connexion " + utilisateur.getUS_LOGIN(),
                    "GestionConnexion");
                utilisateur.setMonPanier(new Panier(utilisateur));
              }
              else {
                utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
                utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_ER_MDP);
                forcerLogErreur("Probl�me de login et / ou mot de passe : " + login);
              }
            }
            else {
              forcerLogErreur("Il existe plusieurs code contacts S�rie M identiques dans le WebShop pour cette adresse Mail : " + login);
            }
          }
          // si plusieurs contacts du m�me logins sont pr�sents et autoris�s
          else if (autorisation == 2 && client == null) {
            utilisateur.setUS_LOGIN(login);
            utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_MULTIPLE_CONNEX);
          }
          // si pas autoris� ou pas pr�sent
          else {
            utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
            utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_REFUSE);
            forcerLogErreur("Probl�me de login et / ou mot de passe : " + login);
          }
        }
        // si son acc�s est celui d'un profil boutique, repr�sentant, RESP ou d'un ADMIN
        else if (accesWS > ConstantesEnvironnement.ACCES_CONSULTATION) {
          record = utilisateur.getListeDeTravail().get(0);
          // Si l'acc�s est OK
          if (utilisateur.majConnexionDB2(login, mdp)) {
            // On checke la provenance de la connexion (WS ou mobilite +)
            utilisateur.setMobilitePlus(provenance != null && provenance.equals("1"));
            // On ajoute les infos � l'utilisateur
            Etablissement etb = null;
            if (record.isPresentField("US_ETB") && !record.getField("US_ETB").toString().trim().equals(""))
              etb = new Etablissement(utilisateur, record.getField("US_ETB").toString().trim());
            
            utilisateur.setInfos(record, etb);
            utilisateur.getAccesDB2().ajoutUnLog(utilisateur.getUS_ID(), session.getId(), "C", "Connexion " + utilisateur.getUS_LOGIN(),
                "GestionConnexion");
            // On renomme les profils de type boutique
            if (accesWS == ConstantesEnvironnement.ACCES_BOUTIQUE) {
              utilisateur.setUS_LOGIN("BOUTIQUE");
              // Attribuer un temps max d'inactivit� � la session
              session.setMaxInactiveInterval(50000);
            }
          }
          // Si la saisie du profil n'est pas bonne
          else {
            utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
            utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_ER_MDP);
          }
        }
        // Si l'acc�s est un profil en attente de validation
        else if (accesWS == ConstantesEnvironnement.ACCES_AT_VALID)
          utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_AT_VALID);
      }
      // Le client existe en double dans USERW
      else if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 1) {
        utilisateur.setUS_LOGIN(login);
        utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_MULTIPLE_CONNEX);
      }
      // Si le profil n'existe pas encore dans WS
      else {
        int autorisation = controlerClientAutorise(utilisateur, login, null);
        
        // Si il est autoris� � acc�der au WS
        if (autorisation == 1 || (autorisation == 2 && client != null)) {
          utilisateur.setUS_LOGIN(login);
          // si on est dans un cas d'inscription
          if (mdpctrl != null) {
            // Si le contr�le de mot de passe est bon
            if (mdp.equals(mdpctrl)) {
              int idUser = insererNouveauClient(utilisateur, mdp, client, null);
              // si le client est bien ins�r� dans la base
              if (idUser > 0) {
                // Basculer le profil en attente de validation
                utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_AT_VALID);
                // Envoyer un mail de demande de conf
                EnvoiMail mails = new EnvoiMail(utilisateur);
                mails.envoyerUnMail(utilisateur.getUS_LOGIN(), "Web Shop: Demande de validation",
                    retournerMessageValidation(idUser, isHttps));
              }
            }
            else
              utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_ER_CONF_MDP);
          }
          else
            utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_AT_MDP);
        }
        
        // si plusieurs contacts du m�me logins sont pr�sents et autoris�s
        else if (autorisation == 2 && client == null) {
          // System.out.println("Ce profil Client est MULTIPLE dans S�rie M");
          utilisateur.setUS_LOGIN(login);
          utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_MULTIPLE_INSCRI);
        }
        // S'il n'est pas autoris�
        else {
          forcerLogMessage(
              "Ce profil n'est pas autoris� � utiliser le WebShop, veuillez contacter votre responsable c�t� WebShop :" + login);
          utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
          utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_INEXISTANT);
        }
      }
      
      return utilisateur.getUS_ACCES();
    }
    else
      return ConstantesEnvironnement.ACCES_NON_DEPLOYE;
  }
  
  /**
   * Retourne si le login pass� en param�tre est un login client autoris� � acc�der au WS
   */
  private int controlerClientAutorise(Utilisateur utilisateur, String login, String etb) {
    if (loggerSiTrue("Utilisateur � NUll", utilisateur == null))
      return 0;
    
    ArrayList<GenericRecord> liste = null;
    // AVEC PASSAGE D'ETB
    if (etb != null && !etb.trim().equals("")) {
      liste = selectSecurite(utilisateur,
          "SELECT * FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND RLETB ='" + etb
              + "' AND UPPER(RENET) = ?  FETCH FIRST 2 ROWS ONLY OPTIMIZE FOR 2 ROWS ",
          login.toUpperCase());
    }
    else {
      // METHODE SANS ETB pour LE CAS DU MULTI ETB
      liste = selectSecurite(utilisateur, "SELECT * FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
          + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
          + ".PSEMRTLM ON RENUM = RLNUMT  WHERE RWACC = 'W' AND RLCOD ='C' AND UPPER(RENET) = ?  FETCH FIRST 2 ROWS ONLY OPTIMIZE FOR 2 ROWS ",
          login.toUpperCase());
    }
    
    // Si le profil existe et est autoris� dans l'AS et si il est unique ou non
    if (liste != null && liste.size() == 1)
      return 1;
    else if (liste != null && liste.size() > 1)
      return 2;
    else
      return 0;
  }
  
  /**
   * Ins�rer un nouveau client dans la base de donn�e du Web Shop
   */
  private int insererNouveauClient(Utilisateur utilisateur, String mdp, String client, String etb) {
    int idUser = 0;
    
    if (utilisateur != null) {
      if (utilisateur.getAccesDB2() != null) {
        // V�rifier client et r�cup�rer les donn�es S�rie M propres � ce contact
        ArrayList<GenericRecord> liste1 = null;
        // Si on passe la s�lection d'un client en param�tre
        if (client != null) {
          if (client.trim().length() < 6) {
            int boucle = 6 - client.trim().length();
            String ajout = "";
            for (int i = 0; i < boucle; i++)
              ajout += "0";
            
            client = ajout + client.trim();
          }
          
          if (etb != null)
            liste1 = selectSecurite(utilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN "
                    + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND RLETB ='"
                    + etb + "' AND UPPER(RENET) = ? AND SUBSTR(RLIND, 1, 6) = '" + client + "' FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS "
                    + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
                utilisateur.getUS_LOGIN().toUpperCase());
          else
            liste1 = selectSecurite(utilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN "
                    + ConstantesEnvironnement.BIBLI_CLIENTS
                    + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND UPPER(RENET) = ? AND SUBSTR(RLIND, 1, 6) = '"
                    + client + "' FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
                utilisateur.getUS_LOGIN().toUpperCase());
          
        }
        // Contact d'un seul client unique
        else {
          if (etb != null)
            liste1 = selectSecurite(utilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN "
                    + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND RLETB ='"
                    + etb + "' AND UPPER(RENET) = ? FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
                utilisateur.getUS_LOGIN().toUpperCase());
          else
            liste1 = selectSecurite(utilisateur, "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM "
                + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND UPPER(RENET) = ? FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS "
                + ConstantesEnvironnement.CLAUSE_OPTIMIZE, utilisateur.getUS_LOGIN().toUpperCase());
        }
        
        if (liste1 != null && liste1.size() == 1) {
          if (liste1.get(0).isPresentField("RLNUMT"))
            utilisateur.setUS_SERIEM(liste1.get(0).getField("RLNUMT").toString().trim());
          if (liste1.get(0).isPresentField("RLETB"))
            utilisateur.setETB_EN_COURS(new Etablissement(utilisateur, liste1.get(0).getField("RLETB").toString().trim()));
          if (liste1.get(0).isPresentField("RWIN1"))
            utilisateur.setIsAutoriseVueStockDispo(liste1.get(0).getField("RWIN1").toString().trim().equals("1"));
          if (liste1.get(0).isPresentField("RWIN2"))
            utilisateur.setIsAutoriseVuePrix(liste1.get(0).getField("RWIN2").toString().trim().equals("1"));
          if (liste1.get(0).isPresentField("RWIN3"))
            utilisateur.setIsAutoriseCommande(liste1.get(0).getField("RWIN3").toString().trim().equals("1"));
          if (liste1.get(0).isPresentField("RWIN4"))
            utilisateur.setIsAutoriseVueCommandesSociete(liste1.get(0).getField("RWIN4").toString().trim().equals("1"));
          if (liste1.get(0).isPresentField("RWIN5"))
            utilisateur.setIsAutoriseDevis(liste1.get(0).getField("RWIN5").toString().trim().equals("1"));
          
          liste1 = null;
          // Insertion du contact dans la base DB2 du Web Shop
          if (utilisateur.getAccesDB2()
              .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".USERW "
                  + "(US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,US_DATC,US_DATM) VALUES " + "('" + utilisateur.getUS_LOGIN() + "'" + ",'"
                  + Base64Coder.encodeString(Base64Coder.encodeString(mdp)) + "'" + ",'" + ConstantesEnvironnement.ACCES_AT_VALID + "'"
                  + ",'" + utilisateur.getUS_SERIEM() + "'" + ",'" + utilisateur.getETB_EN_COURS().getCodeETB() + "'" + ",'"
                  + Outils.recupererDateCouranteInt() + "'" + ",'" + Outils.recupererDateCouranteInt() + "')", this.getClass()) == 1) {
            liste1 = utilisateur.getAccesDB2().select("SELECT US_ID FROM " + ConstantesEnvironnement.BIBLI_WS
                + ".USERW WHERE UPPER(US_LOGIN) = '" + utilisateur.getUS_LOGIN().toUpperCase() + "' ", this.getClass());
            if (liste1 != null && liste1.size() == 1) {
              try {
                idUser = Integer.parseInt(liste1.get(0).getField("US_ID").toString());
              }
              catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        }
        else
          System.out.println("Erreur select");
      }
    }
    
    return idUser;
  }
  
  /**
   * Retourner le contenu du message de demande de validation
   */
  private String retournerMessageValidation(int idUser, boolean isHttps) {
    String retour = "";
    
    // Contruction de la chaine protocole en fonction du mode en cours
    String protocole = "http://";
    if (isHttps) {
      protocole = "https://";
    }
    
    if (idUser != 0) {
      retour += "<table id='confCommande'>";
      retour +=
          "<tr><td class='paddingHorizontal'></td><td class='h2Mail'>Confirmation d'inscription au Web Shop</td><td class='paddingHorizontal'></td></tr>";
      retour += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
      retour += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
      retour += "<tr>	 " + "<td class='paddingHorizontal'></td>" + "<td class='messageDeConf'>"
          + "Afin de finaliser votre acc�s client, Merci de valider votre profil en suivant ce lien: " + "<a href='" + protocole
          + ConstantesEnvironnement.ADRESSE_PUBLIC_SERVEUR + ":" + ConstantesEnvironnement.PORT_SERVEUR + "/"
          + ConstantesEnvironnement.DOSSIER_WS + "/connexion?validation=" + idUser * ConstantesEnvironnement.ENCODE_INT + "'>" + protocole
          + ConstantesEnvironnement.ADRESSE_PUBLIC_SERVEUR + ":" + ConstantesEnvironnement.PORT_SERVEUR + "/"
          + ConstantesEnvironnement.DOSSIER_WS + "/connexion?validation=" + idUser * ConstantesEnvironnement.ENCODE_INT + "</a>" + "</td>"
          + "<td class='paddingHorizontal'></td>" + "</tr>";
      retour += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
      retour += "</table>";
    }
    
    return retour;
  }
  
  /**
   * Retourner le contenu du message de demande de validation
   */
  private String retournerMessageInscription(Utilisateur pUtil) {
    String retour = "";
    if (pUtil == null || pUtil.getUS_LOGIN() == null || pUtil.getClient() == null) {
      return retour;
    }
    
    retour += "<table id='confCommande'>";
    retour +=
        "<tr><td class='paddingHorizontal'></td><td class='h2Mail'>Confirmation d'inscription au Web Shop</td><td class='paddingHorizontal'></td></tr>";
    retour += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
    retour += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
    retour += "<tr>	 " + "<td class='paddingHorizontal'></td>" + "<td class='messageDeConf'>" + pUtil.getCiviliteContact() + " "
        + pUtil.getNomContact() + " (" + pUtil.getUS_LOGIN() + ") appartenant � la soci�t� " + pUtil.getClient().getNomClient() + "("
        + pUtil.getClient().getNumeroClient() + "/" + pUtil.getClient().getSuffixeClient() + ") a valid� son inscription au WebShop."
        + "</td>" + "<td class='paddingHorizontal'></td>" + "</tr>";
    retour += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
    retour += "</table>";
    
    return retour;
  }
  
  /**
   * Envoyer un mail de confirmation d'inscription
   **/
  private boolean envoyerMailInscription(Utilisateur pUtil) {
    boolean isOk = true;
    String mailDestinataire = null;
    
    if (pUtil.getClient() != null) {
      if (pUtil.getClient().getMagasinClient() != null) {
        
        if (pUtil.getClient().getMagasinClient().getMailInscriptions() != null
            && !pUtil.getClient().getMagasinClient().getMailInscriptions().trim().equals("")) {
          mailDestinataire = pUtil.getClient().getMagasinClient().getMailInscriptions();
        }
        else {
          if (pUtil.getETB_EN_COURS() != null) {
            mailDestinataire = pUtil.getETB_EN_COURS().getMail_inscription();
          }
        }
      }
      else if (pUtil.getETB_EN_COURS() != null) {
        mailDestinataire = pUtil.getETB_EN_COURS().getMail_inscription();
      }
    }
    
    if (mailDestinataire != null) {
      if (mails == null) {
        mails = new EnvoiMail(pUtil);
      }
      
      mails.envoyerUnMail(mailDestinataire, "Inscription client", retournerMessageInscription(pUtil));
    }
    
    return isOk;
  }
  
  /**
   * Retourne l'�tat du profil apr�s l'avoir bascul� en CLIENT
   */
  public int validationProfil(Utilisateur utilisateur, String idUser) {
    int retour = ConstantesEnvironnement.ACCES_INEXISTANT;
    
    try {
      // On teste si l'utlisateur existe dans la base et oon r�cup�re ses acc�s
      int idDecode = Integer.parseInt(idUser) / ConstantesEnvironnement.ENCODE_INT;
      ArrayList<GenericRecord> liste =
          utilisateur.getAccesDB2().select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW WHERE US_ID = '" + idDecode
              + "' FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      // Si l'utilisateur existe
      if (liste != null && liste.size() == 1) {
        // Si l'utilisateur est bien en attente de validation
        if (Integer.parseInt(liste.get(0).getField("US_ACCES").toString()) == ConstantesEnvironnement.ACCES_AT_VALID) {
          // Mettre � jour ses droits d'acc�s
          if (utilisateur.getAccesDB2()
              .requete(
                  "UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".USERW SET US_ACCES = '" + ConstantesEnvironnement.ACCES_CLIENT
                      + "', US_DATM = '" + Outils.recupererDateCouranteInt() + "' WHERE US_ID = '" + idDecode + "'",
                  this.getClass()) == 1) {
            if (liste.get(0).isPresentField("US_LOGIN")) {
              utilisateur.setUS_LOGIN(liste.get(0).getField("US_LOGIN").toString());
            }
            
            if (liste.get(0).isPresentField("US_ETB")) {
              utilisateur.setETB_EN_COURS(ConstantesEnvironnement.LISTE_ETBS.get(0));
              
              if (liste.get(0).isPresentField("US_SERIEM")) {
                utilisateur.setUS_SERIEM(liste.get(0).getField("US_SERIEM").toString());
                
                utilisateur.majDesInfosAS400();
                
                envoyerMailInscription(utilisateur);
              }
            }
            
            retour = ConstantesEnvironnement.ACCES_VAL_PROFIL;
            utilisateur.setUS_ACCES(ConstantesEnvironnement.ACCES_PUBLIC);
          }
        }
        else
          retour = ConstantesEnvironnement.ACCES_DEJA_EXISTANT;
      }
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return retour;
  }
  
  /**
   * Permet de d�ployer les param�tres du client au sein m�me du serveur
   */
  public boolean traiterLeDeploiement(HttpServletRequest request, Utilisateur utilisateur) {
    // Tester si le profil admin est bien le bon
    if (!testerProfilAdmin(request, utilisateur))
      return false;
    // tester si les param�tres pass�s sont corrects
    if (request.getParameter("ADRESSE_PUBLIC_SERVEUR") == null
        || !ControleurZone.estUneAdresseIP(request.getParameter("ADRESSE_PUBLIC_SERVEUR")))
      return false;
    if (request.getParameter("PORT_SERVEUR") == null)
      return false;
    if (request.getParameter("ENVIRONNEMENT") == null || request.getParameter("ENVIRONNEMENT").trim().equals(""))
      return false;
    if (request.getParameter("LETTRE_ENV") == null || request.getParameter("LETTRE_ENV").trim().equals(""))
      return false;
    if (request.getParameter("BIBLI_CLIENTS") == null || request.getParameter("BIBLI_CLIENTS").trim().equals(""))
      return false;
    
    boolean isOk = false;
    
    if (utilisateur.majConnexionDB2(request.getParameter("PROFIL_ADMIN"), request.getParameter("MP_ADMIN")))
      isOk = ajouterParametres(request, utilisateur);
    
    return isOk;
  }
  
  /**
   * Insertion dans DB2 des param�tres de d�ploiement propre au client
   */
  private boolean ajouterParametres(HttpServletRequest request, Utilisateur utilisateur) {
    int resultat = -1;
    int nbInsert = 0;
    String arguments = "";
    
    String requeteBase = "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM (EN_CLE, EN_VAL, EN_TYP,EN_GEST,EN_ETB) VALUES ";
    
    // VARIABLES DE SERVEUR
    if (request.getParameter("ADRESSE_PUBLIC_SERVEUR") != null
        && ControleurZone.estUneAdresseIP(request.getParameter("ADRESSE_PUBLIC_SERVEUR"))) {
      arguments += traitementVirgule(arguments) + " ('ADRESSE_PUBLIC_SERVEUR', '"
          + request.getParameter("ADRESSE_PUBLIC_SERVEUR").toString().trim() + "','DONNEES','1','')";
      nbInsert++;
    }
    if (request.getParameter("PORT_SERVEUR") != null) {
      arguments += traitementVirgule(arguments) + " ('PORT_SERVEUR', '" + request.getParameter("PORT_SERVEUR").toString().trim()
          + "','DONNEES','1','')";
      nbInsert++;
    }
    if (request.getParameter("ENVIRONNEMENT") != null && !request.getParameter("ENVIRONNEMENT").trim().equals("")) {
      arguments += traitementVirgule(arguments) + " ('ENVIRONNEMENT', '"
          + request.getParameter("ENVIRONNEMENT").toUpperCase().toString().trim() + "','MAJUSCULE','1','')";
      nbInsert++;
    }
    if (request.getParameter("LETTRE_ENV") != null && !request.getParameter("LETTRE_ENV").trim().equals("")) {
      arguments += traitementVirgule(arguments) + " ('LETTRE_ENV', '" + request.getParameter("LETTRE_ENV").toUpperCase().toString().trim()
          + "','MAJUSCULE','1','')";
      nbInsert++;
    }
    if (request.getParameter("BIBLI_CLIENTS") != null || request.getParameter("BIBLI_CLIENTS").trim().equals("")) {
      arguments += traitementVirgule(arguments) + " ('BIBLI_CLIENTS', '"
          + request.getParameter("BIBLI_CLIENTS").toUpperCase().toString().trim() + "','MAJUSCULE','1','')";
      nbInsert++;
    }
    
    arguments += traitementVirgule(arguments) + " ('MAIL_IS_MODE_TEST', '1','DONNEES','1','')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MAIL_TESTS', '" + ConstantesEnvironnement.MAIL_TESTS + "','DONNEES','1','')";
    nbInsert++;
    
    // VARIABLES D'ETB
    arguments += traitementVirgule(arguments) + " ('LIMIT_LISTE', '15','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('LIMIT_REQUETE_ARTICLE', '150','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('OK_ENVOIS_MAILS', '1','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MAIL_SMTP_HOST', 'smtp.resolution-informatique.com','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MAIL_SMTP_PORT', '587','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MAIL_PROFIL', '" + ConstantesEnvironnement.MAIL_WEBMASTER + "','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MAIL_MP', '','PASSWORD','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MODE_STOCKS', '0','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('MAIL_CONTACT', '','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('IS_MONO_MAGASIN', '0','DONNEES','1','*TP')";
    nbInsert++;
    arguments += traitementVirgule(arguments) + " ('VISUSTOCKS', '0','DONNEES','1','*TP')";
    nbInsert++;
    
    System.out.println("requete deploiement: " + requeteBase + arguments);
    
    resultat = utilisateur.getAccesDB2().requete(requeteBase + arguments, this.getClass());
    
    System.out.println("resultat INSERTION DEPLOIEMENT: " + resultat + " + nbInsert: " + nbInsert);
    
    return nbInsert == resultat;
  }
  
  /**
   * Teste si virgule n�cessaire ou non
   */
  private String traitementVirgule(String arguments) {
    if (!arguments.equals(""))
      return ",";
    else
      return " ";
  }
  
  /**
   * Permet de tester si le profil Admin correspond bien � celui qui est attendu lors du d�ploiement
   */
  private boolean testerProfilAdmin(HttpServletRequest request, Utilisateur utilisateur) {
    return true;
  }
  
  /**
   * Envoyer un mail avec un ID de chgmt de mp si le profil est correct
   */
  public int changementMotPasseProfil(Utilisateur utilisateur, String profil, boolean isHttps) {
    int retour = ConstantesEnvironnement.ACCES_INEXISTANT;
    
    if (utilisateur == null || profil == null || profil.trim().equals(""))
      return retour;
    
    ArrayList<GenericRecord> liste = selectSecurite(utilisateur, "SELECT US_ID, US_ACCES FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".USERW WHERE UPPER(US_LOGIN) = ? " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, profil.toUpperCase());
    // L'utilisateur existe
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("US_ACCES")) {
        try {
          // Si c'est un admin
          if (Integer.parseInt(liste.get(0).getField("US_ACCES").toString()) > ConstantesEnvironnement.ACCES_CLIENT)
            retour = ConstantesEnvironnement.ACCES_NV_MP_ADMIN;
          else {
            int idUser = Integer.parseInt(liste.get(0).getField("US_ID").toString());
            retour = ConstantesEnvironnement.ACCES_DE_NV_MP;
            // if(MarbreEnvironnement.OK_ENVOIS_MAILS)
            // {
            if (mails == null)
              mails = new EnvoiMail(utilisateur);
            
            // Contruction de la chaine protocole en fonction du mode en cours
            String protocole = "http://";
            if (isHttps) {
              protocole = "https://";
            }
            
            mails.envoyerUnMail(profil, "Web Shop",
                "Vous avez souhait� changer de mot de passe dans le Web Shop. Merci de suivre ce lien afin de valider cette demande:"
                    + "<a href='" + protocole + ConstantesEnvironnement.ADRESSE_PUBLIC_SERVEUR + ":" + ConstantesEnvironnement.PORT_SERVEUR
                    + "/" + ConstantesEnvironnement.DOSSIER_WS + "/connexion?saisieNVMP=" + idUser * ConstantesEnvironnement.ENCODE_INT
                    + "'>Changer de mot de passe</a>");
            // }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    
    return retour;
  }
  
  /**
   * V�rifie l'utilisateur et l'ID de modification de MP avant de modifier le mot de passe
   */
  public int validerChgtMP(HttpServletRequest request) {
    int retour = ConstantesEnvironnement.ACCES_ER_MDP;
    if (request == null || request.getParameter("UtilnvMP") == null || request.getParameter("UtilnvMP").equals(""))
      return retour;
    
    ArrayList<GenericRecord> liste = null;
    int idChgt = 0;
    
    try {
      idChgt = Integer.parseInt(request.getParameter("UtilnvMP").toString()) / ConstantesEnvironnement.ENCODE_INT;
    }
    catch (Exception e) {
      return retour;
    }
    
    // on teste si l'ID de changement de mot de passe correspond � l'utilisateur saisi
    if (request.getSession().getAttribute("utilisateur") != null && controlerLaSaisie(request.getParameter("loginUser").toString().trim(),
        request.getParameter("passUser").toString().trim(), request.getParameter("passUserCtrl").toString().trim()))
      // Mots de passes identiques
      if (request.getParameter("passUser").toString().equals(request.getParameter("passUserCtrl").toString())) {
        // V�rifier que l'uilisateur est bien l'ID de changement d�crypt�
        liste = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getAccesDB2()
            .select("SELECT US_ID FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW WHERE US_ID = '" + idChgt
                + "' AND UPPER(US_LOGIN) = '" + request.getParameter("loginUser").toString().trim().toUpperCase() + "' ", this.getClass());
        if (liste != null && liste.size() == 1)
          if (((Utilisateur) request.getSession().getAttribute("utilisateur")).getAccesDB2()
              .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".USERW " + " SET US_PASSW = '"
                  + Base64Coder.encodeString(Base64Coder.encodeString(request.getParameter("passUser").toString().trim()))
                  + "', US_DATM = '" + Outils.recupererDateCouranteInt() + "'  WHERE US_ID ='" + idChgt + "' ", this.getClass()) == 1)
            return ConstantesEnvironnement.ACCES_VAL_NV_MP;
      }
    
    return retour;
  }
  
  /**
   * SELECT sp�cifique de s�curit� -> SI IL MARCHE BIEN JE LE REPAND DANS QUERYMANAGER
   */
  public ArrayList<GenericRecord> selectSecurite(Utilisateur utilisateur, String requete, String parametre) {
    if (utilisateur == null)
      return null;
    Connection dataBase = utilisateur.getMaconnectionAS();
    if (dataBase == null)
      return null;
    
    loggerSiDEBUG("[selectSecurite] requete: " + requete);
    
    final ArrayList<GenericRecord> listeRecord = new ArrayList<GenericRecord>();
    int i = 0;
    GenericRecord record = null;
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Pr�pare le statement
      select = dataBase.prepareStatement(requete/*, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY*/);
      select.setString(1, parametre);
      rs = select.executeQuery();
      if (rs != null) {
        if (IS_EN_DEBUG_SPEC || ConstantesDebug.DEBUG_SQL)
          forcerLogMessage("selectSecurite() REQUETE : " + requete);
        while (rs.next()) {
          record = new GenericRecord();
          for (i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            record.setField(rs.getMetaData().getColumnName(i), rs.getObject(i), rs.getMetaData().getPrecision(i),
                rs.getMetaData().getScale(i)); // , rs.getMetaData().getColumnClassName(i));
          }
          listeRecord.add(record);
        }
        rs.close();
      }
      
      select.close();
    }
    catch (SQLException exc) {
      forcerLogErreur("++++++ PROBLEME selectSecurite() REQUETE : " + requete);
    }
    
    return listeRecord;
  }
  
}
