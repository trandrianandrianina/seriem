
package ri.seriem.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.outils.EnvoiMail;

/**
 * Permet de g�rer l'ensemble des requetes rentrant sur le serveur afin de g�rer la s�curit� du serveur
 */
public class GestionRequetes {
  // liste d'adresses IP suspectes tentant de se connceter au serveur
  private static HashMap<String, AdresseSuspecte> listeAdressesSuspectes = null;
  private static Utilisateur utilisateur = null;
  private static EnvoiMail gestionMail = null;
  
  /**
   * Teste une requ�te rentrante sur certains crit�res de s�curit�
   */
  public static boolean testerRequete(HttpServletRequest request) {
    
    if (request == null || request.getRemoteAddr() == null)
      return false;
    
    if (ConstantesEnvironnement.MODE_DEBUG) {
      System.out.println(System.getProperty("line.separator"));
      System.out.println("+++++++++++++++++++++++++++++++++++ TEST REQUETE CLIENT +++++++++++++++++++++++++++++++++++");
      System.out.println(System.getProperty("line.separator"));
      System.out.println(" request.getRemoteAddr() -> " + request.getRemoteAddr());
      System.out.println(" request.getRequestedSessionId() -> " + request.getRequestedSessionId());
      System.out.println(" request.isRequestedSessionIdFromURL() -> " + request.isRequestedSessionIdFromURL());
      System.out.println(" request.isRequestedSessionIdValid() -> " + request.isRequestedSessionIdValid());
      System.out.println(" request.getRemoteHost() -> " + request.getRemoteHost());
      System.out.println(" request.getRemotePort() -> " + request.getRemotePort());
      System.out.println(" request.getMethod() -> " + request.getMethod());
      System.out.println(" request.getRequestURL() -> " + request.getRequestURL());
      System.out.println(" request.getQueryString() -> " + request.getQueryString());
      System.out.println(" request.isSecure() -> " + request.isSecure());
      System.out.println(" request.getProtocol() -> " + request.getProtocol());
      System.out.println(System.getProperty("line.separator"));
      System.out.println(" session.getId() -> " + request.getSession().getId());
      System.out.println(" session.isNew() -> " + request.getSession().isNew());
      System.out.println(" session.getLastAccessedTime() -> " + request.getSession().getLastAccessedTime());
      System.out.println(" session.getCreationTime() -> " + request.getSession().getCreationTime());
      System.out.println(" session.getMaxInactiveInterval() -> " + request.getSession().getMaxInactiveInterval());
    }
    
    // On bloque la requ�te si l'adresse IP fait partie de la liste noire des adresses
    if (SessionListener.listeIpBloquees != null && SessionListener.listeIpBloquees.containsKey(request.getRemoteAddr())) {
      if (ConstantesEnvironnement.MODE_DEBUG)
        System.out.println("L'adresse IP " + request.getRemoteAddr() + " est d�j� bloqu�e ");
      // On ajoute une tentative par requ�te sur cette adresse
      SessionListener.listeIpBloquees.put(request.getRemoteAddr(), SessionListener.listeIpBloquees.get(request.getRemoteAddr()) + 1);
      System.out.println("On rajoute une tentative pour " + request.getRemoteAddr() + " dans listeIpBloquee "
          + SessionListener.listeIpBloquees.get(request.getRemoteAddr()));
      // Si on d�passe le quota on bloque en BDD
      if (SessionListener.listeIpBloquees.get(request.getRemoteAddr()) > ConstantesEnvironnement.NB_SESSIONS_MAX_PAR_IP) {
        // Si l'adresse est d�j� bloqu�e de mani�re d�finitive
        if (SessionListener.listeIpBloquees.get(request.getRemoteAddr()) >= ConstantesEnvironnement.NB_SESSIONS_MAX)
          System.out.println("Cette adresse " + request.getRemoteAddr() + " est d�j� bloqu�e en BDD");
        // Sinon il faut la bloquer
        else {
          System.out.println("+++++++++++++++++++ Cette adresse " + request.getRemoteAddr() + " est � bloquer en BDD ++++++++++++++++++++");
          // RENTRER EN BASE l'ADRESSE IP
          if (utilisateur == null)
            utilisateur = new Utilisateur();
          if (utilisateur != null && utilisateur.getAccesDB2() != null) {
            ArrayList<GenericRecord> liste = null;
            liste = utilisateur.getAccesDB2().select(
                "SELECT BL_ADR FROM " + ConstantesEnvironnement.BIBLI_WS + ".BLOCAGE_IP WHERE BL_ADR = '" + request.getRemoteAddr() + "' ",
                GestionRequetes.class);
            if (liste == null || liste.size() == 0)
              if (utilisateur.getAccesDB2()
                  .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".BLOCAGE_IP  (BL_ADR,BL_NB) VALUES ('"
                      + request.getRemoteAddr() + "'," + ConstantesEnvironnement.NB_SESSIONS_MAX + ")") > 0) {
                SessionListener.listeIpBloquees.put(request.getRemoteAddr(), ConstantesEnvironnement.NB_SESSIONS_MAX);
                System.out.println("OK cette adresse " + request.getRemoteAddr() + " est bloqu�e en BDD");
                if (gestionMail == null)
                  gestionMail = new EnvoiMail(utilisateur);
                gestionMail.envoyerUnMail(ConstantesEnvironnement.MAIL_WEBMASTER, "Bloquage adresse IP",
                    "Cette adresse IP " + request.getRemoteAddr() + " a �t� bloqu�e en base de donn�es");
              }
              else
                System.out.println("PB pour ins�rer cette adresse " + request.getRemoteAddr() + " en BDD");
          }
        }
      }
      
      return false;
    }
    
    // On va contr�ler la fr�quence des requ�tes sur la base de leur provenance
    if (listeAdressesSuspectes == null)
      listeAdressesSuspectes = new HashMap<String, AdresseSuspecte>();
    
    // Si l'adresse est d�j� rep�r�e
    if (listeAdressesSuspectes.containsKey(request.getRemoteAddr())) {
      // if(MarbreEnvironnement.MODE_DEBUG)
      System.out.println(" ----------- listeAdressesSuspectes contient d�j� cette adresse: " + request.getRemoteAddr());
      // Si l'adresse est consid�r�e comme moisie
      if (listeAdressesSuspectes.get(request.getRemoteAddr()).isMoisie())
        return false;
      else
        return listeAdressesSuspectes.get(request.getRemoteAddr()).ajouterUneTentativeRequete(request);
    }
    // Si nouvelle adresse
    else {
      if (ConstantesEnvironnement.MODE_DEBUG)
        System.out.println(" ----------- listeAdressesSuspectes NE contient PAS cette adresse: " + request.getRemoteAddr()
            + ". On la rajoute � listeAdressesSuspectes");
      listeAdressesSuspectes.put(request.getRemoteAddr(),
          new AdresseSuspecte(1, request.getSession().getLastAccessedTime(), request.getRemoteAddr()));
    }
    
    // On vire les adresses IP qui ne sont plus suspectes
    nettoyerLesAdressesNonSuspectes(request);
    
    return true;
  }
  
  /**
   * On retire les adresses suspectes qui ne le sont plus
   */
  private static void nettoyerLesAdressesNonSuspectes(HttpServletRequest request) {
    if (request == null)
      return;
    
    try {
      for (Map.Entry<String, AdresseSuspecte> indice : listeAdressesSuspectes.entrySet()) {
        // On ne prend que les adresses IP qui ne concernent pas cette requ�te
        if (!indice.getKey().equals(request.getRemoteAddr())) {
          // System.out.println(indice.getKey() + " : " + indice.getValue().getNbTentatives());
          if ((request.getSession().getLastAccessedTime() - indice.getValue().getPremiereTentative()) > ConstantesEnvironnement.TEST_PAR_IP
              && !indice.getValue().isMoisie() && indice.getValue().getNbTentatives() < ConstantesEnvironnement.NB_SESSIONS_MAX_PAR_IP) {
            // Je vire les requ�tes suspectes de cette adresse
            indice.getValue().getRequetes().clear();
            // On vire l'adresse de la liste
            listeAdressesSuspectes.remove(indice.getKey());
          }
        }
      }
    }
    catch (Exception e) {
      // TODO Bloc catch g�n�r� automatiquement
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des adresses IP suspectes
   */
  public static void afficherListeAdressessuspectes() {
    System.out.println(System.getProperty("line.separator"));
    System.out.println("--- liste des adresses IP suspectes: ");
    
    if (listeAdressesSuspectes == null || listeAdressesSuspectes.size() == 0)
      System.out.println("--- Cette liste d'adresse suspectes est vide");
    else
      for (Map.Entry<String, AdresseSuspecte> indice : listeAdressesSuspectes.entrySet())
        System.out.println("--------- Adresse " + indice.getKey() + " -> " + indice.getValue().getNbTentatives());
  }
  
  // ++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++++//
  
  public static HashMap<String, AdresseSuspecte> getListeAdressesSuspectes() {
    return listeAdressesSuspectes;
  }
  
  public static void setListeAdressesSuspectes(HashMap<String, AdresseSuspecte> listeAdressesSuspectes) {
    GestionRequetes.listeAdressesSuspectes = listeAdressesSuspectes;
  }
  
}
