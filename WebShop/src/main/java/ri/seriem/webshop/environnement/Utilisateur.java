
package ri.seriem.webshop.environnement;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.dao.gvx.database.PgvmsecmManager;
import ri.seriem.libas400.database.record.ExtendRecord;
import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesAffichage;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.controleurs.GestionAccueil;
import ri.seriem.webshop.controleurs.GestionCatalogue;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.metier.Client;
import ri.seriem.webshop.metier.Etablissement;
import ri.seriem.webshop.metier.Filtres;
import ri.seriem.webshop.metier.Magasin;
import ri.seriem.webshop.metier.Panier;
import ri.seriem.webshop.metier.PanierIntuitif;
import ri.seriem.webshop.modeles.AccesBDDWS;
import ri.seriem.webshop.modeles.DuplicDevis;
import ri.seriem.webshop.modeles.GestionCommandes;
import ri.seriem.webshop.modeles.GestionStock;
import ri.seriem.webshop.modeles.GestionTarif;
import ri.seriem.webshop.outils.GeneratePDF;
import ri.seriem.webshop.outils.Traduction;

public class Utilisateur extends MesLogs {
  private AccesBDDWS accesDB2 = null;
  private String sessionEnCours = null;
  private int US_ID = ConstantesEnvironnement.PROFIL_INVITE;
  private String pageEnCours = "";
  
  private String US_LOGIN = null;
  private int US_ACCES = ConstantesEnvironnement.ACCES_PUBLIC;
  private Etablissement ETB_EN_COURS = null;
  // code devise de l'utilisateur
  private String US_DEV = null;
  // code repr�sentant
  private String US_REPR = null;
  // civilite de ce contact
  private String US_SERIEM = null;
  private String civiliteContact = null;
  private String nomContact = null;
  private String telContact = null;
  // client associ�
  private Client client = null;
  private boolean isAutoriseVueStockDispo = true;
  private boolean isAutoriseVuePrix = true;
  private boolean isAutoriseDevis = true;
  private boolean isAutoriseCommande = true;
  private boolean isAutoriseVueCommandesSociete = true;
  private int tentativesConnexions = 0;
  private String bibli = null;
  // liste temporaire pour bosser avec des listes de GenericRecord
  private ArrayList<GenericRecord> listeDeTravail = null;
  private GenericRecord recordTravail = null;
  private ArrayList<GenericRecord> paniersIntuitifsActifs = null;
  // AHAHAHHHHH ENFIN UNE LISTE COMMUNE POUR TOUTES LES PAGINATIONS
  private ArrayList<GenericRecord> derniereListePagination = null;
  private ArrayList<GenericRecord> derniereListePartiellePagination = null;
  // Les listes pour pagination et m�morisation des articles
  private ArrayList<GenericRecord> derniereListeArticle = null;
  private ArrayList<GenericRecord> derniereListeArticleMoteur = null;
  private ArrayList<GenericRecord> derniereListeArticleFavoris = null;
  private ArrayList<GenericRecord> derniereListeArticleHistorique = null;
  private ArrayList<GenericRecord> derniereListeArticleConsultee = null;
  private ArrayList<GenericRecord> listeAccesBackOffice = null;
  private ArrayList<GenericRecord> derniereListePanierIntuitif = null;
  private ArrayList<GenericRecord> listePromotionsActives = null;
  
  private String dernierTypeBon = null;
  private SystemManager systeme = null;
  private Connection maconnectionAS = null;
  private PgvmsecmManager mesSecurites = null;
  
  private String derniereRequete = null;
  private GestionTarif gestionTarif = null;
  private GestionStock gestionStock = null;
  private GestionCommandes gestionCommandes = null;
  private DuplicDevis duplicationDevis = null;
  private String derniereExpression = null;
  private String dernierGroupeArticles = null;
  private String derniereFamilleArticles = null;
  private String dernierFiltreFournisseur = null;
  private Filtres filtres = null;
  private String derniersFavoris = null;
  private String dernierHistorique = null;
  private String derniersConsultes = null;
  private String dernierPaniersIntuitifs = null;
  private String indiceDebut = "0";
  private String parametresEnCours = null;
  private boolean isMobilitePlus = false;
  
  private HashMap<String, String[]> derniereListeFournisseur = null;
  private boolean visuStock = false;
  
  private GeneratePDF gestionPDF = null;
  
  // language de l'utilisateur
  private String language = null;
  // Classe de traduction de tous les textes
  private Traduction traduction = null;
  // Mes pieds de page dynamiques
  private ArrayList<GenericRecord> mesFooters = null;
  // Mes menus dynamiques
  private ArrayList<GenericRecord> mesMenus = null;
  
  // Variables n�cessaires au panier
  private Panier monPanier = null;
  private Magasin magasinSiege = null;
  
  private String typeDonneesEncours = "E";
  
  private PanierIntuitif panierIntuitif = null;
  
  /**
   * Constructeur d'un utilisateur
   */
  public Utilisateur(String session) {
    // logs
    classeQuiLogge = "Utilisateur";
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_CONNEXIONS;
    
    sessionEnCours = session;
    // si la r�cup�ration des variables d'environnements ne fonctionne pas correctement
    if (!majConnexionDB2(ConstantesEnvironnement.PROFIL_AS_ANONYME,
        Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_ANONYME))))
      US_ACCES = ConstantesEnvironnement.ACCES_PAS_DB2;
    else {
      if (!majVariablesEnvironnement())
        US_ACCES = ConstantesEnvironnement.ACCES_NON_DEPLOYE;
        
      // On va tout de suite �viter de se faire chier avec le multi dans les cas du MONO -> on attribue le seul ETB �
      // l'utilisateur.
      if (ConstantesEnvironnement.LISTE_ETBS != null && ConstantesEnvironnement.LISTE_ETBS.size() == 1) {
        ETB_EN_COURS = ConstantesEnvironnement.LISTE_ETBS.get(0);
      }
      
      language = System.getProperty("user.language");
      traduction = new Traduction(this);
      traduction.chargerListeEquivalence(language);
      // Charger mes footers
      mesFooters = retournerFooters(language);
      
      filtres = new Filtres(this);
      filtres.init();
      
      // si on doit revenir sur une page par d�faut c'est l'accueil bien s�r
      parametresEnCours = "accueil";
      
      if (US_ACCES >= ConstantesEnvironnement.ACCES_PUBLIC) {
        // on charge automatiquement les promotions
        listePromotionsActives = GestionAccueil.recupererPromotions(this);
      }
    }
  }
  
  /**
   * Constructeur anonyme
   *
   */
  public Utilisateur() {
    classeQuiLogge = "Utilisateur";
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_CONNEXIONS;
    if (majConnexionDB2(ConstantesEnvironnement.PROFIL_AS_ANONYME,
        Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_ANONYME))))
      majVariablesEnvironnement();
      
    // On va tout de suite �viter de se faire chier avec le multi dans les cas du MONO -> on attribue le seul ETB �
    // l'utilisateur.
    if (ConstantesEnvironnement.LISTE_ETBS != null && ConstantesEnvironnement.LISTE_ETBS.size() == 1) {
      ETB_EN_COURS = ConstantesEnvironnement.LISTE_ETBS.get(0);
      loggerSiDEBUG("MONO ETB: " + ETB_EN_COURS.getCodeETB() + " -> " + ETB_EN_COURS.getLibelleETB());
    }
    // JUSTE EN ATTENDANT DE REPARER LES VUES A MULTI ETB -> MAGASINS, PARTENAIRES...ETc
    // ON Y ATTRIBUE LE PREMIER ETB QUI PASSE PAR LA
    else if (ConstantesEnvironnement.LISTE_ETBS != null)
      ETB_EN_COURS = ConstantesEnvironnement.LISTE_ETBS.get(0);
  }
  
  /**
   * Permet de mettre � jour toutes les variables d'environnement � partir de DB2.
   */
  protected boolean majVariablesEnvironnement() {
    // marbreEnvironnement bien mises � jour
    if (ConstantesEnvironnement.BIBLI_CLIENTS != null && ConstantesEnvironnement.LISTE_ETBS != null)
      return true;
    else {
      // Sortir si l'acc�s DB2 est pourrave
      if (accesDB2 == null)
        return false;
      
      listeDeTravail = accesDB2.select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_CLE='LETTRE_ENV' "
          + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      // si pb requ�te ou table environm est vide de cette variable indispensable
      if (listeDeTravail == null || listeDeTravail.size() == 0)
        return false;
      
      listeDeTravail = accesDB2.select(
          "SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      if (listeDeTravail != null && listeDeTravail.size() > 0) {
        /*table remplie mais marbre vide, on affecte avec les valeurs de la table.*/
        for (int i = 0; i < listeDeTravail.size(); i++) {
          if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("ADRESSE_PUBLIC_SERVEUR"))
            ConstantesEnvironnement.ADRESSE_PUBLIC_SERVEUR = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("PORT_SERVEUR"))
            ConstantesEnvironnement.PORT_SERVEUR = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("LETTRE_ENV"))
            ConstantesEnvironnement.LETTRE_ENV = listeDeTravail.get(i).getField("EN_VAL").toString().trim().charAt(0);
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("BIBLI_CLIENTS"))
            ConstantesEnvironnement.BIBLI_CLIENTS = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MAIL_IS_MODE_TEST"))
            ConstantesEnvironnement.MAIL_IS_MODE_TEST = listeDeTravail.get(i).getField("EN_VAL").toString().trim().equals("1");
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MAIL_TESTS"))
            ConstantesEnvironnement.MAIL_TESTS = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MAIL_MASTER_SMTP"))
            ConstantesEnvironnement.MAIL_MASTER_SMTP = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MAIL_MASTER_PORT_SMTP")) {
            try {
              ConstantesEnvironnement.MAIL_MASTER_PORT_SMTP =
                  Integer.parseInt(listeDeTravail.get(i).getField("EN_VAL").toString().trim());
            }
            catch (NumberFormatException e) {
              e.printStackTrace();
            }
          }
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MAIL_MASTER_PROFIL"))
            ConstantesEnvironnement.MAIL_MASTER_PROFIL = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MAIL_MASTER_MP"))
            ConstantesEnvironnement.MAIL_MASTER_MP = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("TEMPS_SESSION_INACTIVE")) {
            try {
              ConstantesEnvironnement.TEMPS_SESSION_INACTIVE =
                  Integer.parseInt(listeDeTravail.get(i).getField("EN_VAL").toString().trim());
            }
            catch (NumberFormatException e) {
              e.printStackTrace();
            }
          }
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("ZONE_CONDITIONNEMENT"))
            ConstantesEnvironnement.ZONE_CONDITIONNEMENT = listeDeTravail.get(i).getField("EN_VAL").toString().trim();
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("MODE_DEBUG"))
            ConstantesEnvironnement.MODE_DEBUG = listeDeTravail.get(i).getField("EN_VAL").toString().trim().equals("1");
          
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("IS_MODE_SSL"))
            ConstantesEnvironnement.IS_MODE_SSL = listeDeTravail.get(i).getField("EN_VAL").toString().trim().equals("1");
          else if (listeDeTravail.get(i).getField("EN_CLE").toString().trim().equals("IS_MODE_LANGUAGES"))
            ConstantesEnvironnement.IS_MODE_LANGUAGES = listeDeTravail.get(i).getField("EN_VAL").toString().trim().equals("1");
        }
        
        if (ConstantesEnvironnement.LETTRE_ENV == ' ') {
          return false;
        }
        String requete = "SELECT ENVCLI FROM " + ConstantesEnvironnement.BIBLI_SYSTEME + "." + ConstantesEnvironnement.TABLE_ENVIRONNEMENT
            + " WHERE ENVVER = '" + ConstantesEnvironnement.LETTRE_ENV + "' ";
        listeDeTravail = accesDB2.select(requete, this.getClass());
        if (listeDeTravail != null && listeDeTravail.size() == 1) {
          if (listeDeTravail.get(0).isPresentField("ENVCLI")) {
            ConstantesEnvironnement.ENVIRONNEMENT = listeDeTravail.get(0).getField("ENVCLI").toString().trim();
            System.out.println("On a recup�r� l'environnement " + ConstantesEnvironnement.ENVIRONNEMENT + " pour la lettre "
                + ConstantesEnvironnement.LETTRE_ENV);
          }
          else {
            System.out.println("Variable ENVCLI non pr�sente ou longueur liste incorrecte : " + requete);
            return false;
          }
        }
        else {
          System.out.println("SQL incorrect pour " + requete);
          return false;
        }
        
        // On va v�rifier les diff�rents �tablissements dans la DG et on met � jour l'environnement ETABLISS si
        // n�cessaire
        if (ConstantesEnvironnement.BIBLI_CLIENTS != null)
          listeDeTravail = accesDB2.select("SELECT PARETB, SUBSTR(PARZ2, 1, 24) AS LIBETB FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PGVMPARM WHERE PARTYP = 'DG' AND PARETB <> '' ");
        
        if (listeDeTravail != null && listeDeTravail.size() > 0) {
          ArrayList<GenericRecord> listeETBS = null;
          ArrayList<GenericRecord> listeMGS = null;
          
          int resultat = -1;
          for (int i = 0; i < listeDeTravail.size(); i++) {
            listeETBS = null;
            listeMGS = null;
            if (listeDeTravail.get(i).isPresentField("PARETB"))
              listeETBS = accesDB2.select("SELECT ETB_ID, ETB_LIB FROM " + ConstantesEnvironnement.BIBLI_WS + ".ETABLISS WHERE ETB_ID = '"
                  + listeDeTravail.get(i).getField("PARETB").toString().trim() + "' AND ETB_FM = '"
                  + ConstantesEnvironnement.BIBLI_CLIENTS + "' ");
            
            // Si l'�tablissement PGVMPARM n'existe pas dans la base ETABLISS
            if (listeETBS != null && listeETBS.size() == 0) {
              resultat = -1;
              loggerSiDEBUG("[majVariablesEnvironnement()] l'ETb " + listeDeTravail.get(i).getField("PARETB").toString().trim()
                  + " n'existe pas dans ETABLISS");
              
              if (listeDeTravail.get(i).isPresentField("PARETB") && listeDeTravail.get(i).isPresentField("LIBETB"))
                resultat = accesDB2
                    .requete("" + " INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ETABLISS (ETB_ID,ETB_LIB,ETB_ACTIF,ETB_FM) "
                        + " VALUES ('" + listeDeTravail.get(i).getField("PARETB").toString().trim() + "','"
                        + listeDeTravail.get(i).getField("LIBETB").toString().trim() + "','1','" + ConstantesEnvironnement.BIBLI_CLIENTS
                        + "')");
              
              loggerSiDEBUG("[majVariablesEnvironnement()] INSERT d'ETB " + listeDeTravail.get(i).getField("PARETB").toString().trim()
                  + " dans ETABLISS resultat: " + resultat);
            }
            // S'il existe d�j� dans ETABLISS
            else if (listeETBS != null && listeETBS.size() == 1) {
              loggerSiDEBUG("[majVariablesEnvironnement()] l'ETB " + listeDeTravail.get(i).getField("PARETB").toString().trim()
                  + " existe d�j� dans ETABLISS");
            }
            // Si probl�me
            else if (listeETBS == null) {
              loggerSiDEBUG("[majVariablesEnvironnement()] la liste ETBS de ETABLISS est NULL !!");
              break;
            }
            
            // TRAITEMENT DES MAGASINS DE CET ETABLISSEMENT
            if (listeDeTravail.get(i).isPresentField("PARETB"))
              listeMGS = accesDB2.select("SELECT PARETB AS ETBMG,PARIND AS CODMG,SUBSTR(PARZ2, 1, 24) AS LIBMG FROM "
                  + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM WHERE PARTYP = 'MA' AND PARETB = '"
                  + listeDeTravail.get(i).getField("PARETB").toString().trim() + "' ");
            // On a bien list� des MGS pour cet ETB
            if (listeMGS != null && listeMGS.size() > 0) {
              resultat = -1;
              ArrayList<GenericRecord> listeMgsWS = null;
              for (int j = 0; j < listeMGS.size(); j++) {
                listeMgsWS = null;
                if (listeMGS.get(j).isPresentField("ETBMG") && listeMGS.get(j).isPresentField("CODMG")) {
                  listeMgsWS = accesDB2.select("SELECT MG_COD FROM " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB = '"
                      + listeMGS.get(j).getField("ETBMG").toString().trim() + "' AND MG_COD = '"
                      + listeMGS.get(j).getField("CODMG").toString().trim() + "' ");
                  if (listeMgsWS != null && listeMgsWS.size() > 0) {
                    loggerSiDEBUG("[majVariablesEnvironnement()] le MG " + listeMgsWS.get(0).getField("MG_COD").toString().trim()
                        + " existe d�j� dans MAGASINS");
                  }
                  else if (listeMgsWS != null && listeMgsWS.size() == 0) {
                    System.out.println("[majVariablesEnvironnement()] le MG " + listeMGS.get(j).getField("CODMG").toString().trim()
                        + " n'existe pas dans MAGASINS");
                    // FAUT QU'ON FASSE NOTRE INSERT IN THE TABLE MAGASINS
                    resultat = accesDB2.requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS
                        + ".MAGASINS (MG_NAME,US_ETB,MG_COD,MG_ADR,MG_TEL,MG_MESS,MG_HOR,MG_CARTE,MG_EQUI,MG_ACTIF)  " + "VALUES ('"
                        + Gestion.traiterCaracteresSpeciauxSQL(listeMGS.get(j).getField("LIBMG").toString().trim()) + "','"
                        + listeMGS.get(j).getField("ETBMG").toString().trim() + "','"
                        + listeMGS.get(j).getField("CODMG").toString().trim() + "','','','','','','','1')");
                    
                    System.out.println("[majVariablesEnvironnement()] ON INSERT UN MAG "
                        + listeMGS.get(j).getField("CODMG").toString().trim() + " resultat: " + resultat);
                  }
                  else
                    loggerSiDEBUG("[majVariablesEnvironnement()] la listeMgsWS est NULL !!");
                }
              }
            }
          }
          
          // Maintenant on met � jour la liste d'�tablissements de l'environnement � partir d'ETABLISS
          ConstantesEnvironnement.LISTE_ETBS = new ArrayList<Etablissement>();
          System.out.println("-- LISTE ETABLISSEMENTS: ");
          for (int i = 0; i < listeDeTravail.size(); i++) {
            listeDeTravail = accesDB2.select("SELECT ETB_ID, ETB_LIB FROM " + ConstantesEnvironnement.BIBLI_WS
                + ".ETABLISS WHERE ETB_ACTIF = '1' AND ETB_FM = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' ");
            if (listeDeTravail.get(i).isPresentField("ETB_ID") && listeDeTravail.get(i).isPresentField("ETB_LIB"))
              ConstantesEnvironnement.LISTE_ETBS.add(new Etablissement(this, listeDeTravail.get(i).getField("ETB_ID").toString().trim(),
                  listeDeTravail.get(i).getField("ETB_LIB").toString().trim()));
            
            System.out.println("---- ETB: " + listeDeTravail.get(i).getField("ETB_ID").toString().trim() + " -> "
                + listeDeTravail.get(i).getField("ETB_LIB").toString().trim());
          }
          System.out.println("--");
        }
        else {
          forcerLogErreur("[majVariablesEnvironnement()] PB de r�cup�ration des �tablissements dans PGVMPARM ");
          return false;
        }
        
        // on met � jour google analytics
        listeDeTravail = accesDB2.select("SELECT GO_ANALY FROM " + ConstantesEnvironnement.BIBLI_WS + ".GOOGLE", this.getClass());
        
        if (listeDeTravail != null && listeDeTravail.size() == 1)
          if (listeDeTravail.get(0).isPresentField("GO_ANALY")
              && !listeDeTravail.get(0).getField("GO_ANALY").toString().trim().equals(""))
            ConstantesEnvironnement.GOOGLE_ANALYTICS = listeDeTravail.get(0).getField("GO_ANALY").toString();
          
        // VARIABLES D'AFFICHAGE
        majVariablesAffichage();
        
        // VARIABLES DEBUG
        majVariablesDEBUG(true);
        
        return true;
      }
      else
        return false;
    }
    
  }
  
  /**
   * mise � jour des variables de debug
   */
  public boolean majVariablesDEBUG(boolean onForce) {
    boolean retour = false;
    
    if (onForce) {
      listeDeTravail = accesDB2.select(
          "SELECT DEB_CLE,DEB_VALEUR FROM " + ConstantesEnvironnement.BIBLI_WS + ".DEBUG " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
      if (listeDeTravail != null && listeDeTravail.size() > 0) {
        // On parcourt toutes les variables de la table DEBUG
        for (int i = 0; i < listeDeTravail.size(); i++) {
          if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_ARTICLES"))
            ConstantesDebug.DEBUG_ARTICLES = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_BACK_OFFICE"))
            ConstantesDebug.DEBUG_BACK_OFFICE = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_CLIENT"))
            ConstantesDebug.DEBUG_CLIENT = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_CONNEXIONS"))
            ConstantesDebug.DEBUG_CONNEXIONS = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_CONTEXT"))
            ConstantesDebug.DEBUG_CONTEXT = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_MAGASINS"))
            ConstantesDebug.DEBUG_MAGASINS = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_MES_DONNEES"))
            ConstantesDebug.DEBUG_MES_DONNEES = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_PANIER"))
            ConstantesDebug.DEBUG_PANIER = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_PARTENAIRES"))
            ConstantesDebug.DEBUG_PARTENAIRES = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("DEBUG_SQL"))
            ConstantesDebug.DEBUG_SQL = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
          else if (listeDeTravail.get(i).getField("DEB_CLE").toString().trim().equals("MODE_LOG4J"))
            ConstantesDebug.MODE_LOG4J = (listeDeTravail.get(i).getField("DEB_VALEUR").toString().trim().equals("1"));
        }
        // Fodra ferssa mieu plus tard
        retour = true;
      }
    }
    
    return retour;
  }
  
  /**
   * Permet de mettre � jour toutes les variables d'affichage � partir de DB2
   */
  protected boolean majVariablesAffichage() {
    if (accesDB2 == null)
      return false;
    
    ArrayList<GenericRecord> liste = null;
    liste = accesDB2.select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".AFFICHAGE ", this.getClass());
    if (liste != null && liste.size() > 0) {
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_CODE_A"))
          ConstantesAffichage.AFFICHE_CODE_A = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_CODE_B"))
          ConstantesAffichage.AFFICHE_CODE_B = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_CODE_C"))
          ConstantesAffichage.AFFICHE_CODE_C = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_CODE_D"))
          ConstantesAffichage.AFFICHE_CODE_D = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_CODE_R"))
          ConstantesAffichage.AFFICHE_CODE_R = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_CODE_N"))
          ConstantesAffichage.AFFICHE_CODE_N = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHE_PAS_CODE"))
          ConstantesAffichage.AFFICHE_PAS_CODE = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_SANS_STK"))
          ConstantesAffichage.AFFICHE_SANS_STK = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ETAT_1"))
          ConstantesAffichage.AFFICHAGE_STATUT_DESACTIVE = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ETAT_2"))
          ConstantesAffichage.AFFICHAGE_STATUT_EPUISE = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ETAT_3"))
          ConstantesAffichage.AFFICHAGE_STATUT_SYSV = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ETAT_4"))
          ConstantesAffichage.AFFICHAGE_STATUT_SYSV2 = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ETAT_5"))
          ConstantesAffichage.AFFICHAGE_STATUT_PREFIN = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ETAT_6"))
          ConstantesAffichage.AFFICHAGE_STATUT_FIN = liste.get(i).getField("EN_VAL").toString().trim();
      }
      
      return true;
    }
    else
      return false;
  }
  
  /**
   * Mise � jour de la connexion AS400/DB2
   */
  public boolean majConnexionDB2(String login, String mdp) {
    accesDB2 = null;
    if (login == null || mdp == null) {
      login = ConstantesEnvironnement.PROFIL_AS_CLIENTS;
      mdp = Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_CLIENTS));
      System.out.println("login null en entree -> CLIENT :" + login);
    }
    
    systeme = new SystemManager(ConstantesEnvironnement.ADRESSE_AS400, login, mdp, true);
    
    if (systeme != null && systeme.getSystem() != null) {
      maconnectionAS = systeme.getdatabase().getConnection();
      if (maconnectionAS != null) {
        accesDB2 = new AccesBDDWS(maconnectionAS, this);
      }
      else
        forcerLogErreur("[majConnexionDB2] ECHEC Connexion DB2 !!");
    }
    else
      forcerLogErreur("[majConnexionDB2] ECHEC Systeme, probl�me avec le login ou le mot de passe :" + login);
    
    return accesDB2 != null;
  }
  
  /**
   * On d�connecte tous les jobs sur l'AS400 (QZRCSRVS & QZDASOINIT)
   */
  public void deconnecterAS400() {
    if (systeme != null) {
      systeme.disconnect();
      systeme = null;
      loggerSiDEBUG("[deconnecterAS400] Systeme d�connect�");
    }
  }
  
  /**
   * Mise � jour de toutes les informations propres � l'utilisateur
   */
  public void setInfos(GenericRecord pRecord, Etablissement pEtb) {
    if (pRecord == null) {
      return;
    }
    
    try {
      if (pRecord.isPresentField("US_ID") && pRecord.getField("US_ID") != null) {
        US_ID = Integer.parseInt(pRecord.getField("US_ID").toString().trim());
      }
      if (pRecord.isPresentField("US_ACCES") && pRecord.getField("US_ACCES") != null) {
        US_ACCES = Integer.parseInt(pRecord.getField("US_ACCES").toString().trim());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    if (pRecord.isPresentField("US_LOGIN") && pRecord.getField("US_LOGIN") != null) {
      US_LOGIN = pRecord.getField("US_LOGIN").toString().trim();
    }
    // s'il s'agit d'un contact client
    if (pRecord.isPresentField("US_SERIEM") && pRecord.getField("US_SERIEM") != null) {
      US_SERIEM = pRecord.getField("US_SERIEM").toString().trim();
    }
    if (pRecord.isPresentField("RWIN1") && pRecord.getField("RWIN1") != null) {
      isAutoriseVueStockDispo = pRecord.getField("RWIN1").toString().trim().equals("1");
    }
    if (pRecord.isPresentField("RWIN2") && pRecord.getField("RWIN2") != null) {
      isAutoriseVuePrix = pRecord.getField("RWIN2").toString().trim().equals("1");
    }
    if (pRecord.isPresentField("RWIN3") && pRecord.getField("RWIN3") != null) {
      isAutoriseCommande = pRecord.getField("RWIN3").toString().trim().equals("1");
    }
    if (pRecord.isPresentField("RWIN4") && pRecord.getField("RWIN4") != null) {
      isAutoriseVueCommandesSociete = pRecord.getField("RWIN4").toString().trim().equals("1");
    }
    if (pRecord.isPresentField("RWIN5") && pRecord.getField("RWIN5") != null) {
      isAutoriseDevis = pRecord.getField("RWIN5").toString().trim().equals("1");
    }
    
    if (pEtb != null)
      ETB_EN_COURS = pEtb;
    
    loggerSiDEBUG("[setInfos] US_ID: " + US_ID);
    loggerSiDEBUG("[setInfos] US_LOGIN: " + US_LOGIN);
    loggerSiDEBUG("[setInfos] US_ACCES: " + US_ACCES);
    if (ETB_EN_COURS != null)
      loggerSiDEBUG("[setInfos] US_ETB: " + ETB_EN_COURS.getCodeETB());
    loggerSiDEBUG("[setInfos] US_SERIEM: " + US_SERIEM);
    
    loggerSiDEBUG("[setInfos] voitStockDispo: " + isAutoriseVueStockDispo);
    loggerSiDEBUG("[setInfos] voitLesPrix: " + isAutoriseVuePrix);
    loggerSiDEBUG("[setInfos] peutDevis: " + isAutoriseDevis);
    loggerSiDEBUG("[setInfos] peutCommander: " + isAutoriseCommande);
    loggerSiDEBUG("[setInfos] voitCommandesSociete: " + isAutoriseVueCommandesSociete);
    
    // TRAITEMENTS
    // Si le client ne peut pas faire de devis, commander ou voir les prix on lui attribue un acc�s consultation
    // temporaire
    if (((!isAutoriseCommande && !isAutoriseDevis) || !isAutoriseVuePrix) && US_ACCES == ConstantesEnvironnement.ACCES_CLIENT) {
      US_ACCES = ConstantesEnvironnement.ACCES_CONSULTATION;
    }
    
    majDesInfosAS400();
    // on change de profil on change les menus !!
    recupererMenus();
    // On attribue les acces back office
    recupererAccesBackOffice();
    // paniers intuitifs
    
    paniersIntuitifsActifs = GestionCatalogue.recupererPanierIntuitifsActifs(this);
  }
  
  /**
   * Recup�rer les acces back office de l'utilisateur en fonction de l'acc�s de l'utilisateur
   */
  private void recupererAccesBackOffice() {
    if (accesDB2 == null) {
      return;
    }
    
    if (US_ACCES == ConstantesEnvironnement.ACCES_ADMIN_WS) {
      listeAccesBackOffice = accesDB2.select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ACCUEILBO ", this.getClass());
    }
    else if (US_ACCES > ConstantesEnvironnement.ACCES_CLIENT) {
      listeAccesBackOffice = accesDB2.select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ACCUEILBO JOIN "
          + ConstantesEnvironnement.BIBLI_WS + ".ACCESBO a ON a.AB_ID = BTN_ID WHERE AB_ACC = 30", this.getClass());
    }
    else {
      listeAccesBackOffice = null;
    }
  }
  
  /**
   * Mise � jour des informations dispos dans DB2
   */
  public void majDesInfosAS400() {
    // Mise � jour de la bibli
    
    // Finalement on file la bibli clients � tout le monde
    bibli = ConstantesEnvironnement.BIBLI_CLIENTS;
    
    // NE METTRE A JOUR QUE SI ON A UN ETB ASSOCIE ( c'est � dire en mode CLIENT ) ATTENTION IL FAUT UNE METHODE QUI
    // METTE A JOUR TOUT LE BOUZIN APRES SELECTION D'UN ETB
    if (ETB_EN_COURS != null)
      mettreAjourDonneesLieesETB(ETB_EN_COURS.getCodeETB());
    else {
      System.out.println("+++++++++ ON A UN UTILISATEUR SANS ETB ASSOCIE (PUBLIC OU AS400)");
      return;
    }
    
    // On va mettre les infos � jour propres au contact S�rie M -> contact et Client concern�
    if (US_SERIEM != null && ETB_EN_COURS != null && !US_SERIEM.equals("") && ETB_EN_COURS.getCodeETB() != null) {
      if (this.getAccesDB2() != null) {
        ArrayList<GenericRecord> liste = this.getAccesDB2()
            .select(" SELECT RENUM,RECIV,REPAC,RETEL,CLCLI,CLLIV,CLNOM " + " FROM " + bibli + ".PSEMRTEM " + " LEFT JOIN " + bibli
                + ".PSEMRTLM ON RENUM = RLNUMT " + " LEFT JOIN " + bibli + ".PGVMCLIM ON DIGITS(CLCLI)||DIGITS(CLLIV) = RLIND "
                + " WHERE RLETB = '" + ETB_EN_COURS.getCodeETB() + "' AND RLCOD = 'C' AND RLNUMT = '" + US_SERIEM + "' "
                + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
        // Si on trouve bien un profil dans la base de S�rie M/N
        if (liste != null && liste.size() == 1) {
          if (liste.get(0).isPresentField("RECIV"))
            civiliteContact = liste.get(0).getField("RECIV").toString().trim();
          if (liste.get(0).isPresentField("REPAC"))
            nomContact = liste.get(0).getField("REPAC").toString().trim();
          if (liste.get(0).isPresentField("RETEL"))
            telContact = liste.get(0).getField("RETEL").toString().trim();
          
          loggerSiDEBUG("[majDesInfosAS400] civiliteContact: " + civiliteContact);
          loggerSiDEBUG("[majDesInfosAS400] nomContact: " + nomContact);
          
          // Si on a bien un client associ� � ce contact on lui attribue
          if (liste.get(0).isPresentField("CLCLI") && liste.get(0).isPresentField("CLLIV") && liste.get(0).isPresentField("CLNOM")) {
            client = new Client(this, ETB_EN_COURS, liste.get(0).getField("CLCLI").toString().trim(),
                liste.get(0).getField("CLLIV").toString().trim());
          }
        }
      }
    }
    // RECUP DU CODE REPRESENTANT SI ON EN EST UN
    else if (ETB_EN_COURS != null && US_LOGIN != null && US_ACCES == ConstantesEnvironnement.ACCES_REPRES_WS && maconnectionAS != null
        && US_REPR == null) {
      if (mesSecurites == null)
        mesSecurites = new PgvmsecmManager(maconnectionAS);
      
      mesSecurites.setLibrary(this.getBibli());
      
      ExtendRecord record = mesSecurites.getRecordsbyUSERandETB(US_LOGIN, ETB_EN_COURS.getCodeETB());
      if (record != null && record.getField("SERPUS") != null && !record.getField("SERPUS").toString().trim().equals("")) {
        US_REPR = record.getField("SERPUS").toString().trim();
        loggerSiDEBUG("Je r�cup�re un code REPRESENTANT: " + US_REPR);
        
        ArrayList<GenericRecord> liste = this.getAccesDB2().select(
            "SELECT RPCIV,RPNOM FROM " + bibli + ".PGVMREPM " + " WHERE RPETB = '" + ETB_EN_COURS.getCodeETB() + "' AND RPREP = '"
                + US_REPR + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
            this.getClass());
        // Si on trouve bien un profil dans la base de S�rie M/N
        if (liste != null && liste.size() == 1) {
          if (liste.get(0).isPresentField("RPCIV"))
            civiliteContact = liste.get(0).getField("RPCIV").toString().trim();
          loggerSiDEBUG("[majDesInfosAS400] REPR civiliteContact: " + civiliteContact);
          if (liste.get(0).isPresentField("RPNOM"))
            nomContact = liste.get(0).getField("RPNOM").toString().trim();
          loggerSiDEBUG("[majDesInfosAS400] REPR nomContact: " + nomContact);
        }
      }
    }
    
  }
  
  /**
   * Met � jour les variables stock�es par l'utilisateur propres � la
   * recherche articles dans le catalogue
   */
  public void setInfosRechercheArticles(String expression, String groupe, String famille, String frs, String favoris, String historique,
      String consulte, String panierInt, String indicePagination) {
    if (indicePagination == null) {
      if (expression != null)
        derniereExpression = expression.trim().toUpperCase();
      else
        derniereExpression = expression;
      // On ne met pas � jour le groupe si on change de famille ou sous famille
      // if(famille==null)
      dernierGroupeArticles = groupe;
      derniereFamilleArticles = famille;
      if (frs != null && frs.equals("")) {
        frs = null;
      }
      dernierFiltreFournisseur = frs;
      derniersFavoris = favoris;
      dernierHistorique = historique;
      derniersConsultes = consulte;
      dernierPaniersIntuitifs = panierInt;
    }
  }
  
  /**
   * Controler l'�tat de la gestion de tarif et l'initialiser si n�cessaire
   */
  public boolean controleGestionTarif() {
    if (gestionTarif == null)
      gestionTarif = new GestionTarif(this.getSysteme(), ConstantesEnvironnement.LETTRE_ENV, ConstantesEnvironnement.BIBLI_CLIENTS);
    
    return gestionTarif.init();
  }
  
  /**
   * Controler l'�tat de la gestion de tarif et l'initialiser si n�cessaire
   */
  public boolean controleGestionCommandes() {
    if (gestionCommandes == null)
      gestionCommandes =
          new GestionCommandes(this.getSysteme(), ConstantesEnvironnement.LETTRE_ENV, ConstantesEnvironnement.BIBLI_CLIENTS);
    
    return gestionCommandes.init();
  }
  
  /**
   * Controler l'�tat de la gestion de tarif et l'initialiser si n�cessaire
   */
  public boolean controleDuplicationDevis() {
    if (duplicationDevis == null)
      duplicationDevis = new DuplicDevis(this.getSysteme(), ConstantesEnvironnement.LETTRE_ENV, ConstantesEnvironnement.BIBLI_CLIENTS);
    
    return duplicationDevis.init();
  }
  
  /**
   * Controler l'�tat de la gestion des stocks et l'initialiser si n�cessaire
   */
  public boolean controleGestionStock() {
    boolean isOk = false;
    if (gestionStock == null) {
      gestionStock = new GestionStock(this.getSysteme(), ConstantesEnvironnement.LETTRE_ENV, ConstantesEnvironnement.BIBLI_CLIENTS);
      isOk = gestionStock.init();
    }
    else
      isOk = true;
    
    return isOk;
  }
  
  /**
   * Faire un RAZ du panier de l'utilisateur
   */
  public boolean razPanier() {
    // On vide le panier courant
    if (getMonPanier() != null) {
      getMonPanier().videPanier(true);
    }
    // On en recr�e un autre
    this.monPanier = new Panier(this);
    
    return true;
  }
  
  /**
   * Retourner la liste des footers (bas de page) de l'utilisateur dans le language param�tr�
   */
  private ArrayList<GenericRecord> retournerFooters(String language) {
    if (accesDB2 == null)
      return null;
    
    return this.getAccesDB2().select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".FOOTER WHERE FT_LANG = '" + language + "'",
        this.getClass());
  }
  
  /**
   * Mettre � jour la liste des footers (bas de page) de l'utilisateur dans le language param�tr�
   */
  public void majFooters(String language) {
    if (language == null)
      return;
    
    mesFooters = retournerFooters(language);
  }
  
  /**
   * mettre � jour la page courante de l'utilisateur
   */
  @SuppressWarnings("unchecked")
  public void majPageEnCours(String nomPage, HttpServletRequest request) {
    pageEnCours = nomPage;
    
    if (nomPage != null && !nomPage.equals("clients") && !nomPage.equals("monPanier")) {
      parametresEnCours = nomPage;
      Enumeration<String> parametres = request.getParameterNames();
      String parametre = null;
      while (parametres.hasMoreElements()) {
        if (parametre == null)
          parametresEnCours += "?";
        else
          parametresEnCours += "&";
        
        parametre = parametres.nextElement().toString().trim();
        parametre += "=" + request.getParameter(parametre);
        
        parametresEnCours += parametre;
      }
    }
    else if (nomPage.equals("monPanier"))
      parametresEnCours = "catalogue";
  }
  
  /**
   * Recup�rer les menus en fonction de l'acc�s de l'utilisateur
   */
  public void recupererMenus() {
    if (accesDB2 == null)
      return;
    
    mesMenus =
        accesDB2.select("" + " SELECT MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES " + " FROM " + ConstantesEnvironnement.BIBLI_WS
            + ".MENU " + " WHERE (US_ACC <= " + US_ACCES + " AND " + US_ACCES + " <= MN_LIM) " + " ORDER BY MN_ORDRE ", this.getClass());
  }
  
  /**
   * R�cuperer un seul enregistrement de la liste de Record
   */
  public void recupererUnRecordTravail(ArrayList<GenericRecord> liste) {
    
    if (liste != null && liste.size() > 0)
      this.recordTravail = liste.get(0);
    else
      this.recordTravail = null;
  }
  
  /**
   * On met � jour toutes les variables d'environnement qui d�pendaient d'un choix d'�tablissement
   */
  public void mettreAjourDonneesLieesETB(String etb) {
    if (etb == null)
      return;
    
    // R�cup de la devise de la DG
    ArrayList<GenericRecord> rcd = this.getAccesDB2().select(
        "SELECT SUBSTR(PARZ2, 121, 3) AS DEV FROM " + bibli + ".PGVMPARM " + " WHERE PARETB ='" + etb + "' AND PARTYP = 'DG'",
        this.getClass());
    if (rcd != null && rcd.size() > 0)
      US_DEV = rcd.get(0).getField("DEV").toString().trim();
    
    // R�cup du magasin si�ge
    ArrayList<GenericRecord> mag = this.getAccesDB2().select("SELECT SUBSTR(PARIND, 1, 2) AS MAG " + "FROM " + bibli + ".PGVMPARM "
        + " WHERE PARETB ='" + etb + "' AND PARTOP = '0' AND PARTYP = 'MA' AND SUBSTR(PARZ3, 1, 1) <>' ' ", this.getClass());
    // S'il en existe un
    if (mag != null && mag.size() > 0) {
      if (mag.get(0).isPresentField("MAG"))
        magasinSiege = new Magasin(this, mag.get(0).getField("MAG").toString().trim());
      
      loggerSiDEBUG("[Utilisateur] mettreAjourDonneesLieesETB() magasinSiege 1 : " + magasinSiege.getCodeMagasin());
    }
    // deuxi�me tentative
    else {
      if (this.getETB_EN_COURS() != null && this.getETB_EN_COURS().getMagasin_siege() != null
          && !this.getETB_EN_COURS().getMagasin_siege().equals(""))
        mag = this.getAccesDB2()
            .select("SELECT SUBSTR(PARIND, 1, 2) AS MAG " + "FROM " + bibli + ".PGVMPARM " + " WHERE PARETB ='" + etb
                + "' AND PARTOP = '0' AND PARTYP = 'MA' AND SUBSTR(PARIND, 1, 2) ='" + this.getETB_EN_COURS().getMagasin_siege() + "' ",
                this.getClass());
      
      if (mag != null && mag.size() > 0) {
        if (mag.get(0).isPresentField("MAG"))
          magasinSiege = new Magasin(this, mag.get(0).getField("MAG").toString().trim());
        loggerSiDEBUG("[Utilisateur] mettreAjourDonneesLieesETB() magasinSiege 2 : " + magasinSiege.getCodeMagasin());
      }
      else
        forcerLogErreur("[Utilisateur] mettreAjourDonneesLieesETB() PAS DE MAGASIN SIEGE !!");
    }
    
    visuStock = ETB_EN_COURS.isVisu_auto_des_stocks();
  }
  
  // +++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++ //
  
  public GestionCommandes getGestionCommandes() {
    return gestionCommandes;
  }
  
  public void setGestionCommandes(GestionCommandes gestionCommandes) {
    this.gestionCommandes = gestionCommandes;
  }
  
  public DuplicDevis getDuplicationDevis() {
    return duplicationDevis;
  }
  
  public ArrayList<GenericRecord> getMesFooters() {
    return mesFooters;
  }
  
  public void setMesFooters(ArrayList<GenericRecord> mesFooters) {
    this.mesFooters = mesFooters;
  }
  
  public AccesBDDWS getAccesDB2() {
    return accesDB2;
  }
  
  public void setAccesDB2(AccesBDDWS accesDB2) {
    this.accesDB2 = accesDB2;
  }
  
  public String getUS_LOGIN() {
    return US_LOGIN;
  }
  
  public void setUS_LOGIN(String uS_LOGIN) {
    US_LOGIN = uS_LOGIN;
  }
  
  public int getUS_ACCES() {
    return US_ACCES;
  }
  
  public void setUS_ACCES(int uS_ACCES) {
    US_ACCES = uS_ACCES;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticle() {
    return derniereListeArticle;
  }
  
  public void setDerniereListeArticle(ArrayList<GenericRecord> derniereListeArticle) {
    this.derniereListeArticle = derniereListeArticle;
  }
  
  public String getDernierTypeBon() {
    return dernierTypeBon;
  }
  
  public void setDernierTypeBon(String dernierTypeBon) {
    this.dernierTypeBon = dernierTypeBon;
  }
  
  public int getUS_ID() {
    return US_ID;
  }
  
  public void setUS_ID(int uS_ID) {
    US_ID = uS_ID;
  }
  
  public String getUS_SERIEM() {
    return US_SERIEM;
  }
  
  public void setUS_SERIEM(String uS_SERIEM) {
    US_SERIEM = uS_SERIEM;
  }
  
  public String getUS_DEV() {
    return US_DEV;
  }
  
  public void setUS_DEV(String uS_DEV) {
    US_DEV = uS_DEV;
  }
  
  public int getTentativesConnexions() {
    return tentativesConnexions;
  }
  
  public void setTentativesConnexions(int tentativesConnexions) {
    this.tentativesConnexions = tentativesConnexions;
  }
  
  public String getNomContact() {
    return nomContact;
  }
  
  public void setNomContact(String nomContact) {
    this.nomContact = nomContact;
  }
  
  public String getBibli() {
    return bibli;
  }
  
  public void setBibli(String bibli) {
    this.bibli = bibli;
  }
  
  public SystemManager getSysteme() {
    return systeme;
  }
  
  public void setSysteme(SystemManager systeme) {
    this.systeme = systeme;
  }
  
  public Connection getMaconnectionAS() {
    return maconnectionAS;
  }
  
  public void setMaconnectionAS(Connection maconnectionAS) {
    this.maconnectionAS = maconnectionAS;
  }
  
  public String getDerniereRequete() {
    return derniereRequete;
  }
  
  public void setDerniereRequete(String derniere) {
    derniereRequete = derniere;
  }
  
  public String getDernierFiltreFournisseur() {
    return dernierFiltreFournisseur;
  }
  
  public void setDernierFiltreFournisseur(String dernierFiltreFournisseur)
  
  {
    this.dernierFiltreFournisseur = dernierFiltreFournisseur;
  }
  
  public HashMap<String, String[]> getDerniereListeFournisseur() {
    return derniereListeFournisseur;
  }
  
  public void setDerniereListeFournisseur(HashMap<String, String[]> derniereListeFournisseur) {
    this.derniereListeFournisseur = derniereListeFournisseur;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleMoteur() {
    return derniereListeArticleMoteur;
  }
  
  public void setDerniereListeArticleMoteur(ArrayList<GenericRecord> derniereListeArticleMoteur) {
    this.derniereListeArticleMoteur = derniereListeArticleMoteur;
  }
  
  public String getCiviliteContact() {
    return civiliteContact;
  }
  
  public void setCiviliteContact(String civiliteContact) {
    this.civiliteContact = civiliteContact;
  }
  
  public GestionTarif getGestionTarif() {
    return gestionTarif;
  }
  
  public GestionStock getGestionStock() {
    return gestionStock;
  }
  
  public String getDerniereExpression() {
    return derniereExpression;
  }
  
  public void setDerniereExpression(String derniereExpression) {
    this.derniereExpression = derniereExpression;
  }
  
  public String getDernierGroupeArticles() {
    return dernierGroupeArticles;
  }
  
  public void setDernierGroupeArticles(String dernierGroupeArticles) {
    this.dernierGroupeArticles = dernierGroupeArticles;
  }
  
  public String getDerniereFamilleArticles() {
    return derniereFamilleArticles;
  }
  
  public void setDerniereFamilleArticles(String derniereFamilleArticles) {
    this.derniereFamilleArticles = derniereFamilleArticles;
  }
  
  public Panier getMonPanier() {
    return monPanier;
  }
  
  public void setMonPanier(Panier monPanier) {
    this.monPanier = monPanier;
  }
  
  public String getIndiceDebut() {
    return indiceDebut;
  }
  
  public void setIndiceDebut(String indiceDebut) {
    this.indiceDebut = indiceDebut;
  }
  
  public void setGestionTarif(GestionTarif gestionTarif) {
    this.gestionTarif = gestionTarif;
  }
  
  public void setGestionStock(GestionStock gestionStock) {
    this.gestionStock = gestionStock;
  }
  
  public boolean isVisuStock() {
    return visuStock;
  }
  
  public void setVisuStock(boolean visuStock) {
    this.visuStock = visuStock;
  }
  
  public Magasin getMagasinSiege() {
    return magasinSiege;
  }
  
  public String getSessionEnCours() {
    return sessionEnCours;
  }
  
  public void setSessionEnCours(String sessionEnCours) {
    this.sessionEnCours = sessionEnCours;
  }
  
  public GeneratePDF getGestionPDF() {
    if (gestionPDF == null)
      gestionPDF = new GeneratePDF(this);
    
    return gestionPDF;
  }
  
  public void setGestionPDF(GeneratePDF gestionPDF) {
    this.gestionPDF = gestionPDF;
  }
  
  public String getLanguage() {
    return language;
  }
  
  public void setLanguage(String language) {
    this.language = language;
  }
  
  public Traduction getTraduction() {
    return traduction;
  }
  
  public void setTraduction(Traduction traduction) {
    this.traduction = traduction;
  }
  
  public String getDerniersFavoris() {
    return derniersFavoris;
  }
  
  public void setDerniersFavoris(String derniersFavoris) {
    this.derniersFavoris = derniersFavoris;
  }
  
  public String getDernierHistorique() {
    return dernierHistorique;
  }
  
  public void setDernierHistorique(String dernierHistorique) {
    this.dernierHistorique = dernierHistorique;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleFavoris() {
    return derniereListeArticleFavoris;
  }
  
  public void setDerniereListeArticleFavoris(ArrayList<GenericRecord> derniereListeArticleFavoris) {
    this.derniereListeArticleFavoris = derniereListeArticleFavoris;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleHistorique() {
    return derniereListeArticleHistorique;
  }
  
  public void setDerniereListeArticleHistorique(ArrayList<GenericRecord> derniereListeArticleHistorique) {
    this.derniereListeArticleHistorique = derniereListeArticleHistorique;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleConsultee() {
    return derniereListeArticleConsultee;
  }
  
  public void setDerniereListeArticleConsultee(ArrayList<GenericRecord> derniereListeArticleConsultee) {
    this.derniereListeArticleConsultee = derniereListeArticleConsultee;
  }
  
  public String getDerniersConsultes() {
    return derniersConsultes;
  }
  
  public void setDerniersConsultes(String derniersConsultes) {
    this.derniersConsultes = derniersConsultes;
  }
  
  public Client getClient() {
    return client;
  }
  
  public void setClient(Client client) {
    this.client = client;
  }
  
  public String getUS_REPR() {
    return US_REPR;
  }
  
  public void setUS_REPR(String uS_REPR) {
    US_REPR = uS_REPR;
  }
  
  public String getTelContact() {
    return telContact;
  }
  
  public void setTelContact(String telContact) {
    this.telContact = telContact;
  }
  
  public String getParametresEnCours() {
    return parametresEnCours;
  }
  
  public void setParametresEnCours(String parametresEnCours) {
    this.parametresEnCours = parametresEnCours;
  }
  
  public String getPageEnCours() {
    return pageEnCours;
  }
  
  public void setPageEnCours(String pageEnCours) {
    this.pageEnCours = pageEnCours;
  }
  
  public ArrayList<GenericRecord> getListeDeTravail() {
    return listeDeTravail;
  }
  
  public void setListeDeTravail(ArrayList<GenericRecord> listeDeTravail) {
    this.listeDeTravail = listeDeTravail;
  }
  
  public ArrayList<GenericRecord> getMesMenus() {
    return mesMenus;
  }
  
  public void setMesMenus(ArrayList<GenericRecord> mesMenus) {
    this.mesMenus = mesMenus;
  }
  
  public GenericRecord getRecordTravail() {
    return recordTravail;
  }
  
  public void setRecordTravail(GenericRecord recordTravail) {
    this.recordTravail = recordTravail;
  }
  
  public PgvmsecmManager getMesSecurites() {
    return mesSecurites;
  }
  
  public void setMesSecurites(PgvmsecmManager mesSecurites) {
    this.mesSecurites = mesSecurites;
  }
  
  public ArrayList<GenericRecord> getDerniereListePagination() {
    return derniereListePagination;
  }
  
  public void setDerniereListePagination(ArrayList<GenericRecord> derniereListePagination) {
    this.derniereListePagination = derniereListePagination;
  }
  
  public ArrayList<GenericRecord> getDerniereListePartiellePagination() {
    return derniereListePartiellePagination;
  }
  
  public void setDerniereListePartiellePagination(ArrayList<GenericRecord> derniereListePartiellePagination) {
    this.derniereListePartiellePagination = derniereListePartiellePagination;
  }
  
  public boolean isMobilitePlus() {
    return isMobilitePlus;
  }
  
  public void setMobilitePlus(boolean isMobilitePlus) {
    this.isMobilitePlus = isMobilitePlus;
  }
  
  public Etablissement getETB_EN_COURS() {
    return ETB_EN_COURS;
  }
  
  public void setETB_EN_COURS(Etablissement eTB_EN_COURS) {
    ETB_EN_COURS = eTB_EN_COURS;
  }
  
  public ArrayList<GenericRecord> getListeAccesBackOffice() {
    return listeAccesBackOffice;
  }
  
  public String getTypeDonneesEncours() {
    return typeDonneesEncours;
  }
  
  public void setTypeDonneesEncours(String typeDonneesEncours) {
    this.typeDonneesEncours = typeDonneesEncours;
  }
  
  /**
   * Renvoyer si l'utilisateur est autoris� � voir les stocks disponibles
   */
  public boolean isAutoriseVueStockDispo() {
    return isAutoriseVueStockDispo;
  }
  
  /**
   * Renvoyer si l'utilisateur est autoris� � voir les prix
   */
  public boolean isAutoriseVuePrix() {
    return isAutoriseVuePrix;
  }
  
  /**
   * Renvoyer si l'utilisateur est autoris� � saisir des devis
   */
  public boolean isAutoriseDevis() {
    return isAutoriseDevis;
  }
  
  /**
   * Renvoyer si l'utilisateur est autoris� � saisir des commandes
   */
  public boolean isAutoriseCommande() {
    return isAutoriseCommande;
  }
  
  /**
   * Renvoyer si l'utilisateur est autoris� � voir les commandes de l'ensemble de la soci�t�
   */
  public boolean isAutoriseVueCommandesSociete() {
    return isAutoriseVueCommandesSociete;
  }
  
  /**
   * Mettre � jour si l'utilisateur est autoris� � voir les stocks disponibles
   */
  public void setIsAutoriseVueStockDispo(boolean pVueStockDispo) {
    isAutoriseVueStockDispo = pVueStockDispo;
  }
  
  /**
   * Mettre � jour si l'utilisateur est autoris� � voir les prix
   */
  public void setIsAutoriseVuePrix(boolean pVuePrix) {
    isAutoriseVuePrix = pVuePrix;
  }
  
  /**
   * Mettre � jour si l'utilisateur est autoris� � saisir des devis
   */
  public void setIsAutoriseDevis(boolean pAutoriseDevis) {
    isAutoriseDevis = pAutoriseDevis;
  }
  
  /**
   * Mettre � jour si l'utilisateur est autoris� � saisir des commandes
   */
  public void setIsAutoriseCommande(boolean pAutoriseCommande) {
    isAutoriseCommande = pAutoriseCommande;
  }
  
  /**
   * Mettre � jour si l'utilisateur est autoris� � voir les commandes de l'ensemble de la soci�t�
   */
  public void setIsAutoriseVueCommandesSociete(boolean pVueCommandesSociete) {
    isAutoriseVueCommandesSociete = pVueCommandesSociete;
  }
  
  public Filtres getFiltres() {
    return filtres;
  }
  
  public PanierIntuitif getPanierIntuitif() {
    return panierIntuitif;
  }
  
  public void setPanierIntuitif(PanierIntuitif panierIntuitif) {
    this.panierIntuitif = panierIntuitif;
  }
  
  public ArrayList<GenericRecord> getDerniereListePanierIntuitif() {
    return derniereListePanierIntuitif;
  }
  
  public void setDerniereListePanierIntuitif(ArrayList<GenericRecord> derniereListePanierIntuitif) {
    this.derniereListePanierIntuitif = derniereListePanierIntuitif;
  }
  
  public String getDernierPaniersIntuitifs() {
    return dernierPaniersIntuitifs;
  }
  
  public void setDernierPaniersIntuitifs(String dernierPaniersIntuitifs) {
    this.dernierPaniersIntuitifs = dernierPaniersIntuitifs;
  }
  
  public ArrayList<GenericRecord> getPaniersIntuitifsActifs() {
    return paniersIntuitifsActifs;
  }
  
  public void setPaniersIntuitifsActifs(ArrayList<GenericRecord> paniersIntuitifsActifs) {
    this.paniersIntuitifsActifs = paniersIntuitifsActifs;
  }
  
  public ArrayList<GenericRecord> getListePromotionsActives() {
    return listePromotionsActives;
  }
  
}
