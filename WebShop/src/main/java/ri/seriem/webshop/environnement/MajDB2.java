
package ri.seriem.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesAffichage;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.metier.Etablissement;

public class MajDB2 extends MesLogs {
  private Utilisateur utilisateur = null;
  
  /**
   * Constructeur de MajDB2
   */
  public MajDB2(Utilisateur util) {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_CONTEXT;
    utilisateur = util;
  }
  
  /**
   * Mettre � jour toutes les tables de DB2
   */
  public boolean mettreAJourDB2() {
    // NE PAS PASSER DANS CETTE PROCEDURE SI ON EST EN DEPLOIEMENT
    if ((ConstantesEnvironnement.BIBLI_CLIENTS == null) || ConstantesEnvironnement.BIBLI_WS == null
        || ConstantesEnvironnement.LISTE_ETBS == null)
      return true;
    System.out.println("+++++++++ Mise � jour de DB2");
    // On r�cup�re les PS n�cessaires (A am�liorer dans une fucking classe � terme)
    majPS();
    // maj des adresses moisies
    majAdressesIPmoisies();
    // Ne pas mettre � jour syst�matiquement quand on d�veloppe
    loggerSiDEBUG("mettreAJourDB2() isEclipse:" + ConstantesEnvironnement.isEclipse);
    
    mettreAjourDivers();
    
    // MarbreEnvironnement.isEclipse = false;
    if (ConstantesEnvironnement.isEclipse)
      return true;
    boolean retour = false;
    if (mettreAjourCatalogue())
      retour = mettreAjourVues();
    
    System.out.println("+++++++++ Fin Mise � jour de DB2");
    return retour;
  }
  
  /**
   * Cette m�thode r�cup�re dans DB2 les adresses IP
   */
  private void majAdressesIPmoisies() {
    if (utilisateur == null || utilisateur.getAccesDB2() == null)
      return;
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT BL_ADR FROM " + ConstantesEnvironnement.BIBLI_WS + ".BLOCAGE_IP ", this.getClass());
    
    if (liste != null && liste.size() > 0) {
      if (SessionListener.listeIpBloquees == null)
        SessionListener.listeIpBloquees = new HashMap<String, Integer>();
      
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("BL_ADR")) {
          SessionListener.listeIpBloquees.put(liste.get(i).getField("BL_ADR").toString().trim(),
              Integer.valueOf(ConstantesEnvironnement.NB_SESSIONS_MAX));
          loggerSiDEBUG("[MajDB2]-[majAdressesIPmoisies]- On rajoute dans la SessionListener.listeIpBloquees l'adresse "
              + liste.get(i).getField("BL_ADR"));
        }
      }
    }
  }
  
  /**
   * Mettre � jour les vues SQL sp�cifiques au WS
   */
  private boolean mettreAjourVues() {
    System.out.println("-------------- Mise � jour des vues / indexs");
    boolean retour = false;
    if (utilisateur != null && utilisateur.getAccesDB2() != null) {
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
      // +++++++++++++++++++++++++++++++++++++++++++ INDEXS +++++++++++++++++++++++++++++++++++++++++++++ //
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
      
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(
          "SELECT INDEX_NAME,INDEX_SCHEMA FROM QSYS2.SYSINDEXES WHERE (INDEX_SCHEMA = '" + ConstantesEnvironnement.BIBLI_CLIENTS
              + "' OR INDEX_SCHEMA = '" + ConstantesEnvironnement.BIBLI_WS + "' OR INDEX_SCHEMA = '"
              + ConstantesEnvironnement.BIBLI_WS.toUpperCase() + "')  AND (INDEX_NAME LIKE 'IN_%' OR INDEX_NAME LIKE 'IND_%')",
          this.getClass());
      
      if (liste == null)
        return false;
      HashMap<String, String> procedures = new HashMap<String, String>();
      loggerSiDEBUG("On checke les indexs d�j� pr�sents");
      int result = 0;
      String requete = null;
      
      // DROPPER LES INDEXS BIBLI CLIENTS DYNAMIQUES
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("INDEX_SCHEMA") && liste.get(i).isPresentField("INDEX_NAME"))
          loggerSiDEBUG("index: " + i + " " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
              + liste.get(i).getField("INDEX_NAME").toString().trim());
        else
          loggerSiDEBUG("index: " + i);
        
        // Si l'index est statique (XWEB ou CLIENTS/IND_)
        if ((liste.get(i).isPresentField("INDEX_SCHEMA")
            && liste.get(i).getField("INDEX_SCHEMA").toString().trim().equals(ConstantesEnvironnement.BIBLI_WS))
            || (liste.get(i).isPresentField("INDEX_SCHEMA") && liste.get(i).isPresentField("INDEX_NAME")
                && liste.get(i).getField("INDEX_NAME").toString().trim().startsWith("IND_"))) {
          // Si il n'est pas encore dans la liste des indexs
          if (!procedures.containsKey(liste.get(i).getField("INDEX_NAME").toString())) {
            procedures.put(liste.get(i).getField("INDEX_NAME").toString(), "1");
            loggerSiDEBUG("  INDEX STATIQUE " + (i + 1) + " " + liste.get(i).getField("INDEX_NAME").toString());
          }
        }
        // Dynamique -> le dropper
        else if (liste.get(i).isPresentField("INDEX_SCHEMA") && liste.get(i).isPresentField("INDEX_NAME")) {
          if (ConstantesEnvironnement.INDEXS_DYNAMIQUES) {
            result = utilisateur.getAccesDB2().requete("DROP INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
                + liste.get(i).getField("INDEX_NAME").toString().trim(), this.getClass());
            loggerSiDEBUG("  Suppression INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
                + liste.get(i).getField("INDEX_NAME").toString().trim() + " -> " + result);
          }
          // Si il n'est pas encore dans la liste des indexs
          else if (!procedures.containsKey(liste.get(i).getField("INDEX_NAME").toString())) {
            procedures.put(liste.get(i).getField("INDEX_NAME").toString(), "1");
            loggerSiDEBUG("  INDEX STATIQUE " + (i + 1) + " " + liste.get(i).getField("INDEX_NAME").toString());
          }
        }
      }
      
      // ++++++++++++++++++++++++++++++++++ INDEXS STATIQUES +++++++++++++++++++++++++++++++++ //
      
      // -------------------------- INDEXS XWEB ---------------------------- //
      
      // INDEXS PROMOTIONS ++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_PRO_DTF")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_PRO_DTF ON " + ConstantesEnvironnement.BIBLI_WS
            + ".PROMOTION (PR_DATEF ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_PRO_DTF: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_PRO_DTF DEJA PRESENT");
      
      // INDEXS USERW +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_USE_LOG")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_USE_LOG ON " + ConstantesEnvironnement.BIBLI_WS
            + ".USERW (US_LOGIN ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_USE_LOG: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_USE_LOG DEJA PRESENT");
      
      // INDEXS MENUS +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_MEN_ORD")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_MEN_ORD ON " + ConstantesEnvironnement.BIBLI_WS
            + ".MENU (MN_ORDRE ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_MEN_ORD: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_MEN_ORD DEJA PRESENT");
      
      if (!procedures.containsKey("IN_MEN_ACC")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_MEN_ACC ON " + ConstantesEnvironnement.BIBLI_WS
            + ".MENU (US_ACC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_MEN_ACC: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_MEN_ACC DEJA PRESENT");
      
      // MAGASINS
      if (!procedures.containsKey("IN_MAG_COD")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_MAG_COD ON " + ConstantesEnvironnement.BIBLI_WS
            + ".MAGASINS (US_ETB ASC,MG_COD ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_MAG_COD: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_MAG_COD DEJA PRESENT");
      
      // INDEXS CATALOGUE ++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_CAT_TYP")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_CAT_TYP ON " + ConstantesEnvironnement.BIBLI_WS
            + ".CATALOGUE (CA_BIB ASC, CA_ETB ASC, CA_TYP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_CAT_TYP: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_CAT_TYP DEJA PRESENT");
      
      if (!procedures.containsKey("IN_CAT_IN")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_CAT_IN ON " + ConstantesEnvironnement.BIBLI_WS
            + ".CATALOGUE (CA_BIB ASC, CA_ETB ASC, CA_IND ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_CAT_IN: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_CAT_IN DEJA PRESENT");
      
      if (!procedures.containsKey("IN_CAT_IN2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_CAT_IN2 ON " + ConstantesEnvironnement.BIBLI_WS
            + ".CATALOGUE (CA_TYP ASC, CA_BIB ASC, CA_ETB ASC, CA_IND ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_CAT_IN2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_CAT_IN2 DEJA PRESENT");
      
      // INDEXS PANIER +++++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_PAN_MAG")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_PAN_MAG ON " + ConstantesEnvironnement.BIBLI_WS
            + ".PANIER (PA_ETA ASC, PA_US ASC, PA_MAG ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_PAN_MAG: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_PAN_MAG DEJA PRESENT");
      
      if (!procedures.containsKey("IN_PAN_SES")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_PAN_SES ON " + ConstantesEnvironnement.BIBLI_WS
            + ".PANIER (PA_ETA ASC, PA_US ASC, PA_SESS ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_PAN_SES: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_PAN_SES DEJA PRESENT");
      
      // INDEXS FOURNISS +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_FOU_CA1")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_FOU_CA1 ON " + ConstantesEnvironnement.BIBLI_WS
            + ".FOURNISS (FO_BIB ASC, FO_ETB ASC, FO_CA ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_FOU_CA1: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_FOU_CA1 DEJA PRESENT");
      
      // INDEXS FAVORIS +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_FAVO_UT")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_FAVO_UT ON " + ConstantesEnvironnement.BIBLI_WS
            + ".FAVORIS (FV_UTIL ASC, FV_ETB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_FAVO_UT: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_FAVO_UT DEJA PRESENT");
      
      // INDEXS CONSULTES +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_CONS_UT")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_CONS_UT ON " + ConstantesEnvironnement.BIBLI_WS
            + ".CONSULTES (CON_UTIL ASC, CON_ETB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_CONS_UT: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_CONS_UT DEJA PRESENT");
      
      // INDEXS DELAISMOY +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_DEL_MG")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_WS + ".IN_DEL_MG ON " + ConstantesEnvironnement.BIBLI_WS
            + ".DELAISMOY (DEL_ETB ASC, DEL_MGDP ASC, DEL_MGAR ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_DEL_MG: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_DEL_MG DEJA PRESENT");
      
      // -------------------------- INDEXS BIBLI CLIENT ---------------------------- //
      
      // INDEXS PGVMPARM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_PAR_IN")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_PAR_IN ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMPARM (PARTYP ASC, PARIND ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_PAR_IN: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_PAR_IN DEJA PRESENT");
      
      if (!procedures.containsKey("IND_PAR_TY")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_PAR_TY ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMPARM (PARTOP ASC, PARETB ASC, PARTYP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_PAR_TY: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_PAR_TY DEJA PRESENT");
      
      // INDEXS PSEMRTLM +++++++++++++++++++++++++++++++++
      
      /*if(!procedures.containsKey("IND_RTL_NU"))
      {
      	requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_RTL_NU ON " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM (RLETB ASC, RLIN2 ASC, RLCOD ASC, RLNUMT ASC) UNIT ANY";
      	result = utilisateur.getAccesDB2().requete(requete,this.getClass());
      	
      	loggerSiDEBUG("CREATE INDEX IND_RTL_NU: " + result );
      }
      else loggerSiDEBUG("INDEX IND_RTL_NU DEJA PRESENT");*/
      
      if (!procedures.containsKey("IND_RTL_N2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_RTL_N2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTLM (RLETB ASC, RLCOD ASC, RLNUMT ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_RTL_N2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_RTL_N2 DEJA PRESENT");
      
      if (!procedures.containsKey("IND_RTL_IN")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_RTL_IN ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTLM (RLCOD ASC, RLNUMT ASC, RLIND ASC) UNIT ANY ";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_RTL_IN: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_RTL_IN DEJA PRESENT");
      
      // INDEXS PSEMRTWM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_RTW_NU")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_RTW_NU ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTWM (RWACC ASC, RWNUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_RTW_NU: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_RTW_NU DEJA PRESENT");
      
      // INDEXS PSEMRTEM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_RTE_NU")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_RTE_NU ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTEM (RENET ASC, RENUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_RTE_NU: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_RTE_NU DEJA PRESENT");
      
      if (!procedures.containsKey("IND_RTE_N2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_RTE_N2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTEM (RENUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_RTE_N2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_RTE_N2 DEJA PRESENT");
      
      // INDEXS PGVMCLIM ++++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_CLI_CL")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_CLI_CL ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMCLIM (CLETB ASC, CLLIV ASC, CLCLI ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_CLI_CL: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_CLI_CL DEJA PRESENT");
      
      // INDEXS PGVMADVM (adresses livraison) ++++++++++++
      
      if (!procedures.containsKey("IND_ADV_NU")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_ADV_NU ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMADVM (AVETB ASC, AVSUF ASC, AVNUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_ADV_NU: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_ADV_NU DEJA PRESENT");
      
      // INDEXS PGVMXLIM (extensions lignes) ++++++++++++++
      
      if (!procedures.containsKey("IND_XLI_NL")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_XLI_NL ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMXLIM (XIETB ASC, XISUF ASC, XINUM ASC, XINLI ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_XLI_NL: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_XLI_NL DEJA PRESENT");
      
      // INDEXS PGVMCNAM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_CNA_AR")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_CNA_AR ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAETB ASC, CAIN9 ASC, CACOL ASC, CAFRS ASC, CAART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_CNA_AR: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_CNA_AR DEJA PRESENT");
      
      if (!procedures.containsKey("IND_CNA_FR")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_CNA_FR ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAETB ASC, CAIN9 ASC, CAART ASC, CACOL ASC, CAFRS ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_CNA_FR: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_CNA_FR DEJA PRESENT");
      
      if (!procedures.containsKey("IND_CNA_AA")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_CNA_AA ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAIN9 ASC, CAETB ASC, CACOL ASC, CAFRS ASC, CAART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_CNA_AA: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_CNA_FR DEJA PRESENT");
      
      if (!procedures.containsKey("IND_CNA_A2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_CNA_A2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAIN9 ASC, CACOL ASC, CAETB ASC, CAFRS ASC, CAART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_CNA_A2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_CNA_A2 DEJA PRESENT");
      
      // INDEXS PGVMTARM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_TAR_DA")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_TAR_DA ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMTARM (ATETB ASC, ATART ASC, ATDAP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_TAR_DA: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_TAR_DA DEJA PRESENT");
        
      // INDEXS PGVMALAM +++++++++++++++++++++++++++++++++
      // Si on est en mode TRAITEMENT_STOCK_RESERVE on bosse avec les PGVMALAM (sp�cial CGED)
      if (ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE) {
        if (!procedures.containsKey("IND_ALA_NO")) {
          requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_ALA_NO ON " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AATOE ASC, AAART ASC, AAETB ASC, AASOS ASC, AALOS ASC, AANOS ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete, this.getClass());
          
          loggerSiDEBUG("CREATE INDEX IND_ALA_NO: " + result);
        }
        else
          loggerSiDEBUG("INDEX IND_ALA_NO DEJA PRESENT");
        
        if (!procedures.containsKey("IND_ALA_N2")) {
          requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_ALA_N2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AAMAG ASC, AATOE ASC, AAART ASC, AAETB ASC, AASOS ASC, AALOS ASC, AANOS ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete, this.getClass());
          
          loggerSiDEBUG("CREATE INDEX IND_ALA_N2: " + result);
        }
        else
          loggerSiDEBUG("INDEX IND_ALA_N2 DEJA PRESENT");
        
        if (!procedures.containsKey("IND_ALA_ET")) {
          requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_ALA_ET ON " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AAMAG ASC, AATOE ASC, AAART ASC, AAETB ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete, this.getClass());
          
          loggerSiDEBUG("CREATE INDEX IND_ALA_ET: " + result);
        }
        else
          loggerSiDEBUG("INDEX IND_ALA_ET DEJA PRESENT");
        
        if (!procedures.containsKey("IND_ALA_E2")) {
          requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_ALA_E2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AATOE ASC, AAART ASC, AAMAG ASC, AAETB ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete, this.getClass());
          
          loggerSiDEBUG("CREATE INDEX IND_ALA_E2: " + result);
        }
        else
          loggerSiDEBUG("INDEX IND_ALA_E2 DEJA PRESENT");
      }
      
      // INDEXS PGVMEBCM (ENTETES de lignes) ++++++++++++++
      if (!procedures.containsKey("IND_EBC_FP")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_EBC_FP ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM (E1ETB ASC, E1CLFS ASC, E1COD ASC, E1CLFP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_EBC_FP: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_EBC_FP DEJA PRESENT");
      
      if (!procedures.containsKey("IND_EBC_NU")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_EBC_NU ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM (E1ETB ASC, E1SUF ASC, E1NUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_EBC_NU: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_EBC_NU DEJA PRESENT");
      
      // INDEXS PGVMLBCM +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IND_LBC_TO")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_LBC_TO ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ERL ASC, L1COD ASC, L1TOP ASC) UNIT ANY ";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_LBC_TO: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_LBC_TO DEJA PRESENT");
      
      if (!procedures.containsKey("IND_LBC_T2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_LBC_T2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ERL ASC, L1COD ASC, L1ETB ASC, L1SUF ASC, L1NLI ASC, L1NUM ASC, L1TOP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_LBC_T2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_LBC_T2 DEJA PRESENT");
      
      if (!procedures.containsKey("IND_LBC_NU")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_LBC_NU ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ETB ASC, L1ERL ASC, L1COD ASC, L1SUF ASC, L1NUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_LBC_NU: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_LBC_NU DEJA PRESENT");
      
      if (!procedures.containsKey("IND_LBC_LI")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_LBC_LI ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ETB ASC, L1COD ASC, L1SUF ASC, L1NUM ASC, L1ERL ASC, L1NLI ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_LBC_LI: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_LBC_LI DEJA PRESENT");
      
      // INDEXS PGVMFRSM (fournisseurs ++++++++++++++++++++
      if (!procedures.containsKey("IND_FRS_FR")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IND_FRS_FR ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMFRSM (FRETB ASC, FRCOL ASC, FRFRS ASC) UNIT ANY ";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IND_FRS_FR: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_FRS_FR DEJA PRESENT");
      
      // ++++++++++++++++++++++++++++++++++ INDEXS DYNAMIQUES +++++++++++++++++++++++++++++++++ //
      
      // -------------------------- INDEXS BIBLI CLIENT ---------------------------- //
      
      // INDEXS PGVMARTM +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_ART_ABC")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_ABC ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_ABC: " + result);
      }
      else
        loggerSiDEBUG("INDEX IND_FRS_FR DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_AB2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_AB2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ART ASC, A1ABC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_AB2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_AB2 DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_LIB")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_LIB ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1FAM ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_LIB: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_LIB DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_LI2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_LI2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_LI2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_LI2 DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_FAM")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_FAM ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1FAM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_FAM: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_FAM DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_SFA")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_SFA ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1SFA ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_SFA: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_SFA DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_SF2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_SF2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1SFA ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_SF2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_SF2 DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_FRS")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_FRS ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1COF ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1FAM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_FRS: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_FRS DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_FR2")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_FR2 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1COF ASC, A1IN15 ASC, A1NPU ASC, A1FAM ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_FR2: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_FR2 DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_FR3")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_FR3 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1COF ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_FR3: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_FR3 DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_A1A")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_A1A ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1IN15 ASC, A1NPU ASC, A1ART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_A1A: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_A1A DEJA PRESENT");
      
      if (!procedures.containsKey("IN_ART_A12")) {
        requete = "CREATE INDEX " + ConstantesEnvironnement.BIBLI_CLIENTS + ".IN_ART_A12 ON " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1ART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete, this.getClass());
        
        loggerSiDEBUG("CREATE INDEX IN_ART_A12: " + result);
      }
      else
        loggerSiDEBUG("INDEX IN_ART_A12 DEJA PRESENT");
        
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // //
      // +++++++++++++++++++++++++++++++++++++++++++++++++ VUES ++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // //
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // //
      
      liste = utilisateur.getAccesDB2().select(
          "SELECT TABLE_NAME, TABLE_SCHEMA FROM QSYS2.SYSVIEWS WHERE TABLE_SCHEMA = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "'",
          this.getClass());
      procedures = new HashMap<String, String>();
      loggerSiDEBUG("On checke les vues pr�sentes");
      for (int i = 0; i < liste.size(); i++) {
        utilisateur.getAccesDB2().requete("DROP VIEW " + liste.get(i).getField("TABLE_SCHEMA").toString().trim() + "."
            + liste.get(i).getField("TABLE_NAME").toString().trim(), this.getClass());
        utilisateur.getAccesDB2().getMsgError();
      }
      result = 0;
      
      // VUE STOCKS
      if (!procedures.containsKey("VUE_STK_RE")) {
        if (ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE) {
          result = utilisateur.getAccesDB2().requete(
              "CREATE VIEW " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_STK_RE AS SELECT AAMAG, AAETB, AAART, SUM(AAQTE) as QTEAFF "
                  + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMALAM LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                  + ".PGVMLBCM " + "ON AAETB=L1ETB AND AACOS=L1COD AND AANOS=L1NUM AND AASOS=L1SUF AND AALOS = L1NLI "
                  + "WHERE AATOE='S' AND AACOS = 'E' AND L1ERL = 'C' AND L1TOP < 4 AND L1QTE > 0 GROUP BY AAMAG, AAETB, AAART",
              this.getClass());
          loggerSiDEBUG("CREATE VUE VUE_STK_RE: " + result);
        }
        else
          loggerSiDEBUG("Pas de gestion du reserv�. Vue VUE_STK_RE inutile");
      }
      else
        loggerSiDEBUG("VUE_STK_RE d�j� pr�sente");
      
      // VUE CNA
      if (!procedures.containsKey("VUE_CNA_9")) {
        result = utilisateur.getAccesDB2().requete("CREATE VIEW " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".VUE_CNA_9 AS SELECT MAX(CACRE) AS DATEM, CAETB, CAART, CAFRS, CACOL," + "CAREF, CARFC FROM "
            + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMCNAM WHERE CAIN9 = '1' " + "GROUP BY CAETB, CAART, CAFRS,CACOL, CAREF, CARFC",
            this.getClass());
        
        loggerSiDEBUG("CREATE VUE VUE_CNA_9: " + result);
      }
      else
        loggerSiDEBUG("VUE_CNA_9 d�j� pr�sente");
      
      // VUE TARIFS
      if (!procedures.containsKey("VUE_TARIFS")) {
        result = utilisateur.getAccesDB2()
            .requete("CREATE VIEW " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_TARIFS AS SELECT " + "ATETB, ATART, ATDAP, ATP01 "
                + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMTARM a WHERE a.ATDAP = " + "(SELECT MAX(ATDAP) FROM "
                + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMTARM b " + "WHERE a.ATETB = b.ATETB AND a.ATART = b.ATART "
                + "AND b.ATDAP <= " + "(SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) "
                + "CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) " + "CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) "
                + "FROM SYSIBM.SYSDUMMY1))", this.getClass());
        loggerSiDEBUG("CREATE VUE VUE_TARIFS: " + result);
      }
      else
        loggerSiDEBUG("VUE_TARIFS d�j� pr�sente");
      
      // VUE ARTICLES
      if (!procedures.containsKey("VUE_ART_UT")) {
        String coeffMultiple = "1";
        if (ConstantesEnvironnement.ZONE_CONDITIONNEMENT != null && ConstantesEnvironnement.ZONE_CONDITIONNEMENT.equals("A1CND"))
          coeffMultiple = "100";
        result = utilisateur.getAccesDB2()
            .requete("CREATE VIEW " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT AS SELECT A1ETB, A1ART, "
                + "A1LIB, A1ABC, A1SAI, A1FAM, A1SFA, CAREF, CARFC, A1NPU, A1UNV, A1CL1, A1CL2, A1ASB, A1IN4, "
                + "SUBSTR(PARZ2, 1, 15) as LIBCND , CASE WHEN " + "SUBSTR(A1UNV, 2, 1)='*' THEN "
                + ConstantesEnvironnement.ZONE_CONDITIONNEMENT + "*" + coeffMultiple + " WHEN "
                + ConstantesEnvironnement.ZONE_CONDITIONNEMENT + " = '0' THEN 1 " + "ELSE " + ConstantesEnvironnement.ZONE_CONDITIONNEMENT
                + " END AS CND, A1TVA, A1COF, DIGITS(A1FRS) AS A1FRS, FRNOM " + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
                + ".PGVMARTM " + "LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_CNA_9 ON CAETB=A1ETB AND "
                + " CAART=A1ART AND A1FRS=CAFRS AND A1COF=CACOL " + "LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                + ".PGVMPARM ON PARTYP='UN' AND PARIND=A1UNV " + "LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                + ".PGVMFRSM ON FRETB=A1ETB AND FRCOL=A1COF " + "AND FRFRS=A1FRS WHERE A1IN15=' ' AND FRIN5 = ' '  ", this.getClass());
        loggerSiDEBUG("CREATE VUE VUE_ART_UT: " + result);
        System.out.println(utilisateur.getAccesDB2().getMsgError());
      }
      else
        loggerSiDEBUG("VUE_ART_UT d�j� pr�sente");
      
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Mettre � jour les groupes, familles et sous familles du catalogue
   */
  private boolean mettreAjourCatalogue() {
    boolean retour = false;
    System.out.println("-------------- Mise � jour du catalogue");
    if (utilisateur != null && utilisateur.getAccesDB2() != null) {
      int result = 0;
      result = utilisateur.getAccesDB2().requete(
          "DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".FOURNISS WHERE FO_BIB = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' ",
          this.getClass());
      if (result > -1) {
        if (utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + ConstantesEnvironnement.BIBLI_CLIENTS + "' ", this.getClass()) <= -1)
          System.out.println("[mettreAjourCatalogue()] PB de suppression des catalogues");
      }
      else
        System.out.println("[mettreAjourCatalogue()] PB de suppression des fournisseurs");
      
      if (ConstantesEnvironnement.LISTE_ETBS != null && ConstantesEnvironnement.LISTE_ETBS.size() > 0) {
        for (int j = 0; j < ConstantesEnvironnement.LISTE_ETBS.size(); j++) {
          loggerSiDEBUG("[mettreAjourCatalogue()] ETB " + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB());
          
          String requete = "SELECT a.PARTYP as TYPE, a.PARIND as IND, SUBSTR(a.PARZ2, 1, 30) as LIB FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM a " + " WHERE a.PARETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND PARTYP='GR' AND a.PARTOP='0' AND a.PARIND <> ' ' "
              + " AND EXISTS (SELECT c.PARTYP FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM c WHERE c.PARETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND SUBSTR(c.PARIND, 1, 1) "
              + "=substr(a.PARIND, 1, 1) AND c.PARTYP='FA' AND SUBSTR(c.PARZ2, 138, 1)=' ' AND c.PARTOP='0' AND EXISTS(SELECT e.A1ART FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMARTM e WHERE e.A1ETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND SUBSTR(c.PARIND, 1, 3) = " + "e.A1FAM  AND e.A1IN15=' '  "
              + ConstantesAffichage.retournerFiltreSqlStatutArticle("e") + " " + ConstantesAffichage.retournerFiltreSqlVieArticle("e")
              + ")) " + "UNION SELECT b.PARTYP as TYPE, b.PARIND as IND, SUBSTR(b.PARZ2, 1, 30) as LIB FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM b WHERE b.PARETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND b.PARTYP='FA' "
              + "AND SUBSTR(b.PARZ2, 138, 1)=' ' AND b.PARTOP='0' AND b.PARIND <> ' ' AND EXISTS (SELECT d.A1ART FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMARTM d WHERE d.A1ETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND SUBSTR(b.PARIND, 1, 3) = " + "d.A1FAM  AND d.A1IN15=' ' "
              + ConstantesAffichage.retournerFiltreSqlStatutArticle("d") + " " + ConstantesAffichage.retournerFiltreSqlVieArticle("d")
              + ") " + " UNION SELECT f.PARTYP as TYPE, f.PARIND as IND, SUBSTR(f.PARZ2, 1, 30) as LIB FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM f " + "WHERE f.PARETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB()
              + "' AND f.PARTYP='SF' AND f.PARTOP='0' AND f.PARIND <> ' ' AND EXISTS (SELECT g.PARTYP FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM g WHERE " + " g.PARETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND SUBSTR(g.PARIND, 1, 3) "
              + "=substr(f.PARIND, 1, 3) AND g.PARTYP='FA' AND SUBSTR(g.PARZ2, 138, 1)=' ' AND g.PARTOP='0' AND EXISTS(SELECT h.A1ART FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMARTM h WHERE h.A1ETB = '"
              + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND SUBSTR(f.PARIND, 1, 5) = h.A1SFA AND h.A1IN15=' ' "
              + ConstantesAffichage.retournerFiltreSqlStatutArticle("h") + " " + ConstantesAffichage.retournerFiltreSqlVieArticle("h")
              + ")) " + "ORDER BY IND FOR READ ONLY";
          System.out.println("requ�te execut�e :" + requete);
          ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(requete, this.getClass());
          
          if (liste != null && liste.size() > 0) {
            result = 0;
            String condition = "";
            ArrayList<GenericRecord> listeFrs = new ArrayList<GenericRecord>();
            for (int i = 0; i < liste.size(); i++) {
              
              result = utilisateur.getAccesDB2()
                  .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".CATALOGUE (CA_BIB,CA_ETB,CA_TYP,CA_IND,CA_LIB) VALUES ('"
                      + ConstantesEnvironnement.BIBLI_CLIENTS + "','" + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "','"
                      + liste.get(i).getField(0) + "','" + liste.get(i).getField(1) + "','"
                      + (liste.get(i).getField(2).toString().replace("'", "''")) + "')", this.getClass());
              if (result > 0) {
                if (liste.get(i).getField("TYPE").toString().trim().equals("GR"))
                  condition = " SUBSTR(A1FAM, 1, 1) = '" + liste.get(i).getField("IND").toString() + "'";
                else if (liste.get(i).getField("TYPE").toString().trim().equals("FA"))
                  condition = " A1FAM = '" + liste.get(i).getField("IND").toString() + "'";
                else if (liste.get(i).getField("TYPE").toString().trim().equals("SF"))
                  condition = " A1SFA = '" + liste.get(i).getField("IND").toString() + "'";
                
                listeFrs = utilisateur.getAccesDB2()
                    .select("SELECT A1ETB, A1COF, A1FRS, FRNOM " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMARTM JOIN "
                        + ConstantesEnvironnement.BIBLI_CLIENTS
                        + ".PGVMFRSM ON A1ETB = FRETB AND A1COF = FRCOL AND A1FRS = FRFRS WHERE A1ETB = '"
                        + ConstantesEnvironnement.LISTE_ETBS.get(j).getCodeETB() + "' AND " + condition + " "
                        + ConstantesAffichage.retournerFiltreSqlStatutArticle(null) + " AND A1IN15=' ' "
                        + ConstantesAffichage.retournerFiltreSqlVieArticle(null) + " AND FRIN5 = ' '  GROUP BY A1ETB, A1COF, A1FRS, FRNOM ",
                        this.getClass());
                
                if (listeFrs != null && listeFrs.size() > 0) {
                  for (int k = 0; k < listeFrs.size(); k++) {
                    result = utilisateur.getAccesDB2()
                        .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".FOURNISS "
                            + " (FO_BIB,FO_ETB,FO_CA,FO_COL,FO_FRS,FO_NOM) " + " VALUES ('" + ConstantesEnvironnement.BIBLI_CLIENTS + "','"
                            + listeFrs.get(k).getField("A1ETB").toString() + "','" + liste.get(i).getField("IND").toString() + "','"
                            + listeFrs.get(k).getField("A1COF").toString() + "','" + listeFrs.get(k).getField("A1FRS").toString() + "','"
                            + Gestion.traiterCaracteresSpeciauxSQL(listeFrs.get(k).getField("FRNOM").toString()) + "')", this.getClass());
                    if (result <= 0)
                      System.out.println("    !!!!! PB INSERT FOURN: " + liste.get(i).getField("TYPE").toString() + " / "
                          + liste.get(i).getField("IND").toString() + " -> Fournisseur: " + listeFrs.get(k).getField("A1ETB") + " / "
                          + listeFrs.get(k).getField("A1COF") + " / " + listeFrs.get(k).getField("A1FRS") + " / "
                          + listeFrs.get(k).getField("FRNOM"));
                  }
                }
              }
              else
                System.out.println("[MajDB2] PB Insert CATALOGUE -> " + result + " -> Valeur table: " + liste.get(i).getField(0) + " -> "
                    + liste.get(i).getField(1) + " -> " + liste.get(i).getField(2));
            }
          }
        }
      }
      
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Mettre � jour des �l�ments divers li�s en g�n�ral aux versions
   */
  private boolean mettreAjourDivers() {
    System.out.println("-------------- Mise � jour divers");
    if (ConstantesEnvironnement.LISTE_ETBS == null || ConstantesEnvironnement.LISTE_ETBS.size() == 0)
      return false;
    
    boolean retour = true;
    ArrayList<GenericRecord> liste = null;
    int resultat = -1;
    
    // QUE SI ON EST EN MONO ETB
    if (ConstantesEnvironnement.LISTE_ETBS.size() == 1) {
      // On met � jour l'�tablissement dans ENVIRONM � partir du temporaire *TP 1.49 QUE SI ON EST EN MONO ETB
      liste = utilisateur.getAccesDB2().select("SELECT EN_CLE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_ETB = '*TP' ",
          this.getClass());
      
      if (liste != null && liste.size() > 0) {
        resultat = utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_ETB ='"
            + ConstantesEnvironnement.LISTE_ETBS.get(0).getCodeETB() + "' WHERE EN_ETB = '*TP' ", this.getClass());
        if (resultat < 0)
          retour = false;
        
        // on met � jour les variables metiers
        resultat = utilisateur.getAccesDB2()
            .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_GEST=2 WHERE EN_ETB <>'' ", this.getClass());
      }
      
      liste = utilisateur.getAccesDB2().select("SELECT EN_CLE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_ETB = '*TP' ",
          this.getClass());
    }
    
    // On fait ce contr�le pour initialiser la variable MAGASIN SIEGE dans tous les etablissements si elle n'existe pas
    for (Etablissement etablissement : ConstantesEnvironnement.LISTE_ETBS) {
      if (etablissement.getCodeETB() != null) {
        liste = utilisateur.getAccesDB2().select("SELECT EN_CLE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_ETB = '"
            + etablissement.getCodeETB() + "' AND EN_CLE = 'MAGASIN_SIEGE' ", this.getClass());
        // Si la variable n'est pas en bdd
        if (liste != null && liste.size() == 0) {
          resultat = utilisateur.getAccesDB2().requete("" + "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM "
              + " VALUES ('MAGASIN_SIEGE', '', 'DONNEES','2','" + etablissement.getCodeETB() + "','')");
          if (resultat <= 0)
            System.out.println(
                "Probleme pour ins�rer le magasin si�ge dans les varaibles d'environnement de l'etb : " + etablissement.getCodeETB());
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Mettre � jour les PS n�cessaires aux traitements du Web Shop
   */
  public boolean majPS() {
    ArrayList<GenericRecord> liste = null;
    String PS = null;
    
    if (utilisateur != null && utilisateur.getAccesDB2() != null) {
      if (loggerSiTrue("[majPS()] pb avec LISTE_ETBS ",
          ConstantesEnvironnement.LISTE_ETBS == null || ConstantesEnvironnement.LISTE_ETBS.size() == 0))
        return false;
      // PS qui contr�le si on tient compte du r�serv� dans les stocks (sp�cial CGED)
      PS = "67";
      liste = utilisateur.getAccesDB2()
          .select("SELECT SUBSTR(PARZ2, " + PS + ", 1) AS PS FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM WHERE PARETB ='"
              + ConstantesEnvironnement.LISTE_ETBS.get(0).getCodeETB() + "' AND PARTYP = 'PS' AND PARIND = ' ' "
              + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      if (liste != null && liste.size() == 1) {
        if (liste.get(0).isPresentField("PS")) {
          ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE = liste.get(0).getField("PS").toString().trim().equals("5");
          loggerSiDEBUG("[MajDB2] PS 67 � 5 " + ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE);
        }
        else
          loggerSiDEBUG("[MajDB2] PB de PS");
      }
      else
        loggerSiDEBUG("[MajDB2] PB de r�cup�ration de param�tres");
    }
    
    // On verra plus tard pour stopper les autres traitements si celui-l� d�conne
    return true;
  }
  
  /**
   * Supprime tous les indexs
   */
  public boolean supprimerIndexs() {
    // MarbreEnvironnement.isEclipse = true;
    System.out.println("-------------- Suppression des vues / indexs");
    if (ConstantesEnvironnement.isEclipse)
      return true;
    
    boolean isOk = false;
    int retourRequete = -1;
    
    // ON SUPPRIME LES INDEXS ++++++++++++++++++++++++++++++++++++++++++
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT INDEX_NAME,INDEX_SCHEMA FROM QSYS2.SYSINDEXES WHERE INDEX_SCHEMA = '"
            + ConstantesEnvironnement.BIBLI_CLIENTS + "'  AND (INDEX_NAME LIKE 'IN_%' OR INDEX_NAME LIKE 'IND_%')", this.getClass());
    
    if (liste == null)
      return isOk;
    
    for (int i = 0; i < liste.size(); i++) {
      retourRequete = utilisateur.getAccesDB2().requete("DROP INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("INDEX_NAME").toString().trim(), this.getClass());
      loggerSiDEBUG("  Suppression INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("INDEX_NAME").toString().trim() + " -> " + retourRequete);
    }
    
    // ON SUPPRIME LES VUES +++++++++++++++++++++++++++++++++++++++++++++
    liste = utilisateur.getAccesDB2().select(
        "SELECT TABLE_NAME, TABLE_SCHEMA FROM QSYS2.SYSVIEWS WHERE TABLE_SCHEMA = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "'",
        this.getClass());
    
    if (liste == null) {
      loggerSiDEBUG("[supprimerIndexs] Probl�me de requete SELECT VIEWS ");
      return isOk;
    }
    
    for (int i = 0; i < liste.size(); i++) {
      retourRequete = utilisateur.getAccesDB2().requete("DROP VIEW " + liste.get(i).getField("TABLE_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("TABLE_NAME").toString().trim(), this.getClass());
      loggerSiDEBUG("  Suppression VIEW OK " + liste.get(i).getField("TABLE_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("TABLE_NAME").toString().trim() + " -> " + retourRequete);
    }
    
    // A faire plus propre pour tenir compte des enregistrmements moisis
    if (retourRequete > -1)
      isOk = true;
    
    return isOk;
  }
  
  /********************** ACCESSEURS ******************************/
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
