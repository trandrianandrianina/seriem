
package ri.seriem.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;

public class AdresseSuspecte {
  // Nb de tentatives de connexion de cet session
  private int nbTentatives = 0;
  // Time de la premi�re tentative de requ�te en ms
  private long premiereTentative = 0;
  // Time de la derni�re tentative de requ�te en ms
  private long derniereTentative = 0;
  // Adresse IP
  private String adresseIp = null;
  // D�termine si cette adresse doit �tre bloqu�e ou non
  private boolean isMoisie = false;
  // liste des requetes tent�es par cette adresse
  private ArrayList<HttpServletRequest> requetes = null;
  
  /**
   * Constructeur
   */
  public AdresseSuspecte(int tentatives, long premiere, String ip) {
    nbTentatives = tentatives;
    premiereTentative = premiere;
    adresseIp = ip;
    requetes = new ArrayList<HttpServletRequest>();
  }
  
  /**
   * On ajoute une tentative de requ�te
   */
  public boolean ajouterUneTentativeRequete(HttpServletRequest requete) {
    if (requetes == null)
      return false;
    
    // On modifie le dernier acc�s de cette
    setDerniereTentative(requete.getSession().getLastAccessedTime());
    
    // On teste si cette adresse m�rite d'�tre r�initialis�e
    if (requetes != null && (derniereTentative - premiereTentative > ConstantesEnvironnement.TEST_PAR_IP
        && nbTentatives < ConstantesEnvironnement.NB_SESSIONS_MAX_PAR_IP)) {
      nbTentatives = 1;
      isMoisie = false;
      setPremiereTentative(derniereTentative);
      requetes.clear();
      requetes.add(requete);
      System.out.println("[AjouterUneTentativeRequete] REINIT de l'adresse: " + adresseIp + " -> " + nbTentatives);
    }
    // ou alors on l'incr�mente
    else {
      requetes.add(requete);
      nbTentatives++;
      System.out.println("[AjouterUneTentativeRequete] INCREMENT de l'adresse: " + adresseIp + " -> " + nbTentatives);
    }
    
    // Si on d�passe le plafond de requ�tes moisies pour une adresse IP
    if (nbTentatives > ConstantesEnvironnement.NB_SESSIONS_MAX_PAR_IP) {
      System.out.println("l'adresse: " + adresseIp + " D�passe le plafond NB_SESSIONS_MAX_PAR_IP "
          + ConstantesEnvironnement.NB_SESSIONS_MAX_PAR_IP + " avec " + nbTentatives + " tentatives");
      // Si on d�passe ce plafond dans un laps de temps qu'un humain ne peut atteindre
      if (derniereTentative - premiereTentative < ConstantesEnvironnement.TEST_PAR_IP) {
        // Je bloque Dans le contexte
        if (SessionListener.listeIpBloquees == null)
          SessionListener.listeIpBloquees = new HashMap<String, Integer>();
        System.out.println("Je rentre l'adresse IP " + adresseIp + " dans listeIPbloquees");
        SessionListener.listeIpBloquees.put(requete.getRemoteAddr(), 1);
      }
      
      isMoisie = true;
      // virerSessionsMoisies(adresseIp);
      
      return false;
    }
    else
      return true;
  }
  
  // +++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++ //
  
  public int getNbTentatives() {
    return nbTentatives;
  }
  
  public void setNbTentatives(int nbTentatives) {
    this.nbTentatives = nbTentatives;
  }
  
  public long getPremiereTentative() {
    return premiereTentative;
  }
  
  public void setPremiereTentative(long premiereTentative) {
    this.premiereTentative = premiereTentative;
  }
  
  public long getDerniereTentative() {
    return derniereTentative;
  }
  
  public void setDerniereTentative(long derniereTentative) {
    this.derniereTentative = derniereTentative;
  }
  
  public String getAdresseIp() {
    return adresseIp;
  }
  
  public boolean isMoisie() {
    return isMoisie;
  }
  
  public void setMoisie(boolean isMoisie) {
    this.isMoisie = isMoisie;
  }
  
  public void setAdresseIp(String adresseIp) {
    this.adresseIp = adresseIp;
  }
  
  public ArrayList<HttpServletRequest> getRequetes() {
    return requetes;
  }
  
  public void setRequetes(ArrayList<HttpServletRequest> requetes) {
    this.requetes = requetes;
  }
  
}
