
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionMetierBO extends Gestion {
  public GestionMetierBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne la liste des param�tres d'un ETB de la table ENVIRONM de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererParametresEtb(Utilisateur utilisateur, String etb) {
    if (loggerSiTrue("[recupererParametresEtb()] Utilisateur ou ETB � NUll",
        (utilisateur == null || utilisateur.getAccesDB2() == null || etb == null)))
      return null;
    // EN_GEST == 1 c'est pour les variables de type CONTEXT et EN_GEST == 2 de type ETABLISSEMENT
    return utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_VAL, EN_TYP FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_GEST > 1 AND EN_ETB = '" + etb + "' ORDER BY EN_CLE ", this.getClass());
  }
  
  /**
   * Retourne la liste des param�tres types ETB de la table ENVIRONM de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererPatternParametresEtb(Utilisateur utilisateur) {
    if (loggerSiTrue("[recupererParametresEtb()] Utilisateur ou ETB � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    // EN_GEST == 1 c'est pour les variables de type CONTEXT et EN_GEST == 2 de type ETABLISSEMENT
    return utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_TYP FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_GEST > 1 GROUP BY EN_CLE,EN_VAL,EN_TYP ORDER BY EN_CLE ", this.getClass());
  }
  
  /**
   * Mise � jour de la table ENVIRONM de XWEBSHOP
   */
  public boolean miseajourParametresEtb(Utilisateur utilisateur, HttpServletRequest request) {
    boolean retour = false;
    if (loggerSiTrue("[miseajourParametreclient()] Utilisateur ou ETB � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return retour;
    
    if (request.getParameter("etbCours") == null)
      return retour;
    
    ArrayList<GenericRecord> liste = null;
    int res = 0;
    // EN_GEST == 1 c'est pour les variables de type CONTEXT et EN_GEST == 2 de type ETABLISSEMENT
    liste = recupererParametresEtb(utilisateur, request.getParameter("etbCours"));
    
    if (liste != null && liste.size() > 0) {
      int nbModif = 0;
      
      for (int i = 0; i < liste.size(); i++) {
        if ((liste.get(i).getField("EN_TYP").toString().trim().equals("MAJUSCULE"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim().toUpperCase() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE").toString().trim() + "' AND EN_ETB = '" + request.getParameter("etbCours") + "' ",
                  this.getClass());
          if (res >= -1)
            nbModif++;
        }
        else if ((liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))) {
          res = utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
              + Base64Coder.encodeString(Base64Coder.encodeString(request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())))
                  .trim()
              + "' WHERE EN_CLE = '" + liste.get(i).getField("EN_CLE").toString().trim() + "' AND EN_ETB = '"
              + request.getParameter("etbCours") + "'  ", this.getClass());
          if (res >= -1)
            nbModif++;
        }
        else {
          res = utilisateur.getAccesDB2().requete(
              "UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE").toString().trim() + "' AND EN_ETB = '" + request.getParameter("etbCours") + "' ",
              this.getClass());
          if (res >= -1)
            nbModif++;
        }
      }
      
      if (liste.size() == nbModif)
        retour = true;
    }
    // Ils n'existent pas encore pour cet �tablissement
    else if (liste != null && liste.size() == 0) {
      liste = recupererPatternParametresEtb(utilisateur);
      if (liste != null && liste.size() > 0) {
        for (int i = 0; i < liste.size(); i++) {
          if ((liste.get(i).getField("EN_TYP").toString().trim().equals("MAJUSCULE"))) {
            if (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim()) != null) {
              res = utilisateur.getAccesDB2().requete(
                  "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM (EN_CLE,EN_VAL,EN_TYP,EN_GEST,EN_ETB,EN_LIB) VALUES ('"
                      + liste.get(i).getField("EN_CLE").toString().trim() + "','"
                      + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim().toUpperCase() + "','"
                      + liste.get(i).getField("EN_TYP").toString().trim() + "','2','" + request.getParameter("etbCours") + "', '') ",
                  this.getClass());
            }
          }
          else if ((liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))) {
            if (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim()) != null) {
              res =
                  utilisateur.getAccesDB2()
                      .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS
                          + ".ENVIRONM (EN_CLE,EN_VAL,EN_TYP,EN_GEST,EN_ETB,EN_LIB) VALUES ('"
                          + liste.get(i).getField("EN_CLE").toString().trim() + "','"
                          + Base64Coder.encodeString(
                              Base64Coder.encodeString(request.getParameter(liste.get(i).getField("EN_CLE").toString().trim()))).trim()
                          + "','" + liste.get(i).getField("EN_TYP").toString().trim() + "','2','" + request.getParameter("etbCours")
                          + "', '') ", this.getClass());
            }
          }
          else {
            res = utilisateur.getAccesDB2().requete(
                "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM (EN_CLE,EN_VAL,EN_TYP,EN_GEST,EN_ETB,EN_LIB) VALUES ('"
                    + liste.get(i).getField("EN_CLE").toString().trim() + "','"
                    + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim() + "','"
                    + liste.get(i).getField("EN_TYP").toString().trim() + "','2','" + request.getParameter("etbCours") + "', '') ",
                this.getClass());
          }
        }
        
        // TODO TEster en amont le nb de r�sultats et mettre � jour l'ETABLISSEMENT et la LISTE d'ETABLISSEMENTS
        // CORRESPONDANTE
        return true;
      }
    }
    
    return retour;
    
  }
  
}
