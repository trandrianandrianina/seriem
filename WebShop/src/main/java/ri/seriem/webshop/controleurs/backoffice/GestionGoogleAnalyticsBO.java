
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionGoogleAnalyticsBO extends Gestion {
  
  public GestionGoogleAnalyticsBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne le code de suivi pr�sent dans la table Google
   */
  
  public ArrayList<GenericRecord> recupereCodeSuivi(Utilisateur utilisateur) {
    if (loggerSiTrue("[recupereCodeSuivi()] utilisateur � NULL", utilisateur == null || utilisateur.getAccesDB2() == null))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT GO_ID,GO_ANALY FROM " + ConstantesEnvironnement.BIBLI_WS + ".GOOGLE", this.getClass());
    
    /*if(liste!=null && liste.size()>0)
    	return liste.get(0);
    else return null;*/
  }
  
  /**
   * Mise � jour de la table Google
   */
  
  public int miseaJourGoogle(Utilisateur utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return -1;
    GenericRecord rcd = new GenericRecord();
    String[] requiredfield = { "GO_ANALY" };
    rcd.setRequiredField(requiredfield);
    
    rcd.setField("GO_ANALY", request.getParameter("code_google"), true);
    
    // on delete l'enregistrement et on me recr�e
    res = utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".GOOGLE", this.getClass());
    // si le delete c'est bien pass� on ajoute le nouvel enregistrement
    
    if (res > -1) {
      res = utilisateur.getAccesDB2().requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + " .GOOGLE (GO_ANALY) VALUES" + "('"
          + traiterCaracteresSpeciauxSQL(request.getParameter("code_google")) + "')", this.getClass());
      
    }
    return res;
    
  }
}
