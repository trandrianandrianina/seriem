
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Gestion de DEBUG
 */
public class GestionDebug extends Gestion {
  public GestionDebug() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne le contenu de la table DEBUG
   * 
   */
  public ArrayList<GenericRecord> recupererVariablesDebug(Utilisateur utilisateur) {
    if (loggerSiTrue("recupererVariablesDebug() Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT DEB_CLE, DEB_VALEUR FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".DEBUG ORDER BY DEB_CLE " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
  }
  
  /**
   * On met � jour les variables de DEBUG sur la base du foraulaire pass�
   */
  public int majVariablesDebug(Utilisateur utilisateur, HttpServletRequest request) {
    int retour = -1;
    
    if (loggerSiTrue("majVariablesDebug() utilisateur ou request � NULL",
        utilisateur == null || request == null || utilisateur.getAccesDB2() == null))
      return retour;
    // Pour mettre � jour les variables on r�cup�re leur �quivalents dans la table DEBUG
    utilisateur.setListeDeTravail(recupererVariablesDebug(utilisateur));
    
    if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0) {
      retour = 0;
      for (int i = 0; i < utilisateur.getListeDeTravail().size(); i++)
        if (request.getParameter(utilisateur.getListeDeTravail().get(i).getField("DEB_CLE").toString().trim()) != null
            && request.getParameterValues(utilisateur.getListeDeTravail().get(i).getField("DEB_CLE").toString().trim())[0] != null) {
          if (retour > -1) // on ne passe � la suite que si on a pas eu d'erreur
            retour += utilisateur.getAccesDB2().requete(
                "UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".DEBUG SET DEB_VALEUR = '"
                    + request.getParameterValues(utilisateur.getListeDeTravail().get(i).getField("DEB_CLE").toString().trim())[0]
                    + "'  WHERE DEB_CLE ='" + utilisateur.getListeDeTravail().get(i).getField("DEB_CLE").toString().trim() + "' ",
                this.getClass());
        }
      // On met � jour les variables de MarbreDebug en live
      utilisateur.majVariablesDEBUG(true);
    }
    
    return retour;
  }
}
