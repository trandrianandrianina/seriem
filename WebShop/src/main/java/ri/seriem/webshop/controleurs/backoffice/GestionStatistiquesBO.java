
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionStatistiquesBO extends Gestion {
  public GestionStatistiquesBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne le CA des commandes Web
   */
  public ArrayList<GenericRecord> retournerCAWeb(Utilisateur utilisateur, String an) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || an == null)))
      return null;
    String montant = "E1THTL";
    // Si on est en franc pacifique c'est � dire � 0 d�cimales
    if (utilisateur.getETB_EN_COURS().getDecimales_client() == 0) {
      montant = "E1THTL*100";
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT SUBSTR(DIGITS(E1CRE), 4, 2) as MOIS, SUM(" + montant + ") as CA " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM " + " WHERE E1COD='E' AND E1IN18='W' AND SUBSTR(DIGITS(E1CRE), 1, 3)+1900 = " + an
            + " GROUP BY SUBSTR(DIGITS(E1CRE), 4, 2)", this.getClass());
  }
  
  /**
   * Retourne le nombres de connexions clients
   */
  public ArrayList<GenericRecord> retournerConnexionsclients(Utilisateur utilisateur, String an) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || an == null)))
      return null;
    
    return utilisateur.getAccesDB2().select(
        "SELECT SUBSTR(DIGITS(LO_DATE), 7, 2) as MOIS, COUNT(*) as CNX" + " FROM " + ConstantesEnvironnement.BIBLI_WS + ".LOGSW "
            + " WHERE LO_TYPE ='C' AND SUBSTR(DIGITS(LO_DATE), 4, 3)+1900 = " + an + " GROUP BY SUBSTR(DIGITS(LO_DATE), 7, 2)",
        this.getClass());
  }
  
}
