
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Etablissement;

public class Gestion extends MesLogs {
  public Gestion() {
    super();
  }
  
  /**
   * Cette m�thode retourne l'ensemble des langues utilis�es pour le WS par ce serveur
   */
  public ArrayList<GenericRecord> retournerLanguesWS(Utilisateur utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null)
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT LA_ID, LA_LIB FROM " + ConstantesEnvironnement.BIBLI_WS + ".LANGUES ORDER BY LA_ID ",
        this.getClass());
  }
  
  /**
   * Echapper les caract�res sp�ciaux
   */
  public String traiterCaracteresSpeciauxAffichage(String chaine) {
    if (chaine == null)
      return null;
    
    return StringEscapeUtils.escapeHtml4(chaine);
  }
  
  /**
   * Echapper les caract�res sp�ciaux
   */
  public static String traiterCaracteresSpeciauxSQL(String chaine) {
    if (chaine == null)
      return null;
    
    return chaine.replace("'", "''");
  }
  
  /**
   * permet d'initier une liste visible via une pagination � partir d'une liste compl�te
   */
  public static void initDerniereListePagination(Utilisateur utilisateur, ArrayList<GenericRecord> liste) {
    if (utilisateur == null) {
      return;
    }
    // on init la liste compl�te � paginer
    utilisateur.setDerniereListePagination(liste);
    // si pas de probl�me on met � jour la liste partielle
    if (utilisateur.getDerniereListePagination() != null)
      majListePartiellePagination(utilisateur, "0");
    else
      utilisateur.setDerniereListePartiellePagination(null);
  }
  
  /**
   * Met � jour la liste vivible via une pagination � partir de la liste compl�te,
   * de l'indice de d�but de lecture et de la limite d'�lements visibles dans une liste
   */
  public static void majListePartiellePagination(Utilisateur utilisateur, String indiceDeb) {
    if (utilisateur == null || indiceDeb == null) {
      return;
    }
    
    // Si la liste compl�te est null ou vide on vide la liste partielle � l'identique
    if (utilisateur.getDerniereListePagination() == null && utilisateur.getDerniereListePagination().size() == 0) {
      utilisateur.setDerniereListePartiellePagination(null);
      return;
    }
    
    // on type l'indice de de d�but r�c�ptionn�
    int indiceDebut = 0;
    try {
      indiceDebut = Integer.parseInt(indiceDeb);
    }
    catch (Exception e) {
      indiceDebut = 0;
    }
    
    // on initialise/reinit la liste partielle
    if (utilisateur.getDerniereListePartiellePagination() == null)
      utilisateur.setDerniereListePartiellePagination(new ArrayList<GenericRecord>());
    else
      utilisateur.getDerniereListePartiellePagination().clear();
    // On rajoute les �lements � s�lectionner dans la liste compl�te � l'int�rieur de la liste partielle
    for (int i = indiceDebut; i < utilisateur.getDerniereListePagination().size()
        && i < indiceDebut + utilisateur.getETB_EN_COURS().getLimit_liste(); i++)
      utilisateur.getDerniereListePartiellePagination().add(utilisateur.getDerniereListePagination().get(i));
  }
  
  /**
   * initialiser ces listes temporaires afin de ne pas r�cup�rer une pr�cendente liste
   */
  public static void initListesTemporaires(Utilisateur utilisateur) {
    if (utilisateur == null)
      return;
    
    utilisateur.setListeDeTravail(null);
    utilisateur.setRecordTravail(null);
  }
  
  /**
   * On teste si la saisie pass�e est alphanum�rique
   */
  public static boolean isAlphanumerique(String variableAtester) {
    return variableAtester.matches("\\p{Alnum}");
  }
  
  /**
   * On attribue un �tablissement � l'utilisateur
   */
  public static void attribuerUnETB(Utilisateur utilisateur, String etb) {
    if (utilisateur == null || etb == null)
      return;
    
    utilisateur.setETB_EN_COURS(new Etablissement(utilisateur, etb));
  }
  
}
