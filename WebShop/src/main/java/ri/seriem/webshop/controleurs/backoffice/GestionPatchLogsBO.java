
package ri.seriem.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.GestionFichierTexte;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;

public class GestionPatchLogsBO extends Gestion {
  
  private String versionInstallee = null;
  private GestionFichierTexte gestionFichier = null;
  
  // constantes
  private static final String VERSIONACTUELLE = "versionActuelle.txt";
  private static final String DIRVERSION = ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF" + ConstantesEnvironnement.SEPARATEUR
      + "versions" + ConstantesEnvironnement.SEPARATEUR;
  
  public GestionPatchLogsBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Recupere tous les patchs
   *
   */
  public ArrayList<GenericRecord> recuperePatchs(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT PA_ID,PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV, PA_APP FROM "
        + ConstantesEnvironnement.BIBLI_WS + ".PATCHSW order by PA_ID desc", this.getClass());
  }
  
  /**
   * liste un patch
   *
   */
  public ArrayList<GenericRecord> recupereUnPatch(Utilisateur utilisateur, int id) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT PA_ID,PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP FROM  "
        + ConstantesEnvironnement.BIBLI_WS + ".PATCHSW where PA_ID= " + id, this.getClass());
  }
  
  /**
   * ajout patch dans la table patch
   *
   */
  public int miseajourPatch(Utilisateur utilisateur, HttpServletRequest request, int id) {
    /*On verifie qu'on est dans la m�me version sql sinon pas de modification dans le patch*/
    int res = 0;
    GenericRecord rcd = new GenericRecord();
    String[] requiredfield = { "PA_CLAS", "PA_DETA", "PA_DATE", "PA_ETAT", "PA_VISI", "PA_DEV", "PA_APP" }; /*D�finition des param�tres*/
    rcd.setRequiredField(requiredfield);
    
    int paDate = (Outils.transformerDateHumaineEnSeriem(request.getParameter("date")));
    String datePa = "" + paDate;
    rcd.setField("PA_CLAS", request.getParameter("classeJava"), true);
    rcd.setField("PA_DETA", request.getParameter("detail"), true);
    rcd.setField("PA_DATE", datePa, true);
    rcd.setField("PA_ETAT", request.getParameter("etat"), true);
    rcd.setField("PA_VISI", request.getParameter("visibilite"), true);
    rcd.setField("PA_DEV", request.getParameter("dev"), true);
    rcd.setField("PA_APP", request.getParameter("application"), true);
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT PA_ID,PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_NOM,PA_APP,PA_SQL,PA_DEV FROM  "
            + ConstantesEnvironnement.BIBLI_WS + ".PATCHSW order by PA_ID desc", this.getClass());
    
    if (liste != null && liste.size() > 0) {
      if (request.getParameter("idPatch") != null) {
        String req = rcd.createSQLRequestUpdate("PATCHSW", ConstantesEnvironnement.BIBLI_WS, "PA_ID=" + (request.getParameter("idPatch")));
        res = utilisateur.getAccesDB2().requete(req, rcd, this.getClass());
      }
      else {
        scanVersion();
        
        // et je mets l'insertion du patch dans la table patchsw
        res = utilisateur.getAccesDB2()
            .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS
                + ".PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES " + "('"
                + traiterCaracteresSpeciauxSQL(request.getParameter("classeJava")) + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("detail")) + "','"
                + (Outils.transformerDateHumaineEnSeriem(request.getParameter("date"))) + "','" + versionInstallee + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("visibilite")) + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("dev")) + "','" + request.getParameter("application") + "','"
                + (request.getParameter("instSql") + "')"), this.getClass());
      }
    }
    return res;
  }
  
  /**
   * lister tous les .class du projet WS
   * @param path
   * @param allFiles
   * @param request
   * @param utilisateur
   * @return
   */
  public ArrayList<String> listerClassesJava(File path, ArrayList<String> allFiles, HttpServletRequest request, Utilisateur utilisateur)// String
                                                                                                                                        // idPatch)
  {
    
    File[] list = path.listFiles();
    if (path.isDirectory()) {
      list = path.listFiles();
      if ((list != null) && (list.length > 0)) {
        for (int i = 0; i < list.length; i++) {
          // Appel r�cursif sur les sous-r�pertoires
          listerClassesJava(list[i], allFiles, request, utilisateur);
        }
      }
    }
    else {
      String currentFilePath = path.getName();
      
      if (currentFilePath.endsWith(".java") || (currentFilePath.endsWith(".class")))
        allFiles.add(currentFilePath);
      
    }
    Collections.sort(allFiles);
    return allFiles;
  }
  
  /**
   * r�cupere la version install�e
   * @return
   */
  public boolean scanVersion() {
    boolean res = false;
    File versionActuelle = new File(DIRVERSION + VERSIONACTUELLE);
    /*si le fichier existe, on lit son contenu et on recupere le n� de version*/
    if (versionActuelle.exists()) {
      if (gestionFichier == null)
        gestionFichier = new GestionFichierTexte(versionActuelle);
      versionInstallee = gestionFichier.getContenuFichierString(true).trim();
      res = true;
    }
    return res;
  }
  
}
