
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionFooterBO extends Gestion {
  
  public final static int VARIABLES_MANQUANTES = -2;
  public final static int ERREUR_MAJ_SQL = -1;
  public final static int AUCUNE_MAJ_SQL = 0;
  public final static int OK_UPDATE = 1;
  public final static int OK_INSERT = 2;
  
  public GestionFooterBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourner la liste des footers
   */
  public ArrayList<GenericRecord> retournerListeFooters(Utilisateur pUtil) {
    if (loggerSiTrue("Utilisateur � NUll", (pUtil == null || pUtil.getAccesDB2() == null)))
      return null;
    
    return pUtil.getAccesDB2().select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".FOOTER ORDER BY FT_LANG, FT_LIB ",
        this.getClass());
  }
  
  /**
   * Retourner le d�tail d'un footer
   */
  public GenericRecord retournerUnFooter(Utilisateur pUtil, String pCode) {
    if (loggerSiTrue("Utilisateur � NUll", (pUtil == null || pUtil.getAccesDB2() == null))) {
      return null;
    }
    if (loggerSiTrue("pCode � NUll", pCode == null)) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = pUtil.getAccesDB2()
        .select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".FOOTER WHERE FT_ID = '" + pCode + "' ", this.getClass());
    
    if (liste == null || liste.size() == 0) {
      return null;
    }
    
    return liste.get(0);
  }
  
  /**
   * On met � jour un footer dans DB2
   * Si le code Footer est pr�sent c'est une mise a jour sinon un insert
   */
  public int majFooter(Utilisateur pUtil, String pCode, String pLib, String pLang, String pLien, String pTarg) {
    if (loggerSiTrue("Utilisateur � NUll", (pUtil == null || pUtil.getAccesDB2() == null))) {
      return VARIABLES_MANQUANTES;
    }
    if (loggerSiTrue("Utilisateur � NUll", (pLib == null || pLang == null || pLien == null || pTarg == null))) {
      return VARIABLES_MANQUANTES;
    }
    // Mise � jour d'un footer
    if (pCode != null) {
      return pUtil.getAccesDB2().requete("" + " UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".FOOTER " + " SET FT_LANG  = '" + pLang
          + "', FT_LIB = '" + pLib + "', FT_LIEN = '" + pLien + "', FT_TARG = '" + pTarg + "' " + " WHERE FT_ID = '" + pCode + "' ",
          this.getClass());
      
    }
    else {
      int ret = pUtil.getAccesDB2()
          .requete("" + " INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG) VALUES " + " ('"
              + pLang + "'," + " '" + pLib + "'," + " '" + pLien + "'," + " '" + pTarg + "' " + " )", this.getClass());
      
      if (ret == 1) {
        return OK_INSERT;
      }
      else
        return ret;
    }
  }
  
  /**
   * On supprime un footer sur la base de son id
   */
  public boolean supprimerFooter(Utilisateur pUtil, String pId) {
    if (loggerSiTrue("Utilisateur � NUll", (pUtil == null || pUtil.getAccesDB2() == null))) {
      return false;
    }
    
    if (loggerSiTrue("Id Footer � NULL", (pId == null))) {
      return false;
    }
    
    return (pUtil.getAccesDB2().requete("" + " DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".FOOTER WHERE FT_ID = '" + pId + "' ",
        this.getClass()) == 1);
  }
  
}
