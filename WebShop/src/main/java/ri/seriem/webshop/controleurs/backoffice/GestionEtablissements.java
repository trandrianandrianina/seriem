
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionEtablissements extends Gestion {
  public GestionEtablissements() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne la liste de tous les �tablissements de S�rie N via PGVMPARM
   */
  public ArrayList<GenericRecord> recupereListeETBsPGVMPARM(Utilisateur utilisateur) {
    if (loggerSiTrue("[recupereListeETBsPGVMPARM()] Utilisateur � NUll ", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT PARETB,SUBSTR(PARZ2, 1, 24) AS LIBETB FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
        + ".PGVMPARM " + " WHERE PARTYP='DG' AND PARETB <> '' ", this.getClass());
  }
  
  /**
   * Retourne la liste de tous les �tablissements de ETABLISS
   */
  public ArrayList<GenericRecord> recupereListeTousETBs(Utilisateur utilisateur) {
    if (loggerSiTrue("[recupereListeTousETBs()] Utilisateur � NUll ", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT ETB_ID, ETB_LIB, ETB_ACTIF FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".ETABLISS  WHERE ETB_FM = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' ORDER BY ETB_ID ", this.getClass());
  }
  
  /**
   * Retourne la liste d'�tablissements actifs de ETABLISS !!
   */
  public ArrayList<GenericRecord> recupereListeETBsActifs(Utilisateur utilisateur) {
    if (loggerSiTrue("[recupereListeETBsActifs()] Utilisateur � NUll ", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT ETB_ID, ETB_LIB, ETB_ACTIF FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".ETABLISS  WHERE ETB_FM = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND ETB_ACTIF='1' ORDER BY ETB_ID ", this.getClass());
  }
  
  /**
   * Retourner un seul �tablissement
   */
  public GenericRecord recupereUnEtB(Utilisateur utilisateur, String etb) {
    if (loggerSiTrue("[recupereUnEtB()] Utilisateur ou ETB � NUll ",
        (utilisateur == null || utilisateur.getAccesDB2() == null || etb == null)))
      return null;
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT ETB_ID, ETB_LIB, ETB_ACTIF FROM " + ConstantesEnvironnement.BIBLI_WS
            + ".ETABLISS WHERE ETB_FM = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND ETB_ID='" + etb + "' ", this.getClass());
    
    if (liste != null && liste.size() > 0)
      return liste.get(0);
    else
      return null;
  }
  
  /**
   * Mis � jour de l'�tablissement avec de nouveaux param�tres
   */
  public int majEtablissement(Utilisateur utilisateur, String etb, String libelle, String etat) {
    return utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ETABLISS SET ETB_LIB = '" + libelle
        + "', ETB_ACTIF = '" + etat + "' WHERE ETB_ID = '" + etb + "' AND ETB_FM ='" + ConstantesEnvironnement.BIBLI_CLIENTS + "' ");
  }
  
}
