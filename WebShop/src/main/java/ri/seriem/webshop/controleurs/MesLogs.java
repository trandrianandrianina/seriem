
package ri.seriem.webshop.controleurs;

import org.apache.log4j.Logger;

import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;

public class MesLogs {
  // Nom de la classe � logger
  protected String classeQuiLogge = "";
  // Doit dont logger de mani�re sp�cifique cette classe
  protected boolean IS_EN_DEBUG_SPEC = false;
  
  /**
   * Constructeur.
   */
  public MesLogs() {
    classeQuiLogge = this.getClass().getSimpleName();
  }
  
  /**
   * logger les conditions QUE si elles sont fausses
   */
  public boolean loggerSiFalse(String pMessage, boolean pResult) {
    // s'il est faux on le logge de toute mani�re
    if (!pResult && ConstantesDebug.MODE_LOG4J) {
      Logger.getLogger(classeQuiLogge).error("******* PROBLEME " + pMessage);
    }
    
    return pResult;
  }
  
  /**
   * logger les conditions QUE si elles sont vraies (objets � NULL)
   */
  public boolean loggerSiTrue(String message, boolean result) {
    return !loggerSiFalse(message, !result);
  }
  
  /**
   * logger SI CONDITION SPEC EN MESSAGE
   */
  public void loggerSiDEBUG(String pMessage) {
    if (ConstantesDebug.MODE_LOG4J && (IS_EN_DEBUG_SPEC || ConstantesEnvironnement.MODE_DEBUG)) {
      Logger.getLogger(classeQuiLogge).debug(pMessage);
    }
  }
  
  /**
   * logger tout court EN MESSAGE
   */
  public void forcerLogMessage(String pMessage) {
    if (ConstantesDebug.MODE_LOG4J) {
      Logger.getLogger(classeQuiLogge).debug(pMessage);
    }
  }
  
  /**
   * logger tout court en ERREUR
   */
  public void forcerLogErreur(String pMessage) {
    if (ConstantesDebug.MODE_LOG4J) {
      Logger.getLogger(classeQuiLogge).error(pMessage);
    }
  }
  
  // -- Accesseurs
  
  public String getClasseQuiLogge() {
    return classeQuiLogge;
  }
  
  public void setClasseQuiLogge(String classeQuiLogge) {
    this.classeQuiLogge = classeQuiLogge;
  }
  
  public boolean isIS_EN_DEBUG_SPEC() {
    return IS_EN_DEBUG_SPEC;
  }
  
  public void setIS_EN_DEBUG_SPEC(boolean iS_EN_DEBUG_SPEC) {
    IS_EN_DEBUG_SPEC = iS_EN_DEBUG_SPEC;
  }
}
