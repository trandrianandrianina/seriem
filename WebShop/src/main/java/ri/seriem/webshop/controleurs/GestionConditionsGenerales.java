
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Classe permettant de remplir les condiftions g�n�rales de vente des magasins
 * 
 * table CGV
 * @author D�borah
 *
 */

public class GestionConditionsGenerales extends Gestion {
  
  /**
   * R�cuperer les GCV du Web Shop
   */
  public ArrayList<GenericRecord> RecupererCgv(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT CGV_NAME FROM " + ConstantesEnvironnement.BIBLI_WS + ".CGV WHERE CGV_LAN = '"
        + utilisateur.getLanguage() + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
  }
}
