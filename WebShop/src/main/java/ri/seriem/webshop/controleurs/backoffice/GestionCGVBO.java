
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionCGVBO extends Gestion {
  public GestionCGVBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Afficher la R�cuperation des GCV des filiales
   */
  public ArrayList<GenericRecord> recupererCgv(Utilisateur utilisateur, String langue) {
    if (loggerSiTrue("Utilisateur ou LANGUE � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || langue == null)))
      return null;
    
    return utilisateur.getAccesDB2()
        .select("SELECT CGV_NAME FROM " + ConstantesEnvironnement.BIBLI_WS + ".CGV WHERE CGV_LAN = '" + langue + "'", this.getClass());
  }
  
  /**
   * Mise � jour de la table des conditions g�n�rales de vente [CGV]
   */
  public int miseAjourCgv(Utilisateur utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (loggerSiTrue("Utilisateur Request � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || request == null)))
      return res;
    
    // On supprime d'�ventuels enregistrements pour ce code langue
    if (request.getParameter("langueInfo") != null)
      res = utilisateur.getAccesDB2().requete(
          "DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".CGV WHERE CGV_LAN = '" + request.getParameter("langueInfo") + "'",
          this.getClass());
    
    // On rajoute un nouvel enregsitrement pour ce code langue
    if (res > -1)
      res = utilisateur.getAccesDB2().requete(
          "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".CGV (cgv_name,CGV_LAN) VALUES ('"
              + traiterCaracteresSpeciauxSQL(request.getParameter("conditions")) + "','" + request.getParameter("langueInfo") + "')",
          this.getClass());
    
    return res;
  }
}
