
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Client;
import ri.seriem.webshop.metier.Panier;

/**
 * Classe permettant de g�rer les donn�es propores � la vue "CLients"
 * 
 * @author David
 *
 */
public class GestionClients extends Gestion {
  /**
   * retourner les clients par mot cl� ou par d�faut sur les clients du repr�sentant
   */
  public ArrayList<GenericRecord> retournerListeClients(Utilisateur utilisateur, String expression) {
    if (loggerSiTrue("retournerListeClients() utilisateur � null ", utilisateur == null))
      return null;
    if (expression != null && !expressionIsOk(expression))
      return null;
    
    // Si on est en saisie d'expression
    if (expression != null) {
      expression = traiterCaracteresSpeciauxSQL(expression.toUpperCase());
      return utilisateur.getAccesDB2()
          .select("" + "SELECT CLCLI,CLLIV,CLETB,CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM " + " WHERE CLETB = '"
              + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND UPPER(CLNOM) LIKE '%" + expression + "%' OR CLCLI LIKE '" + expression
              + "%' " + " ORDER BY CLNOM FETCH FIRST " + utilisateur.getETB_EN_COURS().getLimit_requete_article()
              + " ROWS ONLY OPTIMIZE FOR " + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS "
              + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    }
    // Si on est en mode clients du repr�sentant
    else if (utilisateur.getUS_REPR() != null)
      return utilisateur.getAccesDB2().select("" + "SELECT CLCLI,CLLIV,CLETB,CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM "
          + " WHERE CLETB = '" + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND (CLREP='" + utilisateur.getUS_REPR() + "' OR CLREP2='"
          + utilisateur.getUS_REPR() + "') " + " ORDER BY CLNOM FETCH FIRST " + utilisateur.getETB_EN_COURS().getLimit_requete_article()
          + " ROWS ONLY OPTIMIZE FOR " + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS "
          + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    // Sinon Pas de code repr�sentant (c'est bizarre quand m�me...)
    else
      return utilisateur.getAccesDB2().select(
          "" + "SELECT CLCLI,CLLIV,CLETB,CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM " + " WHERE CLETB = '"
              + utilisateur.getETB_EN_COURS().getCodeETB() + "' " + " ORDER BY CLNOM FETCH FIRST "
              + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
  }
  
  /**
   * Attribuer un client au repr�sentant
   */
  public boolean attribuerUnClient(Utilisateur utilisateur, String client, String suffixe) {
    if (utilisateur == null || client == null || suffixe == null)
      return false;
    
    // On checke si on a bien un code repr�sentant associ� � cet uitlisateur
    if (utilisateur.getUS_REPR() != null) {
      // utilisateur.setClient(new Utilisateur(utilisateur.getSessionEnCours()));
      utilisateur.setClient(new Client(utilisateur, utilisateur.getETB_EN_COURS(), client, suffixe));
      if (utilisateur.getClient() != null) {
        utilisateur.setMonPanier(new Panier(utilisateur));
        
        return true;
      }
      else
        return false;
    }
    else
      return false;
  }
  
  /**
   * Retirer le client � la session du repr�sentant
   */
  public boolean retirerLeClient(Utilisateur utilisateur) {
    if (utilisateur == null)
      return false;
    
    utilisateur.setClient(null);
    utilisateur.setMonPanier(null);
    
    return true;
  }
  
  /**
   * Contr�le la validit� de l'expression
   */
  public boolean expressionIsOk(String expression) {
    int saisieMinimum = 3;
    int saisieMaximum = 30;
    boolean isOk = false;
    
    if (expression != null && expression.trim().length() >= saisieMinimum && expression.trim().length() <= saisieMaximum)
      isOk = true;
    
    return isOk;
  }
}
