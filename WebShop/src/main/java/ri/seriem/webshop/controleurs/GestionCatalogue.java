
package ri.seriem.webshop.controleurs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesAffichage;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.CalculStocks;
import ri.seriem.webshop.metier.GestionVariantes;
import ri.seriem.webshop.metier.Magasin;
import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.webservice.article.prix.ListePrixArticle;
import ri.seriem.webshop.webservice.article.prix.PrixArticle;

public class GestionCatalogue extends Gestion {
  // Constantes
  public static final String TABLE_FICHES_TECHNIQUES = "PSEMFTCM";
  public static final String TABLE_ARTICLE = "PGVMARTM";
  public static final BigDecimal ZERO = new BigDecimal(0);
  public static final BigDecimal CENT = new BigDecimal(100);
  public static final BigDecimal DIXMILLE = new BigDecimal(10000);
  
  // Variables
  private CalculStocks calculDeStocks = null;
  private GestionVariantes gestionVariantes = null;
  
  /**
   * Constructeur.
   */
  public GestionCatalogue() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_ARTICLES;
  }
  
  /**
   * Retourner une liste d'articles.
   */
  public ArrayList<GenericRecord> retournerListeArticlesMoteur(Utilisateur pUtilisateur, String pExpression) {
    String filtreFrs = " ";
    pExpression = traiterCaracteresSpeciauxSQL(pExpression.trim().toUpperCase());
    
    // On d�gage si on est pas bon sur la saisie de l'expression
    if (pUtilisateur == null || !expressionIsOk(pExpression)) {
      pUtilisateur.setDerniereListeArticle(null);
      return null;
    }
    
    if (pUtilisateur.getDernierFiltreFournisseur() != null) {
      filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
          + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
    }
    
    // Zones r�f�rence de recherche A1ART ou CAREF en fonction du param�trage
    String zonePrioritaire = ConstantesAffichage.REF_REF_FOURNISS;
    String zoneSecondaire = ConstantesAffichage.REF_CODE_ARTICLE;
    // Si le param�trage le r�clame c'est le code article qui devient prioritaire et non la r�f�rence fournisseur
    if (pUtilisateur.getETB_EN_COURS().getZone_reference_article() != null
        && pUtilisateur.getETB_EN_COURS().getZone_reference_article().equals(ConstantesAffichage.REF_CODE_ARTICLE)) {
      zonePrioritaire = ConstantesAffichage.REF_CODE_ARTICLE;
      zoneSecondaire = ConstantesAffichage.REF_REF_FOURNISS;
    }
    
    // S�lectionner les articles
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(
        "SELECT A1ETB, A1ART, " + "A1LIB, CAREF, A1UNV, LIBCND , CND, A1TVA, CASE WHEN " + zonePrioritaire + " LIKE '" + pExpression
            + "%' " + " THEN '1' WHEN A1LIB LIKE '" + pExpression + "%' THEN '2' WHEN " + zonePrioritaire + " LIKE '%" + pExpression
            + "%' THEN '3' " + " WHEN A1LIB LIKE '%" + pExpression + "%' THEN '4' END as TRI,A1COF, A1FRS, FRNOM " + " FROM "
            + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB()
            + "' " + ConstantesAffichage.retournerFiltreSqlStatutArticle(null) + " "
            + ConstantesAffichage.retournerFiltreSqlVieArticle(null) + " " + filtreFrs + " AND (" + zonePrioritaire + " LIKE '%"
            + pExpression + "%' OR A1LIB like '%" + pExpression + "%') " + " ORDER BY TRI FETCH FIRST "
            + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY " + " OPTIMIZE FOR "
            + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
    
    // Si on a pas trouv� de r�sultat, on cherche dans les r�f�rences articles
    if (liste != null && liste.size() == 0)
      liste = pUtilisateur.getAccesDB2()
          .select("SELECT A1ETB, A1ART, " + "A1LIB, CAREF, A1UNV, LIBCND , CND, CASE WHEN " + zoneSecondaire + " LIKE '" + pExpression
              + "%'" + "THEN '1' WHEN " + zoneSecondaire + " LIKE '%" + pExpression + "%' THEN '2'END as TRI,A1COF, A1FRS, FRNOM "
              + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '"
              + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' " + ConstantesAffichage.retournerFiltreSqlStatutArticle(null) + " "
              + ConstantesAffichage.retournerFiltreSqlVieArticle(null) + " " + filtreFrs + " AND " + zoneSecondaire + " LIKE '%"
              + pExpression + "%' " + " ORDER BY TRI FETCH FIRST " + pUtilisateur.getETB_EN_COURS().getLimit_requete_article()
              + " ROWS ONLY " + " OPTIMIZE FOR " + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS "
              + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    
    // attribuer la liste � l'utilisateur
    if (pUtilisateur.getDernierFiltreFournisseur() == null)
      pUtilisateur.setDerniereListeArticleMoteur(liste);
    
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Contr�le la validit� de l'expression.
   */
  public boolean expressionIsOk(String pExpression) {
    int saisieMinimum = 3;
    int saisieMaximum = 30;
    boolean isOk = false;
    
    if (pExpression != null && pExpression.trim().length() >= saisieMinimum && pExpression.trim().length() <= saisieMaximum) {
      isOk = true;
    }
    
    return isOk;
  }
  
  /**
   * Retourne la liste des groupes familles.
   */
  public ArrayList<GenericRecord> recupereGroupesFamilles(Utilisateur pUtilisateur) {
    if (loggerSiTrue("Utilisateur ou AccesDB2 � NUll", (pUtilisateur == null || pUtilisateur.getAccesDB2() == null))) {
      return null;
    }
    
    return pUtilisateur.getAccesDB2().select("SELECT CA_TYP as TYPE, CA_IND as IND, CA_LIB as LIB FROM "
        + ConstantesEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB = '"
        + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND CA_TYP <> 'SF' ORDER BY IND  " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
    
  }
  
  /**
   * Retourne la liste des groupes d'articles.
   */
  public ArrayList<GenericRecord> retourneListeGroupes(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur ou AccesDB2 � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null))) {
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT CA_TYP AS TYPE, CA_IND AS IND, CA_LIB AS LIB FROM " + ConstantesEnvironnement.BIBLI_WS
            + ".CATALOGUE WHERE CA_BIB = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB = '"
            + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND CA_TYP ='GR' ORDER BY IND  " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
            this.getClass());
  }
  
  /**
   * Retourne la liste des articles d'une famille ou d'une sous famille.
   */
  public ArrayList<GenericRecord> retourneListeArticlesFamille(Utilisateur pUtilisateur, String pFamille) {
    String filtreFrs = " ";
    
    if (loggerSiTrue("retourneListeArticlesFamille() Utilisateur ou famille � NUll", pFamille == null || pUtilisateur == null)) {
      return null;
    }
    
    pFamille = traiterCaracteresSpeciauxSQL(pFamille);
    String codFam = null;
    String fieldFam = null;
    // Si il s'agit d'une famille -> A1FAM
    if (pFamille.length() > 4 && pFamille.substring(0, 2).equals("FA")) {
      codFam = pFamille.substring(2, 5);
      fieldFam = "A1FAM";
    }
    // Si il s'agit d'une sous famille -> A1SFA
    else if (pFamille.length() > 6 && pFamille.substring(0, 2).equals("SF")) {
      codFam = pFamille.substring(2, 7);
      fieldFam = "A1SFA";
    }
    
    // Filtre sur le fournisseur
    if (pUtilisateur.getDernierFiltreFournisseur() != null)
      filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
          + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
    
    ArrayList<GenericRecord> liste = null;
    
    // Si on a bien r�cup�r� le code et la valeur correspondante on execute la demande d'articles
    if (codFam != null && fieldFam != null) {
      liste = pUtilisateur.getAccesDB2()
          .select("SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, LIBCND, CND , A1TVA, A1COF, A1FRS, FRNOM FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB()
              + "' AND " + fieldFam + "='" + codFam + "' " + ConstantesAffichage.retournerFiltreSqlStatutArticle(null) + " "
              + ConstantesAffichage.retournerFiltreSqlVieArticle(null) + " " + filtreFrs + " ORDER BY A1ABC, A1SAI DESC, A1LIB"
              + " FETCH FIRST " + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY " + " OPTIMIZE FOR "
              + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
              this.getClass());
    }
    // attribuer la liste � l'utilisateur
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Retourne la liste des fournisseurs issue de la liste des articles.
   */
  public HashMap<String, String[]> retourneListeFournisseur(Utilisateur pUtilisateur) {
    ArrayList<GenericRecord> liste = new ArrayList<GenericRecord>();
    
    if (pUtilisateur == null) {
      return null;
    }
    
    if (pUtilisateur.getDernierGroupeArticles() == null && pUtilisateur.getDerniereFamilleArticles() == null
        && pUtilisateur.getDerniereExpression() == null && pUtilisateur.getDerniersFavoris() == null
        && pUtilisateur.getDernierHistorique() == null && pUtilisateur.getDerniersConsultes() == null
        && pUtilisateur.getDernierPaniersIntuitifs() == null)
      return null;
    
    // Gestion de l'historique
    if (pUtilisateur.getDernierHistorique() != null) {
      if (pUtilisateur.getDerniereListeArticleHistorique() != null && pUtilisateur.getDerniereListeArticleHistorique().size() > 0)
        liste = pUtilisateur.getDerniereListeArticleHistorique();
    }
    // Gestion des favoris
    else if (pUtilisateur.getDerniersFavoris() != null) {
      if (pUtilisateur.getDerniereListeArticleFavoris() != null && pUtilisateur.getDerniereListeArticleFavoris().size() > 0)
        liste = pUtilisateur.getDerniereListeArticleFavoris();
    }
    // Gestion des paniers intuitifs
    else if (pUtilisateur.getDernierPaniersIntuitifs() != null) {
      if (pUtilisateur.getDerniereListePanierIntuitif() != null && pUtilisateur.getDerniereListePanierIntuitif().size() > 0)
        liste = pUtilisateur.getDerniereListePanierIntuitif();
    }
    // Gestion des consult�s
    else if (pUtilisateur.getDerniersConsultes() != null) {
      if (pUtilisateur.getDerniereListeArticleConsultee() != null && pUtilisateur.getDerniereListeArticleConsultee().size() > 0)
        liste = pUtilisateur.getDerniereListeArticleConsultee();
    }
    // Si derniere expression du client renseign�, alors on charge la liste
    // de l'utilisateur,
    else if (pUtilisateur.getDerniereExpression() != null) {
      // TODO Dans le cas ou on choisit un filtre seul le filtre actif se
      // r�affiche dans cette liste logique du coup il vaut mieux stocker en amont la liste non Filtree
      if (pUtilisateur.getDerniereListeArticleMoteur() != null && pUtilisateur.getDerniereListeArticleMoteur().size() > 0)
        liste = pUtilisateur.getDerniereListeArticleMoteur();
    }
    // sinon, on recherche tous les fournisseurs pour le groupe ou la
    // famille
    else {
      String recherche = "";
      if (pUtilisateur.getDernierGroupeArticles() != null)
        recherche = pUtilisateur.getDernierGroupeArticles();
      
      if (pUtilisateur.getDerniereFamilleArticles() != null) {
        if (pUtilisateur.getDerniereFamilleArticles().length() > 6
            && pUtilisateur.getDerniereFamilleArticles().substring(0, 2).equals("SF"))
          recherche = pUtilisateur.getDerniereFamilleArticles().substring(2, 7);
        else if (pUtilisateur.getDerniereFamilleArticles().length() > 4
            && pUtilisateur.getDerniereFamilleArticles().substring(0, 2).equals("FA"))
          recherche = pUtilisateur.getDerniereFamilleArticles().substring(2, 5);
      }
      
      liste = pUtilisateur.getAccesDB2().select("SELECT FO_COL AS A1COF, DIGITS(FO_FRS) AS A1FRS, FO_NOM AS FRNOM FROM "
          + ConstantesEnvironnement.BIBLI_WS + ".FOURNISS WHERE FO_BIB = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND FO_ETB = '"
          + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND FO_CA = '" + recherche + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
    }
    
    // Construction de la liste des fournisseurs
    HashMap<String, String[]> listeFrs = new HashMap<String, String[]>();
    if (liste.size() == 0) {
      return listeFrs;
    }
    
    for (int i = 0; i < liste.size(); i++) {
      if (liste.get(i).isPresentField("FRNOM") && liste.get(i).isPresentField("A1COF") && liste.get(i).isPresentField("A1FRS")) {
        // CAS DU A1FRS CODE SUR PLUS DE 6 DIGITS
        if (liste.get(i).getField("A1FRS").toString().length() > 6)
          liste.get(i).setField("A1FRS", liste.get(i).getField("A1FRS").toString().substring(4));
        
        // Ajoute le fournisseur � la liste des fournisseurs
        if (!listeFrs.containsKey(liste.get(i).getField("FRNOM").toString() + liste.get(i).getField("A1COF").toString()
            + liste.get(i).getField("A1FRS").toString()))
          listeFrs.put(
              liste.get(i).getField("FRNOM").toString() + liste.get(i).getField("A1COF").toString()
                  + liste.get(i).getField("A1FRS").toString(),
              new String[] { liste.get(i).getField("FRNOM").toString(), liste.get(i).getField("A1COF").toString(),
                  liste.get(i).getField("A1FRS").toString() });
      }
    }
    
    // Stockage de la derni�re liste des fournisseurs
    pUtilisateur.setDerniereListeFournisseur(listeFrs);
    return listeFrs;
  }
  
  /**
   * Retourne la liste des articles d'un groupe.
   */
  public ArrayList<GenericRecord> retourneListeArticlesGroupe(Utilisateur pUtilisateur, String pGroupe) {
    String filtreFrs = " ";
    
    if (pGroupe == null || pUtilisateur == null) {
      return null;
    }
    
    pGroupe = traiterCaracteresSpeciauxSQL(pGroupe);
    if (pUtilisateur.getDernierFiltreFournisseur() != null)
      filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
          + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(
        "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, LIBCND, CND, A1TVA, A1COF, A1FRS, FRNOM FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".VUE_ART_UT " + " WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' "
            + ConstantesAffichage.retournerFiltreSqlStatutArticle(null) + " " + ConstantesAffichage.retournerFiltreSqlVieArticle(null)
            + " " + filtreFrs + " AND A1FAM LIKE '" + pGroupe + "%' " + " ORDER BY A1ABC, A1SAI DESC, A1LIB FETCH FIRST "
            + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY " + " OPTIMIZE FOR "
            + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
    
    // Attribuer la liste � l'utilisateur
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Retourne le libell� concernant la famille.
   */
  public String retourneInfoFamille(Utilisateur pUtilisateur, String pFamille) {
    String codFam = null;
    
    if (pFamille == null || pUtilisateur == null) {
      return null;
    }
    
    pFamille = traiterCaracteresSpeciauxSQL(pFamille);
    if (pFamille.length() > 4)
      codFam = pFamille.substring(2, 5);
    else
      codFam = pFamille;
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT CA_LIB AS LIB FROM  " + ConstantesEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB= '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND CA_IND = '"
            + codFam + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    if (liste != null && liste.size() > 0)
      codFam = liste.get(0).getField("LIB").toString().trim();
    else
      codFam = "";
    
    return codFam;
  }
  
  /**
   * Retourne le libell� concernant la sous-famille.
   */
  public String retourneInfoSousFamille(Utilisateur pUtilisateur, String pFamille) {
    String codFam = null;
    
    if (pFamille == null || pUtilisateur == null) {
      return null;
    }
    
    pFamille = traiterCaracteresSpeciauxSQL(pFamille);
    if (pFamille.length() > 6)
      codFam = pFamille.substring(2, 7);
    else
      codFam = pFamille;
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT CA_LIB AS LIB FROM  " + ConstantesEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB= '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND CA_IND = '"
            + codFam + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    if (liste != null && liste.size() > 0)
      codFam = liste.get(0).getField("LIB").toString().trim();
    else
      codFam = "";
    
    return codFam;
  }
  
  /**
   * Retourne information concernant le groupe.
   */
  public ArrayList<GenericRecord> retourneInfoGroupe(Utilisateur pUtilisateur, String pGroupe) {
    if (pGroupe == null || pUtilisateur == null) {
      return null;
    }
    
    pGroupe = traiterCaracteresSpeciauxSQL(pGroupe);
    return pUtilisateur.getAccesDB2()
        .select("SELECT CA_LIB AS LIB FROM  " + ConstantesEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB= '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND CA_IND = '"
            + pGroupe + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
  }
  
  /**
   * Retourne la liste des groupes familles.
   */
  public ArrayList<GenericRecord> retournerFamillesPourUnGroupe(Utilisateur pUtilisateur, String pGroupe) {
    if (pGroupe == null || pUtilisateur == null) {
      return null;
    }
    
    pGroupe = traiterCaracteresSpeciauxSQL(pGroupe);
    return pUtilisateur.getAccesDB2()
        .select("SELECT CA_TYP as PARTYP, CA_IND as PARIND, CA_LIB as LIBELLEPAR FROM " + ConstantesEnvironnement.BIBLI_WS
            + ".CATALOGUE WHERE CA_BIB = '" + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB = '"
            + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND CA_TYP <> 'GR' AND SUBSTR(CA_IND, 1, 1) = '" + pGroupe
            + "' ORDER BY PARIND  " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
  }
  
  /**
   * VERSION PAGINATION A JOUR Retourne une extraction de la liste totale d'articles en
   * fonction d'un indice de parcours et du nombre d'articles dans un morceau
   */
  public ArrayList<GenericRecord> retourneMorceauListe(Utilisateur pUtilisateur, int pIndice) {
    if (pUtilisateur == null) {
      return null;
    }
    if (pUtilisateur.getDerniereListeArticle() == null) {
      return null;
    }
    if (pUtilisateur.getDerniereListeArticle().size() == 0) {
      return pUtilisateur.getDerniereListeArticle();
    }
    
    // Initialisation de la liste partielle de l'utilisateur
    if (pUtilisateur.getDerniereListePartiellePagination() == null) {
      pUtilisateur.setDerniereListePartiellePagination(new ArrayList<GenericRecord>());
    }
    else {
      pUtilisateur.getDerniereListePartiellePagination().clear();
    }
    
    // Si on est un client ou un repr�sentant avec un client associ�
    if (pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT
        || pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CONSULTATION
        || (pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS && pUtilisateur.getClient() != null)
            && pUtilisateur.isVisuStock()) {
      String dispo = null;
      String attendu = null;
      pUtilisateur.controleGestionTarif();
      
      for (int i = pIndice; i < pUtilisateur.getDerniereListeArticle().size()
          && i < pIndice + pUtilisateur.getETB_EN_COURS().getLimit_liste(); i++) {
        if ((pUtilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI
            || pUtilisateur.getETB_EN_COURS().voir_stock_total()) && pUtilisateur.isVisuStock()
            && pUtilisateur.isAutoriseVueStockDispo()) {
          // On ne recalcule pas les stocks alors qu'on l'a d�j� fait
          if (!pUtilisateur.getETB_EN_COURS().is_filtre_stock()) {
            if (pUtilisateur.getETB_EN_COURS().voir_stock_total()) {
              dispo = retourneDispoArticleSQLunMagasin(pUtilisateur,
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(), null);
            }
            else {
              dispo = retourneDispoArticleSQL(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString());
            }
            // Si le stock est null ou < � 0
            if (dispo == null || dispo.startsWith("-"))
              dispo = "0.000";
            // On rajoute le stock � l'article
            pUtilisateur.getDerniereListeArticle().get(i).setField("STOCK", dispo);
            // Gestion du stock attendu dans la liste... BEEUUUUURK
            if (pUtilisateur.getMonPanier() != null && pUtilisateur.getETB_EN_COURS().isVoir_stock_attendu()
                && pUtilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI
                && pUtilisateur.getMonPanier().getMagasinPanier() != null) {
              attendu = retournerStockAttendu(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
                  pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
              if (attendu != null) {
                pUtilisateur.getDerniereListeArticle().get(i).setField("ATTEN", attendu);
              }
            }
          }
        }
        
        // Associer les tarifs au record d�j� pr�sent
        if (!pUtilisateur.getDerniereListeArticle().get(i).isPresentField("TARIF") && pUtilisateur.isAutoriseVuePrix()) {
          pUtilisateur.getDerniereListeArticle().get(i).setField("TARIF",
              retournerTarifClient(pUtilisateur, pUtilisateur.getETB_EN_COURS().getCodeETB(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString().trim()));
        }
        
        // Gestion des promotions
        if (estEnPromotion(pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString().trim(),
            pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString().trim(), pUtilisateur)) {
          pUtilisateur.getDerniereListeArticle().get(i).setField("PROMO", "1");
        }
        
        pUtilisateur.getDerniereListePartiellePagination().add(pUtilisateur.getDerniereListeArticle().get(i));
      }
    }
    // Pour les modes au dessus de clients (repr�sentant, gestionnaire, admin)
    else if (pUtilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_BOUTIQUE && pUtilisateur.getClient() == null) {
      GenericRecord record = null;
      
      for (int i = pIndice; i < pUtilisateur.getDerniereListeArticle().size()
          && i < pIndice + pUtilisateur.getETB_EN_COURS().getLimit_liste(); i++) {
        // Associer les tarifs au record d�j� pr�sent
        record = retourneTarifGeneralArticle(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
            pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString().trim(),
            pUtilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString().trim());
        if (record != null && record.isPresentField("ATP01") && pUtilisateur.getDerniereListeArticle().get(i).isPresentField("A1TVA")) {
          if (pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_BOUTIQUE) {
            BigDecimal montantTTC =
                retournerTTC(pUtilisateur, record.getField("ATP01"), pUtilisateur.getDerniereListeArticle().get(i).getField("A1TVA"));
            pUtilisateur.getDerniereListeArticle().get(i).setField("TARIF", montantTTC);
          }
          else
            pUtilisateur.getDerniereListeArticle().get(i).setField("TARIF", record.getField("ATP01"));
        }
        else
          pUtilisateur.getDerniereListeArticle().get(i).setField("TARIF", new BigDecimal(0));
        
        // Gestion des promotions
        if (estEnPromotion(pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString().trim(),
            pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString().trim(), pUtilisateur)) {
          pUtilisateur.getDerniereListeArticle().get(i).setField("PROMO", "1");
        }
        
        pUtilisateur.getDerniereListePartiellePagination().add(pUtilisateur.getDerniereListeArticle().get(i));
      }
    }
    else {
      pUtilisateur.controleGestionTarif();
      for (int i = pIndice; i < pUtilisateur.getDerniereListeArticle().size()
          && i < pIndice + pUtilisateur.getETB_EN_COURS().getLimit_liste(); i++) {
        if (pUtilisateur.getClient() != null) {
          // Associer les tarifs au record d�j� pr�sent
          pUtilisateur.getDerniereListeArticle().get(i).setField("TARIF",
              retournerTarifClient(pUtilisateur, pUtilisateur.getETB_EN_COURS().getCodeETB(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString().trim()));
        }
        else
          pUtilisateur.getDerniereListeArticle().get(i).setField("TARIF", new BigDecimal(0));
        
        pUtilisateur.getDerniereListePartiellePagination().add(pUtilisateur.getDerniereListeArticle().get(i));
      }
    }
    
    return pUtilisateur.getDerniereListePartiellePagination();
  }
  
  /**
   * Retourne le tarif g�n�ral (T1) d'un article.
   */
  public GenericRecord retourneTarifGeneralArticle(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle,
      String pUnite) {
    if (pUnite == null || pCodeEtablissement == null || pCodeArticle == null || pUtilisateur == null) {
      return null;
    }
    
    pUnite = traiterCaracteresSpeciauxSQL(pUnite).trim();
    pCodeEtablissement = traiterCaracteresSpeciauxSQL(pCodeEtablissement).trim();
    pCodeArticle = traiterCaracteresSpeciauxSQL(pCodeArticle).trim();
    
    String requete = null;
    // Gestion d�cimales
    if (pUtilisateur.getETB_EN_COURS().getDecimales_client() > 0) {
      requete = "SELECT DECIMAL((CASE WHEN SUBSTR('" + pUnite + "', 2, 1)='*' THEN ATP01/100 ELSE ATP01 END), 9, 4) AS ATP01 FROM "
          + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_TARIFS " + " WHERE ATETB = '" + pCodeEtablissement + "' AND ATART = '"
          + pCodeArticle + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE;
    }
    // Francs Pacifiques
    else {
      requete = "SELECT (ATP01 * 100) AS ATP01 FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_TARIFS WHERE ATETB = '"
          + pCodeEtablissement + "' AND ATART = '" + pCodeArticle + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE
          + ConstantesEnvironnement.CLAUSE_OPTIMIZE;
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (liste != null && liste.size() > 0) {
      loggerSiDEBUG("[retourneTarifGeneralArticle()] : " + liste.get(0).getField("ATP01").toString());
      return liste.get(0);
    }
    
    return null;
  }
  
  /**
   * Retourner le montant TTC d'un tarif HT en fonction du code tva de l'article et des taux associ�s � l'utilisateur et son
   * �tablissement.
   */
  public BigDecimal retournerTTC(Utilisateur pUtilisateur, Object pMontantHT, Object pCodeTVA) {
    if (pUtilisateur == null || pMontantHT == null || pCodeTVA == null) {
      return null;
    }
    
    BigDecimal montant = (BigDecimal) pMontantHT;
    String codeTVA = pCodeTVA.toString().trim();
    BigDecimal tauxTVA = null;
    
    if (montant.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }
    
    if (codeTVA.equals("0")) {
      codeTVA = "1";
    }
    
    if (codeTVA.equals("1")) {
      tauxTVA = pUtilisateur.getETB_EN_COURS().getTaux_tva_1();
    }
    else if (codeTVA.equals("2")) {
      tauxTVA = pUtilisateur.getETB_EN_COURS().getTaux_tva_2();
    }
    else if (codeTVA.equals("3")) {
      tauxTVA = pUtilisateur.getETB_EN_COURS().getTaux_tva_3();
    }
    else if (codeTVA.equals("4")) {
      tauxTVA = pUtilisateur.getETB_EN_COURS().getTaux_tva_4();
    }
    else if (codeTVA.equals("5")) {
      tauxTVA = pUtilisateur.getETB_EN_COURS().getTaux_tva_5();
    }
    else if (codeTVA.equals("6")) {
      tauxTVA = pUtilisateur.getETB_EN_COURS().getTaux_tva_6();
    }
    else {
      tauxTVA = BigDecimal.ZERO;
    }
    
    if (tauxTVA == null || tauxTVA.compareTo(BigDecimal.ZERO) == 0) {
      return montant;
    }
    montant = montant.add((montant.multiply(tauxTVA).divide(new BigDecimal(100)))).setScale(2, RoundingMode.HALF_UP);
    return montant;
  }
  
  /**
   * Retourne le libell� du fournisseur.
   */
  public GenericRecord retourneNomFournisseur(Utilisateur pUtilisateur, String pCodeFournisseur) {
    if (pCodeFournisseur == null || pUtilisateur == null) {
      return null;
    }
    
    pCodeFournisseur = traiterCaracteresSpeciauxSQL(pCodeFournisseur);
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT FRNOM FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMFRSM " + " WHERE FRETB = '"
            + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND DIGITS(FRCOL) CONCAT DIGITS(FRFRS) = '" + pCodeFournisseur + "' "
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    return null;
  }
  
  /**
   * Retourne le d�tail pour un article.
   */
  public GenericRecord retourneDetailArticle(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pCodeEtablissement == null || pCodeArticle == null || pUtilisateur == null) {
      return null;
    }
    
    pCodeEtablissement = traiterCaracteresSpeciauxSQL(pCodeEtablissement);
    pCodeArticle = traiterCaracteresSpeciauxSQL(pCodeArticle);
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT A1ETB, A1ART, A1LIB, A1FAM, A1SFA, FRNOM, CAREF, A1UNV, CARFC, A1NPU, A1TVA, LIBCND as LIBUNV , CND AS "
            + ConstantesEnvironnement.ZONE_CONDITIONNEMENT + " " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT "
            + " WHERE A1ETB = '" + pCodeEtablissement + "' AND A1ART = '" + pCodeArticle + "' "
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    // traitement des tarifs et des stocks
    if (liste != null && liste.size() == 1) {
      // Clients ou repr�sentant un client
      if ((pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CLIENT
          || pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_CONSULTATION
          || pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_REPRES_WS) && pUtilisateur.getClient() != null) {
        if (pUtilisateur.isAutoriseVuePrix()) {
          BigDecimal tarif = retournerTarifClient(pUtilisateur, liste.get(0).getField("A1ETB").toString(),
              liste.get(0).getField("A1ART").toString().trim(), liste.get(0).getField("A1UNV").toString().trim());
          if (tarif == null || tarif.compareTo(ZERO) == 0) {
            tarif = ZERO;
          }
          
          liste.get(0).setField("TARIF", tarif);
        }
        else {
          liste.get(0).setField("TARIF", ZERO);
        }
      }
      // en mode tarif public (admin, gestionnaire, boutique et repr�sentants sans client)
      else {
        GenericRecord record = retourneTarifGeneralArticle(pUtilisateur, liste.get(0).getField("A1ETB").toString(),
            liste.get(0).getField("A1ART").toString().trim(), liste.get(0).getField("A1UNV").toString().trim());
        if (record != null && record.isPresentField("ATP01") && liste.get(0).isPresentField("A1TVA")) {
          if (pUtilisateur.getUS_ACCES() == ConstantesEnvironnement.ACCES_BOUTIQUE) {
            BigDecimal montantTTC = retournerTTC(pUtilisateur, record.getField("ATP01"), liste.get(0).getField("A1TVA"));
            liste.get(0).setField("TARIF", montantTTC);
          }
          else {
            liste.get(0).setField("TARIF", record.getField("ATP01"));
          }
        }
        else {
          liste.get(0).setField("TARIF", new BigDecimal(0));
        }
      }
      // Stocks
      if (pUtilisateur.getMonPanier() != null && pUtilisateur.getMonPanier().getModeRecup() > ConstantesEnvironnement.MODE_NON_CHOISI
          && pUtilisateur.isAutoriseVueStockDispo())
        liste.get(0).setField("STOCK", retourneDispoArticleSQL(pUtilisateur, pCodeEtablissement, pCodeArticle));
      
      // Gestion des promotions
      if (estEnPromotion(liste.get(0).getField("A1ETB").toString().trim(), liste.get(0).getField("A1ART").toString().trim(),
          pUtilisateur)) {
        liste.get(0).setField("PROMO", "1");
      }
      
      loggerSiDEBUG("+++++++++++++ [retourneDetailArticle()] +++++++++++++");
      loggerSiDEBUG("A1ART: " + liste.get(0).getField("A1ART").toString());
      loggerSiDEBUG("A1UNV: " + liste.get(0).getField("A1UNV").toString());
      loggerSiDEBUG(ConstantesEnvironnement.ZONE_CONDITIONNEMENT + ": "
          + liste.get(0).getField(ConstantesEnvironnement.ZONE_CONDITIONNEMENT).toString());
      loggerSiDEBUG("TARIF: " + liste.get(0).getField("TARIF").toString());
      if (pUtilisateur.getMonPanier() != null && pUtilisateur.getMonPanier().getModeRecup() > ConstantesEnvironnement.MODE_NON_CHOISI)
        loggerSiDEBUG("STOCK: " + liste.get(0).getField("STOCK").toString());
      loggerSiDEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
      
      return liste.get(0);
    }
    
    return null;
  }
  
  /**
   * Permet de retourner le tarif d'un article avec la condition du client.
   */
  public BigDecimal retournerTarifClient(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle, String pUnite) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return null;
    }
    
    BigDecimal valeurBrute = null;
    pUtilisateur.controleGestionTarif();
    if (pUtilisateur.getGestionTarif().execute(pCodeEtablissement, new BigDecimal(pUtilisateur.getClient().getNumeroClient()),
        new BigDecimal(pUtilisateur.getClient().getSuffixeClient()), pCodeArticle, new BigDecimal(1))) {
      valeurBrute = pUtilisateur.getGestionTarif().getTarif().getValue();
      loggerSiDEBUG("Article : " + pCodeEtablissement + "/" + pCodeArticle);
      loggerSiDEBUG("decimales client: " + pUtilisateur.getETB_EN_COURS().getDecimales_client());
      loggerSiDEBUG("retournerTarifClient AV DECIMALE: " + valeurBrute);
      if (valeurBrute != null && valeurBrute.intValue() != 0) {
        if (pUtilisateur.getETB_EN_COURS().getDecimales_client() == 4
            || (pUnite != null && pUnite.toString().endsWith(ConstantesEnvironnement.CODE_QUATRE_DECIMALE))) {
          valeurBrute = valeurBrute.divide(DIXMILLE);
        }
        else if (pUtilisateur.getETB_EN_COURS().getDecimales_client() == 2) {
          valeurBrute = valeurBrute.divide(CENT);
        }
      }
      loggerSiDEBUG("retournerTarifClient AP DECIMALE: " + valeurBrute);
    }
    return valeurBrute;
  }
  
  /**
   * Retourne le stock dispo pour un article � partir d'un programme RPG.
   */
  public String retourneDispoArticle(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    BigDecimal dispoStock = null;
    String dispo = null;
    
    String magasinRetrait = retourneMagasinRetrait(pUtilisateur);
    if (pUtilisateur.getGestionStock().execute(pCodeEtablissement, pCodeArticle, pUtilisateur.getMagasinSiege().getCodeMagasin(),
        magasinRetrait, new BigDecimal(0), new BigDecimal(0))) {
      dispoStock = pUtilisateur.getGestionStock().getStock1().getValue().add(pUtilisateur.getGestionStock().getStock2().getValue());
      loggerSiDEBUG("retourneDispoArticle() dispoStock Brut: " + dispoStock.toString());
    }
    
    if (dispoStock.toString().length() > 1) {
      dispo = dispoStock.toString().substring(0, dispoStock.toString().length() - 3) + "."
          + dispoStock.toString().substring(dispoStock.toString().length() - 3);
    }
    else {
      dispo = dispoStock.toString();
    }
    
    loggerSiDEBUG("retourneDispoArticle() dispo trait�e: " + dispo);
    return dispo;
  }
  
  /**
   * Retourne le stock dispo pour un article (via SQL) en fonction du mode de r�cup�ration, multi-magasins et type de
   * stock de retrait.
   */
  public String retourneDispoArticleSQL(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return null;
    }
    
    pCodeEtablissement = traiterCaracteresSpeciauxSQL(pCodeEtablissement);
    pCodeArticle = traiterCaracteresSpeciauxSQL(pCodeArticle);
    
    if (calculDeStocks == null) {
      calculDeStocks = new CalculStocks();
    }
    
    if (calculDeStocks != null && pUtilisateur.getMonPanier() != null) {
      // En fonction du mode de livraison
      if (pUtilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON) {
        // Si est pas mono magasin
        if (!pUtilisateur.getETB_EN_COURS().is_mono_magasin())
          return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle,
              pUtilisateur.getMagasinSiege().getCodeMagasin());
        else
          return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle,
              pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
      }
      else {
        // Si le stock retrait est un stock SIEGE + MAGASIN et qu'on est un �tablissement multi Magasins
        if (pUtilisateur.getETB_EN_COURS().isRetrait_is_siege_et_mag() && !pUtilisateur.getETB_EN_COURS().is_mono_magasin()) {
          return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle,
              pUtilisateur.getMagasinSiege().getCodeMagasin(), pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
        }
        return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle,
            pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
      }
    }
    return null;
  }
  
  /**
   * Retourne le stock dispo d'un article SQL d'un magasin.
   */
  public String retourneDispoArticleSQLunMagasin(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle,
      String pCodeMagasin) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return null;
    }
    
    pCodeEtablissement = traiterCaracteresSpeciauxSQL(pCodeEtablissement);
    pCodeArticle = traiterCaracteresSpeciauxSQL(pCodeArticle);
    if (calculDeStocks == null) {
      calculDeStocks = new CalculStocks();
    }
    
    if (calculDeStocks != null) {
      // Pas de magasin transmis
      if (pCodeMagasin == null) {
        // C'est normal on veut le stock de tous les magasins h�h�h�...
        if (pUtilisateur.getETB_EN_COURS().voir_stock_total()) {
          if (pUtilisateur.getMonPanier() != null && pUtilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI
              && pUtilisateur.getMonPanier().getMagasinPanier() != null) {
            pCodeMagasin = pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
          }
          return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle, pCodeMagasin);
        }
        else {
          return null;
        }
      }
      // Si le stock retrait est un stock SIEGE + MAGASIN et qu'on est un �tablissement multi Magasins
      else {
        if (pUtilisateur.getETB_EN_COURS().isRetrait_is_siege_et_mag() && !pUtilisateur.getETB_EN_COURS().is_mono_magasin()) {
          return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle,
              pUtilisateur.getMagasinSiege().getCodeMagasin(), pCodeMagasin);
        }
        return calculDeStocks.retournerStock(pUtilisateur, pCodeEtablissement, pCodeArticle, pCodeMagasin);
      }
    }
    return null;
  }
  
  /**
   * Permet de retourner le lien d'une photo par sa r�f�rence fournisseur dans l'IFS ou sur un syst�me d'h�bergement.
   */
  public String recupererIFSphoto(Utilisateur pUtilisateur, String pNomImage) {
    if (pUtilisateur.getETB_EN_COURS().getChemin_images_articles() == null) {
      return null;
    }
    
    return "src='" + pUtilisateur.getETB_EN_COURS().getChemin_images_articles() + pNomImage + "."
        + pUtilisateur.getETB_EN_COURS().getExtensions_photos() + "'";
  }
  
  /**
   * Permet de retourner le lien d'une photo par sa r�f�rence fournisseur dans une URL fournisseur saisie dans S�rie M.
   */
  public String recupererURLphoto(String pReferenceFournisseur, String pCodeEtablissement, String pCodeArticle,
      Utilisateur pUtilisateur) {
    if (verifierPresencePSEMURLM(pUtilisateur)) {
      return recupererPhotoURL(pUtilisateur, pCodeEtablissement, pCodeArticle);
    }
    return null;
  }
  
  /**
   * R�cup�rer l'URL d'une fiche technique fournisseur si elle existe dans PSEMURLM.
   */
  public String recupererFicheTechniqueURL(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      return null;
    }
    pCodeEtablissement = traiterCaracteresSpeciauxSQL(pCodeEtablissement);
    pCodeArticle = traiterCaracteresSpeciauxSQL(pCodeArticle);
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT FTIND,FTURL FROM " + pUtilisateur.getBibli() + "." + TABLE_FICHES_TECHNIQUES + " WHERE FTETB = '"
            + pCodeEtablissement.trim() + "' AND FTART = '" + pCodeArticle.trim() + "' AND FTWEB = '1'  "
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE, this.getClass());
    if (liste != null && liste.size() > 0 && liste.get(0).isPresentField("FTURL")) {
      return liste.get(0).getField("FTURL").toString().trim();
    }
    
    return null;
  }
  
  /**
   * Permet de v�rifier si cet article est vendable ou non: dans un premier temps par son tarif.
   */
  public boolean verifierSiVendable(BigDecimal pTarif, boolean pOnVendAzero) {
    if (pTarif == null) {
      return false;
    }
    if (pTarif.compareTo(ZERO) == 0) {
      return pOnVendAzero;
    }
    return true;
  }
  
  /**
   * Retourner La liste de tous les magasins.
   */
  public ArrayList<Magasin> retournerTousLesMagasins(Utilisateur pUtilisateur) {
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      return null;
    }
    
    ArrayList<Magasin> liste = null;
    ArrayList<GenericRecord> listePartielle =
        pUtilisateur.getAccesDB2().select("SELECT MG_NAME,MG_COD FROM " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB='"
            + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND MG_ACTIF = '1' ", this.getClass());
    if (listePartielle != null && listePartielle.size() > 0) {
      if (liste == null) {
        liste = new ArrayList<Magasin>();
      }
      
      for (int i = 0; i < listePartielle.size(); i++) {
        liste.add(new Magasin(pUtilisateur, listePartielle.get(i).getField("MG_COD").toString().trim(),
            listePartielle.get(i).getField("MG_NAME").toString().trim()));
      }
    }
    
    return liste;
  }
  
  /**
   * Enlever ou mettre un article dans ses favoris.
   */
  public int majFavoriArticle(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return 0;
    }
    
    int retour = isEnFavori(pUtilisateur, pCodeEtablissement, pCodeArticle);
    if (retour == 1) {
      if (pUtilisateur.getAccesDB2()
          .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".FAVORIS (FV_UTIL,FV_ETB,FV_ARTI) VALUES ('"
              + pUtilisateur.getUS_ID() + "','" + pCodeEtablissement + "','" + pCodeArticle + "') ", this.getClass()) == 1) {
        retour = 2;
      }
      else {
        retour = 0;
      }
    }
    else if (retour == 2) {
      if (pUtilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".FAVORIS WHERE FV_UTIL = '"
          + pUtilisateur.getUS_ID() + "' AND FV_ETB = '" + pCodeEtablissement + "' AND FV_ARTi = '" + pCodeArticle + "'",
          this.getClass()) == 1) {
        retour = 1;
      }
      else {
        retour = 0;
      }
    }
    
    return retour;
  }
  
  /**
   * R�cup�rer l'�tat de l'article: Favori ou non.
   */
  public int isEnFavori(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return 0;
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT FV_ID FROM " + ConstantesEnvironnement.BIBLI_WS + ".FAVORIS WHERE FV_UTIL = '" + pUtilisateur.getUS_ID()
            + "' AND FV_ETB = '" + pCodeEtablissement + "' AND FV_ARTI = '" + pCodeArticle + "' ", this.getClass());
    if (liste != null) {
      if (liste.size() > 0) {
        return 2;
      }
      return 1;
    }
    
    return 0;
  }
  
  /**
   * R�cup�rer les articles favoris de l'utilisateur sur l'�tablisssement courant.
   */
  public ArrayList<GenericRecord> retournerListeArticlesFavoris(Utilisateur pUtilisateur) {
    if (pUtilisateur == null) {
      return null;
    }
    
    String filtreFrs = " ";
    ArrayList<GenericRecord> liste =
        pUtilisateur.getAccesDB2().select("SELECT FV_ARTI FROM " + ConstantesEnvironnement.BIBLI_WS + ".FAVORIS WHERE FV_UTIL = '"
            + pUtilisateur.getUS_ID() + "' AND FV_ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' ", this.getClass());
    
    // Il existe des articles dans les favoris de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (pUtilisateur.getDernierFiltreFournisseur() != null)
        filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
      
      String arguments = "(";
      for (int i = 0; i < liste.size(); i++) {
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        arguments += liste.get(i).getField("FV_ARTI").toString().trim() + "'";
      }
      arguments += ")";
      
      loggerSiDEBUG("Arguments ARTICLES FAVORIS:" + arguments);
      
      liste = pUtilisateur.getAccesDB2().select(
          "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, LIBCND , CND , A1TVA, A1COF, A1FRS, FRNOM " + " FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB()
              + "' AND A1ART IN " + arguments + filtreFrs + " ORDER BY A1ABC, A1SAI DESC, A1LIB FETCH FIRST "
              + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
      
    }
    
    // attribuer la liste � l'utilisateur
    if (pUtilisateur.getDernierFiltreFournisseur() == null) {
      pUtilisateur.setDerniereListeArticleFavoris(liste);
    }
    
    // attribuer la liste � l'utilisateur
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * R�cup�rer les articles favoris de l'utilisateur sur l'�tablisssement courant.
   */
  public ArrayList<GenericRecord> retournerListeArticlesAchats(Utilisateur pUtilisateur) {
    if (pUtilisateur == null) {
      return null;
    }
    
    String filtreFrs = " ";
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT DISTINCT ART_ID,MAX(PA_DTC) AS DTMAX, MAX(PA_ID) AS IDMAX FROM " + ConstantesEnvironnement.BIBLI_WS + ".PANIER, "
            + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = PA_ID AND PA_US = '" + pUtilisateur.getUS_ID()
            + "' AND ART_ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "' AND PA_ETA > 0 "
            + " GROUP BY ART_ID ORDER BY DTMAX DESC, IDMAX DESC ", this.getClass());
    
    // Il existe des articles dans les favoris de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (pUtilisateur.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      String triSpecial = " ORDER BY CASE ";
      for (int i = 0; i < liste.size(); i++) {
        // arguments de recherche
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        arguments += liste.get(i).getField("ART_ID").toString().trim() + "'";
        
        // Arguments de tri
        triSpecial += "WHEN A1ART='" + liste.get(i).getField("ART_ID").toString().trim() + "' THEN " + (i + 1) + " ";
      }
      arguments += ")";
      triSpecial += " END ";
      
      loggerSiDEBUG("Arguments ARTICLES Historique:" + arguments);
      
      liste = pUtilisateur.getAccesDB2().select(
          "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, LIBCND , CND , A1TVA, A1COF, A1FRS, FRNOM " + " FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB()
              + "' AND A1ART IN " + arguments + filtreFrs + " " + triSpecial + "  FETCH FIRST "
              + pUtilisateur.getETB_EN_COURS().getQte_max_historique() + " ROWS ONLY OPTIMIZE FOR "
              + pUtilisateur.getETB_EN_COURS().getQte_max_historique() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
    }
    
    // attribuer la liste � l'utilisateur
    if (pUtilisateur.getDernierFiltreFournisseur() == null) {
      pUtilisateur.setDerniereListeArticleHistorique(liste);
    }
    
    // attribuer la liste � l'utilisateur
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Ajouter un article aux articles consult�s de l'utilisateur.
   */
  public void ajouterAuxConsultes(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return;
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT CON_ID FROM " + ConstantesEnvironnement.BIBLI_WS + ".CONSULTES WHERE CON_UTIL = '" + pUtilisateur.getUS_ID()
            + "' AND CON_ETB = '" + pCodeEtablissement + "' AND CON_ARTI = '" + pCodeArticle + "' ", this.getClass());
    
    // Si l'article n'est pas encore dans la table
    if (liste != null && liste.size() == 0) {
      pUtilisateur.getAccesDB2()
          .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".CONSULTES (CON_UTIL,CON_ETB,CON_ARTI) VALUES ('"
              + pUtilisateur.getUS_ID() + "','" + pCodeEtablissement + "','" + pCodeArticle + "')", this.getClass());
    }
    // S'il est d�j� dans la table
    else if (liste != null && liste.size() == 1) {
      if (pUtilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".CONSULTES WHERE CON_UTIL = '"
          + pUtilisateur.getUS_ID() + "' AND CON_ETB = '" + pCodeEtablissement + "' AND CON_ARTI = '" + pCodeArticle + "' ",
          this.getClass()) == 1) {
        pUtilisateur.getAccesDB2()
            .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".CONSULTES (CON_UTIL,CON_ETB,CON_ARTI) VALUES ('"
                + pUtilisateur.getUS_ID() + "','" + pCodeEtablissement + "','" + pCodeArticle + "')", this.getClass());
      }
    }
  }
  
  /**
   * Retourner la liste des articles r�cemment consult�s par un utilisateur.
   */
  public ArrayList<GenericRecord> retournerListeArticlesConsultes(Utilisateur pUtilisateur) {
    if (pUtilisateur == null) {
      return null;
    }
    
    String filtreFrs = " ";
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select("SELECT CON_ARTI FROM " + ConstantesEnvironnement.BIBLI_WS + ".CONSULTES WHERE CON_UTIL = '" + pUtilisateur.getUS_ID()
            + "' AND CON_ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB() + "'  ORDER BY CON_ID DESC FETCH FIRST "
            + pUtilisateur.getETB_EN_COURS().getLimit_liste() + " ROWS ONLY ", this.getClass());
    
    // Il existe des articles dans les consult�s de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (pUtilisateur.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      String triSpecial = " ORDER BY CASE ";
      for (int i = 0; i < liste.size(); i++) {
        // arguments de recherche
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        arguments += liste.get(i).getField("CON_ARTI").toString().trim() + "'";
        
        // Arguments de tri
        triSpecial += "WHEN A1ART='" + liste.get(i).getField("CON_ARTI").toString().trim() + "' THEN " + (i + 1) + " ";
      }
      arguments += ")";
      triSpecial += " END ";
      
      loggerSiDEBUG("Arguments ARTICLES consultes:" + arguments);
      liste = pUtilisateur.getAccesDB2().select(
          "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, LIBCND , CND , A1TVA, A1COF, A1FRS, FRNOM " + " FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB()
              + "' AND A1ART IN " + arguments + filtreFrs + " " + triSpecial + "  FETCH FIRST "
              + pUtilisateur.getETB_EN_COURS().getQte_max_historique() + " ROWS ONLY OPTIMIZE FOR "
              + pUtilisateur.getETB_EN_COURS().getQte_max_historique() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
    }
    
    // attribuer la liste � l'utilisateur
    if (pUtilisateur.getDernierFiltreFournisseur() == null) {
      pUtilisateur.setDerniereListeArticleConsultee(liste);
    }
    
    // attribuer la liste � l'utilisateur
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Retourner la date d'attendu d'un article pour un magasin donn�.
   */
  public String retournerDateAttenduMagasin(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle,
      String pCodeMagasin) {
    String retour = "NR";
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null || pCodeMagasin == null) {
      return retour;
    }
    
    String requete = null;
    String magSouhaite = null;
    
    if (pUtilisateur.getETB_EN_COURS().isIs_reassort_siege() && !pUtilisateur.getETB_EN_COURS().is_mono_magasin()
        && pUtilisateur.getMagasinSiege() != null) {
      magSouhaite = pUtilisateur.getMagasinSiege().getCodeMagasin();
    }
    else {
      magSouhaite = pCodeMagasin;
    }
    
    if (magSouhaite == null) {
      return retour;
    }
    
    if (ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE)
      requete = "SELECT CASE WHEN LADLP<>0 then LADLP ELSE EADLP END AS DISPO," + " LAQTS,(SELECT SUM(AAQTE) AS QTERES  FROM "
          + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMALAM " + " WHERE lb.LAETB=AAETB AND lb.LAART=AAART AND lb.LANUM=AANOE "
          + " AND lb.LASUF=AASOE AND lb.LANLI=AALOE AND AATOS='V' AND AATOE IN ('G' , 'A') ) AS QTEDISPO " + " FROM "
          + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMLBFM lb LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
          + ".PGVMEBFM ON LAETB=EAETB " + " AND LACOD=EACOD AND LANUM=EANUM AND EASUF=LASUF " + " WHERE LACOD='E' AND LAETB = '"
          + pCodeEtablissement + "' AND LAART='" + pCodeArticle + "' AND LAMAG = '" + magSouhaite
          + "' AND EAREC=0 AND EAETA=1 AND EAIN9 <> 'I' " + " ORDER BY DISPO ASC ";
    else
      requete = "SELECT CASE WHEN LADLP<>0 THEN LADLP ELSE EADLP END AS DISPO " + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
          + ".PGVMLBFM INNER JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBFM "
          + "ON LAETB=EAETB AND LACOD=EACOD AND LANUM=EANUM AND EASUF=LASUF " + "WHERE LACOD='E' AND LAETB = '" + pCodeEtablissement
          + "' AND LAART='" + pCodeArticle + "' AND LAMAG = '" + magSouhaite
          + "' AND EAREC=0 AND EAETA=1 AND EAIN9 <> 'I' ORDER BY DISPO ASC";
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (liste != null && liste.size() > 0) {
      GenericRecord record = null;
      
      if (ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE) {
        int i = 0;
        while (i < liste.size() && record == null) {
          if (!liste.get(i).isPresentField("QTEDISPO"))
            liste.get(i).setField("QTEDISPO", "0");
          if (liste.get(i).isPresentField("LAQTS")) {
            if (Float.valueOf(liste.get(i).getField("LAQTS").toString().trim())
                - Float.valueOf(liste.get(i).getField("QTEDISPO").toString().trim()) > Float.valueOf("0"))
              record = liste.get(i);
          }
          i++;
        }
      }
      else
        record = liste.get(0);
      
      if (record == null) {
        record = new GenericRecord();
      }
      if (!record.isPresentField("DISPO")) {
        record.setField("DISPO", "0");
      }
      
      int delaiDispo = Integer.valueOf(record.getField("DISPO").toString().trim());
      int delaiTransfert = 0;
      // Si on tient compte du stock du si�ge dans la date du r�assort de ce magasin
      if (pUtilisateur.getETB_EN_COURS().isIs_reassort_siege() && !pUtilisateur.getETB_EN_COURS().is_mono_magasin() && delaiDispo > 0) {
        // On va lire le d�lai moyen qu'il existe entre le si�ge et ce magasin
        liste =
            pUtilisateur.getAccesDB2().select("SELECT DEL_JOURS FROM " + ConstantesEnvironnement.BIBLI_WS + ".DELAISMOY WHERE DEL_ETB ='"
                + pCodeEtablissement + "' AND DEL_MGDP = '" + magSouhaite + "' AND DEL_MGAR = '" + pCodeMagasin + "' ");
        if (liste != null && liste.size() == 1 && liste.get(0).isPresentField("DEL_JOURS"))
          delaiTransfert = Integer.valueOf(liste.get(0).getField("DEL_JOURS").toString().trim());
      }
      // addition et mise en forme
      retour = Outils.mettreAjourUneDateSerieM(delaiDispo, (pUtilisateur.getETB_EN_COURS().getDelai_jours_securite() + delaiTransfert));
      if (retour == null) {
        retour = "NR";
      }
    }
    
    return retour;
  }
  
  /**
   * Retourner la totalit� des stocks attendus pour un article sur un magasin.
   */
  public String retournerStockAttendu(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle, String pCodeMagasin) {
    String retour = "0";
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null || pCodeMagasin == null) {
      return retour;
    }
    
    String requete = null;
    String magSouhaite = null;
    Float attendu = null;
    Float reserve = new Float(0);
    
    // Choix du magasin � scanner
    if (pUtilisateur.getETB_EN_COURS().isIs_reassort_siege() && !pUtilisateur.getETB_EN_COURS().is_mono_magasin()
        && pUtilisateur.getMagasinSiege() != null) {
      magSouhaite = pUtilisateur.getMagasinSiege().getCodeMagasin();
    }
    else {
      magSouhaite = pCodeMagasin;
    }
    
    if (magSouhaite == null) {
      return retour;
    }
    
    requete = " SELECT S1ATT FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMSTKM " + " WHERE S1ETB = '" + pCodeEtablissement
        + "' AND S1MAG = '" + magSouhaite + "' AND S1ART = '" + pCodeArticle + "' ";
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("S1ATT")) {
        attendu = Float.valueOf(liste.get(0).getField("S1ATT").toString().trim());
      }
    }
    else {
      return retour;
    }
    
    if (ConstantesEnvironnement.TRAITEMENT_STOCK_RESERVE && attendu != null) {
      requete = " SELECT SUM(AAQTE) AS RESERVE FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMALAM " + " LEFT JOIN "
          + ConstantesEnvironnement.BIBLI_CLIENTS
          + ".PGVMLBFM ON LASUF=AASOE AND LANLI=AALOE AND LAETB=AAETB AND LAART=AAART AND LANUM=AANOE " + " LEFT JOIN "
          + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBFM ON LAETB=EAETB AND LACOD=EACOD AND LANUM=EANUM AND EASUF=LASUF "
          + " WHERE AATOS='V' AND AATOE IN ('G' , 'A') AND EAREC=0 AND EAETA=1 AND EAIN9 <> 'I' AND LACOD='E' " + " AND LAETB = '"
          + pCodeEtablissement + "' AND LAART='" + pCodeArticle + "' AND LAMAG = '" + magSouhaite + "' ";
      
      liste = pUtilisateur.getAccesDB2().select(requete, this.getClass());
      
      if (liste != null && liste.size() == 1) {
        if (liste.get(0).isPresentField("RESERVE")) {
          reserve = Float.valueOf(liste.get(0).getField("RESERVE").toString().trim());
        }
      }
    }
    
    attendu = attendu - reserve;
    retour = attendu.toString();
    
    return retour;
  }
  
  /**
   * Retourne l'article de substitution de l'article transmis s'il en existe un.
   * Nous r�cup�rons les types de substitution suivants:
   * Rupture de stock A1IN4 = ' ' A1ASB = [code article valide]
   * Aiguillage A1IN4 = 'A' A1ASB = [code article valide]
   * Equivalent A1IN4 = 'E' A1ASB = [code article valide]
   * Remplacement A1IN4 = 'R' A1ASB = [code article valide]
   */
  public GenericRecord retourneArticleSubstitution(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    String requete = " SELECT A1IN4,A1ASB FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + "." + TABLE_ARTICLE + " WHERE A1ETB = '"
        + pCodeEtablissement + "' AND A1ART = '" + pCodeArticle + "' ";
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (liste != null && liste.size() == 1) {
      GenericRecord recTemp = liste.get(0);
      // On v�rifie qu'on ait un code article de substitution
      if (recTemp.isPresentField("A1ASB") && !recTemp.getField("A1ASB").toString().trim().equals("")) {
        GenericRecord recArticle = retourneDetailArticle(pUtilisateur, pCodeEtablissement, recTemp.getField("A1ASB").toString().trim());
        if (recArticle != null && recArticle.isPresentField("A1ART")) {
          // On v�rifie que le type de substitution existe et soit dans le pattern d�fini
          if (recTemp.isPresentField("A1IN4") && recTemp.getField("A1IN4") != null) {
            String codeSub = recTemp.getField("A1IN4").toString().trim();
            recArticle.setField("CODESUB", codeSub);
            if (codeSub.equals("") && pUtilisateur.getETB_EN_COURS().voir_Subst_Rupture()) {
              recArticle.setField("LIBSUB", pUtilisateur.getTraduction().traduire("$$infoRupture"));
            }
            else if (codeSub.equals("A") && pUtilisateur.getETB_EN_COURS().voir_Subst_Aiguill()) {
              recArticle.setField("LIBSUB", pUtilisateur.getTraduction().traduire("$$infoAiguillage"));
            }
            else if (codeSub.equals("E") && pUtilisateur.getETB_EN_COURS().voir_Subst_Equival()) {
              recArticle.setField("LIBSUB", pUtilisateur.getTraduction().traduire("$$infoEquivalent"));
            }
            else if (codeSub.equals("R") && pUtilisateur.getETB_EN_COURS().voir_Subst_Remplac()) {
              recArticle.setField("LIBSUB", pUtilisateur.getTraduction().traduire("$$infoRemplacement"));
            }
            
            return recArticle;
          }
        }
      }
    }
    
    return null;
  }
  
  /**
   * En fonction des param�tres transmis, on trie la liste d'article d�j� pr�sente.
   */
  public void filtrerUneListeArticles(Utilisateur pUtilisateur) {
    if (pUtilisateur.getDerniereListeArticle() == null) {
      return;
    }
    
    if (pUtilisateur.getDerniereListeArticle().size() <= 1) {
      return;
    }
    
    String dispo = null;
    String attendu = null;
    // Tri par stock
    if (pUtilisateur.getFiltres().isStock_positif()) {
      ArrayList<GenericRecord> listeFiltree = new ArrayList<GenericRecord>();
      ArrayList<GenericRecord> listeRebus = new ArrayList<GenericRecord>();
      BigDecimal valeur = null;
      
      for (int i = 0; i < pUtilisateur.getDerniereListeArticle().size(); i++) {
        dispo = null;
        attendu = null;
        
        if (pUtilisateur.getETB_EN_COURS().voir_stock_total()) {
          dispo =
              retourneDispoArticleSQLunMagasin(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(), null);
        }
        else {
          dispo = retourneDispoArticleSQL(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString());
        }
        // Si le stock est null ou < � 0
        if (dispo == null || dispo.startsWith("-"))
          dispo = "0.000";
        // On rajoute le stock � l'article
        pUtilisateur.getDerniereListeArticle().get(i).setField("STOCK", dispo);
        
        // Gestion du stock attendu dans la liste... BEEUUUUURK
        if (pUtilisateur.getMonPanier() != null && pUtilisateur.getETB_EN_COURS().isVoir_stock_attendu()
            && pUtilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI
            && pUtilisateur.getMonPanier().getMagasinPanier() != null) {
          attendu = retournerStockAttendu(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
              pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
          if (attendu != null) {
            pUtilisateur.getDerniereListeArticle().get(i).setField("ATTEN", attendu);
          }
        }
        
        try {
          valeur = new BigDecimal(dispo);
          if (valeur.compareTo(BigDecimal.ZERO) == 0) {
            listeRebus.add(pUtilisateur.getDerniereListeArticle().get(i));
          }
          else {
            listeFiltree.add(pUtilisateur.getDerniereListeArticle().get(i));
          }
        }
        catch (Exception e) {
          listeRebus.add(pUtilisateur.getDerniereListeArticle().get(i));
        }
      }
      
      pUtilisateur.setDerniereListeArticle(listeFiltree);
    }
    else {
      for (int i = 0; i < pUtilisateur.getDerniereListeArticle().size(); i++) {
        dispo = null;
        if (pUtilisateur.getETB_EN_COURS().voir_stock_total()) {
          dispo =
              retourneDispoArticleSQLunMagasin(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                  pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(), null);
        }
        else {
          dispo = retourneDispoArticleSQL(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString());
        }
        // Si le stock est null ou < � 0
        if (dispo == null || dispo.startsWith("-"))
          dispo = "0.000";
        // On rajoute le stock � l'article
        pUtilisateur.getDerniereListeArticle().get(i).setField("STOCK", dispo);
        
        // Gestion du stock attendu dans la liste... BEEUUUUURK
        if (pUtilisateur.getMonPanier() != null && pUtilisateur.getETB_EN_COURS().isVoir_stock_attendu()
            && pUtilisateur.getMonPanier().getModeRecup() != ConstantesEnvironnement.MODE_NON_CHOISI
            && pUtilisateur.getMonPanier().getMagasinPanier() != null) {
          attendu = retournerStockAttendu(pUtilisateur, pUtilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
              pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
          if (attendu != null) {
            pUtilisateur.getDerniereListeArticle().get(i).setField("ATTEN", attendu);
          }
        }
      }
    }
  }
  
  /**
   * Retourner les articles de substitution de type variante d'un article pass� en param�tre.
   */
  public ArrayList<GenericRecord> retournerVarianteDunArticle(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pCodeEtablissement == null || pCodeArticle == null) {
      return null;
    }
    
    String requete = " SELECT A1ETB, A1ART, A1IN4, A1ASB, A1CL1, A1CL2, A1FAM, CAREF FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
        + ".VUE_ART_UT " + " WHERE A1ETB = '" + pCodeEtablissement + "' AND A1ART = '" + pCodeArticle + "' AND A1IN4 = 'V' ";
    
    ArrayList<GenericRecord> listeSub = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (listeSub != null && listeSub.size() == 1) {
      // On est dans le cas d'un article qui poss�de une variante d�clar�e
      if (listeSub.get(0).isPresentField("A1ASB") && !listeSub.get(0).getField("A1ASB").toString().trim().equals("")) {
        if (gestionVariantes == null) {
          gestionVariantes = new GestionVariantes();
        }
        
        return gestionVariantes.retournerArticlesVariantes(pUtilisateur, pCodeEtablissement, pCodeArticle, listeSub.get(0));
      }
    }
    
    return null;
  }
  
  /**
   * Retourner les paniers intuitifs actifs dans l'�tablissement pass� en param�tre.
   * Si pas d'�tablissement: tous les �tablissements seront pris.
   */
  public static ArrayList<GenericRecord> recupererPanierIntuitifsActifs(Utilisateur pUtilisateur) {
    if (pUtilisateur == null) {
      return null;
    }
    
    String requete = " SELECT IN_ID,IN_LIBELLE FROM " + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF WHERE IN_BIB = '"
        + pUtilisateur.getBibli() + "' AND IN_ETB  = '" + pUtilisateur.getETB_EN_COURS() + "' AND IN_STATUT = '1' ";
    
    if (pUtilisateur.getETB_EN_COURS() == null) {
      requete = " SELECT IN_ID,IN_LIBELLE FROM " + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF WHERE IN_BIB = '"
          + pUtilisateur.getBibli() + "' AND IN_STATUT = '1' ";
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(requete);
    if (liste != null && liste.size() == 0) {
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourne la liste des articles du panier intuitif.
   */
  public ArrayList<GenericRecord> retournerListeArticlesPanierIntuitif(Utilisateur pUtilisateur, String pIdPanier) {
    if (pUtilisateur == null || pIdPanier == null) {
      return null;
    }
    
    String filtreFrs = " ";
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(
        " SELECT ART_CODE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ART_INTUIT WHERE ART_IN_ID = '" + pIdPanier + "' ",
        this.getClass());
    
    // Il existe des articles dans les favoris de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (pUtilisateur.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + pUtilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + pUtilisateur.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      for (int i = 0; i < liste.size(); i++) {
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        arguments += liste.get(i).getField("ART_CODE").toString().trim() + "'";
      }
      arguments += ")";
      
      liste = pUtilisateur.getAccesDB2().select(
          " SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, LIBCND , CND , A1TVA, A1COF, A1FRS, FRNOM " + " FROM "
              + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '" + pUtilisateur.getETB_EN_COURS().getCodeETB()
              + "' AND A1ART IN " + arguments + filtreFrs + " ORDER BY A1ABC, A1SAI DESC, A1LIB FETCH FIRST "
              + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + pUtilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
    }
    
    // Attribuer la liste � l'utilisateur
    if (pUtilisateur.getDernierFiltreFournisseur() == null) {
      pUtilisateur.setDerniereListePanierIntuitif(liste);
    }
    
    // Attribuer la liste � l'utilisateur
    pUtilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * R�cup�rer les promotions li�es � une s�lection par groupe, par familles ou par sous famille.
   */
  public static ArrayList<GenericRecord> recupererPromotionsParCatalogue(Utilisateur pUtilisateur, int pNbPromosMax) {
    if (pUtilisateur == null || pUtilisateur.getListePromotionsActives() == null
        || (pUtilisateur.getDerniereFamilleArticles() == null && pUtilisateur.getDernierGroupeArticles() == null)) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = new ArrayList<GenericRecord>();
    String valeur = null;
    
    // On recherche par famille ou sous famille
    if (pUtilisateur.getDerniereFamilleArticles() != null) {
      valeur = pUtilisateur.getDerniereFamilleArticles();
      // famille
      if (valeur.startsWith("FA")) {
        valeur = valeur.replaceFirst("FA", "");
        int i = 0;
        while (liste.size() <= pNbPromosMax && i < pUtilisateur.getListePromotionsActives().size()) {
          if (pUtilisateur.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
              && pUtilisateur.getListePromotionsActives().get(i).isPresentField("A1FAM")) {
            if (pUtilisateur.getListePromotionsActives().get(i).getField("A1FAM").toString().trim().equals(valeur)) {
              liste.add(pUtilisateur.getListePromotionsActives().get(i));
            }
          }
          i++;
        }
      }
      // sous famille
      else if (valeur.startsWith("SF")) {
        valeur = valeur.replaceFirst("SF", "");
        int i = 0;
        while (liste.size() <= pNbPromosMax && i < pUtilisateur.getListePromotionsActives().size()) {
          if (pUtilisateur.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
              && pUtilisateur.getListePromotionsActives().get(i).isPresentField("A1SFA")) {
            if (pUtilisateur.getListePromotionsActives().get(i).getField("A1SFA").toString().trim().equals(valeur)) {
              liste.add(pUtilisateur.getListePromotionsActives().get(i));
            }
          }
          i++;
        }
      }
    }
    // On recherche par groupe
    else {
      valeur = pUtilisateur.getDernierGroupeArticles();
      int i = 0;
      while (liste.size() < pNbPromosMax && i < pUtilisateur.getListePromotionsActives().size()) {
        if (pUtilisateur.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
            && pUtilisateur.getListePromotionsActives().get(i).isPresentField("A1FAM")) {
          if (pUtilisateur.getListePromotionsActives().get(i).getField("A1FAM").toString().trim().startsWith(valeur)) {
            liste.add(pUtilisateur.getListePromotionsActives().get(i));
          }
        }
        i++;
      }
    }
    
    if (liste == null || liste.size() == 0) {
      return null;
    }
    return liste;
  }
  
  // -- M�thodes publiques pour les services REST
  
  /**
   * R�cup�re les prix nets HT et publics HT d'une liste d'articles (version non optimis�e).
   */
  public void recupererListePrixArticle(Utilisateur pUtilisateur, ListePrixArticle pListePrixArticle) {
    if (pListePrixArticle == null || pListePrixArticle.getListePrix() == null || pListePrixArticle.getListePrix().isEmpty()) {
      return;
    }
    
    long t1 = System.currentTimeMillis();
    // Requ�te afin de r�cup�rer les informations n�cessaires sur les codes articles
    String codeEtablissement = pListePrixArticle.getCodeEtablissement();
    // Construction de la requ�te
    String requete = "select A1ART, A1UNV from " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT where A1ETB = '"
        + codeEtablissement + "' and A1ART in (";
    for (PrixArticle prixArticle : pListePrixArticle.getListePrix()) {
      requete += "'" + traiterCaracteresSpeciauxSQL(prixArticle.getCodeArticle()) + "', ";
    }
    requete = requete.substring(0, requete.length() - 2);
    requete += ')';
    // Ex�cution de la requ�te
    ArrayList<GenericRecord> listeRecord = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (listeRecord == null || listeRecord.isEmpty()) {
      pListePrixArticle.setTexteErreur("Les codes articles fournis ne sont pas pr�sents dans l'�tablissement " + codeEtablissement + '.');
      return;
    }
    long t2 = System.currentTimeMillis() - t1;
    
    // Traitement des donn�es r�cup�r�es
    long t10 = 0;
    long t11 = 0;
    for (GenericRecord record : listeRecord) {
      long t3 = System.currentTimeMillis();
      
      // R�cup�ration de l'objet PrixArticle � partir du code article
      PrixArticle prixArticle = pListePrixArticle.retournerPrixArticle(record.getString("A1ART"));
      if (prixArticle == null) {
        forcerLogMessage("L'article " + record.getString("A1ART") + " n'a pas �t� trouv� dans la liste.");
        continue;
      }
      String unite = record.getString("A1UNV").trim();
      
      // R�cup�ration du prix net HT
      BigDecimal prixNetHT = retournerTarifClient(pUtilisateur, codeEtablissement, prixArticle.getCodeArticle(), unite);
      prixArticle.setPrixNetHT(prixNetHT);
      int precision = prixNetHT.scale();
      long t4 = System.currentTimeMillis() - t3;
      t10 += t4;
      
      long t5 = System.currentTimeMillis();
      // R�cup�ration du prix public HT
      BigDecimal prixPublicHT = BigDecimal.ZERO;
      GenericRecord recordTarif = retourneTarifGeneralArticle(pUtilisateur, codeEtablissement, prixArticle.getCodeArticle(), unite);
      if (recordTarif != null) {
        prixPublicHT = recordTarif.getDecimal("ATP01");
      }
      // La pr�cision du prix net est appliqu�e au prix public car la m�thode retourneTarifGeneralArticle() retourne le prix avec 4
      // d�cimales
      prixPublicHT = prixPublicHT.setScale(precision, RoundingMode.HALF_UP);
      prixArticle.setPrixPublicHT(prixPublicHT);
      long t6 = System.currentTimeMillis() - t5;
      t11 += t6;
      // System.out.println("--> " + prixArticle.getCodeArticle() + " tps prix net:" + t4 + " tps prix public:" + t6);
    }
    System.out.println("--> Cumul tps unit�s:" + t2 + " tps prix net:" + t10 + " tps prix public:" + t11);
  }
  
  /**
   * R�cup�re les prix nets HT et publics HT d'une liste d'articles (version optimis�e).
   */
  public void recupererListePrixArticleOptimisee(Utilisateur pUtilisateur, ListePrixArticle pListePrixArticle) {
    if (pListePrixArticle == null || pListePrixArticle.getListePrix() == null || pListePrixArticle.getListePrix().isEmpty()) {
      return;
    }
    
    long t1 = System.currentTimeMillis();
    
    // Requ�te afin de r�cup�rer les informations n�cessaires sur les codes articles
    String codeEtablissement = pListePrixArticle.getCodeEtablissement();
    // Construction de la requ�te
    String requete = "select A1ART, A1UNV from " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT where A1ETB = '"
        + codeEtablissement + "' and A1ART in (";
    for (PrixArticle prixArticle : pListePrixArticle.getListePrix()) {
      requete += "'" + traiterCaracteresSpeciauxSQL(prixArticle.getCodeArticle()) + "', ";
    }
    requete = requete.substring(0, requete.length() - 2);
    requete += ')';
    // Ex�cution de la requ�te
    ArrayList<GenericRecord> listeRecord = pUtilisateur.getAccesDB2().select(requete, this.getClass());
    if (listeRecord == null || listeRecord.isEmpty()) {
      pListePrixArticle.setTexteErreur("Les codes articles fournis ne sont pas pr�sents dans l'�tablissement " + codeEtablissement + '.');
      return;
    }
    long t2 = System.currentTimeMillis() - t1;
    System.out.println("--> Cumul tps unit�s:" + t2);
    
    // Mise � jour de l'unit� pour chaque article
    for (GenericRecord record : listeRecord) {
      // R�cup�ration de l'objet PrixArticle � partir du code article
      PrixArticle prixArticle = pListePrixArticle.retournerPrixArticle(record.getString("A1ART"));
      if (prixArticle == null) {
        forcerLogMessage("L'article " + record.getString("A1ART") + " n'a pas �t� trouv� dans la liste.");
        continue;
      }
      String unite = record.getString("A1UNV").trim();
      prixArticle.setUnite(unite);
    }
    
    // Appel de la m�thode ensembliste
    recupererPrixArticle(pUtilisateur, codeEtablissement, pListePrixArticle);
  }
  
  // -- M�thodes priv�es
  
  /**
   * Permet de r�cup�rer le prix public et le prix net client d'une liste d'articles.
   * Pour la version optimis�e.
   */
  private void recupererPrixArticle(Utilisateur pUtilisateur, String pCodeEtablissement, ListePrixArticle pListePrixArticle) {
    if (pUtilisateur == null || pCodeEtablissement == null || pListePrixArticle == null) {
      return;
    }
    
    long t1 = System.currentTimeMillis();
    // R�cup�ration des prix net HT clients pour la liste des articles
    pListePrixArticle.setNombreDecimale(pUtilisateur.getETB_EN_COURS().getDecimales_client());
    pUtilisateur.controleGestionTarif();
    boolean retour =
        pUtilisateur.getGestionTarif().execute(pCodeEtablissement, new BigDecimal(pUtilisateur.getClient().getNumeroClient()),
            new BigDecimal(pUtilisateur.getClient().getSuffixeClient()), pListePrixArticle, BigDecimal.ONE);
    if (!retour) {
      return;
    }
    long t2 = System.currentTimeMillis() - t1;
    
    long t3 = System.currentTimeMillis();
    // R�cup�ration des prix publics
    for (PrixArticle prixArticle : pListePrixArticle.getListePrix()) {
      // R�cup�ration du prix public HT
      BigDecimal prixPublicHT = BigDecimal.ZERO;
      GenericRecord recordTarif =
          retourneTarifGeneralArticle(pUtilisateur, pCodeEtablissement, prixArticle.getCodeArticle(), prixArticle.getUnite());
      if (recordTarif != null) {
        prixPublicHT = recordTarif.getDecimal("ATP01");
      }
      prixArticle.modifierPrixPublicBrut(prixPublicHT);
    }
    long t4 = System.currentTimeMillis() - t3;
    System.out.println("--> Cumul tps prix net:" + t2 + " tps prix public:" + t4);
  }
  
  /**
   * Permet de v�rifier si un article est en promotion.
   */
  private static boolean estEnPromotion(String pCodeEtablissement, String pCodeArticle, Utilisateur pUtilisateur) {
    if (pCodeEtablissement == null || pCodeArticle == null || pUtilisateur == null) {
      return false;
    }
    
    boolean estEnpromo = false;
    if (pUtilisateur.getListePromotionsActives() != null) {
      int i = 0;
      while (estEnpromo == false && i < pUtilisateur.getListePromotionsActives().size()) {
        if (pUtilisateur.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
            && pUtilisateur.getListePromotionsActives().get(i).getField("PR_A1ART") != null
            && pUtilisateur.getListePromotionsActives().get(i).isPresentField("PR_A1ETB")
            && pUtilisateur.getListePromotionsActives().get(i).getField("PR_A1ETB") != null) {
          estEnpromo = (pUtilisateur.getListePromotionsActives().get(i).getField("PR_A1ART").toString().trim().equals(pCodeArticle)
              && pUtilisateur.getListePromotionsActives().get(i).getField("PR_A1ETB").toString().trim().equals(pCodeEtablissement));
        }
        
        i++;
      }
    }
    
    return estEnpromo;
  }
  
  /**
   * Retourne la magasin de retrait.
   * @param pUtilisateur
   * @return
   */
  private String retourneMagasinRetrait(Utilisateur pUtilisateur) {
    String magasinRetrait = " ";
    if ((pUtilisateur.getMonPanier() != null) && (pUtilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_RETRAIT)
        && !pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin().trim()
            .equals(pUtilisateur.getMagasinSiege().getCodeMagasin().trim()))
      magasinRetrait = pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
    
    return magasinRetrait;
  }
  
  /**
   * R�cup�rer l'URL d'une image article si elle existe dans PSEMURLM.
   */
  private String recupererPhotoURL(Utilisateur pUtilisateur, String pCodeEtablissement, String pCodeArticle) {
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      return null;
    }
    pCodeEtablissement = traiterCaracteresSpeciauxSQL(pCodeEtablissement);
    pCodeArticle = traiterCaracteresSpeciauxSQL(pCodeArticle);
    String retour = null;
    
    // Si le PSEMURLM existe
    if (ConstantesEnvironnement.PSEMURLM_IS_OK) {
      ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select(
          "SELECT URADR FROM " + pUtilisateur.getBibli() + ".PSEMURNM WHERE URCOD = 'A' AND URETB = '" + pCodeEtablissement.trim()
              + "' AND URARG = '" + pCodeArticle.trim() + "' AND URTYP = 'PHW'  " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
      
      if (liste != null && liste.size() > 0 && liste.get(0).isPresentField("URADR")) {
        retour = "src='" + liste.get(0).getField("URADR").toString().trim() + "' ";
      }
    }
    
    return retour;
  }
  
  /**
   * Permet de v�rifier la pr�sence du PSEMURLM une seule fois.
   */
  private boolean verifierPresencePSEMURLM(Utilisateur pUtilisateur) {
    // Si on a pas d�j� check� le bouzin
    if (!ConstantesEnvironnement.CHECK_PSEMURLM) {
      ArrayList<GenericRecord> liste = null;
      if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
        return false;
      }
      liste = pUtilisateur.getAccesDB2().select(
          "SELECT 1 FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA = '" + pUtilisateur.getBibli() + "' AND TABLE_NAME = 'PSEMURNM' ",
          this.getClass());
      
      ConstantesEnvironnement.CHECK_PSEMURLM = true;
      ConstantesEnvironnement.PSEMURLM_IS_OK = (liste != null && liste.size() == 1);
    }
    
    return ConstantesEnvironnement.PSEMURLM_IS_OK;
  }
  
}
