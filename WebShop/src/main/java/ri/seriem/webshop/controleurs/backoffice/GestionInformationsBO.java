
package ri.seriem.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Classe permettant de modifier les informations du magasin pour le site institutionnel
 * 
 *
 */
public class GestionInformationsBO extends Gestion {
  
  public GestionInformationsBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Recuperer les donn�es du magasin que l'on veut modifier
   */
  public ArrayList<GenericRecord> recupererUneInformation(Utilisateur utilisateur, String langue) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || langue == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT INF_ID,INF_TXT,INF_DES,INF_IMG,INF_IMG2,INF_TXT2,INF_DES2 FROM "
        + ConstantesEnvironnement.BIBLI_WS + ".INFOSW WHERE INF_LAN = '" + langue + "'", this.getClass());
  }
  
  /**
   * Mise � jour de la table INFORMATION de XWEBSHOP
   */
  public int miseajourInformation(Utilisateur utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return -1;
    
    // On supprime d'�ventuels enregistrements pour ce code langue
    if (request.getParameter("langueInfo") != null)
      res = utilisateur.getAccesDB2().requete(
          "DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".INFOSW WHERE INF_LAN = '" + request.getParameter("langueInfo") + "'",
          this.getClass());
    
    // On rajoute un nouvel enregistrement pour ce code langue
    if (res > -1) {
      res = utilisateur.getAccesDB2().requete(
          "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".INFOSW (INF_TXT,INF_DES,INF_IMG,INF_IMG2,INF_TXT2,INF_DES2,INF_LAN) VALUES "
              + "('" + traiterCaracteresSpeciauxSQL(request.getParameter("texte1")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("des1")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("img1")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("img2")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("texte2")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("des2")) + "','" + (request.getParameter("langueInfo")) + "')",
          this.getClass());
    }
    
    return res;
  }
  
  /** traite l'upload des images informations */
  public int upload(Utilisateur utilisateur, HttpServletRequest request, String filename) {
    String uploadDirectory = ConstantesEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separator;
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    int res = -1;
    
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploader*/
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requ�te*/
    
    /*construction du repertoire ou sera stocker le fichier uploader. */
    String uploadPath = uploadDirectory;
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(request);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recup�r�s
      while (iter.hasNext()) {
        FileItem item = (FileItem) iter.next();
        
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          filename = item.getName();
          String filePath = uploadPath + filename; // chemin du fichier avec le nom du fichier
          File storeFile = new File(filePath);
          // Enregistrer le fichier sur le disque
          item.write(storeFile);
          res = 1;
        }
      }
      
    }
    catch (Exception ex) {
      res = -1;
    }
    return res;
  }
  
}
