
package ri.seriem.webshop.controleurs.backoffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.controleurs.GestionCatalogue;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.ArticleIntuitif;
import ri.seriem.webshop.metier.PanierIntuitif;
import ri.seriem.webshop.outils.Outils;

public class GestionPanierIntuitifBO extends Gestion {
  
  private GestionCatalogue gestionCatalogue = null;
  public final static String PATTERN_IMAGE = "panierIntuitif_";
  public final String IMAGE_DEFAUT = "panierIntuitifVide";
  public final static String EXTENSION_IMAGE = ".jpg";
  public static String repertoiresImages = ConstantesEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separator;
  
  /**
   * Retourne la liste des paniers actifs
   */
  public ArrayList<GenericRecord> retournerListePaniers(Utilisateur pUtil) {
    if (loggerSiTrue("Utilisateur � NUll", (pUtil == null || pUtil.getAccesDB2() == null)))
      return null;
    
    return pUtil.getAccesDB2().select(
        " SELECT COUNT(ART_CODE) AS NMB, IN_ID, IN_ETB, IN_LIBELLE,IN_STATUT,IN_CREA,IN_MODIF " + " FROM "
            + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_WS
            + ".ART_INTUIT ON IN_ID = ART_IN_ID " + " WHERE IN_BIB = '" + pUtil.getBibli() + "' "
            + " GROUP BY IN_ID, IN_ETB, IN_LIBELLE, IN_STATUT, IN_CREA, IN_MODIF " + " ORDER BY IN_ETB, IN_STATUT DESC, IN_LIBELLE ",
        this.getClass());
  }
  
  /**
   * Permet de cr�er un nouveau panier intuitif
   */
  public int creerNouveauPanierIntuitif(Utilisateur pUtil, PanierIntuitif pPanier) {
    int retour = -2;
    
    if (pPanier == null || pPanier.getLibelle() == null || pPanier.getLibelle().trim().equals("") || pPanier.getStatut() < 0
        || pPanier.getEtb() == null) {
      return retour;
    }
    
    retour = pUtil.getAccesDB2()
        .requete(" INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF "
            + " (IN_BIB,IN_ETB,IN_STATUT,IN_LIBELLE,IN_CREA,IN_MODIF) " + " VALUES ('" + pUtil.getBibli() + "','" + pPanier.getEtb() + "','"
            + pPanier.getStatut() + "','" + traiterCaracteresSpeciauxSQL(pPanier.getLibelle().trim()) + "', '" + pPanier.getDateCreation()
            + "', '" + pPanier.getDateModification() + "') ", this.getClass());
    
    if (retour > 0) {
      // SELECT le dernier insert et le l'attribuer � retour
      ArrayList<GenericRecord> liste = pUtil.getAccesDB2().select("SELECT MAX(IN_ID) AS MAXID FROM " + ConstantesEnvironnement.BIBLI_WS
          + ".INTUITIF " + " WHERE IN_BIB = '" + pUtil.getBibli() + "' " + " ", this.getClass());
      
      // TODO
      if (liste != null && liste.get(0).isPresentField("MAXID")) {
        retour = Integer.parseInt(liste.get(0).getField("MAXID").toString().trim());
      }
    }
    else
      retour = -1;
    
    return retour;
  }
  
  /**
   * retourne un panier intuitif existant sur la base de son id
   */
  public PanierIntuitif retournerUnPanier(Utilisateur pUtil, String pId) {
    if (pUtil == null || pId == null) {
      return null;
    }
    
    PanierIntuitif panier = null;
    ArrayList<GenericRecord> liste = pUtil.getAccesDB2()
        .select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF WHERE IN_ID = '" + pId + "' ", this.getClass());
    
    if (liste != null && liste.size() == 1) {
      panier = new PanierIntuitif(liste.get(0));
      
      ArrayList<GenericRecord> listeA = pUtil.getAccesDB2().select(
          "SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ART_INTUIT WHERE ART_IN_ID = '" + pId + "' ORDER BY ART_ORDRE ",
          this.getClass());
      if (listeA != null && listeA.size() > 0) {
        for (GenericRecord record : listeA) {
          if (record.isPresentField("ART_CODE")) {
            panier.ajouterUnArticle(pUtil, record.getField("ART_CODE").toString().trim());
          }
        }
      }
    }
    
    return panier;
  }
  
  /**
   * Met � jour un panier intuitif en base de donn�es
   **/
  public int modifierUnPanierIntuitif(Utilisateur pUtil, PanierIntuitif pPanier) {
    if (pUtil == null || pPanier == null) {
      return -2;
    }
    
    // D'abord on met � jour le panier en lui-m�me
    int retour = pUtil.getAccesDB2()
        .requete(" UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF " + " SET IN_ETB = '" + pPanier.getEtb() + "', IN_STATUT = '"
            + pPanier.getStatut() + "', IN_LIBELLE = '" + traiterCaracteresSpeciauxSQL(pPanier.getLibelle().trim()) + "', IN_MODIF = '"
            + Outils.recupererDateCouranteInt() + "' " + " WHERE IN_ID = '" + pPanier.getIdPanier() + "' ", this.getClass());
    
    checkerExistenceImagePanier(pPanier.getIdPanier());
    
    // On d�tecte d'�ventuels articles on les supprime on rajoute les nouveaux.
    int supp = pUtil.getAccesDB2().requete(
        " DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ART_INTUIT " + " WHERE ART_IN_ID = '" + pPanier.getIdPanier() + "' ",
        this.getClass());
    
    if (supp > -1 && pPanier.getListeArticles() != null) {
      int addTheorique = pPanier.getNombreArticle();
      int add = 0;
      int ret = 0;
      for (ArticleIntuitif article : pPanier.getListeArticles()) {
        ret =
            pUtil.getAccesDB2()
                .requete(" INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ART_INTUIT " + " (ART_IN_ID,ART_CODE,ART_ORDRE) "
                    + " VALUES ('" + pPanier.getIdPanier() + "','" + article.getCodeArticle() + "','" + article.getOrdre() + "') ",
                    this.getClass());
        
        if (ret == 1) {
          add++;
        }
      }
      
      if (add == addTheorique) {
        retour = 1;
      }
    }
    
    return retour;
  }
  
  /**
   * Supprime un panier intuitif en base de donn�es sur la base de son ID
   */
  public int supprimerUnPanierIntuitif(Utilisateur pUtil, String pId) {
    if (pUtil == null || pId == null) {
      return -2;
    }
    
    // On d�tecte d'�ventuels articles on les supprime et on rajoute les nouveaux.
    int retour = pUtil.getAccesDB2().requete(
        " DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ART_INTUIT " + " WHERE ART_IN_ID = '" + pId + "' ", this.getClass());
    
    if (retour > -1) {
      retour = pUtil.getAccesDB2()
          .requete(" DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".INTUITIF " + " WHERE IN_ID = '" + pId + "' ", this.getClass());
    }
    
    if (retour == 1) {
      supprimerImagePanierIntuitif(pId);
    }
    
    return retour;
  }
  
  /**
   * On met � jour les informations d'un article intuitif
   */
  public void majInfosArticle(Utilisateur pUtil, String pEtb, ArticleIntuitif pArticle) {
    if (pEtb == null || pArticle == null) {
      return;
    }
    
    if (gestionCatalogue == null) {
      gestionCatalogue = new GestionCatalogue();
    }
    
    pArticle.setEtb(pEtb);
    
    GenericRecord record = gestionCatalogue.retourneDetailArticle(pUtil, pEtb, pArticle.getCodeArticle());
    
    if (record != null) {
      if (record.isPresentField("A1LIB")) {
        pArticle.setLibelle(record.getField("A1LIB").toString().trim());
      }
      if (record.isPresentField("CAREF")) {
        pArticle.setReferenceFourn(record.getField("CAREF").toString().trim());
      }
      if (record.isPresentField("A1UNV")) {
        pArticle.setUnite(record.getField("A1UNV").toString().trim());
      }
      
      if (pArticle.getUnite() != null) {
        GenericRecord record2 = gestionCatalogue.retourneTarifGeneralArticle(pUtil, pEtb, pArticle.getCodeArticle(), pArticle.getUnite());
        if (record2 != null) {
          if (record2.isPresentField("ATP01")) {
            pArticle.setTarifGeneral(record2.getDecimal("ATP01"));
          }
        }
      }
    }
  }
  
  /**
   * On v�rifie l'existence d'une image pour un panier. Si elle n'existe pas on l'initialise
   */
  public void checkerExistenceImagePanier(String pId) {
    int id = -1;
    try {
      id = Integer.parseInt(pId);
      File image = new File(repertoiresImages + PATTERN_IMAGE + id + EXTENSION_IMAGE);
      if (!image.isFile()) {
        creerImagePanierIntuitif(id);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Cr�er une image par d�faut pour un panier
   */
  public void creerImagePanierIntuitif(int idPanier) {
    // On r�cup�re l'image standard
    File imageOrigine = new File(repertoiresImages + IMAGE_DEFAUT + EXTENSION_IMAGE);
    if (imageOrigine.isFile()) {
      File imageFinale = new File(repertoiresImages + PATTERN_IMAGE + idPanier + EXTENSION_IMAGE);
      try {
        copyFileUsingStream(imageOrigine, imageFinale);
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * On copie un fichier image d'origine vers le m�me dossier en renommant l'image
   */
  private static void copyFileUsingStream(File source, File dest) throws IOException {
    InputStream is = null;
    OutputStream os = null;
    try {
      is = new FileInputStream(source);
      os = new FileOutputStream(dest);
      byte[] buffer = new byte[1024];
      int length;
      while ((length = is.read(buffer)) > 0) {
        os.write(buffer, 0, length);
      }
    }
    finally {
      is.close();
      os.close();
    }
  }
  
  /**
   * On supprime l'image du r�f�rentiel sur la base de son identifiant
   */
  private void supprimerImagePanierIntuitif(String pId) {
    if (pId == null) {
      return;
    }
    
    File image = new File(repertoiresImages + PATTERN_IMAGE + pId + EXTENSION_IMAGE);
    if (image.isFile()) {
      image.delete();
    }
  }
  
  /**
   * 
   * Traitement des images Promotion
   **/
  public int uploadImage(HttpServletRequest pRequest, String pIdPanier) {
    String filename = null;
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    
    int res = 0;
    
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploader*/
    
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requ�te*/
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(pRequest);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recup�r�s
      while (iter.hasNext()) {
        FileItem item = (FileItem) iter.next();
        
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          // TODO Mettre en place des contr�les d'images plus pr�cis
          if (item.getName() == null || item.getName().trim().equals("")) {
            return res;
          }
          filename = PATTERN_IMAGE + pIdPanier + EXTENSION_IMAGE;
          
          String filePath = repertoiresImages + filename;
          File storeFile = new File(filePath);
          
          // Enregistrer le fichier sur le disque
          item.write(storeFile);
          res = 1;
        }
      }
      
    }
    catch (Exception ex) {
      res = -1;
    }
    return res;
  }
}
