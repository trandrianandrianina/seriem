
package ri.seriem.webshop.controleurs;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Magasin;
import ri.seriem.webshop.modeles.DuplicDevis;
import ri.seriem.webshop.outils.EnvoiMail;
import ri.seriem.webshop.outils.Outils;

public class GestionMesDonnees extends Gestion {
  
  private EnvoiMail mails = null;
  
  public GestionMesDonnees() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_MES_DONNEES;
  }
  
  /**
   * Retourne les commandes du client
   */
  public ArrayList<GenericRecord> recupererCommandesClient(Utilisateur utilisateur, String numCli, String numLiv, String typeCde) {
    if (loggerSiTrue("Utilisateur ou Client � NUll", numCli == null || numLiv == null || utilisateur == null))
      return null;
    
    utilisateur.setDernierTypeBon(typeCde);
    String multiplicateur = "1";
    // Franc pacifique
    if (utilisateur.getETB_EN_COURS().getDecimales_client() == 0) {
      multiplicateur = "100";
    }
    
    String requete = "SELECT E1COD, DIGITS(E1NUM) AS E1NUM , E1SUF, E1CRE, E1DLP, (E1THTL*" + multiplicateur
        + ") AS E1THTL, E1ETA, E1TDV, E1DAT2, E1IN18, E1TOP, E1NCC, E1RCC " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
        + ".PGVMEBCM " + " WHERE E1ETB = '" + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND E1CLFP = " + numCli + " AND E1CLFS="
        + numLiv + " AND E1COD='" + typeCde + "' " + " ORDER BY E1CRE DESC,E1NUM DESC ,E1SUF DESC " + " FETCH FIRST "
        + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
        + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE;
    
    // TODO a modifier le jour ou il faut le faire : on ne voit que les commandes de l'utilisateur
    if (!utilisateur.isAutoriseVueCommandesSociete()) {
      requete = "SELECT E1COD, DIGITS(E1NUM) AS E1NUM , E1SUF, E1CRE, E1DLP, (E1THTL*" + multiplicateur
          + ") AS E1THTL, E1ETA, E1TDV, E1DAT2, E1IN18, E1TOP, E1NCC, E1RCC " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS
          + ".PGVMEBCM " + " WHERE E1ETB = '" + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND E1CLFP = " + numCli + " AND E1CLFS="
          + numLiv + " AND E1COD='" + typeCde + "' " + " ORDER BY E1CRE DESC,E1NUM DESC ,E1SUF DESC " + " FETCH FIRST "
          + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
          + utilisateur.getETB_EN_COURS().getLimit_requete_article() + " ROWS " + ConstantesEnvironnement.CLAUSE_OPTIMIZE;
    }
    
    return utilisateur.getAccesDB2().select(requete, this.getClass());
  }
  
  /**
   * Retourne le detail d'une commande
   */
  public ArrayList<GenericRecord> recupererDetailCommande(Utilisateur utilisateur, String cde, boolean modeReprise) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    
    if (cde == null || utilisateur == null)
      return null;
    
    if (cde.length() == 8) {
      typeCde = cde.substring(0, 1);
      numCde = cde.substring(1, 7);
      numSuf = cde.substring(7, 8);
    }
    
    ArrayList<GenericRecord> liste = null;
    String multiplicateur = "1";
    // Franc pacifique
    if (utilisateur.getETB_EN_COURS().getDecimales_client() == 0) {
      multiplicateur = "100";
    }
    
    liste = utilisateur.getAccesDB2()
        .select("SELECT L1ETB, L1ART, CAREF, A1LIB, L1UNV, L1QTE*L1KSV as L1QTE, CND, (L1PVC*" + multiplicateur + ") AS L1PVC, (L1MHT*"
            + multiplicateur + ") AS L1MHT, L1ERL, L1NLI, E1ETA , E1TDV, E1DAT2, (E1THTL*" + multiplicateur
            + ") AS E1THTL, E1RCC, E1NCC, E1IN18 " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMLBCM " + " LEFT JOIN "
            + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT ON L1ETB=A1ETB AND L1ART=A1ART " + " LEFT JOIN "
            + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM ON L1ETB=E1ETB AND L1NUM=E1NUM AND L1SUF=E1SUF AND L1COD=E1COD "
            + " WHERE L1ETB = '" + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND L1COD='" + typeCde + "' AND L1NUM = " + numCde
            + " AND L1SUF=" + numSuf + " AND L1ERL IN ('C' , 'S') " // AND L1NLI < 9700 "
            + " ORDER BY L1NLI " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    
    GestionCatalogue gestionCatalogue = null;
    if (liste != null) {
      if (modeReprise) {
        gestionCatalogue = new GestionCatalogue();
        utilisateur.controleGestionTarif();
      }
      
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("L1ART")) {
          if (modeReprise) {
            // Tarif recalcul�
            liste.get(i).setField("TARIF", gestionCatalogue.retournerTarifClient(utilisateur, utilisateur.getETB_EN_COURS().getCodeETB(),
                liste.get(i).getField("L1ART").toString(), liste.get(i).getField("L1UNV").toString().trim()));
            
            loggerSiDEBUG("[recupererDetailCommande] Article: " + liste.get(i).getField("L1ART").toString() + " -> TARIF: |"
                + liste.get(i).getField("TARIF").toString() + "|");
          }
          
          // Tarif L1PVC en mode reprise de commande
          if (liste.get(i).isPresentField("L1PVC")) {
            // Cas de l'unit� * il faut diviser par 100 pour l'affichage
            BigDecimal prix = null;
            if (liste.get(i).isPresentField("L1UNV") && liste.get(i).getField("L1UNV").toString().endsWith("*")) {
              prix = ((BigDecimal) liste.get(i).getField("L1PVC")).divide(new BigDecimal(100));
              liste.get(i).setField("L1PVC", prix);
            }
          }
          
          loggerSiDEBUG("[recupererDetailCommande] Article: " + liste.get(i).getField("L1ART").toString() + " -> L1PVC: |"
              + liste.get(i).getField("L1PVC").toString() + "|");
        }
      }
    }
    
    return liste;
  }
  
  /**
   * Reprend la commande et l'attribue au panier
   */
  public boolean repriseCommande(Utilisateur utilisateur, String numCde) {
    int qte = 0;
    
    boolean retour = false;
    if (utilisateur == null || numCde == null)
      return retour;
    
    // R�cup des donn�es de l'ent�te du bon
    GenericRecord enTete = recupererEnteteCommande(utilisateur, numCde);
    if (enTete != null) {
      utilisateur.getMonPanier().setNumCommandeSerieM(numCde);
      utilisateur.getMonPanier().videPanier(false);
      
      if (enTete.isPresentField("E1MEX")) {
        if (enTete.isPresentField("TYPECDE")) {
          // Mode devis
          if (enTete.getField("TYPECDE").toString().trim().equals("D")) {
            utilisateur.getMonPanier().setModeRecup(ConstantesEnvironnement.MODE_DEVIS);
          }
          else {
            if (enTete.getField("E1MEX").toString().trim().equals("WE"))
              utilisateur.getMonPanier().setModeRecup(ConstantesEnvironnement.MODE_RETRAIT);
            if (enTete.getField("E1MEX").toString().trim().equals("WL"))
              utilisateur.getMonPanier().setModeRecup(ConstantesEnvironnement.MODE_LIVRAISON);
          }
        }
      }
      
      if (enTete.isPresentField("E1MAG") && !utilisateur.getETB_EN_COURS().is_mono_magasin())
        utilisateur.getMonPanier().setMagasinPanier(new Magasin(utilisateur, enTete.getField("E1MAG").toString().trim()));
      
      // date r�cup�ration
      if (enTete.isPresentField("E1DLS"))
        utilisateur.getMonPanier().setDateRecuperation(Integer.parseInt(enTete.getField("E1DLS").toString()));
      // date
      if (enTete.isPresentField("E1RCC"))
        utilisateur.getMonPanier().setE1RCC(enTete.getField("E1RCC").toString());
      
      if (enTete.isPresentField("CLINOM"))
        utilisateur.getMonPanier().setCLNOM(enTete.getField("CLINOM").toString().trim());
      if (enTete.isPresentField("CLICPL"))
        utilisateur.getMonPanier().setCLCPL(enTete.getField("CLICPL").toString().trim());
      if (enTete.isPresentField("CLIRUE"))
        utilisateur.getMonPanier().setCLRUE(enTete.getField("CLIRUE").toString().trim());
      if (enTete.isPresentField("CLILOC"))
        utilisateur.getMonPanier().setCLLOC(enTete.getField("CLILOC").toString().trim());
      
      // Code postal + ville
      if (enTete.isPresentField("CLICDP"))
        utilisateur.getMonPanier().setCLPOS(Outils.affichageHTML_speciaux(enTete.getField("CLICDP").toString().trim()));
      if (enTete.isPresentField("CLIVIL"))
        utilisateur.getMonPanier().setCLVIL(Outils.affichageHTML_speciaux(enTete.getField("CLIVIL").toString().trim()));
    }
    // R�cup du d�tail du bon avec la liste d'articles concern�s
    ArrayList<GenericRecord> liste = recupererDetailCommande(utilisateur, numCde, true);
    
    if (liste != null && liste.size() > 0) {
      for (int i = 0; i < liste.size(); i++) {
        qte = Integer.parseInt(Outils.afficherValeurCorrectement(liste.get(i).getField("L1QTE").toString().trim()));
        // On v�rifie l'existence d'un v�ritable article dans cette liste
        if (liste.get(i).isPresentField("L1ETB") && liste.get(i).isPresentField("L1ART") && liste.get(i).isPresentField("TARIF")
            && liste.get(i).isPresentField("CND") && liste.get(i).isPresentField("L1UNV") && liste.get(i).isPresentField("L1ERL"))
          utilisateur.getMonPanier().ajouteArticle(liste.get(i).getField("L1ETB").toString().trim(),
              liste.get(i).getField("L1ART").toString().trim(), qte, new BigDecimal(liste.get(i).getField("TARIF").toString()),
              liste.get(i).getField("CND").toString().trim(), liste.get(i).getField("L1UNV").toString().trim(),
              liste.get(i).getField("L1ERL").toString().trim(), null);
      }
      
      verouilleCommande(utilisateur, numCde);
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Annule la commande pass�e en param�tre
   */
  public void annulationCommande(Utilisateur utilisateur, String numCde) {
    if (utilisateur == null || numCde == null) {
      return;
    }
    // TODO IL FAUDRAIT VERIFIER D'ABORD QUI FAIT CETTE ANNULATION
    utilisateur.controleGestionCommandes();
    
    if (utilisateur.getGestionCommandes() != null && numCde.length() == 8) {
      try {
        if (utilisateur.getGestionCommandes().execute(numCde.substring(0, 1), utilisateur.getETB_EN_COURS().getCodeETB(),
            new BigDecimal(numCde.substring(1, 7)), new BigDecimal(numCde.substring(7, 8)))) {
          if (mails == null) {
            mails = new EnvoiMail(utilisateur);
          }
          if (utilisateur.getETB_EN_COURS().isOk_envois_mails()) {
            String typeDoc = "commande";
            if (numCde.substring(0, 1).equals("D")) {
              typeDoc = "devis";
            }
            mails.envoyerUnMail(utilisateur.getUS_LOGIN(), "Annulation de " + typeDoc + "",
                retournerMessageAnnulation(numCde.substring(1, 8), typeDoc, numCde.substring(0, 1)));
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Retourne l'ent�te de ma commande
   */
  public GenericRecord recupererEnteteCommande(Utilisateur utilisateur, String cde) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    
    if (cde == null || cde.length() != 8 || utilisateur == null)
      return null;
    
    typeCde = cde.substring(0, 1);
    numCde = cde.substring(1, 7);
    numSuf = cde.substring(7, 8);
    
    String multiplicateur = "1";
    // Franc pacifique
    if (utilisateur.getETB_EN_COURS().getDecimales_client() == 0) {
      multiplicateur = "100";
    }
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2()
        .select("SELECT E1COD, E1MAG, E1MEX, E1DLS, E1DLP, E1ETA, E1TDV, E1DAT2, E1CRE, (E1THTL*" + multiplicateur
            + ") AS E1THTL, E1RCC, E1NCC, E1IN18, " + " CASE WHEN AVNOM IS NOT NULL THEN AVNOM ELSE CLNOM END AS CLINOM, "
            + " CASE WHEN AVCPL IS NOT NULL THEN AVCPL ELSE CLCPL END AS CLICPL, "
            + " CASE WHEN AVRUE IS NOT NULL THEN AVRUE ELSE CLRUE END AS CLIRUE, "
            + " CASE WHEN AVLOC IS NOT NULL THEN AVLOC ELSE CLLOC END AS CLILOC, "
            + " CASE WHEN AVCDP IS NOT NULL THEN AVCDP ELSE SUBSTR(CLVIL, 1, 5) END AS CLICDP, "
            + " CASE WHEN AVVIL IS NOT NULL THEN AVVIL ELSE SUBSTR(CLVIL, 6, 24)  END AS CLIVIL " + " FROM "
            + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMCLIM ON E1ETB=CLETB AND E1CLFP=CLCLI AND E1CLFS=CLLIV" + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMADVM ON E1ETB=AVETB AND E1NUM=AVNUM AND E1SUF=AVSUF AND AVCOD = 'L' WHERE E1ETB = '"
            + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND E1COD='" + typeCde + "' AND E1NUM = " + numCde + " AND E1SUF=" + numSuf
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    
    if (liste != null && liste.size() > 0) {
      liste.get(0).setField("TYPECDE", typeCde);
      return liste.get(0);
    }
    else
      return null;
  }
  
  /**
   * Retourne l'ent�te de ma commande
   */
  public ArrayList<GenericRecord> recupererCommentaireCommande(Utilisateur utilisateur, String cde) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    
    if (cde == null || cde.length() != 8 || utilisateur == null)
      return null;
    
    typeCde = cde.substring(0, 1);
    numCde = cde.substring(1, 7);
    numSuf = cde.substring(7, 8);
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2().select(
        "SELECT XITYP, XILIB " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMLBCM " + " LEFT JOIN "
            + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMXLIM ON L1ETB=XIETB AND L1NUM=XINUM AND L1SUF=XISUF AND L1NLI=XINLI "
            + " WHERE L1ETB = '" + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND L1COD='" + typeCde + "' AND L1NUM = " + numCde
            + " AND l1SUF=" + numSuf + " AND L1ERL='*'" + " ORDER BY XINLI, XITYP " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
    
    return liste;
  }
  
  /**
   * Verrouille une commande reprise dans le panier
   */
  public int verouilleCommande(Utilisateur utilisateur, String cde) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    
    if (cde != null && cde.length() == 8 && utilisateur != null) {
      
      typeCde = cde.substring(0, 1);
      numCde = cde.substring(1, 7);
      numSuf = cde.substring(7, 8);
      
      return utilisateur.getAccesDB2()
          .requete("UPDATE " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + "SET E1TOP=E1ETA+1 " + "WHERE E1ETB = '"
              + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND E1NUM = " + numCde + " AND E1SUF=" + numSuf + " AND E1COD='" + typeCde
              + "'", this.getClass());
    }
    else
      return -1;
  }
  
  /**
   * V�rifie si la commande est en attente
   */
  public boolean isCommandeEnAttente(Utilisateur utilisateur, String cde) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    boolean isOk = false;
    
    if (cde != null && cde.length() == 8 && utilisateur != null) {
      typeCde = cde.substring(0, 1);
      numCde = cde.substring(1, 7);
      numSuf = cde.substring(7, 8);
      
      ArrayList<GenericRecord> liste = null;
      
      liste = utilisateur.getAccesDB2()
          .select("SELECT E1ETA " + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + "WHERE E1ETB = '"
              + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND E1COD='" + typeCde + "' AND E1NUM = " + numCde + " AND E1SUF=" + numSuf
              + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      
      if (liste != null && liste.size() == 1)
        isOk = liste.get(0).getField("E1ETA").toString().trim().equals("0");
      
    }
    
    return isOk;
  }
  
  /**
   * Retourne le message d'une annulation de commande dans un mail
   */
  private String retournerMessageAnnulation(String commande, String type, String code) {
    String contenu = "";
    String det = "e";
    if (code.equals("D")) {
      det = "";
    }
    
    contenu += "<table id='confCommande'>";
    contenu +=
        "<tr><td class='paddingHorizontal'></td><td class='h2Mail'>Annulation de " + type + "</td><td class='paddingHorizontal'></td></tr>";
    contenu += "<tr ><td  colspan='3' class='paddingVertical'></td></tr>";
    contenu += "<tr><td class='paddingHorizontal'></td><td class='messageDeConf'>Votre " + type + " N� " + commande + " a bien �t� annul�"
        + det + "</td><td class='paddingHorizontal'></td></tr>";
    contenu += "</table>";
    
    return contenu;
  }
  
  /**
   * On v�rifie si le bon transmis est un devis valid� !!
   */
  public boolean isDevisValide(Utilisateur pUtil, String pBon) {
    
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    boolean isOk = false;
    
    if (pBon != null && pBon.length() == 8 && pUtil != null) {
      typeCde = pBon.substring(0, 1);
      numCde = pBon.substring(1, 7);
      numSuf = pBon.substring(7, 8);
      
      ArrayList<GenericRecord> liste = null;
      
      // On v�rifie que le bon soit un devis (E1COD = 'D') et que le bon soit dans un �tat de type valid� ou envoy� (1
      // ou 2)
      liste = pUtil.getAccesDB2()
          .select("SELECT E1TDV, E1DAT2, E1COD " + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + "WHERE E1ETB = '"
              + pUtil.getETB_EN_COURS().getCodeETB() + "' AND E1COD='" + typeCde + "' AND E1NUM = " + numCde + " AND E1SUF=" + numSuf
              + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      
      if (liste != null && liste.size() == 1) {
        isOk =
            (liste.get(0).getField("E1TDV").toString().trim().equals("1") || liste.get(0).getField("E1TDV").toString().trim().equals("2"))
                && liste.get(0).getField("E1COD").toString().trim().equals("D");
        if (isOk) {
          isOk = verifierDateValiditeDevis(pUtil, (liste.get(0).getField("E1DAT2").toString().trim()));
        }
      }
    }
    
    return isOk;
  }
  
  /**
   * On v�rifie si la date du devis est encore valable
   */
  public boolean verifierDateValiditeDevis(Utilisateur pUtil, String pDateDevis) {
    boolean isValable = false;
    
    if (pDateDevis == null || pUtil == null) {
      return isValable;
    }
    
    int dateDevisNumerique = -1;
    int dateJour = 0;
    
    try {
      dateDevisNumerique = Integer.parseInt(pDateDevis);
      dateJour = Outils.recupererDateCouranteInt();
      
      isValable = (dateDevisNumerique >= dateJour) || (dateDevisNumerique == 0);
      
    }
    catch (Exception e) {
      
    }
    
    return isValable;
  }
  
  /**
   * Transformer un devis en commande
   */
  public String transformerEnCommande(Utilisateur pUtil, String pBon) {
    String CdeRetour = null;
    if (pUtil == null || pBon == null) {
      return null;
    }
    pUtil.controleDuplicationDevis();
    
    if (pUtil.getDuplicationDevis() != null && pBon.length() == 8) {
      try {
        if (pUtil.getDuplicationDevis().execute(pBon.substring(0, 1), pUtil.getETB_EN_COURS().getCodeETB(),
            new BigDecimal(pBon.substring(1, 7)), new BigDecimal(pBon.substring(7, 8)))) {
          if (pUtil.getDuplicationDevis().getERREURS() == DuplicDevis.PAS_ERREUR) {
            CdeRetour = pUtil.getDuplicationDevis().getCDE_RETOUR();
          }
          else {
            
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    return CdeRetour;
  }
  
  /**
   * Si on passe un devis on retourne l'�ventuelle commande g�n�r�e � partir du devis
   * Si on passe une commande on retourne l'�ventuel devis li� si il existe
   */
  public String retournerDocumentLie(Utilisateur pUtil, String pDoc) {
    if (pUtil == null || pDoc == null || pDoc.length() != 8) {
      return null;
    }
    
    String valeurDocRetour = null;
    String valeurType = pDoc.substring(0, 1);
    String valeurDocument = valeurType + pUtil.getETB_EN_COURS().getCodeETB() + pDoc.substring(1, 8);
    String zoneRecherche = null;
    String zoneDocument = null;
    
    final String ZONE_DEVIS = "IXREF1";
    final String ZONE_CDE = "IXREF2";
    
    if (valeurType.equals("E")) {
      zoneRecherche = ZONE_DEVIS;
      zoneDocument = ZONE_CDE;
    }
    else if (valeurType.equals("D")) {
      zoneRecherche = ZONE_CDE;
      zoneDocument = ZONE_DEVIS;
    }
    else
      return null;
    
    ArrayList<GenericRecord> liste = null;
    
    String requete = "SELECT " + zoneRecherche + " " + "FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMIDXM "
        + "WHERE IXTYP = 'DD' AND IXETB = '" + pUtil.getETB_EN_COURS().getCodeETB() + "' AND " + zoneDocument + " = '" + valeurDocument
        + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE;
    
    liste = pUtil.getAccesDB2().select(requete, this.getClass());
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField(zoneRecherche)) {
        valeurDocRetour = liste.get(0).getField(zoneRecherche).toString().trim();
      }
    }
    
    return valeurDocRetour;
  }
}
