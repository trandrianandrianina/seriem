
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionPartenaires extends Gestion {
  public GestionPartenaires() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_PARTENAIRES;
  }
  
  /**
   * retourne la liste de partenaires de cet �tablissement
   */
  public ArrayList<GenericRecord> retournerListePartenaires(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    if (utilisateur.getETB_EN_COURS() != null)
      return utilisateur.getAccesDB2().select(
          "SELECT PAR_NOM, PAR_IMAGE, PAR_WEB FROM " + ConstantesEnvironnement.BIBLI_WS + ".PARTENAIRE WHERE PAR_ETB='"
              + utilisateur.getETB_EN_COURS().getCodeETB() + "' ORDER BY PAR_NOM " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
    else
      return utilisateur.getAccesDB2().select("SELECT PAR_NOM, PAR_IMAGE, PAR_WEB FROM " + ConstantesEnvironnement.BIBLI_WS
          + ".PARTENAIRE ORDER BY PAR_NOM " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
  }
}
