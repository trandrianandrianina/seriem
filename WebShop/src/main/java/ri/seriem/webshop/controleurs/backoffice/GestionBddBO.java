
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionBddBO extends Gestion {
  public GestionBddBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne la liste des fichiers pour la biblioth�que WebShop.
   */
  public ArrayList<GenericRecord> retournerListeFichier(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select(
        "SELECT TABLE_NAME FROM SYSIBM.TABLES WHERE TABLE_SCHEMA='" + ConstantesEnvironnement.BIBLI_WS.toUpperCase() + "'",
        this.getClass());
  }
  
  /**
   * Retourne la description d'un fichier
   */
  public ArrayList<GenericRecord> retournerDetailFichier(Utilisateur utilisateur, String fichier) {
    if (loggerSiTrue("Utilisateur ou fichier � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || fichier == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT COLUMN_NAME, DATA_TYPE  FROM SYSIBM.COLUMNS WHERE TABLE_SCHEMA = '"
        + ConstantesEnvironnement.BIBLI_WS.toUpperCase() + "' AND TABLE_NAME = '" + fichier + "'", this.getClass());
  }
  
  /**
   * Retourne le resultat de la requete
   */
  public ArrayList<GenericRecord> retournerRequete(Utilisateur utilisateur, String requeteBDD) {
    if (loggerSiTrue("Utilisateur ou requeteBDD � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || requeteBDD == null)))
      return null;
    
    return utilisateur.getAccesDB2().select(" " + requeteBDD + " ", this.getClass());
  }
  
}
