
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionTraductionBO extends Gestion {
  
  /**
   * Affiche liste moteur de recherche
   */
  public ArrayList<GenericRecord> retourneMotCleduMoteur(Utilisateur utilisateur, String expression) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    if (expression == null) {
      return null;
    }
    
    return utilisateur.getAccesDB2().select(
        " SELECT TR_CLE, TR_VALEUR FROM " + ConstantesEnvironnement.BIBLI_WS + ".TRADUCTION "
            + " WHERE TR_CODE = 'fr' AND LCASE(TR_CLE) LIKE '%" + expression.toLowerCase() + "%' " + " ORDER BY TR_CLE ASC ",
        this.getClass());
  }
  
  public ArrayList<GenericRecord> retourUnMotCleParLangue(Utilisateur utilisateur, String mot, String langue) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    return utilisateur.getAccesDB2()
        .select("SELECT TR_ID,TR_CLE, TR_VALEUR, TR_CODE, b.la_id FROM " + ConstantesEnvironnement.BIBLI_WS + ".TRADUCTION a"
            + " left join " + ConstantesEnvironnement.BIBLI_WS + ".LANGUES b ON a.TR_CODE = b.LA_ID" + " WHERE TR_CLE = '" + mot
            + "' AND TR_CODE= '" + langue + "'", this.getClass());
  }
  
  /**
   * Mise � jour d'un mot cl�
   * C'est vraiment pas bo ce DELETE INSERT mais on changera plus tard
   */
  @SuppressWarnings("unchecked")
  public int miseAjourMotCle(Utilisateur utilisateur, HttpServletRequest request, String motCle) {
    int res = -1;
    if (loggerSiTrue("Utilisateur � NULL", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return res;
    
    // Lister les param�tres � mettre � jour
    Enumeration<String> motCles = request.getParameterNames();
    
    // Liste des param�tres pass�s
    ArrayList<String> moscleSelectionne = null;
    String motCleParcouru = null;
    while (motCles.hasMoreElements()) {
      motCleParcouru = motCles.nextElement();
      
      if (motCleParcouru.startsWith("motCle_")) {
        if (moscleSelectionne == null)
          moscleSelectionne = new ArrayList<String>();
        
        moscleSelectionne.add(motCleParcouru.replaceFirst("motCle_", ""));
      }
    }
    
    // On supprime le mot cle selectionn� et ses traductions avant d'inserer avec les nouvelles donn�es
    res = utilisateur.getAccesDB2().requete(
        "DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".TRADUCTION WHERE TR_CLE = '" + request.getParameter("motCle") + "'",
        this.getClass());
    if (res < 0) {
      return res;
    }
    
    // ON INSERT les donnees en param�tres avec les nouvelles valeurs
    if (moscleSelectionne != null && moscleSelectionne.size() > 0) {
      for (int i = 0; i < moscleSelectionne.size(); i++) {
        res = utilisateur.getAccesDB2()
            .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".TRADUCTION (TR_CODE, TR_CLE,TR_VALEUR) VALUES( '"
                + moscleSelectionne.get(i) + "','" + request.getParameter("motCle") + "','"
                + (traiterCaracteresSpeciauxSQL(request.getParameter("motCle_" + moscleSelectionne.get(i)))) + "')", this.getClass());
      }
    }
    
    return res;
  }
  
  /**
   * Permet de retourner la liste des traductions
   */
  public ArrayList<GenericRecord> retournerListeTraductions(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select(
        "SELECT TR_CLE, TR_VALEUR FROM " + ConstantesEnvironnement.BIBLI_WS + ".TRADUCTION WHERE TR_CODE = 'fr' ORDER BY TR_CLE ASC ",
        this.getClass());
  }
}
