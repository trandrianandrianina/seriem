
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Etablissement;

/**
 * Classe permettant d'afficher les informations par magasin
 * Table : Magasin
 * @author D�borah
 *
 */
public class GestionMagasins extends Gestion {
  
  /**
   * Retourne la liste des magasins par filiale
   */
  public ArrayList<GenericRecord> retourneMagasinsParEtbs(Utilisateur utilisateur, Etablissement etablissement) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    // Si on a pas d'�tablissements on va tous les r�cup�rer et trier les magasins par ETB
    if (utilisateur.getETB_EN_COURS() == null) {
      return utilisateur.getAccesDB2().select(
          "SELECT MG_NAME, MG_ADR, MG_TEL, MG_CARTE, US_ETB, ETB_LIB, LI_MESS, LI_HOR, LI_EQUI " + " FROM "
              + ConstantesEnvironnement.BIBLI_WS + ".ETABLISS LEFT JOIN " + ConstantesEnvironnement.BIBLI_WS
              + ".MAGASINS ON US_ETB = ETB_ID LEFT JOIN " + ConstantesEnvironnement.BIBLI_WS + ".LIENSMAG "
              + " ON MG_ID = LI_MAG AND LI_LAN = '" + utilisateur.getLanguage() + "' WHERE MG_ACTIF = '1' AND ETB_FM = '"
              + ConstantesEnvironnement.BIBLI_CLIENTS + "' ORDER BY US_ETB ASC,MG_COD ASC " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
          this.getClass());
      
    }
    else if (utilisateur.getETB_EN_COURS() != null && utilisateur.getETB_EN_COURS().getCodeETB() != null)
      return utilisateur.getAccesDB2()
          .select("SELECT MG_NAME, MG_ADR, MG_TEL, MG_CARTE, US_ETB, LI_MESS, LI_HOR, LI_EQUI " + " FROM "
              + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS LEFT JOIN " + ConstantesEnvironnement.BIBLI_WS + ".LIENSMAG "
              + " ON MG_ID = LI_MAG AND LI_LAN = '" + utilisateur.getLanguage() + "' WHERE US_ETB ='" + etablissement.getCodeETB()
              + "' AND MG_ACTIF = '1' ORDER BY MG_COD ASC " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    else
      return null;
  }
}
