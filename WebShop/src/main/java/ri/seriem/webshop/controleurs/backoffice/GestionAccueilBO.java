
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Classe permettant d'organiser les menus du BO Table : BTN_ACCUEILBO
 * 
 * @author D�borah
 *
 */
public class GestionAccueilBO extends Gestion {
  public GestionAccueilBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * retourne la liste de la table des acc�s au Back office
   */
  public ArrayList<GenericRecord> retourneAccesBO(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT AB_ID, AB_ACC FROM " + ConstantesEnvironnement.BIBLI_WS + ".ACCESBO ", this.getClass());
  }
  
}
