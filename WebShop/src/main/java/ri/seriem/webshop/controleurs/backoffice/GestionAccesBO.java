
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionAccesBO extends Gestion {
  
  public GestionAccesBO() {
    // classeQuiLogge = this.getClass().getName();
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne la liste des menus du BO
   */
  public ArrayList<GenericRecord> recupererMenuBO(Utilisateur utilisateur) {
    if (loggerSiTrue("utilisateur � NULL", utilisateur == null || utilisateur.getAccesDB2() == null))
      return null;
    
    return utilisateur.getAccesDB2().select(
        "SELECT BTN_ID, BTN_NAME FROM " + ConstantesEnvironnement.BIBLI_WS + ".ACCUEILBO " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
  }
  
  /**
   * retourne une ligne de menu avec ses acc�s (30,40..)
   * 
   */
  public ArrayList<GenericRecord> recupereParMenu(Utilisateur utilisateur, String id, String acces) {
    if (loggerSiTrue("utilisateur � NULL", utilisateur == null || id == null || acces == null))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT AB_ID, AB_ACC FROM " + ConstantesEnvironnement.BIBLI_WS + ".accesBO " + " WHERE AB_ID="
        + id + " AND AB_ACC='" + acces + "'" + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
  }
  
  /**
   * Traitement des donn�es dans la base accesbo
   * 
   */
  @SuppressWarnings("unchecked")
  public int ajoutAccesBO(Utilisateur utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (loggerSiTrue("utilisateur � NULL", utilisateur == null))
      return res;
    
    // Lister les param�tres � mettre � jour
    Enumeration<String> menus = request.getParameterNames();
    
    // On supprime le contenu de la table ACCESBO avant d'inserer avec les nouvelles donn�es
    res = utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ACCESBO ", this.getClass());
    if (res < 0) {
      // log.error("[ajoutAccesBO] La requ�te de suppression de la table ACCESBO n'a pas fonctionn�e. Le r�sultat est :
      // "+res+" l'erreur :"+user.getAccesDB2().getMsgError());
      return res;
    }
    
    // Liste des param�tres pass�s
    Object elementMenu = null;
    while (menus.hasMoreElements()) {
      elementMenu = menus.nextElement();
      String param = (String) elementMenu;
      String[] valeurs = request.getParameterValues(param);
      for (int i = 0; i < valeurs.length; i++) {
        // j'exclus le param�tre MAJ
        if (!elementMenu.equals("Maj")) {
          // on insert avec les nouvelles valeurs
          res = utilisateur.getAccesDB2().requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ACCESBO (AB_ID, AB_ACC) VALUES ('"
              + elementMenu + "','" + valeurs[i] + "')", this.getClass());
        }
      }
    }
    
    if (res < 0) {
      return res;
    }
    
    return res;
  }
}

/**************************************************** FIN *********************************************************/
