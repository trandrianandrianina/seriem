
package ri.seriem.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.FileNG;
import ri.seriem.libcommun.outils.GestionFichierTexte;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionLogsBO extends Gestion {
  public GestionLogsBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne une liste de dates pour lesquelles il existe des logs. Le tri
   * s'effectue de la plus r�cente � la plus ancienne
   */
  public ArrayList<GenericRecord> retournerDatesLog(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT LO_DATE FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".LOGSW GROUP BY LO_DATE ORDER BY LO_DATE asc FETCH FIRST 30 ROWS ONLY ", this.getClass());
  }
  
  /**
   * Retourne le d�tail d'une log
   */
  public ArrayList<GenericRecord> retournerDetailLogs(Utilisateur utilisateur, int id) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2()
        .select("SELECT LO_SESS, LO_CLASS FROM " + ConstantesEnvironnement.BIBLI_WS + ".LOGSW  WHERE LO_ID = " + id, this.getClass());
  }
  
  /**
   * modifier la varaible DEBUG
   */
  public boolean activeLogsDebug(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return false;
    
    // si la variable est activ�e on la dsactive sinon l'inverse
    if (ConstantesEnvironnement.MODE_DEBUG) {
      if (utilisateur.getAccesDB2().requete(
          "UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '0' WHERE EN_CLE='MODE_DEBUG'", this.getClass()) > 0)
        ConstantesEnvironnement.MODE_DEBUG = false;
    }
    else {
      if (utilisateur.getAccesDB2().requete(
          "UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '1' WHERE EN_CLE='MODE_DEBUG'", this.getClass()) > 0)
        ConstantesEnvironnement.MODE_DEBUG = true;
    }
    
    return ConstantesEnvironnement.MODE_DEBUG;
  }
  
  /**
   * Liste tous les logs DEBUG
   * @return
   */
  public ArrayList<String> listeFileLogDebug() {
    String fileLog = null;
    FileNG logDebug = new FileNG(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT + File.separator + "logs" + File.separator);
    File[] list = logDebug.listFiles();
    ArrayList<String> allFilesDebug = new ArrayList<String>();
    Comparator<String> comparator = Collections.reverseOrder();
    if (logDebug.exists()) {
      for (int i = 0; i < list.length; i++) {
        fileLog = list[i].getName();
        if (fileLog.startsWith("webshop")) {
          allFilesDebug.add(fileLog);
          
        }
      }
      
    }
    Collections.sort(allFilesDebug, comparator);
    return allFilesDebug;
  }
  
  // retourne le d�tail du fichier
  public ArrayList<String> detailFichierLog(String fileLog) {
    
    File fileModif = new File(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT + File.separator + "logs" + File.separator + fileLog);
    
    GestionFichierTexte gestionFichier = new GestionFichierTexte(fileModif);
    ArrayList<String> allContenuFile = new ArrayList<String>();
    // contenu de nom fichier
    allContenuFile = gestionFichier.getContenuFichier();
    
    // tri de nom fichier
    Comparator<String> comparator = Collections.reverseOrder();
    Collections.sort(allContenuFile, comparator);
    
    return allContenuFile;
    
  }
  
}
