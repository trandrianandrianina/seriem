
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionPartenairesBO extends Gestion {
  public GestionPartenairesBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne tous les partenaires
   */
  public ArrayList<GenericRecord> tousLesPartenaires(Utilisateur utilisateur) {
    if (loggerSiTrue("[tousLesPartenaires()] Utilisateur � NUll ", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return (utilisateur.getAccesDB2().select(
        "SELECT PAR_ID, PAR_NOM, PAR_ETB, PAR_IMAGE, PAR_WEB FROM " + ConstantesEnvironnement.BIBLI_WS + ".PARTENAIRE ", this.getClass()));
  }
  
  /**
   * Retourne un partenaire
   */
  public GenericRecord unPartenaire(Utilisateur utilisateur, String unPartenaire, String etb) {
    if (loggerSiTrue("[unPartenaire()] Utilisateur � NUll ", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT PAR_ID, PAR_NOM, PAR_ETB, PAR_IMAGE, PAR_WEB FROM " + ConstantesEnvironnement.BIBLI_WS
            + ".PARTENAIRE WHERE PAR_ID='" + unPartenaire + "' AND PAR_ETB = '" + etb + "' ", this.getClass());
    
    if (liste != null && liste.size() > 0)
      return liste.get(0);
    else
      return null;
  }
  
  /**
   * Mise � jour ou ajout d'un partenaires
   */
  public int majCreaPartenaires(Utilisateur utilisateur, HttpServletRequest request, int id) {
    int res = -1;
    if (loggerSiTrue("[majCreaPartenaires]Utilisateur � NUll ", (utilisateur == null || request == null)))
      return res;
    
    // on est modifi de partenaire
    if (request.getParameter("idPart") != null) {
      res = utilisateur.getAccesDB2()
          .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".PARTENAIRE SET " + "PAR_NOM ='"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomPART").toString().trim().toUpperCase()) + "' " + ", PAR_IMAGE ='"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomIMAGE").toString()) + "' " + " , PAR_WEB= '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomWEB").toString()) + "' " + " WHERE PAR_ID ="
              + request.getParameter("idPart"), this.getClass());
    }
    // on est en creation de partenaire
    else {
      res = utilisateur.getAccesDB2()
          .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".PARTENAIRE (PAR_NOM, PAR_ETB, PAR_IMAGE, PAR_WEB) VALUES" + " ('"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomPART").toString().trim().toUpperCase()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("etbPart").toString().trim().toUpperCase()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomIMAGE").toString()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomWEB").toString()) + "')", this.getClass());
    }
    return res;
  }
  
  /**
   * Supprimer un partenaire sur la base de son id
   */
  public int suppressionPartenaire(Utilisateur pUtil, int pIdPart) {
    int res = -1;
    if (loggerSiTrue("[suppressionPartenaire] Utilisateur � NUll ", (pUtil == null))) {
      return res;
    }
    
    res = pUtil.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".PARTENAIRE WHERE  PAR_ID =" + pIdPart,
        this.getClass());
    
    return res;
  }
  
}
