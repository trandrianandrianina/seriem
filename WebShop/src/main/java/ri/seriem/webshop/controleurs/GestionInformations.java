
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Classe permettant de retourner les informations des magasins par filiales
 * table : information
 * @author D�borah
 *
 */

public class GestionInformations extends Gestion {
  /**
   * retourner les informations par filiale
   */
  public ArrayList<GenericRecord> retournerInformations(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", utilisateur == null))
      return null;
    
    return utilisateur.getAccesDB2()
        .select("select INF_TXT,INF_DES,INF_IMG,INF_TXT2,INF_DES2,INF_IMG2, INF_LAN FROM " + ConstantesEnvironnement.BIBLI_WS + ".INFOSW "
            + " WHERE INF_LAN = '" + utilisateur.getLanguage() + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
  }
  
}
