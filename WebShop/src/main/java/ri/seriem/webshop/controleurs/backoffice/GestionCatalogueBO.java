
package ri.seriem.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionCatalogueBO extends Gestion {
  
  public GestionCatalogueBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne la liste des param�tres d'affichage de la table AFFICHAGE de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererParametreAffichage(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select(
        "SELECT EN_CLE, EN_LIB, EN_VAL, EN_TYPE  FROM " + ConstantesEnvironnement.BIBLI_WS + ".AFFICHAGE" + " WHERE EN_GEST='1'",
        this.getClass());
  }
  
  /**
   * Mise � jour de la table AFFICHAGE de XWEBSHOP et par cons�quent l'affichage du catalogue
   */
  public ArrayList<GenericRecord> miseajourParametreAffichage(Utilisateur utilisateur, HttpServletRequest request) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    ArrayList<GenericRecord> liste = null;
    
    liste =
        utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_VAL  FROM " + ConstantesEnvironnement.BIBLI_WS + ".AFFICHAGE", this.getClass());
    
    if (liste != null && liste.size() > 0) {
      for (int i = 0; i < liste.size(); i++) {
        if (!liste.get(i).getField("EN_VAL").toString().trim()
            .equals((request.getParameter(liste.get(i).getField("EN_CLE").toString()).trim())))
          loggerSiFalse("miseajourParametreAffichage() UPDATE AFFICHAGE ",
              (utilisateur.getAccesDB2()
                  .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".AFFICHAGE SET EN_VAL  = '"
                      + (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim() + "' WHERE EN_CLE = '"
                      + liste.get(i).getField("EN_CLE") + "'", this.getClass()) > 0));
      }
    }
    
    return liste;
  }
  
  /**
   * traitement de l'upload
   */
  public int upload(Utilisateur utilisateur, HttpServletRequest request, String filename) {
    String uploadDirectory = ConstantesEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separator;
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    
    int res = 0;
    boolean ret = false;
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploader*/
    
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requ�te*/
    
    /*construction du repertoire ou sera stocker le fichier uploader. */
    String uploadPath = uploadDirectory;
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(request);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recup�r�s
      while (iter.hasNext()) {
        FileItem item = (FileItem) iter.next();
        
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          filename = item.getName();
          
          String filePath = uploadPath + filename;
          File storeFile = new File(filePath);
          // Enregistrer le fichier sur le disque
          item.write(storeFile);
          res = 1;
          ret = true;
          // if (MarbreEnvironnement.MODE_DEBUG )//&& log.isDebugEnabled())
          // {
          loggerSiDEBUG("[upload()] L'UPLOAD � correctement fonctionn�");
        }
      }
      
    }
    catch (Exception ex) {
      ret = false;
      forcerLogErreur("[upload()] L'upload � �chou� car fichier non selectionn�, ou repertoire de destination non cr��: " + ret);
      res = -1;
    }
    return res;
  }
  
}
