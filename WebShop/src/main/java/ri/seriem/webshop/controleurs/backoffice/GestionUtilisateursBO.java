
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;

public class GestionUtilisateursBO extends Gestion {
  public GestionUtilisateursBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne une liste d'utilisateurs tri�s par ACCES prioritaire
   */
  public ArrayList<GenericRecord> retournerListeUtilis(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2()
        .select(
            "" + " SELECT US_ID,US_LOGIN,US_ACCES,US_SERIEM,US_ETB,US_DATC, US_DATM, RWACC, RENUM, CLCLI, RENET, CLNOM " + " FROM "
                + ConstantesEnvironnement.BIBLI_WS + ".USERW " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM "
                + " ON CAST(RLNUMT AS CHAR(15)) = US_SERIEM  AND US_ETB = RLETB AND RLCOD = 'C' " + " LEFT JOIN "
                + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMCLIM ON DIGITS(CLCLI)||DIGITS(CLLIV) = RLIND " + " LEFT JOIN "
                + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM ON RLNUMT = RENUM " + " LEFT JOIN "
                + ConstantesEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RLNUMT = RWNUM ORDER BY US_ACCES, RWACC, US_ETB, US_LOGIN ",
            this.getClass());
  }
  
  /**
   * Retourne les infos d'un utilisateur
   */
  public ArrayList<GenericRecord> retournerUnUtilisateur(Utilisateur utilisateur, int user_id) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select(
        "SELECT US_ID,US_LOGIN,US_ACCES,US_SERIEM,US_ETB FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW where US_ID= " + user_id,
        this.getClass());
  }
  
  /**
   * Ajout d'un nouveau responsable dans la table userw
   */
  public int ajoutProfilAs400(Utilisateur utilisateur, HttpServletRequest request, int acces) {
    int res = -1;
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return res;
    
    String login = request.getParameter("loginUser").toString().trim().toUpperCase();
    
    // D'abord on teste si ce profil existe ou non dans le WebShop
    ArrayList<GenericRecord> liste = null;
    liste = utilisateur.getAccesDB2().select("" + " SELECT US_ID FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW "
        + " WHERE UPPER(US_LOGIN) = '" + login.toUpperCase() + "' AND US_ETB = '" + utilisateur.getETB_EN_COURS() + "' ", this.getClass());
    // -1 est le retour pour une erreur SQL
    if (liste == null)
      return res;
    // On retourne le nombre d'enregistrements afin de retourner un message adapt� puisqu'il en a trop
    if (liste.size() > 1)
      return liste.size();
      
    // Ensuite on teste si ce profil est valide S�rie M
    // -2 est le code retour pour un profil non autoris�
    if (!controlerUnProfilAs400(utilisateur, login))
      return -2;
    
    if (liste.size() == 1 && liste.get(0).isPresentField("US_ID")) {
      res = utilisateur.getAccesDB2()
          .requete("" + " UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".USERW " + " SET US_ACCES = '" + acces + "', US_DATM = '"
              + Outils.recupererDateCouranteInt() + "'  " + " WHERE US_ID = '" + liste.get(0).getField("US_ID").toString().trim()
              + "' AND US_ETB = '" + utilisateur.getETB_EN_COURS() + "' ", this.getClass());
    }
    else if (liste.size() == 0) {
      res = utilisateur.getAccesDB2()
          .requete("" + " INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".USERW  "
              + " (US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,US_DATC,US_DATM) VALUES " + " ('" + login.toUpperCase() + "'," + " '',"
              + " '" + acces + "'," + " ''," + " '" + utilisateur.getETB_EN_COURS() + "'," + "'" + Outils.recupererDateCouranteInt() + "',"
              + "'" + Outils.recupererDateCouranteInt() + "'" + ") ", this.getClass());
    }
    
    return res;
  }
  
  /**
   * Contr�ler si le profil pass� est un profil S�rie M valide
   */
  private boolean controlerUnProfilAs400(Utilisateur pUtil, String pProfil) {
    // 1ere �tape on v�rifie au moins que le profil soit un profil existant dans la base de S�rie M
    /*ArrayList<GenericRecord> liste = null;
    liste = pUtil.getAccesDB2().select(""
    		+ " SELECT USSPRF FROM " + MarbreEnvironnement.SERIE + "_GPL.PSEMUSSM "
            + " WHERE USSPRF = '" + pProfil + "' ", this.getClass());
    
    //TODO Faudra mieux contr�ler la s�curit� que cet immonde bouzin ci-dessous. Du coup je l'ai vir� ca marchait jamais
    //liste = utilisateur.getAccesDB2().select("SELECT SEUSR FROM "+ MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMSEC",this.getClass());
    
    return (liste!=null && liste.size()==1);*/
    
    return true;
  }
  
  /**
   * supprimer un responsable dans la table userw
   */
  public int suppressionResponsable(Utilisateur utilisateur, int userId) {
    int res = 0;
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return res;
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2().select(
        "SELECT US_ID,US_LOGIN,US_ACCES,US_SERIEM,US_ETB FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW where US_ID= " + userId,
        this.getClass());
    
    if (liste != null && liste.size() == 1 && utilisateur.getUS_ACCES() >= ConstantesEnvironnement.ACCES_RESPON_WS)
      res = utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".USERW WHERE  US_ID =" + userId,
          this.getClass());
    
    return res;
  }
}
