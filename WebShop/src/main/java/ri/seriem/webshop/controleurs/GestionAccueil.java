
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;

public class GestionAccueil extends Gestion {
  
  /**
   * Retourne la liste des promotions actives [PROMOTION]
   */
  public static ArrayList<GenericRecord> recupererPromotions(Utilisateur utilisateur) {
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(
        " SELECT PR_ID, PR_A1ETB, PR_A1ART, PR_A1LIB, PR_TEXTE, PR_DATED, PR_DATEF, PR_PRIX, A1FAM, A1SFA " + " FROM "
            + ConstantesEnvironnement.BIBLI_WS + ".PROMOTION " + " LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM ON PR_A1ETB = A1ETB AND PR_A1ART = A1ART " + " WHERE PR_DATED <= '" + Outils.recupererDateCouranteInt()
            + "' AND PR_DATEF >= '" + Outils.recupererDateCouranteInt() + "' " + " ORDER BY PR_DATEF FETCH FIRST 80 ROWS ONLY ",
        GestionAccueil.class);
    
    if (liste != null && liste.size() > 0) {
      return liste;
    }
    else
      return null;
  }
  
  /**
   * retourne le texte d'info de l'accueil en fonction de la langue [INFOACC]
   */
  public static ArrayList<GenericRecord> recupereInfoAcc(Utilisateur utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT ACC_NAME FROM " + ConstantesEnvironnement.BIBLI_WS + ".INFOACC WHERE ACC_LAN = '"
        + utilisateur.getLanguage() + "' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        GestionAccueil.class);
  }
  
}
