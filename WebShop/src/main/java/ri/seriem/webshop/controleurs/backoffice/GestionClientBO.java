
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionClientBO extends Gestion {
  public GestionClientBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Retourne la liste des param�tre de la table ENVIRONM de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererParametreclient(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_VAL, EN_TYP  FROM " + ConstantesEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_GEST='1' AND EN_ETB = '' ORDER BY EN_CLE", this.getClass());
  }
  
  /**
   * Mise � jour de la table ENVIRONM de XWEBSHOP
   */
  public boolean miseajourParametreclient(Utilisateur utilisateur, HttpServletRequest request) {
    boolean retour = false;
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return retour;
    
    ArrayList<GenericRecord> liste = null;
    int res = 0;
    
    liste = utilisateur.getAccesDB2().select(
        "SELECT EN_CLE, EN_VAL, EN_TYP  FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_GEST = '1' AND EN_ETB = '' ",
        this.getClass());
    if (liste != null && liste.size() > 0) {
      int nbModif = 0;
      
      for (int i = 0; i < liste.size(); i++) {
        if ((liste.get(i).getField("EN_TYP").toString().trim().equals("MAJUSCULE"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim().toUpperCase() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE") + "'", this.getClass());
          if (res >= -1)
            nbModif++;
          
          if (liste.get(i).getField("EN_CLE").toString().trim().equals("BIBLI_CLIENTS")) {
            ConstantesEnvironnement.BIBLI_CLIENTS = (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim().toUpperCase();
            // System.out.println("MarbreEnvironnement.BIBLI_CLIENTS "+ MarbreEnvironnement.BIBLI_CLIENTS);
            res = utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ETABLISS SET ETB_FM = '"
                + ConstantesEnvironnement.BIBLI_CLIENTS + "' WHERE ETB_ID = '" + utilisateur.getETB_EN_COURS() + "'");
            
          }
        }
        else if ((liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE "
                  + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '" + Base64Coder
                      .encodeString(Base64Coder.encodeString(request.getParameter(liste.get(i).getField("EN_CLE").toString()))).trim()
                  + "' WHERE EN_CLE = '" + liste.get(i).getField("EN_CLE") + "'", this.getClass());
          if (res >= -1)
            nbModif++;
        }
        else {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE") + "'", this.getClass());
          if (res >= -1)
            nbModif++;
        }
      }
      
      if (liste.size() == nbModif)
        retour = true;
    }
    
    return retour;
    
  }
  
}
