
package ri.seriem.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.Gestion;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionMagasinsBO extends Gestion

{
  public GestionMagasinsBO() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_BACK_OFFICE;
  }
  
  /**
   * Recuperer tous les magasins
   */
  public ArrayList<GenericRecord> recupererAllMag(Utilisateur utilisateur) {
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT MG_ID, MG_NAME,MG_ADR, MG_TEL,MG_MESS,MG_HOR,MG_CARTE,MG_EQUI FROM "
        + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB = '" + utilisateur.getETB_EN_COURS() + "' ", this.getClass());
  }
  
  /**
   * recuperer les donn�es par magasins (id)
   */
  public ArrayList<GenericRecord> recupererDetailsMagasin(Utilisateur utilisateur, String id, String langue) {
    if (loggerSiTrue("Utilisateur ou ID � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null || id == null)))
      return null;
    
    return utilisateur.getAccesDB2()
        .select("SELECT MG_ID, MG_COD,MG_NAME,MG_ADR, MG_TEL,MG_CARTE,MG_ACTIF, MG_MAIL_CD, MG_MAIL_DV, MG_MAIL_IN, LI_MESS,LI_HOR,LI_EQUI "
            + "FROM " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS LEFT JOIN " + ConstantesEnvironnement.BIBLI_WS + ".LIENSMAG "
            + "ON MG_ID = LI_MAG AND LI_LAN = '" + langue + "' WHERE MG_ID= " + id, this.getClass());
  }
  
  /**
   * Mise � jour des donn�es dans xwebshop.magasins
   */
  public String miseajourInformationMagasin(Utilisateur utilisateur, HttpServletRequest request) {
    int res = 0;
    String idMagasin = null;
    
    if (loggerSiTrue("Utilisateur � NUll", (utilisateur == null || utilisateur.getAccesDB2() == null)))
      return null;
    
    // Si on est en mode modification
    if (request.getParameter("idMagasin") != null) {
      idMagasin = request.getParameter("idMagasin");
      // On met � jour les "constantes" du magasins
      res = utilisateur.getAccesDB2()
          .requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS SET " + " MG_NAME  = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nameBO")) + "' , MG_ADR = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("adr")) + "' , MG_TEL = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("tel")) + "' , MG_CARTE = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("carte")) + "' , MG_ACTIF = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("etatMG")) + "'," + " MG_MAIL_CD = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailCDE")) + "', " + " MG_MAIL_DV = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailDEV")) + "', " + " MG_MAIL_IN = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailINS")) + "' " + " WHERE MG_ID=" + idMagasin, this.getClass());
    }
    // En insert de nouveau magasin
    else {
      res = utilisateur.getAccesDB2()
          .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS "
              + "(MG_NAME,MG_ADR,MG_TEL,MG_MESS,MG_HOR,MG_CARTE,US_ETB,MG_EQUI,MG_COD,MG_MAIL_CD,MG_MAIL_DV,MG_MAIL_IN) VALUES " + "('"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nameBO")) + "'," + "'"
              + traiterCaracteresSpeciauxSQL(request.getParameter("adr")) + "'," + "'"
              + traiterCaracteresSpeciauxSQL(request.getParameter("tel")) + "'," + "''," + "''," + "'"
              + request.getParameter("carte").toString().replace("'", "''") + "'," + "'" + utilisateur.getETB_EN_COURS().getCodeETB() + "',"
              + "''," + "'" + traiterCaracteresSpeciauxSQL(request.getParameter("code")) + "'," + "'"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailCDE")) + "'," + "'"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailDEV")) + "'," + "'"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailINS")) + "')", this.getClass());
      
      // R�cuperer l'ID pour INSERT DANS LA TABLE LINGUISTIQUE
      if (res > 0) {
        utilisateur.recupererUnRecordTravail(utilisateur.getAccesDB2()
            .select("SELECT MAX(MG_ID) AS ID FROM " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS ", this.getClass()));
        if (utilisateur.getRecordTravail() != null && utilisateur.getRecordTravail().isPresentField("ID"))
          idMagasin = utilisateur.getRecordTravail().getField("ID").toString();
      }
    }
    
    // On va s'occuper des "variables" linguistiques
    if (res > -1 && idMagasin != null) {
      // On supprime d'�ventuels enregistrements pour ce code langue
      if (request.getParameter("langueInfo") != null && request.getParameter("idMagasin") != null)
        res = utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".LIENSMAG WHERE LI_LAN = '"
            + request.getParameter("langueInfo") + "' AND LI_MAG = '" + idMagasin + "'", this.getClass());
      
      // On rajoute un nouvel enregsitrement pour ce code langue
      if (res >= 0) {
        // la suppression s'est bien pass�e on log si la variable debug
        // est activ�e
        res = utilisateur.getAccesDB2().requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".LIENSMAG "
            + "(LI_MAG,LI_LAN,LI_MESS,LI_HOR,LI_EQUI) VALUES " + "('" + idMagasin + "','" + request.getParameter("langueInfo") + "','"
            + traiterCaracteresSpeciauxSQL(request.getParameter("mess")) + "','" + traiterCaracteresSpeciauxSQL(request.getParameter("hor"))
            + "','" + traiterCaracteresSpeciauxSQL(request.getParameter("equi")) + "')", this.getClass());
        if (res < 0)
          return null;
      }
    }
    
    return idMagasin;
  }
  
}
