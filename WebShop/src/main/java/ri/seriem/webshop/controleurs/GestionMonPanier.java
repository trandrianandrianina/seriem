
package ri.seriem.webshop.controleurs;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.CalculStocks;
import ri.seriem.webshop.metier.PanierArticleNormal;
import ri.seriem.webshop.outils.Outils;

public class GestionMonPanier extends Gestion {
  private CalculStocks calculDuStock = null;
  
  public GestionMonPanier() {
    classeQuiLogge = "GestionMonPanier";
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_PANIER;
  }
  
  /**
   * Mettre � jour les informations de r�cup�ration propres aux panier de
   * l'utilisateur
   */
  public boolean majModeRecuperation(Utilisateur utilisateur, String valeur) {
    if (utilisateur == null || !(utilisateur instanceof Utilisateur) || utilisateur.getMonPanier() == null || valeur == null)
      return false;
    try {
      utilisateur.getMonPanier().setModeRecup(Integer.parseInt(valeur));
      if (Integer.parseInt(valeur) == ConstantesEnvironnement.MODE_RETRAIT) {
        if (utilisateur.getClient().getMagasinClient() != null)
          utilisateur.getMonPanier().setMagasinPanier(utilisateur.getClient().getMagasinClient());
        else
          utilisateur.getMonPanier().setMagasinPanier(utilisateur.getMagasinSiege());
        utilisateur.getMonPanier().setDateRecuperation(Outils.recupererDateCouranteInt());
      }
      else if (Integer.parseInt(valeur) == ConstantesEnvironnement.MODE_LIVRAISON) {
        utilisateur.getMonPanier().setMagasinPanier(utilisateur.getMagasinSiege());
        utilisateur.getMonPanier().setDateRecuperation(Outils.transformerDateHumaineEnSeriem(
            Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(), utilisateur.getETB_EN_COURS().getDelai_mini_livraison())));
      }
      
      return true;
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * Permet de r�cup�rer la liste des magasins de la table MAGASINS de la bibli XWEBSHOP (extraction commerciale de
   * celle de la bibli client)
   */
  public ArrayList<GenericRecord> recupererListeMagasins(Utilisateur utilisateur) {
    if (utilisateur == null)
      return null;
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2().select(
        "SELECT MG_ID, MG_COD, MG_NAME FROM " + ConstantesEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB='"
            + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND MG_ACTIF = '1' " + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
        this.getClass());
    
    return liste;
  }
  
  /**
   * Retourne le stock pour un article
   */
  public float retourneStockPourUnArticle(Utilisateur utilisateur, String etb, String article, String magasin) {
    etb = traiterCaracteresSpeciauxSQL(etb);
    article = traiterCaracteresSpeciauxSQL(article);
    
    if (utilisateur == null || etb == null || article == null || magasin == null)
      return Float.parseFloat("0");
    
    if (calculDuStock == null)
      calculDuStock = new CalculStocks();
    
    if (calculDuStock != null) {
      try {
        return Float.parseFloat(calculDuStock.retournerStock(utilisateur, etb.trim(), article.trim(), magasin));
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
        return Float.parseFloat("0");
      }
    }
    else
      return Float.parseFloat("0");
  }
  
  /**
   * retourne le libell� d'un article
   */
  public boolean majInfosArticle(Utilisateur utilisateur, PanierArticleNormal article) {
    boolean retour = false;
    
    if (utilisateur == null || article == null)
      return retour;
    
    ArrayList<GenericRecord> liste = null;
    // r�cup�re le libell� de l'article
    liste = utilisateur.getAccesDB2()
        .select("SELECT A1LIB, A1UNV, CAREF FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '"
            + utilisateur.getETB_EN_COURS().getCodeETB() + "' AND A1ART ='" + article.getA1art() + "' "
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    // On met � jour l'article
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("A1LIB"))
        article.setLibelleArt(liste.get(0).getField("A1LIB").toString().trim());
      if (liste.get(0).isPresentField("A1UNV"))
        article.setUniteArt(liste.get(0).getField("A1UNV").toString().trim());
      if (liste.get(0).isPresentField("CAREF"))
        article.setRefFournisseur(liste.get(0).getField("CAREF").toString().trim());
    }
    
    return retour;
  }
  
  /**
   * Mettre � jour le variables Client du panier avec les infos du bloc adresse
   */
  public boolean majVariablesPanier(Utilisateur utilisateur) {
    boolean isOk = false;
    
    if (utilisateur == null || utilisateur.getClient() == null)
      return isOk;
    
    if (utilisateur.getClient().getNomClient() != null)
      utilisateur.getMonPanier().setCLNOM(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getNomClient()));
    if (utilisateur.getClient().getComplementClient() != null)
      utilisateur.getMonPanier().setCLCPL(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getComplementClient()));
    if (utilisateur.getClient().getRueClient() != null)
      utilisateur.getMonPanier().setCLRUE(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getRueClient()));
    if (utilisateur.getClient().getLocaliteClient() != null)
      utilisateur.getMonPanier().setCLLOC(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getLocaliteClient()));
    if (utilisateur.getClient().getCodePostalClient() != null) {
      if (!utilisateur.getClient().getCodePostalClient().trim().equals(""))
        utilisateur.getMonPanier().setCLPOS(utilisateur.getClient().getCodePostalClient());
      else if (utilisateur.getClient().getVilleClient() != null && utilisateur.getClient().getVilleClient().length() >= 5)
        utilisateur.getMonPanier().setCLPOS(utilisateur.getClient().getVilleClient().substring(0, 5).trim());
    }
    if (utilisateur.getClient().getVilleClient() != null && utilisateur.getClient().getVilleClient().length() >= 6)
      utilisateur.getMonPanier().setCLVIL(utilisateur.getClient().getVilleClient().substring(6).trim());
    
    return isOk;
  }
  
  /**
   * Mettre � jour le variables Client du panier avec les infos du bloc adresse
   */
  public boolean majVariablesPanier(Utilisateur utilisateur, String cle, String valeur) {
    if (utilisateur == null || cle == null || valeur == null)
      return false;
    
    valeur = valeur.trim();
    
    if (cle.equals("idCLNOM"))
      utilisateur.getMonPanier().setCLNOM(valeur);
    else if (cle.equals("idCLCPL"))
      utilisateur.getMonPanier().setCLCPL(valeur);
    else if (cle.equals("idCLRUE"))
      utilisateur.getMonPanier().setCLRUE(valeur);
    else if (cle.equals("idCLLOC"))
      utilisateur.getMonPanier().setCLLOC(valeur);
    else if (cle.equals("idCLVIL"))
      utilisateur.getMonPanier().setCLVIL(valeur);
    else if (cle.equals("idCLPOS"))
      utilisateur.getMonPanier().setCLPOS(valeur);
    else if (cle.equals("idDetailsRetrait"))
      utilisateur.getMonPanier().setDetailsRecuperation(valeur);
    else if (cle.equals("idDateRetrait"))
      utilisateur.getMonPanier().setDateRecuperation(Outils.transformerDateHumaineEnSeriem(valeur));
    else if (cle.equals("idReferenceLongue"))
      utilisateur.getMonPanier().setE1RCC(valeur.toUpperCase());
    
    return true;
  }
  
}
