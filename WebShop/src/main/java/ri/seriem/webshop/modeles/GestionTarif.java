//=================================================================================================
//==>                                                                       26/05/2014 - 18/06/2014
//==> Permet la r�cup�ration d'un tarif pour un article  
//==> A faire:
//==> Exemple: voir test11
//=================================================================================================

package ri.seriem.webshop.modeles;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.seriem.libas400.database.field.Field;
import ri.seriem.libas400.database.field.FieldAlpha;
import ri.seriem.libas400.database.field.FieldDecimal;
import ri.seriem.libas400.system.CallProgram;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.webshop.webservice.article.prix.ListePrixArticle;
import ri.seriem.webshop.webservice.article.prix.PrixArticle;

public class GestionTarif {
  // Constantes
  private static final int LG_DATASTRUCT_PPAR1 = 109;
  private static final int LG_DATASTRUCT_PPAR2 = 657;
  
  private static final int LG_ETB = 3;
  private static final int LG_NUMCLI = 6;
  private static final int LG_LIV = 3;
  private static final int LG_CODEART = 20;
  private static final int LG_QTE = 11;
  private static final int LG_QTE_DEC = 3;
  private static final int LG_CNV = 38;
  private static final int LG_TARIF = 9;
  private static final int LG_TARIF_DEC = 2;
  
  // Variables sp�cifiques du programme RPG
  private char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', ' ', ' ' };
  private FieldAlpha etablissement = new FieldAlpha(LG_ETB);
  private FieldDecimal numClient = new FieldDecimal(LG_NUMCLI, 0);
  private FieldDecimal livClient = new FieldDecimal(new BigDecimal(0), LG_LIV, 0);
  private FieldAlpha codeArticle = new FieldAlpha(LG_CODEART);
  private FieldDecimal quantite = new FieldDecimal(new BigDecimal(1), LG_QTE, LG_QTE_DEC);
  private FieldAlpha nope1 = new FieldAlpha("", 7);
  private FieldAlpha codeCNV = new FieldAlpha("", LG_CNV);
  private FieldAlpha nope2 = new FieldAlpha("", 2);
  private FieldDecimal tarif = new FieldDecimal(new BigDecimal(0), LG_TARIF, LG_TARIF_DEC);
  private FieldAlpha nope3 = new FieldAlpha("", 6);
  
  // Varibles
  private boolean init = false;
  private SystemManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT_PPAR1);
  private AS400Text ppar2 = new AS400Text(LG_DATASTRUCT_PPAR2);
  private char lettre = 'W';
  private String curlib = null;
  private CallProgram rpg = null;
  private StringBuilder sb = new StringBuilder();
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public GestionTarif(SystemManager sys, char let, String curlib) {
    system = sys;
    setLettre(let);
    setCurlib(curlib);
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init) {
      // System.out.println("GestionTarif d�j� initialis�");
      return true;
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT_PPAR1);
    rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/" + lettre + "GVMAS.LIB/SGVMCCA1.PGM", null);
    init = rpg.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + lettre + "')");
    System.out.println("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + lettre + "') -> " + init);
    if (!init) {
      System.out.println("Erreur CHGDTAARA: " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
      return false;
    }
    
    // init = rpg.addLibrary(curlib, true);
    rpg.addLibrary(curlib, true);
    // if (!init)
    System.out.println("ChgCurlib: " + curlib + " -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    
    rpg.addLibrary("M_GPL", false);
    // if (!init)
    System.out.println("AddLibrary: M_GPL -> " + " -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    rpg.addLibrary(lettre + "EXPAS", false);
    // if (!init)
    System.out.println("AddLibrary: " + lettre + "EXPAS -> " + rpg.getMsgError() + " -> " + rpg.getMsgError() + " CODE ERREUR -> "
        + rpg.getCodeErreur());
    rpg.addLibrary(lettre + "GVMAS", false);
    // if (!init)
    System.out.println("Erreur addLibrary: " + lettre + "GVMAS -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    // Pourquoi juste le test sur cet addlibl ? du coup ca fausse la partie. Peut-�tre faudrait-il juste changer et
    // tester init sur l'execute plus haut
    rpg.addLibrary(lettre + "GVMX", false);
    // if (!init)
    System.out.println("Erreur addLibrary: " + lettre + "GVMX -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @param valEtb
   * @param valNumCli
   * @param valLivCli
   * @param valCodeArt
   * @param valQte
   * @return
   */
  public boolean execute(String valEtb, BigDecimal valNumCli, BigDecimal valLivCli, String valCodeArt, BigDecimal valQte) {
    setValueEtablissement(valEtb);
    setValueNumClient(valNumCli);
    setValueLivClient(valLivCli);
    setValueCodeArticle(valCodeArt);
    setValueQuantite(valQte);
    setValueTarif(BigDecimal.ZERO);
    return executeArticle();
  }
  
  /**
   * Effectue la recherche du tarif pour une liste d'article.
   * @param valEtb
   * @param valNumCli
   * @param valLivCli
   * @param pListePrixArticle
   * @param valQte
   * @return
   */
  public boolean execute(String valEtb, BigDecimal valNumCli, BigDecimal valLivCli, ListePrixArticle pListePrixArticle,
      BigDecimal valQte) {
    setValueEtablissement(valEtb);
    setValueNumClient(valNumCli);
    setValueLivClient(valLivCli);
    setValueQuantite(valQte);
    setValueTarif(BigDecimal.ZERO);
    return executeListeArticle(pListePrixArticle);
  }
  
  /**
   * Effectue la recherche du tarif pour un article.
   * @return
   */
  private boolean executeArticle() {
    if (rpg == null) {
      return false;
    }
    
    // Construction du param�tre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(etablissement.toFormattedString()).append(numClient.toFormattedString())
        .append(livClient.toFormattedString()).append(codeArticle.toFormattedString()).append(quantite.toFormattedString())
        .append(nope1.toFormattedString()).append(codeCNV.toFormattedString()).append(nope2.toFormattedString())
        .append(tarif.toFormattedString());
    try {
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      rpg.setParameterList(parameterList);
      // Appel du programme RPG
      if (rpg.execute()) {
        // R�cup�ration de la variable XPPVC de la DS PPAR1 (voir SGVMCCA1)
        String valeur = (String) ppar1.toObject(parameterList[0].getOutputData());
        if (valeur != null) {
          // System.out.println("Valeur tarif dans GestionTarif:" + valeur);
          tarif.setValue(new BigDecimal(valeur.substring(100, 109)));
        }
        return true;
      }
    }
    catch (PropertyVetoException e) {
    }
    return false;
  }
  
  /**
   * Effectue la recherche du tarif pour une liste d'article.
   * Pour la version optimis�e.
   * @return
   */
  private boolean executeListeArticle(ListePrixArticle pListePrixArticle) {
    if (rpg == null) {
      return false;
    }
    
    // Construction du param�tre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(etablissement.toFormattedString()).append(numClient.toFormattedString())
        .append(livClient.toFormattedString()).append(quantite.toFormattedString()).append(nope3.toFormattedString())
        .append(codeCNV.toFormattedString());
    for (PrixArticle prixArticle : pListePrixArticle.getListePrix()) {
      setValueCodeArticle(prixArticle.getCodeArticle());
      sb.append(codeArticle.toFormattedString()).append(tarif.toFormattedString());
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT_PPAR2);
    rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/" + lettre + "GVMAS.LIB/SGVMCCA2.PGM", null);
    init = rpg.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + lettre + "')");
    System.out.println("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + lettre + "') -> " + init);
    if (!init) {
      System.out.println("Erreur CHGDTAARA: " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
      return false;
    }
    
    // init = rpg.addLibrary(curlib, true);
    rpg.addLibrary(curlib, true);
    // if (!init)
    // System.out.println("ChgCurlib: " + curlib + " -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    
    rpg.addLibrary("M_GPL", false);
    // if (!init)
    // System.out.println("AddLibrary: M_GPL -> " + " -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    rpg.addLibrary(lettre + "EXPAS", false);
    // if (!init)
    // System.out.println("AddLibrary: " + lettre + "EXPAS -> " + rpg.getMsgError() + " -> " + rpg.getMsgError() + " CODE ERREUR -> "
    // + rpg.getCodeErreur());
    rpg.addLibrary(lettre + "GVMAS", false);
    // if (!init)
    // System.out.println("Erreur addLibrary: " + lettre + "GVMAS -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    // Pourquoi juste le test sur cet addlibl ? du coup ca fausse la partie. Peut-�tre faudrait-il juste changer et
    // tester init sur l'execute plus haut
    rpg.addLibrary(lettre + "GVMX", false);
    // if (!init)
    // System.out.println("Erreur addLibrary: " + lettre + "GVMX -> " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
    
    try {
      parameterList[0].setInputData(ppar2.toBytes(sb.toString()));
      rpg.setParameterList(parameterList);
      // Appel du programme RPG
      if (rpg.execute()) {
        int posd = 97;
        int posf = 106;
        String valeur = (String) ppar2.toObject(parameterList[0].getOutputData());
        // System.out.println("--> valeur : " + valeur);
        for (PrixArticle prixArticle : pListePrixArticle.getListePrix()) {
          // R�cup�ration de la variable ?PPVC de la DS PPAR2 (voir SGVMCCA2)
          if (valeur != null) {
            tarif.setValue(new BigDecimal(valeur.substring(posd, posf)));
            prixArticle.modifierPrixClientBrut(tarif.getValue());
          }
          // Saut au prochain champ prix de vente
          posd += 29;
          posf += 29;
        }
        return true;
      }
    }
    catch (PropertyVetoException e) {
    }
    return false;
  }
  
  // -- M�thodes priv�es
  
  /**
   * Formate une variable de type String (avec cadrage � droite, notamment pour l'�tablissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null)
      return String.format("%" + lg + "." + lg + "s", "");
    else
      return String.format("%" + lg + "." + lg + "s", valeur);
  }
  
  // -- Accesseurs
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre � d�finir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ')
      this.lettre = lettre;
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib � d�finir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs � d�finir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  /**
   * Retourne le champ Etablissement
   * @return le etablissement
   */
  public Field<String> getEtablissement() {
    return etablissement;
  }
  
  /**
   * Initialise la valeur de l'�tablissement
   * @param etablissement le etablissement � d�finir
   */
  public void setValueEtablissement(String etablissement) {
    this.etablissement.setValue(formateVar(etablissement, LG_ETB));
  }
  
  /**
   * Retourne le champ Num�ro client
   * @return le numClient
   */
  public Field<BigDecimal> getNumClient() {
    return numClient;
  }
  
  /**
   * Initialise la valeur du num�ro client
   * @param numClient le numClient � d�finir
   */
  public void setValueNumClient(BigDecimal numClient) {
    this.numClient.setValue(numClient);
  }
  
  /**
   * Retourne le champ suffixe liv
   * @return le liv
   */
  public Field<BigDecimal> getLivClient() {
    return livClient;
  }
  
  /**
   * Initialise la valeur du suffixe liv
   * @param liv le liv � d�finir
   */
  public void setValueLivClient(BigDecimal liv) {
    this.livClient.setValue(liv);
  }
  
  /**
   * Retourne le champ code Article
   * @return le codeArticle
   */
  public Field<String> getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Initialise la valeur du code Article
   * @param codeArticle le codeArticle � d�finir
   */
  public void setValueCodeArticle(String codeArticle) {
    this.codeArticle.setValue(codeArticle);
  }
  
  /**
   * Retourne le champ Quantit�
   * @return le quantite
   */
  public Field<BigDecimal> getQuantite() {
    return quantite;
  }
  
  /**
   * Initialise la valeur de la quantit�
   * @param quantite le quantite � d�finir
   */
  public void setValueQuantite(BigDecimal quantite) {
    this.quantite.setValue(quantite);
  }
  
  /**
   * Retourne le champ codeCNV
   * @return le codeCNV
   */
  public Field<String> getCodeCNV() {
    return codeCNV;
  }
  
  /**
   * Initialise la valeur du code CNV
   * @param codeCNV le codeCNV � d�finir
   */
  public void setValueCodeCNV(String codeCNV) {
    this.codeCNV.setValue(codeCNV);
  }
  
  /**
   * R�cup�re le champ tarif apr�s l'appel du programme RPG
   * @return
   */
  public Field<BigDecimal> getTarif() {
    return tarif;
  }
  
  /**
   * Initialise la valeur du tarif
   * @param tarif le tarif � d�finir
   */
  public void setValueTarif(BigDecimal tarif) {
    this.tarif.setValue(tarif);
  }
  
}
