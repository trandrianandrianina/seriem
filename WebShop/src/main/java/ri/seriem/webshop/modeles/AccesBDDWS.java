
package ri.seriem.webshop.modeles;

import java.sql.Connection;
import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.database.record.GenericRecordManager;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.outils.Outils;

/**
 * Classe Mod�le permettant d'acc�der aux donn�es de DB2 Serie M et XWEBSHOP
 */
public class AccesBDDWS extends GenericRecordManager {
  
  Utilisateur utilisateur = null;
  Connection db2 = null;
  MesLogs mesLogs = null;
  
  /**
   * Constructeur de la classe AccesBDDWS
   */
  public AccesBDDWS(Connection database, Utilisateur util) {
    super(database);
    db2 = database;
    utilisateur = util;
    mesLogs = new MesLogs();
    mesLogs.setIS_EN_DEBUG_SPEC(ConstantesDebug.DEBUG_SQL);
  }
  
  /**
   * Ajoute une connexion/deconnexion utilisateur dans la base LOGSW
   */
  public int ajoutUnLog(int user_id, String session, String type_log, String message, String classe) {
    // Ins�rer un log
    return requete(
        "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".LOGSW " + " (LO_USER,LO_SESS,LO_TYPE,LO_MESS,LO_CLASS,LO_DATE,LO_HEURE) "
            + " VALUES ('" + user_id + "','" + session + "','" + type_log + "','" + message.trim() + "','" + classe + "','"
            + Outils.recupererDateCouranteInt() + "','" + Outils.recupererHeureCouranteInt() + "')");
  }
  
  /**
   * R�cup�ration de la m�thode de queryManager afin de logger de mani�re g�n�rique le SELECT
   */
  public ArrayList<GenericRecord> select(String requete, Class<?> classe) {
    ArrayList<GenericRecord> liste = select(requete);
    if (classe != null) {
      mesLogs.setClasseQuiLogge(classe.getSimpleName());
    }
    if (mesLogs.loggerSiFalse("REQUETE " + requete + " -> " + getMsgError(), liste != null))
      mesLogs.loggerSiDEBUG("REQUETE " + requete);
    
    return liste;
  }
  
  /**
   * R�cup�ration de la m�thode de queryManager afin de logger de mani�re g�n�rique l'UPDATE, INSERT ou CREATE
   */
  public int requete(String requete, Class<?> classe) {
    int retour = super.requete(requete);
    
    if (classe != null) {
      mesLogs.setClasseQuiLogge(classe.getSimpleName());
    }
    if (mesLogs.loggerSiFalse("REQUETE " + requete + " -> " + retour, retour > -1))
      mesLogs.loggerSiDEBUG("REQUETE " + requete + " -> " + retour);
    
    return retour;
  }
  
  /**
   * R�cup�ration de la m�thode de queryManager afin de logger de mani�re g�n�rique l'UPDATE, INSERT ou CREATE
   */
  public int requete(String requete, GenericRecord record, Class<?> classe) {
    int retour = super.requete(requete, record);
    
    if (classe != null) {
      mesLogs.setClasseQuiLogge(classe.getSimpleName());
    }
    if (mesLogs.loggerSiFalse("REQUETE " + requete + " -> " + retour, retour > -1))
      mesLogs.loggerSiDEBUG("REQUETE " + requete + " -> " + retour);
    
    return retour;
  }
  
  // -- Accesseurs
  
  public Connection getDb2() {
    return db2;
  }
  
  public void setDb2(Connection db2) {
    this.db2 = db2;
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
