//=================================================================================================
//==>                                                                       26/05/2014 - 18/06/2014
//==> Permet la r�cup�ration d'un tarif pour un article  
//==> A faire:
//==> Exemple: voir test11
//=================================================================================================

package ri.seriem.webshop.modeles;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.seriem.libas400.database.field.FieldAlpha;
import ri.seriem.libas400.database.field.FieldDecimal;
import ri.seriem.libas400.system.CallProgram;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;

public class DuplicDevis {
  // Constantes
  private static final int LG_PARAM_ENVOI = 21;
  //
  private static final int LG_DATASTRUCT = 35;
  
  private static final int LG_PICOD = 1;
  private static final int LG_PIETB = 3;
  private static final int LG_PINUM = 6;
  private static final int LG_SUF = 1;
  private static final int LG_ERREURS = 3;
  
  // Variables entr�e sp�cifiques du programme RPG
  private char[] indicateurs = { '2', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
  private FieldAlpha PICOD = new FieldAlpha(LG_PICOD);
  private FieldAlpha PIETB = new FieldAlpha(LG_PIETB);
  private FieldDecimal PINUM = new FieldDecimal(LG_PINUM, 0);
  private FieldDecimal PISUF = new FieldDecimal(new BigDecimal(0), LG_SUF, 0);
  
  // param�tres retour
  private String RETOUR_PGM = null;
  private String TYP_RETOUR = null;
  private String ETB_RETOUR = null;
  private String CDE_RETOUR = null;
  private int ERREURS = 0;
  
  // 001 Duplication effectu�e
  public final static int PAS_ERREUR = 1;
  // 002 Bon d'origine inexistant
  public final static int ERREUR_BON = 2;
  // 003 L'�tat du devis doit �tre valid�
  public final static int ERREUR_VALID = 3;
  // 004 Date de validit� sup�rieur � la date du jour
  public final static int ERREUR_DATE = 4;
  // Erreur de contenu de retour
  public final static int ERREUR_CONT = 99;
  
  // Variables
  private boolean init = false;
  private SystemManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
  private char lettre = 'W';
  private String curlib = null;
  private CallProgram rpg = null;
  private StringBuilder sb = new StringBuilder();
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public DuplicDevis(SystemManager sys, char let, String curlib) {
    system = sys;
    setLettre(let);
    setCurlib(curlib);
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init)
      return true;
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
    rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/" + lettre + "GVMAS.LIB/VGVM25WS.PGM", null);
    // System.out.println("crea RPG 1 " + rpg.getCodeErreur() + " -> " + rpg.getMsgError());
    init = rpg.execute("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + lettre + "')");
    // System.out.println("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('"+lettre+"') -> " + init );
    if (!init) {
      System.out.println("Erreur CHGDTAARA: " + rpg.getMsgError() + " CODE ERREUR -> " + rpg.getCodeErreur());
      return false;
    }
    
    rpg.addLibrary(curlib, true);
    rpg.addLibrary(ConstantesEnvironnement.ENVIRONNEMENT, false);
    rpg.addLibrary(lettre + "EXPAS", false);
    rpg.addLibrary(lettre + "GVMAS", false);
    init = rpg.addLibrary(lettre + "GVMX", false);
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @param valEtb
   * @param valNumCli
   * @param valLivCli
   * @param valCodeArt
   * @param valQte
   * @return
   */
  public boolean execute(String pCode, String pEtb, BigDecimal pNum, BigDecimal pSuf) {
    this.PICOD.setValue(formateVar(pCode, LG_PICOD));
    this.PIETB.setValue(formateVar(pEtb, LG_PIETB));
    this.PINUM.setValue(pNum);
    this.PISUF.setValue(pSuf);
    
    return execute();
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  private boolean execute() {
    if (rpg == null)
      return false;
    
    RETOUR_PGM = null;
    TYP_RETOUR = null;
    ETB_RETOUR = null;
    CDE_RETOUR = null;
    ERREURS = 0;
    
    // Construction du param�tre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(PICOD.toFormattedString()).append(PIETB.toFormattedString()).append(PINUM.toFormattedString())
        .append(PISUF.toFormattedString());
    try {
      // System.out.println("CONCAT PARAMETRES: " + sb.toString());
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      rpg.setParameterList(parameterList);
      // Appel du programme RPG
      if (rpg.execute()) {
        // System.out.println("[DuplicDevis] Ok " + rpg.getCodeErreur() + " " + rpg.getMsgError());
        
        RETOUR_PGM = (String) ppar1.toObject(parameterList[0].getOutputData());
        if (RETOUR_PGM != null) {
          return traiterRetour() == DuplicDevis.PAS_ERREUR;
        }
      }
      else
        System.out.println("[DuplicDevis]probleme de rpg.execute(): " + rpg.getCodeErreur() + " -> " + rpg.getMsgError());
    }
    catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  /**
   * ON d�coupe la chaine de retour en variables m�tier: Type, etb, commande, erreurs. On retourne le code erreur apr�s
   * v�rifications du d�coupage
   */
  private int traiterRetour() {
    if (RETOUR_PGM == null || RETOUR_PGM.length() != LG_DATASTRUCT) {
      ERREURS = ERREUR_CONT;
      return ERREURS;
    }
    
    int debut = LG_PARAM_ENVOI;
    int fin = debut + LG_PICOD;
    TYP_RETOUR = RETOUR_PGM.substring(debut, fin);
    debut = fin;
    fin = debut + LG_PIETB;
    ETB_RETOUR = RETOUR_PGM.substring(debut, fin);
    debut = fin;
    fin = debut + LG_PINUM + LG_SUF;
    CDE_RETOUR = RETOUR_PGM.substring(debut, fin);
    
    try {
      debut = fin;
      fin = debut + LG_ERREURS;
      ERREURS = Integer.parseInt(RETOUR_PGM.substring(debut, fin));
    }
    catch (NumberFormatException e) {
      ERREURS = ERREUR_CONT;
      return ERREURS;
    }
    
    // On contr�le le contenu du d�coupage
    if ((TYP_RETOUR == null || TYP_RETOUR.length() != LG_PICOD) || (ETB_RETOUR == null || ETB_RETOUR.length() != LG_PIETB)
        || (CDE_RETOUR == null || CDE_RETOUR.length() != LG_PINUM + LG_SUF)) {
      ERREURS = ERREUR_CONT;
    }
    
    return ERREURS;
  }
  
  // -- M�thodes priv�es ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage � droite, notamment pour l'�tablissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null)
      return String.format("%" + lg + "." + lg + "s", "");
    else
      return String.format("%" + lg + "." + lg + "s", valeur);
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre � d�finir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ')
      this.lettre = lettre;
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib � d�finir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs � d�finir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  public String getTYP_RETOUR() {
    return TYP_RETOUR;
  }
  
  public String getETB_RETOUR() {
    return ETB_RETOUR;
  }
  
  public String getCDE_RETOUR() {
    return CDE_RETOUR;
  }
  
  public int getERREURS() {
    return ERREURS;
  }
  
}
