
package ri.seriem.webshop.outils;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.vues.PatternErgonomie;

public class EnvoiMail extends MesLogs {
  private Utilisateur utilisateur = null;
  
  public EnvoiMail(Utilisateur util) {
    utilisateur = util;
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_PANIER;
    classeQuiLogge = this.getClass().getSimpleName();
  }
  
  private PatternErgonomie joliCourrier = null;
  private Session mailSession = null;
  private Properties props = null;
  private InternetAddress recipientTo = null;
  private InternetAddress recipientFrom = null;
  private Message message = null;
  private String smtp_host = null;
  private int smtp_port = 0;
  private String profil_mail_envoi = null;
  private String mp_mail_envoi = null;
  
  // private static final Logger log = Logger.getLogger(EnvoiMail.class);
  /**
   * Envoyer un mail avec une adresse mail le sujet et le contenu. bas� sur les param�tres SMTP du serveur
   */
  public void envoyerUnMail(String adresseMail, String sujet, String contenuMessage) {
    if (utilisateur == null || adresseMail == null)
      return;
    else {
      if (utilisateur.getETB_EN_COURS() == null) {
        // CODE POUR ATTRIBUER HOST PORT MAIL ET MP pour un envoi public
        smtp_host = ConstantesEnvironnement.MAIL_MASTER_SMTP;
        smtp_port = ConstantesEnvironnement.MAIL_MASTER_PORT_SMTP;
        profil_mail_envoi = ConstantesEnvironnement.MAIL_MASTER_PROFIL;
        mp_mail_envoi = ConstantesEnvironnement.MAIL_MASTER_MP;
      }
      else {
        smtp_host = utilisateur.getETB_EN_COURS().getMail_smtp_host();
        smtp_port = utilisateur.getETB_EN_COURS().getMail_smtp_port();
        profil_mail_envoi = utilisateur.getETB_EN_COURS().getMail_profil();
        mp_mail_envoi = utilisateur.getETB_EN_COURS().getMail_mp();
      }
    }
    
    try {
      // Dans le cas de tests lors du d�veloppement
      if (ConstantesEnvironnement.MAIL_IS_MODE_TEST)
        adresseMail = ConstantesEnvironnement.MAIL_TESTS;
      
      // On met en place les propri�t�s de base
      props = new Properties();
      props.put("mail.smtp.host", smtp_host);
      props.put("mail.smtp.port", smtp_port);
      props.put("mail.smtp.auth", true);
      
      // Pattern pour mise en forme du mail en HTML + CSS
      if (joliCourrier == null)
        joliCourrier = new PatternErgonomie();
      
      if (mailSession == null)
        mailSession = Session.getDefaultInstance(props, new Authenticator() {
          @Override
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(profil_mail_envoi, Base64Coder.decodeString(Base64Coder.decodeString(mp_mail_envoi)).trim());
          }
        });
      
      message = new MimeMessage(mailSession);
      
      mailSession.setDebug(ConstantesEnvironnement.MODE_DEBUG);
      
      recipientTo = new InternetAddress(adresseMail);
      recipientFrom = new InternetAddress(profil_mail_envoi);
      
      message.setRecipient(Message.RecipientType.TO, recipientTo);
      message.setFrom(recipientFrom);
      
      message.setSubject(sujet);
      message.setContent(joliCourrier.afficherMaquetteMail(contenuMessage), "text/html");
      
      Transport.send(message);
      
    }
    catch (NoSuchProviderException e) {
      forcerLogErreur("Pas de transport disponible pour ce protocole " + e.getMessage());
    }
    catch (AddressException e) {
      forcerLogErreur("Adresse invalide " + e.getMessage());
    }
    catch (MessagingException e) {
      forcerLogErreur("Erreur dans le message " + e.getMessage());
    }
    catch (Exception e) {
      forcerLogErreur("Erreur ind�finie " + e.getMessage());
    }
  }
  
}
