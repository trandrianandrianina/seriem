/**
 * Classe permettant le mappage des dossiers contenant des doc (images, ...) devant �tre accessibles par une URL
 */

package ri.seriem.webshop.outils;

// Voir http://www.developpez.net/forums/d822221/java/serveurs-conteneurs-java-ee/tomcat/image-url-context-webapps/
//      http://www.avajava.com/tutorials/lessons/what-is-a-filter-and-how-do-i-use-it.html

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;

public class FilterFile implements Filter {
  // private static String IMAGE_REPOSITORY;
  private static String dossier_virtuel_specifique = null;
  private static String dossier_virtuel_ifs = null;
  private static int longueur_dossier_virtuel_specifique = 0;
  private static int longueur_dossier_virtuel_ifs = 0;
  
  @Override
  public void init(FilterConfig config) throws ServletException {
    // IMAGE_REPOSITORY = config.getInitParameter("IMAGE_REPOSITORY");
    // System.out.println("-FilterFile->" + IMAGE_REPOSITORY);
    
    // On pr�pare les chemins racines virtuels
    dossier_virtuel_specifique = "/" + ConstantesEnvironnement.DOSSIER_WS + ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE.substring(1);
    longueur_dossier_virtuel_specifique = dossier_virtuel_specifique.length();
    dossier_virtuel_ifs = "/" + ConstantesEnvironnement.DOSSIER_WS + ConstantesEnvironnement.DOSSIER_VIRTUEL_IFS.substring(1);
    longueur_dossier_virtuel_ifs = dossier_virtuel_ifs.length();
  }
  
  @Override
  public void destroy() {
  }
  
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    File file = null;
    BufferedReader reader = null;
    FileReader fileReader = null;
    ServletContext sc = null;
    try {
      HttpServletRequest _request = (HttpServletRequest) request;
      HttpServletResponse _response = (HttpServletResponse) response;
      
      // Recontruit le bon chemin
      String filePath = _request.getRequestURI();
      // System.out.println("chemin demand�: " + filePath);
      file = getRealName(filePath);
      if (!file.exists()) {
        return;
      }
      
      // Personnalisation de l'ent�te de la r�ponse afin de g�rer les probl�mes de cache
      _response.setDateHeader("Last-Modified", file.lastModified());
      
      // Set content type
      sc = _request.getSession().getServletContext();
      String mime = sc.getMimeType(filePath);
      _response.setContentType(mime);
      
      // Set content size
      _response.setContentLength((int) file.length());
      
      // Open the file and output streams
      FileInputStream in = new FileInputStream(file);
      ServletOutputStream out = _response.getOutputStream();
      
      // Copy the contents of the file to the output stream
      byte[] buf = new byte[2048];
      int count = 0;
      while ((count = in.read(buf)) >= 0) {
        out.write(buf, 0, count);
      }
      in.close();
      out.close();
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      if (fileReader != null)
        try {
          fileReader.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      if (reader != null)
        try {
          reader.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
    }
  }
  
  /**
   * Convertit le chemin virtuel en chemin r�el
   * @param virtualname
   * @return
   */
  private File getRealName(String virtualname) {
    String cheminReel = virtualname;
    
    // On ne remplace que la racine du chemin virtuel afin de conserver l'arborescence voulue
    if (virtualname.startsWith(dossier_virtuel_specifique)) {
      cheminReel = ConstantesEnvironnement.DOSSIER_SPECIFIQUE + virtualname.substring(longueur_dossier_virtuel_specifique);
    }
    else if (virtualname.startsWith(dossier_virtuel_ifs)) {
      cheminReel = ConstantesEnvironnement.DOSSIER_IFS + virtualname.substring(longueur_dossier_virtuel_ifs);
    }
    
    File file = null;
    
    /*if(MarbreEnvironnement.isEclipse)
    	file = new File( cheminReel.replace('\\', MarbreEnvironnement.SEPARATEUR_CHAR));
    else	
    	file = new File( cheminReel.replace('/', MarbreEnvironnement.SEPARATEUR_CHAR));*/
    file = new File(cheminReel.replace('/', ConstantesEnvironnement.SEPARATEUR_CHAR));
    
    // System.out.println("-getRealName-> " + virtualname + " -> " + file.getAbsolutePath());
    
    return file;
  }
}
