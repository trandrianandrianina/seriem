
package ri.seriem.webshop.outils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.SaveFile;

import ri.seriem.libas400.database.QueryManager;
import ri.seriem.libas400.system.Library;
import ri.seriem.libas400.system.SystemManager;
import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.libcommun.outils.FileNG;
import ri.seriem.libcommun.outils.GestionFichierTexte;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;

public class Installation extends MesLogs {
  
  // Constantes
  public static final int NOP = 0;
  public static final int INSTALL = 1;
  public static final int UPDATE = 2;
  
  private static final String NAME_PARAMETER_FILE = "ENVIRONM";
  private static final String NAME_FOLDER_SPECIFIC_STANDARD = "specifique";
  private static final String NAME_VERSION_WAR = "versionActuelle.txt";
  private static final String NAME_VERSION_INSTALLED = "version_installed.txt";
  private static final String NAME_LIBRARY_SAVE = "SERIEMFTP";
  private static final String NAME_LOG4J = "log4j.properties";
  
  // Variables
  private SystemManager system = null;
  private boolean statelibrary = false;
  private boolean statefolder = false;
  private String privateFolder = null;
  private File saveFolder = null;
  private int versionInstalled = 0;
  // private EnvoiMail mails = null;
  
  protected String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  /**
   * Constructeur
   */
  public Installation() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_CONTEXT;
    privateFolder = ConstantesEnvironnement.DOSSIER_RACINE_PRIVATE + ConstantesEnvironnement.DOSSIER_WS + File.separatorChar;
    saveFolder = new File(privateFolder + "Sauvegarde");
  }
  
  /**
   * Contr�le des param�tres afin de d�terminer si on doit effectuer une installation ou pas
   * @return
   */
  public int check4Installation() {
    if (!controlVariables()) {
      // Arr�t un peu violent si tout n'est pas ok d�s le d�part
      System.exit(-1);
    }
    // contr�le les fichiers de version
    controlFileVersionInstallee();
    
    System.out.println("\n== Contr�le de l'installation du WebShop");
    // log.info("\n== Contr�le de l'installation du WebShop");
    statelibrary = controlLibrary();
    statefolder = controlFolder();
    
    // On controle ce que l'on doit faire
    int ret = NOP; // 0: on fait rien, 1: on installe, 2: maj
    if (statelibrary && statefolder) {
      if (controlVersion()) {
        ret = UPDATE;
      }
    }
    else {
      ret = INSTALL;
    }
    
    return ret;
  }
  
  /**
   * Contr�le l'existence du dossier sp�cifique
   * @return
   */
  private boolean controlFolder() {
    boolean ret = false;
    
    System.out.println("-- Contr�le du dossier : " + ConstantesEnvironnement.DOSSIER_SPECIFIQUE);
    
    File specific = new File(ConstantesEnvironnement.DOSSIER_SPECIFIQUE);
    if (specific.exists()) {
      System.out.println("\t--> Pr�sent            : [ OK ]");
      ret = (specific.list().length > 0);
      if (ret) {
        System.out.println("\t--> Contenu            : [ OK ]");
      }
      else {
        System.out.println("\t--> Contenu            : [ NO ]");
      }
    }
    else {
      System.out.println("\t--> Pr�sent            : [ NO ]");
    }
    
    return ret;
  }
  
  /**
   * Contr�le l'existence du fichier de version, le lit s'il existe et le compare � celui du WAR
   * @return
   */
  private boolean controlVersion() {
    boolean ret = false;
    
    System.out.println("-- AV Contr�le de la version: " + versionInstalled);
    
    File fversioninstalled = new File(privateFolder + NAME_VERSION_INSTALLED);
    if (fversioninstalled.exists()) {
      GestionFichierTexte gft = new GestionFichierTexte(fversioninstalled);
      String version = gft.getContenuFichierString(false);
      versionInstalled = convertVersion(version);
      gft.dispose();
      
      ret = true;
    }
    System.out.println("\t--> Install�e            : " + versionInstalled);
    System.out.println("\t--> Du WAR               : " + VersionWar.getVersionNum());
    
    // true si on fait une mise � jour, false sinon (installation ou rien)
    ret = ((versionInstalled < VersionWar.getVersionNum()) && (versionInstalled > 0));
    if (ret)
      System.out.println("\t--> Une mise � jour va �tre effectu�e");
    
    return ret;
  }
  
  /**
   * Contr�le l'existence de la biblioth�que et du fichier param�tre
   * @return
   */
  private boolean controlLibrary() {
    boolean ret = false;
    
    System.out.println("-- Contr�le de la biblioth�que : " + ConstantesEnvironnement.BIBLI_WS);
    
    SystemManager system = new SystemManager(ConstantesEnvironnement.ADRESSE_AS400, ConstantesEnvironnement.PROFIL_AS_ANONYME,
        Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_ANONYME)), false);
    if (system.isConnected()) {
      System.out.println("\t--> Connexion          : [ OK ]");
      
      Library lib = new Library(system.getSystem(), ConstantesEnvironnement.BIBLI_WS);
      if (lib.isExists()) {
        System.out.println("\t--> Bib pr�sente       : [ OK ]");
        IFSFile parameterfile = new IFSFile(system.getSystem(), QSYSObjectPathName.toPath(lib.getName(), NAME_PARAMETER_FILE, "FILE"));
        try {
          ret = parameterfile.exists();
          if (ret) {
            System.out.println("\t--> Fichier " + NAME_PARAMETER_FILE + "   : [ OK ]");
          }
          else {
            System.out.println("\t--> Fichier " + NAME_PARAMETER_FILE + "   : [ NO ]");
          }
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
      else {
        System.out.println("\t--> Bib pr�sente       : [ NO ]");
      }
    }
    else {
      System.out.println("\t--> Connexion          : [ ER ]\n" + system.getMsgError());
    }
    system.disconnect();
    
    return ret;
  }
  
  /**
   * Contr�le les variables du context.xml obligatoires (� enrichir)
   * @return
   */
  private boolean controlVariables() {
    boolean ret = true;
    String etatadr = "[ OK ]";
    String etatprf = "[ OK ]";
    String etatmdp = "[ OK ]";
    String etatdto = "[ OK ]";
    String etatdpu = "[ OK ]";
    String etatdpr = "[ OK ]";
    String etatdws = "[ OK ]";
    
    System.out.println("== Contr�le des variables");
    
    if ((ConstantesEnvironnement.ADRESSE_AS400 == null) || ConstantesEnvironnement.ADRESSE_AS400.trim().equals("")) {
      etatadr = "[ ER ]";
      ret = false;
    }
    if ((ConstantesEnvironnement.PROFIL_AS_ANONYME == null) || ConstantesEnvironnement.PROFIL_AS_ANONYME.trim().equals("")) {
      etatadr = "[ ER ]";
      ret = false;
    }
    if ((ConstantesEnvironnement.MP_AS_ANONYME == null) || ConstantesEnvironnement.MP_AS_ANONYME.trim().equals("")) {
      etatadr = "[ ER ]";
      ret = false;
    }
    if (!testFolder(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT)) {
      etatdto = "[ ER ]";
      ret = false;
    }
    if (!testFolder(ConstantesEnvironnement.DOSSIER_RACINE_PUBLIC)) {
      etatdpu = "[ ER ]";
      ret = false;
    }
    if (!testFolder(ConstantesEnvironnement.DOSSIER_RACINE_PRIVATE)) {
      etatdpr = "[ ER ]";
      ret = false;
    }
    if (!testFolder(ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP)) {
      etatdws = "[ ER ]";
      ret = false;
      
    }
    
    System.out.println("\t--> Adresse AS400      : " + etatadr);// + " " + MarbreEnvironnement.ADRESSE_AS400);
    System.out.println("\t--> Profil             : " + etatprf);// + " " + MarbreEnvironnement.PROFIL_AS_ANONYME);
    System.out.println("\t--> Mot de passe       : " + etatmdp);// + " " +
                                                                // Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_ANONYME)));
    System.out.println("\t--> Dossier tomcat     : " + etatdto);// + " " + MarbreEnvironnement.DOSSIER_RACINE_TOMCAT
    System.out.println("\t--> Dossier public     : " + etatdpu);// + " " + MarbreEnvironnement.DOSSIER_RACINE_PUBLIC
    System.out.println("\t--> Dossier private    : " + etatdpr);// + " " + MarbreEnvironnement.DOSSIER_RACINE_PRIVATE
    System.out.println("\t--> Dossier webshop    : " + etatdws);// + " " + MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP
    
    if (!ret) {
      System.out.println("--> Il y a des erreurs de param�trage, veuillez corriger votre fichier context.xml.");
    }
    return ret;
  }
  
  /**
   * Teste la validit� d'un dossier
   * @param folder
   * @return
   */
  private boolean testFolder(String folder) {
    if ((folder == null) || folder.trim().equals("")) {
      return false;
    }
    else {
      File dossier = new File(folder);
      if (!dossier.exists()) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Copie le dossier sp�cifique du WAR vers sa destination (et cr�er l'arborescence si besoin)
   */
  private boolean copyFiles2Specific() {
    boolean ret = true;
    
    File travail = new File(ConstantesEnvironnement.DOSSIER_TRAVAIL_PUBLIC);
    if (!travail.exists()) {
      travail.mkdirs();
    }
    File specificstandard = new File(ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + NAME_FOLDER_SPECIFIC_STANDARD);
    
    if (specificstandard.exists()) {
      File destination = new File(FileNG.cleanNameFolder(ConstantesEnvironnement.DOSSIER_SPECIFIQUE));
      try {
        FileNG.copyDirectory(specificstandard, destination);
      }
      catch (IOException e) {
        ret = false;
        msgErreur += "\nLa copie de " + specificstandard.getAbsolutePath() + " vers " + destination + " n'a pas pu se faire.";
      }
    }
    
    return ret;
  }
  
  /**
   * Cr�er la biblioth�que
   * @param system
   * @return
   */
  private boolean createLibrary(SystemManager system) {
    boolean ret = true;
    
    Library lib = new Library(system.getSystem(), ConstantesEnvironnement.BIBLI_WS);
    ret = lib.create();
    if (!ret) {
      msgErreur += "\n" + lib.getMsgError();
    }
    
    return ret;
  }
  
  public boolean executeSQLFiles() {
    File folderVersion = new File(ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF" + File.separatorChar + "versions");
    
    // Listage des fichiers SQL dans le WAR
    File[] listeSQL = folderVersion.listFiles();
    
    if (listeSQL.length == 0) {
      msgErreur += "\nAucun fichier SQL trouv� dans le WAR";
      return false;
    }
    
    // Elimination des fichiers SQL non n�cessaires (bas� sur le num�ro de version)
    for (int i = 0; i < listeSQL.length; i++) {
      FileNG file = new FileNG(listeSQL[i].getAbsolutePath());
      if (!file.getName().endsWith(".sql")
          || ((versionInstalled > 0) && (versionInstalled >= convertVersion(file.getNamewoExtension(false))))) {
        
        listeSQL[i] = null;
        System.out.println("fichier exclus:" + file.getName());
      }
    }
    
    // Execution des fichiers restants
    QueryManager qrm = new QueryManager(system.getdatabase().getConnection());
    boolean ret = true;
    for (File file : listeSQL) {
      if (file != null) {
        System.out.println("---Fichier SQL �x�cut�s--->" + file.getAbsolutePath());
        ret = qrm.executeSQLFile(file.getAbsolutePath(), ConstantesEnvironnement.BIBLI_WS);
        System.out.println("--->" + ret);
        if (!ret) {
          System.out.println("Erreur: " + qrm.getMsgError());
          
          return ret;
        }
      }
    }
    
    return ret;
  }
  
  /**
   * Convertit un num�ro de version de String en int
   * @param version
   * @return
   */
  public int convertVersion(String version) {
    try {
      return Integer.parseInt(version.trim());
    }
    catch (Exception e) {
      return 0;
    }
  }
  
  /**
   * Connexion � l'AS400
   * @return
   */
  private SystemManager connexion() {
    if (system == null) {
      system = new SystemManager(ConstantesEnvironnement.ADRESSE_AS400, ConstantesEnvironnement.PROFIL_AS_ANONYME,
          Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_ANONYME)), true);
      if (!system.isConnected()) {
        return null;
      }
    }
    return system;
  }
  
  /**
   * Proc�de � l'installation d'une partie ou tout de l'application
   */
  public void installation() {
    boolean ret = false;
    
    System.out.println("\n== Installation du WebShop ==");
    if (!statefolder) {
      if (copyFiles2Specific()) {
        System.out.println("\t--> Installation dans l'IFS [ OK ]");
      }
      else {
        System.out.println("\t--> Installation dans l'IFS [ ER ]" + getMsgErreur());
      }
    }
    
    if (!statelibrary) {
      connexion();
      if (system != null) {
        ret = createLibrary(system);
        System.out.println("r�sultat creation biblio-------->" + ret);
        if (ret) {
          ret = executeSQLFiles();
          System.out.println("r�sultat des executions sql----------->" + ret);
          if (ret) {
            System.out.println("\t--> Installation dans 5250 [ OK ]");
            if (!copyFileVersion()) {
              System.out.println("\t--> Mise � jour du num�ro de version [ ER ]");
              System.out.println("\t\t- " + getMsgErreur());
            }
            else {
              System.out.println("\t--> Mise � jour du num�ro de version [ OK ]");
            }
          }
          System.out.println("pas de mise � jour de num�ro de version" + ret);
        }
        system.disconnect();
      }
      if (!ret) {
        System.out.println("\t--> Installation dans 5250 [ ER ]" + getMsgErreur());
      }
    }
  }
  
  /**
   * Proc�de la mise � jour du WebShop
   */
  public void update() {
    System.out.println("\n== Mise � jour du WebShop ==");
    connexion();
    
    // Sauvegarde du dossier xweb dans un SAVF
    System.out.println("\t--> Sauvegarde de la biblioth�que");
    if (!saveLibrary(system)) {
      System.out.println("\t\t" + getMsgErreur());
      return;
    }
    // Copie du SAVF dans le dossier sauvegarde dans l'IFS aaaammjj_hhmm_name
    System.out.println("\t--> Copie du fichier SAVF dans le dossier Sauvegarde");
    if (!copySavfToPrivate(NAME_LIBRARY_SAVE, ConstantesEnvironnement.BIBLI_WS)) {
      System.out.println("\t\t- " + getMsgErreur());
      return;
      
    }
    // A partir de la version du war rechercher tous les fichiers sql � executer dans l'ordre
    System.out.println("\t--> Execution des mises � jour SQL si n�cessaire");
    if (!executeSQLFiles()) {
      System.out.println("\t\t- " + getMsgErreur());
      return;
    }
    
    // Mettre � jour le fichier version dans le dossier private
    System.out.println("\t--> Mise � jour du fichier de version");
    if (!copyFileVersion()) {
      System.out.println("\t\t- " + getMsgErreur());
      return;
    }
    // Mettre � jour le repertoire specifique/images/decoration
    System.out.println("\t--> Mise � jour du repertoire decoration");
    if (!copyimagesdecoration()) {
      System.out.println("\t\t- " + getMsgErreur());
      return;
    }
    // Mettre � jour le repertoire specifique/images
    System.out.println("\t--> Mise � jour du repertoire images");
    if (!copyNewPictureSpecific()) {
      System.out.println("\t\t- " + getMsgErreur());
      return;
    }
    system.disconnect();
    
  }
  
  /**
   * Sauvegarde de la biblioth�que
   * @param system
   * @return
   */
  private boolean saveLibrary(SystemManager system) {
    boolean ret = false;
    
    SaveFile savf = new SaveFile(system.getSystem(), "SERIEMFTP", ConstantesEnvironnement.BIBLI_WS);
    try {
      savf.delete();
      savf.create();
      savf.save(ConstantesEnvironnement.BIBLI_WS);
      ret = true;
    }
    catch (Exception e) {
      msgErreur += "\nErreur lors de la sauvegarde de la bibliloth�que " + ConstantesEnvironnement.BIBLI_WS + "\n" + e;
    }
    
    return ret;
  }
  
  /**
   * Copie le fichier savf vers le dossier sauvegarde dans l'IFS
   * @param lib
   * @param file
   * @return
   */
  private boolean copySavfToPrivate(String lib, String file) {
    if ((lib == null) || (file == null)) {
      return false;
    }
    boolean ret = false;
    
    try {
      // On v�rifie l'existence de la biblioth�que
      String savf = QSYSObjectPathName.toPath(lib, file, "FILE");
      IFSFile fsavf = new IFSFile(system.getSystem(), savf);
      if (!fsavf.exists()) {
        msgErreur += "\nErreur le fichier " + savf + " n'a pas �t� trouv�";
        return false;
      }
      
      // On v�rifie l'existence du dossier private
      if (!saveFolder.exists()) {
        saveFolder.mkdirs();
      }
      
      // Construction du nom du fichier stock�
      Date now = new Date();
      String savfname = saveFolder.getAbsolutePath() + File.separatorChar
          + String.format("%1$tY%1$tm%1$td_%1$tH%1$tM_%2$s_v%3$d.savf", now, file, versionInstalled);
      
      // On copie le SAVF
      StringBuilder sb = new StringBuilder();
      CommandCall cmd = new CommandCall(system.getSystem(), "QSYS/CPYTOSTMF FROMMBR('" + savf + "') TOSTMF('" + savfname
          + "') STMFOPT(*REPLACE) CVTDTA(*NONE) DBFCCSID(*FILE) ENDLINFMT(*FIXED)");
      ret = cmd.run();
      if (!ret) {
        AS400Message[] msg = cmd.getMessageList();
        if (msg != null) {
          System.out.println("-copySavfToPrivate->msg:" + msg.length);
          for (int i = 0; i < msg.length; i++)
            sb.append(msg[i].getText()).append('\n');
          msgErreur += "\nErreur lors de la copie du fichier SAVF dans l'IFS:\n" + sb.toString();
        }
      }
      else {
        fsavf.delete();
      }
    }
    catch (Exception e) {
      msgErreur += "\nErreur lors de la copie du fichier SAVF dans l'IFS:\n" + e;
      ret = false;
    }
    
    return ret;
  }
  
  /**
   * Copie le nouveau fichier de version
   * @return
   */
  private boolean copyFileVersion() {
    FileNG file = new FileNG(
        ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF" + File.separatorChar + "versions" + File.separatorChar + NAME_VERSION_WAR);
    File fileold = new File(privateFolder + NAME_VERSION_INSTALLED.replaceFirst(".txt", ".old"));
    File filenew = new File(privateFolder + NAME_VERSION_INSTALLED);
    
    // On renomme l'ancien fichier (au cas o�)
    filenew.renameTo(fileold);
    // On copie le nouveau
    boolean ret = file.copyTo(filenew.getAbsolutePath());
    System.out.println("copie du fichier_install� dans le dossier priv�" + ret);
    if (ret) {
      fileold.delete();
    }
    else {
      msgErreur += "\nErreur lors de la copie du fichier de version " + filenew.getAbsolutePath();
    }
    
    return ret;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * copie du repertoire images/decoration
   * @throws IOException
   */
  public boolean copyimagesdecoration() {
    boolean res = true;
    
    // repertoire decoration dans webcontent
    // File folderDecoration= new File( MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + "images" + File.separatorChar +
    // "decoration" + File.separatorChar);
    File folderDecoration = new File(ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + NAME_FOLDER_SPECIFIC_STANDARD + File.separatorChar
        + "images" + File.separatorChar + "decoration" + File.separatorChar);
    // repertoire decoration dans specifique
    File folderdestinationdecoration =
        new File(ConstantesEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separatorChar + "decoration" + File.separatorChar);
    
    if (!folderdestinationdecoration.exists())
      folderdestinationdecoration.mkdir();
    
    try {
      // FileNG fileImage = new FileNG (FileNG.cleanNameFolder(folderDecoration + File.separator+
      // liststandart[i].getName()));
      FileNG.copyDirectory(folderDecoration, folderdestinationdecoration);
      res = true;
      
    }
    catch (IOException e) {
      msgErreur += "\nErreur lors de la copie du repertoire decoration dans l'IFS:\n" + e;
      res = false;
    }
    
    return res;
  }
  
  /**
   * copie du fichier log4j.porperties dans .classes
   */
  public boolean copyFileLog4jProperties() {
    boolean res = true;
    
    FileNG folderFrom = new FileNG(ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF" + File.separator + NAME_LOG4J);
    File folderTo = new File(ConstantesEnvironnement.DOSSIER_RACINE_TOMCAT + "webapps" + File.separatorChar + ConstantesEnvironnement.DOSSIER_WS
        + File.separatorChar + "WEB-INF" + File.separatorChar + "classes");
    
    res = folderFrom.copyTo(folderTo.getAbsolutePath() + File.separatorChar + NAME_LOG4J);
    loggerSiDEBUG("copie du fichier Log4j.properties : dans le dossier classes : " + res);
    if (!res) {
      msgErreur += "\nErreur lors de la copie du fichier de Log4j.properties : " + folderTo.getAbsolutePath();
    }
    
    return res;
  }
  
  /**
   * Copie nouvelles images sp�cifiques
   */
  public boolean copyNewPictureSpecific() {
    boolean res = true;
    String currentPictureTo = null;
    FileNG folderSpecifiqueImageTo = new FileNG(
        ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + NAME_FOLDER_SPECIFIC_STANDARD + File.separatorChar + "images" + File.separatorChar);
    
    File folderSpecifiqueImageFrom =
        new File(ConstantesEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + File.separatorChar + "images" + File.separatorChar);
    
    FileNG fichierAcopier = null;
    // ArrayList<String>allPictureTo = new ArrayList<String>();
    // ArrayList<String>allPictureFrom = new ArrayList<String>();
    // liste les images dans le respertoire image xwebshop
    
    File[] listeImagesTo = folderSpecifiqueImageTo.listFiles();
    
    // liste les images dans le repertoire image sur le serveeur
    File[] listeImagesFrom = folderSpecifiqueImageFrom.listFiles();
    
    // recupere les images To dans une arraylist
    
    for (int i = 0; i < listeImagesTo.length; i++) {
      currentPictureTo = listeImagesTo[i].getName();
      for (int j = 0; j < listeImagesFrom.length; j++) {
        if (!currentPictureTo.equals(listeImagesFrom[j].getName())) {
          fichierAcopier = new FileNG(folderSpecifiqueImageTo + File.separator + currentPictureTo);
          
          res = fichierAcopier.copyTo(folderSpecifiqueImageFrom.getAbsolutePath() + File.separator + currentPictureTo);
        }
      }
    }
    loggerSiDEBUG("copie d'une nouvelle image effectu�e: " + res);
    if (!res) {
      msgErreur += "\nErreur lors de la copie de l'image : " + folderSpecifiqueImageFrom.getAbsolutePath();
    }
    
    return res;
  }
  
  public void controlFileVersionInstallee() {
    File fversioninstalled = new File(privateFolder + NAME_VERSION_INSTALLED);
    // File fileActuelle = new File( MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF" + File.separatorChar+
    // "versions" + File.separatorChar + NAME_VERSION_WAR );
    loggerSiDEBUG("== Contr�le des fichiers de Version");
    
    if (fversioninstalled.exists()) {
      // recupere le num�ro de version du fichier install�
      GestionFichierTexte gft = new GestionFichierTexte(fversioninstalled);
      String version = gft.getContenuFichierString(false);
      
      versionInstalled = convertVersion(version);
      System.out.println("versionInstalled : " + versionInstalled);
      gft.dispose();
      if (VersionWar.getVersionNum() != versionInstalled) {
        // contenuMessage = ("Les fichiers de version sont diff�rents : La version install�e est en : "+
        // versionInstalled +" et le fichier actuelle est en :"+VersionWar.getVersionNum());
        loggerSiDEBUG("----------Fichiers de version sont diff�rents un mail va �tre envoy� au WebMaster");
        // RHAAAAA on a pas l'utilisateur il fo ke je trouv une ot soluce
        // on envoi un mail
        /*if (mails==null)
         mails = new EnvoiMail();
        mails.envoyerUnMail(MarbreEnvironnement.MAIL_WEBMASTER, sujet, contenuMessage);*/
      }
    }
    
    // return res;
  }
}
