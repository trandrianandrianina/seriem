
package ri.seriem.webshop.outils;

import ri.seriem.libcommun.outils.GestionFichierTexte;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;

/** classe permettant de r�cuperer la version d'un fichier */
public class VersionWar {
  private static String version = null;
  private static int version_num = 0;
  
  /**
   * Retourne le num�ro de version format� sous forme de chaine
   * @return
   */
  public static String recupereVersion() {
    if (version == null) {
      readFile();
    }
    
    if (version.length() == 3) {
      version = version.substring(0, 1) + "." + version.substring(1, 3);
    }
    else if (version.length() == 4) {
      version = version.substring(0, 2) + "." + version.substring(2, 4);
    }
    else if (version.length() == 5) {
      version = version.substring(0, 1) + "." + version.substring(1, 3) + "/" + version.substring(3, 5);
    }
    
    return version;
  }
  
  /**
   * Retourne le num�ro de version sous forme num�rique
   * @return
   */
  public static int getVersionNum() {
    if (version_num == 0) {
      readFile();
    }
    return version_num;
  }
  
  /**
   * Lecture du fichier de version du WAR
   */
  private static void readFile() {
    GestionFichierTexte gft = new GestionFichierTexte(ConstantesEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF"
        + ConstantesEnvironnement.SEPARATEUR + "versions" + ConstantesEnvironnement.SEPARATEUR + "versionActuelle.txt");
    version = gft.getContenuFichierString(false);
    if (version == null) {
      version = "";
      version_num = 0;
    }
    else {
      version = version.trim();
      try {
        version_num = Integer.parseInt(version);
      }
      catch (NumberFormatException e) {
      }
    }
    gft.dispose();
  }
  
  /**
   * M�thode statique pour r�cup�rer la version
   *
   *
   * public static String recupereVersion()
   * {
   * 
   * String version = "";
   * 
   * try
   * {
   * // lecture du fichier
   * InputStream ips = new FileInputStream(MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + "WEB-INF" +
   * MarbreEnvironnement.SEPARATEUR + "versions" + MarbreEnvironnement.SEPARATEUR + "versionActuelle.txt");
   * InputStreamReader ipsr = new InputStreamReader(ips);
   * BufferedReader br = new BufferedReader(ipsr);
   * 
   * String ligne;
   * 
   * while ((ligne = br.readLine()) != null)
   * {
   * if(ligne.length()==3)
   * version = ligne.substring(0, 1) + "." + ligne.substring(1, 3);
   * else if(ligne.length()==4)
   * version = ligne.substring(0, 2) + "." + ligne.substring(2, 4);
   * }
   * 
   * br.close();
   * }
   * catch (Exception e)
   * {
   * System.out.println(e.toString());
   * }
   * return version;
   * }
   */
  
}
