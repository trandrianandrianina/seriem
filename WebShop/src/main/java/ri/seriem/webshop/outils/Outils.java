
package ri.seriem.webshop.outils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;

import ri.seriem.webshop.environnement.Utilisateur;

public abstract class Outils {
  private static Date dateCourante = null;
  private static int maDate;
  private static int monHeure;
  private static SimpleDateFormat formater = null;
  private final static String separateurDate = "/";
  private static Calendar monCalendrier = null;
  private final static String DEVISE_DEFAUT = " &euro;";
  
  /**
   * R�cup�rer la date courante sous forme de num�rique 1YYMMDD
   */
  public static int recupererDateCouranteInt() {
    dateCourante = new Date();
    maDate = 0;
    monHeure = 0;
    
    formater = new SimpleDateFormat("yyMMdd");
    
    try {
      maDate = Integer.parseInt("1" + formater.format(dateCourante));
    }
    catch (Exception e) {
      maDate = 0;
    }
    
    return maDate;
  }
  
  /**
   * R�cup�rer l'heure/minutes/secondes de la date courante
   */
  public static int recupererHeureCouranteInt() {
    dateCourante = new Date();
    monHeure = 0;
    
    formater = new SimpleDateFormat("HHmmss");
    
    try {
      monHeure = Integer.parseInt(formater.format(dateCourante));
    }
    catch (Exception e) {
      monHeure = 0;
    }
    
    return monHeure;
  }
  
  /**
   * Met en forme la date de SERIEM � partir d'une String *
   */
  public static String transformerDateSeriemEnHumaine(String dateSeriem) {
    String retour = "";
    
    if (dateSeriem == null)
      return retour;
    
    // date pourrave du si�cle dernier
    if (dateSeriem.length() == 7) {
      // r�cup�ration du jour
      retour = dateSeriem.substring(5, 7) + separateurDate;
      // r�cup�ration du mois
      retour += dateSeriem.substring(3, 5) + separateurDate;
      // r�cup�ration de l'ann�e
      retour += 1900 + Integer.parseInt(dateSeriem.substring(0, 3));
    }
    
    return retour;
  }
  
  /**
   * Met en forme la date de SERIEM � partir d'une String *
   */
  public static String transformerDateSeriemEnHumaine(int dateSeriem) {
    String retour = "";
    
    String datePourrie = Integer.toString(dateSeriem);
    
    // dates de notre si�cle
    if (datePourrie != null && datePourrie.length() == 7) {
      // r�cup�ration du jour
      retour = datePourrie.substring(5, 7) + separateurDate;
      // r�cup�ration du mois
      retour += datePourrie.substring(3, 5) + separateurDate;
      // r�cup�ration de l'ann�e
      retour += 1900 + Integer.parseInt(datePourrie.substring(0, 3));
    }
    // Ann�e avant 2000 (le 0 disparait)
    else {
      if (datePourrie != null && datePourrie.length() == 6) {
        // r�cup�ration du jour
        retour = datePourrie.substring(4, 6) + separateurDate;
        // r�cup�ration du mois
        retour += datePourrie.substring(2, 4) + separateurDate;
        // r�cup�ration de l'ann�e
        retour += 1900 + Integer.parseInt(datePourrie.substring(0, 2));
      }
    }
    
    return retour;
  }
  
  /**
   * A partir d'une date affich�e correctement retourner une date s�rie M
   * pourrie
   */
  public static int transformerDateHumaineEnSeriem(String dateSeriem) {
    int retour = 0;
    String format = "";
    if (dateSeriem == null || dateSeriem.length() != 10)
      return retour;
    
    if (dateSeriem.substring(6, 10).startsWith("2"))
      format += "1" + dateSeriem.substring(8, 10);
    else
      format += "0" + dateSeriem.substring(8, 10);
    
    format += dateSeriem.substring(3, 5);
    format += dateSeriem.substring(0, 2);
    
    try {
      retour = Integer.parseInt(format);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return retour;
  }
  
  /**
   * Met en forme l'heure en HH:MM:SS
   */
  public static String miseEnFormeHeure(String heure) {
    String retour = "";
    retour = heure.substring(4, 6) + ":" + heure.substring(6, 8) + ":" + heure.substring(8, 10);
    
    return retour;
    
  }
  
  /**
   * Retourne le mois en clair
   */
  public static String retourneMois(String mois) {
    String retour = "";
    
    switch (Integer.parseInt(mois)) {
      case 1:
        retour = "Janvier";
        break;
      case 2:
        retour = "Fevrier";
        break;
      case 3:
        retour = "Mars";
        break;
      case 4:
        retour = "Avril";
        break;
      case 5:
        retour = "Mai";
        break;
      case 6:
        retour = "Juin";
        break;
      case 7:
        retour = "Juillet";
        break;
      case 8:
        retour = "Aout";
        break;
      case 9:
        retour = "Septembre";
        break;
      case 10:
        retour = "Octobre";
        break;
      case 11:
        retour = "Novembre";
        break;
      case 12:
        retour = "Decembre";
        break;
      default:
        retour = "";
        
    }
    
    return retour;
    
  }
  
  /**
   * Permet d'�chapper tous les caract�res sp�ciaux en caract�res HTML
   */
  public static String affichageHTML_speciaux(String brut) {
    brut = StringEscapeUtils.escapeHtml4(brut);
    
    return brut;
  }
  
  /**
   * Afficher une valeur d�cimale (stock, conditionnement) correctement HORS PRIX
   */
  public static String afficherValeurCorrectement(String donnee) {
    if (donnee == null || donnee.trim().equals(""))
      return null;
    String retour = donnee;
    
    // System.out.println("affichage du stock, val donnee " + donnee) ;
    if (donnee.endsWith(".000000"))
      donnee = donnee.replaceAll(".000000", "");
    else if (donnee.endsWith(".00000"))
      donnee = donnee.replaceAll(".00000", "");
    else if (donnee.endsWith(".0000"))
      donnee = donnee.replaceAll(".0000", "");
    else if (donnee.endsWith(".000"))
      donnee = donnee.replaceAll(".000", "");
    else if (donnee.endsWith(".00"))
      donnee = donnee.replaceAll(".00", "");
    else if (donnee.endsWith(".0"))
      donnee = donnee.replaceAll(".0", "");
    retour = donnee;
    
    return retour;
  }
  
  /**
   * Retourne le r�sultat d'une date incr�ment�e par un nombre de jours pass�s en param�tres
   */
  public static String mettreAjourUneDateSerieM(int dateDepart, int nbJours) {
    String dateString = null;
    int annee = -1;
    int mois = -1;
    int jour = -1;
    
    // FORMATAGE FUCKING MOTHER SHIT DATE SERIEM
    try {
      dateString = Integer.toString(dateDepart);
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    
    // dates de notre si�cle
    if (dateString != null && dateString.length() == 7) {
      // r�cup�ration du jour
      jour = Integer.parseInt(dateString.substring(5, 7));
      // r�cup�ration du mois
      mois = Integer.parseInt(dateString.substring(3, 5)) - 1;
      // r�cup�ration de l'ann�e
      annee = 1900 + Integer.parseInt(dateString.substring(0, 3));
    }
    // Ann�e avant 2000 (le 0 disparait)
    else if (dateString != null && dateString.length() == 6) {
      if (dateString != null && dateString.length() == 6) {
        // r�cup�ration du jour
        jour = Integer.parseInt(dateString.substring(4, 6));
        // r�cup�ration du mois
        mois = Integer.parseInt(dateString.substring(2, 4)) - 1;
        // r�cup�ration de l'ann�e
        annee = 1900 + Integer.parseInt(dateString.substring(0, 2));
      }
    }
    // Si la date rentr�e est pourrave
    else if (dateString == null || dateString.length() < 6)
      return null;
    
    // MISE A JOUR DU CALENDRIER AVEC LE NB DE JOURS A AJOUTER
    if (jour > -1 && mois > -1 && annee > -1) {
      if (monCalendrier == null)
        monCalendrier = Calendar.getInstance();
      
      monCalendrier.clear();
      monCalendrier.set(annee, mois, jour);
      monCalendrier.add(Calendar.DAY_OF_YEAR, nbJours);
      formater = new SimpleDateFormat("dd/MM/yyyy");
      
      dateString = formater.format(monCalendrier.getTime());
    }
    
    return dateString;
  }
  
  /**
   * Arrondir un tarif � la d�cimale souhait�e puis le formater � l'affichage
   */
  public static String formaterUnTarifArrondi(Utilisateur pUtil, Object pTarif) {
    String retour = "";
    if (pTarif == null || pUtil.getETB_EN_COURS() == null) {
      return retour;
    }
    
    BigDecimal tarif;
    try {
      tarif = (BigDecimal) pTarif;
    }
    catch (Exception e1) {
      return "";
    }
    
    try {
      if (pUtil.getETB_EN_COURS().getDecimales_client() > 0) {
        // On arrondi au nombre de d�cimales � mater
        retour = tarif.setScale(pUtil.getETB_EN_COURS().getDecimales_a_voir(), BigDecimal.ROUND_HALF_UP).toString();
      }
      else {
        retour = tarif.setScale(0, BigDecimal.ROUND_HALF_UP).toString();
      }
    }
    catch (Exception e) {
      retour = tarif.toString();
    }
    
    if (retour == null) {
      return null;
    }
    
    // Formater l'affichage de la virgule.
    retour = retour.replace('.', ',');
    
    // afficher une devise HTML
    if (pUtil.getUS_DEV() != null) {
      if (pUtil.getUS_DEV().equals("EUR")) {
        retour += " &euro;";
      }
      else if (pUtil.getUS_DEV().equals("FRA")) {
        retour += " F";
      }
      else if (pUtil.getUS_DEV().equals("XPF")) {
        retour += " F";
      }
    }
    else {
      retour += DEVISE_DEFAUT;
    }
    
    return retour;
  }
  
  // -- M�thodes import�es de S�rie N contenues dans la classe Constantes
  
  /**
   * Permet de normaliser le contenu d'une cha�ne de texte saisie.
   * Remplacer null par une cha�ne vide, enlever les espaces, g�rer les caract�res sp�ciaux.
   */
  public static String normerTexte(String pTexte) {
    if (pTexte == null) {
      return "";
    }
    return pTexte.trim();
  }
  
  /**
   * Formate strictement une chaine de caract�res � la longueur voulue.
   */
  public static String formaterStringStrict(String pString, int pLongueur) {
    if (pString == null) {
      pString = "";
    }
    if (pString.length() > pLongueur) {
      pString = pString.substring(0, pLongueur);
    }
    else {
      pString = String.format("%-" + pLongueur + "s", pString);
    }
    return pString;
  }
  
  /**
   * Retourne la valeur maximum possible d'une zone num�rique (RPG).
   */
  public static long valeurMaxZoneNumerique(int pNombreChiffres) {
    return (long) (Math.pow(10, pNombreChiffres) - 1);
  }
  
}
