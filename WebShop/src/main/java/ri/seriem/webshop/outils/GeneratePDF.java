
package ri.seriem.webshop.outils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;

public class GeneratePDF extends MesLogs {
  private Utilisateur utilisateur = null;
  private Document document = null;
  private PdfWriter writer = null;
  private CSSResolver cssResolver = null;
  HtmlPipelineContext htmlContext = null;
  PdfWriterPipeline pdf = null;
  HtmlPipeline html = null;
  XMLWorker worker = null;
  private String imageDecorationPDF = null;
  private CssFile cssFile = null;
  CssResolverPipeline css = null;
  XMLParser parser = null;
  
  public GeneratePDF(Utilisateur util) {
    utilisateur = util;
    imageDecorationPDF = "http://resolution-informatique.com:8022/webshop/css/image_decoration.jpg";
  }
  
  public boolean makePDF_HTML(String titre, String typePDF, String idPDF, String etb, String message) {
    if (loggerSiTrue("[makePDF()] utilisateur � NULL", utilisateur == null))
      return false;
    
    boolean retour = false;
    String repertoirePDFs =
        ConstantesEnvironnement.DOSSIER_SPECIFIQUE + ConstantesEnvironnement.SEPARATEUR + "pdf" + ConstantesEnvironnement.SEPARATEUR;
    String filePDF = null;
    String contenuPDF = "";
    // S'il s'agit d'un document unique et officiel (commandes, devis, factures ...etc)
    if (idPDF != null && typePDF != null)
      filePDF = typePDF + "_" + etb + "_" + idPDF + ".pdf";
    else
      filePDF = "tmp_" + utilisateur.getSessionEnCours() + ".pdf";
    
    // tests
    repertoirePDFs = "";
    filePDF = "C:\\Users\\ritoudb\\Desktop\\Desktop\\FilePDF.pdf";
    
    // taille de la page
    document = new Document(PageSize.A4);
    document.setMargins(20, 20, 30, 30);
    
    try {
      /*File dirPdf  = new File (MarbreEnvironnement.DOSSIER_SPECIFIQUE+ MarbreEnvironnement.SEPARATEUR  + "pdf"+ MarbreEnvironnement.SEPARATEUR);
      System.out.println(dirPdf);
      if (!dirPdf.exists())
      {
      	dirPdf.mkdir();
      }*/
      
      writer = PdfWriter.getInstance(document, new FileOutputStream(repertoirePDFs + filePDF));
      
      writer.close();
      
      document.open();
      
      // En t�te PDF
      contenuPDF = retournerHeaderHTML(titre);
      
      // Contenu
      contenuPDF += retournerContenuPDF_HTML(message);
      
      // XMLWorkerHelper helper = XMLWorkerHelper.getInstance();
      cssFile = XMLWorkerHelper.getCSS(new FileInputStream("C:\\Users\\ritoudb\\Desktop\\Desktop\\test.css"));
      
      cssResolver = new StyleAttrCSSResolver();
      cssResolver.addCss(cssFile);
      
      // HTML
      htmlContext = new HtmlPipelineContext(null);
      htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
      // Pipelines
      pdf = new PdfWriterPipeline(document, writer);
      html = new HtmlPipeline(htmlContext, pdf);
      css = new CssResolverPipeline(cssResolver, html);
      
      // XML Worker
      worker = new XMLWorker(css, true);
      parser = new XMLParser(worker);
      parser.parse(new ByteArrayInputStream(contenuPDF.getBytes()));
    }
    catch (Exception e) {
      forcerLogErreur(e.getMessage());
    }
    
    if (document != null)
      document.close();
    
    return retour;
  }
  
  /**
   * Retourner l'ent�te du PDF en HTML
   */
  private String retournerHeaderHTML(String titre) {
    StringBuilder sb = new StringBuilder(2000);
    
    sb.append("<table id='presentation'>");
    sb.append("<tr>");
    sb.append("<td class='tdPres' id='nomServeur'>");
    sb.append("Nom du serveur test");
    sb.append("</td>");
    sb.append("<td class='tdPres' id='image_serveur'>");
    // String logoSociete=MarbreEnvironnement.DOSSIER_SPECIFIQUE + "images" +File.separator +"logoSociete.png";
    sb.append("<img id='imageDeco' src='" + imageDecorationPDF + "'/>");
    sb.append("</td>");
    sb.append("<td class='tdPres' id='webShop'>");
    sb.append("<span id='spanWeb'>Web</span><br/><span id='spanShop'>SHOP</span>");
    // sb.append("WebShop");
    sb.append("</td>");
    sb.append("</tr>");
    sb.append("<tr>");
    sb.append("<td id='titrePage' colspan='3'>" + titre + "</td>");
    sb.append("</tr>");
    sb.append("</table>");
    
    return sb.toString();
  }
  
  /**
   * Retourner le contenu HTML d'un PDF
   */
  private String retournerContenuPDF_HTML(String message) {
    StringBuilder sb = new StringBuilder();
    
    if (message != null)
      sb.append(message);
    
    return sb.toString();
  }
  
  /**
   * D�finition d'une cellule
   * @param table
   * @param cellName
   */
  /*private void insertCell(PdfPTable table,ArrayList<Zone> cellName,GenericRecord enregistrement)//)//String fieldName,int align,int colspan, Font font)
  {
  	//Font fontLo_ID = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, new CMYKColor(0, 0, 0,255)); 
  	//Font fontLogin = new Font(Font.TIMES_ROMAN, 10, Font.BOLD, new CMYKColor(157, 250, 18,255)); 
  	cellName = new ArrayList<Zone>();
  	
  	//int colspan=cellName.getColspan();
  	//int align=cellName.getAlign();
  	//D�finition des cellules � afficher dans le pdf
  	//Zone zoneLO_MESS = new Zone(enregistrement.getField("LO_MESS").toString().trim(), Element.ALIGN_CENTER,fontLogin,5);
  	//Zone zoneUS_LOGIN = new Zone(enregistrement.getField("US_LOGIN").toString().trim(), Element.ALIGN_LEFT, fontLogin,2);
  	//Zone zoneLO_TYPE = new Zone(enregistrement.getField("LO_TYPE").toString().trim(), Element.ALIGN_LEFT,fontLogin, 3 );
  	//cellName.add(zoneLO_MESS);
  	//cellName.add(zoneUS_LOGIN);
  	//cellName.add(zoneLO_TYPE);
  	
  	
  	//cr�ation nouvelle cellule 
  	//PdfPCell cellnom = new PdfPCell (new Phrase(cellName.getFieldName().toString()));
  	
  	/*PdfPCell cellMess = new PdfPCell(new Phrase(zoneLO_MESS.getFieldName().toString()));
  	PdfPCell cellLogin = new PdfPCell(new Phrase(zoneUS_LOGIN.getFieldName().toString()));
  	PdfPCell cellType = new PdfPCell(new Phrase(zoneLO_TYPE.getFieldName().toString()));
  	
  	//d�finit l'alignement de la cellule
  	//cellnom.setHorizontalAlignment(cellName.getAlign());
  	
  	cellMess.setHorizontalAlignment(zoneLO_MESS.getAlign());
  	cellLogin.setHorizontalAlignment(zoneUS_LOGIN.getAlign());
  	cellType.setHorizontalAlignment(zoneLO_TYPE.getAlign());
  	
  	//D�finit la taille de la colonne de la cellule
  	//cellnom.setColspan(cellName.getColspan());
  	
  	cellMess.setColspan(zoneLO_MESS.getColspan());
  	cellLogin.setColspan(zoneUS_LOGIN.getColspan());
  	cellType.setColspan(zoneLO_TYPE.getColspan());
  	
  	//D�finit la taille de ligne de la cellule
  	//cellnom.setRowspan(rowspan);
  	
  	/*cellMess.setRowspan(zoneLO_MESS.getRow());
  	cellLogin.setRowspan(zoneUS_LOGIN.getRow());
  	cellType.setRowspan(zoneLO_TYPE.getRow());*/
  
  /*if (cellName.toString().equalsIgnoreCase(""))
   {
  	//cellnom.setMinimumHeight(10f);
  	cellMess.setMinimumHeight(10f);
  	cellLogin.setMinimumHeight(10f);
  	cellType.setMinimumHeight(10f);
   }*/
  
  // ajoute les 3 cellules dans la table
  // table.addCell(cellnom);
  /*table.addCell(cellMess);
   table.addCell(cellLogin);
   table.addCell(cellType);
   table.completeRow();*/
  // }
  // }
}
