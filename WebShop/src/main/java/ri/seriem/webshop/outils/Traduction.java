
package ri.seriem.webshop.outils;

import java.util.ArrayList;
import java.util.HashMap;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;

public class Traduction extends MesLogs {
  private HashMap<String, String> listeEquivalence = null;
  private Utilisateur utilisateur = null;
  
  /**
   * Constructeur Traduction
   */
  public Traduction(Utilisateur util) {
    utilisateur = util;
    listeEquivalence = new HashMap<String, String>();
  }
  
  /**
   * Charger une premi�re fois cette liste puis l'attribuer � l'objet
   */
  public void chargerListeEquivalence(String code) {
    if (utilisateur != null && utilisateur.getAccesDB2() != null && listeEquivalence != null) {
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(
          "SELECT TR_CLE,TR_VALEUR FROM " + ConstantesEnvironnement.BIBLI_WS + ".TRADUCTION WHERE TR_CODE = '" + code + "'",
          this.getClass());
      if (liste != null && liste.size() > 0) {
        for (int i = 0; i < liste.size(); i++) {
          if (liste.get(i).isPresentField("TR_CLE") && liste.get(i).isPresentField("TR_VALEUR"))
            listeEquivalence.put(liste.get(i).getField("TR_CLE").toString(), liste.get(i).getField("TR_VALEUR").toString());
        }
      }
    }
  }
  
  /**
   * Chercher une �quivalence � un mot cl�
   */
  public String traduire(String cle) {
    if (cle == null || listeEquivalence == null)
      return cle;
    
    if (listeEquivalence.containsKey(cle))
      return listeEquivalence.get(cle);
    else
      return cle;
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public HashMap<String, String> getListeEquivalence() {
    return listeEquivalence;
  }
  
  public void setListeEquivalence(HashMap<String, String> listeEquivalence) {
    this.listeEquivalence = listeEquivalence;
  }
  
}
