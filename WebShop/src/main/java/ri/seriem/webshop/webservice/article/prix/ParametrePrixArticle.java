/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.webshop.webservice.article.prix;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.outils.WebShopMessageErreurException;

/**
 * Cette classe d�crit les param�tres d'entr�es pour le service qui retourne la liste des prix nets et publics d'une liste d'articles.
 */
public class ParametrePrixArticle {
  // Constantes
  public static final int LONGUEUR_CODE_ETABLISSEMENT = 3;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 3;
  public static final int NOMBRE_MAX_ARTICLE = 20;
  
  // Variables
  @XmlElement
  private String codeEtablissement;
  @XmlElement
  private Integer numeroClient;
  @XmlElement
  private Integer suffixeClient;
  @XmlElement
  private List<String> listeCodeArticle;
  
  private String messageErreur = "";
  
  /**
   * Contr�le la validit� des param�tres.
   */
  public boolean controlerParametre() {
    try {
      codeEtablissement = controlerEtablissement(codeEtablissement);
      numeroClient = controlerNumero(numeroClient);
      suffixeClient = controlerSuffixe(suffixeClient);
    }
    catch (WebShopMessageErreurException e) {
      messageErreur = e.getMessage();
      return false;
    }
    return true;
  }
  
  // -- M�thodes priv�es
  
  /**
   * Contr�ler la validit� du code �tablissement.
   * Celui-ci doit faire exactement 3 caract�res. Si le code �tablissement fourni d�passe, une erreur est g�n�r�e. Si le code
   * �tablissement fourni est plus petit, il est compl�t� par la droite avec des espaces.
   */
  private static String controlerEtablissement(String pValeur) {
    if (pValeur == null || pValeur.isEmpty()) {
      throw new WebShopMessageErreurException("Le code �tablissement n'est pas renseign�.");
    }
    if (pValeur.length() > LONGUEUR_CODE_ETABLISSEMENT) {
      throw new WebShopMessageErreurException("Le code �tablissement d�passe la taille maximum (3 caract�res).");
    }
    return Outils.formaterStringStrict(pValeur, LONGUEUR_CODE_ETABLISSEMENT).toUpperCase();
  }
  
  /**
   * Contr�ler la validit� du num�ro client.
   * Le num�ro de client doit �tre sup�rieur � z�ro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new WebShopMessageErreurException("Le num�ro de client n'est pas renseign�.");
    }
    if (pValeur <= 0) {
      throw new WebShopMessageErreurException("Le num�ro du client est inf�rieur ou �gal � z�ro.");
    }
    if (pValeur > Outils.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new WebShopMessageErreurException("Le num�ro du client est sup�rieur � la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contr�ler la validit� du suffixe client.
   * Le suffixe client doit �tre sup�rieur ou �gal � z�ro et doit comportr au maximum 3 chiffres (entre 0 et 999).
   */
  private static Integer controlerSuffixe(Integer pValeur) {
    if (pValeur == null) {
      throw new WebShopMessageErreurException("Le num�ro de client n'est pas renseign�.");
    }
    if (pValeur < 0) {
      throw new WebShopMessageErreurException("Le suffixe du client est inf�rieur � z�ro.");
    }
    if (pValeur > Outils.valeurMaxZoneNumerique(LONGUEUR_SUFFIXE)) {
      throw new WebShopMessageErreurException("Le suffixe du client est sup�rieur � la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code �tablissement.
   */
  public String getCodeEtablissement() {
    return codeEtablissement;
  }
  
  /**
   * Retourne le num�ro client.
   */
  public Integer getNumeroClient() {
    return numeroClient;
  }
  
  /**
   * Retourne le suffixe client.
   */
  public Integer getSuffixeClient() {
    return suffixeClient;
  }
  
  /**
   * Retourne la liste des codes articles.
   */
  public List<String> getListeCodeArticle() {
    return listeCodeArticle;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMessageErreur() {
    return messageErreur;
  }
}
