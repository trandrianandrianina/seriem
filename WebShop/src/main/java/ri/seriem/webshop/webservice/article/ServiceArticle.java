/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.webshop.webservice.article;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ri.seriem.libcommun.outils.Base64Coder;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionCatalogue;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.metier.Client;
import ri.seriem.webshop.metier.Etablissement;
import ri.seriem.webshop.outils.Outils;
import ri.seriem.webshop.webservice.article.prix.ListePrixArticle;
import ri.seriem.webshop.webservice.article.prix.ParametrePrixArticle;
import ri.seriem.webshop.webservice.article.prix.PrixArticle;

/**
 * Cette classe regroupe tous les services concernant les articles.
 * Les service sont de type Restful.
 */
@Path("/article")
public class ServiceArticle {
  // Variables
  private MesLogs logs = new MesLogs();
  
  // -- M�thodes publiques
  
  /**
   * Retourne la liste des prix nets HT et publics HT pour une liste de codes articles.
   */
  @POST
  @Path("/listeprixht")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces(MediaType.APPLICATION_JSON)
  public ListePrixArticle getListePrixHT(ParametrePrixArticle pParametrePrixArticle) {
    long t1 = System.currentTimeMillis();
    
    // Cr�ation de la liste � retourner
    ListePrixArticle listePrixArticle = new ListePrixArticle();
    listePrixArticle.setCodeEtablissement(pParametrePrixArticle.getCodeEtablissement());
    listePrixArticle.setNumeroClient(pParametrePrixArticle.getNumeroClient());
    listePrixArticle.setSuffixeClient(pParametrePrixArticle.getSuffixeClient());
    
    // Contr�ler les param�tres
    if (pParametrePrixArticle == null || !pParametrePrixArticle.controlerParametre()) {
      listePrixArticle.setTexteErreur("Les param�tres ne sont pas valides. " + pParametrePrixArticle.getMessageErreur());
      logs.forcerLogErreur("Les param�tres ne sont pas valides.");
      return listePrixArticle;
    }
    
    // S'il n'y a pas de code articles dans les param�tres d'entr�es
    if (pParametrePrixArticle.getListeCodeArticle() == null || pParametrePrixArticle.getListeCodeArticle().isEmpty()) {
      listePrixArticle.setTexteErreur("Aucun code article n'a �t� fourni.");
      logs.forcerLogErreur("Aucun code article n'a �t� fourni.");
      return listePrixArticle;
    }
    // Initialisation de la liste avec la liste des codes articles demand�es (20 max)
    for (String codeArticle : pParametrePrixArticle.getListeCodeArticle()) {
      codeArticle = Outils.normerTexte(codeArticle);
      if (codeArticle.isEmpty()) {
        logs.forcerLogErreur("Un code article vide a �t� trouv�, il est ignor�.");
        continue;
      }
      PrixArticle prixArticle = new PrixArticle(codeArticle);
      listePrixArticle.ajouterPrix(prixArticle);
      
      // Contr�le du nombre d'articles contenu dans la liste
      if (listePrixArticle.getListePrix().size() == ParametrePrixArticle.NOMBRE_MAX_ARTICLE) {
        break;
      }
    }
    
    // R�cup�ration des prix publics et nets pour la liste des codes articles fournis
    Utilisateur utilisateur = connexion();
    utilisateur.majDesInfosAS400();
    Etablissement etablissement = new Etablissement(utilisateur, pParametrePrixArticle.getCodeEtablissement());
    Client client = new Client(utilisateur, etablissement, pParametrePrixArticle.getNumeroClient().toString(),
        pParametrePrixArticle.getSuffixeClient().toString());
    utilisateur.setClient(client);
    GestionCatalogue gestionCatalogue = new GestionCatalogue();
    // Cette version non optimis�e fonctionne plus rapidement chez Coredime de 100� 200 ms en moyenne mais est plus rapide csur SRP_MAINT
    // de 50 ms en moyenne
    gestionCatalogue.recupererListePrixArticle(utilisateur, listePrixArticle);
    // Version "optimis�e" non activ�e car les gains sont faibles voir inexistant chez coredime (cette version n�cessaire le RPG SGVMCCA2
    // pour fonctionner)
    // gestionCatalogue.recupererListePrixArticleOptimisee(utilisateur, listePrixArticle);
    deconnexion(utilisateur);
    
    long t2 = System.currentTimeMillis() - t1;
    logs.forcerLogErreur("--> temps total " + t2);
    
    return listePrixArticle;
  }
  
  // -- M�thodes priv�es
  
  /**
   * Connexion � la base de donn�es.
   */
  private Utilisateur connexion() {
    Utilisateur utilisateur = new Utilisateur();
    if (utilisateur.getAccesDB2() == null) {
      utilisateur.majConnexionDB2(ConstantesEnvironnement.PROFIL_AS_ANONYME,
          Base64Coder.decodeString(Base64Coder.decodeString(ConstantesEnvironnement.MP_AS_ANONYME)));
    }
    return utilisateur;
  }
  
  /**
   * D�connexion de la base de donn�es.
   */
  private void deconnexion(Utilisateur pUtilisateur) {
    if (pUtilisateur == null) {
      return;
    }
    pUtilisateur.deconnecterAS400();
  }
}
