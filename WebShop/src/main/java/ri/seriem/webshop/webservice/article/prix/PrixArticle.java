/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.webshop.webservice.article.prix;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.GestionCatalogue;

/**
 * Cette classe contient le prix net HT et le prix public HT d'un article.
 * Note :
 * Il faut que tous les accesseurs soient cr��s les set et et les get, le contructeur vide doit exister sinon il y aura une erreur ou les
 * donn�es seront incompl�tes.
 */
public class PrixArticle {
  // Variables
  private String codeArticle;
  private BigDecimal prixPublicHT;
  private BigDecimal prixNetHT;
  
  private transient ListePrixArticle listePrixArticle = null;
  private transient String unite = null;
  private transient int precision = 4;
  
  /**
   * Constructeur.
   */
  public PrixArticle() {
    super();
  }
  
  /**
   * Constructeur.
   */
  public PrixArticle(String pCodeArticle) {
    super();
    setCodeArticle(pCodeArticle);
  }
  
  // -- M�thodes publiques
  
  /**
   * Modifie le prix client � partir du prix client brut (r�cup�rer via programme RPG)
   */
  public void modifierPrixClientBrut(BigDecimal pValeur) {
    if (pValeur == null || pValeur.compareTo(BigDecimal.ZERO) == 0) {
      return;
    }
    if (listePrixArticle.getNombreDecimale() == 4
        || (unite != null && unite.toString().endsWith(ConstantesEnvironnement.CODE_QUATRE_DECIMALE))) {
      pValeur = pValeur.divide(GestionCatalogue.DIXMILLE);
    }
    else if (listePrixArticle.getNombreDecimale() == 2) {
      pValeur = pValeur.divide(GestionCatalogue.CENT);
    }
    prixNetHT = pValeur;
    
    // C'est le prix net qui permet de connaitre la pr�cision
    if (prixNetHT != null) {
      precision = prixNetHT.scale();
    }
  }
  
  /**
   * Modifie le prix public.
   * La pr�cision du prix net est appliqu�e au prix public car la m�thode retourneTarifGeneralArticle() retourne le prix avec 4 d�cimales
   */
  public void modifierPrixPublicBrut(BigDecimal pValeur) {
    if (pValeur == null) {
      prixPublicHT = null;
      return;
    }
    prixPublicHT = pValeur.setScale(precision, RoundingMode.HALF_UP);
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code article.
   */
  public String getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Initialise le code article.
   */
  public void setCodeArticle(String codeArticle) {
    this.codeArticle = codeArticle;
  }
  
  /**
   * Retourne le prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Initialise le prix net HT.
   */
  public void setPrixNetHT(BigDecimal prixNetHT) {
    this.prixNetHT = prixNetHT;
  }
  
  /**
   * Retourne le prix public HT.
   */
  public BigDecimal getPrixPublicHT() {
    return prixPublicHT;
  }
  
  /**
   * Initialise le prix public HT.
   */
  public void setPrixPublicHT(BigDecimal prixPublicHT) {
    this.prixPublicHT = prixPublicHT;
  }
  
  public String getUnite() {
    return unite;
  }
  
  public void setUnite(String unite) {
    this.unite = unite;
  }
  
  public void setListePrixArticle(ListePrixArticle listePrixArticle) {
    this.listePrixArticle = listePrixArticle;
  }
  
}
