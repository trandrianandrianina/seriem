/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package ri.seriem.webshop.webservice.article.prix;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import ri.seriem.webshop.outils.Outils;

/**
 * Classe utilis�e par le service "listeprixht" permettant de retourner une liste de prix nets HT et publics HT.
 * Note :
 * Il faut que tous les accesseurs soient cr��s les set et et les get, le contructeur vide doit exister sinon il y aura une erreur ou les
 * donn�es seront incompl�tes.
 */
@XmlRootElement
public class ListePrixArticle {
  // Variables
  private String codeEtablissement;
  private Integer numeroClient;
  private Integer suffixeClient;
  private List<PrixArticle> listePrix;
  private String texteErreur;
  
  private transient int nombreDecimale = 4;
  
  /**
   * Constructeur.
   */
  public ListePrixArticle() {
    super();
  }
  
  // -- M�thodes publiques
  
  /**
   * Ajoute un prix article � la liste.
   */
  public void ajouterPrix(PrixArticle pPrixArticle) {
    if (pPrixArticle == null) {
      return;
    }
    if (listePrix == null) {
      listePrix = new ArrayList<PrixArticle>();
    }
    pPrixArticle.setListePrixArticle(this);
    listePrix.add(pPrixArticle);
  }
  
  /**
   * Retourne le prix article � partir du code article.
   */
  public PrixArticle retournerPrixArticle(String pCodeArticle) {
    if (listePrix == null) {
      return null;
    }
    pCodeArticle = Outils.normerTexte(pCodeArticle);
    for (PrixArticle prixArticle : listePrix) {
      if (prixArticle.getCodeArticle().equals(pCodeArticle)) {
        return prixArticle;
      }
    }
    return null;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code �tablissement.
   */
  public String getCodeEtablissement() {
    return codeEtablissement;
  }
  
  /**
   * Initialise le code �tablissement.
   */
  public void setCodeEtablissement(String codeEtablissement) {
    this.codeEtablissement = codeEtablissement;
  }
  
  /**
   * Retourne le num�ro client.
   */
  public Integer getNumeroClient() {
    return numeroClient;
  }
  
  /**
   * Initialise le num�ro client.
   */
  public void setNumeroClient(Integer numeroClient) {
    this.numeroClient = numeroClient;
  }
  
  /**
   * Retourne le suffixe client.
   */
  public Integer getSuffixeClient() {
    return suffixeClient;
  }
  
  /**
   * Initialise le suffixe client.
   */
  public void setSuffixeClient(Integer suffixeClient) {
    this.suffixeClient = suffixeClient;
  }
  
  /**
   * Retourne la liste des prix articles.
   */
  public List<PrixArticle> getListePrix() {
    return listePrix;
  }
  
  /**
   * Initialise la liste des prix articles.
   */
  public void setListePrix(List<PrixArticle> listePrix) {
    this.listePrix = listePrix;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getTexteErreur() {
    return texteErreur;
  }
  
  /**
   * Initialise le message d'erreur.
   */
  public void setTexteErreur(String messageErreur) {
    this.texteErreur = messageErreur;
  }
  
  public int getNombreDecimale() {
    return nombreDecimale;
  }
  
  public void setNombreDecimale(int nombreDecimale) {
    this.nombreDecimale = nombreDecimale;
  }
  
}
