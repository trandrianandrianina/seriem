
package ri.seriem.webshop.metier;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;

public class Magasin extends MesLogs {
  private String codeMagasin = "";
  private String libelleMagasin = "";
  private String mailDevis = null;
  private String mailCommandes = null;
  private String mailInscriptions = null;
  private Utilisateur utilisateur = null;
  public static final String TOUS_MAGS = "*TOUS*";
  
  /**
   * Constructeur basique -> Sans le libell�
   */
  public Magasin(Utilisateur utilisateur, String code) {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_MAGASINS;
    
    if (loggerSiTrue("utilisateur ou CODE � NULL |" + code + "|", code == null || code.trim().equals("") || utilisateur == null))
      return;
    
    this.utilisateur = utilisateur;
    
    codeMagasin = code;
    
    majInfosMagasin();
    
    loggerMonMagasin();
  }
  
  /**
   * Constructeur lorsqu'on connait d�j� le libell�
   */
  public Magasin(Utilisateur utilisateur, String code, String libelle) {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_MAGASINS;
    
    if (loggerSiTrue("utilisateur ou CODE lib � NULL |" + code + "|", code == null || code.trim().equals("") || utilisateur == null))
      return;
    
    this.utilisateur = utilisateur;
    
    codeMagasin = code;
    
    libelleMagasin = libelle;
    
    loggerMonMagasin();
  }
  
  /**
   * On logge les attributs du magasin
   */
  private void loggerMonMagasin() {
    loggerSiDEBUG("-------------------------");
    loggerSiDEBUG("loggerMonMagasin() codeMagasin: " + codeMagasin);
    loggerSiDEBUG("loggerMonMagasin() libelleMagasin: " + libelleMagasin);
    loggerSiDEBUG("-------------------------");
  }
  
  /**
   * mettre � jour le libell� du magasin � partir de son code
   */
  public void majInfosMagasin() {
    if (codeMagasin == null || codeMagasin.trim().equals(""))
      return;
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2()
            .select("SELECT MG_NAME, MG_MAIL_CD, MG_MAIL_DV, MG_MAIL_IN FROM " + ConstantesEnvironnement.BIBLI_WS
                + ".MAGASINS WHERE MG_COD = '" + codeMagasin + "' AND US_ETB = '" + utilisateur.getETB_EN_COURS().getCodeETB() + "' ",
                this.getClass());
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("MG_NAME")) {
        libelleMagasin = liste.get(0).getField("MG_NAME").toString().trim();
      }
      
      if (liste.get(0).isPresentField("MG_MAIL_DV")) {
        mailDevis = liste.get(0).getField("MG_MAIL_DV").toString().trim();
      }
      
      if (liste.get(0).isPresentField("MG_MAIL_CD")) {
        mailCommandes = liste.get(0).getField("MG_MAIL_CD").toString().trim();
      }
      
      if (liste.get(0).isPresentField("MG_MAIL_IN")) {
        mailInscriptions = liste.get(0).getField("MG_MAIL_IN").toString().trim();
      }
    }
  }
  
  public String getCodeMagasin() {
    return codeMagasin;
  }
  
  public void setCodeMagasin(String code) {
    this.codeMagasin = code;
  }
  
  public String getLibelleMagasin() {
    return libelleMagasin;
  }
  
  public void setLibelleMagasin(String libelleMagasin) {
    this.libelleMagasin = libelleMagasin;
  }
  
  public String getMailDevis() {
    return mailDevis;
  }
  
  public String getMailCommandes() {
    return mailCommandes;
  }
  
  public String getMailInscriptions() {
    return mailInscriptions;
  }
  
}
