
package ri.seriem.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.libas400.system.AlmostQtemp;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;
import ri.seriem.webshop.modeles.InjecteurBdVetF;
import ri.seriem.webshop.outils.Outils;

/**
 * Cette classe repr�sente le panier de l'utilisateur ainsi que tous ses
 * attributs
 */
public class Panier extends MesLogs {
  // Constantes
  public static final int NOMBRE_MAX_LIGNES_COMMENTAIRES = 5;
  public static final int TAILLE_MAX_LIGNE_COMMENTAIRE = PanierArticleCommentaire.TAILLE_MAX_ZONE * PanierArticleCommentaire.NOMBRE_ZONES;
  public static final String CODE_COMMENTAIRE = "$#$#COM%d";
  
  // Variables
  private int ID_DB2 = 0;
  private LinkedHashMap<String, PanierArticle> listeArticles = new LinkedHashMap<String, PanierArticle>();
  private BigDecimal totalHT = BigDecimal.ZERO;
  private BigDecimal totalEcotaxe = BigDecimal.ZERO;
  private Utilisateur utilisateur = null;
  private Magasin magasinPanier = null;
  // Modes de r�cup�ration du panier 1 ENLEVEMENT 2 LIVRAISON 3 DEVIS (Voir MarbreEnvironnement
  private int modeRecup = ConstantesEnvironnement.MODE_NON_CHOISI;
  private String codeVendeur = null;
  // date de cr�ation du panier
  private int datePanier = 0;
  // date saisie par l'utililsateur quant � la date souhait�e
  private int dateRecuperation = 0;
  private String numCommandeSerieM = null;
  private String idDevisAtransformer = null;
  // bloc notes
  private String detailsRecuperation = null;
  // R�f�rence longue du bon de commande
  private String E1RCC = null;
  // Zones de livraison BLOC ADRESSE
  private String CLNOM = null;
  private String CLCPL = null;
  private String CLRUE = null;
  private String CLLOC = null;
  private String CLVIL = null;
  private String CLPOS = null;
  
  private boolean isModifiable = true;
  
  protected String msgErreur = "";
  
  /**
   * Constructeur
   * 
   * @param utilisateur
   */
  public Panier(Utilisateur utilisateur) {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_PANIER;
    
    this.utilisateur = utilisateur;
    // on attribue par d�faut le magasin de l'utilisateur au panier
    if (this.utilisateur != null) {
      // on checke si l'utilisateur a pas un panier en cours qui traine
      if (utilisateur.getAccesDB2() != null) {
        ArrayList<GenericRecord> liste = null;
        
        liste = utilisateur.getAccesDB2().select(
            "SELECT PA_ID, PA_US, PA_DTC, PA_DTS, PA_MOD, PA_NOM, PA_CPL, PA_RUE, PA_LOC, PA_VIL, PA_CPO, PA_INF, PA_RCC, PARIND AS MAG "
                + "FROM " + ConstantesEnvironnement.BIBLI_WS + ".PANIER " + "LEFT JOIN " + ConstantesEnvironnement.BIBLI_CLIENTS
                + ".PGVMPARM ON PA_MAG = PARIND " + "WHERE PA_ETA = '0' AND PARTYP = 'MA' AND PA_US = '" + utilisateur.getUS_ID() + "' "
                + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE,
            this.getClass());
        
        // Si on a d�j� un panier qui trainouille par l�
        if (liste != null && liste.size() == 1) {
          if (isPanierGarni(liste.get(0)))
            regenererUnPanier(liste.get(0));
          else
            videPanier(true);
        }
      }
      
      // En cas de cr�ation de panier pure et dure (sans r�g�n�ration d'un ancien panier)
      if (ID_DB2 == 0) {
        if (this.utilisateur.getClient().getMagasinClient() != null)
          magasinPanier = this.utilisateur.getClient().getMagasinClient();
        else
          magasinPanier = this.utilisateur.getMagasinSiege();
        
        datePanier = Outils.recupererDateCouranteInt();
        detailsRecuperation = "";
        E1RCC = "";
      }
      codeVendeur = ConstantesEnvironnement.CODE_VENDEUR_WS;
    }
  }
  
  /**
   * R�g�nerer le panier de l'utilisateur s'il existe dans DB2 et que son �tat est non valid� = 0
   */
  private int regenererUnPanier(GenericRecord record) {
    if (record == null)
      return 0;
    
    try {
      if (record.isPresentField("PA_ID"))
        ID_DB2 = Integer.parseInt(record.getField("PA_ID").toString().trim());
      if (record.isPresentField("PA_DTC"))
        datePanier = Integer.parseInt(record.getField("PA_DTC").toString().trim());
      if (record.isPresentField("PA_DTS"))
        dateRecuperation = Integer.parseInt(record.getField("PA_DTS").toString().trim());
      if (record.isPresentField("PA_MOD"))
        modeRecup = Integer.parseInt(record.getField("PA_MOD").toString().trim());
      // zones bloc adresses propres � la livraison
      if (modeRecup == ConstantesEnvironnement.MODE_LIVRAISON) {
        if (record.isPresentField("PA_NOM"))
          CLNOM = record.getField("PA_NOM").toString().trim();
        else
          CLNOM = "";
        if (record.isPresentField("PA_CPL"))
          CLCPL = record.getField("PA_CPL").toString().trim();
        else
          CLCPL = "";
        if (record.isPresentField("PA_RUE"))
          CLRUE = record.getField("PA_RUE").toString().trim();
        else
          CLRUE = "";
        if (record.isPresentField("PA_LOC"))
          CLLOC = record.getField("PA_LOC").toString().trim();
        else
          CLLOC = "";
        if (record.isPresentField("PA_VIL"))
          CLVIL = record.getField("PA_VIL").toString().trim();
        else
          CLVIL = "";
        if (record.isPresentField("PA_CPO"))
          CLPOS = record.getField("PA_CPO").toString().trim();
        else
          CLPOS = "";
      }
      
      if (record.isPresentField("PA_INF"))
        detailsRecuperation = record.getField("PA_INF").toString().trim();
      else
        detailsRecuperation = "";
      
      if (record.isPresentField("PA_RCC"))
        E1RCC = record.getField("PA_RCC").toString().trim();
      else
        E1RCC = "";
      
      // rajout de commentaire
      if (record.isPresentField("MAG"))
        magasinPanier = new Magasin(utilisateur, record.getField("MAG").toString().trim());
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    // On liste les articles li�s au panier et on les rajoute au panier
    if (ID_DB2 > 0) {
      ArrayList<GenericRecord> liste = null;
      liste = utilisateur.getAccesDB2().select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + ID_DB2
          + "'" + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
      // si il existe bien des articles dans ce panier
      if (liste != null && liste.size() > 0)
        for (int i = 0; i < liste.size(); i++)
          ajouteArticle(liste.get(i).getField("ART_ETB").toString().trim(), liste.get(i).getField("ART_ID").toString().trim(),
              liste.get(i).getField("ART_REF").toString().trim(), new BigDecimal(liste.get(i).getField("ART_QTE").toString().trim()),
              new BigDecimal(liste.get(i).getField("ART_TAR").toString().trim()), liste.get(i).getField("ART_CDT").toString().trim(),
              liste.get(i).getField("ART_UNI").toString().trim(), null, false);
    }
    
    return ID_DB2;
  }
  
  // -- M�thodes publiques --------------------------------------------------
  /**
   * Validation de la commande (contr�le + injection)
   * 
   * @param berbv
   * @param rapport
   * @return
   */
  // si on utilise d'autres zones du PINJBDVDSE et PINJBDVDSL
  public boolean valideCommande(String typeBon, String berbv, LinkedHashMap<String, String> rapport) {
    if (utilisateur == null) {
      msgErreur += "La classe Utilisateur est � null.\n";
      return false;
    }
    
    // Cr�ation de la "QTEMP" � partir de l'ID utilisateur ici valeur al�atoire 123
    AlmostQtemp qtemp = new AlmostQtemp(utilisateur.getSysteme().getSystem(), utilisateur.getUS_ID());
    qtemp.create();
    
    InjecteurBdVetF sgvmiv =
        new InjecteurBdVetF(utilisateur.getSysteme(), qtemp.getName(), utilisateur.getBibli(), ConstantesEnvironnement.LETTRE_ENV);
    boolean retour = sgvmiv.init();
    if (!retour) {
      msgErreur += "Initialisation de InjecteurBdVetF en �chec.\n";
      return retour;
    }
    
    sgvmiv.setGestionComplementEntete(true);
    // Nettoyage des fichiers physiques PINJBDV*
    retour = sgvmiv.clearAndCreateAllFilesInWrkLib();
    if (!retour) {
      msgErreur += "Nettoyage et/ou cr�ation fichiers pour l'injection en erreur.\n";
      return retour;
    }
    
    // D�but - Am�liorations possibles
    ArrayList<GenericRecord> listeRcdArticles = new ArrayList<GenericRecord>();
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ENTETE BON
    // Cr�ation de la ligne d'ent�te du bon
    GenericRecord enteteBon = new GenericRecord();
    if (berbv == null)
      berbv = "";
    if (typeBon == null)
      typeBon = "ATT"; // Pour un bon mis en attente
      
    // Type le mode d'exp�dition
    String BEMEX = null;
    if (modeRecup == ConstantesEnvironnement.MODE_RETRAIT)
      BEMEX = ConstantesEnvironnement.EXP_RETRAIT;
    else if (modeRecup == ConstantesEnvironnement.MODE_LIVRAISON)
      BEMEX = ConstantesEnvironnement.EXP_LIVRAI;
    else
      BEMEX = ConstantesEnvironnement.EXP_RETRAIT;
    
    String datePanierPINJ = "";
    // attention aux nouvelles et anciennes dates de S�rie M
    if (String.valueOf(datePanier).length() == 7)
      datePanierPINJ = String.valueOf(datePanier).substring(1, 7);
    
    enteteBon.setField("BEETB", utilisateur.getETB_EN_COURS().getCodeETB());
    
    // Si mode livraison ou mono magasin, on passe le magasin si�ge - Si mode retrait, on passe le magasin du panier
    if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON
        || utilisateur.getETB_EN_COURS().is_mono_magasin())
      enteteBon.setField("BEMAG", utilisateur.getMagasinSiege().getCodeMagasin());
    else
      enteteBon.setField("BEMAG", magasinPanier.getCodeMagasin());
    
    enteteBon.setField("BEDAT", datePanierPINJ);
    enteteBon.setField("BERBV", berbv);
    enteteBon.setField("BEERL", "E");
    enteteBon.setField("BENLI", 0);
    enteteBon.setField("BENCLP", utilisateur.getClient().getNumeroClient());
    enteteBon.setField("BEVDE", codeVendeur);
    
    // Adresse de livraison
    if (modeRecup == ConstantesEnvironnement.MODE_LIVRAISON) {
      enteteBon.setField("BENOML", CLNOM.replaceAll("'", "''"));
      enteteBon.setField("BECPLL", CLCPL.replaceAll("'", "''"));
      enteteBon.setField("BERUEL", CLRUE.replaceAll("'", "''"));
      enteteBon.setField("BELOCL", CLLOC.replaceAll("'", "''"));
      
      // On compl�te le code postal par des blanc si null ou inf�rieur � 5 caract�res
      String codPos = null;
      if (CLPOS == null || CLPOS == "")
        codPos = "00000 ";
      else if (CLPOS.length() < 5)
        codPos = String.format("%-6s", CLPOS);
      else if (CLPOS.length() == 5)
        codPos = CLPOS + " ";
      
      enteteBon.setField("BECDPL", codPos);
      enteteBon.setField("BEVILL", CLVIL.replaceAll("'", "''"));
    }
    
    // R�f�rence longue
    if (E1RCC != null && !E1RCC.trim().equals(""))
      enteteBon.setField("BERCC", E1RCC.replaceAll("'", "''"));
    
    // si commande SerieM renseign�, alors on est en modification. On passe l'ancien n� pour annulation dans le SGVMIV
    if (numCommandeSerieM != null) {
      deverouilleCommande(numCommandeSerieM);
      
      if ((numCommandeSerieM.startsWith("E") || numCommandeSerieM.startsWith("D")) && numCommandeSerieM.length() > 1) {
        enteteBon.setField("BETRT", "A");
        enteteBon.setField("BEBON", numCommandeSerieM.substring(1));
      }
    }
    
    enteteBon.setField("BEOPT", typeBon); // DEV pour devis ou " " pour mise en attente (voir QDDSFCH de PINJBDVDSE pour
                                          // plus de contexte)
    
    // Pas de date inf�rieure � la date du jour
    if (modeRecup == ConstantesEnvironnement.MODE_RETRAIT) {
      if (Outils.recupererDateCouranteInt() > dateRecuperation)
        dateRecuperation = Outils.recupererDateCouranteInt();
    }
    else if (modeRecup == ConstantesEnvironnement.MODE_LIVRAISON) {
      if (Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
          utilisateur.getETB_EN_COURS().getDelai_mini_livraison())) > dateRecuperation)
        dateRecuperation = Outils.transformerDateHumaineEnSeriem(
            Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(), utilisateur.getETB_EN_COURS().getDelai_mini_livraison()));
    }
    else {
      if (Outils.recupererDateCouranteInt() > dateRecuperation)
        dateRecuperation = Outils.recupererDateCouranteInt();
    }
    
    // On vire le si�cle pour l'injecteur
    if (String.valueOf(dateRecuperation).length() == 7)
      enteteBon.setField("BEDLS", String.valueOf(dateRecuperation).substring(1, 7));
    
    if (BEMEX != null)
      enteteBon.setField("BEMEX", BEMEX);
    listeRcdArticles.add(enteteBon);
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ COMPLEMENT D'ENT�TE
    if (sgvmiv.isGestionComplementEntete()) {
      // Cr�ation de la ligne d'ent�te du bon compl�mentaire
      GenericRecord enteteComp = new GenericRecord();
      
      enteteComp.setField("BFETB", utilisateur.getETB_EN_COURS().getCodeETB());
      enteteComp.setField("BFMAG", magasinPanier.getCodeMagasin());
      enteteComp.setField("BFDAT", datePanierPINJ);
      enteteComp.setField("BFRBV", berbv);
      enteteComp.setField("BFERL", "F");
      enteteComp.setField("BFNLI", 0);
      
      // Num�ro de contact
      if (utilisateur.getUS_SERIEM() != null)
        enteteComp.setField("BFCNUM", utilisateur.getUS_SERIEM());
      
      listeRcdArticles.add(enteteComp);
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ LIGNES ARTICLES
    // Pr�paration de la clef pour les lignes du futur bon
    HashMap<String, Object> clefLigne = new HashMap<String, Object>();
    clefLigne.put("BLETB", utilisateur.getETB_EN_COURS().getCodeETB());
    
    // Si mode livraison, on passe le magasin si�ge - Si mode retrait, on passe le magasin du panier
    if (utilisateur.getMonPanier().getModeRecup() == ConstantesEnvironnement.MODE_LIVRAISON
        || utilisateur.getETB_EN_COURS().is_mono_magasin())
      clefLigne.put("BLMAGE", utilisateur.getMagasinSiege().getCodeMagasin());
    else
      clefLigne.put("BLMAGE", magasinPanier.getCodeMagasin());
    ;
    
    clefLigne.put("BLDAT", Integer.parseInt(datePanierPINJ));
    clefLigne.put("BLRBV", berbv);
    // Fin - Am�liorations possibles
    
    // On traite les observations: on les transforme en PanierArticleCommentaire que l'on ins�re en t�te du panier
    traitementDetailsRecuperation();
    
    // Pr�paration des genericrecord pour chaque articles diff�rents du panier
    int numligne = 1;
    for (Entry<String, PanierArticle> entry : listeArticles.entrySet()) {
      GenericRecord article = new GenericRecord();
      entry.getValue().addRcd_pinjbdvdsl(article, clefLigne, numligne++);
      listeRcdArticles.add(article);
    }
    
    // On ins�re les enregistrements dans les PINJBDVDSE et PINJBDVDSL
    retour = sgvmiv.insertRecord(listeRcdArticles);
    if (!retour) {
      msgErreur += "Insertion en erreur:\n" + sgvmiv.getMsgError() + '\n';
      return retour;
    }
    
    // On injecte le PINJBDV avec le rapport mis � jour: soit avec des erreurs soit avec les infos du bon cr��
    retour = sgvmiv.injectFile(rapport);
    // On a un souci
    if (!retour)
      msgErreur += "Injection en erreur (lire le rapport pour le d�tail):\n" + sgvmiv.getMsgError() + '\n';
    else
    // Le bon a �t� cr��
    {
      // on change l'�tat dans DB2
      majPanierDB2("PA_ETA", "1");
      // On cleare le panier car tout c'est bien pass�
      videPanier(false);
    }
    // On supprime la biblioth�que temporaire si est pas en mode DEBUG
    if (ConstantesEnvironnement.MODE_DEBUG || ConstantesDebug.DEBUG_PANIER)
      loggerSiDEBUG("[valideCommande()] Je ne supprime pas la qtemp: " + qtemp.getName());
    else
      qtemp.delete();
    // Les fichiers PINJBDVDSE et PINJBDVDSL sont dans des bibli commencant par TMP chercher les TMP* dans WRKLIB
    // A ne faire qu'en toute fin (� la fin de la session utilisateur - �a permet d'�viter de la recr�er/supprimer �
    // chaque injection) A commenter si vous voulez voir le fichier FCHLOG avec le 5250 de la derni�re injection
    return retour;
  }
  
  /**
   * Retourne le nombre d'articles du panier
   * 
   * @return
   */
  public int getNombreArticle() {
    return listeArticles.size();
  }
  
  /**
   * On supprime un article du panier
   * 
   * @param a1art
   * @return
   */
  public boolean supprimeArticle(String a1etb, String a1art) {
    if ((a1art == null) || (a1art.trim().equals("")) || listeArticles.isEmpty())
      return false;
    
    listeArticles.remove(a1art);
    if (supprimeArticleDB2(a1etb, a1art))
      // Si y a plus d'articles virer le panier de DB2
      if (listeArticles.isEmpty())
        viderPanierDB2();
    // On recalcule le total du panier
    calulTotalHT();
    return true;
  }
  
  /**
   * Supprimer un article de la base de DB2
   */
  private boolean supprimeArticleDB2(String a1etb, String a1art) {
    if (numCommandeSerieM != null)
      return true;
    if (a1art == null || utilisateur == null || utilisateur.getAccesDB2() == null)
      return false;
    
    return (utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + ID_DB2
        + "' AND TRIM(ART_ID) = '" + a1art + "' AND ART_ETB = '" + a1etb + "'", this.getClass()) > 0);
  }
  
  /**
   * On ajoute un article au panier en mode reprise de commande
   * 
   * @param a1art
   * @param qte
   * @param tarifHTUnitaire
   * @param recordart
   * @return
   */
  public boolean ajouteArticle(String a1etb, String a1art, int qte, BigDecimal tarifHTUnitaire, String cdt, String unite, String type,
      GenericRecord recordart) {
    if ((a1art == null) || (a1art.trim().equals("")))
      return false;
    
    PanierArticleNormal article = new PanierArticleNormal(utilisateur);
    article.setA1etb(a1etb);
    article.setA1art(a1art);
    article.setUniteArt(unite);
    article.setTypeArticle(type);
    article.setQuantite(qte);
    article.setTarifHTUnitaire(tarifHTUnitaire);
    article.setConditionnM(cdt);
    
    // au cas o� on joue avec les records directement
    if (recordart != null)
      article.setRcd_pgvmartm(recordart);
    
    return ajouteArticle(article, true);
  }
  
  /**
   * On ajoute un article au panier en mode cr�ation
   * @param a1art
   * @param qte
   * @param tarifHTUnitaire
   * @param recordart
   * @return
   */
  public boolean ajouteArticle(String a1etb, String a1art, String refArticle, BigDecimal qte, BigDecimal tarifHTUnitaire, String cdt,
      String unite, GenericRecord recordart, boolean majDB2) {
    if ((a1art == null) || (a1art.trim().equals("")))
      return false;
    
    PanierArticleNormal article = new PanierArticleNormal(utilisateur);
    article.setA1etb(a1etb);
    article.setA1art(a1art);
    article.setRefFournisseur(refArticle);
    article.setUniteArt(unite);
    article.setQuantite(qte);
    article.setTarifHTUnitaire(tarifHTUnitaire);
    article.setConditionnM(cdt);
    
    if (recordart != null)
      article.setRcd_pgvmartm(recordart);
    
    return ajouteArticle(article, majDB2);
  }
  
  /**
   * Modifie la quantit� d'un article (on l'�crase par la nouvelle valeur)
   * 
   * @param a1art
   * @param qte
   * @return
   */
  public boolean modificationArticle(String a1art, int qte) {
    if ((a1art == null) || (a1art.trim().equals("")) || listeArticles.isEmpty())
      return false;
    
    // On modifie la quantit� de l'article
    ((PanierArticleNormal) listeArticles.get(a1art)).setQuantite(qte);
    modificationDB2Article(a1art, qte);
    // On recalcule le total du panier
    calulTotalHT();
    return true;
  }
  
  /**
   * Modifier la quantit� d'un article en DB2
   */
  private boolean modificationDB2Article(String a1art, int qte) {
    if (numCommandeSerieM != null)
      return true;
    if (a1art == null || this.utilisateur == null || utilisateur.getAccesDB2() == null || qte == 0)
      return false;
    
    return (utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN SET ART_QTE = '" + qte
        + "' WHERE ART_PAN = '" + ID_DB2 + "' AND ART_ID = '" + a1art + "' ", this.getClass()) == 1);
  }
  
  /**
   * Modifie la quantit� d'un article (on l'�crase par la nouvelle valeur)
   * 
   * @param a1art
   * @param qte
   * @return
   */
  public boolean modificationArticle(String a1art, BigDecimal qte) {
    if ((a1art == null) || (a1art.trim().equals("")) || listeArticles.isEmpty())
      return false;
    
    // On modifie la quantit� de l'article
    ((PanierArticleNormal) listeArticles.get(a1art)).setQuantite(qte);
    ((PanierArticleNormal) listeArticles.get(a1art)).calculTotalQuantite();
    modificationDB2Article(a1art, Integer.parseInt(qte.toString()));
    // On recalcule le total du panier
    calulTotalHT();
    return true;
  }
  
  /**
   * Vide le panier de tous les articles
   */
  public void videPanier(boolean clearDB2) {
    listeArticles.clear();
    
    if (utilisateur.getClient().getMagasinClient() != null)
      magasinPanier = this.utilisateur.getClient().getMagasinClient();
    else
      magasinPanier = this.utilisateur.getMagasinSiege();
    
    totalHT = BigDecimal.ZERO;
    datePanier = Outils.recupererDateCouranteInt();
    detailsRecuperation = "";
    E1RCC = "";
    modeRecup = ConstantesEnvironnement.MODE_NON_CHOISI;
    CLNOM = null;
    CLCPL = null;
    CLRUE = null;
    CLLOC = null;
    CLVIL = null;
    CLPOS = null;
    // Si en mode reprise de commande SerieM et qu'on veut supprimer panier, alors on d�v�rouille la commande
    if (numCommandeSerieM != null && clearDB2) {
      deverouilleCommande(numCommandeSerieM);
      numCommandeSerieM = null;
    }
    
    // TODO v�rifier siFo pas d�v�rrouiller le fucking devis
    idDevisAtransformer = null;
    
    if (clearDB2) {
      if (viderPanierDB2())
        ID_DB2 = 0;
    }
    else
      ID_DB2 = 0;
  }
  
  /**
   * Vide le panier dans DB2
   */
  private boolean viderPanierDB2() {
    if (numCommandeSerieM != null)
      return true;
    if (this.utilisateur == null || utilisateur.getAccesDB2() == null || ID_DB2 == 0)
      return false;
    
    if (utilisateur.getAccesDB2().requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + ID_DB2 + "'",
        this.getClass()) > -1)
      return (utilisateur.getAccesDB2()
          .requete("DELETE FROM " + ConstantesEnvironnement.BIBLI_WS + ".PANIER WHERE PA_ID = '" + ID_DB2 + "'", this.getClass()) == 1);
    else
      return false;
  }
  
  /**
   * Modifier le magasin de retrait du panier
   */
  public boolean deverouilleCommande(String cde) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    
    if (cde != null && cde.length() == 8 && utilisateur != null) {
      
      typeCde = cde.substring(0, 1);
      numCde = cde.substring(1, 7);
      numSuf = cde.substring(7, 8);
      
      return (utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM SET E1TOP=0 WHERE E1NUM = "
          + numCde + " AND E1SUF=" + numSuf + " AND E1COD='" + typeCde + "'", this.getClass()) > 0);
    }
    else
      return false;
  }
  
  /**
   * Modifier le magasin de retrait du panier
   */
  public boolean modifierLeMagasinRetrait(String codemag) {
    if (codemag == null || codemag.trim().equals(""))
      return false;
    
    if (codemag.equals(Magasin.TOUS_MAGS)) {
      setModeRecup(ConstantesEnvironnement.MODE_NON_CHOISI);
    }
    else {
      setMagasinPanier(new Magasin(utilisateur, codemag));
    }
    return true;
  }
  
  /**
   * Retourne le message d'erreur
   * 
   * @return
   */
  public String getMsgError() {
    // La r�cup�ration du message est � usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  // -- M�thodes priv�es ----------------------------------------------------
  
  /**
   * Calcul le montant total HT du contenu du panier
   */
  private void calulTotalHT() {
    if (listeArticles.isEmpty())
      return;
    // R�init des valeurs
    totalHT = BigDecimal.ZERO;
    totalEcotaxe = BigDecimal.ZERO;
    // On r�attribue les totaux
    for (Entry<String, PanierArticle> entry : listeArticles.entrySet()) {
      if (entry.getValue() instanceof PanierArticleNormal)
        totalHT = totalHT.add(((PanierArticleNormal) entry.getValue()).getTotalPourQuantite());
      
      // MODE ON ADDITIONNE PAS ET ON AFFICHE DANS UNE LIGNE SUPPL //TODO A PARAMETRER
      if (utilisateur.getETB_EN_COURS().isGestion_ecotaxe())
        totalEcotaxe = totalEcotaxe.add(((PanierArticleNormal) entry.getValue()).getTotalEcotaxe());
      // totalHT = totalHT.add(((PanierArticleNormal) entry.getValue()).getQuantite().multiply(((PanierArticleNormal)
      // entry.getValue()).getTarifHTUnitaireCalcul()));
    }
    
    loggerSiDEBUG("[calculTotalHT()] totalHT: " + totalHT);
    loggerSiDEBUG("[calculTotalHT()] totalEcotaxe: " + totalEcotaxe);
  }
  
  /**
   * On ajoute un article au panier
   * 
   * @param nouvelarticle
   * @return
   */
  private boolean ajouteArticle(PanierArticleNormal nouvelarticle, boolean majDB2) {
    if (nouvelarticle == null)
      return false;
    
    // On v�rifie si l'article
    PanierArticleNormal article = (PanierArticleNormal) listeArticles.get(nouvelarticle.getA1art());
    if (article == null) {
      nouvelarticle.calculTotalQuantite();
      if (majDB2)
        ajouterArticleDB2(nouvelarticle);
      listeArticles.put(nouvelarticle.getA1art(), nouvelarticle);
      // On recalcule le total du panier
      calulTotalHT();
    }
    else
      // S'il existe d�j� on ajoute la quantit� (elle )
      modificationArticle(article.getA1art(), article.getQuantite().add(nouvelarticle.getQuantite()));
    
    return true;
  }
  
  /**
   * Ajouter un article au panier dans DB2
   */
  private boolean ajouterArticleDB2(PanierArticleNormal article) {
    if (numCommandeSerieM != null)
      return true;
    if (article == null || this.utilisateur == null || utilisateur.getAccesDB2() == null)
      return false;
    
    if (majPanierDB2(null, null)) {
      return (utilisateur.getAccesDB2().requete(
          "INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN (ART_PAN,ART_ID,ART_REF,ART_ETB,ART_QTE,ART_TAR,ART_CDT,ART_UNI) "
              + "VALUES ('" + ID_DB2 + "','" + article.getA1art() + "','" + article.getRefFournisseur() + "','"
              + utilisateur.getETB_EN_COURS().getCodeETB() + "','" + article.getQuantite().toString() + "','"
              + article.getTarifHTUnitaire().toString() + "','" + article.getConditionnM() + "','" + article.getUniteArt() + "') ",
          this.getClass()) > 0);
    }
    else
      return false;
  }
  
  /**
   * Mettre � jour le panier dans DB2
   */
  public boolean majPanierDB2(String cle, String valeur) {
    if (numCommandeSerieM != null)
      return true;
    
    // le panier est d�j� dans DB2
    if (ID_DB2 > 0) {
      if (valeur == null)
        valeur = "";
      
      if (cle != null && valeur != null)
        return (utilisateur.getAccesDB2().requete("UPDATE " + ConstantesEnvironnement.BIBLI_WS + ".PANIER SET " + cle + " = '"
            + traiterCaracteresSpeciauxSQL(valeur) + "' WHERE PA_ID = '" + ID_DB2 + "'", this.getClass()) > 0);
      else
        return (ID_DB2 > 0);
    }
    // Si il n'est pas dans DB2
    else {
      ArrayList<GenericRecord> liste = null;
      if (utilisateur.getAccesDB2()
          .requete("INSERT INTO " + ConstantesEnvironnement.BIBLI_WS + ".PANIER (PA_US,PA_ETA,PA_DTC,PA_MOD,PA_DTS,PA_MAG,PA_SESS)"
              + " VALUES ('" + utilisateur.getUS_ID() + "','0','" + datePanier + "','" + modeRecup + "','" + dateRecuperation + "','"
              + magasinPanier.getCodeMagasin() + "','" + utilisateur.getSessionEnCours() + "') ", this.getClass()) > 0) {
        liste = utilisateur.getAccesDB2().select("SELECT PA_ID FROM " + ConstantesEnvironnement.BIBLI_WS + ".PANIER WHERE PA_US = '"
            + utilisateur.getUS_ID() + "' AND PA_ETA = '0' AND PA_SESS = '" + utilisateur.getSessionEnCours() + "'", this.getClass());
        if (liste != null && liste.size() == 1) {
          if (liste.get(0).isPresentField("PA_ID")) {
            try {
              ID_DB2 = Integer.parseInt(liste.get(0).getField("PA_ID").toString().trim());
            }
            catch (Exception e) {
            }
          }
        }
      }
    }
    
    return (ID_DB2 > 0);
  }
  
  public String traiterCaracteresSpeciauxSQL(String chaine) {
    if (chaine == null)
      return null;
    
    return chaine.replace("'", "''");
  }
  
  /**
   * On ajoute un article commentaire au panier
   * 
   * @param nouvelarticle
   * @return
   */
  private boolean ajouteArticle(int indice, PanierArticleCommentaire nouvelarticle) {
    if (nouvelarticle == null)
      return false;
    
    // On doit toujours ajouter l'article commentaire en t�te
    LinkedHashMap<String, PanierArticle> sauve = new LinkedHashMap<String, PanierArticle>();
    sauve.putAll(listeArticles);
    listeArticles.clear();
    listeArticles.put(String.format(CODE_COMMENTAIRE, indice), nouvelarticle);
    listeArticles.putAll(sauve);
    sauve.clear();
    
    return true;
  }
  
  /**
   * Traitement du commentaire que l'on va transformer en
   * PanierArticleCommentaire On sait � la base que l'on a max 600 caract�res
   */
  private void traitementDetailsRecuperation() {
    GestionBlocCommentaire blocCommentaire =
        new GestionBlocCommentaire(detailsRecuperation.replaceAll("'", "''"), TAILLE_MAX_LIGNE_COMMENTAIRE, TAILLE_MAX_LIGNE_COMMENTAIRE);
    ArrayList<String> lignes = blocCommentaire.getFormateCommentaire();
    if (lignes == null)
      return;
    
    // On termine en ins�rant les lignes du commentaire dans le panier
    for (int indice = lignes.size(); --indice >= 0;) {
      PanierArticleCommentaire article = new PanierArticleCommentaire();
      ArrayList<String> zones = blocCommentaire.getZones(lignes.get(indice), PanierArticleCommentaire.TAILLE_MAX_ZONE);
      for (int i = 0; i < zones.size(); i++)
        article.setBllibX(zones.get(i), i + 1);
      ajouteArticle(indice, article);
    }
    lignes.clear();
  }
  
  /**
   * v�rifier si le panier est garni. C'est � dire qu'il contient des articles
   */
  public boolean isPanierGarni(GenericRecord record) {
    boolean retour = false;
    if (record == null)
      return retour;
    
    if (record.isPresentField("PA_ID"))
      ID_DB2 = Integer.parseInt(record.getField("PA_ID").toString().trim());
    
    // chercher les articles li�s � ce panier
    if (ID_DB2 > 0) {
      ArrayList<GenericRecord> liste = null;
      liste = utilisateur.getAccesDB2()
          .select("SELECT ART_PAN FROM " + ConstantesEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + ID_DB2 + "'");
      // Si il existe bien des articles dans ce panier
      retour = (liste != null && liste.size() > 0);
    }
    
    return retour;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  /**
   * @return le totalHT
   */
  public BigDecimal getTotalHT() {
    return totalHT;
  }
  
  public LinkedHashMap<String, PanierArticle> getListeArticles() {
    return listeArticles;
  }
  
  public void setListeArticles(LinkedHashMap<String, PanierArticle> listeArticles) {
    this.listeArticles = listeArticles;
  }
  
  public Magasin getMagasinPanier() {
    return magasinPanier;
  }
  
  public void setMagasinPanier(Magasin magasinPanier) {
    this.magasinPanier = magasinPanier;
    // DB2
    if (magasinPanier != null)
      majPanierDB2("PA_MAG", magasinPanier.getCodeMagasin());
  }
  
  public void setTotalHT(BigDecimal totalHT) {
    this.totalHT = totalHT;
  }
  
  public int getModeRecup() {
    return modeRecup;
  }
  
  public void setModeRecup(int modeRecup) {
    this.modeRecup = modeRecup;
    // DB2
    majPanierDB2("PA_MOD", "" + modeRecup);
  }
  
  public String getCodeVendeur() {
    return codeVendeur;
  }
  
  public void setCodeVendeur(String codeVendeur) {
    this.codeVendeur = codeVendeur;
  }
  
  public int getDatePanier() {
    return datePanier;
  }
  
  public void setDatePanier(int datePanier) {
    this.datePanier = datePanier;
  }
  
  public int getDateRecuperation() {
    return dateRecuperation;
  }
  
  public void setDateRecuperation(int dateRecuperation) {
    this.dateRecuperation = dateRecuperation;
    // DB2
    majPanierDB2("PA_DTS", "" + dateRecuperation);
  }
  
  public String getDetailsRecuperation() {
    return detailsRecuperation;
  }
  
  public void setDetailsRecuperation(String detailsRecuperation) {
    this.detailsRecuperation = detailsRecuperation;
    majPanierDB2("PA_INF", detailsRecuperation);
  }
  
  public String getCLNOM() {
    return CLNOM;
  }
  
  public void setCLNOM(String cLNOM) {
    CLNOM = cLNOM;
    majPanierDB2("PA_NOM", cLNOM);
  }
  
  public String getCLCPL() {
    return CLCPL;
  }
  
  public void setCLCPL(String cLCPL) {
    CLCPL = cLCPL;
    majPanierDB2("PA_CPL", cLCPL);
  }
  
  public String getCLRUE() {
    return CLRUE;
  }
  
  public void setCLRUE(String cLRUE) {
    CLRUE = cLRUE;
    majPanierDB2("PA_RUE", cLRUE);
  }
  
  public String getCLLOC() {
    return CLLOC;
  }
  
  public void setCLLOC(String cLLOC) {
    CLLOC = cLLOC;
    majPanierDB2("PA_LOC", cLLOC);
  }
  
  public String getCLVIL() {
    return CLVIL;
  }
  
  public void setCLVIL(String cLVIL) {
    CLVIL = cLVIL;
    majPanierDB2("PA_VIL", cLVIL);
  }
  
  public String getCLPOS() {
    return CLPOS;
  }
  
  public void setCLPOS(String cLPOS) {
    CLPOS = cLPOS;
    majPanierDB2("PA_CPO", cLPOS);
  }
  
  public String getE1RCC() {
    return E1RCC;
  }
  
  public void setE1RCC(String e1rcc) {
    E1RCC = e1rcc;
    majPanierDB2("PA_RCC", e1rcc);
  }
  
  public String getNumCommandeSerieM() {
    return numCommandeSerieM;
  }
  
  public void setNumCommandeSerieM(String numCommandeSerieM) {
    this.numCommandeSerieM = numCommandeSerieM;
  }
  
  public String getIdDevisAtransformer() {
    return idDevisAtransformer;
  }
  
  public void setIdDevisAtransformer(String idDevisAtransformer) {
    this.idDevisAtransformer = idDevisAtransformer;
  }
  
  public int getID_DB2() {
    return ID_DB2;
  }
  
  public void setID_DB2(int iD_DB2) {
    ID_DB2 = iD_DB2;
  }
  
  public BigDecimal getTotalEcotaxe() {
    return totalEcotaxe;
  }
  
  public void setTotalEcotaxe(BigDecimal totalEcotaxe) {
    this.totalEcotaxe = totalEcotaxe;
  }
  
  public boolean isModifiable() {
    return isModifiable;
  }
  
  public void setModifiable(boolean isModifiable) {
    this.isModifiable = isModifiable;
  }
  
}
