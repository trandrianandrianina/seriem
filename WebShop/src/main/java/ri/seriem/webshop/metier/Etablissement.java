
package ri.seriem.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Classe qui liste un ensemble de param�tres propres aux �tablissements S�rie N
 */
public class Etablissement extends MesLogs {
  private String codeETB = null;
  private Utilisateur utilisateur = null;
  private String libelleETB = null;
  // Cet �tablissement est il param�tr� correctement pour fonctionner ?
  private boolean parametresSontDefinis = false;
  
  // Cet �tablissement contient t il un seul magasin ?
  private boolean is_mono_magasin = false;
  
  // limite d'affichage d'une liste sur un �cran
  private int limit_liste = 15;
  // limite de recherche des requetes articles
  private int limit_requete_article = 150;
  // Le serveur autorise-til d'envoyer des mails
  private boolean ok_envois_mails = true;
  // host smtp du serveur mail
  private String mail_smtp_host = "smtp.resolution-informatique.com";
  // port smtp du serveur mail
  private int mail_smtp_port = 587;
  // profil d'envoi du mail
  private String mail_profil = "noreply@resolution-informatique.com";
  // mp du profil d'envoi du mail
  private String mail_mp = "";
  // mail de r�ception contact de l'ETB
  private String mail_contact = "noreply@resolution-informatique.com";
  // mail de r�ception des commandes WS
  private String mail_commandes = "noreply@resolution-informatique.com";
  // mail de r�ception des devis WS
  private String mail_devis = "noreply@resolution-informatique.com";
  // Mail de r�cpetion des inscriptions client
  private String mail_inscription = "noreply@resolution-informatique.com";
  
  // le code du magasin si�ge s'il existe
  private String magasin_siege = null;
  // la zone de r�f�rence affich�e et recherch�e (soit CAREF soit A1ART)
  private String zone_reference_article = "CAREF";
  // Le stock affich� est la somme du magasin si�ge et le magasin s�lectionn�
  private boolean retrait_is_siege_et_mag = false;
  // voir le tarif public dans la fiche article
  private boolean voir_prix_public = false;
  // le fournisseur doit il apparaitre dans la liste
  private boolean fourni_liste_articles = true;
  // doit on voir le stock attendu dans les listes et la fiche article
  private boolean voir_stock_attendu = false;
  // Chemin de stockage des images
  private String chemin_images_articles = ""; // "http://resolution-informatique.com:8022/webshop/images/";
  // Le type de r�f�rence article qu'il faut chercher en tant que nom d'image correspondant
  private String zone_affichage_photos = "CAREF";
  // Quel type d'extensions photo est attendue si c'est le cas
  private String extensions_photos = "JPG";
  // adresse du site public
  private String adresse_site_public = "";
  // adresse site facebook
  private String adresse_site_facebook = "";
  
  // NB de d�cimales S�rie N pour cet ETB
  private int decimales_client = 2;
  // NB de d�cimales � afficher pour cet ETB
  private int decimales_a_voir = 2;
  // Doit on voir le bloc fournisseur dans la fiche article
  private boolean voir_bloc_fournis_fiche = true;
  // Dans le cas d'une gestion multi Magasins doit on voir le stock des autres magasins (liste et fiche)
  private boolean voir_stocks_ts_mags = false;
  // Permet d'afficher le stock total de tous les magasins pa d�faut
  private boolean voir_stock_total = false;
  // voir la famille et la sous famille de l'article dans la fiche
  private boolean voir_familles_fiche = false;
  // RANG DE LA FICHE TECHNIQUE DANS LE SURAN de PSEMURLM -> LE RESTE EST DE LA PHOTO
  private int rang_fiche_technique = 1;
  // On autorise la gestion des articles favoris de l'utilisateur
  private boolean voir_favoris = true;
  // On autorise la gestion de l'historique des articles achet�s par l'utilisateur
  private boolean voir_art_historique = true;
  // On autorise la gestion des articles r�cemment consult�s par l'utilisateur
  private boolean voir_art_consulte = true;
  // La qte maximum d'articles intelligents affich�s (favoris, cosnult�s et achet�s)
  private int qte_max_historique = 150;
  // D�termine si cet article est vendable ou non malgr� son prix � z�ro
  private boolean vend_articles_a_zero = false;
  // Cet �tablissement g�re l'�cotaxe ou non
  private boolean gestion_ecotaxe = true;
  
  // doit on voir la date de r�assort en cas de rupture de stocks ?
  private boolean voir_date_reassort = false;
  // Le r�assort vient il du si�ge
  private boolean is_reassort_siege = false;
  // D�lai minimum de s�curit� rajout�e lors de l'affichage d'une date de r�appro pr�vue (en nb de jours)
  private int delai_jours_securite = 0;
  // D�lai minimum de s�curit� lors d'une demande de livraison dans la zone de saisie de "date souhait�e" (en nb de
  // jours)
  private int delai_mini_livraison = 0;
  // Doit on voir le stock avec sa valeur ou une icone repr�sentative
  private int mode_stocks = ConstantesEnvironnement.STOCK_AV_VALEUR;
  // Affiche t on directement les stocks ou sur demande ? ATTENTION AUX PERFORMANCES
  private boolean visu_auto_des_stocks = false;
  // Doit on filtrer la liste des articles automatiquement sur le stock positif
  private boolean filtre_stock = false;
  
  // Voir les articles de substitution (par d�faut oui)
  private boolean voirSubstitutions = true;
  // Les variables dessous ne sont interpr�t�es que si "voirSubstitutions" est actif
  private boolean voir_subst_rupture = true;
  private boolean voir_subst_aiguill = true;
  private boolean voir_subst_remplac = true;
  private boolean voir_subst_equival = true;
  private boolean voir_subst_variant = true;
  private int limite_liste_variantes = 10;
  
  // 6 taux TVA de la DG. Chaque article a un taux de tva correspondant
  private BigDecimal taux_tva_1 = null;
  private BigDecimal taux_tva_2 = null;
  private BigDecimal taux_tva_3 = null;
  private BigDecimal taux_tva_4 = null;
  private BigDecimal taux_tva_5 = null;
  private BigDecimal taux_tva_6 = null;
  
  /**
   * Constructeur complet Etablissement
   */
  public Etablissement(Utilisateur util, String code, String libelle) {
    utilisateur = util;
    codeETB = code;
    libelleETB = libelle;
    classeQuiLogge = "Etablissement";
    
    // divers param�tres etb
    majParametresDivers();
  }
  
  /**
   * Constructeur Etablissement sans le libell�. Celui est r�cup�r� � l'int�rieur de ce constructeur
   */
  public Etablissement(Utilisateur util, String code) {
    utilisateur = util;
    codeETB = code;
    
    BigDecimal mille = new BigDecimal(1000);
    
    if (utilisateur != null && utilisateur.getAccesDB2() != null && code != null) {
      // on compl�te le libell�
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
          .select("" + " SELECT SUBSTR(PARZ2, 1, 24) AS LIBETB, " + " substr(hex(substring(parz2 , 51, 4)), 1, 5) as TVA1, "
              + " substr(hex(substring(parz2 , 54, 4)), 1, 5) as TVA2, " + " substr(hex(substring(parz2 , 57, 4)), 1, 5) as TVA3, "
              + " substr(hex(substring(parz2 , 60, 4)), 1, 5) as TVA4, " + " substr(hex(substring(parz2 , 63, 4)), 1, 5) as TVA5, "
              + " substr(hex(substring(parz2 , 66, 4)), 1, 5) as TVA6 " + " FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM "
              + " WHERE PARTYP = 'DG' AND PARETB = '" + code + "' ", this.getClass());
      if (liste != null && liste.size() > 0) {
        if (liste.get(0).isPresentField("LIBETB"))
          libelleETB = liste.get(0).getField("LIBETB").toString().trim();
        if (liste.get(0).isPresentField("TVA1")) {
          try {
            taux_tva_1 = new BigDecimal(liste.get(0).getField("TVA1").toString().trim());
            taux_tva_1 = taux_tva_1.divide(mille);
          }
          catch (Exception e) {
            taux_tva_1 = new BigDecimal(0);
          }
        }
        else
          taux_tva_1 = new BigDecimal(0);
        
        if (liste.get(0).isPresentField("TVA2")) {
          try {
            taux_tva_2 = new BigDecimal(liste.get(0).getField("TVA2").toString().trim());
            taux_tva_2 = taux_tva_2.divide(mille);
            
          }
          catch (Exception e) {
            taux_tva_2 = new BigDecimal(0);
          }
        }
        else
          taux_tva_2 = new BigDecimal(0);
        
        if (liste.get(0).isPresentField("TVA3")) {
          try {
            taux_tva_3 = new BigDecimal(liste.get(0).getField("TVA3").toString().trim());
            taux_tva_3 = taux_tva_3.divide(mille);
            
          }
          catch (Exception e) {
            taux_tva_3 = new BigDecimal(0);
          }
        }
        else
          taux_tva_3 = new BigDecimal(0);
        
        if (liste.get(0).isPresentField("TVA4")) {
          try {
            taux_tva_4 = new BigDecimal(liste.get(0).getField("TVA4").toString().trim());
            taux_tva_4 = taux_tva_4.divide(mille);
            
          }
          catch (Exception e) {
            taux_tva_4 = new BigDecimal(0);
          }
        }
        else
          taux_tva_4 = new BigDecimal(0);
        
        if (liste.get(0).isPresentField("TVA5")) {
          try {
            taux_tva_5 = new BigDecimal(liste.get(0).getField("TVA5").toString().trim());
            taux_tva_5 = taux_tva_5.divide(mille);
          }
          catch (Exception e) {
            taux_tva_5 = new BigDecimal(0);
          }
        }
        else
          taux_tva_5 = new BigDecimal(0);
        
        if (liste.get(0).isPresentField("TVA6")) {
          try {
            taux_tva_6 = new BigDecimal(liste.get(0).getField("TVA6").toString().trim());
            taux_tva_6 = taux_tva_6.divide(mille);
          }
          catch (Exception e) {
            taux_tva_6 = new BigDecimal(0);
          }
        }
        else
          taux_tva_6 = new BigDecimal(0);
      }
      
      // divers param�tres etb
      majParametresDivers();
    }
  }
  
  /**
   * m�thode de mise � jour des param�tres d'environnement propres � l'�tablissement
   */
  private void majParametresDivers() {
    if (codeETB == null || utilisateur == null || utilisateur.getAccesDB2() == null)
      return;
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(
        "SELECT EN_CLE,EN_VAL FROM " + ConstantesEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_ETB = '" + codeETB + "' ", this.getClass());
    if (liste != null && liste.size() > 0) {
      parametresSontDefinis = true;
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).getField("EN_CLE").toString().trim().equals("LIMIT_LISTE")) {
          try {
            limit_liste = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("LIMIT_REQUETE_ARTICLE")) {
          try {
            limit_requete_article = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("OK_ENVOIS_MAILS"))
          ok_envois_mails = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_SMTP_HOST"))
          mail_smtp_host = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_SMTP_PORT")) {
          try {
            mail_smtp_port = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("DECIMALES_A_VOIR"))
          decimales_a_voir = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("DECIMALES_CLIENT"))
          decimales_client = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_PROFIL"))
          mail_profil = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_MP"))
          mail_mp = (liste.get(i).getField("EN_VAL").toString().trim());// Base64Coder.encodeString(Base64Coder.encodeString
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_CONTACT"))
          mail_contact = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_COMMANDES"))
          mail_commandes = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_DEVIS"))
          mail_devis = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAIL_INSCRIPTION"))
          mail_inscription = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MAGASIN_SIEGE"))
          magasin_siege = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("ZONE_REFERENCE_ARTICLE"))
          zone_reference_article = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("RETRAIT_IS_SIEGE_ET_MAG"))
          retrait_is_siege_et_mag = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_PRIX_PUBLIC"))
          voir_prix_public = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("FOURNI_LISTE_ARTICLES"))
          fourni_liste_articles = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("CHEMIN_IMAGES_ARTICLES"))
          chemin_images_articles = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("ADRESSE_SITE_PUBLIC"))
          adresse_site_public = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("ADRESSE_SITE_FACEBOOK"))
          adresse_site_facebook = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("ZONE_AFFICHAGE_PHOTOS"))
          zone_affichage_photos = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("EXTENSIONS_PHOTOS"))
          extensions_photos = liste.get(i).getField("EN_VAL").toString().trim();
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_BLOC_FOURNIS_FICHE"))
          voir_bloc_fournis_fiche = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_STOCKS_TS_MAGS"))
          voir_stocks_ts_mags = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_FAMILLES_FICHE"))
          voir_familles_fiche = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VISUSTOCKS"))
          visu_auto_des_stocks = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("RANG_FICHE_TECHNIQUE"))
          try {
            rang_fiche_technique = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_FAVORIS"))
          voir_favoris = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_ART_HISTORIQUE"))
          voir_art_historique = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_ART_CONSULTE"))
          voir_art_consulte = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("QTE_MAX_HISTORIQUE")) {
          try {
            qte_max_historique = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("DELAI_JOURS_SECURITE"))
          try {
            delai_jours_securite = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("DELAI_MINI_LIVRAISON"))
          try {
            delai_mini_livraison = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("LIMITE_VARIANTE")) {
          try {
            limite_liste_variantes = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_DATE_REASSORT"))
          voir_date_reassort = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("IS_REASSORT_SIEGE"))
          is_reassort_siege = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VEND_ARTICLES_A_ZERO"))
          vend_articles_a_zero = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("IS_MONO_MAGASIN"))
          is_mono_magasin = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("GESTION_ECOTAXE"))
          gestion_ecotaxe = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("MODE_STOCKS"))
          try {
            mode_stocks = Integer.parseInt(liste.get(i).getField("EN_VAL").toString().trim());
          }
          catch (NumberFormatException e) {
            forcerLogErreur(e.getMessage());
          }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("AFFICHAGE_ATTENDU"))
          voir_stock_attendu = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_STOCK_TOTAL"))
          voir_stock_total = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_SUBSTITUT"))
          voirSubstitutions = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("FILTRE_STOCK"))
          filtre_stock = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_SUB_RUPT")) {
          voir_subst_rupture = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_SUB_AIGU")) {
          voir_subst_aiguill = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_SUB_REMP")) {
          voir_subst_remplac = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_SUB_EQUI")) {
          voir_subst_equival = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        }
        else if (liste.get(i).getField("EN_CLE").toString().trim().equals("VOIR_SUB_VARIAN")) {
          voir_subst_variant = liste.get(i).getField("EN_VAL").toString().trim().equals("1");
        }
      }
    }
  }
  
  public String toString() {
    return codeETB;
  }
  
  /**
   * Retourner de mani�re statique la liste compl�te des �tablissements actifs
   */
  public static ArrayList<GenericRecord> retournerListeDesEtablissementsValides(Utilisateur utilisateur) {
    if (utilisateur == null)
      return null;
    
    return utilisateur.getAccesDB2().select("SELECT * FROM " + ConstantesEnvironnement.BIBLI_WS + ".ETABLISS WHERE ETB_FM='"
        + ConstantesEnvironnement.BIBLI_CLIENTS + "' AND ETB_ACTIF = '1' ORDER BY ETB_LIB ");
  }
  
  // ++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++
  
  public String getCodeETB() {
    return codeETB;
  }
  
  public void setCodeETB(String codeETB) {
    this.codeETB = codeETB;
  }
  
  public String getLibelleETB() {
    return libelleETB;
  }
  
  public void setLibelleETB(String libelleETB) {
    this.libelleETB = libelleETB;
  }
  
  public boolean isParametresSontDefinis() {
    return parametresSontDefinis;
  }
  
  public void setParametresSontDefinis(boolean parametresSontDefinis) {
    this.parametresSontDefinis = parametresSontDefinis;
  }
  
  public int getLimit_liste() {
    return limit_liste;
  }
  
  public void setLimit_liste(int limit_liste) {
    this.limit_liste = limit_liste;
  }
  
  public int getLimit_requete_article() {
    return limit_requete_article;
  }
  
  public void setLimit_requete_article(int limit_requete_article) {
    this.limit_requete_article = limit_requete_article;
  }
  
  public boolean isOk_envois_mails() {
    return ok_envois_mails;
  }
  
  public void setOk_envois_mails(boolean ok_envois_mails) {
    this.ok_envois_mails = ok_envois_mails;
  }
  
  public String getMail_smtp_host() {
    return mail_smtp_host;
  }
  
  public void setMail_smtp_host(String mail_smtp_host) {
    this.mail_smtp_host = mail_smtp_host;
  }
  
  public int getMail_smtp_port() {
    return mail_smtp_port;
  }
  
  public void setMail_smtp_port(int mail_smtp_port) {
    this.mail_smtp_port = mail_smtp_port;
  }
  
  public String getMail_profil() {
    return mail_profil;
  }
  
  public void setMail_profil(String mail_profil) {
    this.mail_profil = mail_profil;
  }
  
  public String getMail_mp() {
    return mail_mp;
  }
  
  public void setMail_mp(String mail_mp) {
    this.mail_mp = mail_mp;
  }
  
  public String getMail_contact() {
    return mail_contact;
  }
  
  public void setMail_contact(String mail_contact) {
    this.mail_contact = mail_contact;
  }
  
  public String getMail_commandes() {
    return mail_commandes;
  }
  
  public void setMail_commandes(String mail_commandes) {
    this.mail_commandes = mail_commandes;
  }
  
  public String getMail_devis() {
    return mail_devis;
  }
  
  public void setMail_devis(String mail_devis) {
    this.mail_devis = mail_devis;
  }
  
  public String getMagasin_siege() {
    return magasin_siege;
  }
  
  public void setMagasin_siege(String magasin_siege) {
    this.magasin_siege = magasin_siege;
  }
  
  public int getDecimales_client() {
    return decimales_client;
  }
  
  public void setDecimales_client(int decimales_client) {
    this.decimales_client = decimales_client;
  }
  
  public int getDecimales_a_voir() {
    return decimales_a_voir;
  }
  
  public void setDecimales_a_voir(int decimales_a_voir) {
    this.decimales_a_voir = decimales_a_voir;
  }
  
  public String getZone_reference_article() {
    return zone_reference_article;
  }
  
  public void setZone_reference_article(String zone_reference_article) {
    this.zone_reference_article = zone_reference_article;
  }
  
  public boolean isRetrait_is_siege_et_mag() {
    return retrait_is_siege_et_mag;
  }
  
  public void setRetrait_is_siege_et_mag(boolean retrait_is_siege_et_mag) {
    this.retrait_is_siege_et_mag = retrait_is_siege_et_mag;
  }
  
  public boolean isVoir_prix_public() {
    return voir_prix_public;
  }
  
  public void setVoir_prix_public(boolean voir_prix_public) {
    this.voir_prix_public = voir_prix_public;
  }
  
  public boolean isFourni_liste_articles() {
    return fourni_liste_articles;
  }
  
  public void setFourni_liste_articles(boolean fourni_liste_articles) {
    this.fourni_liste_articles = fourni_liste_articles;
  }
  
  public String getChemin_images_articles() {
    return chemin_images_articles;
  }
  
  public void setChemin_images_articles(String chemin_images_articles) {
    this.chemin_images_articles = chemin_images_articles;
  }
  
  public String getZone_affichage_photos() {
    return zone_affichage_photos;
  }
  
  public void setZone_affichage_photos(String zone_affichage_photos) {
    this.zone_affichage_photos = zone_affichage_photos;
  }
  
  public String getExtensions_photos() {
    return extensions_photos;
  }
  
  public void setExtensions_photos(String extensions) {
    extensions_photos = extensions;
  }
  
  public boolean isVoir_bloc_fournis_fiche() {
    return voir_bloc_fournis_fiche;
  }
  
  public void setVoir_bloc_fournis_fiche(boolean voir_bloc_fournis_fiche) {
    this.voir_bloc_fournis_fiche = voir_bloc_fournis_fiche;
  }
  
  public boolean isVoir_stocks_ts_mags() {
    return voir_stocks_ts_mags;
  }
  
  public void setVoir_stocks_ts_mags(boolean voir_stocks_ts_mags) {
    this.voir_stocks_ts_mags = voir_stocks_ts_mags;
  }
  
  public boolean isVoir_familles_fiche() {
    return voir_familles_fiche;
  }
  
  public void setVoir_familles_fiche(boolean voir_familles_fiche) {
    this.voir_familles_fiche = voir_familles_fiche;
  }
  
  public int getRang_fiche_technique() {
    return rang_fiche_technique;
  }
  
  public void setRang_fiche_technique(int rang_fiche_technique) {
    this.rang_fiche_technique = rang_fiche_technique;
  }
  
  public boolean isVoir_favoris() {
    return voir_favoris;
  }
  
  public void setVoir_favoris(boolean voir_favoris) {
    this.voir_favoris = voir_favoris;
  }
  
  public boolean isVoir_art_historique() {
    return voir_art_historique;
  }
  
  public void setVoir_art_historique(boolean voir_art_historique) {
    this.voir_art_historique = voir_art_historique;
  }
  
  public boolean isVoir_art_consulte() {
    return voir_art_consulte;
  }
  
  public void setVoir_art_consulte(boolean voir_art_consulte) {
    this.voir_art_consulte = voir_art_consulte;
  }
  
  public int getQte_max_historique() {
    return qte_max_historique;
  }
  
  public void setQte_max_historique(int qte_max) {
    qte_max_historique = qte_max;
  }
  
  public boolean isVoir_date_reassort() {
    return voir_date_reassort;
  }
  
  public void setVoir_date_reassort(boolean voir_date_reassort) {
    this.voir_date_reassort = voir_date_reassort;
  }
  
  public boolean isIs_reassort_siege() {
    return is_reassort_siege;
  }
  
  public void setIs_reassort_siege(boolean is_reassort_siege) {
    this.is_reassort_siege = is_reassort_siege;
  }
  
  public int getDelai_jours_securite() {
    return delai_jours_securite;
  }
  
  public void setDelai_jours_securite(int delai_jours_securite) {
    this.delai_jours_securite = delai_jours_securite;
  }
  
  public int getDelai_mini_livraison() {
    return delai_mini_livraison;
  }
  
  public void setDelai_mini_livraison(int delai_mini_livraison) {
    this.delai_mini_livraison = delai_mini_livraison;
  }
  
  public boolean isVend_articles_a_zero() {
    return vend_articles_a_zero;
  }
  
  public void setVend_articles_a_zero(boolean vend_articles_a_zero) {
    this.vend_articles_a_zero = vend_articles_a_zero;
  }
  
  public int getMode_stocks() {
    return mode_stocks;
  }
  
  public void setMode_stocks(int mode_stocks) {
    this.mode_stocks = mode_stocks;
  }
  
  public boolean isVisu_auto_des_stocks() {
    return visu_auto_des_stocks;
  }
  
  public void setVisu_auto_des_stocks(boolean visu_auto_des_stocks) {
    this.visu_auto_des_stocks = visu_auto_des_stocks;
  }
  
  public boolean is_mono_magasin() {
    return is_mono_magasin;
  }
  
  public void setIs_mono_magasin(boolean is_mono_magasin) {
    this.is_mono_magasin = is_mono_magasin;
  }
  
  public boolean isGestion_ecotaxe() {
    return gestion_ecotaxe;
  }
  
  public void setGestion_ecotaxe(boolean gestion_ecotaxe) {
    this.gestion_ecotaxe = gestion_ecotaxe;
  }
  
  public String getAdresse_site_public() {
    return adresse_site_public;
  }
  
  public void setAdresse_site_public(String adresse_site_public) {
    this.adresse_site_public = adresse_site_public;
  }
  
  public String getAdresse_site_facebook() {
    return adresse_site_facebook;
  }
  
  public void setAdresse_site_facebook(String adresse_site_facebook) {
    this.adresse_site_facebook = adresse_site_facebook;
  }
  
  public BigDecimal getTaux_tva_1() {
    return taux_tva_1;
  }
  
  public BigDecimal getTaux_tva_2() {
    return taux_tva_2;
  }
  
  public BigDecimal getTaux_tva_3() {
    return taux_tva_3;
  }
  
  public BigDecimal getTaux_tva_4() {
    return taux_tva_4;
  }
  
  public BigDecimal getTaux_tva_5() {
    return taux_tva_5;
  }
  
  public BigDecimal getTaux_tva_6() {
    return taux_tva_6;
  }
  
  public boolean isVoir_stock_attendu() {
    return voir_stock_attendu;
  }
  
  public boolean voir_stock_total() {
    return voir_stock_total;
  }
  
  public boolean voirSubstitutions() {
    return voirSubstitutions;
  }
  
  public boolean is_filtre_stock() {
    return filtre_stock;
  }
  
  public void setFiltre_stock(boolean pFiltre) {
    this.filtre_stock = pFiltre;
  }
  
  public boolean voir_Subst_Rupture() {
    return voir_subst_rupture;
  }
  
  public boolean voir_Subst_Aiguill() {
    return voir_subst_aiguill;
  }
  
  public boolean voir_Subst_Remplac() {
    return voir_subst_remplac;
  }
  
  public boolean voir_Subst_Equival() {
    return voir_subst_equival;
  }
  
  public boolean voir_Subst_Variante() {
    return voir_subst_variant;
  }
  
  public String getMail_inscription() {
    return mail_inscription;
  }
  
  public void setMail_inscription(String mail_inscription) {
    this.mail_inscription = mail_inscription;
  }
  
  public int getLimite_liste_variantes() {
    return limite_liste_variantes;
  }
  
}
