
package ri.seriem.webshop.metier;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesAffichage;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;

public class GestionVariantes {
  
  /**
   * Retourne la liste articles variantes li�s � un article maitre
   */
  public ArrayList<GenericRecord> retournerArticlesVariantes(Utilisateur pUtil, String pEtb, String pArticle, GenericRecord pRecord) {
    if (pUtil == null || pEtb == null || pArticle == null || pRecord == null) {
      return null;
    }
    
    ArrayList<GenericRecord> listeArticles = null;
    
    if (pRecord.isPresentField("A1ASB")) {
      String requete = " SELECT PARZ2 FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMPARM " + " WHERE PARTYP = 'TV' AND PARETB = '"
          + pUtil.getETB_EN_COURS() + "' AND PARIND = '" + pRecord.getField("A1ASB").toString().trim() + "' ";
      
      ArrayList<GenericRecord> liste = pUtil.getAccesDB2().select(requete, this.getClass());
      if (liste != null && liste.size() == 1 && liste.get(0).isPresentField("PARZ2")) {
        Variante variante = new Variante(liste.get(0).getField("PARZ2").toString());
        
        if (variante.isProposerAvente()) {
          listeArticles = recupererLesArticlesVariantes(pUtil, variante, pRecord);
        }
      }
    }
    
    return listeArticles;
  }
  
  /**
   * Retourner la liste des articles associ�s � une variante et ses conditions de recherche et d'affichage
   */
  private ArrayList<GenericRecord> recupererLesArticlesVariantes(Utilisateur pUtil, Variante pVariante, GenericRecord pRecord) {
    if (pVariante == null) {
      return null;
    }
    
    String conditionsVariante = traiterConditionsSQLVariante(pVariante, pRecord);
    
    if (conditionsVariante == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    String requete = "SELECT A1ETB, A1ART, A1LIB, A1CL1, A1CL2, A1ABC, A1FAM, CAREF, FRNOM " + " FROM "
        + ConstantesEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '" + pUtil.getETB_EN_COURS().getCodeETB() + "' "
        + ConstantesAffichage.retournerFiltreSqlStatutArticle(null) + " " + ConstantesAffichage.retournerFiltreSqlVieArticle(null)
        + conditionsVariante + " ORDER BY A1ABC, A1LIB " + " FETCH FIRST " + pUtil.getETB_EN_COURS().getLimite_liste_variantes()
        + " ROWS ONLY " + " OPTIMIZE FOR " + pUtil.getETB_EN_COURS().getLimite_liste_variantes() + " ROWS "
        + ConstantesEnvironnement.CLAUSE_OPTIMIZE;
    
    liste = pUtil.getAccesDB2().select(requete, this.getClass());
    
    return liste;
  }
  
  /**
   * R�cup�rer les param�tres de la variante et en extraire les conditions SQL
   */
  private String traiterConditionsSQLVariante(Variante pVariante, GenericRecord pRecord) {
    if (pVariante == null || pRecord == null) {
      return "";
    }
    
    String retour = null;
    String comparaison = "";
    
    // On ne fait des recherches sur les articles variantes que si on est en mode proposition � la vente
    if (pVariante.isProposerAvente()) {
      // Code article
      if (pVariante.getNbCodeArticle() > 0 && pRecord.isPresentField("A1ART")) {
        comparaison = pRecord.getField("A1ART").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbCodeArticle()) {
            comparaison = comparaison.substring(0, pVariante.getNbCodeArticle());
          }
          
          retour = " AND A1ART LIKE '" + comparaison + "%' ";
        }
      }
      
      // Mot de classement 1
      if (pVariante.getNbMotClass1() > 0 && pRecord.isPresentField("A1CL1")) {
        comparaison = pRecord.getField("A1CL1").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbMotClass1()) {
            comparaison = comparaison.substring(0, pVariante.getNbMotClass1());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND A1CL1 LIKE '" + comparaison + "%' ";
        }
      }
      
      // Mot de classement 2
      if (pVariante.getNbMotClass2() > 0 && pRecord.isPresentField("A1CL2")) {
        comparaison = pRecord.getField("A1CL2").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbMotClass2()) {
            comparaison = comparaison.substring(0, pVariante.getNbMotClass2());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND A1CL2 LIKE '" + comparaison + "%' ";
        }
      }
      
      // Code famille
      if (pVariante.getNbCodeFamille() > 0 && pRecord.isPresentField("A1FAM")) {
        comparaison = pRecord.getField("A1FAM").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbCodeFamille()) {
            comparaison = comparaison.substring(0, pVariante.getNbCodeFamille());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND A1FAM LIKE '" + comparaison + "%' ";
        }
      }
      
      // R�f�rence article
      if (pVariante.getNbRefFourniss() > 0 && pRecord.isPresentField("CAREF")) {
        comparaison = pRecord.getField("CAREF").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbRefFourniss()) {
            comparaison = comparaison.substring(0, pVariante.getNbRefFourniss());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND CAREF LIKE '" + comparaison + "%' ";
        }
      }
      
      if (retour != null) {
        retour += " AND A1ART <> '" + pRecord.getField("A1ART").toString().trim() + "' ";
      }
    }
    
    return retour;
  }
  
}
