
package ri.seriem.webshop.metier;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.controleurs.MesLogs;
import ri.seriem.webshop.environnement.Utilisateur;

/**
 * Client S�rie N
 */
public class Client extends MesLogs {
  private Utilisateur utilisateur = null;
  private Etablissement etablissement = null;
  private String numeroClient = null;
  private String suffixeClient = null;
  private String nomClient = null;
  private Magasin magasinClient = null;
  private String complementClient = null;
  private String rueClient = null;
  private String localiteClient = null;
  private String codePostalClient = null;
  private String villeClient = null;
  // Ce client est-il autoris� � agir sur le Web Shop
  private boolean isAutoriseActions = true;
  
  /**
   * Constructeur de base
   */
  public Client(Utilisateur util, Etablissement etb, String numero, String suffixe) {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_CLIENT;
    
    utilisateur = util;
    etablissement = etb;
    numeroClient = numero;
    suffixeClient = suffixe;
    // Maj du bloc adresse
    if (etb != null && numero != null && suffixe != null)
      majInfosClient(etb, numero, suffixe);
    
    loggerClient();
  }
  
  /**
   * Mise � jour du client de cet utilisateur
   */
  public void majInfosClient(Etablissement etb, String numero, String suffixe) {
    if (utilisateur == null || etb == null || numero == null || suffixe == null)
      return;
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
        .select("SELECT CLNOM,CLCPL,CLRUE,CLLOC,CLCDP1,CLVIL,CLMAG,CLTOP,CLTNS, SUBSTR(PARZ2, 1, 30) AS LIBMAG " + " FROM "
            + utilisateur.getBibli() + ".PGVMCLIM " + " LEFT JOIN " + utilisateur.getBibli() + ".PGVMPARM ON PARTYP='MA' AND PARIND=CLMAG "
            + " WHERE CLETB = '" + etb.getCodeETB() + "' AND CLCLI = '" + numero + "' AND CLLIV = '" + suffixe + "' "
            + ConstantesEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + ConstantesEnvironnement.CLAUSE_OPTIMIZE, this.getClass());
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("CLNOM"))
        nomClient = liste.get(0).getField("CLNOM").toString().trim();
      
      if (liste.get(0).isPresentField("CLCPL"))
        complementClient = liste.get(0).getField("CLCPL").toString().trim();
      
      if (liste.get(0).isPresentField("CLRUE"))
        rueClient = liste.get(0).getField("CLRUE").toString().trim();
      
      if (liste.get(0).isPresentField("CLLOC"))
        localiteClient = liste.get(0).getField("CLLOC").toString().trim();
      
      if (liste.get(0).isPresentField("CLCDP1"))
        codePostalClient = liste.get(0).getField("CLCDP1").toString().trim();
      
      if (liste.get(0).isPresentField("CLVIL"))
        villeClient = liste.get(0).getField("CLVIL").toString().trim();
      
      // Si on est pas mono magasin, on prend le mag du client
      if (liste.get(0).isPresentField("CLMAG") && !utilisateur.getETB_EN_COURS().is_mono_magasin()) {
        if (!liste.get(0).getField("CLMAG").toString().trim().equals("")) {
          loggerSiDEBUG(
              "+++ On met � jour le magasin client avec son magasin S�rie M : " + liste.get(0).getField("CLMAG").toString().trim());
          magasinClient = new Magasin(utilisateur, liste.get(0).getField("CLMAG").toString().trim());
        }
      }
      
      isAutoriseActions = controlerSiClientAutorise(liste.get(0));
      
    }
    
    if (utilisateur.getMagasinSiege() != null && magasinClient == null) {
      magasinClient = utilisateur.getMagasinSiege();
      loggerSiDEBUG("+++ On met � jour le magasin client avec le magasin siege  : " + utilisateur.getMagasinSiege().getCodeMagasin());
    }
  }
  
  /**
   * Contr�ler si ce client est autoris� � produire des actions
   * Ce contr�le s'effectue sur les conditions suivantes
   * Client erron� , annul� ou inexistant : CLTOP > 0
   * Client interdit ou d�sactiv� : CLTNS = 2 ou 9
   */
  private boolean controlerSiClientAutorise(GenericRecord pRecord) {
    if (pRecord == null) {
      return false;
    }
    boolean retour = true;
    int cltop = 0;
    if (pRecord.isPresentField("CLTOP")) {
      try {
        cltop = Integer.parseInt(pRecord.getField("CLTOP").toString().trim());
        // Clients �rron�s, annul�s ou inexistants
        if (cltop > 0) {
          retour = false;
        }
      }
      catch (Exception e) {
        forcerLogErreur(e.getMessage());
        return false;
      }
    }
    else {
      return false;
    }
    
    int cltns = 0;
    if (pRecord.isPresentField("CLTNS")) {
      try {
        cltns = Integer.parseInt(pRecord.getField("CLTNS").toString().trim());
        // Clients �rron�s, annul�s ou inexistants
        if (cltns == 2 || cltns == 9) {
          retour = false;
        }
      }
      catch (Exception e) {
        forcerLogErreur(e.getMessage());
        return false;
      }
    }
    
    return retour;
  }
  
  /**
   * logger les infos clients
   */
  private void loggerClient() {
    loggerSiDEBUG("------------------------------");
    loggerSiDEBUG("[loggerClient()] etb: " + etablissement);
    loggerSiDEBUG("[loggerClient()] numero: " + numeroClient);
    loggerSiDEBUG("[loggerClient()] suffixe: " + suffixeClient);
    loggerSiDEBUG("[loggerClient()] nomClient: " + nomClient);
    // adresse
    loggerSiDEBUG("[loggerClient()] complementClient: " + complementClient);
    loggerSiDEBUG("[loggerClient()] rueClient: " + rueClient);
    loggerSiDEBUG("[loggerClient()] localiteClient: " + localiteClient);
    loggerSiDEBUG("[loggerClient()] codePostalClient: " + codePostalClient);
    loggerSiDEBUG("[loggerClient()] villeClient: " + villeClient);
    if (magasinClient != null) {
      loggerSiDEBUG("[loggerClient()] COD magasinClient: " + magasinClient.getCodeMagasin());
      loggerSiDEBUG("[loggerClient()] lib magasinClient: " + magasinClient.getLibelleMagasin());
    }
    else
      loggerSiDEBUG("[loggerClient()] Pas de magasinClient");
    
    if (utilisateur.getMagasinSiege() != null)
      loggerSiDEBUG("Magasin siege : " + utilisateur.getMagasinSiege().getCodeMagasin());
    else {
      loggerSiDEBUG("PAS de Magasin siege");
    }
    loggerSiDEBUG("------------------------------");
  }
  
  public String getNumeroClient() {
    return numeroClient;
  }
  
  public void setNumeroClient(String numeroClient) {
    this.numeroClient = numeroClient;
  }
  
  public String getSuffixeClient() {
    return suffixeClient;
  }
  
  public void setSuffixeClient(String suffixeClient) {
    this.suffixeClient = suffixeClient;
  }
  
  public String getNomClient() {
    return nomClient;
  }
  
  public void setNomClient(String nomClient) {
    this.nomClient = nomClient;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  public void setEtablissement(Etablissement etablissement) {
    this.etablissement = etablissement;
  }
  
  public Magasin getMagasinClient() {
    return magasinClient;
  }
  
  public void setMagasinClient(Magasin magasinClient) {
    this.magasinClient = magasinClient;
  }
  
  public String getComplementClient() {
    return complementClient;
  }
  
  public void setComplementClient(String complementClient) {
    this.complementClient = complementClient;
  }
  
  public String getRueClient() {
    return rueClient;
  }
  
  public void setRueClient(String rueClient) {
    this.rueClient = rueClient;
  }
  
  public String getLocaliteClient() {
    return localiteClient;
  }
  
  public void setLocaliteClient(String localiteClient) {
    this.localiteClient = localiteClient;
  }
  
  public String getCodePostalClient() {
    return codePostalClient;
  }
  
  public void setCodePostalClient(String codePostalClient) {
    this.codePostalClient = codePostalClient;
  }
  
  public String getVilleClient() {
    return villeClient;
  }
  
  public void setVilleClient(String villeClient) {
    this.villeClient = villeClient;
  }
  
  public boolean isAutoriseActions() {
    return isAutoriseActions;
  }
  
}
