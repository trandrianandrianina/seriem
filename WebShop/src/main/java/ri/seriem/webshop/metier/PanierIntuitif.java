
package ri.seriem.webshop.metier;

import java.util.ArrayList;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.controleurs.backoffice.GestionPanierIntuitifBO;
import ri.seriem.webshop.environnement.Utilisateur;

public class PanierIntuitif implements Cloneable {
  
  private ArrayList<ArticleIntuitif> listeArticles = null;
  private String idPanier = null;
  private String libelle = "";
  // Statuts du panier -1 erreur sur le panier 0 inactif 1 actif
  private int statut = -1;
  private String dateCreation = null;
  private String dateModification = null;
  private String etb = null;
  
  private GestionPanierIntuitifBO gestion = null;
  
  public PanierIntuitif(GenericRecord pRecord) {
    if (pRecord != null) {
      if (pRecord.isPresentField("IN_ID")) {
        idPanier = pRecord.getField("IN_ID").toString();
      }
      
      if (pRecord.isPresentField("IN_ETB")) {
        etb = pRecord.getField("IN_ETB").toString();
      }
      
      if (pRecord.isPresentField("IN_LIBELLE")) {
        libelle = pRecord.getField("IN_LIBELLE").toString();
      }
      
      if (pRecord.isPresentField("IN_STATUT")) {
        try {
          statut = Integer.parseInt(pRecord.getField("IN_STATUT").toString().trim());
        }
        catch (Exception e) {
          statut = -1;
        }
      }
      
      if (pRecord.isPresentField("IN_CREA")) {
        dateCreation = pRecord.getField("IN_CREA").toString().trim();
      }
      
      if (pRecord.isPresentField("IN_MODIF")) {
        dateModification = pRecord.getField("IN_MODIF").toString().trim();
      }
    }
  }
  
  public PanierIntuitif() {
  }
  
  /**
   * On met � jour le statut du panier
   */
  public void majStatut(String pStatut) {
    try {
      statut = Integer.parseInt(pStatut);
    }
    catch (Exception e) {
      statut = -1;
    }
  }
  
  /**
   * On r�cup�re le statut traduit humainement
   */
  public String traduireStatut() {
    if (statut == 0) {
      return "inactif";
    }
    else if (statut == 1) {
      return "actif";
    }
    else
      return String.valueOf(statut);
  }
  
  /**
   * On r�cup�re le statut traduit humainement
   */
  public static String traduireStatut(int pStt) {
    if (pStt == 0) {
      return "inactif";
    }
    else if (pStt == 1) {
      return "actif";
    }
    else
      return String.valueOf(pStt);
  }
  
  /**
   * On r�cup�re le statut traduit humainement
   */
  public static String traduireStatut(String pStt) {
    int stt = -1;
    try {
      stt = Integer.parseInt(pStt);
    }
    catch (Exception e) {
    }
    
    if (stt == 0) {
      return "inactif";
    }
    else if (stt == 1) {
      return "actif";
    }
    else
      return pStt;
  }
  
  public PanierIntuitif clone() {
    PanierIntuitif copie = null;
    try {
      // On r�cup�re l'instance � renvoyer par l'appel de la
      // m�thode super.clone()
      copie = (PanierIntuitif) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
      // Ne devrait jamais arriver car nous impl�mentons
      // l'interface Cloneable
      cnse.printStackTrace(System.err);
    }
    
    // on renvoie le clone
    return copie;
  }
  
  /**
   * On ajoute un article � la liste des articles de ce panier
   */
  public void ajouterUnArticle(Utilisateur pUtil, String pArticle) {
    int ordre = -1;
    if (listeArticles == null) {
      listeArticles = new ArrayList<ArticleIntuitif>();
    }
    
    if (listeArticles != null) {
      ordre = listeArticles.size();
    }
    
    ArticleIntuitif article = new ArticleIntuitif(pArticle);
    
    if (gestion == null) {
      gestion = new GestionPanierIntuitifBO();
    }
    
    gestion.majInfosArticle(pUtil, etb, article);
    
    article.setOrdre(ordre);
    
    listeArticles.add(article);
  }
  
  /**
   * Retirer un article de la liste d'articles du panier sur la base du code article transmis en param�tre
   */
  public boolean retirerUnArticle(String pCode) {
    if (pCode == null) {
      return false;
    }
    
    boolean trouve = false;
    int i = 0;
    
    while (i < listeArticles.size() && !trouve) {
      if (listeArticles.get(i).getCodeArticle() != null && listeArticles.get(i).getCodeArticle().equals(pCode)) {
        trouve = true;
        listeArticles.remove(i);
      }
      i++;
    }
    
    return trouve;
  }
  
  public int getNombreArticle() {
    if (listeArticles == null)
      return 0;
    else
      return listeArticles.size();
  }
  
  public ArrayList<ArticleIntuitif> getListeArticles() {
    return listeArticles;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public int getStatut() {
    return statut;
  }
  
  public void setStatut(int statut) {
    this.statut = statut;
  }
  
  public String getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(String dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public String getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(String dateModification) {
    this.dateModification = dateModification;
  }
  
  public String getIdPanier() {
    return idPanier;
  }
  
  public void setIdPanier(String idPanier) {
    this.idPanier = idPanier;
  }
  
  public String getEtb() {
    return etb;
  }
  
  public void setEtb(String etb) {
    this.etb = etb;
  }
}
