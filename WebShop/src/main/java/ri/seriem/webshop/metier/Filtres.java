
package ri.seriem.webshop.metier;

import ri.seriem.webshop.environnement.Utilisateur;

public class Filtres {
  
  private boolean stock_positif = false;
  private Utilisateur utilisateur = null;
  
  public Filtres(Utilisateur pUtil) {
    utilisateur = pUtil;
  }
  
  public void init() {
    if (utilisateur == null || utilisateur.getETB_EN_COURS() == null) {
      return;
    }
    
    stock_positif = utilisateur.getETB_EN_COURS().is_filtre_stock();
  }
  
  public boolean isStock_positif() {
    return stock_positif;
  }
  
  public void setStock_positif(boolean stock_positif) {
    this.stock_positif = stock_positif;
  }
}
