
package ri.seriem.webshop.metier;

import java.util.HashMap;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.controleurs.MesLogs;

public abstract class PanierArticle extends MesLogs {
  // Variables
  protected String a1art = null;
  protected String a1etb = null;
  
  public abstract void addRcd_pinjbdvdsl(GenericRecord pinjbdvdsl, HashMap<String, Object> entete, int numligne);
  
  // -- Accesseurs ----------------------------------------------------------
  /**
   * @return le a1art
   */
  public String getA1art() {
    return a1art;
  }
  
  /**
   * @param a1art le a1art � d�finir
   */
  public void setA1art(String a1art) {
    this.a1art = a1art;
  }
  
  public String getA1etb() {
    return a1etb;
  }
  
  public void setA1etb(String a1etb) {
    this.a1etb = a1etb;
  }
  
}
