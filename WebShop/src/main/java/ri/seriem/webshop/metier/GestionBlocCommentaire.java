
package ri.seriem.webshop.metier;

import java.util.ArrayList;

public class GestionBlocCommentaire {
  // Constantes
  public static final String CRLF = "\n";
  
  // Variables
  private int nbrlignesmax = 0;
  private int taillemaxligne = 0;
  
  private String commentaire = null;
  private ArrayList<String> lignes = new ArrayList<String>();
  
  /**
   * Constructeur
   * @param _nbrlignesmax
   * @param _taillemaxligne
   * @param _taillemaxzones
   */
  public GestionBlocCommentaire(String commentaire, int _nbrlignesmax, int _taillemaxligne) {
    setCommentaire(commentaire);
    nbrlignesmax = _nbrlignesmax;
    taillemaxligne = _taillemaxligne;
  }
  
  /**
   * 
   * @return
   */
  public ArrayList<String> getFormateCommentaire() {
    if ((commentaire == null) || commentaire.equals(""))
      return null;
    
    if (!analyseCommentaire()) {
      // Si �a passe pas, on traite de mani�re bourrine : on concat�ne tout et on d�coupe � la taille max de chaque
      // ligne
      StringBuilder sb = new StringBuilder();
      for (String ligne : lignes)
        sb.append(ligne).append(' ');
      sb.deleteCharAt(sb.length() - 1); // On supprime le dernier espace ajouter par la boucle
      lignes.clear();
      lignes = getZones(sb.toString(), taillemaxligne);
    }
    
    return lignes;
  }
  
  /**
   * D�coupe la chaine en X chaines de longueur donn�e
   * @param ligne
   * @param longueurZone
   * @return
   */
  public ArrayList<String> getZones(String chaine, int longueurZone) {
    int deb = 0;
    int fin = longueurZone;
    ArrayList<String> liste = new ArrayList<String>();
    do {
      if (fin < chaine.length()) {
        liste.add(chaine.substring(deb, fin));
        deb = fin;
        fin += longueurZone;
      }
      else {
        liste.add(chaine.substring(deb));
        deb += longueurZone;
      }
    }
    while (deb < chaine.length());
    
    return liste;
  }
  
  // -- M�thodes priv�es ----------------------------------------------------
  
  /**
   * Analyse le bloc commentaire
   */
  private boolean analyseCommentaire() {
    boolean capasse = true;
    String[] lignes_origines = commentaire.split(CRLF);
    
    // On s'occupe d'�liminer les lignes vides si le nombre de lignes est sup�rieur � NOMBRE_MAX_LIGNES_COMMENTAIRES
    // et on contr�le la longueur des lignes
    if (lignes_origines.length <= nbrlignesmax) {
      for (String ligne : lignes_origines) {
        lignes.add(ligne.trim());
        if (ligne.trim().length() > taillemaxligne)
          capasse = false;
      }
    }
    else {
      // On �limine autant que possible les lignes vides
      for (String ligne : lignes_origines)
        if (!ligne.trim().equals("")) {
          lignes.add(ligne.trim());
          if (ligne.trim().length() > taillemaxligne)
            capasse = false;
        }
      // On contr�le le nombre de lignes apr�s le nettoyage
      if (lignes.size() > nbrlignesmax)
        capasse = false;
    }
    return capasse;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }
  
  /**
   * @param commentaire le commentaire � d�finir
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }
}
