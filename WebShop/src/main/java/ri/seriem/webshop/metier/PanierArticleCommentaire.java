
package ri.seriem.webshop.metier;

import java.util.HashMap;
import java.util.Map.Entry;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;

public class PanierArticleCommentaire extends PanierArticle {
  // Constantes
  public static final int TAILLE_MAX_ZONE = 30;
  public static final int NOMBRE_ZONES = 4;
  
  // Variables
  // private String BlocTexte = null;
  private String bllib1 = null;
  private String bllib2 = null;
  private String bllib3 = null;
  private String bllib4 = null;
  
  // -- M�thodes publiques --------------------------------------------------
  
  public PanierArticleCommentaire() {
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_ARTICLES || ConstantesDebug.DEBUG_PANIER;
  }
  
  /**
   * Ajoute les champs de l'article pour la future injection
   * @param pinjbdvdsl
   */
  public void addRcd_pinjbdvdsl(GenericRecord pinjbdvdsl, HashMap<String, Object> entete, int numligne) {
    if (pinjbdvdsl == null)
      return;
    
    // On �crit les champs pour cette ligne article (notamment la clef)
    for (Entry<String, Object> entry : entete.entrySet())
      if (entry.getKey().startsWith("BL")) // On s'assure que c'est un champ pour la ligne
        pinjbdvdsl.setField(entry.getKey(), entry.getValue());
      
    // On �crit les infos g�n�riques
    pinjbdvdsl.setField("BLNLI", numligne);
    pinjbdvdsl.setField("BLERL", "L");
    
    // Et le reste des informations propre � l'article
    // Ajouter ici les champs associ�s aux variables que vous avez rajout� � la classe
    if ((bllib1 != null) && !bllib1.trim().equals(""))
      pinjbdvdsl.setField("BLLIB1", bllib1);
    if ((bllib2 != null) && !bllib2.trim().equals(""))
      pinjbdvdsl.setField("BLLIB2", bllib2);
    if ((bllib3 != null) && !bllib3.trim().equals(""))
      pinjbdvdsl.setField("BLLIB3", bllib3);
    if ((bllib4 != null) && !bllib4.trim().equals(""))
      pinjbdvdsl.setField("BLLIB4", bllib4);
    
    // On supprime le code article (au cas o�) car un article commentaire n'en a pas
    if (pinjbdvdsl.isPresentField("BLART"))
      pinjbdvdsl.removeField("BLART");
    
    loggerSiDEBUG("-addRcd_pinjbdvdsl() ->" + getBllib1() + " / " + getBllib2() + " / " + getBllib2());
  }
  
  /**
   * Initialise le champ � partir de son num�ro
   * @param lib
   * @param indice
   */
  public void setBllibX(String lib, int indice) {
    switch (indice) {
      case 1:
        setBllib1(lib);
        break;
      case 2:
        setBllib2(lib);
        break;
      case 3:
        setBllib3(lib);
        break;
      case 4:
        setBllib4(lib);
        break;
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le bllib1
   */
  public String getBllib1() {
    return bllib1;
  }
  
  /**
   * @param bllib1 le bllib1 � d�finir
   */
  public void setBllib1(String bllib1) {
    this.bllib1 = bllib1;
  }
  
  /**
   * @return le bllib2
   */
  public String getBllib2() {
    return bllib2;
  }
  
  /**
   * @param bllib2 le bllib2 � d�finir
   */
  public void setBllib2(String bllib2) {
    this.bllib2 = bllib2;
  }
  
  /**
   * @return le bllib3
   */
  public String getBllib3() {
    return bllib3;
  }
  
  /**
   * @param bllib3 le bllib3 � d�finir
   */
  public void setBllib3(String bllib3) {
    this.bllib3 = bllib3;
  }
  
  /**
   * @return le bllib4
   */
  public String getBllib4() {
    return bllib4;
  }
  
  /**
   * @param bllib4 le bllib4 � d�finir
   */
  public void setBllib4(String bllib4) {
    this.bllib4 = bllib4;
  }
  
}
