
package ri.seriem.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import ri.seriem.libas400.database.record.GenericRecord;
import ri.seriem.webshop.constantes.ConstantesDebug;
import ri.seriem.webshop.constantes.ConstantesEnvironnement;
import ri.seriem.webshop.environnement.Utilisateur;

public class PanierArticleNormal extends PanierArticle {
  // Variables
  private BigDecimal quantite = BigDecimal.ZERO;
  private BigDecimal tarifHTUnitaire = BigDecimal.ZERO;
  private BigDecimal tarifHTUnitaireCalcul = BigDecimal.ZERO;
  private BigDecimal totalPourQuantite = BigDecimal.ZERO;
  private BigDecimal totalEcotaxe = BigDecimal.ZERO;
  private String typeArticle = "C";
  private String libelleArt = null;
  private String uniteArt = null;
  private Float stockDispo = null;
  private int conditionnM = 1;
  private String refFournisseur = null;
  private Utilisateur utilisateur = null;
  private ArrayList<GenericRecord> listeTravailArticle = null;
  
  private GenericRecord rcd_pgvmartm = null;
  
  // -- M�thodes publiques --------------------------------------------------
  
  /**
   * Constructeur de PanierArticleNormal
   */
  public PanierArticleNormal(Utilisateur util) {
    utilisateur = util;
    IS_EN_DEBUG_SPEC = ConstantesDebug.DEBUG_ARTICLES || ConstantesDebug.DEBUG_PANIER;
    // listeTravailArticle = new ArrayList<GenericRecord>();
  }
  
  /**
   * Ajoute les champs de l'article pour la future injection
   * 
   * @param pinjbdvdsl
   */
  public void addRcd_pinjbdvdsl(GenericRecord pinjbdvdsl, HashMap<String, Object> entete, int numligne) {
    if (pinjbdvdsl == null || utilisateur.getETB_EN_COURS() == null)
      return;
    
    // On �crit les champs pour cette ligne article (notamment la clef)
    for (Entry<String, Object> entry : entete.entrySet())
      if (entry.getKey().startsWith("BL")) // On s'assure que c'est un
        // champ pour la ligne
        pinjbdvdsl.setField(entry.getKey(), entry.getValue());
      
    // On �crit les infos g�n�riques
    pinjbdvdsl.setField("BLNLI", numligne);
    pinjbdvdsl.setField("BLERL", "L");
    
    // Et le reste des informations propre � l'article
    pinjbdvdsl.setField("BLART", a1art);
    
    // Transformation du prix avec d�ciamle pour mise � jour PINJBDV
    String tarifBrut = tarifHTUnitaire.toString();
    if (tarifBrut.length() > utilisateur.getETB_EN_COURS().getDecimales_client())
      tarifBrut = tarifBrut.substring(0, tarifBrut.length() - utilisateur.getETB_EN_COURS().getDecimales_client()) + "."
          + tarifBrut.substring(tarifBrut.length() - utilisateur.getETB_EN_COURS().getDecimales_client());
    else if (tarifBrut.length() == utilisateur.getETB_EN_COURS().getDecimales_client())
      tarifBrut = "0." + tarifBrut;
    else
      tarifBrut = tarifBrut + ".00";
    loggerSiDEBUG("addRcd_pinjbdvdsl() tarifBrut AVANT PINJ: " + tarifBrut);
    
    pinjbdvdsl.setField("BLPRX", tarifHTUnitaire);
    
    pinjbdvdsl.setField("BLQTE", quantite);
    // Ajouter ici les champs associ�s aux variables que vous avez rajout� �
    // la classe
    
  }
  
  /**
   * On calcule le montant de l'article pour la quantit� choisie
   */
  public void calculTotalQuantite() {
    // gestion de l'ecotaxe
    majEcotaxe();
    
    // MODE ON ADDITIONNE L'ECOTAXE DANS LA LIGNE
    totalPourQuantite = tarifHTUnitaireCalcul.multiply(quantite).add(totalEcotaxe);
    loggerSiDEBUG("calculTotalQuantite() totalPourQuantite: " + totalPourQuantite.toString() + " -> tarifHTUnitaireCalcul: "
        + tarifHTUnitaireCalcul.toString() + " -> quantite: " + quantite.toString() + " -> ecotaxe: " + totalEcotaxe.toString());
  }
  
  /**
   * On calcule l'ecotaxe de cet article afin de le rajouter � la ligne
   */
  private void majEcotaxe() {
    if (utilisateur == null || utilisateur.getETB_EN_COURS() == null)
      return;
    
    if (utilisateur.getETB_EN_COURS().isGestion_ecotaxe()) {
      if (quantite != null && a1art != null && a1etb != null) {
        // System.out.println("majEcotaxe() quantite = " + quantite + " -> a1art = " + a1art + " -> a1etb = " + a1etb);
        listeTravailArticle = utilisateur.getAccesDB2()
            .select(" SELECT ATP01,TENBR FROM " + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMTEEM " + " LEFT JOIN "
                + ConstantesEnvironnement.BIBLI_CLIENTS + ".PGVMTARM ON ATETB = TEETB AND ATART = TEARTT " + " WHERE TEETB = '" + a1etb.trim()
                + "' AND TEARTD ='" + a1art.trim() + "' ORDER BY ATDAP DESC FETCH FIRST 1 ROWS ONLY");
        
        if (listeTravailArticle != null && listeTravailArticle.size() > 0) {
          if (listeTravailArticle.get(0).isPresentField("ATP01") && listeTravailArticle.get(0).isPresentField("TENBR"))
            totalEcotaxe = quantite.multiply(((BigDecimal) listeTravailArticle.get(0).getField("TENBR"))
                .multiply((BigDecimal) listeTravailArticle.get(0).getField("ATP01")));
          
          loggerSiDEBUG("[majEcotaxe()] totalEcotaxe: " + totalEcotaxe);
        }
        else
          loggerSiDEBUG("[majEcotaxe()] Pas d'ecotaxe pour cet article !!");
      }
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le quantite
   */
  public BigDecimal getQuantite() {
    return quantite;
  }
  
  /**
   * @param quantite
   *          le quantite � d�finir
   */
  public void setQuantite(int quantite) {
    if (quantite >= 0)
      this.quantite = new BigDecimal(quantite);
  }
  
  /**
   * @param quantite
   *          le quantite � d�finir
   */
  public void setQuantite(BigDecimal quantite) {
    if (quantite.compareTo(BigDecimal.ZERO) >= 0)
      this.quantite = quantite;
  }
  
  /**
   * @return le tarifHTUnitaire
   */
  public BigDecimal getTarifHTUnitaire() {
    return tarifHTUnitaire;
  }
  
  /**
   * @param tarifHTUnitaire
   *          le tarifHTUnitaire � d�finir
   */
  public void setTarifHTUnitaire(BigDecimal tarif) {
    // Si ce n'est pas un article � rajouter au panier
    if (typeArticle == null || !typeArticle.equals("C")) {
      return;
    }
    
    this.tarifHTUnitaireCalcul = tarif;
    
    this.tarifHTUnitaire = tarif;
  }
  
  /**
   * @return le rcd_pgvmartm
   */
  public GenericRecord getRcd_pgvmartm() {
    return rcd_pgvmartm;
  }
  
  /**
   * @param rcd_pgvmartm
   *          le rcd_pgvmartm � d�finir
   */
  public void setRcd_pgvmartm(GenericRecord rcd_pgvmartm) {
    this.rcd_pgvmartm = rcd_pgvmartm;
  }
  
  public String getLibelleArt() {
    return libelleArt;
  }
  
  public void setLibelleArt(String libelleArt) {
    this.libelleArt = libelleArt;
  }
  
  public String getUniteArt() {
    return uniteArt;
  }
  
  public void setUniteArt(String uniteArt) {
    this.uniteArt = uniteArt;
  }
  
  public Float getStockDispo() {
    return stockDispo;
  }
  
  public void setStockDispo(Float stockDispo) {
    this.stockDispo = stockDispo;
  }
  
  public int getConditionnM() {
    return conditionnM;
  }
  
  public void setConditionnM(String conditionnM) {
    try {
      double nbBrut = Double.parseDouble(conditionnM);
      this.conditionnM = (int) nbBrut;
    }
    catch (Exception e) {
      System.out.println("Probl�me de conditionnement: " + conditionnM);
    }
  }
  
  public String getRefFournisseur() {
    return refFournisseur;
  }
  
  public void setRefFournisseur(String refFournisseur) {
    this.refFournisseur = refFournisseur;
  }
  
  public BigDecimal getTarifHTUnitaireCalcul() {
    return tarifHTUnitaireCalcul;
  }
  
  public void setTarifHTUnitaireCalcul(BigDecimal tarifHTUnitaireCalcul) {
    this.tarifHTUnitaireCalcul = tarifHTUnitaireCalcul;
  }
  
  public BigDecimal getTotalPourQuantite() {
    return totalPourQuantite;
  }
  
  public void setTotalPourQuantite(BigDecimal totalPourQuantite) {
    this.totalPourQuantite = totalPourQuantite;
  }
  
  public String getTypeArticle() {
    return typeArticle;
  }
  
  public void setTypeArticle(String typeArticle) {
    this.typeArticle = typeArticle;
  }
  
  public void setConditionnM(int conditionnM) {
    this.conditionnM = conditionnM;
  }
  
  public BigDecimal getTotalEcotaxe() {
    return totalEcotaxe;
  }
  
  public void setTotalEcotaxe(BigDecimal totalEcotaxe) {
    this.totalEcotaxe = totalEcotaxe;
  }
  
}
