
package ri.seriem.webshop.constantes;

public class ConstantesDebug {
  public static boolean MODE_LOG4J = true;
  public static boolean DEBUG_CONTEXT = true;
  public static boolean DEBUG_SQL = false;
  
  // SPECIFIQUES
  // Voir toute les classes du BACK OFFICE
  public static boolean DEBUG_BACK_OFFICE = false;
  // VOIR tous les logs propres � la connexion d'un utilisateur
  public static boolean DEBUG_CONNEXIONS = false;
  // voir tout ce qui touche au panier
  public static boolean DEBUG_PANIER = false;
  // voir tout ce qui touche � la classe PARTENAIRES
  public static boolean DEBUG_PARTENAIRES = false;
  // Voir tout ce qui touche � la classe m�tier CLIENT
  public static boolean DEBUG_CLIENT = false;
  // voir tout ce qui touche aux articles (tarifs/stocks)
  public static boolean DEBUG_ARTICLES = false;
  // voir tout ce qui touche � l'historique du client
  public static boolean DEBUG_MES_DONNEES = false;
  // voir tout ce qui touche � la classe m�tier MAGASIN
  public static boolean DEBUG_MAGASINS = false;
  
}
