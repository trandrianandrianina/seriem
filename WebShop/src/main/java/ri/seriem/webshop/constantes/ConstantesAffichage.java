
package ri.seriem.webshop.constantes;

public class ConstantesAffichage {
  
  public static String AFFICHE_CODE_A = null;
  public static String AFFICHE_CODE_B = null;
  public static String AFFICHE_CODE_C = null;
  public static String AFFICHE_CODE_D = null;
  public static String AFFICHE_CODE_R = null;
  public static String AFFICHE_CODE_N = null;
  public static String AFFICHE_PAS_CODE = null;
  // On affiche ou non les articles qui n'ont jamais �t� vendus ni achet�s
  // Si 0 on affiche tout si 1 on affiche que les articles qui ont eu une
  // action au moins une fois (A1SAI = 1)
  public static String AFFICHE_SANS_STK = null;
  public static String AFFICHAGE_STATUT_DESACTIVE = null;
  public static String AFFICHAGE_STATUT_EPUISE = null;
  public static String AFFICHAGE_STATUT_SYSV = null;
  public static String AFFICHAGE_STATUT_SYSV2 = null;
  public static String AFFICHAGE_STATUT_PREFIN = null;
  public static String AFFICHAGE_STATUT_FIN = null;
  
  // CONSTANTES METIER
  private final static String STATUT_ACTIF = "' '";
  private final static String STATUT_DESACTIVE = "'1'";
  private final static String STATUT_EPUISE = "'2'";
  private final static String STATUT_SYSV = "'3'";
  private final static String STATUT_SYSV2 = "'4'";
  private final static String STATUT_PREFIN = "'5'";
  private final static String STATUT_FIN = "'6'";
  
  private final static String VIE_ARTICLE_A = "'A'";
  private final static String VIE_ARTICLE_B = "'B'";
  private final static String VIE_ARTICLE_C = "'C'";
  private final static String VIE_ARTICLE_D = "'D'";
  private final static String VIE_ARTICLE_R = "'R'";
  private final static String VIE_ARTICLE_N = "'N'";
  private final static String PAS_VIE_ARTICLE = "''";
  
  public final static String REF_CODE_ARTICLE = "A1ART";
  public final static String REF_REF_FOURNISS = "CAREF";
  
  public static boolean OK_ARTICLES_HORS_GAMME = false;
  
  /**
   * Retourne la chaine SQL qui filtre les valeurs autoris�es du statut de
   * l'article
   */
  public static String retournerFiltreSqlStatutArticle(String prefixe) {
    StringBuilder chaineSQL = null;
    String zone = "A1NPU";
    
    if (prefixe != null && !prefixe.trim().equals("")) {
      prefixe = prefixe + ".";
      zone = prefixe + zone;
    }
    
    if (AFFICHAGE_STATUT_DESACTIVE.equals("1")) {
      chaineSQL = new StringBuilder(" AND " + zone + " IN (" + STATUT_ACTIF + "," + STATUT_DESACTIVE);
    }
    if (AFFICHAGE_STATUT_EPUISE.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + STATUT_ACTIF + "," + STATUT_EPUISE);
      else
        chaineSQL.append("," + STATUT_EPUISE);
    }
    if (AFFICHAGE_STATUT_SYSV.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + STATUT_ACTIF + "," + STATUT_SYSV);
      else
        chaineSQL.append("," + STATUT_SYSV);
    }
    if (AFFICHAGE_STATUT_SYSV2.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + STATUT_ACTIF + "," + STATUT_SYSV2);
      else
        chaineSQL.append("," + STATUT_SYSV2);
    }
    if (AFFICHAGE_STATUT_PREFIN.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + STATUT_ACTIF + "," + STATUT_PREFIN);
      else
        chaineSQL.append("," + STATUT_PREFIN);
    }
    if (AFFICHAGE_STATUT_FIN.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + STATUT_ACTIF + "," + STATUT_FIN);
      else
        chaineSQL.append("," + STATUT_FIN);
    }
    
    if (chaineSQL == null) {
      chaineSQL = new StringBuilder(" AND " + zone + " = " + STATUT_ACTIF + " ");
    }
    else {
      chaineSQL.append(") ");
    }
    
    return chaineSQL.toString();
  }
  
  /**
   * Retourne la chaine SQL qui filtre les valeurs autoris�es de la vie de
   * l'article
   */
  public static String retournerFiltreSqlVieArticle(String prefixe) {
    StringBuilder chaineSQL = null;
    String zone = "A1ABC";
    
    if (prefixe != null && !prefixe.trim().equals("")) {
      prefixe = prefixe + ".";
      zone = prefixe + zone;
    }
    
    // s�lection selon les options d'affichages (MarbreAffichage)
    if (ConstantesAffichage.AFFICHE_CODE_A.equals("1")) {
      chaineSQL = new StringBuilder(" AND " + zone + " IN (" + VIE_ARTICLE_A);
    }
    if (ConstantesAffichage.AFFICHE_CODE_B.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + VIE_ARTICLE_B);
      else
        chaineSQL.append("," + VIE_ARTICLE_B);
    }
    if (ConstantesAffichage.AFFICHE_CODE_C.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + VIE_ARTICLE_C);
      else
        chaineSQL.append("," + VIE_ARTICLE_C);
    }
    if (ConstantesAffichage.AFFICHE_CODE_D.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + VIE_ARTICLE_D);
      else
        chaineSQL.append("," + VIE_ARTICLE_D);
    }
    if (ConstantesAffichage.AFFICHE_CODE_R.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + VIE_ARTICLE_R);
      else
        chaineSQL.append("," + VIE_ARTICLE_R);
    }
    if (ConstantesAffichage.AFFICHE_CODE_N.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + VIE_ARTICLE_N);
      else
        chaineSQL.append("," + VIE_ARTICLE_N);
    }
    if (ConstantesAffichage.AFFICHE_PAS_CODE.equals("1")) {
      if (chaineSQL == null)
        chaineSQL = new StringBuilder(" AND " + zone + " IN (" + PAS_VIE_ARTICLE);
      else
        chaineSQL.append("," + PAS_VIE_ARTICLE);
    }
    
    if (chaineSQL == null) {
      chaineSQL = new StringBuilder(" ");
    }
    else {
      chaineSQL.append(") ");
    }
    
    // Affichage des articles n'ayant jamais tourn�s
    if (ConstantesAffichage.AFFICHE_SANS_STK.trim().equals("0")) {
      chaineSQL.append(" AND A1SAI = 1 ");
    }
    
    return chaineSQL.toString();
  }
}
