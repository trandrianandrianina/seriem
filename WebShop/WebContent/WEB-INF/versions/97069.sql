INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$validerDevis','Demander un devis');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$validerDevis','Request quote');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$annulerDevis','Annuler la demande de devis');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$annulerDevis','Cancel quote request');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$validerCommande','Commander');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$validerCommande','Order');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$annulerCommande','Annuler la commande');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$annulerCommande','Cancel order');;;
