INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('MAIL_COMMANDES','d.livet@resolution-informatique.com', 'DONNEES','1');;;

INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('MAIL_DEVIS','d.livet@resolution-informatique.com', 'DONNEES','1');;;

INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('GestionConnexion.java','Mise en place de la connexion pour les profils multiples: un m�me contact (adresse mail) peut appartenir � plusieurs clients. Une interface sp�cifique monte alors � l''�cran.','1150309','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('PatternErgonomie.java','Mise en place du libell� du client dans l''interface globale. Afin qu''au premier coup d''oeil, on puisse rep�rer quel client est connect� mais aussi le type de profil de l''utilisateur: administrateur, gestionnaire...etc','1150309','0','0','0','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('MesDonnees.java','Rajout d''une zone dans le d�tail de la commande de la page "Mes donn�es":  Si on est en mode "retrait en magasin" le magasin concern� apparait.','1150309','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('MonPanier.java','Ajout d''une adresse d''envoi de commandes et de devis dont les destinataires sont des agents de la soci�t� fournisseur du Web Shop. La copie des commandes et des devis pass�s par les clients arrivent directement sur ces adresses respectives','1150309','0','1','2','1','');;;

INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('Catalogue.java','Rajout des images sur la fiche article. Positionnement sur le NAS et appel de l''URL par le code rubis ou r�f�rence fournisseur si n�cessaire.','1150313','0','1','2','1','');;;
