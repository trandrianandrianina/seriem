/*========================================================================*/
/* Table : PATCHSW    				*/
/*========================================================================*/

CREATE table ##CURLIB##.PATCHSW            
(
	PA_ID integer GENERATED ALWAYS AS IDENTITY, 
	PA_CLAS varchar (255),                   
	PA_DETA CLOB(32000) allocate(0),         
	PA_DATE integer,                             
	PA_ETAT integer,                             
	PA_VISI integer,                             
	PA_NOM varchar (255),
	
	
	PRIMARY KEY(PA_ID)
);;;

