CREATE table ##CURLIB##.ACCESBO               
(
	AB_ID INTEGER ,
 	AB_ACC CHARACTER (2),
	
 	PRIMARY KEY(AB_ID,AB_ACC)
);;;           

INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des acc�s du BO','accesBO','40','50');;;


ALTER table  ##CURLIB##.ACCUEILBO ADD AB_ID integer;;;

CREATE table ##CURLIB##.LANGUES
(
   LA_ID CHARACTER(2),
   LA_LIB CHARACTER(100) not null,
   		
   PRIMARY KEY(LA_ID)	
);;;

INSERT INTO ##CURLIB##.LANGUES (LA_ID, LA_LIB) VALUES ('fr','Fran�ais');;;
INSERT INTO ##CURLIB##.LANGUES (LA_ID, LA_LIB) VALUES ('en','Anglais');;;

ALTER TABLE ##CURLIB##.INFOSW ADD INF_LAN CHARACTER(2);;;
UPDATE ##CURLIB##.INFOSW SET INF_LAN = 'fr';;;

ALTER TABLE ##CURLIB##.CGV ADD CGV_LAN CHARACTER(2);;;
UPDATE ##CURLIB##.CGV SET CGV_LAN = 'fr';;;

CREATE table ##CURLIB##.LIENSMAG
(
	LI_ID integer GENERATED ALWAYS AS IDENTITY, 
	LI_MAG integer not null, 
	LI_LAN character (2) not null, 
	LI_MESS varchar (1000) not null,
	LI_HOR varchar (1000) not null,
	LI_EQUI varchar (1000)not null,
	
	primary key (LI_ID)
);;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$horaires','Horaires');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$horaires','Hours');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$localisation','Localisation');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$localisation','Localization');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$monCompte','Votre compte');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$monCompte','Your account');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$contact','Contact');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$contact','Contact');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$adresse','Adresse');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$adresse','Adress');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$vosCommandes','Vos commandes');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$vosCommandes','Your orders');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$vosDevis','Vos devis');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$vosDevis','Your estimate');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$numero','Num�ro');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$numero','Number');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$date','Date');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$date','Date');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$montant','Montant H.T.');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$montant','Amount');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$etat','Etat');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$etat','State');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$type','Type');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$type','Type');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$ville','Ville');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$ville','Town');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$page','Page');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$page','Page');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$complement','Compl�ment');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$complement','Complement');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$ficheArticle','Fiche article');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$ficheArticle','Item sheet');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$rechercheArticles','Recherche articles');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$rechercheArticles','Items search');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$fournisseur','Fournisseur');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$fournisseur','Supplier');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$article','article');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$article','item');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$plafond','Plus de ');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$plafond','More than ');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$famille','Famille');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$famille','Family');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$sousFamille','Sous-famille');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$sousFamille','Subfamily');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$groupe','Groupe');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$groupe','Group');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$texteFiltre','Choisir un nouveau filtre');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$texteFiltre','Choose a new filter');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$filtreActif','Filtres actifs');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$filtreActif','Active filters');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$autreFamille','Autres familles');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$autreFamille','Other families');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$dispo','Dispo.');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$dispo','Avail.');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$reference','R�f�rence');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$reference','Reference');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$libelle','Libell�');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$libelle','Description');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$votreTarif','Votre tarif');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$votreTarif','Your price');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$unite','Unit�');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$unite','Unit');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$traitementCours','Traitement en cours');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$traitementCours','Data processing');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$retraitMagasin','Retrait en magasin');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$retraitMagasin','Store pick up');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$livraison','Livraison');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$livraison','Delivery');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$simpleDevis','Simple devis');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$simpleDevis','Simple estimate');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$disponibilite','Disponibilit�');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$disponibilite','Availability');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$retour','Retour');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$retour','Back');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$detailsArticle','D�tails article');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$detailsArticle','Item details');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$tarifPublic','Tarif public');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$tarifPublic','Public price');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$conditionnement','Conditionnement');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$conditionnement','Packaging');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$photo','Photo');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$photo','Photo');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$panier','Panier');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$panier','Cart');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$monPanier','Mon panier');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$monPanier','My cart');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$fiche','Fiche');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$fiche','Sheet');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$choisirModeRecuperation','Choisissez votre mode de r�cup�ration pour afficher la disponibilit� de vos articles.');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$choisirModeRecuperation','Please, choose your delivery option for display the availability of your items.');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$modeRecuperation','Mode de r�cup�ration de vos articles');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$modeRecuperation','Delivery options of your items');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$motsCles','Mots cl�s');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$motsCles','Keywords');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$promos','Promotions');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','$$promos','Special offers');;;
