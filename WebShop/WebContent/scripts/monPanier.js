//methode permettant d'afficher le calendrier
 $(function openCalendarRetrait () {
	
 $("#idDateRetrait").datepicker($.datepicker.regional["fr"]);
	
});

/*permet de g�rer les boutons d'incr�ments de quantit� d'articles*/
function majQuantiteZone(idZone, incr) {
	var init = document.getElementById(idZone).value;
	var total = 0;
	var increment = Number(incr);

	if (init == null || isNaN(init)) {
		init = 0;
		if (increment == -1)
			total = 0;
		else
			total = 1;
	} else
		total = Number(init) + Number(increment);

	if (total < 0)
		total = 0;

	document.getElementById(idZone).value = total;
}

/* Contr�ler que la saisie */
function only_numeric(e) 
{
	var key = window.event ? e.keyCode : e.which;

	// autoriser les touches de suppr et de retour
	if (key != 0 && key != 8) {
		var valeur = String.fromCharCode(key);
		var regex = new RegExp("^[0-9]*$", "g");
		// sinon ne pas autoriser ce qui n'est pas num�rique
		if (!regex.test(valeur)) {
			e.returnValue = false;
			if (e.preventDefault)
				e.preventDefault();
		}
	}
}

/* Cette m�thode permet d'ajouter dynamiquement un article au panier */
function modifQuantite(id, zoneSaisie,cnd) 
{
	//alert("cnd:" + cnd);
	majCnd(zoneSaisie,cnd);
	var quantite = document.getElementById(zoneSaisie).value.trim();
	
	if (quantite == null || quantite == "" || quantite == "0")
		quantite = "1";

	var url = "monPanier?panierArticle=" + id + "&panierQuantite=" + quantite + "#ancreArticles";
	
	document.location.href = url;
}

/* permet de modifier la quantit� selon le conditionnement */
function majCnd(idZone, cnd) 
{
	if(cnd=='0') cnd = '1';
	var init = document.getElementById(idZone).value;
	var total = 0;
	var conditionnement = Number(cnd);
	var rst = 0 ;
	var quot = 0 ;
	
	total = Number(init) ;
	
	if (conditionnement != 1) 
	{
		if (init < Number(cnd)) 
		{
			total = Number(cnd);
		}
		
		rst = total % Number(cnd) ;
		if (rst !=0)
		{	
			quot =  total /Number(cnd);
			total =  (parseInt(quot)+1) * Number(cnd) ;
		}	
	}

	if (total < 0) total = 0;

	document.getElementById(idZone).value = total;
}

/*
 * Mise � jour des infos bloc adresse / livraison du panier 
 * */
function majInfosPanier(nomZone) 
{
	var valeurModif = document.getElementById(nomZone).value.trim();

	var url = "monPanier?modificationBlocAdresse=1&" + nomZone + "=" + valeurModif;

	if (window.XMLHttpRequest) 
	{
		requete = new XMLHttpRequest();
		if (requete) 
		{
			requete.open("POST", url, true);
			requete.send();
		}
	} 
	else if (window.ActiveXObject) {
		requete = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete) 
		{
			requete.open("POST", url, true);
			requete.send();
		} else
			alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} else {
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}


}
