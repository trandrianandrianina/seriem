

// On v�rifie le profil saisi (client existant obligatoirement) afin de pouvoir l'autoriser � changer son mot de passe
function verifierProfilSaisi(zoneProfil)
{
	var zone = document.getElementById(zoneProfil).value.trim();
	//regexp des adresses mails
	var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    

	if(zone==null || zone=="" || zone.length < 3 || !regex.test(zone))
		alert(" Merci de saisir un profil valide dans [Mon adresse mail] ");
	else
	{
		//var url = "connexion?verifProfil=" + zone;
		var url = "connexion";
		
		if(confirm("Souhaitez vous vraiment changer votre mot de passe ??"))
			post_en_url(url, "verifProfil", zone);
	}
}

