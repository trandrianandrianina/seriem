opaciteGlob = 170;
timer = null;
conditionnementEnCours = 1;

/* Permet de mettre � jour les hauteurs de div en fonction de la plus grande */
function majTailleNavig() {
	if (document.getElementById("listeArticles") != null
			&& document.getElementById("navigationGroupe") != null) {
		// On remet au minimum les tailles de DIV
		if (document.getElementById("listeArticles") != null)
			document.getElementById("listeArticles").style.height = "auto";
		if (document.getElementById("navigationGroupe") != null)
			document.getElementById("navigationGroupe").style.height = "auto";
		
		// Si le navigateur interprete ce mode
		// if(document.getElementById("listeArticles").offsetHeight)
		if (document.getElementById("contenu").offsetHeight) {
			// maj du plus petit
			if (document.getElementById("listeArticles").offsetHeight > document
					.getElementById("navigationGroupe").offsetHeight)
				document.getElementById("navigationGroupe").style.height = (document
						.getElementById("listeArticles").offsetHeight - 30)
						+ "px";
			else if (document.getElementById("listeArticles").offsetHeight < document
					.getElementById("navigationGroupe").offsetHeight)
				document.getElementById("listeArticles").style.height = (document
						.getElementById("navigationGroupe").offsetHeight - 70)
						+ "px";
		}
		// Ou celui-ci
		else if (document.getElementById("listeArticles").style.pixelHeight) {
			// maj du plus petit
			if (document.getElementById("listeArticles").style.pixelHeight > document
					.getElementById("navigationGroupe").style.pixelHeight)
				document.getElementById("navigationGroupe").style.height = (document
						.getElementById("listeArticles").style.pixelHeight - 30)
						+ "px";
			else if (document.getElementById("listeArticles").style.pixelHeight < document
					.getElementById("navigationGroupe").style.pixelHeight)
				document.getElementById("listeArticles").style.height = (document
						.getElementById("navigationGroupe").offsetHeight - 70)
						+ "px";
		}
	}
	//Pour la fiche article il faut faire gaffe à la hauteur de tous les composants
	else if(document.getElementById("detailArticle") != null)
	{
		document.getElementById("detailArticle").style.height = "auto";
		
		//Si on est plus en mode absolu
		if(document.getElementById("blocDroit").offsetTop > 200)
		{
			return;
		}
		
		var htGauche = 0;
		var htDroite = 0;
		
		//Navigateurs standards
		if (document.getElementById("contenu").offsetHeight) 
		{
			
			if(document.getElementById("messagePrincipal")!=null)
			{
				htGauche += document.getElementById("messagePrincipal").offsetHeight + 40 ;
				htDroite += document.getElementById("messagePrincipal").offsetHeight + 40 ;
			}
			
			htGauche += document.getElementById("fi_details").offsetHeight;
			htDroite += document.getElementById("fi_photo").offsetHeight;
			
			if(document.getElementById("fi_fourniss")!=null)
			{
				htGauche += document.getElementById("fi_fourniss").offsetHeight;
			}
			
			if(document.getElementById("fi_stock")!=null)
			{
				htGauche += document.getElementById("fi_stock").offsetHeight;
			}
			
			htDroite += document.getElementById("fi_actions").offsetHeight;
		}
		//Autres navigateurs
		else if (document.getElementById("contenu").style.pixelHeight)
		{
			if(document.getElementById("messagePrincipal")!=null)
			{
				htGauche += document.getElementById("messagePrincipal").style.pixelHeight + 40 ;
				htDroite += document.getElementById("messagePrincipal").style.pixelHeight + 40 ;
			}
			htGauche += document.getElementById("fi_details").style.pixelHeight;
			htDroite += document.getElementById("fi_photo").style.pixelHeight;
			
			if(document.getElementById("fi_fourniss")!=null)
			{
				htGauche += document.getElementById("fi_fourniss").style.pixelHeight;
			}
			
			if(document.getElementById("fi_stock")!=null)
			{
				htGauche += document.getElementById("fi_stock").style.pixelHeight;
			}
			
			htDroite += document.getElementById("fi_actions").style.pixelHeight;
		}
		
		//on retaille comme des sauvages
		if(htDroite>htGauche)
		{
			htDroite+= 90;
			document.getElementById("blocFiche").style.height = htDroite + "px";
			
			if(document.getElementById("fi_variantes")!=null)
			{
				htDroite += document.getElementById("fi_variantes").offsetHeight + 50;
			}
			
			document.getElementById("detailArticle").style.height = htDroite + "px";
		}
		else
		{
			htGauche+= 110;
			document.getElementById("blocFiche").style.height = htGauche + "px";
			
			if(document.getElementById("fi_variantes")!=null)
			{
				htGauche += document.getElementById("fi_variantes").offsetHeight + 50;
			}
			
			document.getElementById("detailArticle").style.height = htGauche + "px";
		}
	}
}

/* ouvrir les bonnes sous familles en fonction de la famille s�lectionn�e */
function ouvrirSousFamilles(idFamille, sousFamille, maj) {
	liste = document.getElementsByClassName("classeSousFamille");
	var idElement = "";
	// le lien sous famille
	for ( var i = 0; i < liste.length; i++) {
		idElement = liste[i].id;

		if (idElement.indexOf("rappel" + idFamille) >= 0) {
			if (liste[i].style.display == "inline-block")
				liste[i].style.display = "none";
			else
				liste[i].style.display = "inline-block";

			if (sousFamille != null) {
				if (liste[i].getAttribute("id") == sousFamille + "rappel"
						+ idFamille)
					liste[i].style.background = "#f1ede9";
				else
					liste[i].style.background = "none";
			}
		} else {
			liste[i].style.display = "none";
		}
	}

	// l'accordeon
	liste = document.getElementsByClassName("accordeonSousFamille");

	// le lien sous famille
	for ( var i = 0; i < liste.length; i++) {
		idElement = liste[i].id;

		if (idElement.indexOf("rappel" + idFamille) >= 0) {
			if (liste[i].style.display == "inline-block")
				liste[i].style.display = "none";
			else
				liste[i].style.display = "inline-block";
		} else {
			liste[i].style.display = "none";
		}
	}

	// mise en �vidence de la famille
	if (sousFamille == null) {
		liste = document.getElementsByClassName("familleArticle");
		for ( var i = 0; i < liste.length; i++) {
			idElement = liste[i].id;

			if (idElement.indexOf(idFamille) >= 0)
				liste[i].style.background = "#f1ede9";
			else
				liste[i].style.background = "none";
		}
	}

	if (maj)
		majTailleNavig();
}

/* ouvre dynamiquement une famille */
function majOuvertureFamilles(groupe, familleBrute) {
	var sousFamille = null;
	var famille = null;

	if (familleBrute != null) {
		if (familleBrute.length == 5 && familleBrute.substring(0, 2) == "FA")
			famille = "Famille" + familleBrute.substring(2, 5);
		else if (familleBrute.length == 7
				&& familleBrute.substring(0, 2) == "SF") {
			famille = "Famille" + familleBrute.substring(2, 5);
			sousFamille = "sf" + familleBrute.substring(2, 7);
		}
	}

	if (famille != null)
		ouvrirSousFamilles(famille, sousFamille, false);
}

/**
 *  permet de g�rer les boutons d'incr�ments de quantit� d'articles */
function majQuantiteZone(idZone, incr, cnd) 
{
	var init = document.getElementById(idZone).value;
	var total = 0;
	var increment = Number(incr);
	var conditionnement = Number(cnd);

	if (conditionnement != 1) 
		increment = Number(increment) * Number(cnd);
	
	if (init == null || isNaN(init)) 
	{
		init = 0;
		if (increment == -1)
			total = 0;
		else
			total = 1;
	}
	else
		total = Number(init) + Number(increment);

	if (total < Number(cnd) && total !=0)
		total = Number(cnd);
	
	if (total < 0)
		total = 0;

	document.getElementById(idZone).value = total;
}

/**
 * permet de g�rer les boutons d'incr�ments de quantit� d'articles avec le conditionnement global
 * */
function majQuantiteGlobal(idZone, incr)
{
	majQuantiteZone(idZone, incr, conditionnementEnCours);
}

/**
 *  permet de modifier la quantit� selon le conditionnement */
function majCnd(idZone, cnd) 
{
	var init = document.getElementById(idZone).value;
	var total = 0;
	var conditionnement = Number(cnd);
	var rst = 0 ;
	var quot = 0 ;
	
	total = Number(init) ;
	
	if (conditionnement > 1) 
	{
		if (init < Number(cnd)) 
		{
			total = Number(cnd);
		}
		
		rst = total % Number(cnd) ;
		if (rst !=0)
		{	
			quot =  total /Number(cnd);
			total =  (parseInt(quot)+1) * Number(cnd) ;
		}	
	}

	if (total < 0) total = 0;

	document.getElementById(idZone).value = total;
}

/**
 * permet de modifier la quantit� d'une zone selon le conditionnement global
 * */
function majCndGlobal(idZone)
{
	majCnd(idZone, conditionnementEnCours);
}

/**
 *  Contr�ler que la saisie */
function only_numeric(e) 
{
	var key = window.event ? e.keyCode : e.which;

	// autoriser les touches de suppr et de retour
	if (key != 0 && key != 8) {
		var valeur = String.fromCharCode(key);
		var regex = new RegExp("^[0-9]*$", "g");
		// sinon ne pas autoriser ce qui n'est pas num�rique
		if (!regex.test(valeur)) {
			e.returnValue = false;
			if (e.preventDefault)
				e.preventDefault();
		}
	}
}

/**
 *  Cette méthode permet d'ajouter dynamiquement un article au panier */
function ajoutPanier(zoneAffichage,etb,id, tarif, zoneSaisie, refFourn, conditionnement, unite, montrePopUp) 
{
	
	majCnd(zoneSaisie, conditionnement);

	var okCestFait = false;
	var quantite = document.getElementById(zoneSaisie).value.trim();
	if (quantite == null || quantite == "" || quantite == "0")
		quantite = "1";
	
	var url = "catalogue?etbArticle= " + etb + "&panierArticle=" + id + "&tarifArticle=" + tarif
			+ "&panierQuantite=" + quantite + "&refFournisseur=" + refFourn + "&conditionnement=" + conditionnement + "&uniteVente=" + unite;
	
	if (window.XMLHttpRequest) {
		requete = new XMLHttpRequest();
		if (requete) {
			requete.open("POST", url, true);
			
			if(montrePopUp)
				requete.onreadystatechange = majIHM;
			requete.send();
			document.getElementById(zoneSaisie).value = "";
			okCestFait = true;
		}
	} else if (window.ActiveXObject) {
		requete = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete) {
			requete.open("POST", url, true);
			if(montrePopUp)
				requete.onreadystatechange = majIHM;
			requete.send();
			document.getElementById(zoneSaisie).value = "";
			okCestFait = true;
		} else
			alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} else {
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}

	if (okCestFait && montrePopUp)
		afficherPopUpAjout(zoneAffichage, zoneSaisie);
}

/**
 *  Cette méthode permet d'ajouter dynamiquement un article au panier intuitif */
function ajouterPanierIntuitif(id) 
{
	var okCestFait = false;
	var montrePopUp = true;
	var zoneAffichage = "";
	
	var url = "catalogue?intuitif=1&codeArticle=" + id;
	
	if (window.XMLHttpRequest) {
		requete2 = new XMLHttpRequest();
		if (requete2) {
			requete2.open("POST", url, true);
			
			if(montrePopUp)
				requete2.onreadystatechange = majIHM2;
			requete2.send();
			
			okCestFait = true;
		}
	} else if (window.ActiveXObject) {
		requete2 = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete2) {
			requete2.open("POST", url, true);
			if(montrePopUp)
				requete2.onreadystatechange = majIHM2;
			requete2.send();
	
			okCestFait = true;
		} else
			alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} else {
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}

	if (okCestFait && montrePopUp)
		afficherPopUpAjout(zoneAffichage, null);
}

/* Ajouter tous les articles s�lectionn�s au panier*/
function ajouterToutAuPanier(nbElements)
{
	var element = null;
	var contenu = "";
	var saisie = null;
	var tab = null;
	
	var A1ART = null;
	var A1ETB = null;
	var zoneSaisie = null;
	var CND = null;
	var CAREF = null;
	var UNITE = null;
	var TARIF = null;
	var nbArticles = 0;
	var indiceNbArticles = 0;
	var onMontreLaPop = false;
	
	//PAS BO A MODIFIER VIIIITE
	for ( var i = 0; i < nbElements; i++)
	{
		saisie = "saisieQ" + i;
		if(document.getElementById(saisie)!=null && document.getElementById(saisie).value != "" && document.getElementById(saisie).value != "0")
			nbArticles++;
	}
	//PAS BO A MODIFIER VIIIITE
	for ( var i = 0; i < nbElements; i++)
	{
		saisie = "saisieQ" + i;
		if(document.getElementById(saisie)!=null && document.getElementById(saisie).value != "" && document.getElementById(saisie).value != "0")
		{
			indiceNbArticles++;
			onMontreLaPop = (indiceNbArticles==nbArticles);
			element = document.getElementById(i).value;
			tab = element.split("$$$"); 
			
			if(tab.length == 7)
			{
				A1ETB = tab[0];
				A1ART = tab[1];	
				TARIF = tab[2];
				CAREF = tab[3];
				CND = tab[4];
				UNITE = tab[5];
				zoneSaisie = tab[6];
				ajoutPanier(A1ART,A1ETB,A1ART, TARIF, zoneSaisie, CAREF, CND, UNITE,onMontreLaPop);
			}	
		}
	}
	
	if(element!=null)
	{
		//si on est en mode repr�sentant bloquer les modifs du client
		if(document.getElementById("surBlocClient")!=null)
			document.getElementById("surBlocClient").innerHTML = "<div id='blocrepresentant'>Vous ne pouvez pas changer de client tant que le panier n'est pas valid� ou supprim�</div>";
	}
}

/**
 * rajouter un filtre à la liste des articles sélectionnés
 * Suivre l'URL passée en paramètre
 * */
function rajouterUnFiltreURL(url)
{
	if(url == null) return;
	
	if(document.getElementById("stockPositif")!=null)
	{
		if(document.getElementById("stockPositif").checked)
		{
			url = url + "&stockPositif=1";
		}
		else
		{
			url = url + "&stockPositif=0";
		}
	} 
	
	if(document.getElementById("choixFournisseur")!=null)
	{
		select = document.getElementById("choixFournisseur");
		choice = select.selectedIndex;
		valeur_cherchee = select.options[choice].value;
		
		if(valeur_cherchee!= null)
		{
			url = url + "&filtreFrs=" + valeur_cherchee;
		}
	}
	
	document.location.href = url;
}


function set_opacity(id) 
{
	var valeurEffacement = 5;
	element = document.getElementById(id);
	if (opaciteGlob >= valeurEffacement)
		opaciteGlob += -valeurEffacement;
	else {
		opaciteGlob = 0;
		clearInterval(timer);
		timer = null;
	}

	if (element != null && opaciteGlob <= 100) {
		element.style["-moz-opacity"] = opaciteGlob / 100;
		element.style["-khtml-opacity"] = opaciteGlob / 100;
		element.style["opacity"] = opaciteGlob / 100;
	} else if (opaciteGlob == (180 - valeurEffacement)) {
		element.style["-moz-opacity"] = 0.9;
		element.style["-khtml-opacity"] = 0.9;
		element.style["opacity"] = 0.9;
	}

	return true;
}

/**
 * afficher la pop up qui ajoute un article au panier
 * */
function afficherPopUpAjout(id, idZoneReplacement) 
{
	opaciteGlob = 180;

	if (document.getElementById("popUpAjout") != null) 
	{
		if(id!=null)
			document.getElementById("popUpAjout").innerHTML = "Article " + id + " ajout&eacute; au panier";
		else document.getElementById("popUpAjout").innerHTML = "Articles ajout&eacute;s au panier";

		var larg = 0;
		var haut = 0;

		if (document.body) {
			larg = (document.body.clientWidth);
			haut = (document.body.clientHeight);
		} else {
			larg = (window.innerWidth);
			haut = (window.innerHeight);
		}
		
		document.getElementById("popUpAjout").style.left = (larg / 2) - 130
				+ "px";
		document.getElementById("popUpAjout").style.top = (haut / 3) + "px";

		document.getElementById("popUpAjout").style.display = "block";
	}

	timer = setInterval("set_opacity(\"popUpAjout\")", 60);
}

/**
 * Cette m�thode permet d'ouvrir cot� client la gestion des filtres de la liste
 * des articles
 */
function gererOuvertureFiltres() 
{
	var contenu = document.getElementById("contenuFiltres");
	var elementC = document.getElementById("filtreArticles");

	if (contenu != null) {
		if (contenu.style.display == "block") {
			contenu.style.display = "none";
			if (elementC != null)
				elementC.style.marginBottom = "8px";
		} else {
			contenu.style.display = "block";
			if (elementC != null)
				elementC.style.marginBottom = "0px";
		}
	}

	majTailleNavig();
}

/**
 * Mise à jour de l'interface graphique avec AJAX
 */
function majIHM() 
{
	if (requete.readyState == 4) 
	{
		if (requete.status == 200 || requete.status == 0)
		{
			document.getElementById("monPanier").innerHTML = requete.responseText;
			if(document.getElementById("surBlocClient")!=null)
				document.getElementById("surBlocClient").innerHTML = "<div id='blocrepresentant'>Vous ne pouvez pas changer de client tant que le panier n'est pas validé ou supprimé</div>";
		}
		else
			alert('Une erreur est survenue lors de la mise a jour de la page.'
					+ '\n\nCode retour = ' + requete.statusText);
	}
}

/**
 * Mise à jour de l'interface graphique avec AJAX pour les paniers intuitifs
 */
function majIHM2() 
{
	if (requete2.readyState == 4) 
	{
		if (requete2.status == 200 || requete2.status == 0)
		{
			document.getElementById("monPanier").innerHTML = requete2.responseText;
		}
		else
			alert('Une erreur est survenue lors de la mise � jour de la page.'
					+ '\n\nCode retour = ' + requete2.statusText);
	}
}

/**
 * permet d'ouvrir la pop-up g�n�rale
 */
function ouvrirPopUpGenerale() 
{
	document.getElementById("filtreTotal").style.display = "block";
	document.getElementById("popUpGenerale").style.display = "block";

	repositionnerPopUp(document.getElementById("popUpGenerale"));
}

/**
 * Appeller la pop up de traitement en cours sp�cial mode de recup�ration
 * */
function traitementStockDispo(url)
{
	if (document.getElementById("filtreTotal") != null)
		document.getElementById("popUpGenerale").style.display = "none";
	if (document.getElementById("filtreTotal") != null)
		document.getElementById("popUpGenerale").style.display = "none";
	//traitement g�n�ral dans general.js	
	traitementEnCours(url);
}

function gererOuvertureStocks()
{
	if (document.getElementById("magasinsPourStocks").style.display == "block") 
		document.getElementById("magasinsPourStocks").style.display = "none";
	else
		document.getElementById("magasinsPourStocks").style.display = "block";
}

function ouvrirPopPanierComplet(condition, referenceAffichee, libelle,tarif,unite,libUnite)
{
	conditionnementEnCours = condition;
	document.getElementById("miniCondition").innerHTML = condition;
	document.getElementById("miniReference").innerHTML = referenceAffichee;
	document.getElementById("miniLibelle").innerHTML = libelle;
	document.getElementById("miniTarif").innerHTML = tarif;
	document.getElementById("miniUnite").innerHTML = libUnite;
	document.getElementById("miniqteSaisie").value = condition;
	document.getElementById("filtreTotal").style.display = "block";
	document.getElementById("popUpPanier").style.display = "block";

	repositionnerPopUp(document.getElementById("popUpPanier"));
}

function fermerPopPanierComplet()
{
	document.getElementById("filtreTotal").style.display = "none";
	document.getElementById("popUpPanier").style.display = "none";

	//repositionnerPopUp(document.getElementById("popUpPanier"));
}

/**
 * On ferme la pop up panier de la fiche et on met au panier
 * */
function mettreAuPanierFiche()
{
	fermerPopPanierComplet();
	document.getElementById("onMetAuPanierInvisible").click();
}

/**
 *  Cette m�thode permet d'ajouter dynamiquement un article au panier */
function majFavori(etb,article) 
{

	var okCestFait = false;
	
	var url = "catalogue?favoriETB=" + etb + "&favoriArticle=" + article;
	
	if (window.XMLHttpRequest) 
	{
		requete1 = new XMLHttpRequest();
		if (requete1) 
		{
			requete1.open("POST", url, true);
			requete1.onreadystatechange = majIHMParametre;
			requete1.send();
			okCestFait = true;
		}
	} 
	else if (window.ActiveXObject) 
	{
		requete1 = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete1) {
			requete1.open("POST", url, true);
			requete1.onreadystatechange = majIHMParametre;
			requete1.send();
			okCestFait = true;
		} else
			alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} else {
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}
}

/**
 * Mise � jour de l'interface graphique avec AJAX
 */
function majIHMParametre() 
{
	if (requete1.readyState == 4) 
	{
		if (requete1.status == 200 || requete1.status == 0)
			document.getElementById("etatFavori").innerHTML = requete1.responseText;
		else
			alert('Une erreur est survenue lors de la mise à jour de la page.'
					+ '\n\nCode retour = ' + requete.statusText);
	}
}

/**
 * 
 * */
function afficherInfoBulle(id)
{
	document.getElementById(id).style.display = "block";
}

/**
 * 
 * */
function fermerInfoBulle(id)
{
	document.getElementById(id).style.display = "none";
}
