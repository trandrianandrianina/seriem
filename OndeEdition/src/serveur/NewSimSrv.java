/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package serveur;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.MessageQueue;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.seriem.libcommun.edition.ConstantesNewSim;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.ControlApplication;
import ri.seriem.libcommun.outils.ControlApplication.Listener;
import ri.seriem.libcommun.outils.FileNG;
import ri.seriem.libcommun.outils.LogMessage;
import ri.seriem.libcommun.outils.User;

//=================================================================================================
//==>                                                                       26/05/2010 - 05/03/2014
//==> Serveur des �ditions
//==> A faire:
//==>     - Voir comment g�rer si plusieurs instance du serveur sur m�me machine=pb de chemin pour
//==>            log, rt, differentes version de seriem sur m�me AS/400, etc
//==>           (emp�cher l'execution de plusieurs serveurs si m�me chemin racine)
//==> A savoir:
//==> Toutes les versions doivent �tre compatibles avec les versions pr�c�dantes pour les messages A0010 & A0011
//==> La version 1.10 am�ne la gestion du bac de sortie (� tester) - il faut mettre � jour le SEXP21LE
//==> La version 1.20 am�ne la gestion du multi �dition pour un m�me document
//==> La version 1.21 am�ne l'�dition directe de PDF sur un serveur install� Windows
//==> La version 1.23 corrige le probl�me du symbole euro
//==> La version 1.27 corrige un bug avec S�rie N lorsque le serveur NewSim est install� sur un PC
//==> La version 1.28 adaptation suite aux modifications des sources de commun400
//==> La version 1.29 corrige le bug lors de la coupure de l'AS400 qui emp�che les �ditions de fonctionner
//==>                 corrige les diff�rences de rendu entre une �dtion AS400 et Windows � cause des polices pour les editions sans affectations
//==>   La version 1.30 am�ne le nettoyage du dossier TMP au lancement du serveur
//==>   La version 1.30b corrige un bug r�cent lors de l'�dition de doc PDF sans description qui rognait parfois le dernier caract�re de chaque ligne 
//==>   La version 1.30c corrige d�finitivement les sauts de ligne intempestifs et encore parfois le dernier carcat�re rogn� 
//==>   La version 1.30d on renomme le projet et toutes les r�f�rences � OndeEdition. Tout est remplac� par NewSim 
//==>   La version 1.31 la liste des spools est transf�r�e sous forme JSON au lieu de l'objet ListContains s�rialis�
//==>   La version 1.31c on ne lit plus le port dans VGMDTA ce n'est plus d'actualit�, on pr�f�rera utiliser -p=
//==>   La version 1.31d Modification de la m�thode du nettoyage du dossier TMP qui se fait de mani�re r�cursive maintenant
//==>  001 mise � jour des apis pdfBox et fontBox
//==>  002 optimisation de lancement de la fen�tre de la Gestion des impressions
//==> La version 1.32 nettoyage des sources + FOP 2.3

//Pour les tests avec SerieN
//java -jar NewSimSrv.jar -d -dir=/tmp/sgm -p=7777 -ipsim=172.31.0.90
//java -Djava.awt.headless=true -jar NewSimSrv.jar -dir=/SerieMnewsim -d -p=5080
//=================================================================================================
public class NewSimSrv {
  private final static String NOM_CLASSE = "[NewSimSrv]";
  
  // Constantes erreurs de chargement
  private final static String ERREUR_SOCKET = "Erreur socket survenue: ";
  private final static String DEMARRAGE_SERVEUR = "D\u00e9marrage du serveur";
  private final static String STOP = "stop";
  private final static String VERSION = "1.32";
  
  // Variables
  private User utilisateur = new User("_NewSimSrv");
  private LogMessage log = null;
  private int portSGM = ConstantesNewSim.SERVER_PORT;
  private boolean debug = false;
  private int delaiLogs = 10; // En jours
  private String serveurAS = ""; // Adresse ip de l'AS/400
  private String profilAS = ""; // Profil pour se connecter � l'AS/400
  private String mdpAS = ""; // Mot de passe pour se connecter � l'AS/400
  private MessageQueue messageQueue = null;
  private ControlApplication ctrl = null;
  private ArrayList<CtrlNewSim> listeInstanceClient = new ArrayList<CtrlNewSim>();
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  /**
   * Constructeur de la classe
   */
  public NewSimSrv(String parametres) {
    AS400 systeme = null;
    
    // Si on execute le serveur NewSim sur l'AS/400, on peut tenter de lire le port
    if (System.getProperty("os.name").equals("OS/400")) {
      systeme = new AS400();
      // Lecture de VGMDTA
      // lectureDtaArea(systeme);
    }
    
    // R�cup�ration des param�tres
    if (!initParametres(parametres)) {
      if (systeme != null)
        systeme.disconnectAllServices();
      System.exit(0);
    }
    
    // Cr�ation de l'instance pour la connexion avec l'AS/400
    systeme = connexionAS400(systeme, true);
    
    // Initialisation du fichier de logs
    log = utilisateur.getLog();
    log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + DEMARRAGE_SERVEUR + " (" + VERSION + ").");
    log.setRedirigeConsole(!debug);
    log.nettoyageLogs(delaiLogs);
    if (systeme != null) {
      log.ecritureMessage("Connexion au serveur " + serveurAS + " r�ussi.");
    }
    else
      log.ecritureMessage("Connexion au serveur " + serveurAS + " �chou�e avec " + profilAS + " " + mdpAS);
    
    // Mise en place de la surveillance du controle
    initControl();
    
    // Cr�ation du socket serveur
    try {
      // On instantie la Message Queue (QSYSOPR)
      messageQueue = new MessageQueue(systeme, QSYSObjectPathName.toPath("QSYS", "QSYSOPR", "MSGQ"));
      
      // Attente de la connection d'un client
      ServerSocket srv = new ServerSocket(portSGM);
      while (true) {
        Socket nouveauclient = srv.accept();
        if (debug) {
          // Test de consommation de ram (� supprimer plus tard)
          long maxMemory = Runtime.getRuntime().maxMemory();
          long memoryUsed = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
          log.ecritureMessage("========================================= Nouvelle connexion (" + (listeInstanceClient.size() + 1) + " => "
              + (memoryUsed / (1024 * 1024)) + "Mo sur " + (maxMemory / (1024 * 1024)) + "Mo utilis�s)");
        }
        
        // On teste d'abord le service FILE afin de voir si la connexion avec l'AS/400 n'a pas �t� coup�
        // Cela pose soucis � la classe GestionSpool - listeSpool
        if (!systeme.isConnected(AS400.FILE))
          systeme = connexionAS400(systeme, true);
        // System.out.println("--> systeme : " + systeme);
        
        // Correction rapide d'un bug pour Duralex (� revoir calmement, c'est un pb de ID dans la classe USER pour la g�n�ration du
        // dossier tempo)
        User client = new User("_NewSimSrv");
        client.setAdresseIP(utilisateur.getAdresseIP());
        client.setProfil(utilisateur.getProfil());
        client.setMotDePasse(utilisateur.getMotDePasse());
        client.setLog(log);
        client.setDossierRacine(utilisateur.getDossierRacine());
        CtrlNewSim serveur = new CtrlNewSim(systeme, nouveauclient, client, messageQueue, listeInstanceClient);
        serveur.setDebug(debug);
        serveur.start();
        listeInstanceClient.add(serveur);
      }
    }
    catch (IOException e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_SOCKET + e);
    }
    if (systeme != null)
      systeme.disconnectAllServices();
    
    if (debug) {
      // Test de consommation de ram (� supprimer plus tard)
      long maxMemory = Runtime.getRuntime().maxMemory();
      long memoryUsed = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
      log.ecritureMessage("========================================= Arr�t du serveur => (" + listeInstanceClient.size() + " "
          + (memoryUsed / (1024 * 1024)) + "Mo sur " + (maxMemory / (1024 * 1024)) + "Mo utilis�s)");
    }
    else
      log.ecritureMessage("========================================= Arr�t du serveur ========================================== ");
  }
  
  /**
   * Initialise le contr�le de l'application via fichier de ctrl
   */
  private void initControl() {
    // On commence par supprimer les fichiers contenus dans le dossier (au cas o� il ait un STOP)
    nettoyageDossierCtrl();
    // Nettoyage du dossier temporaire
    nettoyageDossierTmp();
    
    // Lancement du contr�leur
    ctrl = new ControlApplication(utilisateur.getDossierRacine() + File.separatorChar + Constantes.DOSSIER_CTRL, "NewSimSrv", 10);
    if (debug)
      log.ecritureMessage("Fichier de controle: " + ctrl.getNomFichier().getAbsolutePath());
    ctrl.addListener(new Listener() {
      public void nouveauMessage(String msg) {
        log.ecritureMessage("==> Message de controle: " + msg);
        if (msg.equals(STOP))
          finTraitement();
      }
    });
  }
  
  /**
   * Supprime tous les fichiers du dossier CTRL
   */
  private void nettoyageDossierCtrl() {
    File dossierCtrl = new File(utilisateur.getDossierRacine() + File.separatorChar + Constantes.DOSSIER_CTRL);
    File[] listeFichiers = dossierCtrl.listFiles();
    if (listeFichiers == null)
      return;
    // System.out.println("-nettoyageDossierCtrl-> " + utilisateur.getDossierRacine() + File.separatorChar + Constantes.DOSSIER_CTRL + " "
    // + listeFichiers);
    for (int i = 0; i < listeFichiers.length; i++)
      listeFichiers[i].delete();
    log.ecritureMessage("==> Nettoyage du dossier " + dossierCtrl.getAbsolutePath() + " effectu�.");
  }
  
  /**
   * Supprime tous les fichiers du dossier TMP (de mani�re r�cursive)
   */
  private void nettoyageDossierTmp() {
    File dossierTmp = new File(utilisateur.getDossierRacine() + File.separatorChar + Constantes.DOSSIER_TMP);
    if (!dossierTmp.exists())
      return;
    File[] listeFichiers = dossierTmp.listFiles();
    if (listeFichiers == null)
      return;
    for (int i = 0; i < listeFichiers.length; i++)
      FileNG.remove(listeFichiers[i]);
    log.ecritureMessage("==> Nettoyage du dossier " + dossierTmp.getAbsolutePath() + " effectu�.");
  }
  
  /**
   * Arr�t des instances clients
   */
  private void fermetureInstanceClient() {
    if (listeInstanceClient == null)
      return;
    for (int i = 0; i < listeInstanceClient.size(); i++)
      if (((CtrlNewSim) listeInstanceClient.get(i)).isAlive())
        ((CtrlNewSim) listeInstanceClient.get(i)).interrupt();
    listeInstanceClient.clear();
  }
  
  /**
   * Op�ration de fin de programme
   */
  private void finTraitement() {
    // Arr�t des instances clients
    fermetureInstanceClient();
    // Arr�t de la surveillance du controle
    if (ctrl != null)
      ctrl.arret();
    log.fermetureLog();
    System.exit(0);
  }
  
  /**
   * Obsol�te avec la Dtaara dans QGPL/?SERIEN mais aussi par le param�tre -p=
   * Lecture de VGMDTA de QGPL
   * position 65 (05) port
   * @param system
   * @param infoserveur
   *
   *          private void lectureDtaArea(AS400 systeme)
   *          {
   *          String chaine="";
   * 
   *          // Lecture de la dta
   *          CharacterDataArea vgmdta = new CharacterDataArea(systeme, QSYSObjectPathName.toPath("QGPL", "VGMDTA", "DTAARA"));
   *          try
   *          {
   *          String data = vgmdta.read();
   *          /*
   *          if (data.length() >= 80)
   *          {
   *          chaine = data.substring(65, 80).trim();
   *          if (chaine.length() != 0) adresseIP = chaine;
   *          }
   *
   *          if (data.length() >= 69)
   *          {
   *          chaine = data.substring(64, 69).trim(); // Attention avec java c'est -1
   *          if (chaine.length() != 0) portSGM = Integer.parseInt(chaine);
   *          //System.out.println("-lectureDtaArea-> Port : " + portSGM);
   *          }
   *          }
   *          catch (Exception e)
   *          {}
   *          }
   */
  
  /**
   * Se connecte � l'AS/400 avec les param�tres saisis en ligne de commande
   */
  private AS400 connexionAS400(AS400 systeme, boolean force) {
    if ((!force) && (systeme != null))
      return systeme;
    
    systeme = new AS400(serveurAS, profilAS, mdpAS);
    try {
      systeme.connectService(AS400.FILE);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return systeme;
  }
  
  /**
   * R�cup�re les param�tres en entr�e depuis la chaine
   * @param parametres
   */
  private boolean initParametres(String parametres) {
    if (parametres == null)
      return false;
    
    String listeparametres[] = null;
    StringBuffer param = new StringBuffer(parametres.trim());
    String chaine = null;
    
    // On v�rifie s'il a des param�tres
    if (param.length() == 0)
      return true;
    
    // On arrange la chaine pour r�cup�rer les param�tres correctement
    int pos = param.indexOf(" ");
    while (pos != -1) {
      if (param.charAt(pos + 1) != '-')
        param.replace(pos, pos + 1, "%20");
      pos = param.indexOf(" ", pos + 1);
    }
    
    // On d�coupe la chaine et on stocke les diff�rents param�tres dans un tableau
    listeparametres = Constantes.splitString(param.toString(), ' ');
    
    // On parcourt la liste afin d'en extraire les infos
    for (int i = 0; i < listeparametres.length; i++) {
      chaine = listeparametres[i].replaceAll("%20", " ").trim();
      
      // Affiche l'aide
      if (chaine.toLowerCase().equalsIgnoreCase("-h")) {
        getAfficheListeParametres();
        return false;
      }
      
      // On recherche le port
      if (chaine.toLowerCase().startsWith("-p="))
        portSGM = Integer.parseInt(chaine.substring(3));
      else
      // On recherche le dossier utilisateur
      if (chaine.toLowerCase().startsWith("-dir="))
        utilisateur.setDossierRacine(chaine.substring(5));
      else
      // On recherche si on veut d�bogguer ou pas
      if (chaine.toLowerCase().equalsIgnoreCase("-d"))
        debug = true;
      else
      // On recherche le delai de conservation des logs (en jours) celui du jour inclu
      if (chaine.toLowerCase().startsWith("-kplogs=")) {
        delaiLogs = Integer.parseInt(chaine.substring(8));
        if (delaiLogs < 1)
          delaiLogs = 1;
      }
      else
      // On r�cup�re l'adresse IP ou nom de l'AS/400 si le serveur n'est pas dessus
      if (chaine.toLowerCase().startsWith("-hip=")) {
        serveurAS = chaine.substring(5);
        utilisateur.setAdresseIP(serveurAS); // TODO l� on accepte que les adresses IP va falloir affiner
      }
      else
      // On r�cup�re le profil pour se connecter sur l'AS/400
      if (chaine.toLowerCase().startsWith("-prf=")) {
        profilAS = chaine.substring(5);
        utilisateur.setProfil(profilAS);
      }
      else
      // On r�cup�re le mot de passe pour se connecter sur l'AS/400
      if (chaine.toLowerCase().startsWith("-mdp=")) {
        mdpAS = chaine.substring(5);
        utilisateur.setMotDePasse(mdpAS);
      }
    }
    
    // On v�rifie que les param�tres obligatoires soient bien renseign�s
    if (portSGM <= 0) {
      msgErreur = "Valeur du port incorrecte.";
      return false;
    }
    
    return true;
    
  }
  
  /**
   * Affiche la listes des param�tres possibles
   */
  private void getAfficheListeParametres() {
    System.out.println("Serveur NewSim - version " + VERSION);
    System.out.println("Param�tres possibles:");
    System.out.println("  -p=... le port sur lequel est d�marr� le serveur");
    System.out.println("  -dir=... le chemin complet dans le on trouvera les dossiers lib, sim, ...");
    System.out.println("  -kplogs=... le nombre de jours de conservation des logs");
    System.out.println("  -hip=... l'adresse IP de l'AS/400, si le serveur NewSim s'execute sur un autre machine");
    System.out.println("  -prf=... le profil pour se connecter � l'AS/400");
    System.out.println("  -mdp=... le mot de passe pour se connecter � l'AS/400");
    System.out.println("  -d active le mode d�bug");
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Programme principal
   */
  public static void main(String[] args) {
    StringBuffer parametres = new StringBuffer();
    
    // On concat�ne les arguments trouv�s dans une chaine
    for (int i = 0; i < args.length; i++)
      parametres.append(args[i].trim()).append(' ');
      
    // System.out.println("[ServeurSGM] (main) " + parametres.toString());
    // Lancement serveur
    new NewSimSrv(parametres.toString());
  }
}
