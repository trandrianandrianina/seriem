/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package serveur;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.print.PrintService;

import org.apache.pdfbox.pdmodel.PDDocument;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.OutputQueue;
import com.ibm.as400.access.SpooledFile;

import description.DescriptionDocument;
import description.DescriptionEdition;
import description.DescriptionVariable;
import description.sortie.Sortie;
import description.sortie.SortieFichier;
import description.sortie.SortieImprimante;
import rendu.GenerationFichierFO;
import rendu.GenerationFichierFinal;
import ri.seriem.libcommun.edition.ConstantesNewSim;
import ri.seriem.libcommun.edition.Demande;
import ri.seriem.libcommun.edition.Spool;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.FileNG;
import ri.seriem.libcommun.outils.LogMessage;
import ri.seriem.libcommun.outils.User;
import ri.seriem.libcommun.outils.XMLTools;
import spool.GestionSpoolAS400;

public class GestionEdition {
  // Constantes
  private final static String OUTQSRC = "/QSYS.LIB/QGPL.LIB/NEWSIM.OUTQ";
  
  // Variables
  private GestionSpoolAS400 gestionSpool = null;
  private AS400 systeme = null;
  private Demande demande = new Demande();
  private User utilisateur = null;
  private LogMessage log = null;
  private ArrayList<DescriptionVariable> listeVariables = new ArrayList<DescriptionVariable>();
  
  private StringBuffer message = new StringBuffer(ConstantesNewSim.TAILLE_BUFFER);
  
  /**
   * Constructeur
   * @param profil
   */
  public GestionEdition(User user, LogMessage log) {
    utilisateur = user;
    this.log = log;
    utilisateur.setDossierTravail(utilisateur.getDossierRacine() + File.separatorChar + ConstantesNewSim.DOSSIER_SIM);
    gestionSpool = new GestionSpoolAS400();
  }
  
  /**
   * Constructeur
   * @param profil
   */
  public GestionEdition(AS400 system, User user, LogMessage log) {
    utilisateur = user;
    this.log = log;
    utilisateur.setDossierTravail(utilisateur.getDossierRacine() + File.separatorChar + ConstantesNewSim.DOSSIER_SIM);
    systeme = system;
    gestionSpool = new GestionSpoolAS400(systeme);
  }
  
  /**
   * Liste les spools d'une outq particuli�re
   *
   * private String listeSpool()
   * {
   * message.setLength(0);
   * if (gestionSpool == null) return demande.getReponse();
   * 
   * // Liste le contenu des ou d'une outq particuli�re
   * String nomoutq = demande.getData();
   * 
   * ListContains lc = gestionSpool.getContainslListeSpool(gestionSpool.listeSpool(demande.getPrf(), nomoutq==null?nomoutq:nomoutq.trim(),
   * false));
   * try
   * {
   * message.append(XMLTools.encodeToString(lc));
   * }
   * catch(Exception e) { e.printStackTrace(); }
   * 
   * // Initialise la r�ponse � l'utilisateur
   * demande.setData('1', '1', message.toString());
   * 
   * return demande.getReponse();
   * }
   */
  
  /**
   * Liste les spools d'une outq particuli�re
   */
  private String listeSpool() {
    message.setLength(0);
    if (gestionSpool == null)
      return demande.getReponse();
    
    // Liste le contenu des ou d'une outq particuli�re
    String nomoutq = demande.getData();
    
    // Initialise la r�ponse � l'utilisateur
    String chaine =
        gestionSpool.getJSONListeSpool(gestionSpool.listeSpool(demande.getPrf(), nomoutq == null ? nomoutq : nomoutq.trim(), false));
    if (chaine != null)
      demande.setData('1', '2', chaine);
    else
      demande.setData(' ', '9', gestionSpool.getMsgErreur());
    
    return demande.getReponse();
  }
  
  /**
   * On suspend un spool
   */
  private String suspendSpool() {
    message.setLength(0);
    if (gestionSpool == null)
      return "";
    
    // On retrouve le spool qui nous int�resse
    try {
      SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()),
          demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
      gestionSpool.suspendSpool(sp, "*IMMED");
    }
    catch (Exception e) {
    }
    
    // Initialise la r�ponse � l'utilisateur
    // demande.setDat(message.toString());
    return "";
  }
  
  /**
   * On supprime un spool
   */
  private String supprimeSpool() {
    message.setLength(0);
    if (gestionSpool == null)
      return "";
    
    // On retrouve le spool qui nous int�resse
    try {
      SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()),
          demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
      gestionSpool.supprimeSpool(sp, "*IMMED");
    }
    catch (Exception e) {
    }
    
    // Initialise la r�ponse � l'utilisateur
    // demande.setDat(message.toString());
    return "";
  }
  
  /**
   * On lib�re un spool
   */
  private String libereSpool() {
    message.setLength(0);
    if (gestionSpool == null)
      return "";
    
    // On retrouve le spool qui nous int�resse
    try {
      SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()),
          demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
      gestionSpool.libereSpool(sp);
    }
    catch (Exception e) {
    }
    
    // Initialise la r�ponse � l'utilisateur
    // demande.setDat(message.toString());
    return "";
  }
  
  /**
   * On modifie/imprime un spool (option 2 du WRKSPLF)
   */
  private String modifieSpool() {
    // System.out.println("--> " + demande.getData());
    if (gestionSpool == null)
      return "";
    if (demande.getData().trim().equals(""))
      return "";
    
    // On v�rifie s'il y a le nombre d'exemplaire
    String[] infos = Constantes.splitString(demande.getData(), Constantes.SEPARATEUR_CHAINE_CHAR);
    int nbrexemplaire = 1;
    if (infos.length == 2)
      nbrexemplaire = Integer.parseInt(infos[1].trim());
    
    // On retrouve le spool qui nous int�resse
    try {
      SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()),
          demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
      OutputQueue outq = new OutputQueue(gestionSpool.getSystem(), infos[0]);
      gestionSpool.move(sp, outq, nbrexemplaire);
    }
    catch (Exception e) {
    }
    
    return "";
  }
  
  /**
   * Pr�pare l'envoi des infos sur le serveur NewSim
   * @return
   */
  /*
  @SuppressWarnings("unchecked")
  private String envoiInfosServer()
  {
  	// On r�cup�re les infos sur le param�tre NT de toutes les biblioth�ques fichiers
  	JSONObject objjson = new JSONObject();
  	objjson.put("OS", System.getProperty("os.name"));
  	//objjson.put("NT", getListInfosNTFromListFMLibrary());
  	objjson.put("NT", getListInfosNTFromFMLibrary(demande.getBib()));
  
  	// Initialise la r�ponse pour l'utilisateur
  	//demande.setData('1', '2', objjson.toJSONString());
  	demande.setData(' ', ' ', objjson.toJSONString());
  	
  	return demande.getReponse();
  }*/
  
  /**
   * Liste les imprimantes de l'AS/400
   */
  /*
  private String listeImprimantes()
  {
  	GestionImprimanteAS400 gia = new GestionImprimanteAS400(systeme);
  	
  	// Initialise la r�ponse � l'utilisateur
  	JSONArray listeprt = gia.getListeImprimantes();
  	if (listeprt == null)
  		demande.setData(' ', '9', gia.getMsgErreur());
  	else
  		demande.setData('1', '2', listeprt.toJSONString());
  	
  	return demande.getReponse();
  }*/
  
  /**
   * Demande un spool en particulier
   */
  private String afficheSpool() {
    message.setLength(0);
    if (gestionSpool == null)
      return demande.getReponse();
    
    // On retrouve le spool qui nous int�resse
    try {
      SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()),
          demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
      Spool spool = gestionSpool.getSpool(sp);
      if (spool != null)
        message.append(XMLTools.encodeToString(spool));
    }
    catch (Exception e) {
    }
    
    // Initialise la r�ponse � l'utilisateur
    demande.setData('1', '1', message.toString());
    
    return demande.getReponse();
  }
  
  /**
   * Traite un message sur un spool
   */
  /*
  private String messagesSpool()
  {
  	message.setLength(0);
  	if (gestionSpool == null) return demande.getReponse();
  	
  	// On retrouve le spool qui nous int�resse
  	try
  	{
  		SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()), demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
  		String msg = demande.getData().trim();
  //System.out.println("--> " + msg);			
  		// On demande juste le message
  		if (msg.equals(""))
  			message.append(gestionSpool.messageSpool(sp, false, ""));
  		else // On envoie la r�ponse au message 
  			gestionSpool.messageSpool(sp, true, msg);
  	}
  	catch(Exception e) {}
  	
  	// Initialise la r�ponse � l'utilisateur
  	demande.setData(' ', ' ', message.toString());
  
  	return demande.getReponse();
  }*/
  
  /**
   * Extrait le spool puis g�n�re le fichier
   * @param extension
   * @param fichiersortie
   * @param DescriptionEdition dde
   * @return
   */
  private String getDocument(String extension, String fichiersortie, DescriptionEdition dde, String nomDescriptionDocument) {
    message.setLength(0);
    if (gestionSpool == null)
      return "";
    Spool spool = null;
    String dossier = utilisateur.getDossierTravail() + File.separatorChar + demande.getNomspool().toLowerCase();// utilisateur.getDossierTravail();
    String fichierDDD = null;
    int actionSpool = DescriptionEdition.HOLD;
    DescriptionDocument doc = null;
    GenerationFichierFO gffo = null;
    if (fichiersortie == null)
      fichiersortie = utilisateur.getDossierTempUser() + File.separatorChar + demande.getNomspool().trim().toLowerCase();
      
    // R�cup�ration du dossier DDE
    // dossier = utilisateur.getDossierTravail() + File.separatorChar + demande.getNomspool().toLowerCase();
    if (dde != null) {
      fichierDDD = nomDescriptionDocument;
      actionSpool = dde.getActionSpool();
    }
    else {
      fichierDDD = demande.getNomspool().toLowerCase() + DescriptionDocument.EXTENSION;
    }
    
    // On retrouve le spool qui nous int�resse
    try {
      SpooledFile sp = new SpooledFile(gestionSpool.getSystem(), demande.getNomspool(), Integer.parseInt(demande.getNumspool().trim()),
          demande.getNomjb(), demande.getUsrjb(), demande.getNumjb());
      spool = gestionSpool.getSpool(sp);
      // log.ecritureMessage("-getDocument-> spool:" + spool);
      if (spool == null)
        return null;
      // R�cup�ration de la description du spool � partir de son nom
      // log.ecritureMessage("-getDocument-> chemin desc:" + dossierDDE + File.separatorChar + fichierDDD);
      File fichier = new File(dossier + File.separatorChar + fichierDDD);
      if (fichier.exists())
        doc = (DescriptionDocument) XMLTools.decodeFromFile(fichier.getAbsolutePath());
      else
        doc = null;
      // System.out.println("--> " + dossier + File.separatorChar + fichierDDD);
      // else
      // {
      // log.ecritureMessage("[GestionEdition] (getDocument) Description document non trouv�e : " + fichier.getAbsolutePath());
      // return null;
      // }
      // On supprime le spool si besoin (il est d�j� hold� normalement)
      if (actionSpool == DescriptionEdition.DEL)
        sp.delete();
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    
    // Si pas de description de document
    if (doc == null) {
      dossier = utilisateur.getDossierTempUser();
      gffo = new GenerationFichierFO(spool, dossier, true);
    }
    else // sinon
    {
      gffo = new GenerationFichierFO(doc, spool.getPages(), utilisateur.getDossierTempUser());
      listeVariables.clear();
      gffo.setListeVariables(listeVariables);
    }
    // log.ecritureMessage("-[GestionEdition]-> fichier " + fichiersortie);
    // Rendu final
    // System.out.println("--> extension: " + extension + " - " + fichiersortie);
    GenerationFichierFinal gff = new GenerationFichierFinal(log);
    gff.setTypeSortie(extension);
    gff.setFichierSortie(fichiersortie);
    
    // Initialise la r�ponse � l'utilisateur
    // gffo.genereFOtoFile(); //***> Si on le d�commente on a le fichier FO mais le PDF est en erreur
    // System.out.println("--> user getDossier: " + dossierDDE);
    // System.out.println("--> doc: " + doc);
    // System.out.println("--> dossier: " + dossier);
    
    int dpi = 72;
    if (doc != null) {
      dpi = doc.getSource_dpi();
    }
    
    return gff.renduFinal(dossier, gffo.genereFOtoString(), dpi, 72);
  }
  
  /**
   * Analyse la chaine afin d'en extraire les infos et d'alimenter la DDE pour la g�n�ration du fichier
   * @param dde
   * @return
   */
  private boolean analyseGenereDocument(String chaine, DescriptionEdition dde) {
    String nomoutqsource = OUTQSRC;
    int actionspool = DescriptionEdition.DEL;
    final SortieFichier sf = new SortieFichier();
    
    // R�cup�ration des param�tres de la requ�te
    String[] parametres = Constantes.splitString(chaine, '|');
    for (int i = 0; i < parametres.length; i++) {
      parametres[i] = parametres[i].trim();
      switch (i) {
        // L'extension (type de document g�n�r�: ps, pdf)
        case 0:
          sf.setTypeDoc(parametres[i]);
          sf.setExtension(parametres[i]);
          break;
        // Nom de l'outq d'origine
        case 1:
          nomoutqsource = parametres[i];
          break;
        // Dossier
        case 2:
          // C'est le client qui va donner le bon nom de dossier grace aux infos envoy� par le serveur et pas le serveur
          // String os = getOs();
          // Cas particulier pour Windows le dossier temp pass� en param�tre doit �tre adapt� ("/sgm/tmp" -> "c:\program files\sgm\tmp")
          // Il faudra am�liorer cela plus tard (le /sgm/tmp est mis en dur par la classe GfxSpoolViewer du projet commun package edition)
          // if( (os.equals("windows")) && (parametres[i].equals("/sgm/tmp")) )
          // sf.setNomDossier("y:\\tmp");//utilisateur.getDossierTemp());
          // else
          sf.setNomDossier(parametres[i]);
          break;
        // Chemin complet avec nom du fichier (sans extension)
        case 3:
          sf.setNomFichier(parametres[i]);
          break;
        // Que fait on du spool ? 4:supprime 3:suspend
        case 4:
          if (parametres[i].length() != 0)
            actionspool = Integer.parseInt(parametres[i]);
          break;
        // Si non vide alors on lance une application associ� au document g�n�r�
        case 5:
          sf.setCommande(parametres[i]);
          break;
        
        default:
      }
    }
    // On finit d'alimenter la description d'�dition
    dde.setNomOutQueue(nomoutqsource);
    dde.setActionSpool(actionspool);
    sf.setFichierDescriptionDocument(dde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION);
    dde.addTypeSortie(sf);
    
    return true;
  }
  
  /**
   * G�n�re le document complet gr�ce aux param�tres
   * @return
   */
  private String genereDocument() {
    final DescriptionEdition dde = new DescriptionEdition();
    String fichier = null;
    
    // G�n�ration d'une description d'�dition (� la vol�e)
    dde.setUtilisation(DescriptionEdition.DIRECTE);
    dde.setNomSpool(demande.getNomspool());
    analyseGenereDocument(demande.getData(), dde);
    
    // On traite la description d'�dition
    if (traitementDDE(dde) && (dde.getTypeSortie().size() > 0))
      fichier =
          ((SortieFichier) dde.getTypeSortie().get(0)).getNomComplet().replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR);
    
    // System.out.println("-genereDocument-> " + fichier);
    demande.setData(' ', ' ', fichier);
    return demande.getReponse();
  }
  
  /**
   * Analyse la chaine afin d'en extraire les infos et d'alimenter la DDE pour l'impression du fichier
   * @param dde
   * @return
   */
  private boolean analyseImprimeDocument(String chaine, DescriptionEdition dde) {
    String nomoutqsource = OUTQSRC;
    int actionspool = DescriptionEdition.DEL;
    final SortieImprimante si = new SortieImprimante();
    
    // R�cup�ration des param�tres de la requ�te
    String[] parametres = Constantes.splitString(demande.getData(), '|');
    for (int i = 0; i < parametres.length; i++) {
      parametres[i] = parametres[i].trim();
      switch (i) {
        // L'extension (type de document g�n�r�: ps, pdf)
        case 0:
          si.setTypeDoc(parametres[i]);
          si.setExtension(parametres[i]);
          break;
        // Langage de l'imprimante (ps, pjl, ...)
        case 1:
          si.setLgimprimante(parametres[i]);
          break;
        // Nom de l'outq d'origine
        case 2:
          nomoutqsource = parametres[i];
          break;
        // Nom de l'outq de l'imprimante
        case 3:
          si.setNomOutq(parametres[i]);
          break;
        // Num�ro du tiroir d'entr�e
        case 4:
          if (parametres[i].length() != 0)
            si.setTiroirEntree(Integer.parseInt(parametres[i]));
          break;
        // Recto-verso 0:non 1:rectoverso longedge 2:rectoverso shortedge
        case 5:
          if (parametres[i].equals("0") || parametres[i].equals(""))
            si.setRectoverso(false);
          else
            si.setRectoverso(true);
          if (si.isRectoverso())
            si.setShortedge(parametres[i].equals("2"));
          break;
        // Nombre de copie
        case 6:
          if (parametres[i].length() != 0)
            si.setNbrExemplaire(Integer.parseInt(parametres[i]));
          break;
        // Que fait on du spool ? 4:supprime 3:suspend
        case 7:
          if (parametres[i].length() != 0)
            actionspool = Integer.parseInt(parametres[i]);
          break;
        // Mode brouillon 0:non 1:brouillon
        case 8:
          if (parametres[i].length() != 0)
            si.setEconomie(parametres[i].equals("1"));
          break;
        // Densit� de l'impression (qualit�)
        case 9:
          if (parametres[i].length() != 0)
            si.setDensite(Integer.parseInt(parametres[i]));
          break;
        // Forcer le noir et blanc 0:non 1:force noir&blanc
        case 10:
          if (parametres[i].length() != 0)
            si.setNoirblanc(parametres[i].equals("1"));
          break;
        case 11:
          // On rend la main imm�diatement
          si.setSoumission(!parametres[i].trim().equals(""));
          break;
        // Num�ro du tiroir de sortie
        case 12:
          if (parametres[i].length() != 0)
            si.setTiroirSortie(Integer.parseInt(parametres[i]));
          break;
        
        default:
      }
    }
    
    // On finit d'alimenter la description d'�dition
    dde.setNomOutQueue(nomoutqsource);
    dde.setActionSpool(actionspool);
    si.setNomSpool(dde.getNomSpool());
    si.setFichierDescriptionDocument(dde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION);
    si.genereScript();
    dde.addTypeSortie(si);
    
    return true;
  }
  
  /**
   * Imprime le document complet gr�ce aux param�tres
   */
  private String imprimeDocument() {
    final DescriptionEdition dde = new DescriptionEdition();
    
    // G�n�ration d'une description d'�dition (� la vol�e)
    dde.setUtilisation(DescriptionEdition.DIRECTE);
    dde.setNomSpool(demande.getNomspool());
    // dde.setFichierDescriptionDocument(demande.getNomspool().toLowerCase() + DescriptionDocument.EXTENSION);
    analyseImprimeDocument(demande.getData(), dde);
    
    // On traite la description d'�dition
    traitementDDE(dde);
    
    // On pr�pare la r�ponse au client si l'on ne soumet pas l'�dition
    if ((dde.getTypeSortie().size() > 0) && !(((SortieImprimante) dde.getTypeSortie().get(0)).isSoumission())) {
      demande.setData(' ', ' ', null);
      return demande.getReponse();
    }
    return "";
  }
  
  /**
   * Gestion du multi document
   */
  private String multiDocument() {
    // A faire : suspendu car Pigasse n'a pas donn�e suite
    // Chaine � recevoir pour le d�coupage
    // A0010|....|#FIN#|A0012|....|#FIN#|
    return "";
  }
  
  /**
   * G�n�re le document et le traite � l'aide de la description d'�dition
   * @param extension
   */
  private String genereAvecDDE() {
    DescriptionEdition dde = null;
    // FileNG fichiertempo=null;
    
    // System.out.println("-genereAvecDDE-> data:" + demande.getData() + "|");
    // System.out.println("-genereAvecDDE-> Obj:" + demande.getObj());
    // R�cup�ration de la description (DDE)
    if (!demande.getData().equals("") && (demande.getObj() == '1')) {
      try {
        // G�n�ration de l'objet � partir de sa description XML
        dde = (DescriptionEdition) XMLTools.decodeFromString(demande.getData());
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (dde == null)
      return demande.getReponse(); // TODO A voir qd il a erreur (l� on dit rien)
      
    traitementDDE(dde);
    demande.setData(' ', ' ', null);
    return demande.getReponse();
  }
  
  /**
   * Traitement des Descriptions d'�dition
   * TODO voir � int�grer la suppression dans les m�thodes traiteSortieImprimante & traiteSortieFichier
   * @param dde
   * @return
   */
  private boolean traitementDDE(DescriptionEdition dde) {
    // On parcourt toutes les sorties de la description d'�dition
    for (int i = 0; i < dde.getTypeSortie().size(); i++) {
      // On commence par g�n�rer le document
      final String fichier = getDocument(((Sortie) dde.getTypeSortie().get(i)).getTypeDoc(), null, dde,
          ((Sortie) dde.getTypeSortie().get(i)).getFichierDescriptionDocument());
      // System.out.println("-genereAvecDDE-> fichier " + fichier);
      
      if (fichier == null)
        continue; // TODO A voir qd il a erreur (l� on dit rien)
      final FileNG fichiertempo = new FileNG(fichier);
      
      // On v�rifie le type de sortie (fichier, impression, mail, ...)
      if (dde.getTypeSortie().get(i) instanceof SortieFichier)
        traiteSortieFichier((SortieFichier) dde.getTypeSortie().get(i), fichiertempo);
      else if (dde.getTypeSortie().get(i) instanceof SortieImprimante) {
        // On v�rifie si l'impression est soumise
        if (((SortieImprimante) dde.getTypeSortie().get(i)).isSoumission()) {
          final SortieImprimante si = (SortieImprimante) dde.getTypeSortie().get(i);
          new Thread() {
            public void run() {
              traiteSortieImprimante(si, fichiertempo);
              log.ecritureMessage("[GestionEdition]-traitementDDE-> Suppression du dossier tempo (en soumission) : "
                  + fichiertempo.getParentFile().getAbsolutePath());
              FileNG.remove(fichiertempo.getParentFile());
              // ((FileNG) fichiertempo.getParentFile()).removeFolder(fichiertempo.getParentFile());
            }
          }.start();
          continue;
        }
        else // Pas de soumission
          traiteSortieImprimante((SortieImprimante) dde.getTypeSortie().get(i), fichiertempo);
      }
      
      // Suppression du fichier temporaire (TODO � voir dans certains cas la suppression n'a pas lieu)
      log.ecritureMessage(
          "[GestionEdition]-traitementDDE-> Suppression du dossier tempo : " + fichiertempo.getParentFile().getAbsolutePath());
      FileNG.remove(fichiertempo.getParentFile());
      // ((FileNG) fichiertempo.getParentFile()).removeFolder(fichiertempo.getParentFile());
    }
    
    return true;
  }
  
  /**
   * Traitement des Descriptions d'�dition
   * @param dde
   * @return
   *
   *         private boolean traitementDDE(DescriptionEdition dde)
   *         {
   *         // On commence par g�n�rer le document
   *         String fichier = getDocument(dde.getTypeDoc(), null, dde);
   *         //System.out.println("-genereAvecDDE-> fichier " + fichier);
   *         
   *         if (fichier == null) return false; // TODO A voir qd il a erreur (l� on dit rien)
   *         FileNG fichiertempo = new FileNG(fichier);
   *         
   *         // On v�rifie le type de sortie (fichier, impression, mail, ...)
   *         //System.out.println("-genereAvecDDE-> type de sortie " + dde.getTypeSortie().size());
   *         for (int i=0; i<dde.getTypeSortie().size(); i++)
   *         {
   *         if (dde.getTypeSortie().get(i) instanceof SortieFichier)
   *         traiteSortieFichier((SortieFichier) dde.getTypeSortie().get(i), fichiertempo);
   *         else
   *         if (dde.getTypeSortie().get(i) instanceof SortieImprimante)
   *         traiteSortieImprimante((SortieImprimante) dde.getTypeSortie().get(i), fichiertempo);
   *         }
   * 
   *         // Suppression du fichier temporaire
   *         //System.out.println("-genereAvecDDE-> termin� " + fichiertempo.getParentFile());
   *         log.ecritureMessage("[GestionEdition]-genereAvecDDE-> Suppression du dossier tempo : " +
   *         fichiertempo.getParentFile().getAbsolutePath());
   *         FileNG.remove(fichiertempo.getParentFile());
   *         return true;
   *         }
   */
  
  /**
   * Traitement dans le cas d'un fichier physique � un endroit d�termin�
   * @param dde
   * @param fichiertempo
   */
  private boolean traiteSortieFichier(SortieFichier sf, FileNG fichiertempo) {
    if ((sf == null) || (fichiertempo == null) || (!fichiertempo.exists()))
      return false;
    boolean ret = false;
    
    // On transforme le nom complet du fichier si besoin (s'il contient des variables)
    // System.out.println("-traiteSortieFichier-> " + sf.getNomComplet());
    // String chemin = iData.analyseExpression(sf.getNomComplet());
    String chemin = sf.getNomComplet();
    if (listeVariables != null) {
      // System.out.println("-traiteSortieFichier 1-> " + chemin);
      // TODO bourrin � optimiser un jour
      for (int i = listeVariables.size(); --i >= 0;) {
        // System.out.println("--traiteSortieFichier 2-> " + listeVariables.get(i).getNom() + " " +
        // listeVariables.get(i).getValeur()+"|");
        chemin = chemin.replaceAll(listeVariables.get(i).getNom(), listeVariables.get(i).getValeur());
      }
    }
    
    // System.out.println("-traiteSortieFichier 3-> " + chemin);
    if (chemin != null) {
      chemin = Constantes.code2unicode(chemin);
      ret = fichiertempo.copyTo(chemin);
      if (ret)
        log.ecritureMessage(
            "[GestionEdition]-traiteSortieFichier-> R�ussite de la copie de " + fichiertempo.getAbsolutePath() + " vers " + chemin);
      else
        log.ecritureMessage(
            "[GestionEdition]-traiteSortieFichier-> Echec de la copie de " + fichiertempo.getAbsolutePath() + " vers " + chemin);
    }
    
    // On analyse les param�tres si on en trouve
    if (ret && !sf.getCommande().trim().equals(""))
      analyseParametres(sf.getCommande(), chemin);
    
    return ret;
  }
  
  /**
   * Traitement dans le cas d'une impression directe
   * @param dde
   * @param fichiertempo
   */
  private boolean traiteSortieImprimante(SortieImprimante si, FileNG fichiertempo) {
    if ((si == null) || (fichiertempo == null) || (!fichiertempo.exists()))
      return false;
    
    // V�rification de la validit� du fichier tempo (extension)
    if (!fichiertempo.getExtension(false).toLowerCase().equals(si.getExtension()))
      return false;
    
    // On v�rifie que l'extension soit correcte
    if (!si.getNomOutq().toLowerCase().endsWith(".outq"))
      return false;
    
    // On pr�pare l'envoi du fichier dans la outq de l'imprimante
    final OutputQueue outq = new OutputQueue(systeme, si.getNomOutq());
    gestionSpool.setOutq(outq);
    // log.ecritureMessage("-(traiteSortieImprimante)-> " + si.getNomSpool());
    final SpooledFile splf = gestionSpool.cpyFichier2Spool(fichiertempo.getAbsolutePath(), si.getNomSpool(), si.getNbrExemplaire(),
        si.isSuspendre(), null, si.getPriorite(), si.getScriptAvant(true), si.getScriptApres(true));
    if (splf == null)
      return false;
    gestionSpool.libereSpool(splf);
    
    return true;
  }
  
  /**
   * Traite la demande de l'utilisateur
   * @param msg
   * @return
   */
  public String getTraitementDemande(String msg) {
    if (!demande.setReceptionBuffer(msg))
      return "";
    
    String idm = demande.getIdm();
    // Les demandes directes
    if (idm.equals(ConstantesNewSim.LISTE_SPOOL))
      return listeSpool();
    if (idm.equals(ConstantesNewSim.MODIFIER_SPOOL))
      return modifieSpool();
    if (idm.equals(ConstantesNewSim.SUSPENDRE_SPOOL))
      return suspendSpool();
    if (idm.equals(ConstantesNewSim.SUPPRIMER_SPOOL))
      return supprimeSpool();
    if (idm.equals(ConstantesNewSim.AFFICHE_SPOOL))
      return afficheSpool();
    if (idm.equals(ConstantesNewSim.LIBERER_SPOOL))
      return libereSpool();
    // if (idm.equals(ConstantesNewSim.INFOS_SERVER))
    // return envoiInfosServer();
    if (idm.equals(ConstantesNewSim.GENERE_DOC_FROM_SPL))
      return genereDocument();
    if (idm.equals(ConstantesNewSim.IMPRIME_DOC_FROM_SPL))
      return imprimeDocument();
    if (idm.equals(ConstantesNewSim.MULTI_DOC))
      return multiDocument();
    // if (idm.equals(ConstantesNewSim.LISTE_PRINTER))
    // return listeImprimantes();
    // if (idm.equals(ConstantesNewSim.MESSAGE_SPOOL))
    // return messagesSpool();
    
    // Les demandes avec une description
    if (idm.equals(ConstantesNewSim.DDE_DOC))
      return genereAvecDDE();
    
    if (idm.equals(ConstantesNewSim.DECONNECTION_SPOOL))
      return ConstantesNewSim.STOP;
    
    return "";
  }
  
  /**
   * Analyse les param�tres dans le cas d'une g�n�ration d'un fichier si le serveur NewSim se trouve sur autre chose qu'un AS/400
   * Aujourd'hui 3 ordres possibles
   * -exec=... , ex�cute le script (avec le chemin complet) pour traiter le fichier
   * -printerName=, envoi le fichier (obligatoirement PDF) sur l'imprimante nomm�e (reconnue par le PC)
   * -silentPrint , �dite sans afficher la popup d'impression
   */
  private boolean analyseParametres(String parametres, String fichier) {
    // System.out.println("-analyseParametres-> " + parametres);
    if (parametres == null)
      return false;
    
    String listeparametres[] = null;
    StringBuffer param = new StringBuffer(parametres.trim());
    String chaine = null;
    boolean silentprt = false, deletefile = false;
    String printername = null;
    
    // On v�rifie s'il a des param�tres
    if (param.length() == 0)
      return true;
    
    // On arrange la chaine pour r�cup�rer les param�tres correctement
    int pos = param.indexOf(" ");
    while (pos != -1) {
      if (param.charAt(pos + 1) != '-')
        param.replace(pos, pos + 1, "%20");
      pos = param.indexOf(" ", pos + 1);
    }
    
    // On d�coupe la chaine et on stocke les diff�rents param�tres dans un tableau
    listeparametres = Constantes.splitString(param.toString(), ' ');
    
    // On parcourt la liste afin d'en extraire les infos
    for (int i = 0; i < listeparametres.length; i++) {
      chaine = listeparametres[i].replaceAll("%20", " ").trim();
      // System.out.println("-analyseParametres-> chaine=" + chaine);
      // On lance une commande sur le syst�me
      if (chaine.toLowerCase().startsWith("-exec="))
        executeCommande(chaine.substring(6), fichier);
      else
      // On recherche le dossier utilisateur
      if (chaine.toLowerCase().startsWith("-prt="))
        printername = chaine.substring(5);
      else
      // On recherche si on veut imprimer en silence ou pas
      if (chaine.toLowerCase().equalsIgnoreCase("-silentprint"))
        silentprt = true;
      else
      // On recherche si on veut supprimer le fichier ou non
      if (chaine.toLowerCase().equalsIgnoreCase("-delete"))
        deletefile = true;
    }
    
    // On imprime si besoin
    if (printername != null) {
      try {
        printPDF(printername, silentprt, fichier, deletefile);
      }
      catch (Exception e) {
      }
    }
    return true;
    
  }
  
  /**
   * Execute la commande sur la machine o� se trouve le serveur NewSim
   */
  private boolean executeCommande(String cmd, String fichier) {
    // System.out.println("--> " + cmd + " " + fichier);
    try {
      final Runtime runtime = Runtime.getRuntime();
      runtime.exec(new String[] { cmd, fichier });
      return true;
    }
    catch (Exception e) {
      log.ecritureMessage("Erreur lors du lancement de la commande: " + cmd);
    }
    return false;
  }
  
  /**
   * Imprime directement un fichier PDF gr�ce au driver natif des imprimantes
   * @throws PrinterException
   * @throws IOException
   */
  private boolean printPDF(String printerName, boolean silentPrint, String fichierpdf, boolean delete) throws Exception {
    boolean printerFound = false;
    PDDocument document = null;
    // System.out.println("-printPDF-> " + printerName + "|" + fichierpdf + "|" + silentPrint);
    // On v�rifie que le document soit bien un PDF
    if ((fichierpdf == null) || (fichierpdf.toLowerCase().lastIndexOf(".pdf") == -1))
      return false;
    File fichier = new File(fichierpdf);
    try {
      document = PDDocument.load(fichier);
      
      // if( document.isEncrypted()) document.decrypt( password );
      
      PrinterJob printJob = PrinterJob.getPrinterJob();
      printJob.setJobName(fichier.getName());
      
      if (printerName != null) {
        printerName = printerName.replaceAll("\"", "");
        PrintService[] printService = PrinterJob.lookupPrintServices();
        for (int i = 0; !printerFound && i < printService.length; i++) {
          if (printService[i].getName().indexOf(printerName) != -1) {
            printJob.setPrintService(printService[i]);
            printerFound = true;
          }
        }
      }
      // System.out.println("-printPDF-> " + printerFound);
      // if (!printerFound) return false;
      
      if (silentPrint && printerFound)
        document.silentPrint(printJob);
      else
        document.print(printJob);
    }
    finally {
      if (document != null)
        document.close();
    }
    
    // On supprime le fichier si besoin et s'il existe
    if ((delete) && (fichier.exists()))
      if (fichier.delete())
        log.ecritureMessage("[GestionEdition]-printPDF-> R�ussite de la suppression de " + fichier.getAbsolutePath());
      else
        log.ecritureMessage("[GestionEdition]-printPDF-> Echec de la suppression de " + fichier.getAbsolutePath());
      
    return false;
  }
  
  /**
   * Retourne le lecteur r�seau et le dossier partag� de toutes les FM
   * Plus utilis�e car trop lente entre 15s et 45s lors de la premi�re utilisation
   * @return
   *
   *         @SuppressWarnings("unchecked")
   *         private JSONArray getListInfosNTFromListFMLibrary()
   *         {
   *         //System.out.println("-[GestionEdition]-> IP:" + utilisateur.getAdresseIP()+" Profil:"+ utilisateur.getProfil()+" Mdp:"+
   *         utilisateur.getMotDePasse());
   *         String[] listeFM=null;
   *         ArrayList<String> listeBib=new ArrayList<String>();
   *         ArrayList<String> listeLecteur=new ArrayList<String>();
   *         ArrayList<String> listeDossier=new ArrayList<String>();
   *         JSONArray tabjson = null;
   *         JSONObject objjson=null;
   * 
   *         SystemManager systeme= new SystemManager(utilisateur.getAdresseIP(), utilisateur.getProfil(), utilisateur.getMotDePasse(),
   *         true);
   *         AS400 server = systeme.getSystem();
   *         GenericRecordManager grm = new GenericRecordManager(systeme.getdatabase().getConnection());
   *         
   *         // On liste toutes les biblioth�ques potentiellement interessante
   *         IFSFile qsys = new IFSFile(server, "/QSYS.LIB");
   *         try
   *         {
   *         listeFM = qsys.list(new IFSFileFilter() {
   *         
   *         public boolean accept(IFSFile arg0)
   *         {
   *         return (arg0.getName().startsWith("FM"));
   *         }
   *         });
   *         }
   *         catch (IOException e)
   *         {
   *         e.printStackTrace();
   *         }
   * 
   *         // On parcourt la liste des biblioth�ques afin d'�liminer celles qui n'ont pas le fichier PSEMPARM
   *         if (listeFM != null)
   *         for (int i=0; i<listeFM.length; i++)
   *         {
   *         try
   *         {
   *         IFSFile psemparm = new IFSFile(server, "/QSYS.LIB/" + listeFM[i] + "/PSEMPARM.FILE");
   *         if (psemparm.exists())
   *         {
   *         String bib = listeFM[i].substring(0, listeFM[i].lastIndexOf('.'));
   *         readParamNTOnFMLibrary(grm, bib, i, listeBib, listeLecteur, listeDossier);
   *         }
   *         }
   *         catch (IOException e)
   *         {
   *         e.printStackTrace();
   *         }
   *         }
   * 
   *         // Tranformation en JSon
   *         if (listeBib.size() > 0)
   *         {
   *         tabjson = new JSONArray();
   *         for (int i=0; i<listeBib.size(); i++)
   *         {
   *         objjson = new JSONObject();
   *         objjson.put("BIBLIOTHEQUE", listeBib.get(i));
   *         objjson.put("LECTEUR", listeLecteur.get(i));
   *         objjson.put("DOSSIER", listeDossier.get(i));
   *         tabjson.add(objjson);
   *         }
   *         //System.out.println("--> " + tabjson.toJSONString());
   *         }
   * 
   *         // On d�connecte tout
   *         //if (systeme != null) systeme.disconnect();
   *         //systeme = null;
   *         
   *         return tabjson;
   *         }
   */
  
  /**
   * Retourne le lecteur r�seau et le dossier partag� d'une FM en particuliers
   * @param lib
   * @return
   */
  /*
  @SuppressWarnings("unchecked")
  private JSONObject getListInfosNTFromFMLibrary(String libfm)
  {
  //System.out.println("-[GestionEdition]-> IP:" + utilisateur.getAdresseIP()+" Profil:"+ utilisateur.getProfil()+" Mdp:"+ utilisateur.getMotDePasse());		
  	String[] listeInfos = {"", ""};
  	JSONObject objjson=null;
  
  	SystemManager systeme= new SystemManager(utilisateur.getAdresseIP(), utilisateur.getProfil(), utilisateur.getMotDePasse(), true);
  	AS400 server = systeme.getSystem();
  	GenericRecordManager grm = new GenericRecordManager(systeme.getdatabase().getConnection());
  	
  	try
  	{
  		IFSFile psemparm = new IFSFile(server, "/QSYS.LIB/" + libfm.trim() + ".LIB/PSEMPARM.FILE");
  		if (psemparm.exists())
  		{
  			readParamNTOnFMLibrary(grm, libfm, listeInfos);
  		}
  	}
  	catch (IOException e)
  	{
  		e.printStackTrace();
  	}
  	
  	// Tranformation en JSon
  	objjson = new JSONObject();
  	objjson.put("BIBLIOTHEQUE", libfm);
  	objjson.put("LECTEUR", listeInfos[0]);
  	objjson.put("DOSSIER", listeInfos[1]);
  	
  	return objjson;
  }*/
  
  /**
   * Lit le param�tre NT du fichier PSEMPARM d'un biblioth�que
   * @param grm
   * @param bibfm
   * @param index
   * @param listeBib
   * @param listeLecteur
   * @param listeDossier
   *
   *          private void readParamNTOnFMLibrary(GenericRecordManager grm, String bibfm, int index, ArrayList<String> listeBib,
   *          ArrayList<String> listeLecteur, ArrayList<String> listeDossier)
   *          {
   *          if ((bibfm == null) || (bibfm.trim().equals(""))) return;
   *          
   *          // On r�cup�re les donn�es dans le param�tre NT du fichier PSEMPARM
   *          String requete = "select parfil from "+bibfm+".psemparm where parcle = ' NT '";
   *          //System.out.println("--> " + requete);
   *          // On ne doit en trouver qu'un ou aucun (si le param�trage du NT n'a pas �t� fait)
   *          final ArrayList<GenericRecord>liste = grm.select(requete);
   *          //System.out.println("--> " + liste);
   *          if ((liste != null) && (liste.size() > 0))
   *          {
   *          String parfil = (String) liste.get(0).getField("PARFIL");
   *          
   *          listeBib.add(bibfm);
   *          listeLecteur.add(parfil.charAt(71)+":");
   *          String dossier = parfil.substring(72, 101).trim();
   *          if( (dossier.length() > 0) && (dossier.charAt(0)) != '/')
   *          dossier = '/' + dossier;
   *          listeDossier.add(dossier);
   *          }
   *          }
   */
  
  /**
   * Lit le param�tre NT du fichier PSEMPARM d'un biblioth�que
   * @param grm
   * @param bibfm
   * @param listeinfos
   */
  /*
  private void readParamNTOnFMLibrary(GenericRecordManager grm, String bibfm, String[] listeinfos) {
    if ((bibfm == null) || (bibfm.trim().equals("")))
      return;
    
    // On r�cup�re les donn�es dans le param�tre NT du fichier PSEMPARM
    String requete = "select parfil from " + bibfm + ".psemparm where parcle = '   NT     '";
    // System.out.println("--> " + requete);
    // On ne doit en trouver qu'un ou aucun (si le param�trage du NT n'a pas �t� fait)
    final ArrayList<GenericRecord> liste = grm.select(requete);
    // System.out.println("--> " + liste);
    if ((liste != null) && (liste.size() > 0)) {
      String parfil = (String) liste.get(0).getField("PARFIL");
      
      listeinfos[0] = parfil.charAt(71) + ":";
      String dossier = parfil.substring(72, 101).trim();
      if ((dossier.length() > 0) && (dossier.charAt(0)) != '/')
        dossier = '/' + dossier;
      listeinfos[1] = dossier;
    }
  }*/
  
  /**
   * Retourne le nom g�n�rique du syst�me d'exploitation
   * @return
   *
   *         private String getOs()
   *         {
   *         String os = System.getProperty("os.name").toLowerCase();
   *         int pos = os.indexOf(' ');
   *         if (pos != -1)
   *         os = os.substring(0, pos);
   *         return os;
   *         }
   */
}
