/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package serveur;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.MessageQueue;

import ri.seriem.libcommun.edition.ConstantesNewSim;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.LogMessage;
import ri.seriem.libcommun.outils.StringIP;
import ri.seriem.libcommun.outils.User;

public class CtrlNewSim extends Thread {
  private final static String NOM_CLASSE = "[CtrlOndeEdition]";
  
  // Constantes erreurs de chargement
  private final static String ERREUR_RENCONTREE = "Erreur pendant l'ex\u00e9cution : ";
  private final static String ERREUR_ENVOI_SOCKET = "Probl\u00e8me lors de l'envoi du message sur la socket.";
  private final static String CONNEXION_CLIENT = "Connexion d'un nouveau client : ";
  private final static String DECONNEXION_CLIENT = "D\u00e9connexion du client : ";
  private final static String NOMBRE_CLIENT = "Nombre de client(s) connect\u00e9(s) : ";
  private final static String CODEPAGE = "CP858"; // Mis en place pour les noms de device avec des � (le spool �tait introuvable)
  
  // Variables diverses
  public DataInputStream in;
  public DataOutputStream ou;
  public Socket socketclient;
  private static int nbrclient = 0;
  private LogMessage log = null;
  private boolean debug = false;
  private User utilisateur = null;
  private ArrayList<CtrlNewSim> listeInstanceClient = null;
  private AS400 systeme = null;
  
  /**
   * Constructeur de la classe
   */
  public CtrlNewSim(AS400 system, Socket s, User user, MessageQueue msgQ, ArrayList<CtrlNewSim> lstInstanceClient) {
    systeme = system;
    socketclient = s;
    utilisateur = user;
    log = utilisateur.getLog();
    listeInstanceClient = lstInstanceClient;
    nbrclient++;
    log.ecritureMessage("-CtrlOndeEdition (constructeur)-> nbrclient " + nbrclient);
  }
  
  /**
   * Boucle principale
   */
  public void run() {
    InetAddress inet;
    StringIP client = new StringIP("");
    int boucle = Constantes.TRUE;
    byte buffer[] = null;
    String chaine;
    int idmessage;
    boolean fromAS400 = false;
    
    // System.out.println("-[CtrlOndeEdition]-> IP:" + utilisateur.getAdresseIP()+" Profil:"+ utilisateur.getProfil()+" Mdp:"+
    // utilisateur.getMotDePasse());
    // System.out.println("-[CtrlOndeEdition]-> systeme:" + systeme);
    // System.out.println("-[CtrlOndeEdition]-> systeme:" + systeme.isConnected() + " " + systeme.isConnectionAlive());
    GestionEdition gestionEdition = new GestionEdition(systeme, utilisateur, log);
    
    try {
      inet = socketclient.getInetAddress();
      in = new DataInputStream(new BufferedInputStream(socketclient.getInputStream(), Constantes.TAILLE_BUFFER));
      ou = new DataOutputStream(new BufferedOutputStream(socketclient.getOutputStream(), Constantes.TAILLE_BUFFER));
      socketclient.setSoTimeout(Constantes.SOCKET_TIMEOUT);
      
      client.setIP(inet.toString());
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + CONNEXION_CLIENT + client.getIP());
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + NOMBRE_CLIENT + nbrclient);
    }
    catch (Exception e) {
      // A voir
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
    }
    
    try {
      while (boucle == Constantes.TRUE) {
        idmessage = in.readInt();
        if (debug)
          log.ecritureMessage("IDMessage recu :" + idmessage);
        switch (idmessage) {
          // Le client se connecte et demande le protocole du serveur pour le comparer au sien <----
          /*					case ConstantesOndeEdition.CONNEXION_OK :
          System.out.println("--> " + ConstantesOndeEdition.CONNEXION_OK);						
          						ou.writeInt(ConstantesOndeEdition.CONNEXION_OK);
          						ou.flush();
          					break;
          */
          // Le client (programme RPG mais pour SGM) fait une demande <---------------------------------
          case ConstantesNewSim.DEMANDE_REQUETE_AS:
            fromAS400 = true;
            
            // Le client SGM fait une demande <-----------------------------------------------------------
          case ConstantesNewSim.RECOIT_REQUETE:
            if (fromAS400) {
              chaine = lectureSocketFromAS400();
              fromAS400 = false;
            }
            else {
              buffer = lectureSocket();
              chaine = new String(buffer, 0, buffer.length);
            }
            log.ecritureMessage("-1-> " + chaine);
            chaine = gestionEdition.getTraitementDemande(chaine);
            log.ecritureMessage("-2-> " + chaine);
            if (chaine.equals(ConstantesNewSim.STOP)) {
              // Envoi l'arr�t � la sessionEdition
              EnvoiMessageSocket(Constantes.STOP, null);
              boucle = Constantes.FALSE;
              log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + DECONNEXION_CLIENT + client.getIP());
            }
            else if (!chaine.equals(""))
              EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE, chaine);
            break;
          
          // La surveillance des outq fait une demande <-----------------------------------------------------------
          case ConstantesNewSim.RECOIT_REQUETE_VEILLE:
            buffer = lectureSocket();
            chaine = new String(buffer, 0, buffer.length);
            // System.out.println("-1-> " + chaine);
            chaine = gestionEdition.getTraitementDemande(chaine);
            // System.out.println("-2-> " + chaine);
            if (chaine.equals(ConstantesNewSim.STOP)) {
              // Envoi l'arr�t � la sessionEdition
              EnvoiMessageSocket(Constantes.STOP, null);
              boucle = Constantes.FALSE;
              log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + DECONNEXION_CLIENT + client.getIP());
            }
            else if (!chaine.equals(""))
              EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE_VEILLE, chaine);
            break;
          
          // Valeur non trait�e <----------------------------------------------------------------
          default:
            // On lit le contenu du buffer pour pas bloquer l'instance
            buffer = new byte[in.available()];
            in.read(buffer);
            break;
        }
      }
    }
    catch (IOException e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "Perte de " + client.getIP());
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
    }
    try {
      // Fermeture des sockets
      if (ou != null)
        ou.close();
      if (in != null)
        in.close();
      if (socketclient != null)
        socketclient.close();
      socketclient = null;
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
    listeInstanceClient.remove(this);
    nbrclient--;
    
    // System.out.println("-CtrlOndeEdition (run-fin) nbrclient " + nbrclient);
    log.ecritureMessage("-CtrlOndeEdition (run-fin) nbrclient " + nbrclient);
    if (debug) {
      log.ecritureMessage("-CtrlSGM (run-fin) nbrclient " + nbrclient);
      
      // Test de consommation de ram (� supprimer plus tard)
      long maxMemory = Runtime.getRuntime().maxMemory();
      long memoryUsed = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
      log.ecritureMessage("========================================= Fermeture connexion (" + (listeInstanceClient.size() + 1) + " => "
          + (memoryUsed / (1024 * 1024)) + "Mo sur " + (maxMemory / (1024 * 1024)) + "Mo utilis�s)");
    }
  }
  
  /**
   * Lecture des octets re�us sur la Socket lors d'une demande du serveur SGM
   * @throws UnsupportedEncodingException
   */
  private synchronized String lectureSocketFromAS400() throws UnsupportedEncodingException {
    int octet_lus = 0;
    byte buffercomplet[] = new byte[512]; // Correspond � la taille de la variable Requete du SEXP21LE
    
    // On r�cup�re tous les octets envoy�s par le client
    try {
      octet_lus = in.read(buffercomplet);
      // log.ecritureMessage("[CtrlVGM] (lectureSocket) : octet_lus=" + octet_lus);
    }
    catch (Exception e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
      return null;
    }
    
    // System.out.println("[CtrlVGM] (lectureSocket) " + new String(buffercomplet));
    return new String(buffercomplet, 0, octet_lus - 1, CODEPAGE);
  }
  
  /**
   * Lecture des octets re�us sur la Socket
   */
  private synchronized byte[] lectureSocket() {
    int octet_alire = 0, octet_lus = 0, compteur = 0;
    byte buffercomplet[] = null;
    
    // On r�cup�re tous les octets envoy�s par le client
    try {
      octet_alire = in.readInt();
      buffercomplet = new byte[octet_alire];
      do {
        // System.out.println("[CtrlVGM] (lectureSocket) : octet_alire="+octet_alire);
        octet_lus = in.read(buffercomplet, compteur, octet_alire);
        compteur += octet_lus;
        octet_alire -= octet_lus;
      }
      while (octet_alire != 0);
    }
    catch (Exception e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_RENCONTREE + e);
      return null;
    }
    
    // System.out.println("[CtrlVGM] (lectureSocket) " + new String(buffercomplet));
    return buffercomplet;
  }
  
  /**
   * Envoi un message au Client
   */
  public synchronized int EnvoiMessageSocket(int idmsg, String message) {
    int longueur = 0;
    try {
      ou.writeInt(idmsg);
      if (message != null) {
        message = message.trim(); // Modif � cause du blocage si connexion a distance
        longueur = message.length(); // Modif � cause du blocage si connexion a distance
        ou.writeInt(longueur);
        ou.write(message.getBytes(), 0, longueur);
      }
      ou.flush();
    }
    catch (Exception e) {
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + ERREUR_ENVOI_SOCKET);
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + message);
      log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + e.getMessage());
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Initialise l'activation du serveur Http
   * @param ishttp
   *
   *          public void setHttp(boolean ishttp)
   *          {
   *          http = ishttp;
   *          }
   */
  
  /**
   * Initialise le mode debug
   * @param isdebug
   */
  public void setDebug(boolean isdebug) {
    debug = isdebug;
  }
  
  /**
   * Retourne la version
   * @return
   *
   *         public String getVersion()
   *         {
   *         return VERSION;
   *         }
   */
  
}
