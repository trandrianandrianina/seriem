/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package veilleOutq;

import java.io.File;
import java.io.FilenameFilter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CommandCall;

import description.DescriptionEdition;
import ri.seriem.libcommun.edition.ConstantesNewSim;
import ri.seriem.libcommun.edition.Demande;
import ri.seriem.libcommun.edition.SessionEdition;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.ControlApplication;
import ri.seriem.libcommun.outils.ControlApplication.Listener;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.LinkedHashMapManager.lhdListener;
import ri.seriem.libcommun.outils.LogMessage;
import ri.seriem.libcommun.outils.XMLTools;

//=================================================================================================
//==> Surveille les Outqs en fonctions des descriptions des �ditions DDE
//=================================================================================================
public class VeilleOutq {
  // Constantes
  private final static String NOM_CLASSE = "[VeilleOutq]";
  private final static String VERSION = "1.20";
  private final static String LOCALHOST = "127.0.0.1";
  private final static String STOP = "stop";
  private final static String RELOAD = "reload";
  private final static int PRIORITY = 50;
  
  // Variables
  private String racine = File.separatorChar + ConstantesNewSim.DOSSIER_RACINE;
  private AS400 systeme = null;
  private LogMessage log = null;
  private String profil = null, mot2passe = null, host = null;
  private int delaiLogs = 10; // En jours
  private String dossierDDE = racine + File.separatorChar + ConstantesNewSim.DOSSIER_SIM; // Dossier qui contiendra les documents DDE
  private boolean debug = false;
  private int delaiBalayage = 10; // En seconde
  private LinkedHashMapManager fileAttenteClient = null;
  private SessionEdition sessionEdition = null;
  private ControlApplication ctrl = null;
  private ArrayList<TraitementOutq> listetOutQ = new ArrayList<TraitementOutq>();
  
  /**
   * Constructeur
   * @param parametres
   */
  public VeilleOutq(String parametres) {
    // Initialisation du fichier de logs (TODO un par jour, modifier LogMessage)
    // System.out.println("--> " + racine + ConstantesOndeEdition.DOSSIER_LOG + " " + debug);
    log = new LogMessage(racine + File.separatorChar + ConstantesNewSim.DOSSIER_LOG, "_VeilleOutq");
    log.ecritureMessage(NOM_CLASSE + Constantes.SEPARATEUR + "D�marrage programme" + " " + VERSION);
    log.nettoyageLogs(delaiLogs);
    
    // R�cup�ration des param�tres
    if (!initParametres(parametres))
      sortieTraitement("Param�tres en entr�e incorrects.");
    log.setRedirigeConsole(!debug);
  }
  
  /**
   * Initialisation du traitement
   */
  public void initTraitement() {
    // Mise en place de la surveillance du controle
    initControl();
    
    // Connexion au serveur d'�dition
    initConnexionEditionSrv();
    
    // Ouverture d'une session AS/400
    systeme = getSystem();
  }
  
  /**
   * Lance le traitement
   */
  public void lanceTraitement() {
    File fdossierDDE = null;
    LinkedHashMap<String, ArrayList<DescriptionEdition>> listeDDEOutQ = new LinkedHashMap<String, ArrayList<DescriptionEdition>>();
    TraitementOutq tOutQ = null;
    try {
      // System.out.println("--> V�rif DDE");
      // V�rification du dossier DDE
      fdossierDDE = getVerifDossierDDE();
      
      // Listage du contenu du dossier DDE
      File[] listeFichierDDE = getListeFichierDDE(fdossierDDE);
      log.ecritureMessage("--> Nombre de dossier DDE trouv�s : " + listeFichierDDE.length);
      if (listeFichierDDE.length == 0)
        sortieTraitement("Pas de description DDE � traiter dans le dossier " + dossierDDE);
      
      // On trie les descriptions par Outq
      triDDE(listeFichierDDE, listeDDEOutQ);
      
      // Initialisation de la file d'attente
      initFileAttente();
      TraitementOutq.fileAttenteClient = fileAttenteClient;
      SessionEdition.fileAttenteClient = fileAttenteClient;
      
      // Modification de la priorit� de l'application
      CommandCall cmd = new CommandCall(systeme);
      cmd.setThreadSafe(true);
      cmd.run("CHGJOB RUNPTY(" + PRIORITY + ")");
      
      // Lancement d'un processus par outq (parcourt de la linkedhashmap)
      Iterator<String> i = listeDDEOutQ.keySet().iterator();
      while (i.hasNext()) {
        String clef = (String) i.next();
        tOutQ = new TraitementOutq(systeme, listeDDEOutQ.get(clef), log);
        tOutQ.setDebug(debug);
        tOutQ.setDelai(delaiBalayage);
        if (tOutQ.connexion())
          tOutQ.start();
        listetOutQ.add(tOutQ);
      }
      if (debug)
        log.ecritureMessage("--> Surveillance lanc�e.");
    }
    catch (Exception e) {
      log.ecritureMessage("[veilleOutQ] (lanceTraitement) Erreur: " + e);
      e.printStackTrace();
    }
  }
  
  /**
   * Fermeture des traitements en cours
   */
  private void fermetureTraitement() {
    if (listetOutQ == null)
      return;
    for (int i = 0; i < listetOutQ.size(); i++)
      if (listetOutQ.get(i).isAlive())
        listetOutQ.get(i).interrupt();
    listetOutQ.clear();
  }
  
  /**
   * Travaux en fin de travail
   */
  public void finTraitement() {
    // Arr�t des traitements Outq
    fermetureTraitement();
    // Arr�t de la surveillance du controle
    if (ctrl != null)
      ctrl.arret();
    log.fermetureLog();
    System.exit(0);
  }
  
  /**
   * Ouvre une session sur l'AS/400
   * @return
   */
  private AS400 getSystem() {
    AS400 systeme = null;
    
    // Connexion au syst�me
    if (host != null)
      systeme = new AS400(host, profil, mot2passe);
    else {
      systeme = new AS400();
      host = LOCALHOST;
    }
    return systeme;
  }
  
  /**
   * Chargement des fichiers DDE et triage par outq
   * @param liste
   */
  private void triDDE(File[] liste, LinkedHashMap<String, ArrayList<DescriptionEdition>> listeDDEOutQ) {
    ArrayList<DescriptionEdition> listeDDE = null;
    
    for (int i = 0; i < liste.length; i++) {
      // Chargement du fichier DDE
      DescriptionEdition dde = chargementDDE(liste[i].getAbsolutePath());
      if (dde == null)
        continue;
      if (dde.getVersion() == 0) {
        log.ecritureMessage("--> ATTENTION : la version du fichier DDE est obsol�te. Veuillez la mettre � jour avec l'�diteur.");
        continue;
      }
      
      // R�cup�ration de la liste pour la outq
      if ((dde.getUtilisation() == DescriptionEdition.NON_UTILISE) || (dde.getUtilisation() == DescriptionEdition.DIRECTE))
        continue;
      if (dde.getNomOutQueue() == null)
        continue;
      listeDDE = listeDDEOutQ.get(dde.getNomOutQueue());
      if (listeDDE == null) {
        listeDDE = new ArrayList<DescriptionEdition>();
        listeDDEOutQ.put(dde.getNomOutQueue(), listeDDE);
      }
      listeDDE.add(dde);
    }
  }
  
  /**
   * Charge le fichier de description (dde)
   * @return
   */
  private DescriptionEdition chargementDDE(String fichier) {
    // Lecture du fichier dde
    try {
      return (DescriptionEdition) XMLTools.decodeFromFile(fichier);
    }
    catch (Exception e) {
      log.ecritureMessage("[veilleOutQ] (chargementDDE) Erreur " + e + "\n" + fichier);
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * Retourne la liste des fichiers DDE
   * @param fdossier
   * @return
   */
  private File[] getListeFichierDDE(File fdossier) {
    if ((fdossier == null) || (!fdossier.exists()))
      return null;
    
    // Liste les dossiers DDE
    File[] listeDossierDDE = fdossier.listFiles(new FilenameFilter() {
      public boolean accept(File dir, String name) {
        return new File(dir.getAbsolutePath() + File.separatorChar + name).isDirectory();
      }
    });
    if (listeDossierDDE == null)
      return null;
    
    // On parcourt ces dossiers afin de collecter les fichiers DDE
    ArrayList<File> listeFichierDDE = new ArrayList<File>();
    File tab[] = null;
    for (int i = 0; i < listeDossierDDE.length; i++) {
      tab = listeDossierDDE[i].listFiles(new FilenameFilter() {
        public boolean accept(File dir, String name) {
          return name.endsWith(DescriptionEdition.EXTENSION);
        }
      });
      if (tab != null)
        for (int j = 0; j < tab.length; j++)
          listeFichierDDE.add(tab[j]);
    }
    
    // Transformation de l'arraylist en tableau
    tab = new File[listeFichierDDE.size()];
    listeFichierDDE.toArray(tab);
    return tab;
  }
  
  /**
   * V�rification du dossier DDE
   */
  private File getVerifDossierDDE() {
    if (dossierDDE == null)
      sortieTraitement("Le dossier contenant les DDE n'est pas renseign�.");
    File fdossierDDE = new File(dossierDDE);
    if (!fdossierDDE.exists())
      sortieTraitement("Le dossier des DDE n'existe pas: " + fdossierDDE.getAbsolutePath());
    return fdossierDDE;
  }
  
  /**
   * Sort du traitement avec affichage de la raison
   * @param result
   */
  private void sortieTraitement(String result) {
    if (result != null)
      log.ecritureMessage(result);
    System.exit(1);
  }
  
  /**
   * Initialise la file d'attente qui va recevoir la liste des spools � traiter
   */
  private void initFileAttente() {
    fileAttenteClient = new LinkedHashMapManager(false, false, false);
    fileAttenteClient.addListener(new lhdListener() {
      public void onDataCleared() {
      }
      
      public void onDataRemoved(Object cle) {
        requeteServeurEdition(cle, null);
      }
      
      public void onDataAdded(Object cle, Object val) {
        requeteServeurEdition(cle, val);
      }
    });
  }
  
  /**
   * Initialise le controle de l'application via fichier de ctrl
   */
  private void initControl() {
    ctrl = new ControlApplication(racine + File.separatorChar + ConstantesNewSim.DOSSIER_CTRL, "VeilleOutq", 10);
    if (debug)
      log.ecritureMessage("Fichier de controle: " + ctrl.getNomFichier().getAbsolutePath());
    ctrl.addListener(new Listener() {
      public void nouveauMessage(String msg) {
        log.ecritureMessage("==> Message de controle: " + msg);
        if (msg.equals(STOP))
          finTraitement();
        else if (msg.equals(RELOAD)) {
          fermetureTraitement();
          lanceTraitement();
        }
      }
    });
  }
  
  /**
   * Initialise la connexion au serveur d'�dition
   */
  private void initConnexionEditionSrv() {
    if ((sessionEdition == null) || (!sessionEdition.isConnected())) {
      // TODO Passer ces infos en param�tre de l'application (initParametres)
      try {
        sessionEdition = new SessionEdition(InetAddress.getByName(host), ConstantesNewSim.SERVER_PORT, log);
        if (sessionEdition.setSession() == Constantes.ERREUR) {
          log.ecritureMessage("Probl�me lors de la connexion au serveur d'�dition sur " + host + ":" + ConstantesNewSim.SERVER_PORT);
          sortieTraitement("Non connect� au serveur �dition, l'application est stopp�e.");
        }
      }
      catch (UnknownHostException e) {
        // TODO Bloc catch g�n�r� automatiquement
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Envoi si besoin une demande d'�dition
   */
  private synchronized void requeteServeurEdition(Object cle, Object val) {
    boolean envoi = false;
    
    initConnexionEditionSrv();
    
    // On v�rifie si l'on envoi ou non une requ�te
    if (debug)
      log.ecritureMessage("--> Taille File attente : " + fileAttenteClient.getHashMap().size());
    if (val == null) {
      if (debug)
        log.ecritureMessage("--> Enl�ve File attente-> " + cle);
      if (fileAttenteClient.getHashMap().size() > 0)
        envoi = true;
    }
    else {
      if (debug)
        log.ecritureMessage("--> Ajoute File attente-> " + cle);
      if (fileAttenteClient.getHashMap().size() == 1)
        envoi = true;
    }
    if (!envoi)
      return;
    
    // On envoi une requ�te au serveur d'�dition
    Demande demandeEdition = new Demande(Demande.CLEF, (String) fileAttenteClient.getKeyAtIndex(0));
    DescriptionEdition dde_doc = (DescriptionEdition) fileAttenteClient.getValueAtIndex(0);
    try {
      // On initialise la demande
      demandeEdition.setIdm(ConstantesNewSim.DDE_DOC);
      demandeEdition.setData('1', '1', XMLTools.encodeToString(dde_doc).toString());
      if (sessionEdition.EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE_VEILLE, demandeEdition.getReponse()) == Constantes.FALSE) {
        sessionEdition = null;
        log.ecritureMessage("Probl�me lors de l'envoi de la requ�te vers le serveur d'�dition");
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * R�cup�re les param�tres en entr�e depuis la chaine
   * @param parametres
   */
  private boolean initParametres(String parametres) {
    if (parametres == null)
      return false;
    
    String listeparametres[] = null;
    StringBuffer param = new StringBuffer(parametres.trim());
    String chaine = null;
    
    // On arrange la chaine pour r�cup�rer les param�tres correctement
    int pos = param.indexOf(" ");
    while (pos != -1) {
      if (param.charAt(pos + 1) != '-')
        param.replace(pos, pos + 1, "%20");
      pos = param.indexOf(" ", pos + 1);
    }
    
    // On v�rifie s'il a des param�tres
    if (param.length() == 0)
      return false;
    
    // On d�coupe la chaine et on stocke les diff�rents param�tres dans un tableau
    listeparametres = Constantes.splitString(param.toString(), ' ');
    
    // On parcourt la liste afin d'en extraire les infos
    for (int i = 0; i < listeparametres.length; i++) {
      chaine = listeparametres[i].replaceAll("%20", " ").trim();
      
      // On recherche le fichier INI de configuration
      // if (chaine.toLowerCase().startsWith("-ini="))
      // fichierINI = chaine.substring(5);
      // else
      // On recherche le profil
      if (chaine.toLowerCase().startsWith("-prf="))
        profil = chaine.substring(5);
      else
      // On recherche le mot de passe
      if (chaine.toLowerCase().startsWith("-mdp="))
        mot2passe = chaine.substring(5);
      else
      // On recherche le host
      if (chaine.toLowerCase().startsWith("-h="))
        host = chaine.substring(3);
      else
      // On recherche le delai de conservation des logs (en jours) celui du jour inclu
      if (chaine.toLowerCase().startsWith("-kplogs=")) {
        delaiLogs = Integer.parseInt(chaine.substring(8));
        if (delaiLogs < 1)
          delaiLogs = 10;
      }
      else
      // On recherche si dossier � traiter
      if (chaine.toLowerCase().startsWith("-dir="))
        dossierDDE = chaine.substring(5);
      else
      // On recherche si debug
      if (chaine.toLowerCase().equals("-d"))
        debug = true;
      else
      // On recherche le delai de balayage des outq en secondes
      if (chaine.toLowerCase().startsWith("-hz=")) {
        delaiBalayage = Integer.parseInt(chaine.substring(4));
        if (delaiBalayage < 1)
          delaiBalayage = 10;
      }
      
    }
    
    // On v�rifie que les param�tres obligatoires soient bien renseign�s
    // if ( (fichierINI == null) || (fichierINI.trim().equals("")) )
    // {
    // msgErreur = "Fichier listeouq.ini manquant";
    // return false;
    // }
    
    return true;
  }
  
  /**
   * Retourne la version
   * @return
   */
  public String getVersion() {
    return VERSION;
  }
  
  /**
   * java -Djava.awt.headless=true -classpath .:jt400.jar veilleOutQ.jar
   * @param args
   */
  public static void main(String[] args) {
    StringBuffer parametres = new StringBuffer();
    
    // On teste les arguments en entr�e
    if (args.length == 0) {
      System.out.println("Il manque le dossier contenant la liste des descriptions des �ditions DDE.");
      System.exit(1);
    }
    
    // On concat�ne les arguments trouv�s dans une chaine
    for (int i = 0; i < args.length; i++)
      parametres.append(args[i].trim()).append(' ');
    
    VeilleOutq vo = new VeilleOutq(parametres.toString());
    vo.initTraitement();
    vo.lanceTraitement();
  }
  
}
