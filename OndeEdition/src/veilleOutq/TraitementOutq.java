/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package veilleOutq;

import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.SpooledFile;
import com.ibm.as400.access.SpooledFileList;

import description.DescriptionEdition;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.LogMessage;
import spool.GestionSpoolAS400;

//=================================================================================================
//==> Traite les spools d'une OutQ
//=================================================================================================
public class TraitementOutq extends Thread {
  // Variables
  private AS400 systeme = null;
  private ArrayList<DescriptionEdition> listeDescriptionEdition = null;
  private GestionSpoolAS400 gsa_entree = null;
  private String nomOutq = null;
  private int delai = 10; // en seconde
  private LogMessage log = null;
  private boolean debug = false;
  
  public static LinkedHashMapManager fileAttenteClient = null;
  
  /**
   * Constructeur
   * @param systeme
   * @param listeDDE
   */
  public TraitementOutq(AS400 systeme, ArrayList<DescriptionEdition> listeDDE, LogMessage log) {
    this.systeme = systeme;
    listeDescriptionEdition = listeDDE;
    this.log = log;
    setDaemon(true);
    initNomOutq();
  }
  
  /**
   * Connexion aux OutQ
   * @return
   */
  public boolean connexion() {
    if (systeme == null)
      systeme = new AS400();
    
    // Connexion � la outq d'entr�e
    gsa_entree = new GestionSpoolAS400(systeme);
    if ((gsa_entree == null) || (nomOutq == null))
      return false;
    // gsa_brut.setCodepage(codepage);
    gsa_entree.setOutq(nomOutq);
    
    return true;
  }
  
  /**
   * Surveillance de la outq en cours
   * @return
   */
  public void run() {
    SpooledFile spool = null;
    boolean attend = true;
    
    // V�rification de base
    if (gsa_entree == null)
      return;
    log.ecritureMessage("D�marrage de la surveillance de " + nomOutq);
    
    // Boucle infini
    while (true) {
      attend = true;
      // On liste tous les spools de la outq
      // long t0 = System.currentTimeMillis();
      SpooledFileList listespool = gsa_entree.listeSpool("*ALL", nomOutq, true);
      // long t1 = System.currentTimeMillis() - t0;
      // log.ecritureMessage("Temps execution listage : " + t1);
      // System.out.println("Nombre de spool: " + listespool + "\n" + gsa_entree.getMsgErreur());
      
      // On balaye les spools afin de rechercher ceux qui nous interesse
      // t0 = System.currentTimeMillis();
      if (listespool != null)
        for (int x = 0; x < listespool.size(); x++) {
          spool = (SpooledFile) listespool.getObject(x);
          // for (int i=0; i<listeDescriptionEdition.size(); i++)
          for (int i = listeDescriptionEdition.size(); --i >= 0;) {
            try {
              // Tests pour savoir si le spool nous interesse
              // if( (listeDescriptionEdition.get(i).getEtat() == DescriptionEdition.INACTIF)
              if ((!spool.getName().equals(listeDescriptionEdition.get(i).getNomSpool())
                  || (!spool.getStringAttribute(SpooledFile.ATTR_SPLFSTATUS).equals("*READY"))))
                continue;
              // On suspend le spool
              spool.hold("*IMMED");
              // attend = false;
              
              // On cr�� une demande d'�dition � l'aide des infos du spool puis on la met avec la description dans une file d'attente
              String clef = spool.getJobNumber() + '|' + spool.getJobName() + '|' + spool.getJobUser() + '|' + spool.getName() + '|'
                  + spool.getNumber();
              fileAttenteClient.addObject(clef, listeDescriptionEdition.get(i));
              if (debug)
                log.ecritureMessage("--> Taille file attente client " + fileAttenteClient.getHashMap().size());
            }
            catch (Exception e) {
              log.ecritureMessage("[TraitementOutQ] (run) " + nomOutq + " en erreur " + e);
              e.printStackTrace();
            }
          }
        }
      // t1 = System.currentTimeMillis() - t0;
      // log.ecritureMessage("Temps execution traitement : " + t1);
      
      // Pause avant rebouclage
      if (attend)
        try {
          Thread.sleep(delai * 1000);
        }
        catch (InterruptedException ex) {
          Thread.currentThread().interrupt(); // Tr�s important de r�interrompre
          break;
        }
    }
  }
  
  /**
   * @return the log
   */
  public LogMessage getLog() {
    return log;
  }
  
  /**
   * @param log the log to set
   */
  public void setLog(LogMessage log) {
    this.log = log;
  }
  
  /**
   * Initialise le nom de la outq
   */
  private void initNomOutq() {
    // On r�cup�re le nom de la outq
    if ((listeDescriptionEdition == null) || (listeDescriptionEdition.size() == 0))
      return;
    nomOutq = listeDescriptionEdition.get(0).getNomOutQueue();
  }
  
  /**
   * @return the delai
   */
  public int getDelai() {
    return delai;
  }
  
  /**
   * @param delai the delai to set
   */
  public void setDelai(int delai) {
    this.delai = delai;
  }
  
  /**
   * Initialise le debug
   * @param db
   */
  public void setDebug(boolean db) {
    debug = db;
  }
}
