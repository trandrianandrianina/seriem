/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rendu;

import ri.seriem.libcommun.outils.Constantes;

//=================================================================================================
//==> Interpr�tation des variables (� la JWalk)                             
//==> A faire:
//==>   Voir comment faire quand les donn�es sont modifi�es sur un m�me format (listePanelData)
//==>   Voir si l'on peut optimiser surtout avec 2 adresses emails dans la meme ligne (pas variable)
//=================================================================================================
public class InterpreterData {
  // Constantes
  private static final String LIB = "LIGNE";
  private static final String DATE = "DATE";
  private static final String HEURE = "HEURE";
  /*
  public static final int AAAAMMJJ=0;
  public static final int AAMMJJ=1;
  public static final int JJMMAAAA=2;
  public static final int JJMMAA=3;
  public static final int HHMMSS=4;
  public static final int HH_MM_SS=5;
  */
  // Variables
  private char expression[] = null;
  private char tabResultat[] = null;
  private char tabNomVariable[] = null;
  private char tabParam[] = null;
  private char tabW[] = null;
  private String[] donnees = null;
  
  private String msgErreur = null;
  
  /**
   * Constructeur de la classe
   */
  public InterpreterData() {
    tabResultat = new char[Constantes.TAILLE_BUFFER];
    tabNomVariable = new char[Constantes.TAILLE_BUFFER];
    tabParam = new char[Constantes.TAILLE_BUFFER];
    tabW = new char[Constantes.TAILLE_BUFFER];
  }
  
  /**
   * @return the donnees
   */
  public String[] getDonnees() {
    return donnees;
  }
  
  /**
   * @param donnees
   *          the donnees to set
   */
  public void setDonnees(StringBuffer donnees) {
    this.donnees = donnees.toString().split("\n");
  }
  
  /**
   * Analyse de l'expression
   * 
   * @param valeur
   * @return
   */
  public int analyseExpressionBrut(String valeur) {
    String expressionOri = null;
    String nomVariable = null;
    String valeurVariable = null;
    int surVar = Constantes.FALSE, surParam = Constantes.FALSE;
    int i = 0, j = 0, k = 0;
    
    // System.out.println("[iData] (analyseExpression) listeData=" +
    // listeData.size());
    // System.out.println("[iData] (analyseExpression) : " +valeur);
    
    if (valeur == null)
      return -2;
      
    // On convertie les caract�res unicodes et
    // System.out.println("(iData) Expression = " + valeur);
    // Est ce bien une variable panel dans le cas contraire on renvoi la
    // valeur telle quelle
    // expressionOri = Constantes.code2unicode(valeur.trim());
    expressionOri = valeur; // valeur.trim();
    if (expressionOri.indexOf('@') == -1)
      return -1;// expressionOri;
      
    // On v�rifie que l'expression n'est pas d�j� �t� trait�
    // Source de bug quand on reste sur le meme format mais qu'il y a des
    // maj de donn�e
    // style format en erreur le V03F reste � blanc car dej� trait�
    // valeurVariable = ((String)listeDataPanel.get(expressionOri));
    // if (valeurVariable != null) return valeurVariable;
    
    // Sinon
    expression = new char[expressionOri.length()];
    expressionOri.getChars(0, expressionOri.length(), expression, 0);
    
    for (i = 0; i < expression.length; i++) {
      // On teste le d�but ou la fin d'une variable
      if (expression[i] == '@')
        if (surVar == Constantes.FALSE) {
          surVar = Constantes.TRUE;
          continue;
        }
        else {
          if (surParam == Constantes.TRUE) {
            surParam = Constantes.FALSE;
            valeur = analyseVariable(tabParam, j - 1, valeurVariable);
            // System.out.println("(iData) analyseVariable(" +
            // tabParam.length + "," + (j-1) + "," + valeurVariable
            // +") valeur=" + valeur );
            if (valeur != null) {
              // valeur = valeur.trim();
              valeur.getChars(0, valeur.length(), tabResultat, k);
              k = k + valeur.length();
            }
          }
          else {
            nomVariable = new String(tabNomVariable, 0, j);
            // System.out.println("(iData) (analyseVariable) nomVariable="
            // + nomVariable);
            valeurVariable = getValeur(nomVariable);
            // System.out.println("[iData] (analyseExpressionWOTrim) 2 : "
            // +expressionOri+ "=" + valeurVariable+"|");
            if (valeurVariable != null) {
              // valeurVariable = valeurVariable.trim();
              valeurVariable.getChars(0, valeurVariable.length(), tabResultat, k);
              k = k + valeurVariable.length();
            }
          }
          j = 0;
          surVar = Constantes.FALSE;
          // System.out.println("(iData) (analyseVariable) nomVariable="
          // + nomVariable) ;
          continue;
        }
      else
      // On teste la fin d'une variable
      if ((expression[i] == '/') && (surParam == Constantes.FALSE)) {
        // System.out.println("[iData] (analyseExpression) surVar="
        // + surVar);
        if (surVar == Constantes.TRUE) {
          surParam = Constantes.TRUE;
          nomVariable = new String(tabNomVariable, 0, j);
          // System.out.println("(iData) (analyseVariable) expression[i]="
          // + expression[i]);
          valeurVariable = getValeur(nomVariable);
          // System.out.println("[iData] (analyseExpressionWOTrim) 1 : "
          // +expressionOri+ "=" + valeurVariable+"|");
          j = 0;
          continue;
        }
      }
      
      if (surVar == Constantes.TRUE)
        if (surParam == Constantes.FALSE)
          tabNomVariable[j++] = expression[i];
        else
          tabParam[j++] = expression[i];
      else
        tabResultat[k++] = expression[i];
    }
    
    // valeurVariable = new String(tabResultat, 0, k);
    // System.out.println("[iData] (analyseExpression) : " +expressionOri+
    // Cas d'une adresse email
    if (surVar == Constantes.TRUE) {
      tabResultat[k++] = '@';
      System.arraycopy(tabNomVariable, 0, tabResultat, k, j);
      return j + k;
    }
    // System.out.println("[iData] (analyseExpression) : " + surVar + " " + surParam);
    // System.out.println("[iData] (analyseExpression) : " + new String(tabResultat));
    // System.out.println("[iData] (analyseExpression) : " + new String(expression));
    // System.out.println("[iData] (analyseExpression) : " + new String(tabNomVariable));
    // "=" + valeurVariable+"|");
    return k;
  }
  
  /**
   * Analyse de l'expression sans trim
   * 
   * @param valeur
   * @return
   */
  public String analyseExpressionWOTrim(String valeur) {
    int lg = analyseExpressionBrut(valeur);
    // On ne touche � rien car pas de @ d�tect�
    if (lg == -1)
      return valeur;
    else if (lg == -2)
      return null;
    else
      return new String(tabResultat, 0, lg);
  }
  
  /**
   * Analyse de l'expression avec trim � droite
   * TODO voir le analyseExpressionTrimR merde apparemment enleve pas les blancs � droite (voir dans addVariables de GenerationFichierFO)
   * @param valeur
   * @return
   */
  public String analyseExpressionTrimR(String valeur) {
    int lg = analyseExpressionBrut(valeur);
    // On ne touche � rien car pas de @ d�tect�
    if (lg == -1)
      return valeur;
    else if (lg == -2)
      return null;
    else {
      int i = 0;
      for (i = tabResultat.length - 1; i > 0; i--)
        if ((tabResultat[i] != ' ') || (tabResultat[i] != '\t'))
          break;
      return new String(tabResultat, 0, i);
    }
  }
  
  /**
   * Analyse de l'expression avec trim � droite et � gauche
   * 
   * @param valeur
   * @return
   */
  public String analyseExpression(String valeur) {
    int lg = analyseExpressionBrut(valeur);
    if (lg == -1)
      return valeur.trim();
    else if (lg == -2)
      return null;
    else
      return new String(tabResultat, 0, lg).trim();
  }
  
  /**
   * Analyse de la variable
   * 
   * @param tabparam
   * @param indice
   * @param valeur
   * @return
   */
  public String analyseVariable(char[] tabparam, int indice, String valeur) {
    int i = 0, j = 0, occ = 0, occabs, debpos = 0, finpos = 0, pos = 0;
    int trouve = Constantes.FALSE;
    char element;
    boolean trim = false;
    String chaine = null;
    
    if (valeur == null)
      return null;
    
    // On r�cup�re le caract�re s�parateur
    element = tabparam[indice];
    tabparam[indice] = ' ';
    trim = element == '\u00a8'; // Comme ^ mais en plus on trime la valeur trouv�
    if (trim)
      element = '^';
    indice--;
    
    // On teste la valeur d'�l�ment car cela d�termine la suite des op�
    if (element == '^') // Variable de type @LD/0,30^@
    {
      chaine = new String(tabparam, 0, indice + 1).trim();
      occ = chaine.indexOf(',');
      debpos = Integer.parseInt(chaine.substring(0, occ));
      // System.out.print("--> " + chaine + "|"+ occ + "|" +
      // chaine.substring(0, occ) + "|" + chaine.substring(occ+1)+"|");
      finpos = debpos + Integer.parseInt(chaine.substring(occ + 1));
      // System.out.print("--> " + debpos + " " + finpos);
    }
    /*	else
    		// Date ou heure
    		if (tabparam[0] == '_')
    		{
    			//System.out.println("[iData] (analyseVariable) tabparam=" + new String(tabparam));
    			return getFormateDateHeure(new String(tabparam, 0, indice+1));
    //System.out.println("[iData] (analyseVariable) valeur=" + valeur);
    		}*/
    else // Variable de type @V07F/+1=@
    {
      if (tabparam[0] == '+')
        debpos = 1;
      else
        indice++;
        
      // System.out.println("Param�tre |" + new String(tabparam, debpos,
      // indice) + "|");
      
      // On r�cup�re l'occurence du s�parateur
      occ = Integer.parseInt(new String(tabparam, debpos, indice));
      occabs = Math.abs(occ);
      
      // On cherche dans la chaine
      // System.out.println("--> "+ valeur);
      valeur.getChars(0, valeur.length(), tabW, 0);
      j = 0;
      debpos = 0;
      finpos = 0;
      // On recherche le marqueur
      for (i = 0; (i < valeur.length()) && (trouve == Constantes.FALSE); i++) {
        if (tabW[i] == element)
          j++;
        if (j == occabs) {
          if (occ > 0) {
            debpos = i + 1;
            finpos = valeur.length();
          }
          else {
            debpos = 0;
            finpos = i;
          }
          trouve = Constantes.TRUE;
        }
      }
      
      // On recup�re la partie qui nous interresse
      if (occ > 0) {
        pos = finpos;
        while (i < pos) // � droite du symbole
        {
          // System.out.println("len="+tabW.length
          // +" i="+i+" finpos="+finpos+" tabw[i]="+tabW[i]);
          if (tabW[i] == ' ')
            finpos = i;
          else if (i == (pos - 1))
            finpos = i + 1;
          if (tabW[i] == element)
            break;
          /*
           * Sp�cifique pour les F1=... F2=... if (tabW[i] == 'F') if
           * ( ((i+1) < finpos) && ( (tabW[i+1] == '0') || (tabW[i+1]
           * == '1') || (tabW[i+1] == '2') || (tabW[i+1] == '3') ||
           * (tabW[i+1] == '4') || (tabW[i+1] == '5') || (tabW[i+1] ==
           * '6') || (tabW[i+1] == '7') || (tabW[i+1] == '8') ||
           * (tabW[i+1] == '9'))) finpos = i; if (tabW[i] == element)
           * break;
           */
          i++;
        }
      }
      else
        while (i > 0) // � gauche du symbole
        {
          if (tabW[i] == ' ') {
            debpos = i;
            break;
          }
          i--;
        }
    }
    
    // System.out.println("[iData] (analyseVariable) |" + valeur+"|" +
    // valeur.length()+"|"+debpos+"|"+finpos);
    if (valeur.length() < debpos)
      return "";
    else if (valeur.length() < finpos)
      if (trim)
        return valeur.substring(debpos).trim();
      else
        return valeur.substring(debpos);
    else if (trim)
      return valeur.substring(debpos, finpos).trim();
    else
      return valeur.substring(debpos, finpos);
  }
  
  /**
   * Retourne la valeur de la variable
   * 
   * @param nomVar
   * @return
   */
  private String getValeur(String nomVar) {
    // System.out.println("[iData] (getValeur) nomVar=" + nomVar);
    if (nomVar == null)
      return null;
    // if (nomVar.startsWith(DATE)) return "";
    // if (nomVar.startsWith(HEURE)) return "";
    if (!nomVar.startsWith(LIB))
      return nomVar;
    
    if (donnees == null)
      return "";
    int numeroLigne = Integer.parseInt(nomVar.replaceAll(LIB, "").trim()) - 1;
    if (numeroLigne >= donnees.length)
      return "";
    // System.out.println("[iData] (getValeur) numeroLigne=" + numeroLigne + " " + donnees.length);
    return donnees[numeroLigne];
  }
  
  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    String chaine;
    
    // La r�cup�ration du message est � usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
