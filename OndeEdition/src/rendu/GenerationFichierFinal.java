/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rendu;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URI;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopConfParser;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FopFactoryBuilder;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

import ri.seriem.libcommun.outils.LogMessage;

public class GenerationFichierFinal {
  // Variables
  private String typeSortie = MimeConstants.MIME_PDF;
  private String extension = "pdf";
  private String fichierEntree = "output.fo";
  private String fichierSortie = null;
  private String configFile = "cfg.xml"; // Fichier cfg.xml contenant la config pour la transformation
  private LogMessage log = null;
  
  /**
   * Constructeur
   * @param alog
   */
  public GenerationFichierFinal() {
  }
  
  /**
   * Constructeur
   * @param alog
   */
  public GenerationFichierFinal(LogMessage alog) {
    log = alog;
  }
  
  /**
   * @return the typeSortie
   */
  public String getTypeSortie() {
    return typeSortie;
  }
  
  /**
   * @param typeSortie the typeSortie to set
   */
  public void setTypeSortie(String pExtensionFichier) {
    if (pExtensionFichier == null || pExtensionFichier.trim().isEmpty()) {
      return;
    }
    
    extension = pExtensionFichier.toLowerCase();
    if (extension.equals("awt"))
      typeSortie = MimeConstants.MIME_FOP_AWT_PREVIEW;
    else if (extension.equals("pdf"))
      typeSortie = MimeConstants.MIME_PDF;
    else if (extension.equals("pcl"))
      typeSortie = MimeConstants.MIME_PCL;
    else if (extension.equals("ps"))
      typeSortie = MimeConstants.MIME_POSTSCRIPT;
    else if (extension.equals("tiff"))
      typeSortie = MimeConstants.MIME_TIFF;
  }
  
  /**
   * @return the fichierEntree
   */
  public String getFichierEntree() {
    return fichierEntree;
  }
  
  /**
   * @param fichierEntree the fichierEntree to set
   */
  public void setFichierEntree(String fichierEntree) {
    // System.out.println("-setFichierEntree-> " + fichierEntree);
    this.fichierEntree = fichierEntree;
  }
  
  /**
   * @return the fichierSortie
   */
  public String getFichierSortie() {
    return fichierSortie;
  }
  
  /**
   * @param fichierSortie the fichierSortie to set
   *          Note: il est pr�ferable d'ex�cuter setTypeSortie avant cette m�thode
   */
  public void setFichierSortie(String fichiersortie) {
    if ((fichiersortie == null) || (fichiersortie.trim().equals("")))
      return;
    fichierSortie = fichiersortie.trim();
    
    // On v�rifie qu'il n'y est pas d�j� l'extension
    if (!fichierSortie.toLowerCase().endsWith(extension))
      fichierSortie += '.' + extension;
  }
  
  /**
   * Retourne le fichier de configuration (TODO arriver � lire le fichier cfg.xml du jar)
   * @param fichier
   * @return
   * @throws ConfigurationException
   * @throws SAXException
   * @throws IOException
   */
  /*
  private Configuration getConfigurationFile(String fichier) throws ConfigurationException, SAXException, IOException {
    File cfg = null;
    
    DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
    if (fichier == null) {
      cfg = new File("cfg.xml");
      // System.out.println("-rendufinal/getConfigurationFile-> " + cfg.getAbsolutePath());
      if (cfg.exists())
        return cfgBuilder.buildFromFile(cfg);
    }
    // System.out.println("-rendufinal-> " + fichier);
    cfg = new File(fichier);
    if (cfg.exists())
      return cfgBuilder.buildFromFile(new File(fichier));
    else {
      cfg = new File(fichier.replaceAll("sim", "lib"));
      // System.out.println("-rendufinal/getConfigurationFile-> " + cfg.getAbsolutePath());
      if (cfg.exists())
        return cfgBuilder.buildFromFile(cfg);
    }
    return null;
  }*/
  
  /**
   * Retourne le fichier de configuration.
   * @param pCheminFichierConfig
   * @return
   * @throws ConfigurationException
   * @throws SAXException
   * @throws IOException
   */
  private FopFactoryBuilder getConfigurationFile(String pCheminFichierConfig) {
    try {
      File cfg = null;
      if (pCheminFichierConfig == null) {
        cfg = new File("cfg.xml");
        if (cfg.exists()) {
          FopConfParser parser = new FopConfParser(cfg);
          // Trace.alerte("-1-" + cfg);
          return parser.getFopFactoryBuilder();
        }
      }
      else {
        cfg = new File(pCheminFichierConfig);
        if (cfg.exists()) {
          FopConfParser parser = new FopConfParser(cfg);
          // Trace.alerte("-2-" + cfg);
          return parser.getFopFactoryBuilder();
        }
        else {
          cfg = new File(pCheminFichierConfig.replaceAll("sim", "lib"));
          if (cfg.exists()) {
            FopConfParser parser = new FopConfParser(cfg);
            // Trace.alerte("-3-" + cfg);
            return parser.getFopFactoryBuilder();
          }
          else {
            Configuration configuration = (Configuration) getClass().getClassLoader().getResourceAsStream("/cfg/" + configFile);
            // Trace.alerte("-4-" + configuration);
            return new FopFactoryBuilder(URI.create("/")).setConfiguration(configuration);
          }
        }
      }
    }
    catch (Exception e) {
    }
    return null;
  }
  
  /**
   * Conversion du document
   * @return
   */
  /*
  public String renduFinal(String dossiertravail, String dataFO, float dpisrc, float dpidest) {
    if ((dataFO == null) || (dossiertravail == null))
      return null;
    
    OutputStream out = null;
    File dossierSim = new File(dossiertravail);
    
    // Step 1: Construct a FopFactory
    // (reuse if you plan to render multiple documents!)
    FopFactory fopFactory = FopFactory.newInstance();
    
    // Step 2: Set up output stream.
    // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
    try {
      if (fichierSortie == null)
        fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + extension);
      // System.out.println("--> " + fichierSortie + " " + fichierEntree);
      out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
      
      Configuration cfg = getConfigurationFile(dossierSim.getParent() + File.separatorChar + configFile);
      // System.out.println("-rendufinal-> " + dossierSim.getParent() + File.separatorChar + configFile + " " + cfg);
      if (cfg != null)
        fopFactory.setUserConfig(cfg);
      
      // Step 3: Construct fop with desired output format
      fopFactory.setBaseURL(dossiertravail);
      fopFactory.setSourceResolution(dpisrc);
      fopFactory.setTargetResolution(dpidest);
      
      Fop fop = fopFactory.newFop(typeSortie, out);
      // Fop fop = fopFactory.newFop(typeSortie, userAgent, out);
      
      // Step 4: Setup JAXP using identity transformer
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(); // identity transformer
      
      // Step 5: Setup input and output for XSLT transformation
      // Setup input stream
      Source src = new StreamSource(new StringReader(dataFO));
      
      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result res = new SAXResult(fop.getDefaultHandler());
      
      // Step 6: Start XSLT transformation and FOP processing
      transformer.transform(src, res);
      out.close();
    }
    catch (Exception e) {
      if (log != null)
        log.ecritureMessage("-[GenerationFichierFinal]-(renduFinal)-> Erreur : " + e);
      e.printStackTrace();
      return null;
    }
    
    return fichierSortie;
  }*/
  
  /**
   * Conversion du document depuis le serveur.
   * @return
   */
  public String renduFinal(String dossiertravail, String dataFO, float dpisrc, float dpidest) {
    if (dataFO == null || dossiertravail == null) {
      return null;
    }
    
    OutputStream out = null;
    // File dossierSim = new File(dossiertravail);
    
    // Step 1: Construct a FopFactory
    // (reuse if you plan to render multiple documents!)
    // FopFactory fopFactory = FopFactory.newInstance();
    
    // Step 2: Set up output stream.
    // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
    try {
      if (fichierSortie == null) {
        fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + extension);
      }
      out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
      
      // Configuration cfg = getConfigurationFile(dossierSim.getParent() + File.separatorChar + configFile);
      // if (cfg != null) fopFactory.setUserConfig(cfg);
      
      // Step 3: Construct fop with desired output format
      // fopFactory.setBaseURL(dossiertravail);
      // fopFactory.setSourceResolution(dpisrc);
      // fopFactory.setTargetResolution(dpidest);
      
      // TODO attention crash si le dossiertravail ne contient rien
      FopFactoryBuilder builder = getConfigurationFile(dossiertravail + File.separatorChar + configFile);
      builder.setBaseURI(new File(dossiertravail).toURI());
      builder.setSourceResolution(dpisrc);
      builder.setTargetResolution(dpidest);
      FopFactory fopFactory = builder.build();
      
      Fop fop = fopFactory.newFop(typeSortie, out);
      // Fop fop = fopFactory.newFop(typeSortie, userAgent, out);
      
      // Step 4: Setup JAXP using identity transformer
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(); // identity transformer
      
      // Step 5: Setup input and output for XSLT transformation
      // Setup input stream
      Source src = new StreamSource(new StringReader(dataFO));
      
      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result res = new SAXResult(fop.getDefaultHandler());
      
      // Step 6: Start XSLT transformation and FOP processing
      transformer.transform(src, res);
      out.close();
    }
    catch (Exception e) {
      log.ecritureMessage("-[GenerationFichierFinal]-(renduFinal)-> Erreur lors de l'appel de la m�thode renduFinal : " + e);
      return null;
    }
    
    return fichierSortie;
  }
  
  /**
   * Conversion du document depuis le rad.
   * @return
   */
  public String renduFinal(float dpisrc, float dpidest) {
    if (fichierEntree == null) {
      return null;
    }
    
    OutputStream out = null;
    File entree = new File(fichierEntree);
    
    // Step 1: Construct a FopFactory
    // (reuse if you plan to render multiple documents!)
    // FopFactory fopFactory = FopFactory.newInstance(new File(entree.getParent()).toURI()); // Attention avec la
    // version fop-1.1rc1 cela ne fonctionne plus
    
    // Step 2: Set up output stream.
    // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
    try {
      if (fichierSortie == null) {
        fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + extension);
      }
      out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
      
      /*
      URL url = getClass().getResource("/cfg/" + configFile);
      String chemin = url.getFile();
      File cfg = new File(chemin);
      if (!cfg.exists()) {
        if (chemin.contains(".jar!")) {
          cfg = new File(configFile);
        }
        else {
          chemin = chemin.substring(6);
          cfg = new File(chemin);
        }
      }
      FopConfParser parser = new FopConfParser(cfg);
      FopFactoryBuilder builder = parser.getFopFactoryBuilder();*/
      FopFactoryBuilder builder = getConfigurationFile("/cfg/" + configFile);
      builder.setBaseURI(new File(entree.getParent()).toURI());
      builder.setSourceResolution(dpisrc);
      builder.setTargetResolution(dpidest);
      FopFactory fopFactory = builder.build();
      
      // Step 3: Construct fop with desired output format
      // fopFactory.setBaseURL(entree.getParent());
      // fopFactory.setSourceResolution(dpisrc);
      // fopFactory.setTargetResolution(dpidest);
      Fop fop = fopFactory.newFop(typeSortie, out);
      
      // Step 4: Setup JAXP using identity transformer
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(); // identity transformer
      
      // Step 5: Setup input and output for XSLT transformation
      // Setup input stream
      Source src = new StreamSource(entree);
      
      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result res = new SAXResult(fop.getDefaultHandler());
      
      // Step 6: Start XSLT transformation and FOP processing
      transformer.transform(src, res);
      out.close();
    }
    catch (Exception e) {
      log.ecritureMessage("-[GenerationFichierFinal]-(renduFinal)-> Erreur lors de l'appel de la m�thode renduFinal : " + e);
      return null;
    }
    
    return fichierSortie;
  }
  
  /**
   * Conversion du document
   * @return
   */
  /*
  public String renduFinal(float dpisrc, float dpidest) {
  if (fichierEntree == null)
    return null;
  
  OutputStream out = null;
  File entree = new File(fichierEntree);
  
  // Step 1: Construct a FopFactory
  // (reuse if you plan to render multiple documents!)
  FopFactory fopFactory = FopFactory.newInstance(); // Attention avec la version fop-1.1rc1 cela ne fonctionne plus
  
  // Step 2: Set up output stream.
  // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
  try {
    // System.out.println("--> " + fichierEntree);
    if (fichierSortie == null)
      fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + extension);
    out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
    
    Configuration cfg = getConfigurationFile(configFile);
    if (cfg != null)
      fopFactory.setUserConfig(cfg);
      
    // Step 3: Construct fop with desired output format
    // System.out.println("-baseUrl-> " + entree.getParent());
    fopFactory.setBaseURL(entree.getParent());
    fopFactory.setSourceResolution(dpisrc);
    fopFactory.setTargetResolution(dpidest);
    // System.out.println("--> " + dpisrc + " " + dpidest);
    Fop fop = fopFactory.newFop(typeSortie, out);
    
    // Step 4: Setup JAXP using identity transformer
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(); // identity transformer
    
    // Step 5: Setup input and output for XSLT transformation
    // Setup input stream
    Source src = new StreamSource(entree);
    
    // Resulting SAX events (the generated FO) must be piped through to FOP
    Result res = new SAXResult(fop.getDefaultHandler());
    
    // Step 6: Start XSLT transformation and FOP processing
    transformer.transform(src, res);
    out.close();
  }
  catch (Exception e) {
    System.out.println("Erreur " + e);
    e.printStackTrace();
    return null;
  }
  
  return fichierSortie;
  }*/
}
