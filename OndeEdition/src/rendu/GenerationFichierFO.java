/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rendu;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import description.DescriptionCodeBarre;
import description.DescriptionCondition;
import description.DescriptionDocument;
import description.DescriptionImage;
import description.DescriptionLabel;
import description.DescriptionPage;
import description.DescriptionVariable;
import ri.seriem.libcommun.edition.ConstantesNewSim;
import ri.seriem.libcommun.edition.Spool;
import ri.seriem.libcommun.outils.Constantes;
import ri.seriem.libcommun.outils.DateHeure;
import ri.seriem.libcommun.outils.GestionFichierTexte;

//=================================================================================================
//==> G�n�re un fichier FO � partir d'une description de document (.dds)
//=================================================================================================
public class GenerationFichierFO {
  // Constantes
  private static final String NOIMAGE = null;
  private static final String NONAME = "spool";
  private static final String EXTENSION = ".fo";
  private static final String PREFIXE_PAGE_MASTER = "A4-";
  
  public static final double HAUTEUR = ConstantesNewSim.A4_HEIGHT;
  public static final double LARGEUR = ConstantesNewSim.A4_WIDTH;
  public static final int HAUTEUR_AVEC_1CM_MARGE = 784; // Avec 1 cm de marge de chaque c�t� (donc 2 cm au total)
  public static final int LARGEUR_AVEC_1CM_MARGE = 537; // Avec 1 cm de marge de chaque c�t� (donc 2 cm au total)
  
  // Variables
  private String encoding = System.getProperty("file.encoding");
  // private boolean isWindows = System.getProperty("os.name").startsWith("Windows");
  private DescriptionDocument document = null;
  private String[] listing = null;
  private Spool spool = null;
  private ArrayList<String> docfo = new ArrayList<String>();
  private double hauteurCm = ConstantesNewSim.A4_HEIGHT, largeurCm = ConstantesNewSim.A4_WIDTH;
  private Font police = null;
  private String dossierTravail = "";
  private InterpreterData id = new InterpreterData();
  private String nomFichierFO = null;
  private boolean affineLigneColonne = false;
  
  // private int compteur=0;
  
  private HashMap<String, DescriptionCondition> listeConditions = new HashMap<String, DescriptionCondition>();
  private ArrayList<DescriptionVariable> listeVariables = null;
  private ArrayList<DescriptionPage> listeDescriptionPage = new ArrayList<DescriptionPage>();
  
  // A recalculer car sous AS/400 les polices sont un peu plus grandes (calculer avec le programme test26)
  // private int[][] taillepolicewh_MACOSX={{2, 4}, {3, 6}, {4, 7}, {4, 8}, {5, 8}, {5, 10}, {6, 11}, {7, 12}, {7, 12}};
  // private int[][] taillepolicewh_LINUX={{2, 5}, {3, 7}, {4, 8}, {4, 9}, {5, 10}, {5, 12}, {6, 13}, {7, 14}, {7, 15}};
  private int[][] taillepolicewh_WINDOWS =
      { { 2, 6 }, { 3, 7 }, { 4, 8 }, { 4, 10 }, { 5, 11 }, { 5, 12 }, { 6, 14 }, { 7, 16 }, { 7, 17 } };
  
  /**
   * Constructeur
   * @param doc
   */
  public GenerationFichierFO(DescriptionDocument doc, String[] spool) {
    this(doc, spool, "");
  }
  
  /**
   * Constructeur
   * @param doc
   */
  public GenerationFichierFO(DescriptionDocument doc, String[] listing, String dossierTrav) {
    // System.out.println("--> " + spool.length);
    setDocument(doc);
    setListing(listing);
    setDossierTravail(dossierTrav);
    docfo.clear();
  }
  
  /**
   * Constructeur dans le cas o� il n'y a pas de description
   * @param doc
   *
   *          public GenerationFichierFO(int nbrligne, int nbrcolonne, String[] spool)
   *          {
   *          this(nbrligne, nbrcolonne, spool, "", false);
   *          }
   */
  
  /**
   * Constructeur dans le cas o� il n'y a pas de description
   * @param doc
   */
  // public GenerationFichierFO(int nbrligne, int nbrcolonne, String[] spool, String dossierTrav, boolean affinecol)
  public GenerationFichierFO(Spool spool, String dossierTrav, boolean affinecol) {
    setSpool(spool);
    setListing(spool.getPages());
    setDossierTravail(dossierTrav);
    setAffineCalculLigneColonne(true);
    if (spool.isPortrait())
      setDimension(HAUTEUR, LARGEUR);
    else
      setDimension(LARGEUR, HAUTEUR);
    
    // En fonction du nombre de caract�res par ligne/colonne on calibre la page
    if (police == null)
      // police = new Font("Courier", Font.PLAIN, getTaillePolice(spool.getNbrLignes(), spool.getNbrColonnes(), spool.isPortrait()));
      police = new Font("Monospaced", Font.PLAIN, getTaillePolice(spool.getNbrLignes(), spool.getNbrColonnes(), spool.isPortrait()));
    docfo.clear();
  }
  
  /**
   * Initialise la variable qui indique si on doit affiner les colonnes (largeur page)
   * @param affinecol
   */
  public void setAffineCalculLigneColonne(boolean affine) {
    affineLigneColonne = affine;
  }
  
  /**
   * Recherche au mieux la ligne maximum
   * @param nbrcol
   * @return
   */
  private int getAffineLigne(int nbrlig) {
    if (listing == null)
      return nbrlig;
    
    String[] ligne = null;
    nbrlig = 0;
    for (int i = 0; i < listing.length; i++) {
      ligne = listing[i].split(Constantes.crlf);
      if (nbrlig < ligne.length)
        nbrlig = ligne.length;
    }
    // System.out.println("--> nbrlig:" + nbrlig);
    return nbrlig;
  }
  
  /**
   * Recherche au mieux la colonne maximum (tout en faisant un trim des blancs � droite)
   * @param nbrcol
   * @return
   */
  private int getAffineColonne(int nbrcol) {
    if (listing == null)
      return nbrcol;
    
    int val = 0;
    String[] lignes = null;
    StringBuffer newpage = new StringBuffer();
    String chaine;
    nbrcol = 0;
    for (int i = 0; i < listing.length; i++) {
      newpage.setLength(0);
      lignes = listing[i].split(Constantes.crlf);
      for (int j = 0; j < lignes.length; j++) {
        chaine = getTrimR(lignes[j]);
        newpage.append(chaine).append(Constantes.crlf);
        val = chaine.length();
        if (nbrcol < val)
          nbrcol = val;
      }
      listing[i] = newpage.toString();
    }
    // System.out.println("--> nbrcol:" + nbrcol);
    return nbrcol;
  }
  
  /**
   * Evalue la taille de la police � utiliser et l'orientation portrait/paysage (dans le cas o� il n'y a pas de description)
   * TODO � revoir car taille police un peu trop petite ou orientation ne correspond pas par ex avec les bons
   * @param nbrligne
   * @param nbrcolonne
   * @return
   */
  private int getTaillePolice(int nbrligne, int nbrcolonne, boolean isportrait) {
    double taille = 0;
    
    // System.out.println("-getTaillePolice-> c=" + nbrcolonne + " l=" + nbrligne + " " + affineLigneColonne);
    if (affineLigneColonne) {
      nbrligne = getAffineLigne(nbrligne);
      nbrcolonne = getAffineColonne(nbrcolonne);
    }
    // System.out.println("-getTaillePolice-> c=" + nbrcolonne + " l=" + nbrligne);
    
    double taille_h = 0;
    double taille_l = 0;
    int deltaportrait = 4; // Valeur pifom�trique qui permet de faire rentrer un spool
    int deltapaysage = 4; // Valeur pifom�trique qui permet de faire rentrer un spool
    
    // Calcul de la taille d'un caract�re avec la police "Monospaced"
    
    for (int i = 0; i < taillepolicewh_WINDOWS.length; i++) {
      taille_l = nbrcolonne * taillepolicewh_WINDOWS[i][0];
      taille_h = nbrligne * taillepolicewh_WINDOWS[i][1];
      
      // System.out.println("--> taille_l:" + taille_l + " taille_h:" + taille_h + " portrait:" + isportrait);
      if (isportrait && (taille_l <= LARGEUR_AVEC_1CM_MARGE) && (taille_h <= HAUTEUR_AVEC_1CM_MARGE))
        taille = i + deltaportrait;
      else if (!isportrait && (taille_h <= LARGEUR_AVEC_1CM_MARGE) && (taille_l <= HAUTEUR_AVEC_1CM_MARGE))
        taille = i + deltapaysage;
      else
        break;
    }
    // System.out.println("--> taille:" + taille);
    /*
    if (isportrait)
    {
    	taille_h = Math.ceil((HAUTEUR_AVEC_1CM_MARGE / nbrligne));
    	taille_l = Math.ceil((LARGEUR_AVEC_1CM_MARGE / nbrcolonne));
    //System.out.println("--> " + taille_l + " " + taille_h);
    	taille = Math.ceil(taille_l + ((taille_h - taille_l) / 2)); // Algo pifometrique 
    	//taille = (taille_h > taille_l)?taille_l:taille_h;
    }
    else
    {
    	taille_h = Math.ceil((LARGEUR_AVEC_1CM_MARGE / nbrligne));  
    	taille_l = Math.ceil((HAUTEUR_AVEC_1CM_MARGE / nbrcolonne));
    	//taille = (taille_h < taille_l)?taille_l:taille_h;
    //System.out.println("--> " + taille_l + " " + taille_h);
    	taille = Math.ceil(taille_h - ((taille_h - taille_l) / 2)); // Algo pifometrique 
    }
    System.out.println("--> " + (int)taille);
    */
    // System.out.println("--> " + taille);
    return (int) taille;
  }
  
  /**
   * @return the document
   */
  public DescriptionDocument getDocument() {
    return document;
  }
  
  /**
   * @param document the document to set
   */
  public void setDocument(DescriptionDocument document) {
    this.document = document;
    analyseDocument();
  }
  
  /**
   * @param spool the spool to set
   */
  public void setListing(String[] listing) {
    this.listing = listing;
  }
  
  /**
   * @param spool the spool to set
   */
  public void setSpool(Spool spool) {
    this.spool = spool;
  }
  
  /**
   * @return the spool
   */
  public Spool getSpool() {
    return spool;
  }
  
  /**
   * @param dossierTravail the dossierTravail to set
   */
  public void setDossierTravail(String dossierTravail) {
    this.dossierTravail = dossierTravail;
  }
  
  /**
   * @return the dossierTravail
   */
  public String getDossierTravail() {
    if (dossierTravail == null)
      return "";
    return dossierTravail;
  }
  
  /**
   * @param nomFichierFO the nomFichierFO to set
   */
  public void setNomFichierFO(String nomFichierFO) {
    this.nomFichierFO = nomFichierFO;
  }
  
  /**
   * @return the nomFichierFO
   */
  public String getNomFichierFO() {
    return nomFichierFO;
  }
  
  /**
   * G�n�re le fichier FO
   * @return
   */
  public String genereFOtoString() {
    // if (document == null) return null;
    if (listing == null)
      return null;
    boolean retour = true;
    
    retour = addEnteteFichier();
    if (retour)
      retour = addEnteteDocument();
    if (retour)
      retour = addCorpsDocument(listing);
    if (retour)
      retour = addPiedFichier();
    
    StringBuffer data = new StringBuffer(Constantes.TAILLE_BUFFER);
    for (int i = 0; i < docfo.size(); i++)
      data.append(docfo.get(i));
    
    return data.toString();
  }
  
  /**
   * G�n�re le fichier FO
   * @return
   */
  public boolean genereFOtoFile() {
    // if (document == null) return false;
    // System.out.println("-genereFOtoFile()-> " + spool);
    if (listing == null)
      return false;
    boolean retour = true;
    String nom = NONAME;
    
    retour = addEnteteFichier();
    if (retour)
      retour = addEnteteDocument();
    if (retour)
      retour = addCorpsDocument(listing);
    if (retour)
      retour = addPiedFichier();
      
    // for (int i=0; i<docfo.size(); i++)
    // System.out.println(docfo.get(i));
    if ((document != null) && (document.getNom() != null))
      nom = document.getNom();
    
    nomFichierFO = getDossierTravail() + File.separatorChar + nom.trim() + EXTENSION;
    // System.out.println("-genereFOtoFile()-> " + nomFichierFO);
    if (retour)
      retour = ecrireFichier(nomFichierFO);
    return retour;
  }
  
  /**
   * Ecriture du fichier FO
   * @param fichier
   * @return
   */
  private boolean ecrireFichier(String fichier) {
    if (fichier == null)
      return false;
    
    GestionFichierTexte gft = new GestionFichierTexte(fichier);
    gft.setContenuFichier(docfo);
    int ret = gft.ecritureFichier();
    
    return ret == 0;
  }
  
  /**
   * Initialise les dimensions du document
   * @param largeur
   * @param hauteur
   */
  private void setDimension(double hauteur, double largeur) {
    this.hauteurCm = hauteur;
    this.largeurCm = largeur;
  }
  
  /**
   * R�cup�re les informations d'une page
   * @param page
   */
  private void recupInfosPage(DescriptionPage page) {
    if (page == null)
      return;
      
    // TODO A revoir ambiguit� entre dim cm et px
    // if ((page.getHauteur() == 842) && (page.getLargeur() == 595))
    setDimension(hauteurCm, largeurCm);
    // else
    // setDimension(largeurCm, hauteurCm);
    
    // if (!images.contains(page.getImage()))
    // images.add(page.getImage());
  }
  
  /**
   * Analyse le document (dimension, nombre de fond de page diff�rent)
   */
  private void analyseDocument() {
    // Si pas de description
    if (document == null)
      return;
      
    // Sinon
    // On parcourt le document (TODO A voir car c'est le dernier qui gagne)
    recupInfosPage(document.getPremiereDeCouverture());
    recupInfosPage(document.getDeuxiemeDeCouverture());
    if (document.getSequence() != null)
      for (int i = 0; i < document.getSequence().size(); i++)
        recupInfosPage((DescriptionPage) document.getSequence().get(i));
    recupInfosPage(document.getTroisiemeDeCouverture());
    recupInfosPage(document.getQuatriemeDeCouverture());
  }
  
  /**
   * On g�n�re l'ent�te du fichier
   * @return
   */
  private boolean addEnteteFichier() {
    if (encoding.equals("ISO-8859-1") || encoding.equals("ISO8859_1") || encoding.toLowerCase().equals("cp1252"))
      encoding = "ISO-8859-15";
    
    docfo.add("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>");
    docfo.add("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" >");
    docfo.add("");
    
    return true;
  }
  
  /**
   * On g�n�re le pied du fichier
   * @return
   */
  private boolean addPiedFichier() {
    docfo.add("</fo:root>");
    
    return true;
  }
  
  /***
   * On g�n�re l'ent�te
   * @return
   */
  private boolean addEnteteDocument() {
    if (listing == null)
      return false;
    // System.out.println("-addEnteteDocument-> " + document.getNom());
    docfo.add("<!-- D�claration de la mise en page -->");
    docfo.add("<fo:layout-master-set>");
    
    if (document != null) {
      addEnteteDocument_descriptionPage((DescriptionPage) document.getPremiereDeCouverture());
      addEnteteDocument_descriptionPage((DescriptionPage) document.getDeuxiemeDeCouverture());
      for (int j = 0; j < document.getSequence().size(); j++)
        addEnteteDocument_descriptionPage((DescriptionPage) document.getSequence().get(j));
      addEnteteDocument_descriptionPage((DescriptionPage) document.getTroisiemeDeCouverture());
      addEnteteDocument_descriptionPage((DescriptionPage) document.getQuatriemeDeCouverture());
    }
    else // Si pas de description
    {
      // docfo.add("<fo:simple-page-master master-name=\"A4-0\" page-height=\"29.7cm\" page-width=\"21.0cm\" margin-top=\"0.75cm\"
      // margin-left=\"0.75cm\" >");
      docfo.add("<fo:simple-page-master master-name=\"A4-0\" page-height=\"" + hauteurCm + "cm\" page-width=\"" + largeurCm
          + "cm\"  margin-top=\"0.75cm\" margin-left=\"0.75cm\" >");
      docfo.add("<fo:region-body />");
      docfo.add("</fo:simple-page-master>");
    }
    docfo.add("</fo:layout-master-set>");
    docfo.add("");
    
    return true;
  }
  
  /***
   * On g�n�re l'ent�te d'une page
   * @return
   */
  private void addEnteteDocument_descriptionPage(DescriptionPage page) {
    if (page == null)
      return;
    
    docfo.add("\t<fo:simple-page-master master-name=\"" + PREFIXE_PAGE_MASTER + page.getNomFichierDDP() + "\" page-height=\""
        + page.getHauteur() + "cm\" page-width=\"" + page.getLargeur() + "cm\"  margin-top=\"" + page.getMarge_y() + "cm\" margin-left=\""
        + page.getMarge_x() + "cm\" >");
    if (page.getImage() != NOIMAGE)
      docfo.add("\t\t<fo:region-body background-image=\"" + page.getImage() + "\" background-repeat=\"no-repeat\" />"); // width=\"100%\"
                                                                                                                        // height=\"100%\"
                                                                                                                        // />");
    // docfo.add("\t\t<fo:region-body background-image=\"url('"+ chaine.toURI() +"') \" background-repeat=\"no-repeat\" width=\"100%\"
    // height=\"100%\" />");
    else
      docfo.add("\t\t<fo:region-body />");
    docfo.add("\t</fo:simple-page-master>");
  }
  
  /**
   * G�n�ration du corps du document (toutes les pages)
   * @return
   */
  private boolean addCorpsDocument(String[] spool) {
    if (spool == null)
      return false;
    int pageencours = -1, pageagarder = 0;
    int pageatraiter = 0, pagesequence = 0;
    
    // G�n�ration du corps du document
    docfo.add("<!-- Corps du document -->");
    
    // Avec la description
    if (document != null) {
      // On v�rifie si on doit la s�quence doit laisser des pages pour la troisiemedecouv & la quatriemedecouv
      if ((document.getTroisiemeDeCouverture() != null) && (document.getTroisiemeDeCouverture().isLienSpool()))
        pageagarder++;
      if ((document.getQuatriemeDeCouverture() != null) && (document.getQuatriemeDeCouverture().isLienSpool()))
        pageagarder++;
      if (document.getSequence() != null) {
        pageatraiter = spool.length - pageagarder;
        for (int j = 0; j < document.getSequence().size(); j++)
          if (((DescriptionPage) document.getSequence().get(j)).isLienSpool())
            pagesequence++;
      }
      // System.out.println("-addCorpsDocument-> A " + spool.length + " " + pageagarder + " " +pageatraiter);
      
      pageencours = addPageDocument((DescriptionPage) document.getPremiereDeCouverture(), spool, pageencours);
      pageencours = addPageDocument((DescriptionPage) document.getDeuxiemeDeCouverture(), spool, pageencours);
      // System.out.println("-addCorpsDocument-> B " + pageencours);
      if (pageatraiter > 0) {
        pageatraiter = pageatraiter / pagesequence;
        // System.out.println("-addCorpsDocument-> C " + pageatraiter + " " + pagesequence);
        for (int i = 0; i < pageatraiter; i++)
          for (int j = 0; j < document.getSequence().size(); j++)
            pageencours = addPageDocument((DescriptionPage) document.getSequence().get(j), spool, pageencours);
      }
      pageencours = addPageDocument((DescriptionPage) document.getTroisiemeDeCouverture(), spool, pageencours);
      pageencours = addPageDocument((DescriptionPage) document.getQuatriemeDeCouverture(), spool, pageencours);
    }
    else // Sans la description
      addPageDocument(spool);
    
    return true;
  }
  
  /**
   * G�n�ration d'une page du document � partir d'une description
   * @param page
   * @return
   */
  private int addPageDocument(DescriptionPage page, String[] documentspool, int pageencours) {
    if (page == null)
      return pageencours;
    
    // Recherche s'il ya un fond de page
    String master = PREFIXE_PAGE_MASTER + page.getNomFichierDDP();// images.indexOf(page.getImage());
    if (page.isLienSpool()) {
      pageencours++;
      id.setDonnees(new StringBuffer(documentspool[pageencours]));
      addVariables(page);
      initConditions(page);
    }
    // System.out.println("-addPageDocument-> page en cours:" + pageencours + " " + page.isLienSpool());
    
    // On g�n�re la description de la page
    docfo.add("<fo:page-sequence master-reference=\"" + master + "\" >");
    docfo.add("<fo:flow flow-name=\"xsl-region-body\" white-space-collapse=\"false\"  wrap-option=\"wrap\" white-space=\"pre\" >");
    // On ajoute les composants
    for (int i = 0; i < page.getDetiquette().getDObject().size(); i++)
      if (page.getDetiquette().getDObject().get(i) instanceof DescriptionLabel)
        addLabelDocument((DescriptionLabel) page.getDetiquette().getDObject().get(i), documentspool[pageencours]);
      else if (page.getDetiquette().getDObject().get(i) instanceof DescriptionImage)
        addImageDocument((DescriptionImage) page.getDetiquette().getDObject().get(i), documentspool[pageencours]);
      else if (page.getDetiquette().getDObject().get(i) instanceof DescriptionCodeBarre)
        addCodeBarreDocument((DescriptionCodeBarre) page.getDetiquette().getDObject().get(i), documentspool[pageencours]);
      
    // Si aucun composant, on ajoute un bloc vide (pour pas avoir d'erreur de FOP)
    if (page.getDetiquette().getDObject().size() == 0)
      addLabelVide();
    docfo.add("</fo:flow>");
    docfo.add("</fo:page-sequence>");
    docfo.add("");
    
    return pageencours;
  }
  
  /**
   * G�n�ration d'un page du document sans description
   * @param page
   * @return
   */
  private int addPageDocument(String[] documentspool) {
    // Pas de fond de page
    String master = PREFIXE_PAGE_MASTER + "0";
    
    for (int i = 0; i < documentspool.length; i++) {
      // On g�n�re la description de la page
      docfo.add("<fo:page-sequence master-reference=\"" + master + "\" >");
      docfo.add("<fo:flow flow-name=\"xsl-region-body\" white-space-collapse=\"false\"  wrap-option=\"wrap\" white-space=\"pre\" >");
      // docfo.add("<fo:flow flow-name=\"xsl-region-body\" white-space-collapse=\"false\"
      // white-space-treatment=\"ignore-if-before-linefeed\" >");
      addLabelDocument(documentspool[i], police);
      docfo.add("</fo:flow>");
      docfo.add("</fo:page-sequence>");
      docfo.add("");
    }
    return documentspool.length - 1;
  }
  
  /**
   * G�n�re une ligne vide dans le cas o� il n'y a aucun composant dans la description
   */
  private void addLabelVide() {
    docfo.add(
        "<fo:block-container position=\"absolute\" top=\"1cm\" left=\"1cm\" width=\"1cm\" height=\"1cm\" line-height=\"10%\" display-align=\"center\">");
    docfo.add("<fo:block />");
    docfo.add("</fo:block-container>");
  }
  
  /**
   * G�n�ration d'un label dans le document � partir d'une description de spool
   * @param ligne
   * @param pagespool
   * @return
   */
  private boolean addLabelDocument(DescriptionLabel dlabel, String pagespool) {
    if (dlabel == null)
      return false;
    // System.out.println("--> FO " + dlabel.getTexte());
    String texte = id.analyseExpressionWOTrim(dlabel.getTexte());
    
    if (texte.equals(""))
      return true; // si le texte est vide on ne g�n�re pas le code n�cessaire
    if (dlabel.getCondition() != null)
      if (!listeConditions.get(dlabel.getCondition()).isResultat())
        return false;
    // On convertit les coordonn�es �crans en coordonn�es documents final
    // int x = (int) ((dlabel.getLeft_px(texte)-marge_x) * document.getTarget_dpi() / document.getSource_dpi());
    // int y = (int) ((dlabel.getYPos_px()-marge_y) * document.getTarget_dpi() / document.getSource_dpi());
    
    docfo.add("<fo:block-container position=\"absolute\" top=\"" + dlabel.getYPos_cm() + "cm\" left=\"" + dlabel.getXPos_cm()
        + "cm\" width=\"" + dlabel.getLargeur() + "cm\" height=\"" + dlabel.getHauteur() + "cm\" line-height=\"" + dlabel.getLineHeight()
        + "%\" display-align=\"" + getJustificationTexte(dlabel.getJustificationVTexte()) + "\">");
    docfo.add("\t<fo:block font-family=\"" + dlabel.getNomPolice() + "\" font-size=\"" + dlabel.getTaillePolice() + "pt\" "
        + getStyle(dlabel.getStylePolice()) + " " + getCouleur(dlabel.getForeGroundColor()) + " text-align=\""
        + getJustificationTexte(dlabel.getJustificationHTexte()) + "\">");
    texte = convertSymboleEuro(texte);
    docfo.add("<![CDATA[" + texte + "]]>");
    
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * G�n�ration d'un label dans le document sans description
   * @param ligne
   * @param pagespool
   * @return
   */
  private boolean addLabelDocument(String bloc, Font police) {
    if (bloc == null)
      return false;
    if (bloc.equals(""))
      return true; // si le texte est vide on ne g�n�re pas le code n�cessaire
      
    docfo.add("<fo:block-container width=\"100%\" >");
    docfo.add("\t<fo:block font-family=\"" + police.getName() + "\" font-size=\"" + police.getSize() + "\" " + getStyle(police.getStyle())
        + " " + getCouleur(Color.BLACK) + ">");
    // bloc = getTrimR(bloc);
    // System.out.println("-bloc->" + bloc + "|");
    bloc = convertSymboleEuro(bloc);
    docfo.add("<![CDATA[" + bloc + "]]>");
    
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * G�n�ration d'une image dans le document � partir d'une description de spool
   * @param ligne
   * @param pagespool
   * @return
   */
  private boolean addImageDocument(DescriptionImage dimage, String pagespool) {
    if (dimage == null)
      return false;
    if (dimage.getCondition() != null)
      if (!listeConditions.get(dimage.getCondition()).isResultat())
        return false;
      
    // String image = id.analyseExpressionWOTrim(dimage.getCheminImage());
    String image = dimage.getCheminImage();
    if (listeVariables != null)
      for (int i = listeVariables.size(); --i >= 0;)
        image = image.replaceAll(listeVariables.get(i).getNom(), listeVariables.get(i).getValeur());
    // System.out.println("-->"+dimage.getCheminImage()+"|"+image+"|");
    if (image.equals(""))
      return true; // si l'image est vide on ne g�n�re pas le code n�cessaire
      
    docfo.add("<fo:block-container position=\"absolute\" top=\"" + dimage.getYPos_cm() + "cm\" left=\"" + dimage.getXPos_cm()
        + "cm\" line-height=\"3pt\" display-align=\"center\" >");
    docfo.add("\t<fo:block>");
    docfo.add("<fo:external-graphic src=\"" + image.trim() + "\"  width=\"" + dimage.getLargeur() + "cm\" height=\"" + dimage.getHauteur()
        + "cm\" content-height=\"scale-to-fit\"	content-width=\"scale-to-fit\" />");
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * G�n�ration d'un code barre dans le document � partir d'une description de spool
   * @param ligne
   * @param pagespool
   * @return
   */
  private boolean addCodeBarreDocument(DescriptionCodeBarre dcodebarre, String pagespool) {
    if (dcodebarre == null)
      return false;
    
    String texte = id.analyseExpressionWOTrim(dcodebarre.getTexte());
    if (texte.equals(""))
      return true; // si le texte est vide on ne g�n�re pas le code n�cessaire
    if (dcodebarre.getCondition() != null)
      if (!listeConditions.get(dcodebarre.getCondition()).isResultat())
        return false;
      
    /*
    String image = dimage.getCheminImage();
    if (listeVariables != null)
    	for(int i=listeVariables.size(); --i>=0;)
    		image = image.replaceAll(listeVariables.get(i).getNom(), listeVariables.get(i).getValeur());
    //System.out.println("-->"+dimage.getCheminImage()+"|"+image+"|");
    if (image.equals("")) return true;	// si l'image est vide on ne g�n�re pas le code n�cessaire
    */
    docfo.add("<fo:block-container position=\"absolute\" top=\"" + dcodebarre.getYPos_cm() + "cm\" left=\"" + dcodebarre.getXPos_cm()
        + "cm\" >");
    docfo.add("\t<fo:block>");
    docfo.add("\t\t<fo:instream-foreign-object>");
    docfo.add("\t\t\t<bc:barcode xmlns:bc=\"http://barcode4j.krysalis.org/ns\" message=\"" + texte + "\" orientation=\""
        + dcodebarre.getRotation() + "\">");
    docfo.add("\t\t\t\t<bc:" + dcodebarre.getTypeCodeBarre() + ">");
    docfo.add("\t\t\t\t\t<bc:human-readable>" + dcodebarre.getPositionMessage() + "</bc:human-readable>");
    docfo.add("\t\t\t\t\t<bc:height>" + dcodebarre.getHauteur() + "cm</bc:height>");
    if (dcodebarre.getLargeurModule() > 0)
      docfo.add("\t\t\t\t\t<bc:module-width>" + dcodebarre.getLargeurModule() + "mm</bc:module-width>");
    docfo.add("\t\t\t\t</bc:" + dcodebarre.getTypeCodeBarre() + ">");
    docfo.add("\t\t\t</bc:barcode>");
    docfo.add("\t\t</fo:instream-foreign-object>");
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * Retourne le style cod� en chaine
   * @param style
   * @return
   */
  private String getStyle(int style) {
    String style_ch = "";
    switch (style) {
      case Font.ITALIC:
        style_ch = "font-style=\"italic\" ";
      case Font.PLAIN:
        style_ch += "font-weight=\"400\"";
        break;
      case Font.BOLD:
        style_ch += "font-weight=\"700\"";
        break;
    }
    
    return style_ch;
  }
  
  /**
   * Retourne la couleur cod�e en chaine
   * @param style
   * @return
   */
  private String getCouleur(Color couleur) {
    if (couleur == Color.BLACK)
      return "";
    else
      return "color=\"rgb(" + couleur.getRed() + "," + couleur.getGreen() + "," + couleur.getBlue() + ")\" ";
  }
  
  /**
   * Retourne le texte exprimant la justification du texte
   * @param justification
   * @return
   */
  private String getJustificationTexte(int jht) {
    switch (jht) {
      case DescriptionLabel.GAUCHE:
        return "left";
      case DescriptionLabel.CENTRE:
        return "center";
      case DescriptionLabel.DROITE:
        return "right";
      // case DescriptionLabel.HAUT : return "before";
      // case DescriptionLabel.BAS : return "after";
      default:
        return "left";
    }
  }
  
  /**
   * Ajoute les variables d'une description de page
   * @param page
   */
  private void addVariables(DescriptionPage page) {
    if ((listeVariables == null) || (page.getListeDescriptionVariable() == null))
      return;
    
    // On v�rifie que l'on ne l'ai d�j� pas trait�
    if (listeDescriptionPage.contains(page))
      return;
    listeDescriptionPage.add(page);
    
    String chaine = null;
    // On ajoute les variables dans la liste avec leur valeur
    for (int i = page.getListeDescriptionVariable().size(); --i >= 0;) {
      // TODO voir le analyseExpressionTrimR merde apparemment enleve pas les blancs � droite
      chaine = id.analyseExpression(((DescriptionVariable) page.getListeDescriptionVariable().get(i)).getVariable());
      ((DescriptionVariable) page.getListeDescriptionVariable().get(i)).setValeur(chaine == null ? "" : chaine.trim());
      // listeVariable.put( ((DescriptionVariable) page.getListeDescriptionVariable().get(i)).getNom(),
      // id.analyseExpressionTrimR(((DescriptionVariable)page.getListeDescriptionVariable().get(i)).getVariable()));
      listeVariables.add((DescriptionVariable) page.getListeDescriptionVariable().get(i));
    }
  }
  
  /**
   * Initialise les conditions pour la page courante
   * @param page
   */
  private void initConditions(DescriptionPage page) {
    if (page.getListeDescriptionCondition() == null)
      return;
    
    listeConditions.clear();
    for (int i = page.getListeDescriptionCondition().size(); --i >= 0;) {
      ((DescriptionCondition) page.getListeDescriptionCondition().get(i)).setResultat(id);
      listeConditions.put(((DescriptionCondition) page.getListeDescriptionCondition().get(i)).getNom(),
          (DescriptionCondition) page.getListeDescriptionCondition().get(i));
    }
  }
  
  /**
   * Initialise la liste des variables
   * @param lstVar
   */
  // public void setListeVariable(HashMap<String, String> lstVar)
  public void setListeVariables(ArrayList<DescriptionVariable> lstvar) {
    listeVariables = lstvar;
    if (listeVariables == null)
      return;
    
    // Ajout des variables syst�mes date & heure (au cas o�)
    for (int i = 0; i <= 5; i++) {
      DescriptionVariable var = new DescriptionVariable();
      var.setNom(DateHeure.getNomVariable(i));
      var.setValeur(DateHeure.getFormateDateHeure(i));
      listeVariables.add(var);
    }
  }
  
  /**
   * Convertit le caract�re 164 en euros (r�gle le pb avec les codes pages)
   * @param texte
   * @return
   */
  private String convertSymboleEuro(String texte) {
    // if (texte == null) return texte;
    
    int pos = texte.indexOf(164);
    if (pos == -1)
      return texte;
    
    char c = (char) Integer.parseInt("A4", 16);
    return texte.replace(c, '�');
    // return texte.replace(c, '\u20AC');
  }
  
  /**
   * Retourne une chaine trim�e � droite
   * @param texte
   * @return
   */
  private String getTrimR(String texte) {
    // compteur++;
    // if (compteur < 50) System.out.println("-Avant->" + texte+"|" + texte.length());
    if (texte == null)
      return texte;
    
    int i = 0;
    char[] tab = texte.toCharArray();
    for (i = tab.length; --i > 0;) {
      if ((tab[i] == '\n') || (tab[i] == '\r'))
        continue;
      if (tab[i] != ' ')
        break;
    }
    if (i < 0)
      return "";
    // texte = new String(tab, 0, i+1);
    // if (compteur < 50) System.out.println("-Apres->" + texte+"|" + texte.length());
    return new String(tab, 0, i + 1);
  }
}
