/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package spool;

import com.ibm.as400.access.AS400;

//=================================================================================================
//==> Gestion des imprimantes sur un AS/400 
//=================================================================================================
public class GestionImprimanteAS400 {
  // Variables
  private AS400 systeme = null;
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur �mit et non lu
  
  /**
   * Constructeur
   */
  public GestionImprimanteAS400() {
    systeme = new AS400();
  }
  
  /**
   * Constructeur
   * @param systeme
   */
  public GestionImprimanteAS400(AS400 systeme) {
    if (systeme == null)
      this.systeme = new AS400();
    else
      this.systeme = systeme;
  }
  
  /**
   * Constructeur
   * @param host
   * @param user
   * @param mdp
   */
  public GestionImprimanteAS400(String host, String user, String mdp) {
    systeme = new AS400(host, user, mdp);
  }
  
  /**
   * D�connecte la session
   */
  public void deconnecter() {
    if (systeme != null)
      systeme.disconnectAllServices();
    systeme = null;
  }
  
  /**
   * Retourne la valeur de syst�me
   * @return
   */
  public AS400 getSystem() {
    if (systeme == null)
      systeme = new AS400();
    return systeme;
  }
  
  /**
   * Retourne la liste des imprimantes avec leur status et leur outq
   * @return
   */
  /*
  public JSONArray getListeImprimantes()
  {
  	JSONArray tabjson=new JSONArray();
  	PrinterList liste = new PrinterList(systeme);
  
  	try
  	{
  		liste.openSynchronously();
  		for (int i=0; i<liste.size(); i++)
  		{
  			Printer prt = (Printer) liste.getObject(i);
  			JSONObject objjson = new JSONObject();
  			objjson.put("NAME", prt.getName());
  			objjson.put("STATUS", prt.getStringAttribute(prt.ATTR_WTRSTRTD));
  			objjson.put("OUTQ",  prt.getStringAttribute(prt.ATTR_OUTPUT_QUEUE));
  			tabjson.add(objjson);
  			//System.out.println("--> " + prt.getName() + " " + prt.getStringAttribute(prt.ATTR_WTRSTRTD ) + " " + prt.getStringAttribute(prt.ATTR_OUTPUT_QUEUE));
  		}
  	}
  	catch(Exception e)
  	{
  		msgErreur += "\n" + NOM_CLASSE + " (getListeImprimantes) " + e;
  		tabjson = null;
  	}
  	finally
  	{
  		liste.close();
  	}
  	
  	return tabjson;
  }*/
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    // La r�cup�ration du message est � usage unique
    String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
