/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description;

//=================================================================================================
//==> Description d'un object primitif (futur label, image, ...)
//=================================================================================================
public class DescriptionObject {
  // Variables
  protected float yPos_cm = 0; // En cm
  protected float xPos_cm = 0; // En cm
  protected float largeur = 0; // En cm
  protected float hauteur = 0; // En cm
  protected String condition = null; // Suite des conditions saisie en notation polonaise (liste contenu dans la description de page),
  // le r�sultat doit �tre vrai pour que l'objet s'affiche (ex: COND_1 COND_2 &)
  
  /**
   * Position Y en cm du label
   * @return colonne
   */
  public float getYPos_cm() {
    return yPos_cm;
  }
  
  /**
   * Initialise la position Y en cm du label
   * @return colonne
   */
  public void setYPos_cm(float y) {
    yPos_cm = y;
  }
  
  /**
   * Position X en cm du label
   * @return colonne
   */
  public float getXPos_cm() {
    return xPos_cm;
  }
  
  /**
   * Initialise la position Y en cm du label
   * @return colonne
   */
  public void setXPos_cm(float x) {
    xPos_cm = x;
  }
  
  /**
   * @return the largeur
   */
  public float getLargeur() {
    return largeur;
  }
  
  /**
   * @param largeur the largeur to set
   */
  public void setLargeur(float largeur) {
    this.largeur = largeur;
  }
  
  /**
   * @return the hauteur
   */
  public float getHauteur() {
    return hauteur;
  }
  
  /**
   * @param hauteur the hauteur to set
   */
  public void setHauteur(float hauteur) {
    this.hauteur = hauteur;
  }
  
  /**
   * @param condition the condition to set
   */
  public void setCondition(String condition) {
    this.condition = condition;
  }
  
  /**
   * @return the condition
   */
  public String getCondition() {
    return condition;
  }
}
