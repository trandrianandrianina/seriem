/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description;

import java.util.ArrayList;

import ri.seriem.libcommun.edition.ConstantesNewSim;

//=================================================================================================
//==> Description d'une page d'un document
//=================================================================================================
public class DescriptionPage {
  // Constantes
  public static final String EXTENSION = ".ddp";
  
  // Variables
  private String nomFichierDDP = null; // Nom de la description (correspondant au nom du fichier ddp)
  protected boolean portrait = true; // Portrait ou paysage
  protected float marge_x = 0; // Marge en cm
  protected float marge_y = 0; // Marge en cm
  protected float largeur = ConstantesNewSim.A4_WIDTH; // Largeur en cm
  protected float hauteur = ConstantesNewSim.A4_HEIGHT; // Hauteur en cm
  protected int nbrColonne = 1; // Nombre d'�tiquette sur la largeur
  protected int nbrLigne = 1; // Nombre d'�tiquette sur la hauteur
  protected String imagefond = null;
  protected DescriptionEtiquette detiquette = new DescriptionEtiquette();
  protected ArrayList<DescriptionCondition> listeDescriptionCondition = null;
  protected ArrayList<DescriptionVariable> listeDescriptionVariable = null;
  private String commentaire = null;
  private boolean lienSpool = true; // La description est li�e � la page du spool
  
  /**
   * @param nom the nom to set
   */
  public void setNomFichierDDP(String nom) {
    this.nomFichierDDP = nom;
  }
  
  /**
   * @return the nom
   */
  public String getNomFichierDDP() {
    return nomFichierDDP;
  }
  
  /**
   * @return detq
   */
  public DescriptionEtiquette getDetiquette() {
    return detiquette;
  }
  
  /**
   * @param detq detq � d�finir
   */
  public void setDetiquette(DescriptionEtiquette detq) {
    this.detiquette = detq;
  }
  
  /**
   * @return hauteur
   */
  public float getHauteur() {
    return hauteur;
  }
  
  /**
   * @param hauteur hauteur � d�finir
   */
  public void setHauteur(float hauteur) {
    this.hauteur = hauteur;
  }
  
  /**
   * @return largeur
   */
  public float getLargeur() {
    return largeur;
  }
  
  /**
   * @param largeur largeur � d�finir
   */
  public void setLargeur(float largeur) {
    this.largeur = largeur;
  }
  
  /**
   * @return marge_x
   */
  public float getMarge_x() {
    return marge_x;
  }
  
  /**
   * @param marge_x marge_x � d�finir
   */
  public void setMarge_x(float marge_x) {
    this.marge_x = marge_x;
  }
  
  /**
   * @return marge_y
   */
  public float getMarge_y() {
    return marge_y;
  }
  
  /**
   * @param marge_y marge_y � d�finir
   */
  public void setMarge_y(float marge_y) {
    this.marge_y = marge_y;
  }
  
  /**
   * @return nbrColonne
   */
  public int getNbrColonne() {
    return nbrColonne;
  }
  
  /**
   * @param nbrColonne nbrColonne � d�finir
   */
  public void setNbrColonne(int nbrColonne) {
    this.nbrColonne = nbrColonne;
  }
  
  /**
   * @return nbrLigne
   */
  public int getNbrLigne() {
    return nbrLigne;
  }
  
  /**
   * @param nbrLigne nbrLigne � d�finir
   */
  public void setNbrLigne(int nbrLigne) {
    this.nbrLigne = nbrLigne;
  }
  
  /**
   * Intervertit Portrait / Paysage
   */
  public void setPortrait(boolean portrait) {
    if (this.portrait == portrait)
      return;
    this.portrait = portrait;
    
    float ech = hauteur;
    setHauteur(largeur);
    setLargeur(ech);
    // Si on est en pleine page (TODO � am�liorer le contr�le)
    if ((detiquette != null) && (nbrColonne == 1) && (nbrLigne == 1)) {
      detiquette.setHauteur(getHauteur());
      detiquette.setLargeur(getLargeur());
    }
  }
  
  /**
   * Retourne si protrait ou paysage
   * @return
   */
  public boolean isPortrait() {
    return portrait;
  }
  
  /**
   * @param image the image to set
   */
  public void setImage(String image) {
    this.imagefond = image;
  }
  
  /**
   * @return the image
   */
  public String getImage() {
    return imagefond;
  }
  
  /**
   * @return the listeDescriptionCondition
   */
  public ArrayList<DescriptionCondition> getListeDescriptionCondition() {
    return listeDescriptionCondition;
  }
  
  /**
   * @param listeDescriptionCondition the listeDescriptionCondition to set
   */
  public void setListeDescriptionCondition(ArrayList<DescriptionCondition> listeDescriptionCondition) {
    this.listeDescriptionCondition = listeDescriptionCondition;
  }
  
  /**
   * @param listeDescriptionCondition the listeDescriptionCondition to set
   */
  public void addListeDescriptionCondition(DescriptionCondition condition) {
    // if (sequence == null) sequence = new ArrayList();
    listeDescriptionCondition.add(condition);
  }
  
  /**
   * @return the listeDescriptionVariable
   */
  public ArrayList<DescriptionVariable> getListeDescriptionVariable() {
    return listeDescriptionVariable;
  }
  
  /**
   * @param listeDescriptionVariable the listeDescriptionVariable to set
   */
  public void setListeDescriptionVariable(ArrayList<DescriptionVariable> listeDescriptionVariable) {
    this.listeDescriptionVariable = listeDescriptionVariable;
  }
  
  /**
   * @param listeDescriptionVariable the listeDescriptionVariable to set
   */
  public void addListeDescriptionVariable(DescriptionVariable variable) {
    // if (sequence == null) sequence = new ArrayList();
    listeDescriptionVariable.add(variable);
  }
  
  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }
  
  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }
  
  /**
   * @param lienSpool the lienSpool to set
   */
  public void setLienSpool(boolean lienSpool) {
    this.lienSpool = lienSpool;
  }
  
  /**
   * @return the lienSpool
   */
  public boolean isLienSpool() {
    return lienSpool;
  }
  
  /**
   * A priori ne sert pas
   * Retourne une page pour le Rad
   * @param dpi_aff
   * @param dpi_img
   * @return
   *
   *         public GfxPage getGfxPage(int dpi_aff, int dpi_img, final GfxPageEditor planTravail)
   *         {
   *         GfxPage page = new GfxPage(this, dpi_aff, dpi_img, planTravail);
   *         return page;
   *         }
   */
  
}
