/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description.sortie;

import ri.seriem.libcommun.outils.Constantes;

//=================================================================================================
//==> Description de la sortie de type imprimante
//=================================================================================================
public class SortieImprimante extends Sortie {
  // Constantes
  public final static String ESC = "<ESC>";
  public final static char ESC_CHAR = '\u001B';
  public final static int SCRIPT_AUCUN = 0;
  public final static int SCRIPT_PJL = 1;
  public final static int SCRIPT_PERSO = 2;
  
  public final static String LG_AUCUN = null;
  public final static String LG_PJL = "pjl";
  public final static String LG_PS = "ps";
  
  // Variables
  private String nomSpool = null; // Nom du spool que l'on va g�n�rer sur l'AS/400
  private String nomOutq = null; // Nom de la Outq de l'imprimante
  private String lgimprimante = null; // Langage de l'imprimante (ps, pjl, ...)
  private int tiroirEntree = 1; // Tiroir d'entr�e papier (=0 tiroir par d�faut)
  private int tiroirSortie = 0; // Tiroir de sortie du papier (=0 tiroir par d�faut)
  private boolean rectoverso = false;
  private boolean shortedge = false; // Par d�faut c'est longedge (axe de rotation sur le c�t� le plus long)
  private int nbrExemplaire = 1; // Nombre d'exemplaire � g�n�rer
  // private String qualite=null; // Qualit� d'impression : *STD, *DRAFT, *NLQ, *FASTDRAFT
  private String priorite = null; // Priorit� : 1 (haute) � 9 (basse)
  private boolean suspendre = false; // Holder le spool sur l'AS/400 apr�s sa cr�ation (ps / pdf) [!!pas celui d'origine!!]
  private boolean economie = false; // Economie du toner
  private int densite = 3; // Densit� 1 � 5
  private boolean noirblanc = false; // Force le noir & blanc sur imprimante couleur
  private int x_translate = 0; // Translation sur X
  private int y_translate = 0; // Translation sur Y
  private int x_scale = 100; // Redimensionnement sur X
  private int y_scale = 100; // Redimensionnement sur Y
  private int typeScript = SCRIPT_AUCUN; // Type de script : aucun, pjl
  private String scriptAvant = null; // Script avant le fichier
  private String scriptApres = null; // Script apr�s le fichier
  private boolean soumission = false; // Soumission pour l'impression directe
  
  /**
   * Constructeur
   */
  public SortieImprimante() {
    this(PDF);
  }
  
  /**
   * Constructeur
   * @param ext
   */
  public SortieImprimante(String ext) {
    extension = ext;
  }
  
  /**
   * @return the rectoverso
   */
  public boolean isRectoverso() {
    return rectoverso;
  }
  
  /**
   * @param rectoverso the rectoverso to set
   */
  public void setRectoverso(boolean rectoverso) {
    this.rectoverso = rectoverso;
  }
  
  /**
   * @return the shortedge
   */
  public boolean isShortedge() {
    return shortedge;
  }
  
  /**
   * @param shortedge the shortedge to set
   */
  public void setShortedge(boolean shortedge) {
    this.shortedge = shortedge;
  }
  
  /**
   * @param nbrExemplaire the nbrExemplaire to set
   */
  public void setNbrExemplaire(int nbrExemplaire) {
    this.nbrExemplaire = nbrExemplaire;
  }
  
  /**
   * @return the nbrExemplaire
   */
  public int getNbrExemplaire() {
    return nbrExemplaire;
  }
  
  /**
   * @param nomOutq the nomOutq to set
   */
  public void setNomOutq(String nomOutq) {
    this.nomOutq = nomOutq;
  }
  
  /**
   * @return the nomOutq
   */
  public String getNomOutq() {
    return nomOutq;
  }
  
  /**
   * @param lgimprimante the lgimprimante to set
   */
  public void setLgimprimante(String lgimprimante) {
    if (lgimprimante != null)
      this.lgimprimante = lgimprimante.trim().toLowerCase();
    else
      this.lgimprimante = lgimprimante;
  }
  
  /**
   * @return the lgimprimante
   */
  public String getLgimprimante() {
    return lgimprimante;
  }
  
  /**
   * @return the tiroirEntree
   */
  public int getTiroirEntree() {
    return tiroirEntree;
  }
  
  /**
   * @param tiroirEntree the tiroirEntree to set
   */
  public void setTiroirEntree(int tiroirEntree) {
    this.tiroirEntree = tiroirEntree;
  }
  
  /**
   * @param tiroirSortie the tiroirSortie to set
   */
  public void setTiroirSortie(int tiroirSortie) {
    this.tiroirSortie = tiroirSortie;
  }
  
  /**
   * @return the tiroirSortie
   */
  public int getTiroirSortie() {
    return tiroirSortie;
  }
  
  /**
   * @return the qualite
   *
   *         public String getQualite()
   *         {
   *         return qualite;
   *         }
   *         /**
   * @param qualite the qualite to set
   *
   *          public void setQualite(String qualite)
   *          {
   *          this.qualite = qualite;
   *          }
   */
  /**
   * @return the priorite
   */
  public String getPriorite() {
    return priorite;
  }
  
  /**
   * @param priorite the priorite to set
   */
  public void setPriorite(String priorite) {
    this.priorite = priorite;
  }
  
  /**
   * @return the suspendre
   */
  public boolean isSuspendre() {
    return suspendre;
  }
  
  /**
   * @param suspendre the suspendre to set
   */
  public void setSuspendre(boolean suspendre) {
    this.suspendre = suspendre;
  }
  
  /**
   * @return the nomSpool
   */
  public String getNomSpool() {
    return nomSpool;
  }
  
  /**
   * @param nomSpool the nomSpool to set
   */
  public void setNomSpool(String nomSpool) {
    this.nomSpool = nomSpool;
  }
  
  /**
   * @return the typeScript
   */
  public int getTypeScript() {
    return typeScript;
  }
  
  /**
   * @param typeScript the typeScript to set
   */
  public void setTypeScript(int typeScript) {
    this.typeScript = typeScript;
  }
  
  /**
   * @return the scriptAvant
   */
  public String getScriptAvant() {
    return scriptAvant;
  }
  
  /**
   * @return the scriptAvant
   */
  public String getScriptAvant(boolean convert) {
    // System.out.println("-SortieImprimante-> script avant \n" + scriptAvant);
    if ((scriptAvant != null) && convert)
      return scriptAvant.replaceAll(ESC, "" + ESC_CHAR);
    else
      return getScriptAvant();
  }
  
  /**
   * @param scriptAvant the scriptAvant to set
   */
  public void setScriptAvant(String scriptAvant) {
    this.scriptAvant = scriptAvant;
  }
  
  /**
   * @return the scriptApres
   */
  public String getScriptApres() {
    return scriptApres;
  }
  
  /**
   * @return the scriptApres
   */
  public String getScriptApres(boolean convert) {
    // System.out.println("-SortieImprimante-> script apres \n" + scriptApres);
    if ((scriptApres != null) && convert)
      return scriptApres.replaceAll(ESC, "" + ESC_CHAR);
    else
      return getScriptApres();
  }
  
  /**
   * @param scriptApres the scriptApres to set
   */
  public void setScriptApres(String scriptApres) {
    this.scriptApres = scriptApres;
  }
  
  /**
   * @return the economie
   */
  public boolean isEconomie() {
    return economie;
  }
  
  /**
   * @param economie the economie to set
   */
  public void setEconomie(boolean economie) {
    this.economie = economie;
  }
  
  /**
   * @return the densite
   */
  public int getDensite() {
    return densite;
  }
  
  /**
   * @param densite the densite to set
   */
  public void setDensite(int densite) {
    this.densite = densite;
  }
  
  /**
   * @param noirblanc the noirblanc to set
   */
  public void setNoirblanc(boolean noirblanc) {
    this.noirblanc = noirblanc;
  }
  
  /**
   * @return the noirblanc
   */
  public boolean isNoirblanc() {
    return noirblanc;
  }
  
  /**
   * @return the x_translate
   */
  public int getX_translate() {
    return x_translate;
  }
  
  /**
   * @param xTranslate the x_translate to set
   */
  public void setX_translate(int xTranslate) {
    x_translate = xTranslate;
  }
  
  /**
   * @return the y_translate
   */
  public int getY_translate() {
    return y_translate;
  }
  
  /**
   * @param yTranslate the y_translate to set
   */
  public void setY_translate(int yTranslate) {
    y_translate = yTranslate;
  }
  
  /**
   * @return the x_scale
   */
  public int getX_scale() {
    return x_scale;
  }
  
  /**
   * @param xScale the x_scale to set
   */
  public void setX_scale(int xScale) {
    x_scale = xScale;
  }
  
  /**
   * @return the y_scale
   */
  public int getY_scale() {
    return y_scale;
  }
  
  /**
   * @param yScale the y_scale to set
   */
  public void setY_scale(int yScale) {
    y_scale = yScale;
  }
  
  /**
   * G�n�ration du script PJL
   */
  private void genereScriptDebutPjl() {
    StringBuffer script = new StringBuffer();
    
    script.append(SortieImprimante.ESC).append("%-12345X@PJL JOB").append(Constantes.crlf);
    script.append("@PJL SET MANUALFEED=ON").append(Constantes.crlf);
    
    if (rectoverso) {
      script.append("@PJL SET DUPLEX=ON").append(Constantes.crlf);
      if (shortedge)
        script.append("@PJL SET BINDING=SHORTEDGE").append(Constantes.crlf);
      else
        script.append("@PJL SET DUPLEX=LONGEDGE").append(Constantes.crlf);
    }
    if (economie) {
      script.append("@PJL SET ECONOMODE=ON").append(Constantes.crlf);
      script.append("@PJL SET DENSITY=").append(densite).append(Constantes.crlf);
    }
    if (noirblanc)
      script.append("@PJL SET RENDERMODE=GRAYSCALE").append(Constantes.crlf); // RENDERMODE=COLOR
    if (tiroirEntree > 0) {
      script.append("@PJL SET MEDIASOURCE=TRAY").append(tiroirEntree).append(Constantes.crlf);
      script.append("@PJL SET LPAPERSOURCE=TRAY").append(tiroirEntree).append(Constantes.crlf);
    }
    if (tiroirSortie > 0) {
      script.append("@PJL SET OUTTRAY").append(tiroirSortie).append(Constantes.crlf);
    }
    if (extension.equals(Sortie.PDF))
      script.append("@PJL ENTER LANGUAGE=PDF").append(Constantes.crlf);
    else if (extension.equals(Sortie.PS))
      script.append("@PJL ENTER LANGUAGE=POSTSCRIPT").append(Constantes.crlf);
    
    setScriptAvant(script.toString());
  }
  
  /**
   * G�n�ration du script PJL
   */
  private void genereScriptFinPjl() {
    StringBuffer script = new StringBuffer();
    
    script.append("@PJL SET MANUALFEED=OFF").append(Constantes.crlf);
    script.append("@PJL RESET").append(Constantes.crlf);
    script.append(SortieImprimante.ESC).append("%-12345X@PJL EOJ").append(Constantes.crlf);
    script.append(SortieImprimante.ESC).append("%-12345X").append(Constantes.crlf);
    
    setScriptApres(script.toString());
  }
  
  /**
   * G�n�ration du script Postscript
   */
  private void genereScriptDebutPs() {
    StringBuffer script = new StringBuffer();
    
    script.append("%!PS").append(Constantes.crlf);
    if (rectoverso) {
      script.append("%%BeginFeature: *Duplex DuplexTumble").append(Constantes.crlf);
      if (shortedge)
        script.append("<</Duplex true /Tumble true>> setpagedevice").append(Constantes.crlf);
      else
        script.append("<</Duplex true /Tumble false>> setpagedevice").append(Constantes.crlf);
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    if (noirblanc) // A voir si �a fonctionne (pas eu l'occasion car pas de couleur)
    {
      script.append("%%BeginFeature: *ColorasGray").append(Constantes.crlf);
      script.append("<</ProcessColorModel /DeviceGray>> setpagedevice").append(Constantes.crlf);
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    // A tester sur une imprimante (ne fonctionne pas sur LexMark) (postscript 3)
    if (tiroirEntree > 0) {
      script.append("%%BeginFeature:").append(Constantes.crlf);
      script.append("<</DeferredMediaSelection true  /MediaPosition " + tiroirEntree + ">> setpagedevice").append(Constantes.crlf); // /ManualFeed
                                                                                                                                    // false
                                                                                                                                    // �
                                                                                                                                    // ajouter
                                                                                                                                    // apr�s
                                                                                                                                    // /MediaPosition
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    // A imp�menter en GAP
    if ((x_translate > 0) || (y_translate > 0) || (x_scale < 100) || (y_scale < 100)) {
      script.append("%%BeginFeature:").append(Constantes.crlf);
      script.append("<</Install { " + x_translate + " " + y_translate + " translate " + (double) (x_scale / 100) + " "
          + (double) (y_scale / 100) + " scale } bind>> setpagedevice").append(Constantes.crlf);
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    setScriptAvant(script.toString());
  }
  
  public void genereScript() {
    if (lgimprimante == null)
      return;
    if (lgimprimante.equals(LG_PJL)) {
      genereScriptDebutPjl();
      genereScriptFinPjl();
    }
    else if (lgimprimante.equals(LG_PS)) {
      genereScriptDebutPs();
      setScriptApres(null);
      // System.out.println("-Sortieimprimante-> " + scriptAvant);
    }
    else {
      setScriptAvant(null);
      setScriptApres(null);
    }
    
  }
  
  /**
   * @param soumission the soumission to set
   */
  public void setSoumission(boolean soumission) {
    this.soumission = soumission;
  }
  
  /**
   * @return the soumission
   */
  public boolean isSoumission() {
    return soumission;
  }
  
}
