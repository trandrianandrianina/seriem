/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description.sortie;

import java.io.File;

//=================================================================================================
//==> Description de la sortie de type fichier
//=================================================================================================
public class SortieFichier extends Sortie {
  // Variables
  private String nomFichier = null; // Nom du fichier
  private String nomDossier = null; // Chemin du dossier complet sans le nom du fichier
  private String commande = null; // Execution d'une application associ� au document g�n�r�
  
  /**
   * Constructeur
   */
  public SortieFichier() {
    this(PDF);
  }
  
  /**
   * Constructeur
   * @param ext
   */
  public SortieFichier(String ext) {
    extension = ext;
  }
  
  /**
   * @param nomFichier the nomFichier to set
   */
  public void setNomFichier(String nomFichier) {
    this.nomFichier = nomFichier;
  }
  
  /**
   * @return the nomFichier
   */
  public String getNomFichier() {
    return nomFichier;
  }
  
  /**
   * @param nomDossier the nomDossier to set
   */
  public void setNomDossier(String nomDossier) {
    if (nomDossier != null) {
      nomDossier = nomDossier.trim();
      if (nomDossier.endsWith("/") || nomDossier.endsWith("\\"))
        nomDossier = nomDossier.substring(0, nomDossier.length() - 1);
    }
    this.nomDossier = nomDossier;
  }
  
  /**
   * @return the nomDossier
   */
  public String getNomDossier() {
    return nomDossier;
  }
  
  /**
   * Retourne le chemin complet avec le nom du fichier
   * @return
   */
  public String getNomComplet() {
    if ((nomDossier == null) || (nomFichier == null) || (extension == null))
      return "";
    return nomDossier.trim() + File.separatorChar + nomFichier.trim() + '.' + extension;
  }
  
  /**
   * @param commande the commande to set
   */
  public void setCommande(String commande) {
    this.commande = commande;
  }
  
  /**
   * @return the commande
   */
  public String getCommande() {
    if (commande == null)
      commande = "";
    return commande;
  }
  
}
