/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description.sortie;

//=================================================================================================
//==> Description de la sortie
//=================================================================================================
public class Sortie {
  // Constantes
  public final static String PDF = "pdf";
  public final static String PS = "ps";
  public final static String PNG = "png";
  public final static String TIFF = "tiff";
  
  // Variables
  private String description = null; // Description de la sortie
  protected String extension = null; // L'extension du fichier
  private String typeDoc = Sortie.PDF; // Extension du fichier sans le point: ps, tiff, png, ...
  private int dpiSortie = 72; // Nombre de dpi pour le document en sortie 72 pour le PDF
  private String fichierDescriptionDocument = null; // Le nom du fichier de la description de document (ddd)
  
  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }
  
  public void setExtension(String ext) {
    if (ext != null)
      extension = ext.toLowerCase();
    else
      extension = null;
  }
  
  public String getExtension() {
    return extension;
  }
  
  /**
   * @return the typeDoc
   */
  public String getTypeDoc() {
    return typeDoc;
  }
  
  /**
   * @param typeDoc the typeDoc to set
   */
  public void setTypeDoc(String typeDoc) {
    this.typeDoc = typeDoc;
  }
  
  /**
   * @return the dpiSortie
   */
  public int getDpiSortie() {
    return dpiSortie;
  }
  
  /**
   * @param dpiSortie the dpiSortie to set
   */
  public void setDpiSortie(int dpiSortie) {
    this.dpiSortie = dpiSortie;
  }
  
  /**
   * @return the fichierDescriptionDocument
   */
  public String getFichierDescriptionDocument() {
    return fichierDescriptionDocument;
  }
  
  /**
   * @param fichierDescriptionDocument the fichierDescriptionDocument to set
   */
  public void setFichierDescriptionDocument(String fichierDescriptionDocument) {
    this.fichierDescriptionDocument = fichierDescriptionDocument;
  }
  
}
