/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description;

//=================================================================================================
//==> Description d'un code barre
//=================================================================================================
public class DescriptionCodeBarre extends DescriptionObject {
  // Constantes
  public static final String HAUT = "top";
  public static final String BAS = "bottom";
  
  // Variables
  private String texte = null;
  private String typeCodeBarre = null;
  private String positionMessage = BAS;
  private float largeurModule = -1;
  private int rotation = 0;
  
  /**
   * @param texte the texte to set
   */
  public void setTexte(String texte) {
    this.texte = texte;
  }
  
  /**
   * @return the texte
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * @param typeCodeBarre the typeCodeBarre to set
   */
  public void setTypeCodeBarre(String typeCodeBarre) {
    this.typeCodeBarre = typeCodeBarre;
  }
  
  /**
   * @return the typeCodeBarre
   */
  public String getTypeCodeBarre() {
    return typeCodeBarre;
  }
  
  /**
   * @param positionMessage the positionMessage to set
   */
  public void setPositionMessage(String positionMessage) {
    this.positionMessage = positionMessage;
  }
  
  /**
   * @return the positionMessage
   */
  public String getPositionMessage() {
    return positionMessage;
  }
  
  /**
   * @param largeurModule the largeurModule to set
   */
  public void setLargeurModule(float largeurModule) {
    this.largeurModule = largeurModule;
  }
  
  /**
   * @return the largeurModule
   */
  public float getLargeurModule() {
    return largeurModule;
  }
  
  /**
   * @param rotation the rotation to set
   */
  public void setRotation(int rotation) {
    this.rotation = rotation;
  }
  
  /**
   * @return the rotation
   */
  public int getRotation() {
    return rotation;
  }
  
  /**
   * Retourne une image pour le Rad
   * @param dpi_aff
   * @param dpi_img
   * @return
   *
   *         public GfxCodeBarre getGfxCodeBarre(int dpi_aff, int dpi_img, final GfxPageEditor planTravail)
   *         {
   *         GfxCodeBarre codebarre = new GfxCodeBarre(this, dpi_aff, dpi_img, planTravail);
   *         return codebarre;
   *         }
   */
  
}
