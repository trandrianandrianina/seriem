/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description;

import java.util.ArrayList;

//=================================================================================================
//==> Description d'une �dition d'un document
//=================================================================================================
public class DescriptionEdition {
  // Constantes
  public static final String EXTENSION = ".dde";
  public static final int NON_UTILISE = 0;
  public static final int VEILLEOUTQ = 1;
  public static final int DIRECTE = 2;
  public static final int LES_DEUX = 3;
  
  public static final int HOLD = 3;
  public static final int DEL = 4;
  public static final int RDY = 6; // Non utilis�
  
  public static final double VERSION = 1.20;
  
  // Variables
  private String nomFichierDDE = null; // Nom de cette description (le m�me que celui du fichier dde correspondant)
  private String nomSpool = null; // Nom du spool � r�cup�rer
  private String nomOutQueue = null; // Nom de la outqueue contenant le spool
  private ArrayList<Object> typeSortie = new ArrayList<Object>(); // Type de sortie: fichier, mail, impression, ...
  private int utilisation = NON_UTILISE; // Indique si cette description est utilis�e ou pas et comment
  private int actionSpool = HOLD; // Action sur le spool apr�s la transformation
  private double version = 0; // Version du protocole du fichier DDE (li� � la version du serveur NewSim)
  
  private transient String nomFM = null; // Nom de la biblioth�que de travail
  
  /**
   * @return the nom
   */
  public String getNomFichierDDE() {
    return nomFichierDDE;
  }
  
  /**
   * @param nom the nom to set
   */
  public void setNomFichierDDE(String nom) {
    this.nomFichierDDE = nom;
  }
  
  /**
   * @return the nomSpool
   */
  public String getNomSpool() {
    return nomSpool;
  }
  
  /**
   * @param nomSpool the nomSpool to set
   */
  public void setNomSpool(String nomSpool) {
    this.nomSpool = nomSpool;
  }
  
  /**
   * @return the nomOutQueue
   */
  public String getNomOutQueue() {
    return nomOutQueue;
  }
  
  /**
   * @param nomOutQueue the nomOutQueue to set
   */
  public void setNomOutQueue(String nomOutQueue) {
    this.nomOutQueue = nomOutQueue;
  }
  
  /**
   * @return the typeSortie
   */
  public ArrayList<Object> getTypeSortie() {
    return typeSortie;
  }
  
  /**
   * @param typeSortie the typeSortie to set
   */
  public void setTypeSortie(ArrayList<Object> typeSortie) {
    this.typeSortie = typeSortie;
  }
  
  /**
   * @param typeSortie the typeSortie to set
   */
  public void addTypeSortie(Object sortie) {
    this.typeSortie.add(sortie);
  }
  
  /**
   * @return the utilisation
   */
  public int getUtilisation() {
    return utilisation;
  }
  
  /**
   * @param utilisation the utilisation to set
   */
  public void setUtilisation(int utilisation) {
    this.utilisation = utilisation;
  }
  
  /**
   * @param actionSpool the actionSpool to set
   */
  public void setActionSpool(int actionSpool) {
    this.actionSpool = actionSpool;
  }
  
  /**
   * @return the actionSpool
   */
  public int getActionSpool() {
    return actionSpool;
  }
  
  /**
   * @param version the version to set
   */
  public void setVersion(double version) {
    this.version = version;
  }
  
  /**
   * @return the version
   */
  public double getVersion() {
    return version;
  }
  
  /**
   * @return the nomFM
   */
  public String getNomFM() {
    return nomFM;
  }
  
  /**
   * @param nomFM the nomFM to set
   */
  public void setNomFM(String nomFM) {
    this.nomFM = nomFM;
  }
}
