/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package description;

import java.util.ArrayList;

//=================================================================================================
//==> Description d'un document
//=================================================================================================
public class DescriptionDocument {
  // Constantes
  public static final String EXTENSION = ".ddd";
  
  // Variables
  private String nom = null; // A titre indicatif
  private DescriptionPage premiereDeCouverture = null;
  private DescriptionPage deuxiemeDeCouverture = null;
  private ArrayList<DescriptionPage> sequence = new ArrayList<DescriptionPage>();
  private DescriptionPage troisiemeDeCouverture = null;
  private DescriptionPage quatriemeDeCouverture = null;
  private int source_dpi = 90; // Dpi des svg & bitmap du fond de page
  private String commentaire = null;
  
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  /**
   * @return the nom
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * @return the premiereDeCouverture
   */
  public DescriptionPage getPremiereDeCouverture() {
    return premiereDeCouverture;
  }
  
  /**
   * @param premiereDeCouverture the premiereDeCouverture to set
   */
  public void setPremiereDeCouverture(DescriptionPage premiereDeCouverture) {
    this.premiereDeCouverture = premiereDeCouverture;
  }
  
  /**
   * @return the deuxiemeDeCouverture
   */
  public DescriptionPage getDeuxiemeDeCouverture() {
    return deuxiemeDeCouverture;
  }
  
  /**
   * @param deuxiemeDeCouverture the deuxiemeDeCouverture to set
   */
  public void setDeuxiemeDeCouverture(DescriptionPage deuxiemeDeCouverture) {
    this.deuxiemeDeCouverture = deuxiemeDeCouverture;
  }
  
  /**
   * @return the sequence
   */
  public ArrayList<DescriptionPage> getSequence() {
    return sequence;
  }
  
  /**
   * @param sequence the sequence to set
   */
  public void setSequence(ArrayList<DescriptionPage> sequence) {
    this.sequence = sequence;
  }
  
  /**
   * @param sequence the sequence to set
   */
  public void addSequence(DescriptionPage page) {
    // if (sequence == null) sequence = new ArrayList();
    sequence.add(page);
    // pageCourante = (DescriptionPage) sequence.get(sequence.size()-1);
  }
  
  /**
   * @return the troisiemeDeCouverture
   */
  public DescriptionPage getTroisiemeDeCouverture() {
    return troisiemeDeCouverture;
  }
  
  /**
   * @param troisiemeDeCouverture the troisiemeDeCouverture to set
   */
  public void setTroisiemeDeCouverture(DescriptionPage troisiemeDeCouverture) {
    this.troisiemeDeCouverture = troisiemeDeCouverture;
  }
  
  /**
   * @return the quatriemeDeCouverture
   */
  public DescriptionPage getQuatriemeDeCouverture() {
    return quatriemeDeCouverture;
  }
  
  /**
   * @param quatriemeDeCouverture the quatriemeDeCouverture to set
   */
  public void setQuatriemeDeCouverture(DescriptionPage quatriemeDeCouverture) {
    this.quatriemeDeCouverture = quatriemeDeCouverture;
  }
  
  /**
   * Recherche la premi�re description de page non nulle
   *
   * public void initPageCourante()
   * {
   * if (premiereDeCouverture != null) pageCourante = premiereDeCouverture;
   * else
   * if (deuxiemeDeCouverture != null) pageCourante = deuxiemeDeCouverture;
   * else
   * if ((sequence != null) && (sequence.size() > 0))
   * pageCourante = ((DescriptionPage)sequence.get(0));
   * else
   * if (troisiemeDeCouverture != null) pageCourante = (DescriptionPage) troisiemeDeCouverture;
   * else
   * if (quatriemeDeCouverture != null) pageCourante = (DescriptionPage) quatriemeDeCouverture;
   * else // Si on n'en trouve pas, on cr�� une page vide dans la s�quence (comportement par d�faut)
   * {
   * addSequence(new DescriptionPage());
   * pageCourante = (DescriptionPage)sequence.get(0);
   * }
   * }
   */
  
  /**
   * @param source_dpi the source_dpi to set
   */
  public void setSource_dpi(int source_dpi) {
    this.source_dpi = source_dpi;
  }
  
  /**
   * @return the source_dpi
   */
  public int getSource_dpi() {
    return source_dpi;
  }
  
  /**
   * @param commentaire the commentaire to set
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }
  
  /**
   * @return the commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }
  
  /**
   * @param cible_format the cible_format to set
   *
   *          public void setCible_format(String[] cible_format)
   *          {
   *          this.cible_format = cible_format;
   *          }
   * 
   *          /**
   * @return the cible_format
   *
   *         public String[] getCible_format()
   *         {
   *         return cible_format;
   *         }
   * 
   *         /**
   * @param cible_dpi the cible_dpi to set
   *
   *          public void setCible_dpi(int[] cible_dpi)
   *          {
   *          this.cible_dpi = cible_dpi;
   *          }
   * 
   *          /**
   * @return the cible_dpi
   *
   *         public int[] getCible_dpi()
   *         {
   *         return cible_dpi;
   *         }
   */
  
}
