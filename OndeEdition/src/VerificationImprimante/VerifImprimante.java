/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package VerificationImprimante;

import com.ibm.as400.access.OutputQueue;
import com.ibm.as400.access.SpooledFile;

import spool.GestionSpoolAS400;

//=================================================================================================
//==> Permet de tester si l'imprimante est capable d'imprimer du ps, pdf
//=================================================================================================
public class VerifImprimante {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    if (args.length < 2) {
      System.out.println("--> Usage : java -jar VerifImprimante.jar fichier.[ps/pdf] outq_imprimante");
      return;
    }
    
    String fichier = args[0];
    String nomoutq = args[1];
    GestionSpoolAS400 gsa = new GestionSpoolAS400();
    OutputQueue outq = new OutputQueue(gsa.getSystem(), nomoutq);
    gsa.setOutq(outq);
    SpooledFile spl = gsa.cpyFichier2Spool(fichier, "testprt", 1, false, null, null, null, null);
    if (spl == null)
      System.out.println("Erreur: " + gsa.getMsgErreur());
    System.out.println("Transfert dans la outq termin�.");
    gsa.getSystem().disconnectAllServices();
  }
  
}
