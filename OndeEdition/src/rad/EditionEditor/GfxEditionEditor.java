/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.EditionEditor;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.text.AbstractDocument;

import org.jdesktop.swingx.VerticalLayout;

import base.SaisieDefinition;
import description.DescriptionEdition;
import description.sortie.Sortie;
import description.sortie.SortieFichier;
import description.sortie.SortieImprimante;
import rad.Parametres.Preferences;
import rad.outils.RiFileChooser;
import ri.seriem.libcommun.outils.LinkedHashMapManager;
import ri.seriem.libcommun.outils.LinkedHashMapManager.lhdListener;
import ri.seriem.libcommun.outils.XMLTools;

//=================================================================================================
//==> Editeur de document
//=================================================================================================
public class GfxEditionEditor extends JFrame {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private File fichierdde = null;
  private Preferences prefs = null;
  private DescriptionEdition edition = null;
  private File dernierPath = null;
  private LinkedHashMapManager listeSortie = new LinkedHashMapManager();
  private GfxEditionEditor cetteFenetre = this;
  
  public GfxEditionEditor(String ficdde, Preferences prefs) {
    super();
    initComponents();
    setVisible(true);
    
    this.prefs = prefs;
    if (prefs != null)
      dernierPath = new File(prefs.getDernierPath());
    else
      dernierPath = new File(System.getProperty("user.dir"));
    
    // On ouvre un document d�j� existant
    if (ficdde != null) {
      fichierdde = new File(ficdde);
      // dossierdde = fichierdde.getParentFile();
      chargementEdition(fichierdde);
      initEdition();
    }
    else
      lst_TypeSortie.setModel(new DefaultListModel());
    ((AbstractDocument) tf_NomSpool.getDocument()).setDocumentFilter(new SaisieDefinition(10, true));
    ((AbstractDocument) tf_Outqueue.getDocument()).setDocumentFilter(new SaisieDefinition(255, true));
    
    // Cr�ation du manager pour les sorties
    listeSortie.addListener(new lhdListener() {
      public void onDataCleared() {
      }
      
      public void onDataRemoved(Object cle) {
        majListe(cle, null);
      }
      
      public void onDataAdded(Object cle, Object val) {
        majListe(cle, val);
      }
    });
  }
  
  /**
   * Chargement de la description
   * @param fichierdde
   */
  private void chargementEdition(File fichierdde) {
    if ((fichierdde == null) || (!fichierdde.exists()))
      return;
    
    // Lecture du fichier dde
    try {
      edition = (DescriptionEdition) XMLTools.decodeFromFile(fichierdde.getAbsolutePath());
      ArrayList<String> listeException = XMLTools.getListeMsgExceptionDecoder();
      if ((listeException != null) && (listeException.size() > 0))
        gestionVersionAnterieureDDE(listeException);
      
    }
    catch (Exception e) {
    }
    dernierPath = fichierdde.getParentFile();
  }
  
  /**
   * Initalise les donn�es � partir de la description
   */
  private void initEdition() {
    if (edition == null)
      return;
    
    switch (edition.getUtilisation()) {
      case DescriptionEdition.NON_UTILISE:
        rb_NonUtilise.setSelected(true);
        break;
      case DescriptionEdition.VEILLEOUTQ:
        rb_VeilleOutq.setSelected(true);
        break;
      case DescriptionEdition.DIRECTE:
        rb_Directe.setSelected(true);
        break;
      case DescriptionEdition.LES_DEUX:
        rb_LesDeux.setSelected(true);
        break;
    }
    if (edition.getNomSpool() != null)
      tf_NomSpool.setText(edition.getNomSpool());
    
    if (edition.getNomOutQueue() != null)
      tf_Outqueue.setText(edition.getNomOutQueue());
    if (edition.getActionSpool() == DescriptionEdition.DEL)
      cb_ActionSpool.setSelectedIndex(1);
    else
      cb_ActionSpool.setSelectedIndex(0);
    // if (edition.getFichierDescriptionDocument() != null) tf_Ddd.setText(edition.getFichierDescriptionDocument());
    
    // On remplit la liste avec les types de sortie trouv�s
    DefaultListModel listModel = new DefaultListModel();
    for (int i = 0; i < edition.getTypeSortie().size(); i++)
      if (edition.getTypeSortie().get(i) instanceof SortieFichier) {
        listeSortie.addObject(((SortieFichier) edition.getTypeSortie().get(i)).getDescription(), edition.getTypeSortie().get(i));
        listModel.addElement(((SortieFichier) edition.getTypeSortie().get(i)).getDescription());
      }
      else if (edition.getTypeSortie().get(i) instanceof SortieImprimante) {
        listeSortie.addObject(((SortieImprimante) edition.getTypeSortie().get(i)).getDescription(), edition.getTypeSortie().get(i));
        listModel.addElement(((SortieImprimante) edition.getTypeSortie().get(i)).getDescription());
      }
    
    lst_TypeSortie.setModel(listModel);
  }
  
  /**
   * Validation des donn�es
   * @return
   */
  private boolean validEdition() {
    if (edition == null)
      edition = new DescriptionEdition();
    
    if (rb_NonUtilise.isSelected())
      edition.setUtilisation(DescriptionEdition.NON_UTILISE);
    else if (rb_VeilleOutq.isSelected())
      edition.setUtilisation(DescriptionEdition.VEILLEOUTQ);
    else if (rb_Directe.isSelected())
      edition.setUtilisation(DescriptionEdition.DIRECTE);
    else if (rb_LesDeux.isSelected())
      edition.setUtilisation(DescriptionEdition.LES_DEUX);
    edition.setNomSpool(tf_NomSpool.getText().trim());
    edition.setNomOutQueue(tf_Outqueue.getText().trim());
    edition.setActionSpool(cb_ActionSpool.getSelectedIndex() == 0 ? DescriptionEdition.HOLD : DescriptionEdition.DEL);
    // edition.setFichierDescriptionDocument(tf_Ddd.getText().trim());
    
    edition.getTypeSortie().clear();
    Iterator<?> i = listeSortie.getHashMap().keySet().iterator();
    while (i.hasNext()) {
      Object clef = i.next();
      edition.addTypeSortie(listeSortie.getHashMap().get(clef));
    }
    
    // Enregistrement de la version du fichier DDE (version du serveur NewSim)
    edition.setVersion(DescriptionEdition.VERSION);
    
    return true;
  }
  
  /**
   * Met � jour la liste des sorties
   * @param cle
   * @param val
   */
  private void majListe(Object cle, Object val) {
    if (val == null) // Suppression
      ((DefaultListModel) lst_TypeSortie.getModel()).removeElementAt(lst_TypeSortie.getSelectedIndex());
    else
      ((DefaultListModel) lst_TypeSortie.getModel()).addElement(cle);
  }
  
  /**
   * Ouvre la fen�tre de sauvegarde de la description d'�dition
   * @return
   */
  private String saveDDE(boolean affichedialog) {
    if (fichierdde == null)
      fichierdde = dernierPath;
    RiFileChooser selectionFichier = new RiFileChooser(this, fichierdde);
    
    // On v�rifie qu'on ait bien un nom de fichier
    if ((!affichedialog) && (!fichierdde.getName().endsWith(DescriptionEdition.EXTENSION)))
      affichedialog = true;
    
    if (fichierdde.isDirectory())
      affichedialog = true;
    
    // Affiche la boite de dialogue si besoin
    if (affichedialog) {
      fichierdde = selectionFichier.enregistreFichier(fichierdde, "dde", "Fichier description d'�dition (*.dde)");
      if (fichierdde == null)
        return null;
    }
    
    // V�rifie qu'il ait bien l'extension
    if (fichierdde.getName().lastIndexOf('.') == -1)
      fichierdde = new File(fichierdde.getAbsolutePath() + DescriptionEdition.EXTENSION);
    
    // G�n�ration du fichier XML
    try {
      if (validEdition()) {
        edition.setNomFichierDDE(fichierdde.getName());
        XMLTools.encodeToFile(edition, fichierdde.getAbsolutePath());
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    
    if (prefs != null)
      prefs.setDernierPath(fichierdde.getParent());
    return fichierdde.getAbsolutePath();
  }
  
  /**
   * @return the listeSortie
   */
  public LinkedHashMapManager getListeSortie() {
    return listeSortie;
  }
  
  /**
   * G�re les versions ant�rieures de DDE
   */
  private void gestionVersionAnterieureDDE(ArrayList<String> listeException) {
    if (edition == null)
      return;
    
    // Affichage d'un message d'avertissement
    JOptionPane.showMessageDialog(this,
        "Vous avez charg� un fichier DDE en version " + edition.getVersion()
            + " donc ant�rieure � la version courante,\nil va �tre converti pour la nouvelle version en " + DescriptionEdition.VERSION
            + ".\nV�rifier que toutes informations aient bien �t� r�cup�r�es.",
        "Attention !", JOptionPane.WARNING_MESSAGE);
    
    // Pour les versions ant�rieures � la 1.20 (grosses modifications afin d'int�grer la gestion du multi-document)
    if (edition.getVersion() == 0)
      gestionVersionDDE0To1_20(listeException);
  }
  
  /**
   * G�re la conversion de la version 0 � la version 1.20
   * @param listeException
   */
  private void gestionVersionDDE0To1_20(ArrayList<String> listeException) {
    int posd = 0, posf = 0;
    
    // On a d�plac� 3 variables: fichierDescriptionDocument, typeDoc, dpiSortie vers la description de sortie
    String nomDDD = null;
    String typeDoc = "";
    int dpiSortie = 72;
    
    // On parcourt la liste des m�thodes non trouv�es
    for (int i = 0; i < listeException.size(); i++) {
      // R�cup�ration du nom du fichier DDD
      if (listeException.get(i).indexOf("DescriptionEdition.setFichierDescriptionDocument") != -1) {
        posd = listeException.get(i).indexOf('"') + 1;
        posf = listeException.get(i).lastIndexOf('"');
        nomDDD = listeException.get(i).substring(posd, posf);
      }
      else
      // R�cup�ration du nom type de doc � g�n�rer
      if (listeException.get(i).indexOf("DescriptionEdition.setTypeDoc") != -1) {
        posd = listeException.get(i).indexOf('"') + 1;
        posf = listeException.get(i).lastIndexOf('"');
        typeDoc = listeException.get(i).substring(posd, posf);
      }
      else
      // R�cup�ration du nombre de dpi du doc
      if (listeException.get(i).indexOf("<unbound>=DescriptionEdition.setDpiSortie") != -1) {
        posd = listeException.get(i).indexOf('(') + 1;
        posf = listeException.get(i).lastIndexOf(')');
        dpiSortie = Integer.parseInt(listeException.get(i).substring(posd, posf));
      }
    }
    listeException.clear();
    listeException = null;
    
    // On met � jour les variables de la nouvelle version
    for (int i = 0; i < edition.getTypeSortie().size(); i++) {
      ((Sortie) edition.getTypeSortie().get(i)).setFichierDescriptionDocument(nomDDD);
      ((Sortie) edition.getTypeSortie().get(i)).setTypeDoc(typeDoc);
      ((Sortie) edition.getTypeSortie().get(i)).setDpiSortie(dpiSortie);
    }
  }
  
  // -------------------------- Ev�nementiel --------------------------------
  
  private void bt_AjouterActionPerformed(ActionEvent e) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        if (cb_TypeSortie.getSelectedIndex() == 0)
          new GfxSortieFichier(cetteFenetre, new SortieFichier());// ,
                                                                  // fichierdde!=null?fichierdde.getParent()+File.separatorChar+tf_Ddd.getText():null);
        else if (cb_TypeSortie.getSelectedIndex() == 1)
          new GfxSortieImprimante(cetteFenetre, new SortieImprimante());
      }
    });
  }
  
  private void bt_ModifierActionPerformed(ActionEvent e) {
    if (lst_TypeSortie.getSelectedIndex() == -1)
      return;
    
    String type = (String) lst_TypeSortie.getSelectedValue();
    if (listeSortie.getObject(type) instanceof SortieFichier) {
      SortieFichier sf = (SortieFichier) listeSortie.getObject(type);
      if (sf != null)
        new GfxSortieFichier(this, sf);// , fichierdde!=null?fichierdde.getParent()+File.separatorChar+tf_Ddd.getText():null);
    }
    else if (listeSortie.getObject(type) instanceof SortieImprimante) {
      SortieImprimante si = (SortieImprimante) listeSortie.getObject(type);
      if (si != null)
        new GfxSortieImprimante(this, si);
    }
  }
  
  private void bt_SupprimerActionPerformed(ActionEvent e) {
    if (lst_TypeSortie.getSelectedIndex() == -1)
      return;
    
    String type = (String) lst_TypeSortie.getSelectedValue();
    listeSortie.removeObject(type);
  }
  
  private void bt_AnnulerActionPerformed(ActionEvent e) {
    dispose();
  }
  
  /* Ne sert plus
  private void cb_ExtItemStateChanged(ItemEvent e) {
  	Iterator<?> i = listeSortie.getHashMap().keySet().iterator();
  	while (i.hasNext())
  	{
  		Object clef = i.next();
  		((Sortie)listeSortie.getHashMap().get(clef)).setExtension((String) cb_Ext.getSelectedItem());
  	}
  	majDpi();
  	bt_Ajouter.setEnabled(isImpressionPossible());
  }*/
  
  private void bt_ValiderActionPerformed(ActionEvent e) {
    saveDDE(true);
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_Utilisation = new JPanel();
    rb_NonUtilise = new JRadioButton();
    rb_VeilleOutq = new JRadioButton();
    rb_Directe = new JRadioButton();
    rb_LesDeux = new JRadioButton();
    p_Spool = new JPanel();
    l_NomSpool = new JLabel();
    tf_NomSpool = new JTextField();
    l_Outqueue = new JLabel();
    tf_Outqueue = new JTextField();
    l_ActionSpool = new JLabel();
    cb_ActionSpool = new JComboBox();
    p_Sortie = new JPanel();
    l_TypeSortie = new JLabel();
    cb_TypeSortie = new JComboBox();
    scrollPane1 = new JScrollPane();
    lst_TypeSortie = new JList();
    bt_Ajouter = new JButton();
    bt_Modifier = new JButton();
    bt_Supprimer = new JButton();
    p_Pied = new JPanel();
    bt_Valider = new JButton();
    bt_Annuler = new JButton();
    
    // ======== this ========
    setTitle("Editeur d'\u00e9dition");
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new VerticalLayout());
    
    // ======== p_Utilisation ========
    {
      p_Utilisation.setBorder(new TitledBorder("Utilisation"));
      p_Utilisation.setName("p_Utilisation");
      p_Utilisation.setLayout(null);
      
      // ---- rb_NonUtilise ----
      rb_NonUtilise.setText("Non utilis\u00e9");
      rb_NonUtilise.setName("rb_NonUtilise");
      p_Utilisation.add(rb_NonUtilise);
      rb_NonUtilise.setBounds(new Rectangle(new Point(25, 30), rb_NonUtilise.getPreferredSize()));
      
      // ---- rb_VeilleOutq ----
      rb_VeilleOutq.setText("VeilleOutq");
      rb_VeilleOutq.setName("rb_VeilleOutq");
      p_Utilisation.add(rb_VeilleOutq);
      rb_VeilleOutq.setBounds(new Rectangle(new Point(133, 30), rb_VeilleOutq.getPreferredSize()));
      
      // ---- rb_Directe ----
      rb_Directe.setText("Directe");
      rb_Directe.setName("rb_Directe");
      p_Utilisation.add(rb_Directe);
      rb_Directe.setBounds(new Rectangle(new Point(255, 30), rb_Directe.getPreferredSize()));
      
      // ---- rb_LesDeux ----
      rb_LesDeux.setText("Les 2");
      rb_LesDeux.setName("rb_LesDeux");
      p_Utilisation.add(rb_LesDeux);
      rb_LesDeux.setBounds(new Rectangle(new Point(355, 30), rb_LesDeux.getPreferredSize()));
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_Utilisation.getComponentCount(); i++) {
          Rectangle bounds = p_Utilisation.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Utilisation.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Utilisation.setMinimumSize(preferredSize);
        p_Utilisation.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(p_Utilisation);
    
    // ======== p_Spool ========
    {
      p_Spool.setBorder(new TitledBorder("Spool \u00e0 rechercher"));
      p_Spool.setName("p_Spool");
      p_Spool.setLayout(null);
      
      // ---- l_NomSpool ----
      l_NomSpool.setText("Nom du spool");
      l_NomSpool.setName("l_NomSpool");
      p_Spool.add(l_NomSpool);
      l_NomSpool.setBounds(new Rectangle(new Point(25, 30), l_NomSpool.getPreferredSize()));
      
      // ---- tf_NomSpool ----
      tf_NomSpool.setName("tf_NomSpool");
      p_Spool.add(tf_NomSpool);
      tf_NomSpool.setBounds(145, 24, 100, tf_NomSpool.getPreferredSize().height);
      
      // ---- l_Outqueue ----
      l_Outqueue.setText("OutQueue");
      l_Outqueue.setName("l_Outqueue");
      p_Spool.add(l_Outqueue);
      l_Outqueue.setBounds(new Rectangle(new Point(25, 57), l_Outqueue.getPreferredSize()));
      
      // ---- tf_Outqueue ----
      tf_Outqueue.setName("tf_Outqueue");
      p_Spool.add(tf_Outqueue);
      tf_Outqueue.setBounds(145, 51, 305, tf_Outqueue.getPreferredSize().height);
      
      // ---- l_ActionSpool ----
      l_ActionSpool.setText("Action sur le spool apr\u00e8s \u00e9dition");
      l_ActionSpool.setName("l_ActionSpool");
      p_Spool.add(l_ActionSpool);
      l_ActionSpool.setBounds(new Rectangle(new Point(25, 84), l_ActionSpool.getPreferredSize()));
      
      // ---- cb_ActionSpool ----
      cb_ActionSpool.setModel(new DefaultComboBoxModel(new String[] { "Suspendre", "Supprimer" }));
      cb_ActionSpool.setName("cb_ActionSpool");
      p_Spool.add(cb_ActionSpool);
      cb_ActionSpool.setBounds(315, 80, 135, cb_ActionSpool.getPreferredSize().height);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_Spool.getComponentCount(); i++) {
          Rectangle bounds = p_Spool.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Spool.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Spool.setMinimumSize(preferredSize);
        p_Spool.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(p_Spool);
    
    // ======== p_Sortie ========
    {
      p_Sortie.setBorder(new TitledBorder("Sortie"));
      p_Sortie.setName("p_Sortie");
      p_Sortie.setLayout(null);
      
      // ---- l_TypeSortie ----
      l_TypeSortie.setText("Type de sortie");
      l_TypeSortie.setName("l_TypeSortie");
      p_Sortie.add(l_TypeSortie);
      l_TypeSortie.setBounds(new Rectangle(new Point(20, 35), l_TypeSortie.getPreferredSize()));
      
      // ---- cb_TypeSortie ----
      cb_TypeSortie.setModel(new DefaultComboBoxModel(new String[] { "Fichier", "Impression" }));
      cb_TypeSortie.setName("cb_TypeSortie");
      p_Sortie.add(cb_TypeSortie);
      cb_TypeSortie.setBounds(350, 60, 120, cb_TypeSortie.getPreferredSize().height);
      
      // ======== scrollPane1 ========
      {
        scrollPane1.setName("scrollPane1");
        
        // ---- lst_TypeSortie ----
        lst_TypeSortie.setName("lst_TypeSortie");
        scrollPane1.setViewportView(lst_TypeSortie);
      }
      p_Sortie.add(scrollPane1);
      scrollPane1.setBounds(20, 55, 205, 95);
      
      // ---- bt_Ajouter ----
      bt_Ajouter.setText("Ajouter");
      bt_Ajouter.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Ajouter.setName("bt_Ajouter");
      bt_Ajouter.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_AjouterActionPerformed(e);
        }
      });
      p_Sortie.add(bt_Ajouter);
      bt_Ajouter.setBounds(245, 60, 98, bt_Ajouter.getPreferredSize().height);
      
      // ---- bt_Modifier ----
      bt_Modifier.setText("Modifier");
      bt_Modifier.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Modifier.setName("bt_Modifier");
      bt_Modifier.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_ModifierActionPerformed(e);
        }
      });
      p_Sortie.add(bt_Modifier);
      bt_Modifier.setBounds(245, 90, 98, bt_Modifier.getPreferredSize().height);
      
      // ---- bt_Supprimer ----
      bt_Supprimer.setText("Supprimer");
      bt_Supprimer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Supprimer.setName("bt_Supprimer");
      bt_Supprimer.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_SupprimerActionPerformed(e);
        }
      });
      p_Sortie.add(bt_Supprimer);
      bt_Supprimer.setBounds(245, 120, 98, bt_Supprimer.getPreferredSize().height);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_Sortie.getComponentCount(); i++) {
          Rectangle bounds = p_Sortie.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Sortie.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Sortie.setMinimumSize(preferredSize);
        p_Sortie.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(p_Sortie);
    
    // ======== p_Pied ========
    {
      p_Pied.setName("p_Pied");
      p_Pied.setLayout(new FlowLayout(FlowLayout.RIGHT));
      
      // ---- bt_Valider ----
      bt_Valider.setText("Valider");
      bt_Valider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Valider.setName("bt_Valider");
      bt_Valider.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_ValiderActionPerformed(e);
        }
      });
      p_Pied.add(bt_Valider);
      
      // ---- bt_Annuler ----
      bt_Annuler.setText("Annuler");
      bt_Annuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Annuler.setName("bt_Annuler");
      bt_Annuler.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_AnnulerActionPerformed(e);
        }
      });
      p_Pied.add(bt_Annuler);
    }
    contentPane.add(p_Pied);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rb_NonUtilise);
    buttonGroup1.add(rb_VeilleOutq);
    buttonGroup1.add(rb_Directe);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_Utilisation;
  private JRadioButton rb_NonUtilise;
  private JRadioButton rb_VeilleOutq;
  private JRadioButton rb_Directe;
  private JRadioButton rb_LesDeux;
  private JPanel p_Spool;
  private JLabel l_NomSpool;
  private JTextField tf_NomSpool;
  private JLabel l_Outqueue;
  private JTextField tf_Outqueue;
  private JLabel l_ActionSpool;
  private JComboBox cb_ActionSpool;
  private JPanel p_Sortie;
  private JLabel l_TypeSortie;
  private JComboBox cb_TypeSortie;
  private JScrollPane scrollPane1;
  private JList lst_TypeSortie;
  private JButton bt_Ajouter;
  private JButton bt_Modifier;
  private JButton bt_Supprimer;
  private JPanel p_Pied;
  private JButton bt_Valider;
  private JButton bt_Annuler;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
