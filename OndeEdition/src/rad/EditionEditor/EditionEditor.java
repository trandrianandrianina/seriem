/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.EditionEditor;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import ri.seriem.libcommun.outils.GestionFiltre;

//=================================================================================================
//==> Editeur des �ditions en mode solo
//=================================================================================================
public class EditionEditor {
  // Variables
  private File dernierPath = null;
  
  /**
   * Constructeur
   */
  public EditionEditor() {
    String fichierdde = ouvrir("dde", "Fichier description d'�dition (*.dde)");
    if (fichierdde != null)
      new GfxEditionEditor(fichierdde, null);
    else
      new GfxEditionEditor(null, null);
  }
  
  /**
   * Recherche le fichier � ouvrir
   */
  private String ouvrir(String extension, String extdescription) {
    String chemin = null;
    final JFileChooser choixFichier = new JFileChooser();
    // choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
    
    choixFichier.addChoosableFileFilter(new GestionFiltre(new String[] { extension }, extdescription));
    // On se place dans le dernier dossier courant s'il y en a un
    if (dernierPath != null)
      choixFichier.setCurrentDirectory(dernierPath);
    
    // Recuperation du chemin
    if (choixFichier.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
      chemin = choixFichier.getSelectedFile().getPath();
    else
      return null;
    
    // On stocke comme le dossier courant
    dernierPath = choixFichier.getCurrentDirectory();
    
    return chemin;
  }
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    // Utilisation du Look & Feel Nimbus
    try {
      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
      // UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {
    }
    
    // D�marrage du programme
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        // PreferencesManager.removeFile();
        new EditionEditor();
      }
    });
  }
  
}
