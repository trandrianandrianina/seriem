/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.EditionEditor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AbstractDocument;

import base.SaisieDefinition;
import description.sortie.Sortie;
import description.sortie.SortieImprimante;

//=================================================================================================
//==> Description de la sortie imprimante
//=================================================================================================
public class GfxSortieImprimante extends JDialog {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private GfxEditionEditor editionEditor = null;
  private SortieImprimante sortieImprimante = null;
  private boolean creation = true;
  
  public GfxSortieImprimante(Frame owner, SortieImprimante sortieimprimante) {
    super(owner);
    initComponents();
    setVisible(true);
    editionEditor = (GfxEditionEditor) owner;
    sortieImprimante = sortieimprimante;
    initData();
    majDpi();
    tf_Description.setEditable(creation);
  }
  
  /**
   * Initialisation des donn�es de la fen�tre
   */
  private void initData() {
    if (sortieImprimante == null)
      return;
    
    if (sortieImprimante.getTypeDoc() != null) {
      cb_Ext.setSelectedItem(sortieImprimante.getTypeDoc());
      tf_Dpi.setText("" + sortieImprimante.getDpiSortie());
    }
    if (sortieImprimante.getFichierDescriptionDocument() != null)
      tf_Ddd.setText(sortieImprimante.getFichierDescriptionDocument());
    
    if (sortieImprimante.getDescription() != null) {
      tf_Description.setText(sortieImprimante.getDescription());
      creation = false;
    }
    if (sortieImprimante.getNomSpool() != null)
      tf_NomSpool.setText(sortieImprimante.getNomSpool());
    ((AbstractDocument) tf_NomSpool.getDocument()).setDocumentFilter(new SaisieDefinition(10, true));
    
    if (sortieImprimante.getNomOutq() != null)
      tf_NomOutQ.setText(sortieImprimante.getNomOutq());
    ((AbstractDocument) tf_NomOutQ.getDocument()).setDocumentFilter(new SaisieDefinition(255, true));
    
    switch (sortieImprimante.getTypeScript()) {
      case SortieImprimante.SCRIPT_AUCUN:
        rb_ScriptAucun.setSelected(true);
        break;
      case SortieImprimante.SCRIPT_PJL:
        rb_ScriptPjl.setSelected(true);
        addPanelScriptPjl();
        break;
      case SortieImprimante.SCRIPT_PERSO:
        rb_ScriptPerso.setSelected(true);
        addPanelScriptPerso();
        break;
    }
    
    if (sortieImprimante.getNbrExemplaire() > 0)
      sp_NbrExemplaire.setValue(sortieImprimante.getNbrExemplaire());
    if (sortieImprimante.getTiroirEntree() > 0)
      cb_TiroirEntree.setSelectedIndex(sortieImprimante.getTiroirEntree() - 1);
    else
      cb_TiroirEntree.setSelectedIndex(0);
    cb_TiroirSortie.setSelectedIndex(sortieImprimante.getTiroirSortie());
    chk_RectoVerso.setSelected(sortieImprimante.isRectoverso());
    chk_ShortEdge.setEnabled(chk_RectoVerso.isSelected());
    chk_ShortEdge.setSelected(sortieImprimante.isShortedge());
    chk_Economie.setSelected(sortieImprimante.isEconomie());
    cb_Densite.setSelectedIndex(sortieImprimante.getDensite() - 1);
    chk_NoirBlanc.setSelected(sortieImprimante.isNoirblanc());
    if (sortieImprimante.getScriptAvant() != null)
      tp_DebutScript.setText(sortieImprimante.getScriptAvant());
    if (sortieImprimante.getScriptApres() != null)
      tp_FinScript.setText(sortieImprimante.getScriptApres());
  }
  
  /**
   * R�cup�re les donn�es
   */
  private boolean validData() {
    if (editionEditor == null)
      return false;
    if (sortieImprimante == null)
      return false;
    if (tf_Description.getText().trim().equals(""))
      return false;
    
    sortieImprimante.setTypeDoc((String) cb_Ext.getSelectedItem());
    if (!tf_Dpi.getText().trim().equals(""))
      sortieImprimante.setDpiSortie(Integer.parseInt(tf_Dpi.getText().trim()));
    
    sortieImprimante.setFichierDescriptionDocument(tf_Ddd.getText().trim());
    
    sortieImprimante.setDescription(tf_Description.getText().trim());
    sortieImprimante.setNomSpool(tf_NomSpool.getText().trim());
    sortieImprimante.setNomOutq(tf_NomOutQ.getText().trim());
    
    sortieImprimante.setNbrExemplaire((Integer) sp_NbrExemplaire.getValue());
    
    if (rb_ScriptAucun.isSelected()) {
      sortieImprimante.setTypeScript(SortieImprimante.SCRIPT_AUCUN);
      sortieImprimante.setTiroirEntree(1);
      sortieImprimante.setRectoverso(false);
      sortieImprimante.setEconomie(false);
      sortieImprimante.setDensite(3);
      sortieImprimante.setNoirblanc(false);
      sortieImprimante.setLgimprimante(SortieImprimante.LG_AUCUN);
      sortieImprimante.genereScript();
    }
    else if (rb_ScriptPjl.isSelected()) {
      sortieImprimante.setTypeScript(SortieImprimante.SCRIPT_PJL);
      sortieImprimante.setTiroirEntree(cb_TiroirEntree.getSelectedIndex() + 1);
      sortieImprimante.setTiroirSortie(cb_TiroirSortie.getSelectedIndex());
      sortieImprimante.setRectoverso(chk_RectoVerso.isSelected());
      sortieImprimante.setShortedge(chk_ShortEdge.isSelected());
      sortieImprimante.setEconomie(chk_Economie.isSelected());
      sortieImprimante.setDensite(cb_Densite.getSelectedIndex() + 1);
      sortieImprimante.setNoirblanc(chk_NoirBlanc.isSelected());
      // sortieImprimante.setScriptAvant(genereScriptDebutPjl());
      // sortieImprimante.setScriptApres(genereScriptFinPjl());
      sortieImprimante.setLgimprimante(SortieImprimante.LG_PJL);
      sortieImprimante.genereScript();
    }
    else {
      sortieImprimante.setTypeScript(SortieImprimante.SCRIPT_PERSO);
      sortieImprimante.setTiroirEntree(1);
      // sortieImprimante.setRectoverso(false);
      // sortieImprimante.setEconomie(false);
      // sortieImprimante.setDensite(3);
      if (tp_DebutScript.getText().trim().equals(""))
        sortieImprimante.setScriptAvant(null);
      else
        sortieImprimante.setScriptAvant(tp_DebutScript.getText());
      if (tp_FinScript.getText().trim().equals(""))
        sortieImprimante.setScriptApres(null);
      else
        sortieImprimante.setScriptApres(tp_FinScript.getText());
    }
    
    if (creation)
      editionEditor.getListeSortie().addObject(sortieImprimante.getDescription(), sortieImprimante);
    
    return true;
  }
  
  /**
   * G�n�ration du script PJL
   *
   * private String genereScriptDebutPjl()
   * {
   * StringBuffer script = new StringBuffer();
   * 
   * script.append(SortieImprimante.ESC).append("%-12345X@PJL JOB").append(Constantes.crlf);
   * script.append("@PJL SET MANUALFEED=ON").append(Constantes.crlf);
   * 
   * if (chk_Economie.isSelected()) script.append("@PJL SET ECONOMODE=ON").append(Constantes.crlf);
   * script.append("@PJL SET DENSITY=").append(cb_Densite.getSelectedIndex()+1).append(Constantes.crlf);
   * if (chk_NoirBlanc.isSelected()) script.append("@PJL SET RENDERMODE=GRAYSCALE").append(Constantes.crlf);
   * script.append("@PJL SET MEDIASOURCE=TRAY").append(cb_TiroirEntree.getSelectedIndex()+1).append(Constantes.crlf);
   * script.append("@PJL SET LPAPERSOURCE=TRAY").append(cb_TiroirEntree.getSelectedIndex()+1).append(Constantes.crlf);
   * 
   * if (sortieImprimante.getExtension().equals(Sortie.PDF))
   * script.append("@PJL ENTER LANGUAGE=PDF").append(Constantes.crlf);
   * else
   * if (sortieImprimante.getExtension().equals(Sortie.PS))
   * script.append("@PJL ENTER LANGUAGE=POSTSCRIPT").append(Constantes.crlf);
   * 
   * return script.toString();
   * }
   * 
   * /**
   * G�n�ration du script PJL
   *
   * private String genereScriptFinPjl()
   * {
   * StringBuffer script = new StringBuffer();
   * 
   * script.append("@PJL SET MANUALFEED=OFF").append(Constantes.crlf);
   * script.append("@PJL RESET").append(Constantes.crlf);
   * script.append(SortieImprimante.ESC).append("%-12345X@PJL EOJ").append(Constantes.crlf);
   * script.append(SortieImprimante.ESC).append("%-12345X").append(Constantes.crlf);
   * 
   * return script.toString();
   * }
   */
  
  /**
   * Mise � jour de l'�tat des zones concerant le dpi
   */
  private void majDpi() {
    boolean result = !(cb_Ext.getSelectedItem().equals(Sortie.PDF) || cb_Ext.getSelectedItem().equals(Sortie.PS));
    l_Dpi.setEnabled(result);
    tf_Dpi.setEnabled(result);
  }
  
  /**
   * Enl�ve tous les panels propres aux scripts
   */
  private void removeAllPanelScript() {
    setPreferredSize(new Dimension(555, 290));
    setSize(555, 290);
    contentPanel.remove(p_ScriptPjl);
    contentPanel.remove(p_ScriptPerso);
    p_ScriptPjl.setVisible(false);
    p_ScriptPerso.setVisible(false);
    pack();
    repaint();
  }
  
  /**
   * Ajoute le panel pour les scripts PJL
   */
  private void addPanelScriptPjl() {
    setPreferredSize(new Dimension(555, 605));
    setSize(555, 605);
    contentPanel.add(p_ScriptPjl);
    p_ScriptPjl.setVisible(true);
    p_ScriptPjl.setBounds(20, 200, 480, 300);
    validate();
    repaint();
  }
  
  /**
   * Ajoute le panel pour les scripts personnalis�s
   */
  private void addPanelScriptPerso() {
    setPreferredSize(new Dimension(555, 605));
    setSize(555, 605);
    contentPanel.add(p_ScriptPerso);
    p_ScriptPerso.setVisible(true);
    p_ScriptPerso.setBounds(20, 200, 480, 300);
    validate();
    repaint();
  }
  
  private void bt_ValiderActionPerformed(ActionEvent e) {
    if (validData())
      dispose();
    else
      JOptionPane.showMessageDialog(this, "Saisissez une description", "Erreur", JOptionPane.ERROR_MESSAGE);
  }
  
  private void bt_AnnulerActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void bt_GenererScriptActionPerformed(ActionEvent e) {
    validData();
    tp_DebutScript.setText(sortieImprimante.getScriptAvant());
    tp_FinScript.setText(sortieImprimante.getScriptApres());
    // tp_DebutScript.setText(genereScriptDebutPjl());
    // tp_FinScript.setText(genereScriptFinPjl());
  }
  
  private void rb_ScriptAucunActionPerformed(ActionEvent e) {
    removeAllPanelScript();
  }
  
  private void rb_ScriptPjlActionPerformed(ActionEvent e) {
    removeAllPanelScript();
    addPanelScriptPjl();
  }
  
  private void rb_ScriptPersoActionPerformed(ActionEvent e) {
    removeAllPanelScript();
    addPanelScriptPerso();
  }
  
  private void chk_RectoVersoStateChanged(ChangeEvent e) {
    chk_ShortEdge.setEnabled(chk_RectoVerso.isSelected());
  }
  
  private void cb_ExtItemStateChanged(ItemEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    l_Description = new JLabel();
    tf_Description = new JTextField();
    l_NomSpool = new JLabel();
    tf_NomSpool = new JTextField();
    l_NomOutQ = new JLabel();
    tf_NomOutQ = new JTextField();
    rb_ScriptAucun = new JRadioButton();
    rb_ScriptPjl = new JRadioButton();
    rb_ScriptPerso = new JRadioButton();
    l_NbrExemplaire = new JLabel();
    sp_NbrExemplaire = new JSpinner();
    l_Ext = new JLabel();
    cb_Ext = new JComboBox();
    l_Dpi = new JLabel();
    tf_Dpi = new JTextField();
    l_Ddd = new JLabel();
    tf_Ddd = new JTextField();
    buttonBar = new JPanel();
    bt_Valider = new JButton();
    bt_Annuler = new JButton();
    p_ScriptPerso = new JPanel();
    l_Debut = new JLabel();
    scrollPane1 = new JScrollPane();
    tp_DebutScript = new JTextPane();
    l_Fin = new JLabel();
    scrollPane2 = new JScrollPane();
    tp_FinScript = new JTextPane();
    p_ScriptPjl = new JPanel();
    l_TiroirEntree = new JLabel();
    cb_TiroirEntree = new JComboBox();
    chk_Economie = new JCheckBox();
    l_Densite = new JLabel();
    cb_Densite = new JComboBox();
    chk_RectoVerso = new JCheckBox();
    bt_GenererScript = new JButton();
    chk_NoirBlanc = new JCheckBox();
    chk_ShortEdge = new JCheckBox();
    l_TiroirSortie = new JLabel();
    cb_TiroirSortie = new JComboBox();
    
    // ======== this ========
    setTitle("Sortie imprimante");
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setName("dialogPane");
      dialogPane.setLayout(new BorderLayout());
      
      // ======== contentPanel ========
      {
        contentPanel.setBorder(new TitledBorder(""));
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(null);
        
        // ---- l_Description ----
        l_Description.setText("Description");
        l_Description.setName("l_Description");
        contentPanel.add(l_Description);
        l_Description.setBounds(25, 75, 71, 15);
        
        // ---- tf_Description ----
        tf_Description.setName("tf_Description");
        contentPanel.add(tf_Description);
        tf_Description.setBounds(180, 69, 160, 27);
        
        // ---- l_NomSpool ----
        l_NomSpool.setText("Nom du spool");
        l_NomSpool.setName("l_NomSpool");
        contentPanel.add(l_NomSpool);
        l_NomSpool.setBounds(new Rectangle(new Point(25, 102), l_NomSpool.getPreferredSize()));
        
        // ---- tf_NomSpool ----
        tf_NomSpool.setName("tf_NomSpool");
        contentPanel.add(tf_NomSpool);
        tf_NomSpool.setBounds(180, 96, 100, tf_NomSpool.getPreferredSize().height);
        
        // ---- l_NomOutQ ----
        l_NomOutQ.setText("Nom de la Outqueue");
        l_NomOutQ.setName("l_NomOutQ");
        contentPanel.add(l_NomOutQ);
        l_NomOutQ.setBounds(new Rectangle(new Point(25, 130), l_NomOutQ.getPreferredSize()));
        
        // ---- tf_NomOutQ ----
        tf_NomOutQ.setName("tf_NomOutQ");
        contentPanel.add(tf_NomOutQ);
        tf_NomOutQ.setBounds(180, 124, 305, tf_NomOutQ.getPreferredSize().height);
        
        // ---- rb_ScriptAucun ----
        rb_ScriptAucun.setText("Aucun scriptage");
        rb_ScriptAucun.setName("rb_ScriptAucun");
        rb_ScriptAucun.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            rb_ScriptAucunActionPerformed(e);
          }
        });
        contentPanel.add(rb_ScriptAucun);
        rb_ScriptAucun.setBounds(new Rectangle(new Point(25, 185), rb_ScriptAucun.getPreferredSize()));
        
        // ---- rb_ScriptPjl ----
        rb_ScriptPjl.setText("PJL");
        rb_ScriptPjl.setName("rb_ScriptPjl");
        rb_ScriptPjl.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            rb_ScriptPjlActionPerformed(e);
          }
        });
        contentPanel.add(rb_ScriptPjl);
        rb_ScriptPjl.setBounds(new Rectangle(new Point(180, 180), rb_ScriptPjl.getPreferredSize()));
        
        // ---- rb_ScriptPerso ----
        rb_ScriptPerso.setText("Script personnalis\u00e9");
        rb_ScriptPerso.setName("rb_ScriptPerso");
        rb_ScriptPerso.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            rb_ScriptPersoActionPerformed(e);
          }
        });
        contentPanel.add(rb_ScriptPerso);
        rb_ScriptPerso.setBounds(new Rectangle(new Point(265, 185), rb_ScriptPerso.getPreferredSize()));
        
        // ---- l_NbrExemplaire ----
        l_NbrExemplaire.setText("Nombre d'exemplaire");
        l_NbrExemplaire.setName("l_NbrExemplaire");
        contentPanel.add(l_NbrExemplaire);
        l_NbrExemplaire.setBounds(new Rectangle(new Point(25, 158), l_NbrExemplaire.getPreferredSize()));
        
        // ---- sp_NbrExemplaire ----
        sp_NbrExemplaire.setName("sp_NbrExemplaire");
        contentPanel.add(sp_NbrExemplaire);
        sp_NbrExemplaire.setBounds(180, 152, 60, sp_NbrExemplaire.getPreferredSize().height);
        
        // ---- l_Ext ----
        l_Ext.setText("Extension");
        l_Ext.setName("l_Ext");
        contentPanel.add(l_Ext);
        l_Ext.setBounds(new Rectangle(new Point(25, 20), l_Ext.getPreferredSize()));
        
        // ---- cb_Ext ----
        cb_Ext.setModel(new DefaultComboBoxModel(new String[] { "pdf", "ps", "png", "tiff" }));
        cb_Ext.setName("cb_Ext");
        cb_Ext.addItemListener(new ItemListener() {
          public void itemStateChanged(ItemEvent e) {
            cb_ExtItemStateChanged(e);
          }
        });
        contentPanel.add(cb_Ext);
        cb_Ext.setBounds(180, 15, 65, cb_Ext.getPreferredSize().height);
        
        // ---- l_Dpi ----
        l_Dpi.setText("Dpi");
        l_Dpi.setName("l_Dpi");
        contentPanel.add(l_Dpi);
        l_Dpi.setBounds(new Rectangle(new Point(405, 20), l_Dpi.getPreferredSize()));
        
        // ---- tf_Dpi ----
        tf_Dpi.setText("72");
        tf_Dpi.setHorizontalAlignment(SwingConstants.RIGHT);
        tf_Dpi.setName("tf_Dpi");
        contentPanel.add(tf_Dpi);
        tf_Dpi.setBounds(440, 14, 45, tf_Dpi.getPreferredSize().height);
        
        // ---- l_Ddd ----
        l_Ddd.setText("Nom du fichier DDD");
        l_Ddd.setName("l_Ddd");
        contentPanel.add(l_Ddd);
        l_Ddd.setBounds(new Rectangle(new Point(25, 47), l_Ddd.getPreferredSize()));
        
        // ---- tf_Ddd ----
        tf_Ddd.setName("tf_Ddd");
        contentPanel.add(tf_Ddd);
        tf_Ddd.setBounds(180, 41, 160, tf_Ddd.getPreferredSize().height);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < contentPanel.getComponentCount(); i++) {
            Rectangle bounds = contentPanel.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = contentPanel.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          contentPanel.setMinimumSize(preferredSize);
          contentPanel.setPreferredSize(preferredSize);
        }
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      
      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setName("buttonBar");
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 85, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0 };
        
        // ---- bt_Valider ----
        bt_Valider.setText("Valider");
        bt_Valider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Valider.setName("bt_Valider");
        bt_Valider.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_ValiderActionPerformed(e);
          }
        });
        buttonBar.add(bt_Valider, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- bt_Annuler ----
        bt_Annuler.setText("Annuler");
        bt_Annuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Annuler.setName("bt_Annuler");
        bt_Annuler.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_AnnulerActionPerformed(e);
          }
        });
        buttonBar.add(bt_Annuler, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ======== p_ScriptPerso ========
    {
      p_ScriptPerso.setBorder(new TitledBorder("Script personnalis\u00e9"));
      p_ScriptPerso.setName("p_ScriptPerso");
      p_ScriptPerso.setLayout(null);
      
      // ---- l_Debut ----
      l_Debut.setText("D\u00e9but");
      l_Debut.setName("l_Debut");
      p_ScriptPerso.add(l_Debut);
      l_Debut.setBounds(new Rectangle(new Point(25, 35), l_Debut.getPreferredSize()));
      
      // ======== scrollPane1 ========
      {
        scrollPane1.setName("scrollPane1");
        
        // ---- tp_DebutScript ----
        tp_DebutScript.setName("tp_DebutScript");
        scrollPane1.setViewportView(tp_DebutScript);
      }
      p_ScriptPerso.add(scrollPane1);
      scrollPane1.setBounds(75, 35, 360, 120);
      
      // ---- l_Fin ----
      l_Fin.setText("Fin");
      l_Fin.setName("l_Fin");
      p_ScriptPerso.add(l_Fin);
      l_Fin.setBounds(new Rectangle(new Point(30, 160), l_Fin.getPreferredSize()));
      
      // ======== scrollPane2 ========
      {
        scrollPane2.setName("scrollPane2");
        
        // ---- tp_FinScript ----
        tp_FinScript.setName("tp_FinScript");
        scrollPane2.setViewportView(tp_FinScript);
      }
      p_ScriptPerso.add(scrollPane2);
      scrollPane2.setBounds(75, 160, 360, 120);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_ScriptPerso.getComponentCount(); i++) {
          Rectangle bounds = p_ScriptPerso.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_ScriptPerso.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_ScriptPerso.setMinimumSize(preferredSize);
        p_ScriptPerso.setPreferredSize(preferredSize);
      }
    }
    
    // ======== p_ScriptPjl ========
    {
      p_ScriptPjl.setBorder(new TitledBorder("Script PJL"));
      p_ScriptPjl.setName("p_ScriptPjl");
      p_ScriptPjl.setLayout(null);
      
      // ---- l_TiroirEntree ----
      l_TiroirEntree.setText("Tiroir d'entr\u00e9e papier");
      l_TiroirEntree.setName("l_TiroirEntree");
      p_ScriptPjl.add(l_TiroirEntree);
      l_TiroirEntree.setBounds(new Rectangle(new Point(30, 45), l_TiroirEntree.getPreferredSize()));
      
      // ---- cb_TiroirEntree ----
      cb_TiroirEntree.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6" }));
      cb_TiroirEntree.setName("cb_TiroirEntree");
      p_ScriptPjl.add(cb_TiroirEntree);
      cb_TiroirEntree.setBounds(190, 40, 45, cb_TiroirEntree.getPreferredSize().height);
      
      // ---- chk_Economie ----
      chk_Economie.setText("Brouillon (qualit\u00e9 d'impression)");
      chk_Economie.setName("chk_Economie");
      p_ScriptPjl.add(chk_Economie);
      chk_Economie.setBounds(new Rectangle(new Point(30, 118), chk_Economie.getPreferredSize()));
      
      // ---- l_Densite ----
      l_Densite.setText("Densit\u00e9");
      l_Densite.setName("l_Densite");
      p_ScriptPjl.add(l_Densite);
      l_Densite.setBounds(new Rectangle(new Point(30, 160), l_Densite.getPreferredSize()));
      
      // ---- cb_Densite ----
      cb_Densite.setModel(new DefaultComboBoxModel(new String[] { "Lightest", "Lighter", "Normal", "Darker", "Darkest" }));
      cb_Densite.setName("cb_Densite");
      p_ScriptPjl.add(cb_Densite);
      cb_Densite.setBounds(190, 155, 95, cb_Densite.getPreferredSize().height);
      
      // ---- chk_RectoVerso ----
      chk_RectoVerso.setText("Recto-Verso");
      chk_RectoVerso.setName("chk_RectoVerso");
      chk_RectoVerso.addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent e) {
          chk_RectoVersoStateChanged(e);
        }
      });
      p_ScriptPjl.add(chk_RectoVerso);
      chk_RectoVerso.setBounds(new Rectangle(new Point(30, 80), chk_RectoVerso.getPreferredSize()));
      
      // ---- bt_GenererScript ----
      bt_GenererScript.setText("Envoyer vers script personnalis\u00e9");
      bt_GenererScript.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_GenererScript.setName("bt_GenererScript");
      bt_GenererScript.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          bt_GenererScriptActionPerformed(e);
        }
      });
      p_ScriptPjl.add(bt_GenererScript);
      bt_GenererScript.setBounds(new Rectangle(new Point(220, 255), bt_GenererScript.getPreferredSize()));
      
      // ---- chk_NoirBlanc ----
      chk_NoirBlanc.setText("Noir et blanc");
      chk_NoirBlanc.setName("chk_NoirBlanc");
      p_ScriptPjl.add(chk_NoirBlanc);
      chk_NoirBlanc.setBounds(new Rectangle(new Point(30, 200), chk_NoirBlanc.getPreferredSize()));
      
      // ---- chk_ShortEdge ----
      chk_ShortEdge.setText("ShortEdge");
      chk_ShortEdge.setName("chk_ShortEdge");
      p_ScriptPjl.add(chk_ShortEdge);
      chk_ShortEdge.setBounds(190, 80, 125, chk_ShortEdge.getPreferredSize().height);
      
      // ---- l_TiroirSortie ----
      l_TiroirSortie.setText("Tiroir de sortie");
      l_TiroirSortie.setName("l_TiroirSortie");
      p_ScriptPjl.add(l_TiroirSortie);
      l_TiroirSortie.setBounds(280, 45, 95, l_TiroirSortie.getPreferredSize().height);
      
      // ---- cb_TiroirSortie ----
      cb_TiroirSortie.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6" }));
      cb_TiroirSortie.setName("cb_TiroirSortie");
      p_ScriptPjl.add(cb_TiroirSortie);
      cb_TiroirSortie.setBounds(381, 40, 45, 26);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_ScriptPjl.getComponentCount(); i++) {
          Rectangle bounds = p_ScriptPjl.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_ScriptPjl.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_ScriptPjl.setMinimumSize(preferredSize);
        p_ScriptPjl.setPreferredSize(preferredSize);
      }
    }
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rb_ScriptAucun);
    buttonGroup1.add(rb_ScriptPjl);
    buttonGroup1.add(rb_ScriptPerso);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JLabel l_Description;
  private JTextField tf_Description;
  private JLabel l_NomSpool;
  private JTextField tf_NomSpool;
  private JLabel l_NomOutQ;
  private JTextField tf_NomOutQ;
  private JRadioButton rb_ScriptAucun;
  private JRadioButton rb_ScriptPjl;
  private JRadioButton rb_ScriptPerso;
  private JLabel l_NbrExemplaire;
  private JSpinner sp_NbrExemplaire;
  private JLabel l_Ext;
  private JComboBox cb_Ext;
  private JLabel l_Dpi;
  private JTextField tf_Dpi;
  private JLabel l_Ddd;
  private JTextField tf_Ddd;
  private JPanel buttonBar;
  private JButton bt_Valider;
  private JButton bt_Annuler;
  private JPanel p_ScriptPerso;
  private JLabel l_Debut;
  private JScrollPane scrollPane1;
  private JTextPane tp_DebutScript;
  private JLabel l_Fin;
  private JScrollPane scrollPane2;
  private JTextPane tp_FinScript;
  private JPanel p_ScriptPjl;
  private JLabel l_TiroirEntree;
  private JComboBox cb_TiroirEntree;
  private JCheckBox chk_Economie;
  private JLabel l_Densite;
  private JComboBox cb_Densite;
  private JCheckBox chk_RectoVerso;
  private JButton bt_GenererScript;
  private JCheckBox chk_NoirBlanc;
  private JCheckBox chk_ShortEdge;
  private JLabel l_TiroirSortie;
  private JComboBox cb_TiroirSortie;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
