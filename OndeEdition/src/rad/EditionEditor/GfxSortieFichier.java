/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.EditionEditor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import description.DescriptionDocument;
import description.DescriptionPage;
import description.DescriptionVariable;
import description.sortie.Sortie;
import description.sortie.SortieFichier;
import ri.seriem.libcommun.outils.DateHeure;
import ri.seriem.libcommun.outils.XMLTools;

//=================================================================================================
//==> Description dde la sortie fichier
//=================================================================================================
public class GfxSortieFichier extends JDialog {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private GfxEditionEditor editionEditor = null;
  private SortieFichier sortieFichier = null;
  private boolean creation = true;
  // private InterpreterData iData=new InterpreterData();
  private DescriptionDocument document = null;
  
  /**
   * Constructeur
   * @param owner
   */
  public GfxSortieFichier(Frame owner, SortieFichier sortiefichier) // , String fichierDDD)
  {
    super(owner);
    initComponents();
    setVisible(true);
    editionEditor = (GfxEditionEditor) owner;
    sortieFichier = sortiefichier;
    initData();
    majDpi();
    tf_Description.setEditable(creation);
    // setDocument(fichierDDD);
  }
  
  /**
   * Initialise la description de document
   * @param doc
   */
  public void setDocument(String ficdoc) {
    // System.out.println("--> " + ficdoc);
    if ((ficdoc == null) || (ficdoc.trim().equals("")))
      return;
    
    try {
      document = (DescriptionDocument) XMLTools.decodeFromFile(ficdoc);
    }
    catch (Exception e) {
    }
    if (document != null)
      initMenuContextuel();
  }
  
  /**
   * Initialise le menu contextuel
   */
  private void initMenuContextuel() {
    if (document.getPremiereDeCouverture() != null)
      addMenuItem(document.getPremiereDeCouverture().getNomFichierDDP(),
          document.getPremiereDeCouverture().getListeDescriptionVariable());
    if (document.getDeuxiemeDeCouverture() != null)
      addMenuItem(document.getDeuxiemeDeCouverture().getNomFichierDDP(),
          document.getDeuxiemeDeCouverture().getListeDescriptionVariable());
    if (document.getSequence() != null)
      for (int i = 0; i < document.getSequence().size(); i++)
        addMenuItem(((DescriptionPage) document.getSequence().get(i)).getNomFichierDDP(),
            ((DescriptionPage) document.getSequence().get(i)).getListeDescriptionVariable());
    if (document.getTroisiemeDeCouverture() != null)
      addMenuItem(document.getTroisiemeDeCouverture().getNomFichierDDP(),
          document.getTroisiemeDeCouverture().getListeDescriptionVariable());
    if (document.getQuatriemeDeCouverture() != null)
      addMenuItem(document.getQuatriemeDeCouverture().getNomFichierDDP(),
          document.getQuatriemeDeCouverture().getListeDescriptionVariable());
  }
  
  /**
   * Ajoute les variables dans le menu contextuel
   * @param listeVariable
   */
  private void addMenuItem(String nomddp, final ArrayList<DescriptionVariable> listeVariable) {
    if (listeVariable == null)
      return;
    
    for (int i = listeVariable.size(); --i >= 0;)
      createMenuItem(nomddp, listeVariable.get(i));
  }
  
  /**
   * Cr�� et ajoute un menuitem
   * @param nomddp
   * @param variable
   */
  private void createMenuItem(String nomddp, final DescriptionVariable variable) {
    JMenuItem mi = new JMenuItem(variable.getNom());
    mi.setToolTipText(nomddp);
    mi.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        tf_Fichier.setText(tf_Fichier.getText() + variable.getNom());
      }
    });
    pop_btd.add(mi);
  }
  
  /**
   * Initialisation des donn�es de la fen�tre
   */
  private void initData() {
    if (sortieFichier == null)
      return;
    
    if (sortieFichier.getTypeDoc() != null) {
      cb_Ext.setSelectedItem(sortieFichier.getTypeDoc());
      tf_Dpi.setText("" + sortieFichier.getDpiSortie());
    }
    if (sortieFichier.getFichierDescriptionDocument() != null)
      tf_Ddd.setText(sortieFichier.getFichierDescriptionDocument());
    
    if (sortieFichier.getDescription() != null) {
      tf_Description.setText(sortieFichier.getDescription());
      creation = false;
    }
    if (sortieFichier.getNomDossier() != null)
      tf_Dossier.setText(sortieFichier.getNomDossier());
    if (sortieFichier.getNomFichier() != null)
      tf_Fichier.setText(sortieFichier.getNomFichier());
    
    // l_VisuNomcomplet.setText(iData.analyseExpressionTrimR(sortieFichier.getNomComplet()));
    majVisuNomcomplet();
  }
  
  /**
   * Met � jour le nom complet
   */
  private void majVisuNomcomplet() {
    l_VisuNomcomplet.setText(tf_Dossier.getText().trim() + '/' + tf_Fichier.getText() + '.' + sortieFichier.getExtension());
  }
  
  /**
   * R�cup�re les donn�es
   */
  private boolean validData() {
    if (editionEditor == null)
      return false;
    if (sortieFichier == null)
      return false;
    if (tf_Description.getText().trim().equals(""))
      return false;
    
    sortieFichier.setFichierDescriptionDocument(tf_Ddd.getText().trim());
    
    sortieFichier.setTypeDoc((String) cb_Ext.getSelectedItem());
    if (!tf_Dpi.getText().trim().equals(""))
      sortieFichier.setDpiSortie(Integer.parseInt(tf_Dpi.getText().trim()));
    sortieFichier.setDescription(tf_Description.getText().trim());
    sortieFichier.setNomDossier(tf_Dossier.getText());
    sortieFichier.setNomFichier(tf_Fichier.getText());
    
    if (creation)
      editionEditor.getListeSortie().addObject(sortieFichier.getDescription(), sortieFichier);
    
    return true;
  }
  
  /**
   * Mise � jour de l'�tat des zones concerant le dpi
   */
  private void majDpi() {
    boolean result = !(cb_Ext.getSelectedItem().equals(Sortie.PDF) || cb_Ext.getSelectedItem().equals(Sortie.PS));
    l_Dpi.setEnabled(result);
    tf_Dpi.setEnabled(result);
  }
  
  /**
   * Retourne une variable systeme format�e (Date ou Heure)
   * @param choix
   * @return
   */
  private void getVariableSysteme(int choix) {
    tf_Fichier.setText(tf_Fichier.getText() + DateHeure.getNomVariable(choix));
  }
  
  private void bt_AnnulerActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void bt_ValiderActionPerformed(ActionEvent e) {
    if (validData())
      dispose();
    else
      JOptionPane.showMessageDialog(this, "Saisissez une description", "Erreur", JOptionPane.ERROR_MESSAGE);
  }
  
  private void tf_DossierCaretUpdate(CaretEvent e) {
    majVisuNomcomplet();
  }
  
  private void tf_FichierCaretUpdate(CaretEvent e) {
    majVisuNomcomplet();
  }
  
  private void m_Date_aaaammjjActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.AAAAMMJJ);
  }
  
  private void m_Date_aammjjActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.AAMMJJ);
  }
  
  private void m_Date_jjmmaaaaActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.JJMMAAAA);
  }
  
  private void m_Date_jjmmaaActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.JJMMAA);
  }
  
  private void m_Heure_hhmmssActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.HHMMSS);
  }
  
  private void m_Heure_hhmmss2ActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.HH_MM_SS);
  }
  
  private void m_Heure_hhmmssmsActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.HHMMSSMS);
  }
  
  private void m_Heure_hhmmssms2ActionPerformed(ActionEvent e) {
    getVariableSysteme(DateHeure.HH_MM_SS_MS);
  }
  
  private void cb_ExtItemStateChanged(ItemEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    l_Description = new JLabel();
    tf_Description = new JTextField();
    l_Dossier = new JLabel();
    tf_Dossier = new JTextField();
    l_Fichier = new JLabel();
    tf_Fichier = new JTextField();
    l_Com = new JLabel();
    l_NomComplet = new JLabel();
    l_VisuNomcomplet = new JLabel();
    l_Ext = new JLabel();
    cb_Ext = new JComboBox();
    l_Dpi = new JLabel();
    tf_Dpi = new JTextField();
    tf_Ddd = new JTextField();
    l_Ddd = new JLabel();
    buttonBar = new JPanel();
    bt_Valider = new JButton();
    bt_Annuler = new JButton();
    pop_btd = new JPopupMenu();
    mi_VariablesSystemes = new JMenuItem();
    m_Date = new JMenu();
    m_Date_aaaammjj = new JMenuItem();
    m_Date_aammjj = new JMenuItem();
    m_Date_jjmmaaaa = new JMenuItem();
    m_Date_jjmmaa = new JMenuItem();
    m_Heure = new JMenu();
    m_Heure_hhmmss = new JMenuItem();
    m_Heure_hhmmssms = new JMenuItem();
    m_Heure_hhmmss2 = new JMenuItem();
    m_Heure_hhmmssms2 = new JMenuItem();
    mi_VariablesSpool = new JMenuItem();
    
    // ======== this ========
    setTitle("Sortie fichier");
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setName("dialogPane");
      dialogPane.setLayout(new BorderLayout());
      
      // ======== contentPanel ========
      {
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(new BorderLayout());
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- l_Description ----
          l_Description.setText("Description");
          l_Description.setName("l_Description");
          panel1.add(l_Description);
          l_Description.setBounds(new Rectangle(new Point(25, 80), l_Description.getPreferredSize()));
          
          // ---- tf_Description ----
          tf_Description.setName("tf_Description");
          panel1.add(tf_Description);
          tf_Description.setBounds(145, 74, 160, tf_Description.getPreferredSize().height);
          
          // ---- l_Dossier ----
          l_Dossier.setText("Dossier");
          l_Dossier.setName("l_Dossier");
          panel1.add(l_Dossier);
          l_Dossier.setBounds(new Rectangle(new Point(25, 113), l_Dossier.getPreferredSize()));
          
          // ---- tf_Dossier ----
          tf_Dossier.setName("tf_Dossier");
          tf_Dossier.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent e) {
              tf_DossierCaretUpdate(e);
            }
          });
          panel1.add(tf_Dossier);
          tf_Dossier.setBounds(145, 107, 260, tf_Dossier.getPreferredSize().height);
          
          // ---- l_Fichier ----
          l_Fichier.setText("Nom du fichier");
          l_Fichier.setName("l_Fichier");
          panel1.add(l_Fichier);
          l_Fichier.setBounds(new Rectangle(new Point(25, 140), l_Fichier.getPreferredSize()));
          
          // ---- tf_Fichier ----
          tf_Fichier.setComponentPopupMenu(pop_btd);
          tf_Fichier.setName("tf_Fichier");
          tf_Fichier.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent e) {
              tf_FichierCaretUpdate(e);
            }
          });
          panel1.add(tf_Fichier);
          tf_Fichier.setBounds(145, 140, 260, tf_Fichier.getPreferredSize().height);
          
          // ---- l_Com ----
          l_Com.setText(" (sans l'extension)");
          l_Com.setName("l_Com");
          panel1.add(l_Com);
          l_Com.setBounds(new Rectangle(new Point(25, 150), l_Com.getPreferredSize()));
          
          // ---- l_NomComplet ----
          l_NomComplet.setText("Nom complet");
          l_NomComplet.setName("l_NomComplet");
          panel1.add(l_NomComplet);
          l_NomComplet.setBounds(new Rectangle(new Point(25, 175), l_NomComplet.getPreferredSize()));
          
          // ---- l_VisuNomcomplet ----
          l_VisuNomcomplet.setText("text");
          l_VisuNomcomplet.setName("l_VisuNomcomplet");
          panel1.add(l_VisuNomcomplet);
          l_VisuNomcomplet.setBounds(145, 173, 295, l_VisuNomcomplet.getPreferredSize().height);
          
          // ---- l_Ext ----
          l_Ext.setText("Extension");
          l_Ext.setName("l_Ext");
          panel1.add(l_Ext);
          l_Ext.setBounds(new Rectangle(new Point(25, 15), l_Ext.getPreferredSize()));
          
          // ---- cb_Ext ----
          cb_Ext.setModel(new DefaultComboBoxModel(new String[] { "pdf", "ps", "png", "tiff" }));
          cb_Ext.setName("cb_Ext");
          cb_Ext.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
              cb_ExtItemStateChanged(e);
            }
          });
          panel1.add(cb_Ext);
          cb_Ext.setBounds(145, 10, 65, cb_Ext.getPreferredSize().height);
          
          // ---- l_Dpi ----
          l_Dpi.setText("Dpi");
          l_Dpi.setName("l_Dpi");
          panel1.add(l_Dpi);
          l_Dpi.setBounds(new Rectangle(new Point(325, 15), l_Dpi.getPreferredSize()));
          
          // ---- tf_Dpi ----
          tf_Dpi.setText("72");
          tf_Dpi.setHorizontalAlignment(SwingConstants.RIGHT);
          tf_Dpi.setName("tf_Dpi");
          panel1.add(tf_Dpi);
          tf_Dpi.setBounds(360, 10, 45, tf_Dpi.getPreferredSize().height);
          
          // ---- tf_Ddd ----
          tf_Ddd.setName("tf_Ddd");
          panel1.add(tf_Ddd);
          tf_Ddd.setBounds(145, 41, 160, tf_Ddd.getPreferredSize().height);
          
          // ---- l_Ddd ----
          l_Ddd.setText("Nom du fichier DDD");
          l_Ddd.setName("l_Ddd");
          panel1.add(l_Ddd);
          l_Ddd.setBounds(new Rectangle(new Point(25, 47), l_Ddd.getPreferredSize()));
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        contentPanel.add(panel1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      
      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setName("buttonBar");
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 85, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0 };
        
        // ---- bt_Valider ----
        bt_Valider.setText("Valider");
        bt_Valider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Valider.setName("bt_Valider");
        bt_Valider.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_ValiderActionPerformed(e);
          }
        });
        buttonBar.add(bt_Valider, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- bt_Annuler ----
        bt_Annuler.setText("Annuler");
        bt_Annuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Annuler.setName("bt_Annuler");
        bt_Annuler.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_AnnulerActionPerformed(e);
          }
        });
        buttonBar.add(bt_Annuler, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ======== pop_btd ========
    {
      pop_btd.setName("pop_btd");
      
      // ---- mi_VariablesSystemes ----
      mi_VariablesSystemes.setText("Ajout variables syst\u00e8mes");
      mi_VariablesSystemes.setFont(mi_VariablesSystemes.getFont().deriveFont(mi_VariablesSystemes.getFont().getStyle() | Font.BOLD));
      mi_VariablesSystemes.setName("mi_VariablesSystemes");
      pop_btd.add(mi_VariablesSystemes);
      
      // ======== m_Date ========
      {
        m_Date.setText("Date");
        m_Date.setName("m_Date");
        
        // ---- m_Date_aaaammjj ----
        m_Date_aaaammjj.setText("aaaammjj");
        m_Date_aaaammjj.setName("m_Date_aaaammjj");
        m_Date_aaaammjj.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Date_aaaammjjActionPerformed(e);
          }
        });
        m_Date.add(m_Date_aaaammjj);
        
        // ---- m_Date_aammjj ----
        m_Date_aammjj.setText("aammjj");
        m_Date_aammjj.setName("m_Date_aammjj");
        m_Date_aammjj.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Date_aammjjActionPerformed(e);
          }
        });
        m_Date.add(m_Date_aammjj);
        
        // ---- m_Date_jjmmaaaa ----
        m_Date_jjmmaaaa.setText("jjmmaaaa");
        m_Date_jjmmaaaa.setName("m_Date_jjmmaaaa");
        m_Date_jjmmaaaa.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Date_jjmmaaaaActionPerformed(e);
          }
        });
        m_Date.add(m_Date_jjmmaaaa);
        
        // ---- m_Date_jjmmaa ----
        m_Date_jjmmaa.setText("jjmmaa");
        m_Date_jjmmaa.setName("m_Date_jjmmaa");
        m_Date_jjmmaa.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Date_jjmmaaActionPerformed(e);
          }
        });
        m_Date.add(m_Date_jjmmaa);
      }
      pop_btd.add(m_Date);
      
      // ======== m_Heure ========
      {
        m_Heure.setText("Heure");
        m_Heure.setName("m_Heure");
        
        // ---- m_Heure_hhmmss ----
        m_Heure_hhmmss.setText("hhmmss");
        m_Heure_hhmmss.setName("m_Heure_hhmmss");
        m_Heure_hhmmss.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Heure_hhmmssActionPerformed(e);
          }
        });
        m_Heure.add(m_Heure_hhmmss);
        
        // ---- m_Heure_hhmmssms ----
        m_Heure_hhmmssms.setText("hhmmssms");
        m_Heure_hhmmssms.setName("m_Heure_hhmmssms");
        m_Heure_hhmmssms.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Heure_hhmmssmsActionPerformed(e);
          }
        });
        m_Heure.add(m_Heure_hhmmssms);
        
        // ---- m_Heure_hhmmss2 ----
        m_Heure_hhmmss2.setText("hh.mm.ss");
        m_Heure_hhmmss2.setName("m_Heure_hhmmss2");
        m_Heure_hhmmss2.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Heure_hhmmss2ActionPerformed(e);
          }
        });
        m_Heure.add(m_Heure_hhmmss2);
        
        // ---- m_Heure_hhmmssms2 ----
        m_Heure_hhmmssms2.setText("hh.mm.ss.ms");
        m_Heure_hhmmssms2.setName("m_Heure_hhmmssms2");
        m_Heure_hhmmssms2.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            m_Heure_hhmmssms2ActionPerformed(e);
          }
        });
        m_Heure.add(m_Heure_hhmmssms2);
      }
      pop_btd.add(m_Heure);
      pop_btd.addSeparator();
      
      // ---- mi_VariablesSpool ----
      mi_VariablesSpool.setText("Ajout variables du spool");
      mi_VariablesSpool.setFont(mi_VariablesSpool.getFont().deriveFont(mi_VariablesSpool.getFont().getStyle() | Font.BOLD));
      mi_VariablesSpool.setName("mi_VariablesSpool");
      pop_btd.add(mi_VariablesSpool);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel l_Description;
  private JTextField tf_Description;
  private JLabel l_Dossier;
  private JTextField tf_Dossier;
  private JLabel l_Fichier;
  private JTextField tf_Fichier;
  private JLabel l_Com;
  private JLabel l_NomComplet;
  private JLabel l_VisuNomcomplet;
  private JLabel l_Ext;
  private JComboBox cb_Ext;
  private JLabel l_Dpi;
  private JTextField tf_Dpi;
  private JTextField tf_Ddd;
  private JLabel l_Ddd;
  private JPanel buttonBar;
  private JButton bt_Valider;
  private JButton bt_Annuler;
  private JPopupMenu pop_btd;
  private JMenuItem mi_VariablesSystemes;
  private JMenu m_Date;
  private JMenuItem m_Date_aaaammjj;
  private JMenuItem m_Date_aammjj;
  private JMenuItem m_Date_jjmmaaaa;
  private JMenuItem m_Date_jjmmaa;
  private JMenu m_Heure;
  private JMenuItem m_Heure_hhmmss;
  private JMenuItem m_Heure_hhmmssms;
  private JMenuItem m_Heure_hhmmss2;
  private JMenuItem m_Heure_hhmmssms2;
  private JMenuItem mi_VariablesSpool;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
