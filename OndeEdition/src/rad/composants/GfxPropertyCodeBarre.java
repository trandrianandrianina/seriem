/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.krysalis.barcode4j.BarcodeClassResolver;
import org.krysalis.barcode4j.DefaultBarcodeClassResolver;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;

import description.DescriptionCodeBarre;
import rad.PageEditor.GfxPageEditor;
import rad.outils.CalculDPI;

//=================================================================================================
//==> Panel permettant de changer les propri�t�s d'un code barre
//=================================================================================================
public class GfxPropertyCodeBarre extends JPanel // implements iGfxPropertyObject
{
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private GfxObject object = null;
  private GfxPageEditor pageEditor = null;
  private int nbr2condition = 0;
  private boolean modeMultiSelection = false;
  
  /**
   * Constructeur
   */
  public GfxPropertyCodeBarre(final GfxPageEditor pageEditor) {
    initComponents();
    setPageEditor(pageEditor);
    // initGestionLostFocus(sp_Largeur);
    // initGestionLostFocus(sp_Hauteur);
    // initGestionLostFocus(sp_X);
    // initGestionLostFocus(sp_Y);
    chargeListeCodeBarre();
    chargePositionTexte();
    chargeRotation();
  }
  
  /**
   * Initialise l'�diteur de page
   * @param pageEditor
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label
   * @param label
   */
  public void setObject2PanelAttributs(GfxObject codebarre) {
    object = codebarre;
    int dpi_aff = codebarre.getDpiAff();
    int dpi_img = codebarre.getDpiImg();
    
    modeMultiSelection = false;
    
    cb_Type.setSelectedItem(((GfxCodeBarre) codebarre).getTypeCodeBarre());
    tf_Message.setText(((GfxCodeBarre) codebarre).getTexte());
    tf_LargeurModule.setText("" + ((GfxCodeBarre) codebarre).getLargeurModule());
    // tf_Rotation.setText("" + ((GfxCodeBarre)codebarre).getRotation());
    cb_Rotation.setSelectedItem(((GfxCodeBarre) codebarre).getRotation());
    cb_PosTexte.setSelectedItem(((GfxCodeBarre) codebarre).getPositionMessage());
    
    sp_Largeur.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, codebarre.getWidth()));
    l_LargeurPx.setText(codebarre.getWidth() + " px");
    sp_Hauteur.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, codebarre.getHeight()));
    l_HauteurPx.setText(codebarre.getHeight() + " px");
    sp_X.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, codebarre.getLocation().x));
    l_XPx.setText(codebarre.getLocation().x + " px");
    sp_Y.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, codebarre.getLocation().y));
    l_YPx.setText(codebarre.getLocation().y + " px");
    
    // Les conditions
    chargeComboCondition(object.getCondition());
    
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * @param label
   */
  public void addObject2PanelAttributs(GfxObject obj) {
    object = (GfxCodeBarre) obj;
    int dpi_aff = obj.getDpiAff();
    int dpi_img = obj.getDpiImg();
    
    modeMultiSelection = true;
    addObject2PanelAttributs_coordonnees(obj, dpi_aff, dpi_img);
    // Les conditions
    chargeComboCondition(object.getCondition());
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * Pour les coordonn�es / dimensions
   * @param label
   * @param dpi_aff
   * @param dpi_img
   */
  private void addObject2PanelAttributs_coordonnees(GfxObject codebarre, int dpi_aff, int dpi_img) {
    // Les coordonn�es, dimension
    if (((Float) sp_Largeur.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, dpi_img, object.getWidth()))
      sp_Largeur.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_Hauteur.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getHeight()))
      sp_Hauteur.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_X.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().x))
      sp_X.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_Y.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().y))
      sp_Y.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
  }
  
  /**
   * Met � jour le label � partir des attributs saisis dans le panel
   */
  private void setPanel2LabelAttributs() {
    int dpi_aff = object.getDpiAff();
    int dpi_img = object.getDpiImg();
    
    // float largeur = ((Float)sp_Largeur.getValue()).floatValue();
    // float hauteur = ((Float)sp_Hauteur.getValue()).floatValue();
    
    // On met � jour
    ((GfxCodeBarre) object).setTypeCodeBarre((String) cb_Type.getSelectedItem());
    ((GfxCodeBarre) object).setTexte(tf_Message.getText().trim());
    ((GfxCodeBarre) object).setLargeurModule(Float.valueOf(tf_LargeurModule.getText().trim()));
    ((GfxCodeBarre) object).setRotation((Integer) cb_Rotation.getSelectedItem());
    ((GfxCodeBarre) object).setPositionMessage((String) cb_PosTexte.getSelectedItem());
    
    object.setLocation(CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_X.getValue()).floatValue()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Y.getValue()).floatValue()));
    object.setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Largeur.getValue()).floatValue()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Hauteur.getValue()).floatValue()));
    l_XPx.setText(object.getLocation().x + " px");
    l_YPx.setText(object.getLocation().y + " px");
    l_LargeurPx.setText(object.getWidth() + " px");
    l_HauteurPx.setText(object.getHeight() + " px");
    if (!cb_Condition.getSelectedItem().equals(GfxObject.PAS_DE_COND))
      object.setCondition((String) cb_Condition.getSelectedItem());
    else
      object.setCondition(null);
    
  }
  
  /**
   * Met � jour tous les labels � partir des attributs saisis dans les attributs
   */
  private void setPanel2AllLabelAttributs() {
    int dpi_aff = object.getDpiAff();
    int dpi_img = object.getDpiImg();
    int a = 0, b = 0;
    
    GfxEtiquette parent = (GfxEtiquette) object.getParent();
    // System.out.println("-setPanel2AllLabelAttributs-> " + modeMultiSelection);
    // System.out.println("-setPanel2AllLabelAttributs-> " + parent.isKey_ctrl());
    // Tant que la touche control est enfonc�e on ne fait aucune modif
    if (parent.isKey_ctrl())
      return;
    
    ArrayList<GfxObject> listeSelectionObject = parent.getSelectionGfxObjet();
    
    // On met � jour
    for (int i = listeSelectionObject.size(); --i >= 0;) {
      // Position
      if (((Float) sp_X.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        a = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_X.getValue()).floatValue());
      else
        a = listeSelectionObject.get(i).getLocation().x;
      if (((Float) sp_Y.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        b = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Y.getValue()).floatValue());
      else
        b = listeSelectionObject.get(i).getLocation().y;
      listeSelectionObject.get(i).setLocation(a, b);
      // Taille
      if (((Float) sp_Largeur.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        a = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Largeur.getValue()).floatValue());
      else
        a = listeSelectionObject.get(i).getWidth();
      if (((Float) sp_Hauteur.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        b = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Hauteur.getValue()).floatValue());
      else
        b = listeSelectionObject.get(i).getHeight();
      listeSelectionObject.get(i).setSize(a, b);
      
      // if (!cb_Condition.getSelectedItem().equals(GfxObject.PAS_DE_COND))
      // listeSelectionObject.get(i).setCondition((String)cb_Condition.getSelectedItem());
    }
  }
  
  /**
   * Gestion de la perte du focus sur un JSpinner
   * @param spinner
   *
   *          private void initGestionLostFocus(final JSpinner spinner)
   *          {
   *          ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().addFocusListener(new FocusAdapter()
   *          {
   *          public void focusLost(FocusEvent evt)
   *          {
   *          try
   *          {
   *          spinner.commitEdit();
   *          }
   *          catch (ParseException e) {}
   *          setPanel2LabelAttributs();
   *          }
   *          });
   * 
   *          }
   */
  
  /**
   * Charge la combo des conditions
   */
  private void chargeComboCondition(String cond) {
    nbr2condition = pageEditor.getListeCondition().size();
    String[] nomCondition = new String[pageEditor.getListeCondition().size() + 1];
    nomCondition[0] = GfxObject.PAS_DE_COND;
    for (int i = 0; i < nbr2condition; i++)
      nomCondition[i + 1] = pageEditor.getListeCondition().get(i).getNom();
    cb_Condition.setModel(new DefaultComboBoxModel(nomCondition));
    if (cond != null)
      cb_Condition.setSelectedItem(cond);
    
  }
  
  /**
   * Charge la combo avec le liste des types de code barres possibles
   */
  private void chargeListeCodeBarre() {
    BarcodeClassResolver classResolver = new DefaultBarcodeClassResolver();
    cb_Type.setModel(new DefaultComboBoxModel(classResolver.getBarcodeNames().toArray()));
  }
  
  /**
   * Charge la combo avec le liste des positions du texte
   */
  private void chargePositionTexte() {
    String[] postexte = { DescriptionCodeBarre.BAS, DescriptionCodeBarre.HAUT };
    cb_PosTexte.setModel(new DefaultComboBoxModel(postexte));
  }
  
  /**
   * Charge la combo avec le liste des rotations
   */
  private void chargeRotation() {
    Integer[] choixRotation = { 0, 90, 180, 270, -90, -180, -270 };
    cb_Rotation.setModel(new DefaultComboBoxModel(choixRotation));
  }
  
  // Gestion des �v�nements -----------------------------------------------------------------------------------------------------
  
  private void sp_LargeurStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setSize(CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Largeur.getValue()).floatValue()),
            object.getHeight());
        l_LargeurPx.setText(object.getWidth() + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_HauteurStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setSize(object.getWidth(),
            CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Hauteur.getValue()).floatValue()));
        l_HauteurPx.setText(object.getHeight() + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_XStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setLocation(CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_X.getValue()).floatValue()),
            object.getLocation().y);
        l_XPx.setText(object.getLocation().x + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_YStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setLocation(object.getLocation().x,
            CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Y.getValue()).floatValue()));
        l_YPx.setText(object.getLocation().y + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void cb_ConditionFocusGained(FocusEvent e) {
    if (!modeMultiSelection)
      if (nbr2condition != pageEditor.getListeCondition().size())
        chargeComboCondition(null);
  }
  
  private void cb_ConditionFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void tf_LargeurModuleFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void cb_TypeFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void cb_PosTexteFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void tf_MessageFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void cb_RotationFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  // -----------------------------------------------------------------------------------------------------------------------------
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_Type = new JLabel();
    cb_Type = new JComboBox();
    l_Message = new JLabel();
    tf_Message = new JTextField();
    label2 = new JLabel();
    sp_Largeur = new JSpinner();
    l_LargeurPx = new JLabel();
    label3 = new JLabel();
    sp_Hauteur = new JSpinner();
    l_HauteurPx = new JLabel();
    label4 = new JLabel();
    sp_X = new JSpinner();
    l_XPx = new JLabel();
    label5 = new JLabel();
    sp_Y = new JSpinner();
    l_YPx = new JLabel();
    l_Rotation = new JLabel();
    cb_Rotation = new JComboBox();
    label7 = new JLabel();
    l_PosTexte = new JLabel();
    cb_PosTexte = new JComboBox();
    label1 = new JLabel();
    tf_LargeurModule = new JTextField();
    label6 = new JLabel();
    l_Condition = new JLabel();
    cb_Condition = new JComboBox();
    CellConstraints cc = new CellConstraints();
    
    // ======== this ========
    setBorder(new TitledBorder("Propri\u00e9t\u00e9s du Code Barre"));
    setMinimumSize(new Dimension(250, 450));
    setPreferredSize(new Dimension(250, 450));
    setName("this");
    setLayout(new FormLayout(
        new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
            new ColumnSpec(Sizes.dluX(49)), new ColumnSpec(Sizes.dluX(26)) },
        new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC }));
    
    // ---- l_Type ----
    l_Type.setText("Type");
    l_Type.setName("l_Type");
    add(l_Type, cc.xy(2, 2));
    
    // ---- cb_Type ----
    cb_Type.setName("cb_Type");
    cb_Type.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        cb_TypeFocusLost(e);
      }
    });
    add(cb_Type, cc.xywh(4, 2, 2, 1));
    
    // ---- l_Message ----
    l_Message.setText("Message");
    l_Message.setName("l_Message");
    add(l_Message, cc.xy(2, 4));
    
    // ---- tf_Message ----
    tf_Message.setName("tf_Message");
    tf_Message.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tf_MessageFocusLost(e);
      }
    });
    add(tf_Message, cc.xywh(4, 4, 2, 1));
    
    // ---- label2 ----
    label2.setText("Largeur");
    label2.setName("label2");
    add(label2, cc.xy(2, 6));
    
    // ---- sp_Largeur ----
    sp_Largeur.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Largeur.setName("sp_Largeur");
    sp_Largeur.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_LargeurStateChanged(e);
      }
    });
    add(sp_Largeur, cc.xy(4, 6));
    
    // ---- l_LargeurPx ----
    l_LargeurPx.setText(" ? px");
    l_LargeurPx.setName("l_LargeurPx");
    add(l_LargeurPx, cc.xy(5, 6));
    
    // ---- label3 ----
    label3.setText("Hauteur");
    label3.setName("label3");
    add(label3, cc.xy(2, 8));
    
    // ---- sp_Hauteur ----
    sp_Hauteur.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Hauteur.setName("sp_Hauteur");
    sp_Hauteur.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_HauteurStateChanged(e);
      }
    });
    add(sp_Hauteur, cc.xy(4, 8));
    
    // ---- l_HauteurPx ----
    l_HauteurPx.setText("? px");
    l_HauteurPx.setName("l_HauteurPx");
    add(l_HauteurPx, cc.xy(5, 8));
    
    // ---- label4 ----
    label4.setText("X");
    label4.setName("label4");
    add(label4, cc.xy(2, 10));
    
    // ---- sp_X ----
    sp_X.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_X.setName("sp_X");
    sp_X.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_XStateChanged(e);
      }
    });
    add(sp_X, cc.xy(4, 10));
    
    // ---- l_XPx ----
    l_XPx.setText("? px");
    l_XPx.setName("l_XPx");
    add(l_XPx, cc.xy(5, 10));
    
    // ---- label5 ----
    label5.setText("Y");
    label5.setName("label5");
    add(label5, cc.xy(2, 12));
    
    // ---- sp_Y ----
    sp_Y.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Y.setName("sp_Y");
    sp_Y.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_YStateChanged(e);
      }
    });
    add(sp_Y, cc.xy(4, 12));
    
    // ---- l_YPx ----
    l_YPx.setText("? px");
    l_YPx.setName("l_YPx");
    add(l_YPx, cc.xy(5, 12));
    
    // ---- l_Rotation ----
    l_Rotation.setText("Rotation");
    l_Rotation.setName("l_Rotation");
    add(l_Rotation, cc.xy(2, 14));
    
    // ---- cb_Rotation ----
    cb_Rotation.setName("cb_Rotation");
    cb_Rotation.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        cb_RotationFocusLost(e);
      }
    });
    add(cb_Rotation, cc.xy(4, 14));
    
    // ---- label7 ----
    label7.setText("\u00b0");
    label7.setName("label7");
    add(label7, cc.xy(5, 14));
    
    // ---- l_PosTexte ----
    l_PosTexte.setText("Pos texte");
    l_PosTexte.setName("l_PosTexte");
    add(l_PosTexte, cc.xy(2, 16));
    
    // ---- cb_PosTexte ----
    cb_PosTexte.setName("cb_PosTexte");
    cb_PosTexte.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        cb_PosTexteFocusLost(e);
      }
    });
    add(cb_PosTexte, cc.xy(4, 16));
    
    // ---- label1 ----
    label1.setText("LModule");
    label1.setName("label1");
    add(label1, cc.xy(2, 18));
    
    // ---- tf_LargeurModule ----
    tf_LargeurModule.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_LargeurModule.setName("tf_LargeurModule");
    tf_LargeurModule.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tf_LargeurModuleFocusLost(e);
      }
    });
    add(tf_LargeurModule, cc.xy(4, 18));
    
    // ---- label6 ----
    label6.setText("mm");
    label6.setName("label6");
    add(label6, cc.xy(5, 18));
    
    // ---- l_Condition ----
    l_Condition.setText("Condition");
    l_Condition.setName("l_Condition");
    add(l_Condition, cc.xy(2, 20));
    
    // ---- cb_Condition ----
    cb_Condition.setModel(new DefaultComboBoxModel(new String[] { "(aucune)" }));
    cb_Condition.setName("cb_Condition");
    cb_Condition.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        cb_ConditionFocusGained(e);
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        cb_ConditionFocusLost(e);
      }
    });
    add(cb_Condition, cc.xywh(4, 20, 2, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_Type;
  private JComboBox cb_Type;
  private JLabel l_Message;
  private JTextField tf_Message;
  private JLabel label2;
  private JSpinner sp_Largeur;
  private JLabel l_LargeurPx;
  private JLabel label3;
  private JSpinner sp_Hauteur;
  private JLabel l_HauteurPx;
  private JLabel label4;
  private JSpinner sp_X;
  private JLabel l_XPx;
  private JLabel label5;
  private JSpinner sp_Y;
  private JLabel l_YPx;
  private JLabel l_Rotation;
  private JComboBox cb_Rotation;
  private JLabel label7;
  private JLabel l_PosTexte;
  private JComboBox cb_PosTexte;
  private JLabel label1;
  private JTextField tf_LargeurModule;
  private JLabel label6;
  private JLabel l_Condition;
  private JComboBox cb_Condition;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
