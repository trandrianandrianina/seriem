/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import rad.PageEditor.GfxPageEditor;

//=================================================================================================
//==> Panel permettant de changer les propri�t�s d'un label
//=================================================================================================
public class GfxPropertyObject extends JPanel {
  private static final long serialVersionUID = 1L;
  public static final String VALEUR_DIFF = "[Valeurs diff�rentes]";
  public static final int VALEUR_DIFF_INT = -1;
  public static final float VALEUR_DIFF_FLOAT = -1f;
  
  // Variables
  private GfxObject objet = null;
  private GfxPropertyLabel p_propertyLabel = null;
  private GfxPropertyImage p_propertyImage = null;
  private GfxPropertyCodeBarre p_propertyCodeBarre = null;
  
  /**
   * Constructeur
   */
  public GfxPropertyObject(final GfxPageEditor pageEditor) {
    setLayout(new BorderLayout());
    p_propertyLabel = new GfxPropertyLabel(pageEditor);
    p_propertyImage = new GfxPropertyImage(pageEditor);
    p_propertyCodeBarre = new GfxPropertyCodeBarre(pageEditor);
  }
  
  /**
   * Retourne le panel des propri�t�s adapt� � l'objet s�lectionn�
   * @param objet
   * @return
   */
  public void getPanelProperty(GfxObject objet) {
    if (objet == null)
      return;
    
    // R�cup�ration du parent de l'objet
    GfxEtiquette parent = (GfxEtiquette) objet.getParent();
    
    if (objet instanceof GfxLabel) {
      if (this.objet != objet) {
        removeAll();
        add(p_propertyLabel, BorderLayout.CENTER);
        p_propertyLabel.setVisible(true);
        validate();
        repaint();
        this.objet = objet;
      }
      if ((parent == null) || (parent.getSelectionGfxObjet().size() <= 1))
        p_propertyLabel.setObject2PanelAttributs(objet);
      else
        p_propertyLabel.addObject2PanelAttributs(objet);
    }
    else if (objet instanceof GfxImage) {
      if (this.objet != objet) {
        removeAll();
        add(p_propertyImage, BorderLayout.CENTER);
        p_propertyImage.setVisible(true);
        validate();
        repaint();
        this.objet = objet;
      }
      if ((parent == null) || (parent.getSelectionGfxObjet().size() <= 1))
        p_propertyImage.setObject2PanelAttributs(objet);
      else
        p_propertyImage.addObject2PanelAttributs(objet);
    }
    else if (objet instanceof GfxCodeBarre) {
      if (this.objet != objet) {
        removeAll();
        add(p_propertyCodeBarre, BorderLayout.CENTER);
        p_propertyCodeBarre.setVisible(true);
        validate();
        repaint();
        this.objet = objet;
      }
      if ((parent == null) || (parent.getSelectionGfxObjet().size() <= 1))
        p_propertyCodeBarre.setObject2PanelAttributs(objet);
      else
        p_propertyCodeBarre.addObject2PanelAttributs(objet);
    }
  }
  
}
