/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;

import rad.PageEditor.GfxPageEditor;
import rad.outils.CalculDPI;

//=================================================================================================
//==> Panel permettant de changer les propri�t�s d'un label
//=================================================================================================
public class GfxPropertyLabel extends JPanel {
  // Constantes
  private static final long serialVersionUID = 1L;
  private static final String UNITE = "cm";
  
  // Variables
  private GfxLabel object = null;
  private GfxPageEditor pageEditor = null;
  private int nbr2condition = 0;
  private boolean modeMultiSelection = false;
  
  /**
   * Constructeur
   */
  public GfxPropertyLabel(final GfxPageEditor pageEditor) {
    initComponents();
    setPageEditor(pageEditor);
    // initGestionLostFocus(sp_Largeur);
    // initGestionLostFocus(sp_Hauteur);
    // initGestionLostFocus(sp_X);
    // initGestionLostFocus(sp_Y);
    // initGestionLostFocus(sp_TaillePolice);
  }
  
  /**
   * Initialise l'�diteur de page
   * @param pageEditor
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label
   * @param label
   */
  public void setObject2PanelAttributs(GfxObject label) {
    object = (GfxLabel) label;
    int dpi_aff = label.getDpiAff();
    int dpi_img = label.getDpiImg();
    
    modeMultiSelection = false;
    
    if (!tf_Texte.getText().equals(object.getText()))
      tf_Texte.setText(object.getText());
    chk_TrimTexte.setSelected(object.isTrimTexte());
    sp_Largeur.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, object.getWidth()));
    // l_LargeurPx.setText(object.getWidth() + " px");
    l_LargeurPx.setText(UNITE);
    sp_Hauteur.setValue(CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getHeight()));
    // l_HauteurPx.setText(object.getHeight() + " px");
    l_HauteurPx.setText(UNITE);
    sp_X.setValue(CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().x));
    // l_XPx.setText(object.getLocation().x + " px");
    l_XPx.setText(UNITE);
    sp_Y.setValue(CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().y));
    // l_YPx.setText(object.getLocation().y + " px");
    l_YPx.setText(UNITE);
    // System.out.println("-setObject2PanelAttributs-> " + sp_Hauteur.getValue() + " " +object.getHeight());
    // La police
    // System.out.println("-plabel-> " + object.getText() +" : " + object.getFont().getName() + " " + object.getFont().getFontName());
    cb_Police.setSelectedItem(object.getNomPolice()); // Attention � la casse des noms des polices (serif, SansSerif, Monopaced), c'est
                                                      // important
    // sp_TaillePolice.setValue(Integer.valueOf( CalculDPI.getPoliceDescription2PoliceEcran(object.getDpiImg(),
    // (object.getFont().getSize()) )));
    // System.out.println("--> " + object.getTaillePolice());
    sp_TaillePolice.setValue(object.getTaillePolice());
    l_TaillePolice.setText(object.getFont().getSize() + " pt");
    cb_StylePolice.setSelectedIndex(object.getStylePolice());
    cb_JustificationH.setSelectedIndex(object.getHorizontalText2Description(object.getHorizontalAlignment()));
    
    // Les conditions
    chargeComboCondition(object.getCondition());
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * @param label
   */
  public void addObject2PanelAttributs(GfxObject obj) {
    object = (GfxLabel) obj;
    int dpi_aff = obj.getDpiAff();
    int dpi_img = obj.getDpiImg();
    
    modeMultiSelection = true;
    addObject2PanelAttributs_coordonnees(obj, dpi_aff, dpi_img);
    if (obj instanceof GfxLabel) {
      // Le texte
      addObject2PanelAttributs_texte((GfxLabel) obj, dpi_aff, dpi_img);
      // Les conditions
      chargeComboCondition(object.getCondition());
    }
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * Pour les coordonn�es / dimensions
   * @param label
   * @param dpi_aff
   * @param dpi_img
   */
  private void addObject2PanelAttributs_coordonnees(GfxObject label, int dpi_aff, int dpi_img) {
    // Les coordonn�es, dimension
    if (((Float) sp_Largeur.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, dpi_img, object.getWidth()))
      sp_Largeur.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_Hauteur.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getHeight()))
      sp_Hauteur.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_X.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().x))
      sp_X.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_Y.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().y))
      sp_Y.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * Pour le texte
   * @param label
   * @param dpi_aff
   * @param dpi_img
   */
  private void addObject2PanelAttributs_texte(final GfxLabel label, int dpi_aff, int dpi_img) {
    // Le texte
    if (!tf_Texte.getText().equals(label.getText()))
      tf_Texte.setText(GfxPropertyObject.VALEUR_DIFF);
    // chk_TrimTexte.setSelected(label.isTrimTexte());
    
    // La police
    
    if ((cb_Police.getSelectedItem() != null) && (!cb_Police.getSelectedItem().equals(label.getNomPolice())))
      cb_Police.setSelectedIndex(GfxPropertyObject.VALEUR_DIFF_INT);
    if (((Float) sp_TaillePolice.getValue()).floatValue() != label.getTaillePolice())
      sp_TaillePolice.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    l_TaillePolice.setText("");
    if (cb_StylePolice.getSelectedIndex() != label.getStylePolice())
      cb_StylePolice.setSelectedIndex(GfxPropertyObject.VALEUR_DIFF_INT);
    if (cb_JustificationH.getSelectedIndex() != label.getHorizontalText2Description(label.getHorizontalAlignment()))
      cb_JustificationH.setSelectedIndex(GfxPropertyObject.VALEUR_DIFF_INT);
  }
  
  /**
   * Met � jour le label � partir des attributs saisis dans les attributs
   */
  private void setPanel2LabelAttributs() {
    int dpi_aff = object.getDpiAff();
    int dpi_img = object.getDpiImg();
    
    // System.out.println("-setPanel2LabelAttributs-> " + sp_Hauteur.getValue() + " " +CalculDPI.getCmtoPx(dpi_aff, dpi_img,
    // ((Float)sp_Hauteur.getValue()).floatValue()));
    
    // On met � jour
    object.setText(tf_Texte.getText());
    object.setTrimTexte(chk_TrimTexte.isSelected());
    object.setLocation(CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_X.getValue()).floatValue()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Y.getValue()).floatValue()));
    object.setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Largeur.getValue()).floatValue()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Hauteur.getValue()).floatValue()));
    // l_XPx.setText(object.getLocation().x + " px");
    // l_YPx.setText(object.getLocation().y + " px");
    // l_LargeurPx.setText(object.getWidth() + " px");
    // l_HauteurPx.setText(object.getHeight() + " px");
    
    // object.setFont(new Font((String)cb_Police.getSelectedItem(), cb_StylePolice.getSelectedIndex(),
    // CalculDPI.getPoliceEcran2PoliceDescription(dpi_img, ((Integer)sp_TaillePolice.getValue()).intValue()) ));
    object.setNomPolice((String) cb_Police.getSelectedItem());
    object.setStylePolice(cb_StylePolice.getSelectedIndex());
    object.setTaillePolice(((Float) sp_TaillePolice.getValue()).floatValue());
    // object.setFont(new Font((String)cb_Police.getSelectedItem(), cb_StylePolice.getSelectedIndex(),
    // 10).deriveFont(((Float)sp_TaillePolice.getValue()).floatValue()));
    l_TaillePolice.setText(object.getFont().getSize() + " pt");
    
    // object.setHorizontalTextPosition(SwingConstants.RIGHT);//(horizontal_align);
    object.setHorizontalAlignment(((GfxLabel) object).getDescription2HorizontalText(cb_JustificationH.getSelectedIndex()));
    if (!cb_Condition.getSelectedItem().equals(GfxObject.PAS_DE_COND))
      object.setCondition((String) cb_Condition.getSelectedItem());
    else
      object.setCondition(null);
  }
  
  /**
   * Met � jour tous les labels � partir des attributs saisis dans les attributs
   */
  private void setPanel2AllLabelAttributs() {
    int dpi_aff = object.getDpiAff();
    int dpi_img = object.getDpiImg();
    int a = 0, b = 0;
    
    GfxEtiquette parent = (GfxEtiquette) object.getParent();
    // System.out.println("-setPanel2AllLabelAttributs-> " + modeMultiSelection);
    // System.out.println("-setPanel2AllLabelAttributs-> " + parent.isKey_ctrl());
    // Tant que la touche control est enfonc�e on ne fait aucune modif
    if (parent.isKey_ctrl())
      return;
    
    ArrayList<GfxObject> listeSelectionObject = parent.getSelectionGfxObjet();
    
    // On met � jour
    for (int i = listeSelectionObject.size(); --i >= 0;) {
      // Position
      if (((Float) sp_X.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        a = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_X.getValue()).floatValue());
      else
        a = listeSelectionObject.get(i).getLocation().x;
      if (((Float) sp_Y.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        b = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Y.getValue()).floatValue());
      else
        b = listeSelectionObject.get(i).getLocation().y;
      listeSelectionObject.get(i).setLocation(a, b);
      // Taille
      if (((Float) sp_Largeur.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        a = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Largeur.getValue()).floatValue());
      else
        a = listeSelectionObject.get(i).getWidth();
      if (((Float) sp_Hauteur.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        b = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Hauteur.getValue()).floatValue());
      else
        b = listeSelectionObject.get(i).getHeight();
      listeSelectionObject.get(i).setSize(a, b);
      
      // Les attributs propres au label
      if (listeSelectionObject.get(i) instanceof GfxLabel) {
        if (!tf_Texte.getText().trim().equals(GfxPropertyObject.VALEUR_DIFF))
          listeSelectionObject.get(i).setText(tf_Texte.getText());
        // listeSelectionObject.get(i).setTrimTexte(chk_TrimTexte.isSelected());
        if (cb_Police.getSelectedIndex() != GfxPropertyObject.VALEUR_DIFF_INT)
          listeSelectionObject.get(i).setNomPolice((String) cb_Police.getSelectedItem());
        if (cb_StylePolice.getSelectedIndex() != GfxPropertyObject.VALEUR_DIFF_INT)
          listeSelectionObject.get(i).setStylePolice(cb_StylePolice.getSelectedIndex());
        if (((Float) sp_TaillePolice.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
          listeSelectionObject.get(i).setTaillePolice(((Float) sp_TaillePolice.getValue()).floatValue());
        // l_TaillePolice.setText(listeSelectionObject.get(i).getFont().getSize() + " pt");
        if (cb_JustificationH.getSelectedIndex() != GfxPropertyObject.VALEUR_DIFF_INT)
          listeSelectionObject.get(i).setHorizontalAlignment(
              ((GfxLabel) listeSelectionObject.get(i)).getDescription2HorizontalText(cb_JustificationH.getSelectedIndex()));
      }
      
      // if (!cb_Condition.getSelectedItem().equals(GfxObject.PAS_DE_COND))
      // listeSelectionObject.get(i).setCondition((String)cb_Condition.getSelectedItem());
    }
  }
  
  /**
   * Gestion de la perte du focus sur un JSpinner
   * @param spinner
   *
   *          private void initGestionLostFocus(final JSpinner spinner)
   *          {
   *          ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().addFocusListener(new FocusAdapter()
   *          {
   *          public void focusLost(FocusEvent evt)
   *          {
   *          try
   *          {
   *          spinner.commitEdit();
   *          }
   *          catch (ParseException e) {}
   *          setPanel2LabelAttributs();
   *          }
   *          });
   * 
   *          }
   */
  
  /**
   * Charge la combo des conditions
   */
  private void chargeComboCondition(String cond) {
    nbr2condition = pageEditor.getListeCondition().size();
    String[] nomCondition = new String[pageEditor.getListeCondition().size() + 1];
    nomCondition[0] = GfxObject.PAS_DE_COND;
    for (int i = 0; i < nbr2condition; i++)
      nomCondition[i + 1] = pageEditor.getListeCondition().get(i).getNom();
    cb_Condition.setModel(new DefaultComboBoxModel(nomCondition));
    if (cond != null)
      cb_Condition.setSelectedItem(cond);
    
  }
  
  // Gestion des �v�nements -----------------------------------------------------------------------------------------------------
  private void tf_TexteFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  /*
  	private void cb_PoliceFocusLost(FocusEvent e) {
  		setPanel2LabelAttributs();
  	}
  
  	private void cb_StylePoliceFocusLost(FocusEvent e) {
  		setPanel2LabelAttributs();
  	}
  
  	private void cb_JustificationHFocusLost(FocusEvent e) {
  		setPanel2LabelAttributs();
  	}
  */
  private void chk_TrimTexteActionPerformed(ActionEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void cb_ConditionFocusGained(FocusEvent e) {
    if (!modeMultiSelection)
      if (nbr2condition != pageEditor.getListeCondition().size())
        chargeComboCondition(null);
  }
  
  private void cb_ConditionFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void sp_LargeurStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null)
        object.setSize(CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Largeur.getValue()).floatValue()),
            object.getHeight());
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_HauteurStateChanged(ChangeEvent e) {
    // System.out.println("-sp_HauteurStateChanged-> " + sp_Hauteur.getValue() + " " +CalculDPI.getCmtoPx(object.getDpiAff(),
    // object.getDpiImg(), ((Float)sp_Hauteur.getValue()).floatValue()) + " " + object.getDpiAff() + " " + object.getDpiImg());
    if (!modeMultiSelection) {
      if (object != null)
        object.setSize(object.getWidth(),
            CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Hauteur.getValue()).floatValue()));
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_XStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null)
        object.setLocation(CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_X.getValue()).floatValue()),
            object.getLocation().y);
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_YStateChanged(ChangeEvent e) {
    // System.out.println("--> modeMultiSelectionY " + modeMultiSelection);
    if (!modeMultiSelection) {
      if (object != null)
        object.setLocation(object.getLocation().x,
            CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Y.getValue()).floatValue()));
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void cb_PoliceItemStateChanged(ItemEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setNomPolice((String) cb_Police.getSelectedItem());
        object.setFont(new Font(object.getNomPolice(), object.getStylePolice(), 10).deriveFont(object.getTaillePolice()));
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_TaillePoliceStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setTaillePolice(((Float) sp_TaillePolice.getValue()).floatValue());
        // object.setFont(new Font(object.getFont().getName(), object.getFont().getStyle(),
        // CalculDPI.getPoliceEcran2PoliceDescription(object.getDpiImg(), ((Integer)sp_TaillePolice.getValue()).intValue()) ));
        object.setFont(new Font(object.getNomPolice(), object.getStylePolice(), 10).deriveFont(object.getTaillePolice()));
        l_TaillePolice.setText(object.getFont().getSize() + " pt");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void cb_StylePoliceItemStateChanged(ItemEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setStylePolice(cb_StylePolice.getSelectedIndex());
        object.setFont(new Font(object.getNomPolice(), object.getStylePolice(), 10).deriveFont(object.getTaillePolice()));
        // object.setFont(new Font(object.getFont().getName(), cb_StylePolice.getSelectedIndex(), object.getFont().getSize()));
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void cb_JustificationHItemStateChanged(ItemEvent e) {
    if (!modeMultiSelection) {
      if (object != null)
        object.setHorizontalAlignment(((GfxLabel) object).getDescription2HorizontalText(cb_JustificationH.getSelectedIndex()));
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  // -----------------------------------------------------------------------------------------------------------------------------
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    label1 = new JLabel();
    tf_Texte = new JTextField();
    label2 = new JLabel();
    sp_Largeur = new JSpinner();
    l_LargeurPx = new JLabel();
    label3 = new JLabel();
    sp_Hauteur = new JSpinner();
    l_HauteurPx = new JLabel();
    label4 = new JLabel();
    sp_X = new JSpinner();
    l_XPx = new JLabel();
    label5 = new JLabel();
    sp_Y = new JSpinner();
    l_YPx = new JLabel();
    label6 = new JLabel();
    cb_Police = new JComboBox();
    label7 = new JLabel();
    sp_TaillePolice = new JSpinner();
    l_TaillePolice = new JLabel();
    label8 = new JLabel();
    cb_StylePolice = new JComboBox();
    label11 = new JLabel();
    cb_JustificationH = new JComboBox();
    chk_TrimTexte = new JCheckBox();
    l_Condition = new JLabel();
    cb_Condition = new JComboBox();
    CellConstraints cc = new CellConstraints();
    
    // ======== this ========
    setBorder(new TitledBorder("Propri\u00e9t\u00e9s du Label"));
    setMinimumSize(new Dimension(250, 450));
    setPreferredSize(new Dimension(250, 450));
    setName("this");
    setLayout(new FormLayout(
        new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
            new ColumnSpec(Sizes.dluX(49)), FormFactory.DEFAULT_COLSPEC, new ColumnSpec(Sizes.DLUX4) },
        new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.NO_GROW),
            FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
            FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
            FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
            FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
            FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
            FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC }));
    
    // ---- label1 ----
    label1.setText("Texte");
    label1.setName("label1");
    add(label1, cc.xy(2, 2));
    
    // ---- tf_Texte ----
    tf_Texte.setHorizontalAlignment(SwingConstants.LEFT);
    tf_Texte.setName("tf_Texte");
    tf_Texte.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tf_TexteFocusLost(e);
      }
    });
    add(tf_Texte, cc.xywh(4, 2, 2, 1));
    
    // ---- label2 ----
    label2.setText("Largeur");
    label2.setName("label2");
    add(label2, cc.xy(2, 4));
    
    // ---- sp_Largeur ----
    sp_Largeur.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 0.1F));
    sp_Largeur.setName("sp_Largeur");
    sp_Largeur.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_LargeurStateChanged(e);
      }
    });
    add(sp_Largeur, cc.xy(4, 4));
    
    // ---- l_LargeurPx ----
    l_LargeurPx.setText(" ? px");
    l_LargeurPx.setName("l_LargeurPx");
    add(l_LargeurPx, cc.xy(5, 4));
    
    // ---- label3 ----
    label3.setText("Hauteur");
    label3.setName("label3");
    add(label3, cc.xy(2, 6));
    
    // ---- sp_Hauteur ----
    sp_Hauteur.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 0.1F));
    sp_Hauteur.setName("sp_Hauteur");
    sp_Hauteur.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_HauteurStateChanged(e);
      }
    });
    add(sp_Hauteur, cc.xy(4, 6));
    
    // ---- l_HauteurPx ----
    l_HauteurPx.setText("? px");
    l_HauteurPx.setName("l_HauteurPx");
    add(l_HauteurPx, cc.xy(5, 6));
    
    // ---- label4 ----
    label4.setText("X");
    label4.setName("label4");
    add(label4, cc.xy(2, 8));
    
    // ---- sp_X ----
    sp_X.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_X.setName("sp_X");
    sp_X.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_XStateChanged(e);
      }
    });
    add(sp_X, cc.xy(4, 8));
    
    // ---- l_XPx ----
    l_XPx.setText("? px");
    l_XPx.setName("l_XPx");
    add(l_XPx, cc.xy(5, 8));
    
    // ---- label5 ----
    label5.setText("Y");
    label5.setName("label5");
    add(label5, cc.xy(2, 10));
    
    // ---- sp_Y ----
    sp_Y.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Y.setName("sp_Y");
    sp_Y.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_YStateChanged(e);
      }
    });
    add(sp_Y, cc.xy(4, 10));
    
    // ---- l_YPx ----
    l_YPx.setText("? px");
    l_YPx.setName("l_YPx");
    add(l_YPx, cc.xy(5, 10));
    
    // ---- label6 ----
    label6.setText("Police");
    label6.setName("label6");
    add(label6, cc.xy(2, 12));
    
    // ---- cb_Police ----
    cb_Police.setModel(new DefaultComboBoxModel(new String[] { "Monospaced", "SansSerif", "serif" }));
    cb_Police.setToolTipText("Monospaced:Courier, SansSerif:Arial, serif:Times New Roman");
    cb_Police.setName("cb_Police");
    cb_Police.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        cb_PoliceItemStateChanged(e);
      }
    });
    add(cb_Police, cc.xywh(4, 12, 2, 1));
    
    // ---- label7 ----
    label7.setText("Taille police");
    label7.setName("label7");
    add(label7, cc.xy(2, 14));
    
    // ---- sp_TaillePolice ----
    sp_TaillePolice.setModel(new SpinnerNumberModel(8.0F, null, null, 0.1F));
    sp_TaillePolice.setName("sp_TaillePolice");
    sp_TaillePolice.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_TaillePoliceStateChanged(e);
      }
    });
    add(sp_TaillePolice, cc.xy(4, 14));
    
    // ---- l_TaillePolice ----
    l_TaillePolice.setText("? pt");
    l_TaillePolice.setName("l_TaillePolice");
    add(l_TaillePolice, cc.xy(5, 14));
    
    // ---- label8 ----
    label8.setText("Style police");
    label8.setName("label8");
    add(label8, cc.xy(2, 16));
    
    // ---- cb_StylePolice ----
    cb_StylePolice.setModel(new DefaultComboBoxModel(new String[] { "Normal", "Normal Gras", "Italique Normal", "Italique Gras" }));
    cb_StylePolice.setName("cb_StylePolice");
    cb_StylePolice.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        cb_StylePoliceItemStateChanged(e);
      }
    });
    add(cb_StylePolice, cc.xywh(4, 16, 2, 1));
    
    // ---- label11 ----
    label11.setText("Justification");
    label11.setName("label11");
    add(label11, cc.xy(2, 18));
    
    // ---- cb_JustificationH ----
    cb_JustificationH.setModel(new DefaultComboBoxModel(new String[] { "Gauche", "Centre", "Droite" }));
    cb_JustificationH.setName("cb_JustificationH");
    cb_JustificationH.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        cb_JustificationHItemStateChanged(e);
      }
    });
    add(cb_JustificationH, cc.xywh(4, 18, 2, 1));
    
    // ---- chk_TrimTexte ----
    chk_TrimTexte.setText("Trim du texte");
    chk_TrimTexte.setEnabled(false);
    chk_TrimTexte.setName("chk_TrimTexte");
    chk_TrimTexte.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        chk_TrimTexteActionPerformed(e);
      }
    });
    add(chk_TrimTexte, cc.xywh(4, 20, 3, 1));
    
    // ---- l_Condition ----
    l_Condition.setText("Condition");
    l_Condition.setName("l_Condition");
    add(l_Condition, cc.xy(2, 22));
    
    // ---- cb_Condition ----
    cb_Condition.setModel(new DefaultComboBoxModel(new String[] { "(aucune)" }));
    cb_Condition.setName("cb_Condition");
    cb_Condition.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        cb_ConditionFocusGained(e);
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        cb_ConditionFocusLost(e);
      }
    });
    add(cb_Condition, cc.xywh(4, 22, 2, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JTextField tf_Texte;
  private JLabel label2;
  private JSpinner sp_Largeur;
  private JLabel l_LargeurPx;
  private JLabel label3;
  private JSpinner sp_Hauteur;
  private JLabel l_HauteurPx;
  private JLabel label4;
  private JSpinner sp_X;
  private JLabel l_XPx;
  private JLabel label5;
  private JSpinner sp_Y;
  private JLabel l_YPx;
  private JLabel label6;
  private JComboBox cb_Police;
  private JLabel label7;
  private JSpinner sp_TaillePolice;
  private JLabel l_TaillePolice;
  private JLabel label8;
  private JComboBox cb_StylePolice;
  private JLabel label11;
  private JComboBox cb_JustificationH;
  private JCheckBox chk_TrimTexte;
  private JLabel l_Condition;
  private JComboBox cb_Condition;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
