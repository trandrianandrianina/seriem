/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import description.DescriptionObject;
import rad.PageEditor.GfxPageEditor;
import rad.outils.CalculDPI;

//=================================================================================================
//==> Objet de base
//=================================================================================================
public class GfxObject extends JLabel implements Comparable<Object> {
  // Constantes
  private static final long serialVersionUID = 1L;
  public static final String PAS_DE_COND = "(Aucune)";
  
  // Variables
  protected GfxPageEditor pageEditor = null;
  private int dx = 0, dy = 0;
  protected String nomPolice = "Monospaced";
  protected int stylePolice = 0; // 0=Plain 1=Bold 2=Italic
  protected float taillePolice = 10f; // Taille de la police en pt
  private boolean focusBorder = true;
  protected boolean redimensionne = false;
  protected int dpi_aff = 72; // Nombre de dpi dans le rad
  protected int dpi_img = 72; // Nombre de dpi dans le rad
  protected boolean clic_gauche = false;
  protected boolean key_ctrl = false;
  protected String condition = null; // Suite des conditions saisie en notation polonaise (liste contenu dans la description de page),
  // le r�sultat doit �tre vrai pour que l'objet s'affiche (ex: COND_1 COND_2 &)
  private boolean ytrie = false; // Organise le trie sur X ou Y (il faut modifier tous les objets avant le sort)
  
  /**
   * Constructeur
   * @param x
   * @param y
   */
  public GfxObject(int x, int y, final GfxPageEditor pageEditor) {
    setOpaque(true);
    setLocation(x, y);
    setSize(getPreferredSize());
    initDeplacement();
    setPageEditor(pageEditor);
  }
  
  /**
   * Constructeur
   * @param x
   * @param y
   * @param largeur
   * @param hauteur
   */
  public GfxObject(int x, int y, int largeur, int hauteur, final GfxPageEditor pageEditor) {
    setOpaque(true);
    setLocation(x, y);
    setSize(largeur, hauteur);
    initDeplacement();
    setPageEditor(pageEditor);
  }
  
  /**
   * Constructeur
   * @param text
   */
  public GfxObject(DescriptionObject dobject, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    this(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getXPos_cm()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getYPos_cm()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getLargeur()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getHauteur()),
        planTravail);
    setDpiAff(dpi_aff);
    setDpiAff(dpi_img);
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(final GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
    if (pageEditor != null)
      pageEditor.getPropertyObject().getPanelProperty(this);
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * Initialise la fonction de d�placement
   */
  public void initDeplacement() {
    // Ecouteur Souris
    addMouseMotionListener(new MouseMotionAdapter() {
      public void mouseMoved(MouseEvent e) {
        labelMouseMoved(e);
      }
    });
    
    addMouseMotionListener(new MouseMotionAdapter() {
      public void mouseDragged(MouseEvent e) {
        labelMouseDragged(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        labelMousePressed(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      public void mouseReleased(MouseEvent e) {
        labelMouseReleased(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent e) {
        labelMouseEntered(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      public void mouseExited(MouseEvent e) {
        labelMouseExited(e);
      }
    });
  }
  
  /**
   * Active le border pour indiquer qu'il a le focus
   * @param border
   */
  public void setFocusBorder(boolean border, boolean keypressed, boolean btdreleased) {
    // System.out.println("border:" + border + " key_ctrl:" + key_ctrl + " nbr:"+ getNbrSelection() + " pressed:" + pressed);
    // On demande l'activation de la s�lection
    if (border)
      // Est ce que Ctrl est enfonc� ?
      if (key_ctrl)
        border = !focusBorder;
      else if (getNbrSelection() > 1) {
        if (!keypressed && btdreleased)
          enleveFocusBorder();
      }
      else
        enleveFocusBorder();
      
    // Traitement
    focusBorder = border;
    if (focusBorder)
      setBorder(BorderFactory.createLineBorder(Color.red));
    else
      setBorder(null);
  }
  
  /**
   * Active le border (forc�)
   * @param border
   */
  public void setFocusBorder(boolean border) {
    // Traitement
    focusBorder = border;
    if (focusBorder)
      setBorder(BorderFactory.createLineBorder(Color.red));
    else
      setBorder(null);
  }
  
  /**
   * Retourne si le label a le border du focus
   * @return
   */
  public boolean isFocusBorder() {
    return focusBorder;
  }
  
  /**
   * Enleve le focus Border des autres labels du panel
   */
  private void enleveFocusBorder() {
    if (getParent() == null)
      return;
    
    Component[] listeLabel = getParent().getComponents();
    for (int i = 0; i < listeLabel.length; i++)
      if (listeLabel[i] instanceof GfxObject)
        ((GfxObject) listeLabel[i]).setFocusBorder(false, true, true);
    getParent().repaint();
  }
  
  /**
   * Retourne le nombre d'objets s�lectionn�e
   * @return
   */
  private int getNbrSelection() {
    if (getParent() == null)
      return 0;
    
    int nbr = 0;
    Component[] listeLabel = getParent().getComponents();
    for (int i = 0; i < listeLabel.length; i++)
      if ((listeLabel[i] instanceof GfxObject) && ((GfxObject) listeLabel[i]).isFocusBorder())
        nbr++;
    return nbr;
  }
  
  /**
   * Affiche le menu contextuel
   * @param e
   * @param btd
   */
  private void maybeShowPopup(MouseEvent e, JPopupMenu btd) {
    // if (e.isPopupTrigger())
    // {
    btd.show(e.getComponent(), e.getX(), e.getY());
    // }
  }
  
  /**
   * Action lors du d�placement de la souris avec bouton appuy� donc du label
   * @param e
   */
  protected void labelMouseDragged(MouseEvent e) {
    if (SwingUtilities.isRightMouseButton(e))
      return;
    
    // On redimensionne l'objet
    if (redimensionne)
      switch (getCursor().getType()) {
        case Cursor.N_RESIZE_CURSOR:
          setLocation(getX(), getY() + e.getY());
          setSize(getWidth(), getHeight() - e.getY());
          break;
        case Cursor.S_RESIZE_CURSOR:
          setSize(getWidth(), e.getY());
          break;
        case Cursor.E_RESIZE_CURSOR:
          setSize(e.getX(), getHeight());
          break;
        case Cursor.W_RESIZE_CURSOR:
          setLocation(getX() + e.getX(), getY());
          setSize(getWidth() - e.getX(), getHeight());
          break;
      }
    else // On d�place la s�lection
    {
      GfxEtiquette parent = (GfxEtiquette) getParent();
      ArrayList<GfxObject> listeSelectionObject = parent.getSelectionGfxObjet();
      // Calcul du delta
      int deltaX = e.getX() - dx;
      int deltaY = e.getY() - dy;
      // Modification des coordonn�es
      if (listeSelectionObject.size() == 1)
        setLocation(getX() + deltaX, getY() + deltaY);
      else
        // for (int i=listeSelectionObject.size(); --i>=0;)
        for (int i = 0; i < listeSelectionObject.size(); i++) {
          Point p = listeSelectionObject.get(i).getLocation();
          listeSelectionObject.get(i).setLocation(p.x + deltaX, p.y + deltaY);
          // System.out.println("-"+i+"-> " + listeSelectionObject.get(i).getLocation());
        }
    }
  }
  
  /**
   * Action lors du d�placement de la souris donc du label
   * @param e
   */
  protected void labelMouseMoved(MouseEvent e) {
    // if ( (redimensionne) || ((getPlanTravail() != null) &&
    // (!getPlanTravail().getZoneCreation().getCursor().equals(Cursor.getDefaultCursor())))) return;
    
    // Les lignes
    Rectangle haut = new Rectangle(3, 0, getWidth() - 6, 2);
    Rectangle bas = new Rectangle(3, getHeight() - 2, getWidth() - 6, 2);
    Rectangle gauche = new Rectangle(0, 3, 2, getHeight() - 6);
    Rectangle droite = new Rectangle(getWidth() - 2, 3, 2, getHeight() - 4);
    
    if (haut.contains(e.getPoint()))
      setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
    else if (bas.contains(e.getPoint()))
      setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
    else if (gauche.contains(e.getPoint()))
      setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
    else if (droite.contains(e.getPoint()))
      setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
    else
      setCursor(Cursor.getDefaultCursor());
  }
  
  /**
   * Action lors du clic souris sur label (bouton press�)
   * @param e
   */
  private void labelMousePressed(MouseEvent e) {
    clic_gauche = e.getButton() == MouseEvent.BUTTON1;
    key_ctrl = e.isMetaDown() || e.isControlDown();
    
    // On v�rifie si c'est un redimensionnement du label
    if ((getCursor().getType() == Cursor.N_RESIZE_CURSOR) || (getCursor().getType() == Cursor.S_RESIZE_CURSOR)
        || (getCursor().getType() == Cursor.E_RESIZE_CURSOR) || (getCursor().getType() == Cursor.W_RESIZE_CURSOR))
      redimensionne = true;
    else
      redimensionne = false;
    
    // Si bouton gauche press�
    if (SwingUtilities.isLeftMouseButton(e)) {
      // Test si double clic pour saisie du libell�
      if (e.getClickCount() == 2)
        saisieLibelle();
      else {
        // On planque les coordonn�es pour le d�placement
        dx = e.getX();
        dy = e.getY();
        // System.out.println("--> " + dx + " " + dy);
        // On change la couleur du border pour indiquer qu'il a le focus
        setFocusBorder(true, key_ctrl, false);
      }
    }
  }
  
  /**
   * Action lors du clic souris sur label (bouton lach�)
   * @param e
   */
  protected void labelMouseReleased(MouseEvent e) {
    clic_gauche = false;
    key_ctrl = e.isMetaDown() || e.isControlDown();
    
    if (redimensionne)
      redimensionne = false;
    // else
    // if (SwingUtilities.isLeftMouseButton(e))
    // setLocation(getX(), getY());
    
    // On v�rifie si on doit changer le menu contextuel
    if (SwingUtilities.isRightMouseButton(e))
      if ((getParent() != null) && (getParent() instanceof GfxEtiquette)) {
        GfxEtiquette parent = (GfxEtiquette) getParent();
        if (parent.getSelectionGfxObjet().size() <= 1) {
          if (this instanceof GfxLabel)
            maybeShowPopup(e, parent.getPopMenuLabel());
          else if (this instanceof GfxImage)
            maybeShowPopup(e, parent.getPopMenuImage());
          else if (this instanceof GfxCodeBarre)
            maybeShowPopup(e, parent.getPopMenuCodeBarre());
          
        }
        else
          maybeShowPopup(e, parent.getPopMenuMultiSelection());
      }
  }
  
  /**
   * Action lorsque la souris est sur le label
   * @param e
   */
  private void labelMouseEntered(MouseEvent e) {
    setBorder(BorderFactory.createLineBorder(new Color(255, 153, 0)));
  }
  
  /**
   * Action lorsque la souris sort du label
   * @param e
   */
  private void labelMouseExited(MouseEvent e) {
    if (isFocusBorder())
      setBorder(BorderFactory.createLineBorder(Color.red));
    else
      setBorder(null);
  }
  
  /**
   * Permet de saisir un libell� dans le label
   */
  private void saisieLibelle() {
    Dimension d = getSize();
    Point p = getLocation();
    if (p.y < (getParent().getHeight() - d.height * 2))
      p.y = p.y + d.height;
    else
      p.y = p.y - (d.height * 2);
    d.height = d.height + getFont().getSize();
    new SaisieVolatile(p, d, this);
  }
  
  /**
   * @return the dpi
   */
  public int getDpiAff() {
    return dpi_aff;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiAff(int dpi) {
    this.dpi_aff = dpi;
  }
  
  /**
   * @return the dpi
   */
  public int getDpiImg() {
    return dpi_img;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiImg(int dpi) {
    this.dpi_img = dpi;
  }
  
  /**
   * @param taillePolice the taillePolice to set
   */
  public void setTaillePolice(float taillePolice) {
    this.taillePolice = taillePolice;
  }
  
  /**
   * @return the taillePolice
   */
  public float getTaillePolice() {
    return taillePolice;
  }
  
  /**
   * @return the nomPolice
   */
  public String getNomPolice() {
    return nomPolice;
  }
  
  /**
   * @param nomPolice the nomPolice to set
   */
  public void setNomPolice(String nomPolice) {
    this.nomPolice = nomPolice;
  }
  
  /**
   * @return the stylePolice
   */
  public int getStylePolice() {
    return stylePolice;
  }
  
  /**
   * @param stylePolice the stylePolice to set
   */
  public void setStylePolice(int stylePolice) {
    this.stylePolice = stylePolice;
  }
  
  /**
   * @return the condition
   */
  public String getCondition() {
    return condition;
  }
  
  /**
   * @param condition the condition to set
   */
  public void setCondition(String condition) {
    this.condition = condition;
  }
  
  /**
   * @return the ytrie
   */
  public boolean isYtrie() {
    return ytrie;
  }
  
  /**
   * @param ytrie the ytrie to set
   */
  public void setYtrie(boolean ytrie) {
    this.ytrie = ytrie;
  }
  
  /**
   * Permet de trier les objets lorsqu'ils sont dans une arraylist
   * @param objet
   * @return
   */
  public int compareTo(Object objet) {
    if (ytrie) {
      if (getLocation().y > ((GfxObject) objet).getLocation().y)
        return 1;
      if (getLocation().y < ((GfxObject) objet).getLocation().y)
        return -1;
      if (getLocation().y == ((GfxObject) objet).getLocation().y)
        return 0;
    }
    else {
      if (getLocation().x > ((GfxObject) objet).getLocation().x)
        return 1;
      if (getLocation().x < ((GfxObject) objet).getLocation().x)
        return -1;
      if (getLocation().x == ((GfxObject) objet).getLocation().x)
        return 0;
    }
    return 0;
  }
  
}
