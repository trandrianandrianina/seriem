/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JTextField;

public class SaisieVolatile extends JTextField {
  // Constantes
  private static final long serialVersionUID = 1376116858127905427L;
  
  // Variables
  private JComponent composant = null;
  
  /**
   * Constructeur
   */
  public SaisieVolatile(Point position, Dimension taille, final JComponent composant) {
    setOpaque(true);
    addFocusListener(new FocusAdapter() {
      public void focusLost(FocusEvent e) {
        valideSaisie();
      }
    });
    addKeyListener(new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
          valideSaisie();
      }
    });
    setAffiche(position, taille, composant);
  }
  
  /**
   * Appel pour la saisie
   * @param position
   * @param taille
   * @param texte
   */
  public void setAffiche(Point position, Dimension taille, final JComponent composant) {
    setLocation(position);
    setSize(taille);
    this.composant = composant;
    composant.getParent().add(this);
    if (composant != null)
      if (composant instanceof GfxLabel)
        setText(((GfxLabel) composant).getText());
    requestFocus();
    selectAll();
    setVisible(true);
  }
  
  /**
   * Valide la saisie
   */
  private void valideSaisie() {
    if (composant != null)
      if (composant instanceof GfxLabel)
        ((GfxLabel) composant).setText(getText());
    setVisible(false);
  }
}
