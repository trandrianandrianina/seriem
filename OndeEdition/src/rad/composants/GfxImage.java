/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;

import description.DescriptionImage;
import rad.PageEditor.GfxPageEditor;
import rad.outils.CalculDPI;

//=================================================================================================
//==> Description de l'objet image pour RAD (g�n�ration Etiquette)
//=================================================================================================
public class GfxImage extends GfxObject {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private boolean keepRatio = true;
  private String cheminImage = null;
  private ImageIcon imageOriginale = null; // Image sans transformation
  private boolean avecDossier = false; // Pr�cise si les images sont identifi�es avec le chemin complet ou pas
  
  /**
   * Constructeur
   * @param x
   * @param y
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxImage(int x, int y, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(x, y, planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    setIcon(new ImageIcon(getClass().getResource("/images/alternative-detourage-photo-image-icone-4234-32.png")));
    setSize(getIcon().getIconWidth(), getIcon().getIconHeight());
    setHorizontalAlignment(CENTER);
    setBackground(Color.blue);
  }
  
  /**
   * Constructeur
   * @param dlabel
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxImage(DescriptionImage dimage, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(dimage, dpi_aff, dpi_img, planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    setImageOriginale(dimage.getCheminImage());
    
    if (getImageOriginale() == null)
      setIcon(new ImageIcon(getClass().getResource("/images/alternative-detourage-photo-image-icone-4234-32.png")));
    setHorizontalAlignment(CENTER);
    setBackground(Color.blue);
    
    setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dimage.getLargeur()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dimage.getHauteur()));
    setKeepRatio(dimage.isKeepRatio());
    if (dimage.getCheminImage() != null)
      avecDossier = dimage.getCheminImage().trim().startsWith("/");
    
    setFocusBorder(false, false, false);
    setCondition(dimage.getCondition());
  }
  
  /**
   * Redimensionne l'image
   * @param origine
   * @param rw
   * @param rh
   * @return
   *
   *         private BufferedImage getResizeImage(BufferedImage origine, int w, int h)
   *         {
   *         if (origine == null) return null;
   *         float rw=1, rh=1;
   *         
   *         // Calcul du ratio
   *         if (w == -1) // La r�f�rence est la hauteur
   *         {
   *         rh = ((float)origine.getHeight() / h);
   *         rw = rh;
   *         }
   *         else
   *         if (h == -1) // La r�f�rence est la largeur
   *         {
   *         rw = ((float)origine.getWidth() / w);
   *         rh = rw;
   *         }
   *         else
   *         {
   *         rw = ((float)origine.getWidth() / w);
   *         rh = ((float)origine.getHeight() / h);
   *         }
   * 
   *         // Transformation de l'image
   *         AffineTransform tx = new AffineTransform();
   *         tx.scale(rw, rh);
   *         AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
   *         BufferedImage bufFinal = new BufferedImage( (int) (origine.getWidth() * rw), (int) (origine.getHeight() * rh),
   *         origine.getType());
   * 
   *         return op.filter(origine, bufFinal);
   *         }
   */
  
  /**
   * Retourne une icone redimensionn�e
   * @param ii
   * @param ratio
   * @return
   */
  public ImageIcon getResizeIcon(ImageIcon origine, int w, int h) {
    if (origine == null)
      return null;
    float rw = 1, rh = 1;
    
    // Calcul du ratio
    if (w == -1) // La r�f�rence est la hauteur
    {
      rh = ((float) origine.getIconHeight() / h);
      rw = rh;
    }
    else if (h == -1) // La r�f�rence est la largeur
    {
      rw = ((float) origine.getIconWidth() / w);
      rh = rw;
    }
    else {
      rw = ((float) origine.getIconWidth() / w);
      rh = ((float) origine.getIconHeight() / h);
    }
    
    // Calcul des nouvelles dimensions
    w = (int) (origine.getIconWidth() / rw);
    h = (int) (origine.getIconHeight() / rh);
    
    BufferedImage buf = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    // On dessine sur le Graphics de l'image bufferis�e.
    Graphics2D g = buf.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.drawImage(origine.getImage(), 0, 0, w, h, null);
    g.dispose();
    return new ImageIcon(buf);
  }
  
  /**
   * @return the ligne
   */
  public DescriptionImage getDescription() {
    DescriptionImage dimage = new DescriptionImage();
    dimage.setHauteur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getHeight()));
    dimage.setLargeur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getWidth()));
    dimage.setXPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().x));
    dimage.setYPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().y));
    dimage.setCheminImage(cheminImage);
    dimage.setCondition(condition);
    
    return dimage;
  }
  
  /**
   * @param keepRatio the keepRatio to set
   */
  public void setKeepRatio(boolean keepRatio) {
    this.keepRatio = keepRatio;
    // Mise � jour des attributs
    // if (pageEditor != null)
    // pageEditor.getPropertyObject().getPanelProperty(this);
  }
  
  /**
   * @return the keepRatio
   */
  public boolean isKeepRatio() {
    return keepRatio;
  }
  
  /**
   * @return the cheminImage
   */
  public String getCheminImage() {
    return cheminImage;
  }
  
  /**
   * @return the imageOriginale
   */
  public ImageIcon getImageOriginale() {
    return imageOriginale;
  }
  
  /**
   * Chargement de l'image
   * Attention certaines images jpg peuvent ne pas �tre afficher
   * Faudrait intercepter l'erreur: sun.awt.image.ImageFormatException: Unsupported color conversion request
   * @param imageOriginale the imageOriginale to set
   */
  public void setImageOriginale(String chimage) {
    if (chimage == null) {
      imageOriginale = null;
      cheminImage = null;
    }
    else {
      if (avecDossier)
        cheminImage = chimage;
      else
        cheminImage = new File(chimage).getName();
      if (pageEditor != null)
        imageOriginale = new ImageIcon(pageEditor.getDossierTravail() + File.separator + cheminImage);
      setIcon(getResizeIcon(imageOriginale, getWidth(), getHeight()));
    }
    
  }
  
  /**
   * @param imageOriginale the imageOriginale to set
   */
  public void setImageOriginale(URL chimage) {
    setImageOriginale(chimage.getFile());
  }
  
  /**
   * @return the avecDossier
   */
  public boolean isAvecDossier() {
    return avecDossier;
  }
  
  /**
   * @param avecDossier the avecDossier to set
   */
  public void setAvecDossier(boolean avecDossier) {
    this.avecDossier = avecDossier;
  }
  
  /**
   * Action lors du clic souris sur label (bouton lach�)
   * @param e
   */
  protected void labelMouseReleased(MouseEvent e) {
    // Gestion du redimensionnement de l'image
    if (redimensionne) {
      if (imageOriginale != null)
        if (keepRatio) {
          switch (getCursor().getType()) {
            case Cursor.N_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, -1, getHeight()));
              break;
            case Cursor.S_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, -1, getHeight()));
              break;
            case Cursor.E_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, getWidth(), -1));
              break;
            case Cursor.W_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, getWidth(), -1));
              break;
          }
        }
        else
          setIcon(getResizeIcon(imageOriginale, getWidth(), getHeight()));
      // setSize(getIcon().getIconWidth(), getIcon().getIconHeight());
    }
    
    // On effectue les op�rations d'origine
    super.labelMouseReleased(e);
    
    // On met � jour les propri�t�s de l'objet dans le panel des propri�t�s
    // Mise � jour des attributs
    if (pageEditor != null)
      pageEditor.getPropertyObject().getPanelProperty(this);
  }
  
  /**
   * Action lors du clic souris sur label (bouton lach�)
   * @param e
   */
  protected void labelMouseDragged(MouseEvent e) {
    super.labelMouseDragged(e);
    // On met � jour les propri�t�s de l'objet dans le panel des propri�t�s
    // Mise � jour des attributs
    if ((clic_gauche) && (pageEditor != null))
      pageEditor.getPropertyObject().getPanelProperty(this);
  }
}
