/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;
import org.apache.batik.swing.gvt.GVTTreeRendererListener;
import org.apache.batik.swing.svg.SVGDocumentLoaderEvent;
import org.apache.batik.swing.svg.SVGDocumentLoaderListener;

import description.DescriptionPage;
import rad.PageEditor.GfxPageEditor;
import rad.outils.CalculDPI;

//=================================================================================================
//==> Description de la zone de cr�ation
//=================================================================================================
public class GfxPage extends JSVGCanvas {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  public static final int MODE_DEPLACEMENT = 0;
  public static final int MODE_LABEL = 1;
  public static final int MODE_CODEBARRE = 2;
  public static final int MODE_IMAGE = 3;
  
  // Variables
  // private File dernierPath=null;
  private String dossierTravail = "";
  private JLabel labelPositionSouris = null;
  private GfxPageEditor pageEditor = null;
  private GfxEtiquette etiquette = null;
  private int dpi_aff = 72;
  private int dpi_img = 72;
  
  private File cheminImageFond = null;
  // private Image imagefond=null;
  private boolean portrait = true;
  private float largeurPage = 21f, hauteurPage = 29.7f; // Dimension en cm
  private float margeXPage = 0, margeYPage = 0; // Dimension en cm
  private float old_margeXPage = 0, old_margeYPage = 0; // Dimension en cm
  private float margeXEtiquette = 0, margeYEtiquette = 0; // Dimension en cm
  private int nbrEtiquetteSurLargeur = 1, nbrEtiquetteSurHauteur = 1;
  private boolean lienSpool = true;
  private boolean amodifier = false;
  private boolean svgok = false;
  
  /**
   * Constructeur
   */
  public GfxPage(final GfxPageEditor planTravail) {
    initComponents();
    this.pageEditor = planTravail;
    if (planTravail != null)
      setDossierTravail(planTravail.getDossierTravail());
    addSVGDocumentLoaderListener(new SVGDocumentLoaderListener() {
      public void documentLoadingStarted(SVGDocumentLoaderEvent arg0) {
      }
      
      public void documentLoadingFailed(SVGDocumentLoaderEvent arg0) {
      }
      
      public void documentLoadingCompleted(SVGDocumentLoaderEvent arg0) {
        svgok = true;
      }
      
      public void documentLoadingCancelled(SVGDocumentLoaderEvent arg0) {
      }
    });
    addGVTTreeRendererListener(new GVTTreeRendererListener() {
      
      public void gvtRenderingStarted(GVTTreeRendererEvent arg0) {
      }
      
      public void gvtRenderingPrepare(GVTTreeRendererEvent arg0) {
      }
      
      public void gvtRenderingFailed(GVTTreeRendererEvent arg0) {
      }
      
      public void gvtRenderingCompleted(GVTTreeRendererEvent arg0) {
        translationFondImage();
      }
      
      public void gvtRenderingCancelled(GVTTreeRendererEvent arg0) {
      }
    });
    
  }
  
  /**
   * Constructeur
   */
  public GfxPage(DescriptionPage dpage, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    this(planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    setDossierTravail(dossierTravail);
    // System.out.println("-dpi-> " + getDpiAff() + " " + getDpiImg());
    setPortrait(dpage.isPortrait());
    setLargeurPage(dpage.getLargeur());
    setHauteurPage(dpage.getHauteur());
    setMargeXPage(dpage.getMarge_x());
    setMargeYPage(dpage.getMarge_y());
    setMargeXEtiquette(dpage.getDetiquette().getMarge_x());
    setMargeYEtiquette(dpage.getDetiquette().getMarge_y());
    setNbrEtiquetteSurHauteur(dpage.getNbrLigne());
    setNbrEtiquetteSurLargeur(dpage.getNbrColonne());
    setLienSpool(dpage.isLienSpool());
    // etiquette = new GfxEtiquette(dpage.getDetiquette(), getDpiAff(), getDpiImg(), planTravail);
    // etiquette.setBounds(CalculDPI.getCmtoPx(getDpiAff(), getDpiImg(), getMargeXPage()), CalculDPI.getCmtoPx(getDpiAff(), getDpiImg(),
    // getMargeYPage()), CalculDPI.getCmtoPx(getDpiAff(), getDpiImg(), etiquette.getLargeurEtiquette()), CalculDPI.getCmtoPx(getDpiAff(),
    // getDpiImg(), etiquette.getHauteurEtiquette()));
    etiquette = new GfxEtiquette(dpage.getDetiquette(), dpi_aff, dpi_img, planTravail);
    etiquette.setBounds(CalculDPI.getCmtoPx(dpi_aff, dpi_img, getMargeXPage()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, getMargeYPage()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, etiquette.getLargeurEtiquette()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, etiquette.getHauteurEtiquette()));
    setCheminImageFond(dpage.getImage());
    planTravail.setListeCondition(dpage.getListeDescriptionCondition());
    planTravail.setListeVariable(dpage.getListeDescriptionVariable());
  }
  
  /**
   * Initialise les infos pour le Rad
   * @param rad
   * @param pageEditor
   * @param zoneCreation
   */
  public void setInfos() {
    
    // Ajout de l'�tiquette
    if (etiquette == null)
      etiquette = new GfxEtiquette(pageEditor);
    etiquette.setInfos(pageEditor, this);
    add(etiquette);
    
    initPreparation(true);
  }
  
  /**
   * Initialise la cr�ation d'une nouvelle page
   */
  public void initPreparation(boolean portrait) {
    float tmp = 0;
    
    if (!this.portrait == portrait) {
      // Page
      tmp = hauteurPage;
      hauteurPage = largeurPage;
      largeurPage = tmp;
      // Etiquette
      tmp = etiquette.getHauteurEtiquette();
      etiquette.setHauteurEtiquette(etiquette.getLargeurEtiquette());
      etiquette.setLargeurEtiquette(tmp);
    }
    this.portrait = portrait;
    
    Dimension d = new Dimension(CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurPage));
    setSize(d);
    setMinimumSize(d);
    setMaximumSize(d);
    setPreferredSize(d);
    
    pageEditor.resizeZoneCreation();
    etiquette.initPreparation();
    
    // Couleur de fond
    setBackground(Color.white);
  }
  
  /**
   * Met � jour la position de la souris dans le texte d'un label
   * @param x
   * @param y
   */
  public void setPositionSouris(int x, int y) {
    if (labelPositionSouris == null)
      return;
    labelPositionSouris.setText("x:" + x + " px, y:" + y + " px (" + CalculDPI.getPxtoCm(dpi_aff, dpi_img, y) + " cm, "
        + CalculDPI.getPxtoCm(dpi_aff, dpi_img, x) + " cm)");
  }
  
  /**
   * Initialise le label qui recevra la position de la souris
   * @param labelPositionSouris
   */
  public void setLabelPositionSouris(JLabel labelPositionSouris) {
    this.labelPositionSouris = labelPositionSouris;
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPlanTravail() {
    return pageEditor;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPlanTravail(GfxPageEditor planTravail) {
    this.pageEditor = planTravail;
  }
  
  /**
   * @param dossierTravail the dossierTravail to set
   */
  public void setDossierTravail(String dossierTravail) {
    // System.out.println("-zonecreation-> " + dossierTravail);
    this.dossierTravail = dossierTravail;
  }
  
  /**
   * @return the dossierTravail
   */
  public String getDossierTravail() {
    return dossierTravail;
  }
  
  /**
   * @param cheminImageFond the cheminImageFond to set
   */
  public void setCheminImageFond(final String cheminImageFond) {
    if (cheminImageFond != null) {
      this.cheminImageFond = new File(getDossierTravail() + File.separator + cheminImageFond);
      setURI(this.cheminImageFond.toURI().toString());
      amodifier = true;
    }
    else {
      this.cheminImageFond = null;
      setURI(null);
      // On remet � z�ro pour que le prochain fond de page soit bien en place
      old_margeXPage = 0;
      old_margeYPage = 0;
    }
    /*
    // On met � jour le dpi en fonction de l'image
    if ((cheminImageFond != null) && (cheminImageFond.endsWith("svg")))
    {
    	setDpiAff(90);
    	setDpiImg(90);
    	pageEditor.setDpiImage(90);
    	pageEditor.setDpiAffichage(90);
    }		
    else
    {
    	setDpiAff(72);
    	setDpiImg(72);
    	pageEditor.setDpiImage(72);
    	pageEditor.setDpiAffichage(72);
    }*/
    
    pageEditor.setLibelleImage(cheminImageFond);
    pageEditor.resizeZoneCreation();
  }
  
  /**
   * @return the cheminImageFond
   */
  public File getCheminImageFond() {
    return cheminImageFond;
  }
  
  /**
   * Retourne l'image de fond de la page
   * @return
   *
   *         public Image getFondPage()
   *         {
   *         if (cheminImageFond == null) return null;
   *         
   *         // On affiche le fond s'il y en a un
   *         if (cheminImageFond.exists())
   *         if (cheminImageFond.getName().toLowerCase().endsWith(".svg"))
   *         {
   *         try
   *         {
   *         URL urlFond = cheminImageFond.toURI().toURL();
   *         //System.out.println("-dpi->" + dpi_img);
   *         return SVGtoBitmap.getImageFromSvg(urlFond, CalculDPI.DPI_ECRAN);
   *         }
   *         catch(Exception e){}
   *         }
   *         else
   *         return new ImageIcon(cheminImageFond.getAbsolutePath()).getImage();
   *         
   *         return null;
   *         }
   */
  
  /**
   * Dessine dans le panel
   *
   * protected void paintComponent(Graphics g)
   * {
   * super.paintComponent(g);
   * 
   * // On efface tout
   * //g.setColor(Color.white);
   * //g.fillRect(0, 0, CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurPage));
   * 
   * // On ajoute l'image
   * if (imagefond != null)
   * g.drawImage(imagefond, CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeXPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeYPage), null);
   * //System.out.println("-page-> aff:" + dpi_aff + " img:" + dpi_img + " l:" + getWidth() + " h:" + getHeight());
   * dessineContourPage(g);
   * }
   */
  /**
   * Dessine dans le panel
   *
   * public void paintComponent(Graphics g)
   * {
   * super.paintComponent(g);
   * translationFondImage();
   * dessineContourPage(g);
   * }
   */
  
  private void translationFondImage() {
    if (!amodifier)
      return;
    float dx = this.margeXPage - old_margeXPage;
    float dy = this.margeYPage - old_margeYPage;
    System.out.println("-translationFondImage-> " + dx + " " + dy);
    old_margeXPage = this.margeXPage;
    old_margeYPage = this.margeYPage;
    
    AffineTransform t = getRenderingTransform();
    t.translate(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dx), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dy));
    setRenderingTransform(t);
    
    // dessineContourPage(getGraphics());
    amodifier = false;
  }
  
  /**
   * Dessine le contour de la page (les �l�ments fig�s)
   * @param parent
   */
  public void dessineContourPage(Graphics g) {
    // Dimensionnement de la zone de cr�ation
    Graphics2D g2d = (Graphics2D) g;
    
    // On dessine les marges haute et gauche de la page
    g2d.setColor(new Color(223, 223, 223));
    g2d.fillRect(0, 0, CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeXPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurPage));
    g2d.fillRect(0, 0, CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeYPage));
    
    g2d.setColor(Color.black);
    // On dessine le contour de la page enti�re
    g2d.drawRect(0, 0, CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurPage));
    // On dessine le contour de la page r�duite
    g2d.drawRect(CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeXPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeYPage),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurPage - margeXPage), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurPage - margeYPage));
  }
  
  /**
   * @return the largeurPage
   */
  public float getLargeurPage() {
    return largeurPage;
  }
  
  /**
   * @param largeurPage the largeurPage to set
   */
  public void setLargeurPage(float largeurPage) {
    this.largeurPage = largeurPage;
  }
  
  /**
   * @return the hauteurPage
   */
  public float getHauteurPage() {
    return hauteurPage;
  }
  
  /**
   * @param hauteurPage the hauteurPage to set
   */
  public void setHauteurPage(float hauteurPage) {
    this.hauteurPage = hauteurPage;
  }
  
  /**
   * @return the margeXPage
   */
  public float getMargeXPage() {
    return margeXPage;
  }
  
  /**
   * @param margeXPage the margeXPage to set
   */
  public void setMargeXPage(float margeXPage) {
    old_margeXPage = this.margeXPage;
    this.margeXPage = margeXPage;
    amodifier = svgok;
    translationFondImage();
  }
  
  /**
   * @return the margeYPage
   */
  public float getMargeYPage() {
    return margeYPage;
  }
  
  /**
   * @param margeYPage the margeYPage to set
   */
  public void setMargeYPage(float margeYPage) {
    old_margeYPage = this.margeYPage;
    this.margeYPage = margeYPage;
    amodifier = svgok;
    translationFondImage();
  }
  
  /**
   * @return the nbrEtiquetteSurLargeur
   */
  public int getNbrEtiquetteSurLargeur() {
    return nbrEtiquetteSurLargeur;
  }
  
  /**
   * @param nbrEtiquetteSurLargeur the nbrEtiquetteSurLargeur to set
   */
  public void setNbrEtiquetteSurLargeur(int nbrEtiquetteSurLargeur) {
    this.nbrEtiquetteSurLargeur = nbrEtiquetteSurLargeur;
  }
  
  /**
   * @return the nbrEtiquetteSurHauteur
   */
  public int getNbrEtiquetteSurHauteur() {
    return nbrEtiquetteSurHauteur;
  }
  
  /**
   * @param nbrEtiquetteSurHauteur the nbrEtiquetteSurHauteur to set
   */
  public void setNbrEtiquetteSurHauteur(int nbrEtiquetteSurHauteur) {
    this.nbrEtiquetteSurHauteur = nbrEtiquetteSurHauteur;
  }
  
  public void setPortrait(boolean portrait) {
    this.portrait = portrait;
  }
  
  public boolean isPortrait() {
    return portrait;
  }
  
  /**
   * Retourne la description de la page courante
   * @return
   */
  public DescriptionPage getDescriptionPage() {
    // Donn�es pour la page
    DescriptionPage pagecourante = new DescriptionPage();
    pagecourante.setPortrait(portrait);
    pagecourante.setMarge_x(margeXPage);
    pagecourante.setMarge_y(margeYPage);
    pagecourante.setLargeur(largeurPage);
    pagecourante.setHauteur(hauteurPage);
    pagecourante.setNbrColonne(nbrEtiquetteSurLargeur);
    pagecourante.setNbrLigne(nbrEtiquetteSurHauteur);
    pagecourante.setImage(cheminImageFond == null ? null : cheminImageFond.getName());
    pagecourante.setDetiquette(etiquette.getDescriptionEtiquette());
    pagecourante.getDetiquette().setMarge_x(margeXEtiquette);
    pagecourante.getDetiquette().setMarge_y(margeYEtiquette);
    pagecourante.setListeDescriptionCondition(pageEditor.getListeCondition());
    pagecourante.setLienSpool(lienSpool);
    pagecourante.setListeDescriptionVariable(pageEditor.getListeVariable());
    
    return pagecourante;
  }
  
  /**
   * @return the dpi
   */
  public int getDpiAff() {
    return dpi_aff;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiAff(int dpi) {
    this.dpi_aff = dpi;
    if (etiquette != null) {
      etiquette.setDpiAff(dpi);
      initPreparation(portrait);
      repaint();
    }
  }
  
  /**
   * @return the dpi
   */
  public int getDpiImg() {
    return dpi_img;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiImg(int dpi) {
    this.dpi_img = dpi;
    if (etiquette != null) {
      etiquette.setDpiImg(dpi);
      initPreparation(portrait);
      repaint();
    }
  }
  
  /**
   * @return the etiquette
   */
  public GfxEtiquette getEtiquette() {
    return etiquette;
  }
  
  /**
   * @param etiquette the etiquette to set
   */
  public void setEtiquette(GfxEtiquette etiquette) {
    this.etiquette = etiquette;
  }
  
  /**
   * @return the margeXEtiquette
   */
  public float getMargeXEtiquette() {
    return margeXEtiquette;
  }
  
  /**
   * @param margeXEtiquette the margeXEtiquette to set
   */
  public void setMargeXEtiquette(float margeXEtiquette) {
    this.margeXEtiquette = margeXEtiquette;
  }
  
  /**
   * @return the margeYEtiquette
   */
  public float getMargeYEtiquette() {
    return margeYEtiquette;
  }
  
  /**
   * @param margeYEtiquette the margeYEtiquette to set
   */
  public void setMargeYEtiquette(float margeYEtiquette) {
    this.margeYEtiquette = margeYEtiquette;
  }
  
  /**
   * @param lienSpool the lienSpool to set
   */
  public void setLienSpool(boolean lienSpool) {
    this.lienSpool = lienSpool;
  }
  
  /**
   * @return the lienSpool
   */
  public boolean isLienSpool() {
    return lienSpool;
  }
  
  private void thisMouseMoved(MouseEvent e) {
    setPositionSouris(e.getX(), e.getY());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    
    // ======== this ========
    setBorder(LineBorder.createBlackLineBorder());
    setBackground(Color.gray);
    setName("this");
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        thisMouseMoved(e);
      }
    });
    setLayout(null);
    
    { // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
