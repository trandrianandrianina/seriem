/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.composants;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;

import rad.PageEditor.GfxPageEditor;
import rad.outils.CalculDPI;

//=================================================================================================
//==> Panel permettant de changer les propri�t�s d'une Image
//=================================================================================================
public class GfxPropertyImage extends JPanel // implements iGfxPropertyObject
{
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private GfxObject object = null;
  private GfxPageEditor pageEditor = null;
  private int nbr2condition = 0;
  private boolean modeMultiSelection = false;
  
  /**
   * Constructeur
   */
  public GfxPropertyImage(final GfxPageEditor pageEditor) {
    initComponents();
    setPageEditor(pageEditor);
    // initGestionLostFocus(sp_Largeur);
    // initGestionLostFocus(sp_Hauteur);
    // initGestionLostFocus(sp_X);
    // initGestionLostFocus(sp_Y);
  }
  
  /**
   * Initialise l'�diteur de page
   * @param pageEditor
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label
   * @param label
   */
  public void setObject2PanelAttributs(GfxObject image) {
    object = image;
    int dpi_aff = image.getDpiAff();
    int dpi_img = image.getDpiImg();
    
    modeMultiSelection = false;
    
    chk_CheminComplet.setSelected(((GfxImage) image).isAvecDossier());
    tf_Image.setText(((GfxImage) image).getCheminImage());
    sp_Largeur.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, image.getWidth()));
    l_LargeurPx.setText(image.getWidth() + " px");
    sp_Hauteur.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, image.getHeight()));
    l_HauteurPx.setText(image.getHeight() + " px");
    sp_X.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, image.getLocation().x));
    l_XPx.setText(image.getLocation().x + " px");
    sp_Y.setValue(CalculDPI.getPxtoCm(dpi_aff, dpi_img, image.getLocation().y));
    l_YPx.setText(image.getLocation().y + " px");
    chk_KeepRatio.setSelected(((GfxImage) image).isKeepRatio());
    
    // Les conditions
    chargeComboCondition(object.getCondition());
    
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * @param label
   */
  public void addObject2PanelAttributs(GfxObject obj) {
    object = (GfxImage) obj;
    int dpi_aff = obj.getDpiAff();
    int dpi_img = obj.getDpiImg();
    
    modeMultiSelection = true;
    addObject2PanelAttributs_coordonnees(obj, dpi_aff, dpi_img);
    // Les conditions
    chargeComboCondition(object.getCondition());
  }
  
  /**
   * Met � jour le panel des attributs � partir d'un label (multi-s�lection)
   * Pour les coordonn�es / dimensions
   * @param label
   * @param dpi_aff
   * @param dpi_img
   */
  private void addObject2PanelAttributs_coordonnees(GfxObject image, int dpi_aff, int dpi_img) {
    // Les coordonn�es, dimension
    if (((Float) sp_Largeur.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, dpi_img, object.getWidth()))
      sp_Largeur.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_Hauteur.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getHeight()))
      sp_Hauteur.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_X.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().x))
      sp_X.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
    if (((Float) sp_Y.getValue()).floatValue() != CalculDPI.getPxtoCm(dpi_aff, object.getDpiImg(), object.getLocation().y))
      sp_Y.setValue(GfxPropertyObject.VALEUR_DIFF_FLOAT);
  }
  
  /**
   * Met � jour le label � partir des attributs saisis dans le panel
   */
  private void setPanel2LabelAttributs() {
    int dpi_aff = object.getDpiAff();
    int dpi_img = object.getDpiImg();
    
    // float largeur = ((Float)sp_Largeur.getValue()).floatValue();
    // float hauteur = ((Float)sp_Hauteur.getValue()).floatValue();
    
    // On met � jour
    ((GfxImage) object).setAvecDossier(chk_CheminComplet.isSelected());
    ((GfxImage) object).setImageOriginale(tf_Image.getText());
    object.setLocation(CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_X.getValue()).floatValue()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Y.getValue()).floatValue()));
    object.setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Largeur.getValue()).floatValue()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Hauteur.getValue()).floatValue()));
    l_XPx.setText(object.getLocation().x + " px");
    l_YPx.setText(object.getLocation().y + " px");
    l_LargeurPx.setText(object.getWidth() + " px");
    l_HauteurPx.setText(object.getHeight() + " px");
    ((GfxImage) object).setKeepRatio(chk_KeepRatio.isSelected());
    if (!cb_Condition.getSelectedItem().equals(GfxObject.PAS_DE_COND))
      object.setCondition((String) cb_Condition.getSelectedItem());
    else
      object.setCondition(null);
    
  }
  
  /**
   * Met � jour tous les labels � partir des attributs saisis dans les attributs
   */
  private void setPanel2AllLabelAttributs() {
    int dpi_aff = object.getDpiAff();
    int dpi_img = object.getDpiImg();
    int a = 0, b = 0;
    
    GfxEtiquette parent = (GfxEtiquette) object.getParent();
    // System.out.println("-setPanel2AllLabelAttributs-> " + modeMultiSelection);
    // System.out.println("-setPanel2AllLabelAttributs-> " + parent.isKey_ctrl());
    // Tant que la touche control est enfonc�e on ne fait aucune modif
    if (parent.isKey_ctrl())
      return;
    
    ArrayList<GfxObject> listeSelectionObject = parent.getSelectionGfxObjet();
    
    // On met � jour
    for (int i = listeSelectionObject.size(); --i >= 0;) {
      // Position
      if (((Float) sp_X.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        a = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_X.getValue()).floatValue());
      else
        a = listeSelectionObject.get(i).getLocation().x;
      if (((Float) sp_Y.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        b = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Y.getValue()).floatValue());
      else
        b = listeSelectionObject.get(i).getLocation().y;
      listeSelectionObject.get(i).setLocation(a, b);
      // Taille
      if (((Float) sp_Largeur.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        a = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Largeur.getValue()).floatValue());
      else
        a = listeSelectionObject.get(i).getWidth();
      if (((Float) sp_Hauteur.getValue()).floatValue() != GfxPropertyObject.VALEUR_DIFF_FLOAT)
        b = CalculDPI.getCmtoPx(dpi_aff, dpi_img, ((Float) sp_Hauteur.getValue()).floatValue());
      else
        b = listeSelectionObject.get(i).getHeight();
      listeSelectionObject.get(i).setSize(a, b);
      
      // if (!cb_Condition.getSelectedItem().equals(GfxObject.PAS_DE_COND))
      // listeSelectionObject.get(i).setCondition((String)cb_Condition.getSelectedItem());
    }
  }
  
  /**
   * Gestion de la perte du focus sur un JSpinner
   * @param spinner
   *
   *          private void initGestionLostFocus(final JSpinner spinner)
   *          {
   *          ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().addFocusListener(new FocusAdapter()
   *          {
   *          public void focusLost(FocusEvent evt)
   *          {
   *          try
   *          {
   *          spinner.commitEdit();
   *          }
   *          catch (ParseException e) {}
   *          setPanel2LabelAttributs();
   *          }
   *          });
   * 
   *          }
   */
  
  /**
   * Charge la combo des conditions
   */
  private void chargeComboCondition(String cond) {
    nbr2condition = pageEditor.getListeCondition().size();
    String[] nomCondition = new String[pageEditor.getListeCondition().size() + 1];
    nomCondition[0] = GfxObject.PAS_DE_COND;
    for (int i = 0; i < nbr2condition; i++)
      nomCondition[i + 1] = pageEditor.getListeCondition().get(i).getNom();
    cb_Condition.setModel(new DefaultComboBoxModel(nomCondition));
    if (cond != null)
      cb_Condition.setSelectedItem(cond);
    
  }
  
  // Gestion des �v�nements -----------------------------------------------------------------------------------------------------
  
  private void chk_KeepRatioActionPerformed(ActionEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void sp_LargeurStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setSize(CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Largeur.getValue()).floatValue()),
            object.getHeight());
        l_LargeurPx.setText(object.getWidth() + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_HauteurStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setSize(object.getWidth(),
            CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Hauteur.getValue()).floatValue()));
        l_HauteurPx.setText(object.getHeight() + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_XStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setLocation(CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_X.getValue()).floatValue()),
            object.getLocation().y);
        l_XPx.setText(object.getLocation().x + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void sp_YStateChanged(ChangeEvent e) {
    if (!modeMultiSelection) {
      if (object != null) {
        object.setLocation(object.getLocation().x,
            CalculDPI.getCmtoPx(object.getDpiAff(), object.getDpiImg(), ((Float) sp_Y.getValue()).floatValue()));
        l_YPx.setText(object.getLocation().y + " px");
      }
    }
    else
      setPanel2AllLabelAttributs();
  }
  
  private void cb_ConditionFocusGained(FocusEvent e) {
    if (!modeMultiSelection)
      if (nbr2condition != pageEditor.getListeCondition().size())
        chargeComboCondition(null);
  }
  
  private void cb_ConditionFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void tf_ImageFocusLost(FocusEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  private void chk_CheminCompletActionPerformed(ActionEvent e) {
    if (!modeMultiSelection)
      setPanel2LabelAttributs();
  }
  
  // -----------------------------------------------------------------------------------------------------------------------------
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_Image = new JLabel();
    tf_Image = new JTextField();
    chk_CheminComplet = new JCheckBox();
    label2 = new JLabel();
    sp_Largeur = new JSpinner();
    l_LargeurPx = new JLabel();
    label3 = new JLabel();
    sp_Hauteur = new JSpinner();
    l_HauteurPx = new JLabel();
    label4 = new JLabel();
    sp_X = new JSpinner();
    l_XPx = new JLabel();
    label5 = new JLabel();
    sp_Y = new JSpinner();
    l_YPx = new JLabel();
    chk_KeepRatio = new JCheckBox();
    l_Condition = new JLabel();
    cb_Condition = new JComboBox();
    CellConstraints cc = new CellConstraints();
    
    // ======== this ========
    setBorder(new TitledBorder("Propri\u00e9t\u00e9s de l'Image"));
    setMinimumSize(new Dimension(250, 450));
    setPreferredSize(new Dimension(250, 450));
    setName("this");
    setLayout(new FormLayout(
        new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
            new ColumnSpec(Sizes.dluX(49)), new ColumnSpec(Sizes.dluX(26)) },
        new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
            FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC }));
    
    // ---- l_Image ----
    l_Image.setText("image");
    l_Image.setName("l_Image");
    add(l_Image, cc.xy(2, 2));
    
    // ---- tf_Image ----
    tf_Image.setName("tf_Image");
    tf_Image.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tf_ImageFocusLost(e);
      }
    });
    add(tf_Image, cc.xywh(4, 2, 2, 1));
    
    // ---- chk_CheminComplet ----
    chk_CheminComplet.setText("Chemin complet");
    chk_CheminComplet.setName("chk_CheminComplet");
    chk_CheminComplet.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        chk_CheminCompletActionPerformed(e);
      }
    });
    add(chk_CheminComplet, cc.xywh(2, 4, 3, 1));
    
    // ---- label2 ----
    label2.setText("Largeur");
    label2.setName("label2");
    add(label2, cc.xy(2, 6));
    
    // ---- sp_Largeur ----
    sp_Largeur.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Largeur.setName("sp_Largeur");
    sp_Largeur.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_LargeurStateChanged(e);
      }
    });
    add(sp_Largeur, cc.xy(4, 6));
    
    // ---- l_LargeurPx ----
    l_LargeurPx.setText(" ? px");
    l_LargeurPx.setName("l_LargeurPx");
    add(l_LargeurPx, cc.xy(5, 6));
    
    // ---- label3 ----
    label3.setText("Hauteur");
    label3.setName("label3");
    add(label3, cc.xy(2, 8));
    
    // ---- sp_Hauteur ----
    sp_Hauteur.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Hauteur.setName("sp_Hauteur");
    sp_Hauteur.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_HauteurStateChanged(e);
      }
    });
    add(sp_Hauteur, cc.xy(4, 8));
    
    // ---- l_HauteurPx ----
    l_HauteurPx.setText("? px");
    l_HauteurPx.setName("l_HauteurPx");
    add(l_HauteurPx, cc.xy(5, 8));
    
    // ---- label4 ----
    label4.setText("X");
    label4.setName("label4");
    add(label4, cc.xy(2, 10));
    
    // ---- sp_X ----
    sp_X.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_X.setName("sp_X");
    sp_X.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_XStateChanged(e);
      }
    });
    add(sp_X, cc.xy(4, 10));
    
    // ---- l_XPx ----
    l_XPx.setText("? px");
    l_XPx.setName("l_XPx");
    add(l_XPx, cc.xy(5, 10));
    
    // ---- label5 ----
    label5.setText("Y");
    label5.setName("label5");
    add(label5, cc.xy(2, 12));
    
    // ---- sp_Y ----
    sp_Y.setModel(new SpinnerNumberModel(0.0F, null, null, 0.1F));
    sp_Y.setName("sp_Y");
    sp_Y.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        sp_YStateChanged(e);
      }
    });
    add(sp_Y, cc.xy(4, 12));
    
    // ---- l_YPx ----
    l_YPx.setText("? px");
    l_YPx.setName("l_YPx");
    add(l_YPx, cc.xy(5, 12));
    
    // ---- chk_KeepRatio ----
    chk_KeepRatio.setText("Conserver les proportions");
    chk_KeepRatio.setName("chk_KeepRatio");
    chk_KeepRatio.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        chk_KeepRatioActionPerformed(e);
      }
    });
    add(chk_KeepRatio, cc.xywh(2, 14, 4, 1));
    
    // ---- l_Condition ----
    l_Condition.setText("Condition");
    l_Condition.setName("l_Condition");
    add(l_Condition, cc.xy(2, 16));
    
    // ---- cb_Condition ----
    cb_Condition.setModel(new DefaultComboBoxModel(new String[] { "(aucune)" }));
    cb_Condition.setName("cb_Condition");
    cb_Condition.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        cb_ConditionFocusGained(e);
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        cb_ConditionFocusLost(e);
      }
    });
    add(cb_Condition, cc.xywh(4, 16, 2, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_Image;
  private JTextField tf_Image;
  private JCheckBox chk_CheminComplet;
  private JLabel label2;
  private JSpinner sp_Largeur;
  private JLabel l_LargeurPx;
  private JLabel label3;
  private JSpinner sp_Hauteur;
  private JLabel l_HauteurPx;
  private JLabel label4;
  private JSpinner sp_X;
  private JLabel l_XPx;
  private JLabel label5;
  private JSpinner sp_Y;
  private JLabel l_YPx;
  private JCheckBox chk_KeepRatio;
  private JLabel l_Condition;
  private JComboBox cb_Condition;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
