/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.SpoolEditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import description.DescriptionCondition;

//=================================================================================================
//==> Description du textArea pour la s�lection des Variables
//=================================================================================================
public class SpoolTextAreaCondition extends SpoolTextArea {
  private static final long serialVersionUID = 1L;
  
  // Variables
  private JMenuItem mi_Condition;
  private ArrayList<DescriptionCondition> listeCondition = null; // = new ArrayList<DescriptionCondition>();
  private JLabel texteCondition = null;
  
  /**
   * Constructeur
   */
  public SpoolTextAreaCondition() {
    super();
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        ta_PageConditionMouseMoved(e);
      }
    });
    creationMenuContextuel();
  }
  
  /**
   * Initialise la liste des conditions
   * @return
   */
  public void setListeCondition(ArrayList<DescriptionCondition> listeCondition) {
    this.listeCondition = listeCondition;
  }
  
  /**
   * Cr�ation du menu contextuel
   */
  protected void creationMenuContextuel() {
    super.creationMenuContextuel();
    
    /*		pm_Btd.addPopupMenuListener(new PopupMenuListener() {
    			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    				pm_BtdPopupMenuWillBecomeVisible(e);
    			}
    			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}
    			public void popupMenuCanceled(PopupMenuEvent e) {}
    		});
    */
    mi_Condition = new JMenuItem();
    
    // ---- mi_TrimageDroiteSelection ----
    mi_Condition.setText("Editeur de condition");
    mi_Condition.setName("mi_Condition");
    mi_Condition.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        mi_ConditionActionPerformed(e);
      }
    });
    pm_Btd.add(mi_Condition);
  }
  
  /**
   * @param texteCondition the texteCondition to set
   */
  public void setTexteCondition(JLabel texteCondition) {
    this.texteCondition = texteCondition;
  }
  
  /**
   * @return the texteCondition
   */
  public JLabel getTexteCondition() {
    return texteCondition;
  }
  
  /**
   * Importation de la description pour les conditions
   */
  public void importerDescription() {
    if (listeCondition == null)
      return;
    
    // R�cup�ration des s�lections
    for (int i = listeCondition.size(); --i >= 0;)
      convertDescription2Selection(listeCondition.get(i).getVariable(), listeRectangle);
    repaint();
  }
  
  // ------------------------------------ M�thodes priv�es -------------------
  
  private void ta_PageConditionMouseMoved(MouseEvent e) {
    indiceSelection = findWhoIsSelectionned(e);
    
    if (indiceSelection == -1)
      return;
    String selection = ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
    DescriptionCondition condition = getCondition(selection);
    if ((condition != null) && (texteCondition != null))
      texteCondition.setText(condition.getLibelleCondition());
  }
  
  /**
   * Retourne la condition � partir de son nom
   */
  private DescriptionCondition getCondition(String variable) {
    for (int i = 0; i < listeCondition.size(); i++)
      if (listeCondition.get(i).getVariable().equals(variable))
        return listeCondition.get(i);
    return null;
  }
  
  /**
   * Supprime la condition � partir de son nom
   */
  private void removeCondition(String variable) {
    for (int i = 0; i < listeCondition.size(); i++)
      if (listeCondition.get(i).getVariable().equals(variable)) {
        listeCondition.remove(i);
        return;
      }
  }
  
  // ------------------------------------ Ev�nements -------------------------
  
  protected void mi_SuppimerActionPerformed(ActionEvent e) {
    if (indiceSelection == -1)
      return;
    
    String selection = ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
    removeCondition(selection);
    super.mi_SuppimerActionPerformed(e);
    // System.out.println("--> " + listeCondition.size());
  }
  
  private void mi_ConditionActionPerformed(ActionEvent e) {
    if (indiceSelection == -1)
      return;
    DescriptionCondition condition = null;
    
    // Recherche la condition � partir de la s�lection (variable)
    String selection = ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
    condition = getCondition(selection);
    if (condition == null) {
      condition = new DescriptionCondition();
      listeCondition.add(condition);
      condition.setVariable(selection);
    }
    
    // Affichage de la boite de dialogue
    GfxConditionEditor dialog_cond = new GfxConditionEditor((JFrame) this.getTopLevelAncestor());
    dialog_cond.setCondition(condition);
    dialog_cond.setVisible(true);
    texteCondition.setText(condition.getLibelleCondition());
    
    repaint();
    // System.out.println("++> " + listeCondition.size());
  }
}
