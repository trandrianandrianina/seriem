/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.SpoolEditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import rad.PageEditor.GfxPageEditor;

//=================================================================================================
//==> Description des �v�nements d'une s�lection
//=================================================================================================
public class SelectionModeCaractereListener implements MouseListener, MouseMotionListener, KeyListener {
  // Variables
  public int x = 0, y = 0, w = 0, h = 0, x_ori = 0, y_ori = 0;
  private FontMetrics fm = null;
  private Component composant = null;
  private int largeurLettre = 0, hauteurLettre = 0;
  private ArrayList<SelectionModeCaractere> listeRectangle = new ArrayList<SelectionModeCaractere>();
  private Color couleur = Color.blue;
  private Color couleurSelection = Color.red;
  private boolean selectionBloc = false; // Permet soit de s�lectionner une seule ligne ou un bloc qu'il va d�couper en plusieurs lignes
  private boolean createRectangle = false;
  private GfxPageEditor pageEditor = null;
  
  /**
   * Retourne la colonne du premier caract�re de la chaine s�lectionn�e (indice prem�re colonne: 0)
   * @return
   */
  public int getColonneChaine() {
    return x / largeurLettre;
  }
  
  /**
   * Retourne la longueur de la chaine s�lectionn�e
   * @return
   */
  public int getLongueurChaine() {
    return w / largeurLettre;
  }
  
  /**
   * Retourne la ligne de la chaine s�lectionn�e (indice prem�re ligne: 0)
   * @return
   */
  public int getLigneChaine() {
    return y / hauteurLettre;
  }
  
  /**
   * Retourne le rectangle
   * @return
   */
  public ArrayList<SelectionModeCaractere> getRectangleSelection() {
    return listeRectangle;
  }
  
  /**
   * Initialisation des diff�rentes variables
   * @param arg0
   */
  public void init(Component acomposant) {
    composant = acomposant;
    fm = composant.getFontMetrics(composant.getFont()); // A am�ilorer avec g.getFontMetrics(); (� voir)
    largeurLettre = fm.charWidth('M');
    hauteurLettre = fm.getHeight();
  }
  
  /**
   * @return the couleur
   */
  public Color getCouleur() {
    return couleur;
  }
  
  /**
   * @param couleur the couleur to set
   */
  public void setCouleur(Color couleur) {
    this.couleur = couleur;
    // if (rs != null) rs.setCouleur(couleur);
    if (listeRectangle.size() != 0)
      listeRectangle.get(0).setCouleur(couleur);
  }
  
  /**
   * @return the couleurSelection
   */
  public Color getCouleurSelection() {
    return couleurSelection;
  }
  
  /**
   * @param couleurSelection the couleurSelection to set
   */
  public void setCouleurSelection(Color couleurSelection) {
    this.couleurSelection = couleurSelection;
    // if (rs != null) rs.setCouleurSelection(couleurSelection);
    if (listeRectangle.size() != 0)
      listeRectangle.get(0).setCouleurSelection(couleurSelection);
  }
  
  // ------------------------------------ Ev�nements -------------------------
  
  /**
   * @param selectionBloc the selectionBloc to set
   */
  public void setSelectionBloc(boolean selectionBloc) {
    this.selectionBloc = selectionBloc;
  }
  
  /**
   * @return the selectionBloc
   */
  public boolean isSelectionBloc() {
    return selectionBloc;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  public void mouseClicked(MouseEvent arg0) {
  }
  
  public void mouseEntered(MouseEvent arg0) {
  }
  
  public void mouseExited(MouseEvent arg0) {
  }
  
  public void mousePressed(MouseEvent arg0) {
    listeRectangle.clear();
    if (composant == null)
      init(arg0.getComponent());
    x = (arg0.getX() / largeurLettre) * largeurLettre;
    y = (arg0.getY() / hauteurLettre) * hauteurLettre;
    x_ori = x;
    y_ori = y;
    w = 0;
    h = 0;
  }
  
  public void mouseReleased(MouseEvent arg0) {
    // System.out.println("--> une ligne " + w + " " + h);
    /*
    if ( (w == 0) && (h==0) )
    {
    	listeRectangle.clear();
    	return;
    }*/
    // System.out.println("--> " + h + " " + (h/hauteurLettre));
    
    if (createRectangle)
      if (selectionBloc) {
        // Cr�ation d'une ou plusieurs s�lection (si hauteur > � une ligne)
        int nbrligne = h / hauteurLettre;
        for (int i = 0; i < nbrligne; i++) {
          SelectionModeCaractere rs =
              new SelectionModeCaractere(x, y + (i * hauteurLettre), w, hauteurLettre, largeurLettre, hauteurLettre);
          listeRectangle.add(rs);
          // System.out.println("y:" + (y+i*hauteurLettre) + " " + getLigneChaine());
          rs.setLigne(i + getLigneChaine());
          rs.setColonne(getColonneChaine());
          rs.setLongueur(getLongueurChaine());
          rs.setSelection(true);
          rs.setDimChar(largeurLettre, hauteurLettre);
          rs.setCouleur(couleur);
          rs.setCouleurSelection(couleurSelection);
        }
      }
      else // Pour une seule ligne
      {
        SelectionModeCaractere rs = new SelectionModeCaractere(x, y, w, h, largeurLettre, hauteurLettre);
        listeRectangle.add(rs);
        rs.setLigne(getLigneChaine());
        rs.setColonne(getColonneChaine());
        rs.setLongueur(getLongueurChaine());
        rs.setSelection(true);
        rs.setDimChar(largeurLettre, hauteurLettre);
        rs.setCouleur(couleur);
        rs.setCouleurSelection(couleurSelection);
      }
    // else
    
    // System.out.println("--> NbrRectangle : " + listeRectangle.size() +" " + createRectangle);
    composant.repaint();
    createRectangle = false;
  }
  
  public void mouseDragged(MouseEvent e) {
    createRectangle = true;
    
    // D�placement vers la droite
    if (e.getX() >= x_ori)
      w = ((e.getX() / largeurLettre) * largeurLettre) - x + largeurLettre;
    else // D�placement vers la gauche
    {
      x = (e.getX() / largeurLettre) * largeurLettre;
      w = x_ori - x;
    }
    
    // D�placement vers le bas
    if (e.getY() >= y_ori) {
      if (selectionBloc) // Permet de faire du multiligne
        h = ((e.getY() / hauteurLettre) * hauteurLettre) - y + hauteurLettre;
      else // Permet de faire du monoligne
        h = hauteurLettre;
    }
    else // D�placement vers le haut
    {
      y = (e.getY() / hauteurLettre) * hauteurLettre;
      if (selectionBloc) // Permet de faire du multiligne
        h = y_ori - y + hauteurLettre;
      else // Permet de faire du monoligne
        h = hauteurLettre;
    }
    
    composant.repaint();
  }
  
  public void mouseMoved(MouseEvent arg0) {
  }
  
  public void keyPressed(KeyEvent e) {
    // System.out.println("--> " + listeRectangle.size());
    // Si Shift enfonc� alors on est en mode aggrandissement ou d�placement
    if (e.isShiftDown() && listeRectangle.size() > 0) {
      SelectionModeCaractere rs = listeRectangle.get(listeRectangle.size() - 1);
      // System.out.println("-SelectionModeCaractereListener (keyPressed)-> passage refresh text");
      if (e.isControlDown() || e.isMetaDown()) // Mode d�placemement
      {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT)
          rs.setX(rs.getX() + largeurLettre);
        else if (e.getKeyCode() == KeyEvent.VK_LEFT)
          rs.setX(rs.getX() - largeurLettre);
      }
      else // Mode Aggrandissement
      {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT)
          rs.setW(rs.getW() + largeurLettre);
        else if (e.getKeyCode() == KeyEvent.VK_LEFT)
          rs.setW(rs.getW() - largeurLettre);
        // Mise � jour dans l'�diteur de page
        if (pageEditor != null)
          pageEditor.getGfxPage().getEtiquette().modifieObjet(rs.getSauveLibelle(), rs.getLibelle());
      }
    }
    composant.repaint();
  }
  
  public void keyReleased(KeyEvent e) {
  }
  
  public void keyTyped(KeyEvent e) {
  }
  
}
