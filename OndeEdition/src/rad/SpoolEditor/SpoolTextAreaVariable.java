/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.SpoolEditor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import description.DescriptionVariable;

//=================================================================================================
//==> Description du textArea pour la s�lection des Variables
//=================================================================================================
public class SpoolTextAreaVariable extends SpoolTextArea {
  private static final long serialVersionUID = 1L;
  
  // Variables
  private JMenuItem mi_Variable;
  private ArrayList<DescriptionVariable> listeVariable = null;
  private JLabel texteVariable = null;
  
  /**
   * Constructeur
   */
  public SpoolTextAreaVariable() {
    super();
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        ta_PageVariableMouseMoved(e);
      }
    });
    creationMenuContextuel();
    setCouleurSelection(new Color(0, 255, 0));
  }
  
  /**
   * Initialise la liste des variables
   * @return
   */
  public void setListeVariable(ArrayList<DescriptionVariable> listeVar) {
    this.listeVariable = listeVar;
  }
  
  /**
   * Importation de la description pour les variables
   */
  public void importerDescription() {
    if (listeVariable == null)
      return;
    
    // R�cup�ration des s�lections
    for (int i = listeVariable.size(); --i >= 0;)
      convertDescription2Selection(listeVariable.get(i).getVariable(), listeRectangle);
    repaint();
  }
  
  /**
   * Cr�ation du menu contextuel
   */
  protected void creationMenuContextuel() {
    super.creationMenuContextuel();
    
    /*		pm_Btd.addPopupMenuListener(new PopupMenuListener() {
    			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    				pm_BtdPopupMenuWillBecomeVisible(e);
    			}
    			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}
    			public void popupMenuCanceled(PopupMenuEvent e) {}
    		});
    */
    mi_Variable = new JMenuItem();
    
    // ---- mi_TrimageDroiteSelection ----
    mi_Variable.setText("Editeur de variable");
    mi_Variable.setName("mi_Variable");
    mi_Variable.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        mi_VariableActionPerformed(e);
      }
    });
    pm_Btd.add(mi_Variable);
  }
  
  /**
   * @param texteVariable the texteVariable to set
   */
  public void setTexteVariable(JLabel texteVariable) {
    this.texteVariable = texteVariable;
  }
  
  /**
   * @return the texteVariable
   */
  public JLabel getTexteVariable() {
    return texteVariable;
  }
  
  // ------------------------------------ M�thodes priv�es -------------------
  
  private void ta_PageVariableMouseMoved(MouseEvent e) {
    indiceSelection = findWhoIsSelectionned(e);
    
    if (indiceSelection == -1)
      return;
    String selection = ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
    DescriptionVariable variable = getVariable(selection);
    if ((variable != null) && (texteVariable != null))
      texteVariable.setText(variable.getNom());
  }
  
  /**
   * Retourne la variable � partir de son nom
   */
  private DescriptionVariable getVariable(String variable) {
    for (int i = 0; i < listeVariable.size(); i++)
      if (listeVariable.get(i).getVariable().equals(variable))
        return listeVariable.get(i);
    return null;
  }
  
  /**
   * Supprime la variable � partir de son nom
   */
  private void removeVariable(String variable) {
    for (int i = 0; i < listeVariable.size(); i++)
      if (listeVariable.get(i).getVariable().equals(variable)) {
        listeVariable.remove(i);
        return;
      }
  }
  
  // ------------------------------------ Ev�nements -------------------------
  
  protected void mi_SuppimerActionPerformed(ActionEvent e) {
    if (indiceSelection == -1)
      return;
    
    String selection = ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
    removeVariable(selection);
    super.mi_SuppimerActionPerformed(e);
    // System.out.println("--> " + listeVariable.size());
  }
  
  private void mi_VariableActionPerformed(ActionEvent e) {
    if (indiceSelection == -1)
      return;
    DescriptionVariable variable = null;
    
    // Recherche la condition � partir de la s�lection (variable)
    String selection = ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
    variable = getVariable(selection);
    if (variable == null) {
      variable = new DescriptionVariable();
      listeVariable.add(variable);
      variable.setVariable(selection);
    }
    
    // Affichage de la boite de dialogue
    GfxVariableEditor dialog_var = new GfxVariableEditor((JFrame) this.getTopLevelAncestor());
    dialog_var.setVariable(variable);
    dialog_var.setVisible(true);
    // texteCondition.setText(condition.getLibelleCondition());
    
    repaint();
    // System.out.println("++> " + listeVariable.size());
  }
}
