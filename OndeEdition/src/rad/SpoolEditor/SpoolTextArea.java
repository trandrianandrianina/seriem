/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.SpoolEditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;

import base.clipboard.Clipboard;
import rad.PageEditor.GfxPageEditor;

//=================================================================================================
//==> Description du textArea pour la s�lection des zones
//=================================================================================================
public class SpoolTextArea extends JTextArea {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  protected SelectionModeCaractereListener smcl = null;
  protected ArrayList<SelectionModeCaractere> listeRectangle = new ArrayList<SelectionModeCaractere>();
  protected boolean selectionEncours = false;
  protected int indiceSelection = -1;
  private JComponent libelle = null;
  private JComponent positionCurseur = null;
  
  protected Point positionSouris = null;
  protected GfxPageEditor pageEditor = null;
  
  protected JPopupMenu pm_Btd;
  private JMenuItem mi_Suppimer;
  protected int nbrlignes = 0, nbrcolonnes = 0;
  private Color couleur = Color.blue;
  private Color couleurSelection = Color.red;
  private boolean creationAutoLabel = false;
  
  /**
   * Constructeur
   */
  public SpoolTextArea() {
    init();
  }
  
  /**
   * Initialise le composant
   */
  private void init() {
    // Personnalisation du composant
    setEditable(false);
    setSelectionColor(Color.WHITE);
    setSelectedTextColor(getForeground());
    setBorder(null);
    
    // Gestion des �v�nements
    smcl = new SelectionModeCaractereListener();
    addMouseListener(smcl);
    addMouseMotionListener(smcl);
    addMouseListener(new MouseAdapter() {
      public void mouseReleased(MouseEvent e) {
        thisMouseReleased(e);
      }
      
      public void mousePressed(MouseEvent e) {
        thisMousePressed(e);
      }
    });
    addMouseMotionListener(new MouseAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        super.mouseMoved(e);
        thisMouseMoved(e);
      }
    });
    addKeyListener(smcl);
    addKeyListener(new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        refreshText(null);
      }
    });
    
    // Cr�ation du menu contextuel
    creationMenuContextuel();
  }
  
  /**
   * Initialise les dimensions en caract�res du composant
   * @param nbrlignes
   * @param nbrcolonnes
   */
  public void setDimensionCaracteres(int nbrlignes, int nbrcolonnes) {
    this.nbrlignes = nbrlignes;
    this.nbrcolonnes = nbrcolonnes;
  }
  
  /**
   * Initialise le composant qui va recevoir le libell�
   * @param composant
   */
  public void setComponentLibelle(final JComponent composant) {
    libelle = composant;
  }
  
  /**
   * Initialise le composant qui va recevoir la position curseur
   * @param composant
   */
  public void setComponentPosCurseur(final JComponent composant) {
    positionCurseur = composant;
  }
  
  /**
   * S�lectionne automatiquement les lignes du spool
   */
  public void selectionAuto() {
    // R�cup�ration de la taille d'un caract�re
    FontMetrics fm = getFontMetrics(getFont());
    int w_char = fm.charWidth('M');
    int h_char = fm.getHeight();
    
    // Cr�ation des s�lections dans une liste provisoire
    int tailleListe = listeRectangle.size();
    for (int i = 0; i < nbrlignes; i++) {
      // Cr�ation d'un nouvel �l�ment
      SelectionModeCaractere smc = new SelectionModeCaractere(i, 0, nbrcolonnes, w_char, h_char, true);
      String chaine = smc.getLibelle().replace('@', ' ').trim();
      boolean trouve = false;
      // On recherche la chaine dans la liste officielle (du d�but) pour �viter les doublons
      for (int j = 0; j < tailleListe; j++)
        if (((SelectionModeCaractere) listeRectangle.get(j)).getLibelle().indexOf(chaine) != -1) {
          trouve = true;
          break;
        }
      // Si on n'a pas trouv� on ins�re la s�lection dans la liste
      if (!trouve)
        listeRectangle.add(smc);
    }
    repaint();
  }
  
  /**
   * Effacement de toutes les s�lections
   */
  public void effacerSelection() {
    listeRectangle.clear();
    repaint();
  }
  
  /**
   * Recherche quel est la selection qui a le focus
   * @param e
   */
  protected int findWhoIsSelectionned(MouseEvent e) {
    int indice = -1;
    for (int i = 0; i < listeRectangle.size(); i++)
      if (((SelectionModeCaractere) listeRectangle.get(i)).setIsIn(e.getX(), e.getY()))
        indice = i;
    repaint();
    
    return indice;
  }
  
  /**
   * Retourne la ligne dans le textearea de la s�lection
   * @return
   */
  protected int getLigneSelection(String texte) {
    // Le texte doit �tre une variable de type @LIGNE...
    if ((texte != null) && (texte.startsWith('@' + SelectionModeCaractere.LIB))) {
      // R�cup�ration de la ligne concern�e
      int pos = texte.indexOf(SelectionModeCaractere.LIB) + SelectionModeCaractere.LIB.length();
      // System.out.println("--> texte: " + texte + " pos: " +pos + " *:" + texte.substring(pos, pos+2));
      try {
        return Integer.parseInt(texte.substring(pos, pos + 2)) - 1;
      }
      catch (Exception e) {
        // JOptionPane.showMessageDialog(this, "Format du libell� incorrect : " + texte, "Erreur", JOptionPane.ERROR_MESSAGE);
        // System.out.println("--> erreur " + e);
        return -1;
      }
    }
    return 0;
  }
  
  /**
   * Rafraichit le texte du composant libell�
   */
  protected void refreshText(SelectionModeCaractere rs) {
    if (libelle == null)
      return;
    String chaine = getLibelle();
    ((JLabel) libelle).setText(chaine);
    Clipboard.sendTextToClipBoard(chaine);
    
    // Mise � jour dans l'�diteur de page
    if ((pageEditor != null) && (rs != null)) {
      pageEditor.getGfxPage().getEtiquette().modifieObjet(rs.getSauveLibelle(), chaine);
      System.out.println("-SpoolTextArea (refreshText)-> passage refresh text");
    }
  }
  
  /**
   * Converti une description de page en rectangle (s�lection)
   * @param texte
   * @param listeRectangle
   */
  protected void convertDescription2Selection(String texte, ArrayList<SelectionModeCaractere> listeRectangle) {
    if (texte == null)
      return;
    // R�cup�ration de la taille d'un caract�re
    FontMetrics fm = getFontMetrics(getFont());
    int w_char = fm.charWidth('M');
    int h_char = fm.getHeight();
    int posdeb = 0, posfin = 0;
    boolean trim = false;
    
    if (texte.indexOf('@') != -1) {
      // R�cup�ration de la ligne concern�e
      int row = getLigneSelection(texte);
      if (row <= 0) {
        if (row == -1)
          JOptionPane.showMessageDialog(this, "Format du libell� incorrect : " + texte, "Erreur", JOptionPane.ERROR_MESSAGE);
        return;
      }
      
      // R�cup�ration de la colonne
      int colonne = 0;
      posdeb = texte.indexOf('/');
      if (posdeb != -1) {
        posfin = texte.lastIndexOf(',');
        colonne = Integer.parseInt(texte.substring(posdeb + 1, posfin));
      }
      /* Ne comprend pas � quoi �a sert
      else
      {
      	JOptionPane.showMessageDialog(this, "Format du libell� incorrect : " + texte, "Erreur", JOptionPane.ERROR_MESSAGE);
      	return;
      }*/
      
      // R�cup�ration de la longueur de la s�lection
      int longueur = nbrcolonnes;
      posdeb = texte.indexOf(',');
      if (posdeb != -1) {
        posfin = texte.lastIndexOf('^');
        if (posfin == -1) {
          posfin = texte.lastIndexOf('\u00a8');
          if (posfin == -1) {
            JOptionPane.showMessageDialog(this, "Format du libell� incorrect : " + texte, "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
          }
          trim = true;
        }
        longueur = Integer.parseInt(texte.substring(posdeb + 1, posfin));
      }
      
      // Ajout du rectangle apr�s validation de ses infos
      if (texte.startsWith("@" + SelectionModeCaractere.LIB)) {
        listeRectangle.add(new SelectionModeCaractere(row, colonne, longueur, w_char, h_char, longueur == nbrcolonnes));
        listeRectangle.get(listeRectangle.size() - 1).setTrim(trim);
      }
    }
  }
  
  /**
   * Recalcule la taille et la position des selection suite au changement de la taille de la police
   * @param taille
   */
  public void setChgSizeFont(int taille) {
    // Modification de la taille de la police
    Font police = getFont();
    setFont(new Font(police.getName(), police.getStyle(), taille));
    // R�cup�ration de la taille d'un caract�re
    FontMetrics fm = getFontMetrics(getFont());
    // Modification de cette taille dans l'�couteur et dans la liste des s�lections
    smcl.init(this);
    for (int i = 0; i < listeRectangle.size(); i++)
      ((SelectionModeCaractere) listeRectangle.get(i)).setDimChar(fm.charWidth('M'), fm.getHeight());
  }
  
  /**
   * @param creationAutoLabel the creationAutoLabel to set
   */
  public void setCreationAutoLabel(boolean creationAutoLabel) {
    this.creationAutoLabel = creationAutoLabel;
  }
  
  /**
   * Retourne le libell� de la s�lection courante
   * @return
   */
  public String getLibelle() {
    if (indiceSelection == -1)
      return "";
    return ((SelectionModeCaractere) listeRectangle.get(indiceSelection)).getLibelle();
  }
  
  /**
   * @return the couleur
   */
  public Color getCouleur() {
    return couleur;
  }
  
  /**
   * @param couleur the couleur to set
   */
  public void setCouleur(Color couleur) {
    this.couleur = couleur;
    if (smcl != null)
      smcl.setCouleur(couleur);
  }
  
  /**
   * @return the couleurSelection
   */
  public Color getCouleurSelection() {
    return couleurSelection;
  }
  
  /**
   * @param couleurSelection the couleurSelection to set
   */
  public void setCouleurSelection(Color couleurSelection) {
    this.couleurSelection = couleurSelection;
    if (smcl != null)
      smcl.setCouleurSelection(couleurSelection);
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
    smcl.setPageEditor(pageEditor);
  }
  
  // ------------------------------------ M�thodes priv�es -------------------
  
  /**
   * Cr�ation du menu contextuel
   */
  protected void creationMenuContextuel() {
    pm_Btd = new JPopupMenu();
    mi_Suppimer = new JMenuItem();
    
    // setComponentPopupMenu(pm_Btd);
    pm_Btd.setName("pm_Btd");
    /*
    pm_Btd.addPopupMenuListener(new PopupMenuListener() {
    	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    		pm_BtdPopupMenuWillBecomeVisible(e);
    	}
    	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}
    	public void popupMenuCanceled(PopupMenuEvent e) {}
    });
    */
    // ---- mi_Suppimer ----
    mi_Suppimer.setText("Supprimer");
    mi_Suppimer.setName("mi_Suppimer");
    mi_Suppimer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        mi_SuppimerActionPerformed(e);
      }
    });
    pm_Btd.add(mi_Suppimer);
  }
  
  /**
   * Grise les menuItems si on a pas cliqu� sur une s�lection
   * @param enable
   */
  protected void setEnabledChild(boolean enable) {
    Component[] composants = pm_Btd.getComponents();
    for (int i = 0; i < composants.length; i++)
      composants[i].setEnabled(enable);
  }
  
  // ------------------------------------ Ev�nements -------------------------
  
  /**
   * Action lors du relach� du bouton de la souris
   * @param e
   */
  private void thisMousePressed(MouseEvent e) {
    // System.out.println(e.getButton());
    positionSouris = e.getPoint();
    selectionEncours = true;
    // On recherche si on a cliqu� sur une s�lection
    indiceSelection = findWhoIsSelectionned(e);
  }
  
  /**
   * Action lors du relach� du bouton de la souris
   * @param e
   */
  private void thisMouseReleased(MouseEvent e) {
    // System.out.println("-textarea ->" + listeRectangle.size());
    selectionEncours = false;
    // On ajoute la nouvelle s�lection � la liste
    if (smcl.getRectangleSelection() != null)
      for (int i = 0; i < smcl.getRectangleSelection().size(); i++) {
        listeRectangle.add(smcl.getRectangleSelection().get(i));
        if (creationAutoLabel)
          pageEditor.creationAutoLabel(smcl.getRectangleSelection().get(i).getX(), smcl.getRectangleSelection().get(i).getY(),
              smcl.getRectangleSelection().get(i).getLibelle(), getPreferredSize());
      }
    indiceSelection = findWhoIsSelectionned(e);
    refreshText(null);
    repaint();
    
    // On met dans la liste le rectangle s�lectionn� (� am�liorer) et on active le menu contextuel
    smcl.getRectangleSelection().clear();
    if (indiceSelection >= 0) {
      smcl.getRectangleSelection().add(listeRectangle.get(indiceSelection));
      setComponentPopupMenu(pm_Btd);
    }
    else
      setComponentPopupMenu(null);
  }
  
  /**
   * Action lors du d�placement de la souris
   * @param e
   */
  private void thisMouseMoved(MouseEvent e) {
    FontMetrics fm = getFontMetrics(getFont());
    int w_char = (e.getPoint().x / fm.charWidth('M')) + 1;
    int h_char = (e.getPoint().y / fm.getHeight()) + 1;
    
    ((JLabel) positionCurseur).setText("Curseur: " + h_char + " / " + w_char);
  }
  
  /**
   * Dessine dans le panel
   */
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    
    // On dessine le rectangle en cours uniquement
    if ((smcl != null) && (selectionEncours)) {
      g.setColor(couleurSelection);
      g.drawRect(smcl.x, smcl.y, smcl.w, smcl.h);
    }
    
    // On dessine les rectangles d�j� cr�es
    for (int i = 0; i < listeRectangle.size(); i++)
      ((SelectionModeCaractere) listeRectangle.get(i)).paint(g);
  }
  
  protected void mi_SuppimerActionPerformed(ActionEvent e) {
    if (indiceSelection == -1)
      return;
    listeRectangle.remove(indiceSelection);
    repaint();
  }
  
}
