/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.DocumentEditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import description.DescriptionDocument;
import description.DescriptionPage;
import rad.Parametres.Preferences;
import rad.outils.RiFileChooser;
import rendu.GenerationFichierFO;
import rendu.GenerationFichierFinal;
import ri.seriem.libcommun.edition.Spool;
import ri.seriem.libcommun.outils.GestionFiltre;
import ri.seriem.libcommun.outils.XMLTools;

//=================================================================================================
//==> Editeur de document
//=================================================================================================

public class GfxDocumentEditor extends JFrame {
  // Constantes
  private static final long serialVersionUID = 1L;
  public final static String UN = "Premi�re de couverture";
  public final static String DEUX = "Deuxi�me de couverture";
  public final static String SEQUENCE = "Sequence";
  public final static String TROIS = "Troisi�me de couverture";
  public final static String QUATRE = "Quatri�me de couverture";
  
  // Variables
  private File fichierddd = null;
  private File dossierddd = null;
  private Preferences prefs = null;
  private DndTree tree_Document = null;
  private DescriptionDocument document = null;
  private File dernierPath = null;
  
  /**
   * Constructeur
   * @param ficddd
   */
  public GfxDocumentEditor(String ficddd, Preferences prefs) {
    super();
    initComponents();
    setVisible(true);
    
    this.prefs = prefs;
    dernierPath = new File(prefs.getDernierPath());
    setSize(640, 480);
    
    // On ouvre un document d�j� existant
    if (ficddd != null) {
      fichierddd = new File(ficddd);
      dossierddd = fichierddd.getParentFile();
      initListePage(dossierddd);
      initDocument(fichierddd);
      initInformations();
    }
    createTree();
  }
  
  /**
   * Initalise les donn�es � partir de la description
   */
  private void initInformations() {
    if (document == null)
      return;
    
    if (document.getNom() != null)
      tf_Nom.setText(document.getNom());
    tf_Dpi.setText("" + document.getSource_dpi());
    if (document.getCommentaire() != null)
      ta_Commentaire.setText(document.getCommentaire());
  }
  
  /**
   * Cr�ation de l'arbre
   */
  private void createTree() {
    DefaultMutableTreeNode root = new DefaultMutableTreeNode("Document");
    DefaultMutableTreeNode groupe = null;
    
    // Cr�ation de l'arborescence
    groupe = new DefaultMutableTreeNode(UN);
    if ((document != null) && (document.getPremiereDeCouverture() != null))
      groupe.add(new DefaultMutableTreeNode(document.getPremiereDeCouverture().getNomFichierDDP()));
    root.add(groupe);
    groupe = new DefaultMutableTreeNode(DEUX);
    if ((document != null) && (document.getDeuxiemeDeCouverture() != null))
      groupe.add(new DefaultMutableTreeNode(document.getDeuxiemeDeCouverture().getNomFichierDDP()));
    root.add(groupe);
    groupe = new DefaultMutableTreeNode(SEQUENCE);
    if ((document != null) && (document.getSequence().size() > 0))
      for (int i = 0; i < document.getSequence().size(); i++)
        groupe.add(new DefaultMutableTreeNode(((DescriptionPage) document.getSequence().get(i)).getNomFichierDDP()));
    root.add(groupe);
    groupe = new DefaultMutableTreeNode(TROIS);
    if ((document != null) && (document.getTroisiemeDeCouverture() != null))
      groupe.add(new DefaultMutableTreeNode(document.getTroisiemeDeCouverture().getNomFichierDDP()));
    root.add(groupe);
    groupe = new DefaultMutableTreeNode(QUATRE);
    if ((document != null) && (document.getQuatriemeDeCouverture() != null))
      groupe.add(new DefaultMutableTreeNode(document.getQuatriemeDeCouverture().getNomFichierDDP()));
    root.add(groupe);
    
    // Cr�ation de l'arbre
    tree_Document = new DndTree(root);
    splitPane1.setLeftComponent(new JScrollPane(tree_Document));
    splitPane1.setDividerLocation(0.6);
    tree_Document.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    MouseListener ml = new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        int selRow = tree_Document.getRowForLocation(e.getX(), e.getY());
        TreePath selPath = tree_Document.getPathForLocation(e.getX(), e.getY());
        if (selRow != -1) {
          if (SwingUtilities.isRightMouseButton(e))
            createMenucontextuel(selRow, selPath).show((Component) e.getSource(), e.getX(), e.getY());
        }
      }
    };
    tree_Document.addMouseListener(ml);
    tree_Document.setExpandsSelectedPaths(true);
  }
  
  /**
   * Cr�ation du menu contextuel
   */
  private JPopupMenu createMenucontextuel(final int selRow, final TreePath selPath) {
    JPopupMenu pop_Menu = new JPopupMenu();
    JMenuItem mi_Supprimer = new JMenuItem("Supprimer");
    mi_Supprimer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath.getLastPathComponent();
        // System.out.println("--> " + node);
        if ((node == null) || (node.toString().equals(UN)) || (node.toString().equals(DEUX)) || (node.toString().equals(SEQUENCE))
            || (node.toString().equals(TROIS)) || (node.toString().equals(QUATRE)))
          return;
        // Object nodeInfo = node.getUserObject();
        if (node.isLeaf()) {
          // System.out.println("--> " + node + " " + nodeInfo );
          node.removeFromParent();
          ((DefaultTreeModel) tree_Document.getModel()).reload();
        }
      }
    });
    pop_Menu.add(mi_Supprimer);
    
    return pop_Menu;
  }
  
  /**
   * Initialise la liste des description de page disponible
   * @param dossierddd
   */
  private void initListePage(File dossierddd) {
    File[] listeDDP = getListeDDP(dossierddd);
    if (listeDDP == null)
      return;
    
    DefaultListModel listModel = new DefaultListModel();
    for (int i = 0; i < listeDDP.length; i++)
      listModel.addElement(listeDDP[i].getName());
    lst_Pages.setModel(listModel);
    lst_Pages.setDragEnabled(true);
  }
  
  /**
   * Chargement de la description
   * @param fichierddd
   */
  private void initDocument(File fichierddd) {
    if ((fichierddd == null) || (!fichierddd.exists()))
      return;
    
    // Lecture du fichier ddd
    try {
      document = (DescriptionDocument) XMLTools.decodeFromFile(fichierddd.getAbsolutePath());
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Retourne la liste des fichiers
   * @param dossierddd
   * @return
   */
  private File[] getListeDDP(File dossierddd) {
    if ((dossierddd == null) || (!dossierddd.exists()))
      return null;
    
    return dossierddd.listFiles(new FilenameFilter() {
      public boolean accept(File dir, String name) {
        return name.endsWith(DescriptionPage.EXTENSION);
      }
    });
  }
  
  /**
   * Retourne la description de la page d�sign�e
   * @return
   */
  private DescriptionPage getDescriptionPage(String nomddp) {
    File fichierddp = new File(dossierddd.getAbsolutePath() + File.separatorChar + nomddp);
    if (!fichierddp.exists()) {
      JOptionPane.showMessageDialog(this, "Fichier " + fichierddp.getAbsolutePath() + " non trouv�.", "Erreur",
          JOptionPane.ERROR_MESSAGE);
      return null;
    }
    
    try {
      // System.out.println("--> " + dossierddd.getAbsolutePath() + File.separatorChar + nomddp);
      return (DescriptionPage) XMLTools.decodeFromFile(dossierddd.getAbsolutePath() + File.separatorChar + nomddp);
    }
    catch (Exception e) {
      System.out.println("[GfxDocumentEditor] (getDescriptionPage) Erreur " + e + "\n" + nomddp);
      e.printStackTrace();
      return null;
    }
    
  }
  
  /**
   * Parcourt de l'arbre pour r�cup�rer les descriptions de page
   * @param r
   * @param doc
   */
  private boolean parcourirNoeud(Object r, DescriptionDocument doc) {
    DescriptionPage dp = null;
    TreeNode root = (TreeNode) r;
    // System.out.println(root);
    for (int i = 0; i < root.getChildCount(); i++) {
      if (root.getChildAt(i).toString().endsWith(DescriptionPage.EXTENSION)) {
        dp = getDescriptionPage(root.getChildAt(i).toString());
        if (dp == null)
          return false;
      }
      if (root.toString().equals(UN))
        doc.setPremiereDeCouverture(dp);
      else if (root.toString().equals(DEUX))
        doc.setDeuxiemeDeCouverture(dp);
      else if (root.toString().equals(SEQUENCE))
        doc.getSequence().add(dp);
      else if (root.toString().equals(TROIS))
        doc.setTroisiemeDeCouverture(dp);
      else if (root.toString().equals(QUATRE))
        doc.setQuatriemeDeCouverture(dp);
      
      if (!root.getChildAt(i).isLeaf())
        if (!parcourirNoeud(root.getChildAt(i), doc))
          return false;
    }
    
    return true;
  }
  
  /**
   * G�n�re la description du document en cours
   * @return
   */
  private DescriptionDocument genereDescription() {
    document = new DescriptionDocument();
    
    // On alimente les descriptions de pages
    if (!parcourirNoeud(tree_Document.getModel().getRoot(), document))
      return null;
    
    // Les autres infos du document
    if (tf_Nom.getText().trim().equals(""))
      document.setNom(null);
    else
      document.setNom(tf_Nom.getText().trim());
    if (!tf_Dpi.getText().trim().equals(""))
      document.setSource_dpi(Integer.parseInt(tf_Dpi.getText().trim()));
    if (ta_Commentaire.getText().trim().equals(""))
      document.setCommentaire(null);
    else
      document.setCommentaire(ta_Commentaire.getText());
    
    return document;
  }
  
  /**
   * Ouvre une boite de dialogue afin de s�lectionner un dossier
   */
  private File getSelectionDossier(String origine) {
    if (origine == null)
      origine = prefs.getDernierPath();
    RiFileChooser selectionDossier = new RiFileChooser(null, origine);
    return selectionDossier.choisirDossier(null);
  }
  
  /**
   * Ouvre la fen�tre de sauvegarde de la description de document
   * @return
   *
   *         private String saveDDD(boolean affichedialog)
   *         {
   *         String chemin = null;
   *         
   *         // On v�rifie qu'on ait bien un nom de fichier
   *         if ((!affichedialog) && (!dernierPath.getName().endsWith(DescriptionDocument.EXTENSION)))
   *         if (getTitle().endsWith(DescriptionDocument.EXTENSION))
   *         dernierPath = new File(getTitle());
   *         else
   *         affichedialog = true;
   *         
   *         if (dernierPath.isDirectory()) affichedialog = true;
   *         else
   *         chemin = dernierPath.getAbsolutePath();
   *         
   *         // Affiche la boite de dialogue si besoin
   *         if (affichedialog)
   *         {
   *         final JFileChooser choixFichier = new JFileChooser();
   *         //choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
   * 
   *         choixFichier.addChoosableFileFilter(new GestionFiltre(new String[]{"ddd"}, "Fichier description de document (*.ddd)"));
   *         // On se place dans le dernier dossier courant s'il y en a un
   *         if (dernierPath != null)
   *         choixFichier.setCurrentDirectory(dernierPath);
   * 
   *         // R�cup�ration du chemin
   *         if (choixFichier.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
   *         chemin = choixFichier.getSelectedFile().getPath();
   *         else
   *         return null;
   * 
   *         // On stocke comme le dossier courant
   *         dernierPath = choixFichier.getSelectedFile();//getCurrentDirectory();
   *         }
   *         else
   *         dernierPath = new File(getTitle());
   *         
   *         // V�rifie qu'il ait bien l'extension
   *         if (chemin.lastIndexOf('.') == -1)
   *         chemin = chemin + DescriptionDocument.EXTENSION;
   *         setTitle(chemin);
   * 
   *         // G�n�ration du fichier XML
   *         try
   *         {
   *         document = genereDescription();
   *         XMLTools.encodeToFile(document, chemin);
   *         }
   *         catch (Exception ex)
   *         {
   *         ex.printStackTrace();
   *         }
   * 
   *         return chemin;
   *         }
   */
  /**
   * Ouvre la fen�tre de sauvegarde de la description de document
   * @return
   */
  private String saveDDD(boolean affichedialog) {
    if (fichierddd == null)
      fichierddd = dernierPath;
    RiFileChooser selectionFichier = new RiFileChooser(this, fichierddd);
    
    // On v�rifie qu'on ait bien un nom de fichier
    if ((!affichedialog) && (!fichierddd.getName().endsWith(DescriptionDocument.EXTENSION)))
      affichedialog = true;
    
    if (fichierddd.isDirectory())
      affichedialog = true;
    
    // Affiche la boite de dialogue si besoin
    if (affichedialog) {
      fichierddd = selectionFichier.enregistreFichier(fichierddd, "ddd", "Fichier description de document (*.ddd)");
      if (fichierddd == null)
        return null;
    }
    
    // V�rifie qu'il ait bien l'extension
    if (fichierddd.getName().lastIndexOf('.') == -1)
      fichierddd = new File(fichierddd.getAbsolutePath() + DescriptionDocument.EXTENSION);
    
    // G�n�ration du fichier XML
    try {
      document = genereDescription();
      if (document == null) {
        JOptionPane.showMessageDialog(this, "Fichier non enregistr�, car il contient des donn�es erron�es.", "Erreur",
            JOptionPane.ERROR_MESSAGE);
        return null;
      }
      XMLTools.encodeToFile(document, fichierddd.getAbsolutePath());
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    
    if (prefs != null)
      prefs.setDernierPath(fichierddd.getParent());
    return fichierddd.getAbsolutePath();
  }
  
  /**
   * Teste le document avec un spool
   */
  private void testWithSpool() {
    Spool spool = null;
    String chemin = ouvrir("spl", "Fichier spool (*.spl)");
    if (chemin != null)
      spool = chargementSPL(chemin);
    if (spool == null)
      return;
    previewDocument(spool.getPages(), "pdf");
    // previewDocument(spool.getPages(), "ps");
  }
  
  /**
   * Recherche le fichier � ouvrir
   */
  private String ouvrir(String extension, String extdescription) {
    String chemin = null;
    final JFileChooser choixFichier = new JFileChooser();
    // choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
    
    choixFichier.addChoosableFileFilter(new GestionFiltre(new String[] { extension }, extdescription));
    // On se place dans le dernier dossier courant s'il y en a un
    if (dernierPath != null)
      choixFichier.setCurrentDirectory(dernierPath);
    
    // Recuperation du chemin
    if (choixFichier.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
      chemin = choixFichier.getSelectedFile().getPath();
    else
      return null;
    
    // On stocke comme le dossier courant
    dernierPath = choixFichier.getCurrentDirectory();
    
    return chemin;
  }
  
  /**
   * Charge le fichier spool (spl)
   * @return
   */
  private Spool chargementSPL(String fichier) {
    // Lecture du fichier spl
    try {
      return (Spool) XMLTools.decodeFromFile(fichier);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
  
  private void previewDocument(String[] pages, String extension) {
    if (pages == null)
      return;
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    
    DescriptionDocument doc = genereDescription();
    // G�n�ration du doc
    GenerationFichierFO gffo = new GenerationFichierFO(doc, pages, dernierPath.getAbsolutePath());
    gffo.genereFOtoFile();
    // System.out.println("--> dossier preview " + dernierPath.getAbsolutePath());
    
    // Rendu final
    GenerationFichierFinal gff = new GenerationFichierFinal();
    gff.setFichierEntree(gffo.getNomFichierFO());
    gff.setTypeSortie(extension);
    String rendu = gff.renduFinal(doc.getSource_dpi(), 72);
    
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    // On lance l'application associ�e
    if (rendu == null)
      return;
    try {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      Desktop.getDesktop().open(new File(rendu));
    }
    catch (IOException e1) {
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }
  
  private void bt_ValiderActionPerformed(ActionEvent e) {
    saveDDD(true);
    dispose();
  }
  
  private void bt_AnnulerActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void bt_ParcourirActionPerformed(ActionEvent e) {
    dossierddd = getSelectionDossier(null);
    if (dossierddd != null)
      initListePage(dossierddd);
  }
  
  private void bt_TestActionPerformed(ActionEvent e) {
    testWithSpool();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    splitPane1 = new JSplitPane();
    scrollPane2 = new JScrollPane();
    panel1 = new JPanel();
    scrollPane1 = new JScrollPane();
    lst_Pages = new JList();
    panel2 = new JPanel();
    bt_Parcourir = new JButton();
    p_Infos = new JPanel();
    l_Nom = new JLabel();
    tf_Nom = new JTextField();
    l_Dpi = new JLabel();
    tf_Dpi = new JTextField();
    l_Commentaire = new JLabel();
    scrollPane3 = new JScrollPane();
    ta_Commentaire = new JTextArea();
    label3 = new JLabel();
    label1 = new JLabel();
    p_Pied = new JPanel();
    p_PiedGauche = new JPanel();
    bt_Test = new JButton();
    p_PiedDroit = new JPanel();
    bt_Valider = new JButton();
    bt_Annuler = new JButton();
    
    // ======== this ========
    setTitle("Editeur de document");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== splitPane1 ========
    {
      splitPane1.setName("splitPane1");
      
      // ======== scrollPane2 ========
      {
        scrollPane2.setName("scrollPane2");
      }
      splitPane1.setLeftComponent(scrollPane2);
      
      // ======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(new BorderLayout());
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");
          
          // ---- lst_Pages ----
          lst_Pages.setName("lst_Pages");
          scrollPane1.setViewportView(lst_Pages);
        }
        panel1.add(scrollPane1, BorderLayout.CENTER);
        
        // ======== panel2 ========
        {
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- bt_Parcourir ----
          bt_Parcourir.setText("Parcourir");
          bt_Parcourir.setToolTipText("S\u00e9lection du dossier pour la DDD");
          bt_Parcourir.setName("bt_Parcourir");
          bt_Parcourir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              bt_ParcourirActionPerformed(e);
            }
          });
          panel2.add(bt_Parcourir);
          bt_Parcourir.setBounds(new Rectangle(new Point(10, 10), bt_Parcourir.getPreferredSize()));
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        panel1.add(panel2, BorderLayout.EAST);
      }
      splitPane1.setRightComponent(panel1);
    }
    contentPane.add(splitPane1, BorderLayout.CENTER);
    
    // ======== p_Infos ========
    {
      p_Infos.setBorder(new TitledBorder("Informations"));
      p_Infos.setName("p_Infos");
      p_Infos.setLayout(null);
      
      // ---- l_Nom ----
      l_Nom.setText("Nom");
      l_Nom.setName("l_Nom");
      p_Infos.add(l_Nom);
      l_Nom.setBounds(new Rectangle(new Point(35, 45), l_Nom.getPreferredSize()));
      
      // ---- tf_Nom ----
      tf_Nom.setName("tf_Nom");
      p_Infos.add(tf_Nom);
      tf_Nom.setBounds(140, 39, 125, tf_Nom.getPreferredSize().height);
      
      // ---- l_Dpi ----
      l_Dpi.setText("Dpi");
      l_Dpi.setName("l_Dpi");
      p_Infos.add(l_Dpi);
      l_Dpi.setBounds(new Rectangle(new Point(35, 80), l_Dpi.getPreferredSize()));
      
      // ---- tf_Dpi ----
      tf_Dpi.setName("tf_Dpi");
      p_Infos.add(tf_Dpi);
      tf_Dpi.setBounds(140, 74, 40, tf_Dpi.getPreferredSize().height);
      
      // ---- l_Commentaire ----
      l_Commentaire.setText("Commentaire");
      l_Commentaire.setName("l_Commentaire");
      p_Infos.add(l_Commentaire);
      l_Commentaire.setBounds(new Rectangle(new Point(35, 115), l_Commentaire.getPreferredSize()));
      
      // ======== scrollPane3 ========
      {
        scrollPane3.setName("scrollPane3");
        
        // ---- ta_Commentaire ----
        ta_Commentaire.setName("ta_Commentaire");
        scrollPane3.setViewportView(ta_Commentaire);
      }
      p_Infos.add(scrollPane3);
      scrollPane3.setBounds(140, 115, 375, 115);
      
      // ---- label3 ----
      label3.setText("(des images utilis\u00e9es dans les descriptions de page)");
      label3.setName("label3");
      p_Infos.add(label3);
      label3.setBounds(new Rectangle(new Point(185, 80), label3.getPreferredSize()));
      
      // ---- label1 ----
      label1.setText("(\u00e0 titre indicatif)");
      label1.setName("label1");
      p_Infos.add(label1);
      label1.setBounds(new Rectangle(new Point(280, 45), label1.getPreferredSize()));
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_Infos.getComponentCount(); i++) {
          Rectangle bounds = p_Infos.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Infos.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Infos.setMinimumSize(preferredSize);
        p_Infos.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(p_Infos, BorderLayout.NORTH);
    
    // ======== p_Pied ========
    {
      p_Pied.setName("p_Pied");
      p_Pied.setLayout(new BorderLayout());
      
      // ======== p_PiedGauche ========
      {
        p_PiedGauche.setName("p_PiedGauche");
        p_PiedGauche.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        // ---- bt_Test ----
        bt_Test.setText("Tester avec un spool");
        bt_Test.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Test.setName("bt_Test");
        bt_Test.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_TestActionPerformed(e);
          }
        });
        p_PiedGauche.add(bt_Test);
      }
      p_Pied.add(p_PiedGauche, BorderLayout.WEST);
      
      // ======== p_PiedDroit ========
      {
        p_PiedDroit.setName("p_PiedDroit");
        p_PiedDroit.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        // ---- bt_Valider ----
        bt_Valider.setText("Valider");
        bt_Valider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Valider.setName("bt_Valider");
        bt_Valider.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_ValiderActionPerformed(e);
          }
        });
        p_PiedDroit.add(bt_Valider);
        
        // ---- bt_Annuler ----
        bt_Annuler.setText("Annuler");
        bt_Annuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Annuler.setName("bt_Annuler");
        bt_Annuler.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_AnnulerActionPerformed(e);
          }
        });
        p_PiedDroit.add(bt_Annuler);
      }
      p_Pied.add(p_PiedDroit, BorderLayout.EAST);
    }
    contentPane.add(p_Pied, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JSplitPane splitPane1;
  private JScrollPane scrollPane2;
  private JPanel panel1;
  private JScrollPane scrollPane1;
  private JList lst_Pages;
  private JPanel panel2;
  private JButton bt_Parcourir;
  private JPanel p_Infos;
  private JLabel l_Nom;
  private JTextField tf_Nom;
  private JLabel l_Dpi;
  private JTextField tf_Dpi;
  private JLabel l_Commentaire;
  private JScrollPane scrollPane3;
  private JTextArea ta_Commentaire;
  private JLabel label3;
  private JLabel label1;
  private JPanel p_Pied;
  private JPanel p_PiedGauche;
  private JButton bt_Test;
  private JPanel p_PiedDroit;
  private JButton bt_Valider;
  private JButton bt_Annuler;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
