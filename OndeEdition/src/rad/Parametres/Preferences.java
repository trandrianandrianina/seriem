/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.Parametres;

import java.io.File;

import ri.seriem.libcommun.outils.Chiffrage;

//=================================================================================================
//==> Pr�f�rences utilisateur pour le Rad
//=================================================================================================
public class Preferences {
  // Constantes
  private final static String NOM_ESPACE = "NewSim";
  // Variables
  private String espaceTravail = System.getProperty("user.home") + File.separatorChar + NOM_ESPACE;
  private String dernierPath = System.getProperty("user.home");
  private Chiffrage chiffrage = new Chiffrage("ISO8859-15");
  
  private String serveur = null;
  private String profil = null;
  private String mdp = null;
  private transient String mdpclair = null;
  
  /**
   * @param espaceTravail the espaceTravail to set
   */
  public void setEspaceTravail(String espaceTravail) {
    this.espaceTravail = espaceTravail;
  }
  
  /**
   * @return the espaceTravail
   */
  public String getEspaceTravail() {
    return espaceTravail;
  }
  
  /**
   * @param dernierPath the dernierPath to set
   */
  public void setDernierPath(String dernierPath) {
    this.dernierPath = dernierPath;
  }
  
  /**
   * @return the dernierPath
   */
  public String getDernierPath() {
    return dernierPath;
  }
  
  /**
   * @param serveur the serveur to set
   */
  public void setServeur(String serveur) {
    this.serveur = serveur;
  }
  
  /**
   * @return the serveur
   */
  public String getServeur() {
    return serveur;
  }
  
  /**
   * @param profil the profil to set
   */
  public void setProfil(String profil) {
    this.profil = profil;
  }
  
  /**
   * @return the profil
   */
  public String getProfil() {
    return profil;
  }
  
  /**
   * @param mdp the mdp to set
   */
  public void setMdp(String mdp) {
    this.mdp = mdp;
  }
  
  /**
   * @return the mdp
   */
  public String getMdp() {
    return mdp;
  }
  
  /**
   * @param mdpclair the mdpclair to set
   */
  public void setMdpclair(String mdpclair) {
    this.mdpclair = mdpclair;
    // On chiffre le mdp
    setMdp(chiffrage.cryptInString(mdpclair));
    // System.out.println("--> " + mdp);
  }
  
  /**
   * @return the mdpclair
   */
  public String getMdpclair() {
    // On d�chiffre le mot de passe
    mdpclair = chiffrage.decryptInString(getMdp());
    // System.out.println("--> " + mdpclair);
    return mdpclair;
  }
  
}
