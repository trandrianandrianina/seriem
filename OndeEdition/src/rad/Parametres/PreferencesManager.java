/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.Parametres;

import java.io.File;

import ri.seriem.libcommun.outils.XMLTools;

//=================================================================================================
//==>  Permet de g�rer (enregistrement, lecture) les pr�f�rences de l'utilisateur
//==>  ainsi l'historique des menus
//=================================================================================================
public class PreferencesManager {
  // Constantes non enregistr�es
  public transient static final String DOSSIER_PREFS = System.getProperty("user.home") + File.separator + ".RadOndeEdition";
  public transient static final String FICHIER_PREFS = DOSSIER_PREFS + File.separator + "preferences.xml";
  
  // Variables
  private Preferences prefsUser = null; // Pr�f�rences diverses
  
  /**
   * Retourne les pr�f�rences de l'utilisateur
   * @return the prefsUser
   */
  public Preferences getPreferences() {
    if (prefsUser == null)
      if (!chargePreferencesUser())
        prefsUser = new Preferences();
      
    return prefsUser;
  }
  
  /**
   * Sauvegarde les pr�f�rences de l'utilisateur sur disque
   */
  public void sauvePreferencesUser() {
    File dossier = new File(DOSSIER_PREFS);
    if (!dossier.exists())
      dossier.mkdirs();
    
    // Sauvegarde du fichier sur disque
    try {
      XMLTools.encodeToFile(getPreferences(), FICHIER_PREFS);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Charge les pr�f�rences de l'utilisateur � partir d'un fichier
   */
  private boolean chargePreferencesUser() {
    try {
      prefsUser = (Preferences) XMLTools.decodeFromFile(FICHIER_PREFS);
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }
  
  /**
   * Supprime le fichier des pr�f�rences
   */
  public static void removeFile() {
    File fichier = new File(FICHIER_PREFS);
    fichier.delete();
  }
  
}
