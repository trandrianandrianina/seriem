/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.outils;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import ri.seriem.libcommun.outils.GestionFiltre;

public class RiFileChooser {
  // Variables
  private File dernierChemin = null;
  private JFrame frame = null;
  private JFileChooser choixFichier = null;
  
  /**
   * Constructeur
   * @param derchemin
   */
  public RiFileChooser(JFrame frame, File derchemin) {
    setFrame(frame);
    setDossier(derchemin);
  }
  
  /**
   * Constructeur
   * @param derchemin
   */
  public RiFileChooser(JFrame frame, String derchemin) {
    setFrame(frame);
    if (derchemin == null)
      return;
    setDossier(new File(derchemin));
  }
  
  /**
   * Initialise la fen�tre maitresse
   * @param frame
   */
  private void setFrame(JFrame frame) {
    this.frame = frame;
  }
  
  /**
   * Initialise le dossier par d�faut
   * @param derchemin
   */
  public void setDossier(File derchemin) {
    dernierChemin = derchemin;
    // if (dernierChemin != null)
    // choixFichier.setCurrentDirectory(dernierChemin);
  }
  
  /**
   * Retourne le dossier par d�faut
   * @return
   */
  public File getDossierFile() {
    return dernierChemin;
  }
  
  /**
   * Retourne le dossier par d�faut
   * @return
   */
  public String getDossier() {
    return dernierChemin.getAbsolutePath();
  }
  
  /**
   * Recherche le fichier � ouvrir
   */
  public String choisirFichier(String extension, String extdescription) {
    String fichier = null;
    
    choixFichier = new JFileChooser();
    choixFichier.setCurrentDirectory(dernierChemin);
    choixFichier.addChoosableFileFilter(new GestionFiltre(new String[] { extension }, extdescription));
    
    // R�cup�ration du chemin
    if (choixFichier.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION)
      fichier = choixFichier.getSelectedFile().getPath();
    else
      return null;
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
  
  /**
   * Ouvre une boite de dialogue afin de s�lectionner un dossier
   */
  public File choisirDossier(String origine) {
    if (origine == null)
      origine = dernierChemin.getAbsolutePath();
    choixFichier = new JFileChooser(origine);
    choixFichier.setApproveButtonText("S�lectionner");
    choixFichier.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (choixFichier.showOpenDialog(frame) == JFileChooser.CANCEL_OPTION)
      return null;
    dernierChemin = choixFichier.getSelectedFile();
    
    return dernierChemin;
  }
  
  /**
   * Recherche le fichier pour enregistrer
   * @return
   */
  public File enregistreFichier(File fichier, String extension, String extdescription) {
    choixFichier = new JFileChooser();
    choixFichier.setCurrentDirectory(dernierChemin);
    choixFichier.addChoosableFileFilter(new GestionFiltre(new String[] { extension }, extdescription));
    choixFichier.setSelectedFile(fichier);
    
    // R�cup�ration du chemin
    if (choixFichier.showSaveDialog(choixFichier) == JFileChooser.APPROVE_OPTION)
      fichier = choixFichier.getSelectedFile();
    else
      return null;
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
}
