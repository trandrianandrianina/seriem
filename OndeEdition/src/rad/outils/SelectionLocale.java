/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.outils;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Cette classe est une enveloppe pour le transfert de donn�es des r�f�rences
 * d'objet transf�r�es dans la m�me machine virtuelle
 */
public class SelectionLocale implements Transferable {
  private Object selection;
  
  /**
   * Construit la s�lection
   * 
   * @param s�lection
   *          : n'importe quel type d'objet
   */
  public SelectionLocale(Object selection) {
    this.selection = selection;
  }
  
  public DataFlavor[] getTransferDataFlavors() {
    DataFlavor[] flavors = new DataFlavor[1];
    Class type = selection.getClass();
    String typeMIME = "application/x-java-jvm-local-objectref;class=" + type.getName();
    try {
      flavors[0] = new DataFlavor(typeMIME);
      return flavors;
    }
    catch (ClassNotFoundException ex) {
      return new DataFlavor[0];
    }
  }
  
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    return "application".equals(flavor.getPrimaryType()) && "x-java-jvm-local-objectref".equals(flavor.getSubType())
        && flavor.getRepresentationClass().isAssignableFrom(selection.getClass());
  }
  
  public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
    if (!isDataFlavorSupported(flavor))
      throw new UnsupportedFlavorException(flavor);
    return selection;
  }
}
