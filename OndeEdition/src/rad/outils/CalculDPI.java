/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad.outils;

//=================================================================================================
//==> Calcule la largeur/hauteur d'un A4 en fonction du dpi
//=================================================================================================
public class CalculDPI {
  // Constantes
  // public static final int DPI_STANDARD=72;
  public static final int DPI_ECRAN = 72;
  public static final double INCH = 2.54; // cm
  public static final double A4_HAUTEUR = 29.7; // cm
  public static final double A4_LARGEUR = 21; // cm
  
  /**
   * Retourne la hauteur d'un A4 en cm
   * @return the a4_hauteur
   */
  public static double getA4_hauteur_cm(boolean portrait) {
    if (portrait)
      return A4_HAUTEUR;
    else
      return A4_LARGEUR;
  }
  
  /**
   * Retourne la largeur d'un A4 en cm
   * @return the a4_largeur
   */
  public static double getA4_largeur_cm(boolean portrait) {
    if (portrait)
      return A4_LARGEUR;
    else
      return A4_HAUTEUR;
  }
  
  /**
   * Retourne une dimension en pixel en fonction du dpi souhait� et du dpi d'origine
   * @param dpio
   * @param dim
   * @param dpid
   * @return
   */
  public static int getDim_px(int dpio, float dim, int dpid) {
    return (int) Math.round((dim * dpid) / dpio);
  }
  
  /**
   * Retourne une dimension en pixel en fonction du dpi souhait� et du dpi d'origine
   * @param dpio
   * @param dim
   * @return
   */
  public static int getDim_px(int dpio, float dim) {
    return getDim_px(dpio, dim, DPI_ECRAN);
  }
  
  /**
   * Convertie une dimension en cm en pixel en fonction du dpi
   * @param dpi
   * @param dim
   * @return
   */
  public static int getCmtoPx(int dpi_aff, int dpi_img, float dim) {
    // System.out.println("-aff-> " + dpi_aff);
    // System.out.println("-img-> " + dpi_img);
    return (int) Math.round((dim * ((dpi_aff * dpi_aff) / (dpi_img * INCH))) * 100 / 100.);
  }
  
  /**
   * Convertie une dimension en cm en pixel en fonction du dpi standart
   * @param dim
   * @return
   *
   *         public static int getCmtoPx(float dim)
   *         {
   *         return getCmtoPx(DPI_ECRAN, dim);
   *         }
   */
  
  /**
   * Convertie une dimension en pixel en cm en fonction du dpi
   * @param dpi
   * @param dim
   * @return
   */
  public static float getPxtoCm(int dpi_aff, int dpi_img, float dim) {
    // return (float) (Math.round(((dim * INCH) / dpi) *100) / 100.);
    // return (float) (Math.round( (dim / ((DPI_ECRAN / INCH) * (DPI_ECRAN / dpi)) ) * 100) / 100.);
    // System.out.println("-CalculDpi-> dpi_aff:" + dpi_aff + " dpi_img: " + dpi_img);
    return (float) (Math.round((dim / ((dpi_aff * dpi_aff) / (dpi_img * INCH))) * 100) / 100.);
  }
  
  /**
   * Retourne le ratio qui permet de convertir la police � l'�cran vers la police de la description
   * @param dpi_img
   * @return
   */
  public static int getPoliceEcran2PoliceDescription(int dpi_img, int taille_police) {
    return Math.round(taille_police * ((float) DPI_ECRAN / dpi_img));
  }
  
  /**
   * Retourne le ratio qui permet de convertir la police de la description vers la police de l'�cran
   * @param dpi_img
   * @return
   */
  public static int getPoliceDescription2PoliceEcran(int dpi_img, int taille_police) {
    return Math.round(taille_police / ((float) DPI_ECRAN / dpi_img));
  }
}
