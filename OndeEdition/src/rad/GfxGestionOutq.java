/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package rad;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.ibm.as400.access.SpooledFileList;

import rad.Parametres.PreferencesManager;
import rad.SpoolEditor.GfxSpoolEditor;
import rad.outils.RiFileChooser;
import ri.seriem.libcommun.edition.ListContains;
import ri.seriem.libcommun.edition.Spool;
import ri.seriem.libcommun.outils.HashMapManager;
import ri.seriem.libcommun.outils.XMLTools;
import spool.GestionSpoolAS400;

//=================================================================================================
//==> Gestion des outqs
//=================================================================================================
public class GfxGestionOutq extends JPanel {
  // Constantes
  private static final long serialVersionUID = 1L;
  
  // Variables
  private GestionSpoolAS400 gestionSpool = null;
  private Spool spool = null;
  private HashMapManager listeSpoolEditor = null;
  private RiFileChooser selectionFichier = null;
  // private File dernierPath = new File(System.getProperty("user.home"));
  private PreferencesManager preference = null;
  
  /**
   * Constructeur
   */
  public GfxGestionOutq() {
    initComponents();
    bt_ListeSpool.setEnabled(false);
    selectionFichier = new RiFileChooser(null, System.getProperty("user.home"));
  }
  
  public void setPreference(PreferencesManager apreference) {
    preference = apreference;
  }
  
  /**
   * Lecture du fichier param�tre
   * @return
   */
  public void lectureInfosConnexion() {
    tf_Serveur.setText(preference.getPreferences().getServeur());
    tf_Profil.setText(preference.getPreferences().getProfil());
    pf_Mdp.setText(preference.getPreferences().getMdpclair());
  }
  
  /**
   * Enregistre les informations de l'utilisateur
   */
  private void sauveInfosConnexion() {
    boolean sauve = (!tf_Serveur.getText().trim().equals(preference.getPreferences().getServeur()))
        || (!tf_Profil.getText().trim().equals(preference.getPreferences().getProfil()))
        || (!new String(pf_Mdp.getPassword()).equals(preference.getPreferences().getMdpclair()));
    // System.out.println("--> sauve:" + sauve);
    if (!sauve)
      return;
    
    preference.getPreferences().setServeur(tf_Serveur.getText().trim());
    preference.getPreferences().setProfil(tf_Profil.getText().trim());
    preference.getPreferences().setMdpclair(new String(pf_Mdp.getPassword()));
    preference.sauvePreferencesUser();
  }
  
  /**
   * Connexion � l'I5
   * @return
   */
  private boolean connecter() {
    String mdp = new String(pf_Mdp.getPassword());
    String profil = tf_Profil.getText().trim().toUpperCase();
    tf_Profil.setText(profil);
    if (!tf_Serveur.getText().trim().equals("") && !profil.equals("") && !mdp.trim().equals(""))
      gestionSpool = new GestionSpoolAS400(tf_Serveur.getText(), tf_Profil.getText(), mdp);
    else {
      l_Erreur.setText("V�rifiez les identifiants que vous avez saisis.");
      return false;
    }
    
    if (gestionSpool.getSystem() == null) {
      l_Erreur.setText("V�rifiez les identifiants que vous avez saisis.");
      return false;
    }
    else {
      l_Erreur.setText("");
      return true;
    }
  }
  
  /**
   * Liste les spools d'une outq particuli�re
   * 
   */
  private void listeSpool() {
    if (gestionSpool == null) {
      l_Erreur.setText("V�rifiez le chemin de la OUTQ que vous souhaitez lister.");
      return;
    }
    if (chk_NomOutq.isSelected())
      majTable(gestionSpool.listeSpool(tf_Filtre.getText().trim(), tf_Outq.getText().trim(), false));
    else
      majTable(gestionSpool.listeSpool(tf_Filtre.getText().trim(), null, false));
  }
  
  /**
   * Mise � jour de la table contenant la liste des spools
   * @param listeSpool
   *
   *          private void majTable(SpooledFileList listeSpool)
   *          {
   *          // Cas d'un probl�me sur la listage de spools
   *          if (listeSpool == null)
   *          l_Erreur.setText("Probl�me lors du listage de la OUTQ.");
   *          else
   *          l_Erreur.setText("");
   * 
   *          table1.setModel(gestionSpool.getContainslListeSpool(listeSpool).getModeleTable());
   *          }
   */
  
  /**
   * Mise � jour de la table contenant la liste des spools
   * @param listeSpool
   */
  private void majTable(SpooledFileList listeSpool) {
    // Cas d'un probl�me sur la listage de spools
    if (listeSpool == null)
      l_Erreur.setText("Probl�me lors du listage de la OUTQ.");
    else
      l_Erreur.setText("");
    
    /*
    // Conversion des donn�es JSON en donn�e pour la JTable
    JSONParser parser = new JSONParser();
    JSONObject objet;
    String[] titres=null;
    String[][] data=null;
    try
    {
    	objet = (JSONObject) parser.parse(gestionSpool.getJSONListeSpool(listeSpool));
    	// Titre
    	JSONArray array=(JSONArray)objet.get("TITLE");
    	titres = new String[array.size()];
    	for (int i=0; i<array.size(); i++)
    		titres[i] = (String)array.get(i);
    	// Donn�es
    	JSONArray arraydata=(JSONArray)objet.get("DATA");
    	data = new String[arraydata.size()][titres.length];
    	for (int i=0; i<arraydata.size(); i++)
    	{
    		JSONArray arrayligne=(JSONArray)arraydata.get(i);
    		for (int j=0; j<arrayligne.size(); j++)
    			data[i][j] = (String)arrayligne.get(j);
    	}
    	table1.setModel(new ListContains(data, titres).getModeleTable());
    }
    catch (Exception e)
    {
    	JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur dans [GfxGestionOutq] (majTable)", JOptionPane.ERROR_MESSAGE);
    }*/
    table1.setModel(new ListContains(gestionSpool.getJSONListeSpool(listeSpool)).getModeleTable());
  }
  
  /**
   * Retourne le spool
   * @return
   */
  public Spool getSpool() {
    return spool;
  }
  
  /**
   * @return the listeSpoolEditor
   */
  public HashMapManager getListeSpoolEditor() {
    return listeSpoolEditor;
  }
  
  public void setListeSpoolEditor(HashMapManager lst) {
    listeSpoolEditor = lst;
  }
  
  /**
   * Enregistre le spool avec la boite de dialogue de sauvegarde
   * @return
   */
  private void saveSpool(String titre, Spool spool) {
    if (spool == null)
      return;
    if (titre == null)
      titre = "spool";
    
    // On construit le nom du fichier
    titre = selectionFichier.getDossier() + File.separatorChar + titre.trim().toLowerCase() + ".spl";
    File fichier = selectionFichier.enregistreFichier(new File(titre), "spl", "Fichier spool (*.spl)");
    
    // G�n�ration du fichier XML
    if (fichier == null)
      return;
    try {
      XMLTools.encodeToFile(spool, fichier.getAbsolutePath());
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  /**
   * Red�finition de la classe
   */
  public void dispose() {
    if (gestionSpool != null)
      gestionSpool.deconnecter();
  }
  
  // ---------------------------------- Ev�nements ---------------------------
  
  private void tbt_ConnexionActionPerformed(ActionEvent e) {
    // On est d�j� connect�
    if (tbt_Connexion.isSelected()) {
      if (!connecter()) {
        tbt_Connexion.setSelected(false);
        return;
      }
      l_Erreur.setText("");
      tbt_Connexion.setToolTipText("Cliquer pour vous d�connectez");
      tbt_Connexion.setIcon(new ImageIcon(getClass().getResource("/images/contact-etabli-icone-3684-32.png")));
      sauveInfosConnexion();
    }
    else // On est d�connect�
    {
      tbt_Connexion.setToolTipText("Cliquer pour vous connectez");
      tbt_Connexion.setIcon(new ImageIcon(getClass().getResource("/images/contact-pas-de-icone-7398-32.png")));
      if (gestionSpool != null)
        gestionSpool.deconnecter();
    }
    tf_Serveur.setEnabled(!tbt_Connexion.isSelected());
    tf_Profil.setEnabled(!tbt_Connexion.isSelected());
    pf_Mdp.setEnabled(!tbt_Connexion.isSelected());
    bt_ListeSpool.setEnabled(tbt_Connexion.isSelected());
  }
  
  private void bt_ListeSpoolActionPerformed(ActionEvent e) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        listeSpool();
      }
    });
  }
  
  private void mi_AfficherActionPerformed(ActionEvent e) {
    // R�cup�ration du spool
    if (gestionSpool == null)
      return;
    
    // On parcourt les lignes s�lectionn�es
    int[] selected = table1.getSelectedRows();
    if (selected.length > 0) {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      spool = gestionSpool.getSpool(gestionSpool.getSpooledFile(selected[0]));
      String titre = (String) table1.getValueAt(selected[0], 0);
      if (spool != null)
        listeSpoolEditor.addObject(new GfxSpoolEditor(titre, spool, listeSpoolEditor), titre);
      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }
  
  private void mi_EnregistrerActionPerformed(ActionEvent e) {
    // R�cup�ration du spool
    if (gestionSpool == null)
      return;
    
    // On parcourt les lignes s�lectionn�es
    int[] selected = table1.getSelectedRows();
    if (selected.length > 0) {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      spool = gestionSpool.getSpool(gestionSpool.getSpooledFile(selected[0]));
      if (spool != null)
        saveSpool((String) table1.getValueAt(selected[0], 0), spool);
      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }
  
  private void mi_ImprimerActionPerformed(ActionEvent e) {
    // R�cup�ration du spool
    if (gestionSpool == null)
      return;
    
    // On parcourt les lignes s�lectionn�es
    int[] selected = table1.getSelectedRows();
    if (selected.length > 0) {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      spool = gestionSpool.getSpool(gestionSpool.getSpooledFile(selected[0]));
    }
    
    if (spool != null) {
      PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
      DocFlavor flavor = DocFlavor.STRING.TEXT_PLAIN;
      PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
      PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
      PrintService service = ServiceUI.printDialog(null, 200, 200, printService, defaultService, flavor, pras);
      if (service != null) {
        DocPrintJob job = service.createPrintJob();
        DocAttributeSet das = new HashDocAttributeSet();
        Doc doc = new SimpleDoc(spool.toString(), flavor, das);
        try {
          job.print(doc, pras);
          try {
            Thread.sleep(10000);
          }
          catch (InterruptedException ex) {
            ex.printStackTrace();
          }
        }
        catch (PrintException ex) {
          ex.printStackTrace();
        }
      }
    }
    
    /*
    try
    {
    // Find the default service 
    DocFlavor flavor = DocFlavor.String;
    PrintService service = PrintServiceLookup.lookupDefaultPrintService();
    
    // Create the print job
    DocPrintJob job = service.createPrintJob();
    Doc doc = new SimpleDoc(spool, flavor, null);
    /
    // Monitor print job events; for the implementation of PrintJobWatcher,
    // see Determining When a Print Job Has Finished
    PrintJobWatcher pjDone = new PrintJobWatcher(job);
    // Print it
    job.print(doc, null);
    // Wait for the print job to be done
    pjDone.waitForDone();
    * 
    // Set up destination attribute
    PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
    //aset.add(new Destination(new java.net.URI("file:e:/temp/out.ps")));
    // Print it
    job.print(doc, null);
    }
    catch(Exception ex) {}
    */
    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
  }
  
  private void chk_NomOutqStateChanged(ChangeEvent e) {
    tf_Outq.setEditable(chk_NomOutq.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    label1 = new JLabel();
    tf_Serveur = new JTextField();
    label2 = new JLabel();
    tf_Profil = new JTextField();
    label3 = new JLabel();
    pf_Mdp = new JPasswordField();
    tbt_Connexion = new JToggleButton();
    panel3 = new JPanel();
    panel4 = new JPanel();
    label4 = new JLabel();
    tf_Filtre = new JTextField();
    chk_NomOutq = new JCheckBox();
    tf_Outq = new JTextField();
    bt_ListeSpool = new JButton();
    scrollPane1 = new JScrollPane();
    table1 = new JTable();
    l_Erreur = new JLabel();
    pm_Btd = new JPopupMenu();
    mi_Afficher = new JMenuItem();
    mi_Enregistrer = new JMenuItem();
    mi_Imprimer = new JMenuItem();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== panel1 ========
    {
      panel1.setBorder(new TitledBorder("Identification"));
      panel1.setName("panel1");
      panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
      
      // ---- label1 ----
      label1.setText("Serveur");
      label1.setName("label1");
      panel1.add(label1);
      
      // ---- tf_Serveur ----
      tf_Serveur.setMinimumSize(new Dimension(106, 27));
      tf_Serveur.setPreferredSize(new Dimension(106, 27));
      tf_Serveur.setName("tf_Serveur");
      panel1.add(tf_Serveur);
      
      // ---- label2 ----
      label2.setText("Profil");
      label2.setName("label2");
      panel1.add(label2);
      
      // ---- tf_Profil ----
      tf_Profil.setPreferredSize(new Dimension(100, 27));
      tf_Profil.setName("tf_Profil");
      panel1.add(tf_Profil);
      
      // ---- label3 ----
      label3.setText("Mot de passe");
      label3.setName("label3");
      panel1.add(label3);
      
      // ---- pf_Mdp ----
      pf_Mdp.setPreferredSize(new Dimension(100, 27));
      pf_Mdp.setName("pf_Mdp");
      panel1.add(pf_Mdp);
      
      // ---- tbt_Connexion ----
      tbt_Connexion.setIcon(new ImageIcon(getClass().getResource("/images/contact-pas-de-icone-7398-32.png")));
      tbt_Connexion.setToolTipText("Cliquer pour vous connecter");
      tbt_Connexion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_Connexion.setName("tbt_Connexion");
      tbt_Connexion.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          tbt_ConnexionActionPerformed(e);
        }
      });
      panel1.add(tbt_Connexion);
    }
    add(panel1, BorderLayout.NORTH);
    
    // ======== panel3 ========
    {
      panel3.setBorder(new TitledBorder("Liste des spools"));
      panel3.setName("panel3");
      panel3.setLayout(new BorderLayout());
      
      // ======== panel4 ========
      {
        panel4.setName("panel4");
        panel4.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        // ---- label4 ----
        label4.setText("Filtre");
        label4.setName("label4");
        panel4.add(label4);
        
        // ---- tf_Filtre ----
        tf_Filtre.setPreferredSize(new Dimension(80, 27));
        tf_Filtre.setText("*ALL");
        tf_Filtre.setName("tf_Filtre");
        panel4.add(tf_Filtre);
        
        // ---- chk_NomOutq ----
        chk_NomOutq.setText("OutQ");
        chk_NomOutq.setName("chk_NomOutq");
        chk_NomOutq.addChangeListener(new ChangeListener() {
          public void stateChanged(ChangeEvent e) {
            chk_NomOutqStateChanged(e);
          }
        });
        panel4.add(chk_NomOutq);
        
        // ---- tf_Outq ----
        tf_Outq.setText("/QSYS.LIB/QGPL.LIB/QPRINT.OUTQ");
        tf_Outq.setPreferredSize(new Dimension(300, 27));
        tf_Outq.setName("tf_Outq");
        panel4.add(tf_Outq);
        
        // ---- bt_ListeSpool ----
        bt_ListeSpool.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_ListeSpool.setIcon(new ImageIcon(getClass().getResource("/images/demande-sous-bois-mime-x-icone-9147-32.png")));
        bt_ListeSpool.setToolTipText("Rafra\u00eechit la liste des spools");
        bt_ListeSpool.setName("bt_ListeSpool");
        bt_ListeSpool.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bt_ListeSpoolActionPerformed(e);
          }
        });
        panel4.add(bt_ListeSpool);
      }
      panel3.add(panel4, BorderLayout.NORTH);
      
      // ======== scrollPane1 ========
      {
        scrollPane1.setName("scrollPane1");
        
        // ---- table1 ----
        table1.setComponentPopupMenu(pm_Btd);
        table1.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table1.setName("table1");
        scrollPane1.setViewportView(table1);
      }
      panel3.add(scrollPane1, BorderLayout.CENTER);
      
      // ---- l_Erreur ----
      l_Erreur.setForeground(Color.red);
      l_Erreur.setFont(l_Erreur.getFont().deriveFont(l_Erreur.getFont().getStyle() | Font.BOLD));
      l_Erreur.setName("l_Erreur");
      panel3.add(l_Erreur, BorderLayout.SOUTH);
    }
    add(panel3, BorderLayout.CENTER);
    
    // ======== pm_Btd ========
    {
      pm_Btd.setName("pm_Btd");
      
      // ---- mi_Afficher ----
      mi_Afficher.setText("Afficher");
      mi_Afficher.setName("mi_Afficher");
      mi_Afficher.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          mi_AfficherActionPerformed(e);
        }
      });
      pm_Btd.add(mi_Afficher);
      
      // ---- mi_Enregistrer ----
      mi_Enregistrer.setText("Enregistrer");
      mi_Enregistrer.setName("mi_Enregistrer");
      mi_Enregistrer.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          mi_EnregistrerActionPerformed(e);
        }
      });
      pm_Btd.add(mi_Enregistrer);
      
      // ---- mi_Imprimer ----
      mi_Imprimer.setText("Imprimer");
      mi_Imprimer.setEnabled(false);
      mi_Imprimer.setVisible(false);
      mi_Imprimer.setName("mi_Imprimer");
      mi_Imprimer.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          mi_ImprimerActionPerformed(e);
        }
      });
      pm_Btd.add(mi_Imprimer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel label1;
  private JTextField tf_Serveur;
  private JLabel label2;
  private JTextField tf_Profil;
  private JLabel label3;
  private JPasswordField pf_Mdp;
  private JToggleButton tbt_Connexion;
  private JPanel panel3;
  private JPanel panel4;
  private JLabel label4;
  private JTextField tf_Filtre;
  private JCheckBox chk_NomOutq;
  private JTextField tf_Outq;
  private JButton bt_ListeSpool;
  private JScrollPane scrollPane1;
  private JTable table1;
  private JLabel l_Erreur;
  private JPopupMenu pm_Btd;
  private JMenuItem mi_Afficher;
  private JMenuItem mi_Enregistrer;
  private JMenuItem mi_Imprimer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
