/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package guiServeur;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Properties;

import javax.swing.JOptionPane;

/*
 * Created on Jan 24, 2007
 *
 */
public class gestionProperties {
  
  private static String urlConfig = System.getProperty("user.home") + File.separator + "serveurImpression.properties";
  private File proprietes = new File(urlConfig);
  
  public gestionProperties() {
    super();
  }
  
  /**
   * Cette m�thode stocke le fichier Properties � l'emplacement sp�cifi�
   */
  public void saveProperties(Properties props, String comments) {
    OutputStream out;
    try {
      out = new FileOutputStream(urlConfig);
      props.store(out, comments);
      out.flush();
      out.close();
    }
    catch (FileNotFoundException e) {
      JOptionPane.showMessageDialog(null, e.toString());
    }
    catch (IOException e) {
      JOptionPane.showMessageDialog(null, e.toString());
    }
    
  }
  
  /**
   * Cette m�thode lit un fichier Properties � l'emplacement sp�cifi�
   */
  public Properties loadProperties() throws IOException {
    Properties props = new Properties();
    try {
      props.load(new FileInputStream(urlConfig));
    }
    catch (FileNotFoundException e) {
      // JOptionPane.showMessageDialog(null, e.toString());
      // JOptionPane.showInternalMessageDialog(null, "Le fichier des pr�f�rence va �tre automatiquement cr��", "Fichier innexistant",
      // JOptionPane.INFORMATION_MESSAGE, null);
      // JOptionPane.showMessageDialog(null, "Le fichier des pr�f�rences n'existe pas sur votre disque : il va �tre automatiquement
      // cr��");
    }
    catch (IOException e) {
      JOptionPane.showMessageDialog(null, e.toString());
    }
    return props;
  }
  
  /**
   * 
   * Cette m�thode affiche chaque paire [cl�,valuer] d'un fichier Properties
   * 
   * @param props Le fichier � afficher
   */
  public void getProperties(Properties props) {
    Iterator<?> it = props.keySet().iterator();
    while (it.hasNext()) {
      String propertyName = (String) it.next();
      String propertyValue = props.getProperty(propertyName);
      System.out.println(propertyName + "=" + propertyValue);
    }
  }
  
  // est ce que le fichier des propri�t�s existe sur disque ?
  public boolean propsExist() {
    return proprietes.exists();
  }
  
  public void creationProps() {
    try {
      new File(urlConfig).createNewFile();
    }
    catch (IOException e) {
      JOptionPane.showMessageDialog(null, e.toString());
    }
  }
}
