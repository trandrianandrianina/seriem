/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package guiServeur;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class guiServeur {
  
  /**
   * @param args
   */
  
  private static gestionProperties fich = new gestionProperties();
  // logo de l'application
  private static String logo = "/guiServeur/images/ri_logo.png";
  
  // @SuppressWarnings("static-access")
  public static void main(String[] args) {
    // force nimbus comme look and feel
    for (UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
      if ("Nimbus".equals(laf.getName())) {
        try {
          UIManager.setLookAndFeel(laf.getClassName());
          
        }
        catch (Exception e) {
          // TODO: handle exception
        }
      }
    }
    
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        new guiAffichage(fich, logo);
      }
    });
    
  }
  
}
