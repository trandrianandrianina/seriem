/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package guiServeur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * @author HB
 */
public class guiAffichage extends JFrame {
  
  private static final long serialVersionUID = 1L;
  public static final String VERSION = "1.02";
  
  private preferences pref = null;
  private String etat = null;
  // private Image imaggePlay = null;
  // private Image imaggeStop = null;
  private controleur ctrl = null;
  private gestionProperties fichierIni = null;
  
  public guiAffichage(gestionProperties fich, String logo) {
    super();
    fichierIni = fich;
    pref = new preferences(fichierIni, logo);
    // on ouvre la popup de param�trage si le fichier n'existe pas sur disque
    if (!fichierIni.propsExist()) {
      pref.reveiller();
      fichierIni.creationProps();
    }
    
    ctrl = new controleur(pref);
    initComponents();
    
    setData();
  }
  
  public void setData() {
    if (ctrl.isActif()) {
      etat = "D�marr�";
      // buttonPlay.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/play1.png")));
      buttonPlay.setVisible(false);
      buttonStop.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/stop1.png")));
      buttonStop.setEnabled(true);
      buttonStop.setVisible(true);
    }
    else {
      etat = "Arr�t�";
      buttonPlay.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/play1.png")));
      buttonPlay.setEnabled(true);
      buttonPlay.setVisible(true);
      buttonStop.setVisible(false);
      // buttonStop.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/stop1.png")));
    }
    
    setTitle("Serveur d'�ditions (Etat=" + etat + ")");
  }
  
  private void verifArret() {
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        if (!ctrl.isActif()) {
          setData();
          this.cancel();
        }
        else
          setTitle("Serveur d'�ditions (Arr�t en cours)");
      }
    };
    
    Timer timer = new Timer();
    timer.scheduleAtFixedRate(task, 0, 1000);
  }
  
  private void buttonPlayActionPerformed(ActionEvent e) {
    buttonPlay.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/play.png")));
    buttonPlay.setEnabled(false);
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        ctrl.startSvr();
        setData();
      }
    });
  }
  
  private void buttonStopActionPerformed(ActionEvent e) {
    buttonStop.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/stop.png")));
    buttonStop.setEnabled(false);
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        ctrl.stopSvr();
        verifArret();
      }
    });
  }
  
  private void buttonPrefActionPerformed(ActionEvent e) {
    pref.reveiller();
    ctrl.setPreferences(pref);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    buttonPlay = new JButton();
    buttonStop = new JButton();
    buttonPref = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(300, 120));
    setVisible(true);
    setBackground(new Color(102, 102, 102));
    setResizable(false);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/guiServeur/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel1 ========
    {
      panel1.setBackground(new Color(102, 102, 102));
      panel1.setName("panel1");
      panel1.setLayout(null);
      
      // ---- buttonPlay ----
      buttonPlay.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/play.png")));
      buttonPlay.setToolTipText("Cliquer pour d\u00e9marrer le serveur");
      buttonPlay.setOpaque(true);
      buttonPlay.setBackground(new Color(102, 102, 102));
      buttonPlay.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      buttonPlay.setName("buttonPlay");
      buttonPlay.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          buttonPlayActionPerformed(e);
        }
      });
      panel1.add(buttonPlay);
      buttonPlay.setBounds(25, 10, 110, 70);
      
      // ---- buttonStop ----
      buttonStop.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/stop.png")));
      buttonStop.setToolTipText("Cliquer pour arr\u00eater le serveur");
      buttonStop.setBackground(new Color(102, 102, 102));
      buttonStop.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      buttonStop.setName("buttonStop");
      buttonStop.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          buttonStopActionPerformed(e);
        }
      });
      panel1.add(buttonStop);
      buttonStop.setBounds(25, 10, 110, 70);
      
      // ---- buttonPref ----
      buttonPref.setIcon(new ImageIcon(getClass().getResource("/guiServeur/images/pref.png")));
      buttonPref.setToolTipText("Pr\u00e9f\u00e9rences");
      buttonPref.setBackground(new Color(102, 102, 102));
      buttonPref.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      buttonPref.setName("buttonPref");
      buttonPref.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          buttonPrefActionPerformed(e);
        }
      });
      panel1.add(buttonPref);
      buttonPref.setBounds(155, 10, 110, 70);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(panel1, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JButton buttonPlay;
  private JButton buttonStop;
  private JButton buttonPref;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
