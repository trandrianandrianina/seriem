/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package guiServeur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import rad.outils.RiFileChooser;

/**
 * @author St�phane V�n�ri
 */
public class preferences extends JDialog {
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private Properties fichierIni = null;
  private gestionProperties fich = null;
  
  public preferences(gestionProperties pref, String logo) {
    super();
    initComponents();
    fich = pref;
    
    this.setVisible(false);
    setData();
  }
  
  public void setData() {
    // affichage
    setTitle("Version : " + guiAffichage.VERSION + " - Serveur d'�ditions : pr�f�rences");
    
    attention.setText("");
    adresseIP.setForeground(Color.BLACK);
    
    // adresse IP
    adresseIP.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        testIP();
      }
    });
    
    // initialisation
    lectureFichier();
  }
  
  public void getData() {
    fichierIni.setProperty("url", chemin.getText());
    fichierIni.setProperty("ip", adresseIP.getText());
    fichierIni.setProperty("port", port.getText());
    
    fich.saveProperties(fichierIni, null);
  }
  
  /*
   * r�veil de la popup
   */
  public void reveiller() {
    setData();
    this.setVisible(true);
  }
  
  public String getProperty(String propName) {
    return fichierIni.getProperty(propName);
  }
  
  public void setProperty(String propName, String propData) {
    fichierIni.setProperty(propName, propData);
  }
  
  /**
   * permet de lire dans le fichier "parametres.txt" et y r�cup�rer le dossier de travail m�moris�
   * @param fichier
   * @return String
   */
  private void lectureFichier() {
    
    try {
      fichierIni = fich.loadProperties();
    }
    catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      System.out.println("lecture");
      e.printStackTrace();
    }
    catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    chemin.setText(fichierIni.getProperty("url"));
    adresseIP.setText(fichierIni.getProperty("ip"));
    port.setText(fichierIni.getProperty("port"));
    
  }
  
  private void testIP() {
    if (!adresseIP.getText().matches(
        "^([O1]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([O1]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([O1]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([O1]?\\d\\d?|2[0-4]\\d|25[0-5])$")) {
      attention.setText("Adresse IP incorrecte");
      adresseIP.setForeground(Color.RED);
    }
    else {
      attention.setText("");
      adresseIP.setForeground(Color.BLACK);
    }
  }
  
  /**
   * Ouvre une boite de dialogue afin de s�lectionner un dossier
   */
  private File getSelectionDossier(String origine) {
    if (origine == null)
      origine = chemin.getText();
    RiFileChooser selectionDossier = new RiFileChooser(null, origine);
    return selectionDossier.choisirDossier(null);
  }
  
  private void btnEnregistrerActionPerformed(ActionEvent e) {
    if (attention.getText().trim().equals("")) {
      getData();
      this.setVisible(false);
    }
  }
  
  private void btnFermerActionPerformed(ActionEvent e) {
    this.setVisible(false);
  }
  
  private void adresseIPFocusLost(FocusEvent e) {
    testIP();
  }
  
  private void adresseIPActionPerformed(ActionEvent e) {
    testIP();
  }
  
  private void btnParcourirActionPerformed(ActionEvent e) {
    File dossier = getSelectionDossier(null);
    if (dossier != null)
      chemin.setText(dossier.getAbsolutePath());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    chemin = new JTextField();
    port = new JTextField();
    btnEnregistrer = new JButton();
    btnFermer = new JButton();
    adresseIP = new JTextField();
    attention = new JLabel();
    btnParcourir = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(740, 190));
    setResizable(false);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel1 ========
    {
      panel1.setBackground(new Color(102, 102, 102));
      panel1.setName("panel1");
      panel1.setLayout(null);
      
      // ---- label1 ----
      label1.setText("Chemin racine du serveur");
      label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 3f));
      label1.setForeground(Color.white);
      label1.setName("label1");
      panel1.add(label1);
      label1.setBounds(15, 15, 195, label1.getPreferredSize().height);
      
      // ---- label2 ----
      label2.setText("Adresse IP du serveur");
      label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 3f));
      label2.setForeground(Color.white);
      label2.setName("label2");
      panel1.add(label2);
      label2.setBounds(15, 55, 195, label2.getPreferredSize().height);
      
      // ---- label3 ----
      label3.setText("Port du serveur");
      label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 3f));
      label3.setForeground(Color.white);
      label3.setName("label3");
      panel1.add(label3);
      label3.setBounds(15, 95, 195, label3.getPreferredSize().height);
      
      // ---- chemin ----
      chemin.setOpaque(true);
      chemin.setForeground(Color.black);
      chemin.setFont(chemin.getFont().deriveFont(chemin.getFont().getSize() + 3f));
      chemin.setName("chemin");
      panel1.add(chemin);
      chemin.setBounds(220, 9, 505, chemin.getPreferredSize().height);
      
      // ---- port ----
      port.setOpaque(true);
      port.setForeground(Color.black);
      port.setFont(port.getFont().deriveFont(port.getFont().getSize() + 3f));
      port.setName("port");
      panel1.add(port);
      port.setBounds(220, 89, 50, port.getPreferredSize().height);
      
      // ---- btnEnregistrer ----
      btnEnregistrer.setText("Enregistrer");
      btnEnregistrer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btnEnregistrer.setName("btnEnregistrer");
      btnEnregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btnEnregistrerActionPerformed(e);
        }
      });
      panel1.add(btnEnregistrer);
      btnEnregistrer.setBounds(590, 130, 110, btnEnregistrer.getPreferredSize().height);
      
      // ---- btnFermer ----
      btnFermer.setText("Fermer");
      btnFermer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btnFermer.setName("btnFermer");
      btnFermer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btnFermerActionPerformed(e);
        }
      });
      panel1.add(btnFermer);
      btnFermer.setBounds(703, 130, 110, btnFermer.getPreferredSize().height);
      
      // ---- adresseIP ----
      adresseIP.setFont(adresseIP.getFont().deriveFont(adresseIP.getFont().getSize() + 3f));
      adresseIP.setName("adresseIP");
      adresseIP.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          adresseIPActionPerformed(e);
        }
      });
      adresseIP.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          adresseIPFocusLost(e);
        }
      });
      panel1.add(adresseIP);
      adresseIP.setBounds(220, 49, 125, 32);
      
      // ---- attention ----
      attention.setForeground(Color.red);
      attention.setFont(attention.getFont().deriveFont(attention.getFont().getStyle() | Font.BOLD));
      attention.setName("attention");
      panel1.add(attention);
      attention.setBounds(10, 130, 475, 30);
      
      // ---- btnParcourir ----
      btnParcourir.setText("Parcourir");
      btnParcourir.setIcon(null);
      btnParcourir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btnParcourir.setName("btnParcourir");
      btnParcourir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btnParcourirActionPerformed(e);
        }
      });
      panel1.add(btnParcourir);
      btnParcourir.setBounds(new Rectangle(new Point(735, 11), btnParcourir.getPreferredSize()));
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(panel1, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JTextField chemin;
  private JTextField port;
  private JButton btnEnregistrer;
  private JButton btnFermer;
  private JTextField adresseIP;
  private JLabel attention;
  private JButton btnParcourir;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
