/*
 * Copyright (C) R�solution Informatique - Tout droits r�serv�s.
 * Les copies non autoris�es de ce fichier, quel que soit le m�dia, sont strictements interdites.
 */

package guiServeur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;

import ri.seriem.libcommun.edition.ConstantesNewSim;

public class controleur {
  
  private boolean actif = false;
  private String dossierStop = null;
  
  public controleur(preferences pref) {
    super();
    
    setPreferences(pref);
  }
  
  public void setPreferences(preferences pref) {
    if (pref.getProperty("url") != null)
      dossierStop = pref.getProperty("url");
    else
      dossierStop = null;
  }
  
  public boolean isActif() {
    try {
      String line;
      
      Process p = Runtime.getRuntime().exec("tasklist /FI \"IMAGENAME eq NewSimSrv.exe\" /FO csv");
      BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
      actif = false;
      while ((line = input.readLine()) != null) {
        if (line.contains("NewSimSrv.exe")) {
          actif = true;
        }
      }
      input.close();
      p.destroy();
    }
    catch (Exception err) {
      JOptionPane.showMessageDialog(null, err.toString());
    }
    return actif;
  }
  
  public void startSvr() {
    if (isActif())
      return;
    File exe = new File(dossierStop + File.separatorChar + "lib" + File.separatorChar + "NewSimSrv.exe");
    if (!exe.exists())
      return;
    
    try {
      Runtime.getRuntime().exec(exe.getAbsolutePath(), null);
    }
    catch (Exception e1) {
      // TODO Auto-generated catch block
      JOptionPane.showMessageDialog(null, e1.toString());
    }
  }
  
  public void stopSvr() {
    if (!isActif())
      return;
    try {
      if (dossierStop == null)
        return;
      File stop = new File(dossierStop + File.separatorChar + ConstantesNewSim.DOSSIER_CTRL + File.separatorChar + "NewSimSrv."
          + ConstantesNewSim.DOSSIER_CTRL);
      if (stop.exists())
        return;
      
      FileWriter fw = new FileWriter(stop, false);
      BufferedWriter output = new BufferedWriter(fw);
      output.write("stop");
      output.flush();
      output.close();
    }
    catch (IOException ioe) {
      JOptionPane.showMessageDialog(null, ioe.toString());
    }
  }
  
}
